package pe.com.indra.reporte.mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

import pe.com.indra.calidad.util.Sql;
import pe.com.indra.calidad.mbean.FileUtilUploadBean;

@ManagedBean(name="procesoReporteUploadBean")
@SessionScoped
public class ProcesoReporteUploadBean extends FileUtilUploadBean implements Serializable {
	
	
		public ProcesoReporteUploadBean(){
			filaInicial = 2;
			numColumnas = 2;
			columnas = "1,2,3";
			tipoProceso = "1";
			nombreColumnas = "NUMSUMINISTRO,FECSUBSANACION,ESTADOSUBSANACION";
			
			itemsNombreColumnas=new ArrayList<SelectItem>();
			itemsNombreColumnas.add(new SelectItem("1",nombreColumnas));
			
			itemsSeparadorColumnas=new ArrayList<SelectItem>();
			itemsSeparadorColumnas.add(new SelectItem("\t","TAB"));
			
			itemsFormatoFecha=new ArrayList<SelectItem>();
			itemsFormatoFecha.add(new SelectItem("YYYY-MM-DD","YYYY-MM-DD"));
			//itemsFormatoFecha.add(new SelectItem("DD/MM/YYYY","DD/MM/YYYY"));
			
			columnas = "1,2,3";
			
			this.nombreArchivoBase = "compensacion";
		}
		

		public String procesar() throws Exception{
		        //List<String[]> lst = getListStringDesdeCsv();
		        //int num = lst.size();
				registrarCsv();
		        //actualizarTodo("2016","S2");
		        
		        /*
		    	for (int i = 1;i<num;i=i+1) {
		    		System.out.println("registro :" + i + " de " + num);
		    		String[] e = lst.get(i);
		    		if (e[0]!=null && !e[0].isEmpty() && e[1]!=null && !e[1].isEmpty() && e[1]!="0")
		    			actualizarRegistro("2016","S2",e[0],e[1]);
				}*/
		    return "/zonasegura/proc228/deficienciaListar?faces-redirect=true"; //retornar a la pagina que deseas por Ejemplo a medicionListar.xhtml donde esta el boton
		}	

		public String prepararMostrarCarga(){
			return "/zonasegura/reporte/cargaMasiva?faces-redirect=true";
		}

		public String actualizarTodo (String anio, String semestre) {
			String mensaje = null;
			Sql sql = new Sql("CALIDAD");
			String resultado = null;
	        String updateSql = "UPDATE CAL_COMPENSACION_SUMINISTRO SET MON_COM_LEY_CONCESIONES = 0 WHERE ANO = " + anio + " AND SEMESTRE = '" + semestre + "'";
			System.out.println(updateSql);
			resultado = sql.ejecuta(updateSql);
			return mensaje;
		}
		
		public String actualizarRegistro (String anio, String semestre,String numSuministro, String monto) {
			//from Deficiencia i where i.periodoMedicion.perMedicionId = :perMedicionId order by i.fecDenuncia desc
			String updateSql;
			String mensaje = null;
			Sql sql = new Sql("CALIDAD");
			String resultado = null;
			//YYYY-MM-DD
			//TO_DATE('2011-07-28T23:54:14Z',  'YYYY-MM-DD"T"HH24:MI:SS"Z"')
			
			
			
			
			String idSql = "SELECT COM_SUMINISTRO_ID FROM CAL_COMPENSACION_SUMINISTRO WHERE NUM_SUM_AFECTADO = " + numSuministro + " AND ANO = " + anio + " AND SEMESTRE = '" + semestre + "' AND TIP_COMPENSACION IN ('123PSU','123PUNU','456ENU')";
			updateSql = "UPDATE CAL_COMPENSACION_SUMINISTRO SET MON_COM_LEY_CONCESIONES = TO_NUMBER('" + monto + "', '99999999.99999999') WHERE COM_SUMINISTRO_ID = ("  +  idSql + ")";
			System.out.println(updateSql);
			resultado = sql.ejecuta(updateSql);
			//AND TIP_COMPENSACION IN ('123PUNU','456ENU')
			
			return mensaje;
		}
		
		
		
		
		
		
		


}
