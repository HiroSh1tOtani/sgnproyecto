package pe.com.indra.calidad.mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.entity.IntervaloFRTension;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.service.IntervaloFRTensionService;
import pe.com.indra.calidad.service.MedicionService;
import pe.com.indra.calidad.util.Utilidad;

@ManagedBean(name="intervaloFRTensionBean")
@SessionScoped
public class IntervaloFRTensionBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<IntervaloFRTension> IntervalosFRTension;
	private IntervaloFRTension intervaloFRTension;
	
	private long medicionId;
	private Medicion medicion;
	
	
	private String mensaje;
	
	@PostConstruct 
	public void init(){
		System.out.println("se ejecuto PostConstruct "+new java.util.Date());
		intervaloFRTension=new IntervaloFRTension();
		intervaloFRTension.setEstado("1");
		
	}
	
	public List<IntervaloFRTension> getIntervalosFRTension() {
		IntervaloFRTensionService service=new IntervaloFRTensionService();
		IntervalosFRTension=service.getAllRows();
		return IntervalosFRTension;
	}

	public void setIntervalosFRTension(List<IntervaloFRTension> intervalosFRTension) {
		IntervalosFRTension = intervalosFRTension;
	}
	
		
	public IntervaloFRTension getIntervaloFRTension() {
		
		return intervaloFRTension;
	}

	public void setIntervaloFRTension(IntervaloFRTension intervaloFRTension) {
		this.intervaloFRTension = intervaloFRTension;
	}
	
		
	public String prepararCrear(){
		//Esto funciona siempre que el bean tenga ambito de sesion 
		setIntervaloFRTension(new IntervaloFRTension());
		getIntervaloFRTension().setEstado("1");
		return "/zonasegura/IntervaloFRTensionCrear?faces-redirect=true";
	}
	
	public String prepararEditar(){
		
		try {
			cargarIntervaloFRTensionActual();
			return "/zonasegura/intervaloFRTensionEditar?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/intervaloFRTensionListar?faces-redirect=true";
		}
	}

	public String prepararVer(){
		/*
		if(intervaloFRTension == null){
			return null;
		}
		*/
		try {
			cargarIntervaloFRTensionActual();
			//return "/zonasegura/intervaloFRTensionVer?faces-redirect=true";
			return "/zonasegura/intervaloFRTensionVer?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/intervaloTensionListar?faces-redirect=true";
		}
	}
	
	public String salvar(){
				
		IntervaloFRTensionService service=new IntervaloFRTensionService();
		
		try {
			service.create(intervaloFRTension);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/intervaloFRTensionListar?faces-redirect=true";
	}
	
	public String actualizar(){
			
		IntervaloFRTensionService service=new IntervaloFRTensionService();
		
		try {
			service.update(intervaloFRTension);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/intervaloFRTensionListar?faces-redirect=true";
	}
	
	public String eliminar(){
		String intFRTensionId=Utilidad.getParametro("intFRTensionId");
		IntervaloFRTensionService service=new IntervaloFRTensionService();
		
		
		try {
			intervaloFRTension=service.ReadById(new Long(intFRTensionId));
			intervaloFRTension.setEstado("0");
			//service.delete(parametroNorma.getParNormaId());
			service.update(intervaloFRTension);
		} catch (HibernateException e) {
			
			
			System.out.println(e.getMessage()+":"+e.getCause());
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			
		} catch (Exception e) {
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/intervaloFRTensionListar?faces-redirect=true";
	}
	
	public void cargarIntervaloFRTensionActual(){
		
		String medicionIdStr=Utilidad.getParametro("medicionId");		
		medicionId = Long.valueOf(medicionIdStr); //capturar valor
				
		IntervaloFRTensionService intervaloFRTensionService =  new IntervaloFRTensionService();
		
		
		try {
			intervaloFRTension = intervaloFRTensionService.getAllRows1(medicionId);
			
			
			if(intervaloFRTension == null){
				return;
			}
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

			throw new HibernateException("No se puede cargar IntervaloFRTension.", e);
			
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public long getMedicionId() {
		return medicionId;
	}

	public void setMedicionId(long medicionId) {
		this.medicionId = medicionId;
	}

	public Medicion getMedicion() {
		MedicionService service=new MedicionService();
		medicion=service.ReadById(new Long(medicionId));
		return medicion;
	}

	public void setMedicion(Medicion medicion) {
		this.medicion = medicion;
	}
}
