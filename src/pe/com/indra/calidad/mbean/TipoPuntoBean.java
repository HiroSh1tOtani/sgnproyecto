package pe.com.indra.calidad.mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

import pe.com.indra.calidad.entity.TipoPunto;
import pe.com.indra.calidad.service.TipoPuntoService;

@ManagedBean(name="tipoPuntoBean")
@SessionScoped
public class TipoPuntoBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TipoPuntoService service=null;
	
	private List<TipoPunto> tipopuntos=null;

	@PostConstruct
	public void init(){
		service=new TipoPuntoService();
		tipopuntos=service.getAllRows();
	}
	
	public List<TipoPunto> getTipopuntos() {
		tipopuntos=(List<TipoPunto>) service.getAllRows();
		return tipopuntos;
	}

	public void setTipopuntos(List<TipoPunto> tipopuntos) {
		this.tipopuntos = tipopuntos;
	}
	
}
