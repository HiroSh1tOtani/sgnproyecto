package pe.com.indra.calidad.mbean;

import java.util.Set;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import pe.com.indra.calidad.entity.TipoTrabajo;
import pe.com.indra.calidad.service.TipoTrabajoService;

@ManagedBean(name="tipoTrabajoBean")
@SessionScoped
public class TipoTrabajoBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TipoTrabajoService service = null;
	
	private List<TipoTrabajo> tipoTrabajos = null;

	@PostConstruct
	public void init(){
		service=new TipoTrabajoService();
		tipoTrabajos = service.getAllRows();
	}
	
	public List<TipoTrabajo> getTipoTrabajos() {
		tipoTrabajos = (List<TipoTrabajo>) service.getAllRows();
		return tipoTrabajos;
	}

	public void setTipoTrabajos(List<TipoTrabajo> tipoTrabajos) {
		this.tipoTrabajos = tipoTrabajos;
	}
}
