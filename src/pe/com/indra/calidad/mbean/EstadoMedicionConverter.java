package pe.com.indra.calidad.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.indra.calidad.entity.EstadoMedicion;
import pe.com.indra.calidad.service.EstadoMedicionService;

@FacesConverter(value="estadoMedicionConverter",forClass=EstadoMedicionConverter.class)

public class EstadoMedicionConverter implements Converter{

	@Override
	public EstadoMedicion getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long tipParametroId=new Long(cadena);
		EstadoMedicionService service=new EstadoMedicionService();
		EstadoMedicion estadoMedicion=new EstadoMedicion();
		estadoMedicion=service.ReadById(tipParametroId);
		System.out.println(estadoMedicion.getCodEstMedicion()+":"+estadoMedicion.getDescripcion());
		return estadoMedicion;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			EstadoMedicion estadoMedicion=(EstadoMedicion) objeto;
			//System.out.println("objeto:"+new Long(estadoMedicion.getEstMedicionId()).toString());
			return new Long(estadoMedicion.getEstMedicionId()).toString();
		}
		return null;
	}


}
