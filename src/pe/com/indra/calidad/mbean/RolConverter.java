package pe.com.indra.calidad.mbean;

import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.indra.calidad.entity.Rol;
import pe.com.indra.calidad.service.RolService;


@FacesConverter("rolConverter")
@SessionScoped
public class RolConverter implements Converter{

	@Override
	public Rol getAsObject(FacesContext arg0, UIComponent arg1, String roleName) {
		RolService service = new RolService();
		//System.out.println("BUSCANDO EL ROL POR NOMBRE");
		Rol rol = service.ReadById(roleName);
		System.out.println("ROL "+rol.getRolename()+" ENCONTRADO");
		return rol;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			Rol rol = (Rol) objeto;
			//System.out.println("NOMBRE DEL ROL "+rol.getRolename()+" ENCONTRADO");
			return rol.getRolename();
		}
		return null;
	}


}

