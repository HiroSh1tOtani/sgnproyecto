package pe.com.indra.calidad.mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.entity.CompensacionUnitaria;
import pe.com.indra.calidad.entity.TipoParametro;
import pe.com.indra.calidad.service.CompensacionUnitariaService;
import pe.com.indra.calidad.service.TipoParametroService;
import pe.com.indra.calidad.util.Utilidad;

@ManagedBean(name="compensacionUnitariaBean")
@SessionScoped

public class CompensacionUnitariaBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<CompensacionUnitaria> compensacionUnitarias;
	private List<CompensacionUnitaria> compensacionUnitarias1;
	private CompensacionUnitaria compensacionUnitaria;
	private List<SelectItem> itemsTipoParametro;
	private List<SelectItem> itemsTipoParametro1;
	
	private List<SelectItem> itemsCodigo;
	

	private String mensaje;
	
	@PostConstruct 
	public void init(){
		System.out.println("se ejecuto PostConstruct "+new java.util.Date());
		compensacionUnitaria=new CompensacionUnitaria();
		compensacionUnitaria.setEstado("1");
		/*
		String compensacionUnitariaId=Utilidad.getParametro("compensacionUnitariaId");
		if(compensacionUnitariaId==null){
		  compensacionUnitaria=new CompensacionUnitaria();
		  compensacionUnitaria.setEstado("1");
		}else{
			CompensacionUnitariaService service=new CompensacionUnitariaService();
			compensacionUnitaria=service.ReadById(new Long(compensacionUnitariaId));
		}
		*/
	}
	
	public List<CompensacionUnitaria> getCompensacionUnitarias() {
		CompensacionUnitariaService service=new CompensacionUnitariaService();
		compensacionUnitarias= service.getAllRows();
		return compensacionUnitarias;
	}

	public void setCompensacionUnitarias(List<CompensacionUnitaria> compensacionUnitarias) {
		this.compensacionUnitarias = compensacionUnitarias;
	}

	
	
	public List<CompensacionUnitaria> getCompensacionUnitarias1() {
		CompensacionUnitariaService service=new CompensacionUnitariaService();
		compensacionUnitarias1= service.getAllRows1();
		return compensacionUnitarias1;
	}

	public void setCompensacionUnitarias1(
			List<CompensacionUnitaria> compensacionUnitarias1) {
		this.compensacionUnitarias1 = compensacionUnitarias1;
	}

	public CompensacionUnitaria getCompensacionUnitaria() {
		
		return compensacionUnitaria;
	}

	public void setCompensacionUnitaria(CompensacionUnitaria compensacionUnitaria) {
		this.compensacionUnitaria = compensacionUnitaria;
	}
	
	public List<SelectItem> getItemsCodigo() {
		itemsCodigo=new ArrayList<SelectItem>();
		itemsCodigo.add(new SelectItem("a","Compensacion Unitaria Por VIOLACION DE TENSIONES"));
		itemsCodigo.add(new SelectItem("b","Compensacion Unitaria Por VIOLACION DE FRECUENCIAS"));
		itemsCodigo.add(new SelectItem("c","Compensacion Unitaria Por FLICKER"));
		itemsCodigo.add(new SelectItem("d","Compensacion Unitaria Por ARMONICAS"));
		itemsCodigo.add(new SelectItem("e","Compensacion Unitaria Por INCUMPLIMIENTO DE LA CALIDAD DE SUMINISTRO"));
		return itemsCodigo;
	}

	public void setItemsCodigo(List<SelectItem> itemsCodigo) {
		this.itemsCodigo = itemsCodigo;
	}
	
	
	public List<SelectItem> getItemsTipoParametro() {
		TipoParametroService service=new TipoParametroService();
		itemsTipoParametro=new ArrayList<SelectItem>();
		for(TipoParametro tp:service.getAllRows()){
			itemsTipoParametro.add(new SelectItem(tp,tp.getCodTipParametro()+"-"+tp.getDescripcion()));
		}
		return itemsTipoParametro;
	}

	public void setItemsTipoParametro(List<SelectItem> itemsTipoParametro) {
		this.itemsTipoParametro = itemsTipoParametro;
	}

	
	public List<SelectItem> getItemsTipoParametro1() {
		TipoParametroService service=new TipoParametroService();
		itemsTipoParametro1=new ArrayList<SelectItem>();
		for(TipoParametro tp:service.getAllRows1()){
			itemsTipoParametro1.add(new SelectItem(tp,tp.getCodTipParametro()+"-"+tp.getDescripcion()));
		}
		
		return itemsTipoParametro1;
	}

	public void setItemsTipoParametro1(List<SelectItem> itemsTipoParametro1) {
		this.itemsTipoParametro1 = itemsTipoParametro1;
	}

	public String prepararCrear(){
		//Esto funciona siempre que el bean tenga ambito de sesion 
		setCompensacionUnitaria(new CompensacionUnitaria());
		getCompensacionUnitaria().setEstado("1");
		return "/zonasegura/compensacionUnitariaCrear?faces-redirect=true";
	}
	
	public String prepararEditar(){
		//TODO: Por implementar carga de la instancia
		//System.out.println("compensacionUnitariaId:"+Utilidad.getParametro("compensacionUnitariaId"));
		//cargarCompensacionUnitariaActual();
		//return "/zonasegura/compensacionUnitariaEditar?faces-redirect=true";
		
		try {
			cargarCompensacionUnitariaActual();
			return "/zonasegura/compensacionUnitariaEditar?faces-redirect=true";
		} catch (HibernateException e) {
			
			setMensaje(e.getMessage());
			return "/zonasegura/compensacionUnitariaListar?faces-redirect=true";
		}
	}

	public String prepararVer(){
		//System.out.println("compensacionUnitariaId:"+Utilidad.getParametro("compensacionUnitariaId"));
		//cargarCompensacionUnitariaActual();
		//return "/zonasegura/compensacionUnitariaVer?faces-redirect=true";
		
		try {
			cargarCompensacionUnitariaActual();
			return "/zonasegura/compensacionUnitariaVer?faces-redirect=true";
		} catch (HibernateException e) {
		
			setMensaje(e.getMessage());
			return "/zonasegura/compensacionUnitariaListar?faces-redirect=true";
		}
	}
	
	public String salvar(){
		//CompensacionUnitariaService service=new CompensacionUnitariaService();
		//service.create(compensacionUnitaria);
		//System.out.println("Se salvo un compensacionUnitaria con exito.");
		//return "/zonasegura/compensacionUnitariaListar?faces-redirect=true";
		
		CompensacionUnitariaService service=new CompensacionUnitariaService();
		
		try {
			service.create(compensacionUnitaria);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			return "/zonasegura/compensacionUnitariaCrear?faces-redirect=true";

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/compensacionUnitariaListar?faces-redirect=true";
	}
	
	public String actualizar(){
		//CompensacionUnitariaService service=new CompensacionUnitariaService();
		//service.update(compensacionUnitaria);
		//System.out.println("Se actualizo un compensacionUnitaria con exito.");
		//return "/zonasegura/compensacionUnitariaListar?faces-redirect=true";
		
		CompensacionUnitariaService service=new CompensacionUnitariaService();
		
		try {
			service.update(compensacionUnitaria);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/compensacionUnitariaListar?faces-redirect=true";
	}
	
	public String eliminar(){
		String compensacionUnitariaId=Utilidad.getParametro("compensacionUnitariaId");
	
		CompensacionUnitariaService service=new CompensacionUnitariaService();
		
		//compensacionUnitaria=service.ReadById(Long.parseLong(compensacionUnitariaId)); //new Long(compensacionUnitariaId)
		//compensacionUnitaria.setEstado("0");
		//service.update(compensacionUnitaria);
		//System.out.println("Se elimino una compensacionUnitaria con exito.");
		//return "/zonasegura/compensacionUnitariaListar?faces-redirect=true";
		
		try {
			compensacionUnitaria=service.ReadById(Long.parseLong(compensacionUnitariaId)); //new Long(compensacionUnitariaId)
			compensacionUnitaria.setEstado("0");
			service.update(compensacionUnitaria);
			
		} catch (HibernateException e) {
			
			
			System.out.println(e.getMessage()+":"+e.getCause());
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			
		} catch (Exception e) {
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/compensacionUnitariaListar?faces-redirect=true";
	}
	
	public void cargarCompensacionUnitariaActual(){
		String compensacionUnitariaId=Utilidad.getParametro("compensacionUnitariaId");
		CompensacionUnitariaService service=new CompensacionUnitariaService();
		//compensacionUnitaria=service.ReadById(new Long(compensacionUnitariaId));
		
		try {
			compensacionUnitaria=service.ReadById(new Long(compensacionUnitariaId));
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
