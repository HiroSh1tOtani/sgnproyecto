package pe.com.indra.calidad.mbean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import pe.com.indra.calidad.entity.Rol;
import pe.com.indra.calidad.entity.Usuario;
import pe.com.indra.calidad.service.UsuarioService;
import pe.com.indra.calidad.util.Utilidad;

@SuppressWarnings("serial")
@ManagedBean(name="usuarioBean")
@SessionScoped
public class UsuarioBean implements Serializable{
	
	private Usuario user;
	private Usuario userAutenticado;
	private String username;
	private List<Usuario> usuarios;
	private String rolesAsignados;
	private String manager;
	
	
	public UsuarioBean(){}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	
	@PostConstruct
	public void init() {
		UsuarioService service = new UsuarioService();
		username=new Utilidad().getUsuarioAutenticado();
		user =  new Usuario();
		userAutenticado = service.getUsuarioByUsername(username);
	}

	public Usuario getUser() {
		return user;
	}

	public void setUser(Usuario user) {
		this.user = user;
	}
	
	public String prepareCreate(){
		String forward = "/zonasegura/seguridad/usuarioCrear?faces-redirect=true";
		user = new Usuario();
		user.setStatus("alta");
		return forward;
	}
	
	public String prepareUpdate(Usuario user){
		String forward = "/zonasegura/seguridad/usuarioEditar?faces-redirect=true";
		// TODO
		this.user =  user;
		return forward;
	}
	
	public String prepareView(Usuario user){
		String forward = "/zonasegura/seguridad/usuarioVer?faces-redirect=true";
		//TODO
		this.user =  user;
		return forward;
	}
	
	public String prepareDelete(Usuario user){
		String forward = "/zonasegura/seguridad/usuarioEliminar?faces-redirect=true";
		//TODO
		this.user =  user;
		return forward;
	}
	
	public String prepareListar(){
		String forward = "/zonasegura/seguridad/usuarioListar?faces-redirect=true";
		//TODO
		return forward;
	}
	
	public String create(){
		String forward = "/zonasegura/seguridad/usuarioListar?faces-redirect=true";
		UsuarioService service =  new UsuarioService();
		service.create(getUser());
		return forward;
	}
	
	public String update(){
		String forward = "/zonasegura/seguridad/usuarioListar?faces-redirect=true";
		UsuarioService service =  new UsuarioService();
		Usuario temp = user;
		
		/*
		List<Rol> roles = user.getRols();
		
		user.getRols().clear();
		*/
		
		service.delete(user);
		
		/*
		temp.getRols().clear();
		*/
		
		service.create(temp);
		
		/*
		temp.setRols(roles);
		
		service.update(temp);
		
		*/
		
		return forward;
	}
	
	public String delete(){
		String forward = "/zonasegura/seguridad/usuarioListar?faces-redirect=true";
		UsuarioService service =  new UsuarioService();
		service.delete(user);
		return forward;
	}

	public List<Usuario> getUsuarios() {
		UsuarioService service =  new UsuarioService();
		usuarios = service.getAllUsuarios();
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}
	
	public String getRolesAsignados(Usuario u){
		String rolesAsignados = " "; 
		for(Rol r:u.getRols()){ 
		   rolesAsignados = rolesAsignados + r.getRolename() + ",";
		}
		return rolesAsignados.substring(0,rolesAsignados.length()-1);
	}

	public String getManager() {
		manager = "0";
		String roles = getRolesAsignados(userAutenticado);
		
		if(roles.indexOf("manager") >= 0 ){
			manager = "1";
		}
		
		
		return manager;
	}
	
	public long tienePerfil(String perfil) {
		String roles = getRolesAsignados(userAutenticado);
		long rpta = 0;
		
		if(roles.indexOf(perfil) >= 0 ){
			rpta = 1;
		}
		
		return rpta;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	public Usuario getUserAutenticado() {
		return userAutenticado;
	}

	public void setUserAutenticado(Usuario userAutenticado) {
		this.userAutenticado = userAutenticado;
	}

	
	
}
