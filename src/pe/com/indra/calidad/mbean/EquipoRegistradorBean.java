package pe.com.indra.calidad.mbean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.entity.EquipoRegistrador;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.TipoParametro;
import pe.com.indra.calidad.service.EquipoRegistradorService;
import pe.com.indra.calidad.service.ParametroNormaService;
import pe.com.indra.calidad.util.Sql;
import pe.com.indra.calidad.util.Utilidad;

@ManagedBean(name="equipoRegistradorBean")
@SessionScoped

public class EquipoRegistradorBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<EquipoRegistrador> equipoRegistradores;
	private List<EquipoRegistrador> equipoRegistradores1;
	private EquipoRegistrador equipoRegistrador;
	private List<SelectItem> itemsTipMedicion;
	private List<SelectItem> itemsMarcaMedidor;
	private java.util.Date fecAdquisicion;
	
	private java.util.Date fecCalibracion;
	
    private List<SelectItem> itemsEquipoRegistrador7;
    
    private String mensaje;
    
    private List<SelectItem> itemsNombreColumnas;
    private List<SelectItem> itemsSeparadorColumnas;
    private List<SelectItem> itemsFormatoFecha;
    private String codEquRegistradorFilter;

	@PostConstruct 
	public void init(){
		System.out.println("se ejecuto PostConstruct "+new java.util.Date());
		equipoRegistrador=new EquipoRegistrador();
		equipoRegistrador.setEstado("1");
		/*
		String compensacionUnitariaId=Utilidad.getParametro("compensacionUnitariaId");
		if(compensacionUnitariaId==null){
		  compensacionUnitaria=new CompensacionUnitaria();
		  compensacionUnitaria.setEstado("1");
		}else{
			CompensacionUnitariaService service=new CompensacionUnitariaService();
			compensacionUnitaria=service.ReadById(new Long(compensacionUnitariaId));
		}
		*/
	}
	
	public List<EquipoRegistrador> getEquipoRegistradores() {
		EquipoRegistradorService service=new EquipoRegistradorService();
		equipoRegistradores= service.getAllRows();
		return equipoRegistradores;
	}

	public void setEquipoRegistradores(List<EquipoRegistrador> equipoRegistradores) {
		this.equipoRegistradores = equipoRegistradores;
	}

	public List<SelectItem> getItemsTipMedicion() {
		itemsTipMedicion=new ArrayList<SelectItem>();
		itemsTipMedicion.add(new SelectItem("0","TENSION MONOFASICA"));
		itemsTipMedicion.add(new SelectItem("1","TENSIÓN TRIFASICA"));
		itemsTipMedicion.add(new SelectItem("2","TENSIÓN TRIFASICA + ENERGIA"));
		itemsTipMedicion.add(new SelectItem("3","TENSION + (FLICKER o ARMONICAS) + ENERGIA"));
		itemsTipMedicion.add(new SelectItem("4","TENSION + FLICKER + ARMONICAS + ENERGIA"));
		return itemsTipMedicion;
	}

	public void setItemsTipMedicion(List<SelectItem> itemsTipMedicion) {
		this.itemsTipMedicion = itemsTipMedicion;
	}
	
	
	public List<EquipoRegistrador> getEquipoRegistradores1() {
		EquipoRegistradorService service=new EquipoRegistradorService();
		equipoRegistradores1= service.getAllRows1();
		return equipoRegistradores1;
	}

	public void setEquipoRegistradores1(
			List<EquipoRegistrador> equipoRegistradores1) {
		this.equipoRegistradores1 = equipoRegistradores1;
	}

	public EquipoRegistrador getEquipoRegistrador() {
		
		return equipoRegistrador;
	}

	public void setEquipoRegistrador(EquipoRegistrador equipoRegistrador) {
		this.equipoRegistrador = equipoRegistrador;
	}
	
	
	public List<SelectItem> getItemsNombreColumnas() {
		itemsNombreColumnas=new ArrayList<SelectItem>();
		/*
		itemsNombreColumnas.add(new SelectItem("INTERVALO ,TENSION_R, TENSION_S, TENSION_T","INTERVALO ,TENSION_R, TENSION_S, TENSION_T"));
		itemsNombreColumnas.add(new SelectItem("INTERVALO ,TENSION_R, TENSION_S, TENSION_T, ENERGIA_R, ENERGIA_S, ENERGIA_T","INTERVALO ,TENSION_R, TENSION_S, TENSION_T, ENERGIA_R, ENERGIA_S, ENERGIA_T"));
		itemsNombreColumnas.add(new SelectItem("INTERVALO ,TENSION_R, ENERGIA_R","INTERVALO ,TENSION_R, ENERGIA_R"));
		itemsNombreColumnas.add(new SelectItem("INTERVALO ,TENSION_R","INTERVALO ,TENSION_R"));
		*/
		
		itemsNombreColumnas.add(new SelectItem("1","INTERVALO, TENSION_R, TENSION_S, TENSION_T"));
		itemsNombreColumnas.add(new SelectItem("2","INTERVALO,TENSION_R, TENSION_S, TENSION_T, ENERGIA_R, ENERGIA_S, ENERGIA_T"));
		itemsNombreColumnas.add(new SelectItem("3","INTERVALO,TENSION_R, TENSION_S, TENSION_T, ENERGIA_INTERVALO"));
		itemsNombreColumnas.add(new SelectItem("4","INTERVALO, TENSION_R, ENERGIA_R"));
		itemsNombreColumnas.add(new SelectItem("5","INTERVALO, TENSION_R"));
		
		itemsNombreColumnas.add(new SelectItem("6","INTERVALO, TENSION_R, TENSION_S, TENSION_T, ARMONICAS, FLICKER"));
		itemsNombreColumnas.add(new SelectItem("7","INTERVALO, TENSION_R, TENSION_S, TENSION_T, ENERGIA_R, ENERGIA_S, ENERGIA_T, ARMONICAS, FLICKER"));
		itemsNombreColumnas.add(new SelectItem("8","INTERVALO, TENSION_R, TENSION_S, TENSION_T, ENE_INTERVALO, ARMONICAS, FLICKER"));
		itemsNombreColumnas.add(new SelectItem("9","INTERVALO, TENSION_R, ENERGIA_R, ARMONICAS, FLICKER"));
		itemsNombreColumnas.add(new SelectItem("10","INTERVALO, TENSION_R, ARMONICAS, FLICKER"));
		
		
		return itemsNombreColumnas;
	}
	

	public void setItemsNombreColumnas(List<SelectItem> itemsNombreColumnas) {
		this.itemsNombreColumnas = itemsNombreColumnas;
	}
	
	public List<SelectItem> getItemsSeparadorColumnas() {
		itemsSeparadorColumnas=new ArrayList<SelectItem>();
		itemsSeparadorColumnas.add(new SelectItem("\t","TAB"));
		itemsSeparadorColumnas.add(new SelectItem("","ESPACIO"));
		itemsSeparadorColumnas.add(new SelectItem(",","COMA"));
		itemsSeparadorColumnas.add(new SelectItem("|","BARRA VERTICAL"));
		itemsSeparadorColumnas.add(new SelectItem(";","PUNTO Y COMA"));
		return itemsSeparadorColumnas;
	}

	public void setItemsSeparadorColumnas(List<SelectItem> itemsSeparadorColumnas) {
		this.itemsSeparadorColumnas = itemsSeparadorColumnas;
	}
	
	public List<SelectItem> getItemsFormatoFecha() {
		itemsFormatoFecha=new ArrayList<SelectItem>();
		/*
		itemsFormatoFecha.add(new SelectItem("DD/MM/YYYY HH24:MI","DD/MM/YYYY HH24:MI"));
		itemsFormatoFecha.add(new SelectItem("DD/MM/YYYY HH24:MI:SS","DD/MM/YYYY HH24:MI:SS"));
		itemsFormatoFecha.add(new SelectItem("DD/MM/YYYY HH:MI","DD/MM/YYYY HH:MI"));
		itemsFormatoFecha.add(new SelectItem("DD/MM/YYYY HH:MI:SS","DD/MM/YYYY HH:MI:SS"));
		*/
		
		itemsFormatoFecha.add(new SelectItem("DD/MM/YYYY HH24:MI","DD/MM/YYYY HH24:MI"));
		itemsFormatoFecha.add(new SelectItem("DD/MM/YYYY HH24:MI:SS.FF","DD/MM/YYYY HH24:MI:SS.FF"));
		itemsFormatoFecha.add(new SelectItem("DD/MM/YYYY HH:MI AM","DD/MM/YYYY HH:MI AM"));
		itemsFormatoFecha.add(new SelectItem("DD/MM/YYYY HH:MI:SS.FF AM","DD/MM/YYYY HH:MI:SS.FF AM"));			
		itemsFormatoFecha.add(new SelectItem("DD/MM/YY HH24:MI","DD/MM/YY HH24:MI"));
		itemsFormatoFecha.add(new SelectItem("DD/MM/YY HH24:MI:SS.FF","DD/MM/YY HH24:MI:SS.FF"));
		itemsFormatoFecha.add(new SelectItem("DD/MM/YY HH:MI AM","DD/MM/YY HH:MI AM"));
		itemsFormatoFecha.add(new SelectItem("DD/MM/YY HH:MI:SS.FF AM","DD/MM/YY HH:MI:SS.FF AM"));	
		
		return itemsFormatoFecha;
	}

	public void setItemsFormatoFecha(List<SelectItem> itemsFormatoFecha) {
		this.itemsFormatoFecha = itemsFormatoFecha;
	}
			
	public String prepararCrear(){
		//Esto funciona siempre que el bean tenga ambito de sesion 
		setEquipoRegistrador(new EquipoRegistrador());
		getEquipoRegistrador().setEstado("1");
		return "/zonasegura/equipoRegistradorCrear?faces-redirect=true";
	}
	
	public String prepararEditar(){
		//TODO: Por implementar carga de la instancia
		//System.out.println("equipoRegistradorId:"+Utilidad.getParametro("equipoRegistradorId"));
		//cargarEquipoRegistradorActual();
		//return "/zonasegura/equipoRegistradorEditar?faces-redirect=true";
		
		try {
			cargarEquipoRegistradorActual();
			return "/zonasegura/equipoRegistradorEditar?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/equipoRegistradorListar?faces-redirect=true";
		}
	}

	public String prepararVer(){
		//System.out.println("equipoRegistradorId:"+Utilidad.getParametro("equipoRegistradorId"));
		//cargarEquipoRegistradorActual();
		//return "/zonasegura/equipoRegistradorVer?faces-redirect=true";
		
		try {
			cargarEquipoRegistradorActual();
			return "/zonasegura/equipoRegistradorVer?faces-redirect=true";
		} catch (HibernateException e) {
			// 
			setMensaje(e.getMessage());
			return "/zonasegura/equipoRegistradorListar?faces-redirect=true";
		}
	}
	
	public String salvar(){
		//EquipoRegistradorService service=new EquipoRegistradorService();
		//service.create(equipoRegistrador);
		//System.out.println("Se salvo un equipo Registrador con exito.");
		//return "/zonasegura/equipoRegistradorListar?faces-redirect=true";
		
		EquipoRegistradorService service=new EquipoRegistradorService();
		
		try {
			if(fecAdquisicion!=null){
				equipoRegistrador.setFecAdquisicion(new java.sql.Date(fecAdquisicion.getTime()));
			}else {
				equipoRegistrador.setFecAdquisicion(null);
			}
			
			if(fecCalibracion!=null){
				equipoRegistrador.setFecCalibracion(new java.sql.Date(fecCalibracion.getTime()));
			}else {
				equipoRegistrador.setFecCalibracion(null);
			}
			
			service.create(equipoRegistrador);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			e.printStackTrace();
			
			return "/zonasegura/equipoRegistradorCrear?faces-redirect=true";

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			e.printStackTrace();
			
			return "/zonasegura/equipoRegistradorCrear?faces-redirect=true";
		}
		return "/zonasegura/equipoRegistradorListar?faces-redirect=true";
	}
	
	public String actualizar(){
		//EquipoRegistradorService service=new EquipoRegistradorService();
		//service.update(equipoRegistrador);
		//System.out.println("Se actualizo un equipo Registrador con exito.");
		//return "/zonasegura/equipoRegistradorListar?faces-redirect=true";
		
		EquipoRegistradorService service=new EquipoRegistradorService();
		
		try {
			if(fecAdquisicion!=null){
				equipoRegistrador.setFecAdquisicion(new java.sql.Date(fecAdquisicion.getTime()));
			}else {
				equipoRegistrador.setFecAdquisicion(null);
			}
			
			if(fecCalibracion!=null){
				equipoRegistrador.setFecCalibracion(new java.sql.Date(fecCalibracion.getTime()));
			}else {
				equipoRegistrador.setFecCalibracion(null);
			}
			
			service.update(equipoRegistrador);
		}  catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/equipoRegistradorListar?faces-redirect=true";
	}
	
		
	public String eliminar(){
		String equipoRegistradorId=Utilidad.getParametro("equipoRegistradorId");
		EquipoRegistradorService service=new EquipoRegistradorService();
		//equipoRegistrador=service.ReadById(new Long(equipoRegistradorId));
		//equipoRegistrador.setEstado("0");
		//service.delete(equipoRegistrador.getEquRegistradorId());
		//System.out.println("Se elimino un equipoRegistrador con exito.");
		//return "/zonasegura/equipoRegistradorListar?faces-redirect=true";
		
		try {
			equipoRegistrador=service.ReadById(new Long(equipoRegistradorId));
			equipoRegistrador.setEstado("0");
			  //service.delete(equipoRegistrador.getEquRegistradorId());
			  service.update(equipoRegistrador);
		}catch (HibernateException e) {
			
			
			System.out.println(e.getMessage()+":"+e.getCause());
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			
		} catch (Exception e) {
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		
		return "/zonasegura/equipoRegistradorListar?faces-redirect=true";
	}
	
	public void cargarEquipoRegistradorActual(){
		String equipoRegistradorId=Utilidad.getParametro("equipoRegistradorId");
		EquipoRegistradorService service=new EquipoRegistradorService();
		//equipoRegistrador=service.ReadById(new Long(equipoRegistradorId));
		
		try {
			equipoRegistrador=service.ReadById(new Long(equipoRegistradorId));
			
			if(equipoRegistrador.getFecAdquisicion()!=null) {
				fecAdquisicion = new java.util.Date( equipoRegistrador.getFecAdquisicion().getTime());				
			}else {
				fecAdquisicion = null;
			}
			
			if(equipoRegistrador.getFecCalibracion()!=null){
				fecCalibracion = new java.util.Date(equipoRegistrador.getFecCalibracion().getTime());
			}else {
				fecCalibracion = null;
			}
			
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

			throw new HibernateException("No se puede cargar EquipoRegistrador.", e);
			
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
	}
	
				
		public List<SelectItem> getItemsEquipoRegistrador7() {
			EquipoRegistradorService service=new EquipoRegistradorService();
			itemsEquipoRegistrador7=new ArrayList<SelectItem>();
			for(EquipoRegistrador tp:service.getAllRows()){
				itemsEquipoRegistrador7.add(new SelectItem(tp,tp.getNombreColumnas()));
			}
			return itemsEquipoRegistrador7;
		}
	
		public void setItemsEquipoRegistrador7(List<SelectItem> itemsEquipoRegistrador7) {
			this.itemsEquipoRegistrador7 = itemsEquipoRegistrador7;
		}

		public String getMensaje() {
			return mensaje;
		}

		public void setMensaje(String mensaje) {
			this.mensaje = mensaje;
		}

		public List<SelectItem> getItemsMarcaMedidor() {
			
			Sql sql = new Sql("CALIDAD"); 
			String selectSql = "SELECT COD_MARCA_MEDIDOR , NOMBRE FROM CAL_MARCA_MEDIDOR ORDER BY COD_MARCA_MEDIDOR" ;
			List<Object[]> listObject = sql.consulta(selectSql, false);
			
			itemsMarcaMedidor=new ArrayList<SelectItem>();
			for (Object[] objects: listObject) {
				 
				  
				  itemsMarcaMedidor.add(new SelectItem((String) objects[1],(String) objects[1]));
			    
			}
			
			return itemsMarcaMedidor;
		}

		public void setItemsMarcaMedidor(List<SelectItem> itemsMarcaMedidor) {
			this.itemsMarcaMedidor = itemsMarcaMedidor;
		}
		
		public java.util.Date getFecAdquisicion() {
			return fecAdquisicion;
		}

		public void setFecAdquisicion(java.util.Date fecAdquisicion) {
			this.fecAdquisicion = fecAdquisicion;
		}

		public java.util.Date getFecCalibracion() {
			return fecCalibracion;
		}

		public void setFecCalibracion(java.util.Date fecCalibracion) {
			this.fecCalibracion = fecCalibracion;
		}

		public String getCodEquRegistradorFilter() {
			return codEquRegistradorFilter;
		}

		public void setCodEquRegistradorFilter(String codEquRegistradorFilter) {
			this.codEquRegistradorFilter = codEquRegistradorFilter;
		}

}
