package pe.com.indra.calidad.mbean;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.SQLException;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.sql.rowset.serial.SerialException;

import pe.com.indra.calidad.entity.TipoAlimentacion;
import pe.com.indra.calidad.service.TipoAlimentacionService;

@FacesConverter(value="clobConverter",forClass=ClobConverter.class)

public class ClobConverter implements Converter{

	@Override
	public Clob getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		
		try {
			return new javax.sql.rowset.serial.SerialClob(cadena.toCharArray());
		} catch (SerialException e) {
			// TODO Auto-generated catch block
			return null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			return null;
		}
		
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			             
			StringBuffer str = new StringBuffer();
			String strng;
			Clob clb = (Clob) objeto;
			           			 
			BufferedReader bufferRead = null;
			try {
				bufferRead = new BufferedReader(clb.getCharacterStream());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				while ((strng=bufferRead .readLine())!=null)
				    str.append(strng);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return str.toString();
		}
		return null;
	}


}
