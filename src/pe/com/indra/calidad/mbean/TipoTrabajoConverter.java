package pe.com.indra.calidad.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.indra.calidad.entity.TipoTrabajo;
import pe.com.indra.calidad.service.TipoTrabajoService;

@FacesConverter(value="tipoTrabajoConverter",forClass=TipoTrabajoConverter.class)

public class TipoTrabajoConverter implements Converter{

	@Override
	public TipoTrabajo getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long tipTrabajoId=new Long(cadena);
		TipoTrabajoService service=new TipoTrabajoService();
		TipoTrabajo tipoTrabajo=new TipoTrabajo();
		tipoTrabajo=service.ReadById(tipTrabajoId);
		System.out.println(tipoTrabajo.getCodTipTrabajo()+":"+tipoTrabajo.getDescripcion());
		return tipoTrabajo;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			TipoTrabajo tipoTrabajo=(TipoTrabajo) objeto;
			//System.out.println("objeto:"+new Long(tipoTrabajo.getTipTrabajoId()).toString());
			return new Long(tipoTrabajo.getTipTrabajoId()).toString();
		}
		return null;
	}


}

