package pe.com.indra.calidad.mbean;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@ManagedBean(name="sessionBean")
@SessionScoped
public class SessionBean implements Serializable {

	/**
	 * Clase para manejar el fin de la sesion
	 */
	private static final long serialVersionUID = -5276681963780493387L;

	public SessionBean() {
    }
	
	public String logout() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        HttpSession session = (HttpSession) externalContext.getSession(false);
        session.invalidate();
        
        /*
     // create new session
        ((HttpServletRequest) externalContext.getRequest()).getSession(true);

        // restore last used user settings because login / logout pages reference "userSettings"
        FacesAccessor.setValue2ValueExpression(userSettings, "#{userSettings}");

        // redirect to the specified logout page
        externalContext.redirect(externalContext.getRequestContextPath() + "/views/logout.jsf");
       */
        
        return "/zonasegura/default?faces-redirect=true";
        //return "default?faces-redirect=false";
    }
	
}
