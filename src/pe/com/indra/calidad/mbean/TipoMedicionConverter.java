package pe.com.indra.calidad.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.indra.calidad.entity.TipoMedicion;
import pe.com.indra.calidad.service.TipoMedicionService;

@FacesConverter(value="tipoMedicionConverter",forClass=TipoMedicionConverter.class)

public class TipoMedicionConverter implements Converter{

	@Override
	public TipoMedicion getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long tipMedicionId=new Long(cadena);
		TipoMedicionService service=new TipoMedicionService();
		TipoMedicion tipoMedicion=new TipoMedicion();
		tipoMedicion=service.ReadById(tipMedicionId);
		System.out.println(tipoMedicion.getCodTipMedicion()+":"+tipoMedicion.getDescripcion());
		return tipoMedicion;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			TipoMedicion tipoMedicion=(TipoMedicion) objeto;
			//System.out.println("objeto:"+new Long(tipoMedicion.getTipMedicionId()).toString());
			return new Long(tipoMedicion.getTipMedicionId()).toString();
		}
		return null;
	}


}
