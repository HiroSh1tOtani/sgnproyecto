package pe.com.indra.calidad.mbean;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRTextExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.HibernateException;
import org.richfaces.component.UIExtendedDataTable;
import org.richfaces.model.Filter;
import pe.com.indra.calidad.entity.*;
import pe.com.indra.calidad.service.*;
import pe.com.indra.calidad.util.ConectaDb;
import pe.com.indra.calidad.util.ModificarExcel;
import pe.com.indra.calidad.util.Sql;
import pe.com.indra.calidad.util.Utilidad;

@ManagedBean(name="medicionBean")
@SessionScoped
public class MedicionBean  implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<Medicion> mediciones;
	private List<Medicion> medicionesxSuministro;
	private List<Medicion> medicionesPendientesComp;
	private Medicion medicion;
	private List<SelectItem> itemsTipoMedicion;
	private List<SelectItem> itemsTipoPunto;
	private List<SelectItem> itemsLocalidad;
	private List<SelectItem> itemsTipoAlimentacion;
	private List<SelectItem> itemsTipoTrabajo;
	private List<SelectItem> itemsEquipoRegistrador;
	private List<SelectItem> itemsParametroMedido;
	private List<SelectItem> itemsEstadoMedicion;
	private List<SelectItem> itemsCampanaMedicion;
	
	private List<SelectItem> itemsTipoMedicion1;
	private List<SelectItem> itemsTipoPunto1;
	private List<SelectItem> itemsLocalidad1;
	private List<SelectItem> itemsTipoAlimentacion1;
	private List<SelectItem> itemsTipoTrabajo1;
	private List<SelectItem> itemsEquipoRegistrador1;
	private List<SelectItem> itemsParametroMedido1;
	private List<SelectItem> itemsEstadoMedicion1;
	private List<SelectItem> itemsCampanaMedicion1;
	
	private List<SelectItem> itemsPerEstadoMedicion;
	private List<SelectItem> itemsPerEstadoMedicion1;

	private String mensaje;
	private java.util.Date fecInstalacion;
	private java.util.Date fecRetiro;
	private String numSumMedicion;
	private CampanaMedicion campanaMedicion;
	private String tipoPunto;
	public List<Medicion> getMedicionesPendientesComp() {
		return medicionesPendientesComp;
	}

	public void setMedicionesPendientesComp(List<Medicion> medicionesPendientesComp) {
		this.medicionesPendientesComp = medicionesPendientesComp;
	}

	private String tipServicio;
	private String nisFilter;
	private String nomUsuarioFilter;
	
	private List<SelectItem> itemsTipoPuntos;
	private List<SelectItem> itemsTipoServicios;
	private List<SelectItem> itemsTipoSuministro;
	private List<SelectItem> itemsUbicacionSuministro;
	private List<SelectItem> itemsParametroPerturbacion;
	

	private List<SelectItem> itemsParametroMedido2;
	private String parametroMedidoFilter;
	private String tipoSuministroFilter;
	private List<SelectItem> itemsTipoSuministro2;
	
	private String localidadFilter;
	private List<SelectItem> itemsLocalidad2;
	private String formato;
	private List<SelectItem> itemsFormato;
	private long tipo;
	private List<SelectItem> itemsTipo;
	
	
	private long tipoPuntoReporte;
	private List<SelectItem> itemsTipoPuntoReporte;
	
	private List<SelectItem> itemsPreFlicker;
	private List<SelectItem> itemsPreArmonicas;
	
	private String tipoPuntoFilter;
	private String tipoServicioFilter;
	private String sedFilter;
	private String ubiSumMedicionFilter;
	private List<SelectItem> itemsUbiSumMedicion;
	
	private long filasTotal;
	private long filasVisibles;

	private Collection<Object> selection;
	private List<Medicion> selectionItems = new ArrayList<Medicion>();
	private List<SelectItem> itemsTipoEnergia;
	private List<SelectItem> itemsTipoMedicionEnergia;
	private String numSumFilter;
	private String numSum1Filter;
	
	public Collection<Object> getSelection() {
		return selection;
	}

	public void setSelection(Collection<Object> selection) {
		this.selection = selection;
	}

	public List<Medicion> getSelectionItems() {
		return selectionItems;
	}

	public void setSelectionItems(List<Medicion> selectionItems) {
		this.selectionItems = selectionItems;
	}	
	
	public MedicionBean(){
		medicion=new Medicion();
		medicion.setEstado("1");
		
	}
	
		
    public long getFilasTotal() {
		return filasTotal;
	}


	public void setFilasTotal(long filasTotal) {
		this.filasTotal = filasTotal;
	}


	public long getFilasVisibles() {
		return filasVisibles;
	}


	public void setFilasVisibles(long filasVisibles) {
		this.filasVisibles = filasVisibles;
	}

	public String numeroFilas(){
		UIExtendedDataTable dataTable = (UIExtendedDataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("idform-tableMedicion");
				
		filasTotal = dataTable.getRowCount();
		
		
		
		System.out.println(dataTable.getRowCount() + "-" + dataTable.getClientRows() + "-" + dataTable.getChildCount() + "-" + dataTable.getRows() + "-" + dataTable.getResourceBundleMap().size());

		return null;
	}
	
	public void handleEvent(ComponentSystemEvent event){
		UIExtendedDataTable dataTable = (UIExtendedDataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("idform-tableMedicion");
		
		filasTotal = dataTable.getRowCount();		
		
		System.out.println(dataTable.getRowCount() + "-" + dataTable.getClientRows() + "-" + dataTable.getChildCount() + "-" + dataTable.getRows() + "-" + dataTable.getResourceBundleMap().size());

	}
	
	public void actualizaFilas(AjaxBehaviorEvent event){
		//UIExtendedDataTable dataTable = (UIExtendedDataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("idform-tableMedicion");
		UIExtendedDataTable dataTable = (UIExtendedDataTable) event.getComponent().getParent().getParent().getParent();
		filasTotal = dataTable.getRowCount();		
		
		System.out.println(dataTable.getRowCount() + "-" + dataTable.getClientRows() + "-" + dataTable.getChildCount() + "-" + dataTable.getRows() + "-" + dataTable.getResourceBundleMap().size());

	}

	
	public void actualizaFilas2(AjaxBehaviorEvent event){
		//UIExtendedDataTable dataTable = (UIExtendedDataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("idform-tableMedicion");
		UIExtendedDataTable dataTable = (UIExtendedDataTable) event.getComponent();
		filasTotal = dataTable.getRowCount();
		
		Object originalKey = dataTable.getRowKey();
        selectionItems.clear();
        for (Object selectionKey : selection) {
            dataTable.setRowKey(selectionKey);
            if (dataTable.isRowAvailable()) {
                selectionItems.add((Medicion) dataTable.getRowData());
            }
        }
        dataTable.setRowKey(originalKey);		
		
		System.out.println(dataTable.getRowCount() + "-" + dataTable.getClientRows() + "-" + dataTable.getChildCount() + "-" + dataTable.getRows() + "-" + dataTable.getResourceBundleMap().size());

	}
	
	public void addError(ActionEvent actionEvent) {  //javax.faces.event.AjaxBehaviorEvent
    	
        FacesContext.getCurrentInstance().addMessage( actionEvent.getComponent().getClientId(), new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error en la Transacción", getMensaje()));
        //FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
    }	
	
	public String actionBuscar(){
		// Aqui responder a buscar en IGEA
		System.out.println("Numero de suministro:"+medicion.getMedicionId());
		
		
		return "campanha";
	}
	
	
	@PostConstruct 
	public void init(){
		System.out.println("se ejecuto PostConstruct "+new java.util.Date());
		medicion=new Medicion();
		medicion.setEstado("1");
		tipoPunto = "N";
		tipServicio = "N";
		formato = "PDF";
		tipoPuntoReporte = 1;
		tipo = 1;
		filasTotal = 0;
		/*
		String medicionId=Utilidad.getParametro("medicionId");
		if(medicionId==null){
		  medicion=new Medicion();
		  medicion.setEstado("1");
		}else{
			MedicionService service=new MedicionService();
			medicion=service.ReadById(new Long(medicionId));
		}
		*/
	}
	
	public List<Medicion> getMediciones() {
			
		//MedicionesxCampana();
		//MedicionService service=new MedicionService();
		
		//mediciones= service.getAllRows();
		
		return mediciones;
	}
	
	public void setMediciones(List<Medicion> mediciones) {
		this.mediciones = mediciones;
	}

	
	public List<SelectItem> getItemsTipoPuntos() {
		TipoPuntoService service = new TipoPuntoService();
		itemsTipoPuntos=new ArrayList<SelectItem>();
		itemsTipoPuntos.add(new SelectItem("", ""));
		for(TipoPunto tp:service.getAllRows()){
			itemsTipoPuntos.add(new SelectItem(tp.getCodTipPunto(),tp.getDescripcion()));
		}
		return itemsTipoPuntos;
	}
	

	public void setItemsTipoPuntos(List<SelectItem> itemsTipoPuntos) {
		this.itemsTipoPuntos = itemsTipoPuntos;
	}
	
	public List<SelectItem> getItemsTipoServicios() {
		itemsTipoServicios=new ArrayList<SelectItem>();
		itemsTipoServicios.add(new SelectItem("",""));
		itemsTipoServicios.add(new SelectItem("U","URBANO"));
		itemsTipoServicios.add(new SelectItem("R","RURAL"));
		itemsTipoServicios.add(new SelectItem("UR","URBANO-RURAL"));
		return itemsTipoServicios;
	}


	public void setItemsTipoServicios(List<SelectItem> itemsTipoServicios) {
		this.itemsTipoServicios = itemsTipoServicios;
	}
	
	public List<SelectItem> getItemsTipoSuministro() {
		itemsTipoSuministro=new ArrayList<SelectItem>();
		itemsTipoSuministro.add(new SelectItem("BT","BAJA TENSION"));
		itemsTipoSuministro.add(new SelectItem("SED","SUBESTACION"));
		itemsTipoSuministro.add(new SelectItem("MT","MEDIA TENSION"));
		return itemsTipoSuministro;
	}


	public void setItemsTipoSuministro(List<SelectItem> itemsTipoSuministro) {
		this.itemsTipoSuministro = itemsTipoSuministro;
	}
	
	public List<SelectItem> getItemsUbicacionSuministro() {
		itemsUbicacionSuministro=new ArrayList<SelectItem>();
		itemsUbicacionSuministro.add(new SelectItem("N","NO CORRESPONDE"));
		itemsUbicacionSuministro.add(new SelectItem("I","TRAMO INICIAL DE LA SED MT/BT"));
		itemsUbicacionSuministro.add(new SelectItem("F","TRAMO FINAL DE LA SED MT/BT"));
		return itemsUbicacionSuministro;
	}


	public void setItemsUbicacionSuministro(
			List<SelectItem> itemsUbicacionSuministro) {
		this.itemsUbicacionSuministro = itemsUbicacionSuministro;
	}
	
	public List<SelectItem> getItemsParametroPerturbacion() {
		itemsParametroPerturbacion=new ArrayList<SelectItem>();
		itemsParametroPerturbacion.add(new SelectItem("N","NO CORRESPONDE"));
		itemsParametroPerturbacion.add(new SelectItem("F","FLICKER"));
		itemsParametroPerturbacion.add(new SelectItem("A","ARMONICAS"));
		itemsParametroPerturbacion.add(new SelectItem("FA","FLICKER Y ARMONICAS"));
		return itemsParametroPerturbacion;
	}


	public void setItemsParametroPerturbacion(
			List<SelectItem> itemsParametroPerturbacion) {
		this.itemsParametroPerturbacion = itemsParametroPerturbacion;
	}

	
	public String MedicionesxCampana() {
		MedicionService service=new MedicionService();
		String result;
		
		System.out.println("ANTES!!!");
		
		if(campanaMedicion.getCamMedicionId() > 0) {
			
			tipServicio = "N";
			tipoPunto = "N";
			
			nisFilter = "";
			nomUsuarioFilter = "";
			tipoSuministroFilter = "";
			parametroMedidoFilter = "";
			localidadFilter = "";
			tipoPuntoFilter = "";
			tipoServicioFilter = "";
			sedFilter  = "";
			ubiSumMedicionFilter = "";
			
			System.out.println("CAMPANA_ID:" + campanaMedicion.getCamMedicionId());
			mediciones= service.getAllRows1(campanaMedicion.getCamMedicionId());
			
		}
		
		
		return result = "/zonasegura/medicionListar?faces-redirect=true";
	}
	
	public String MedicionesxCampanaFiltro() {
		MedicionService service=new MedicionService();
		String result;
		
		System.out.println("ANTES!!!");
		
		if(campanaMedicion.getCamMedicionId() > 0) {
			System.out.println("CAMPANA_ID:" + campanaMedicion.getCamMedicionId());
			mediciones= service.getAllRows1(campanaMedicion.getCamMedicionId(),tipServicio, tipoPunto);
			
		}
		
		
		return result = "/zonasegura/medicionListar?faces-redirect=true";
	}
	
	public String MedicionesxSuministro() {
		MedicionService service=new MedicionService();
		
		try {
			
			if(!numSumMedicion.trim().equals("")) {
				medicionesxSuministro= service.getMedicionxSuministro(numSumMedicion);		
			} else {
				medicionesxSuministro = null;
			}
				
			
		} catch (HibernateException e) {
			setMensaje(e.getMessage());
		}
		
		
		return "/zonasegura/medicionxSuministroListar?faces-redirect=true";
	}
	
	public String medicionesPendientes() {
		MedicionService service=new MedicionService();
		
		long id;
		String result;
		String campoString;
		String selectSql = null;
		Sql sql = new Sql("CALIDAD");
		BigDecimal campoBD;
		Medicion medicionTemp;

		List<Object[]> objTabla;
		
		ConectaDb db = new ConectaDb("CALIDAD");
		Connection cn = db.getConnection();
        CallableStatement cstmt = null;
        

        try {
        	cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.AGREGAR_NO_PROGRAMADOS_TEMP(?)}");
		   	cstmt.setLong(1, campanaMedicion.getCamMedicionId());
			
            int ctos = cstmt.executeUpdate();;
            cstmt.close(); 

            if (ctos == 0) {
                result = "0 filas afectadas";
            }

        } catch (SQLException e) {
            result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
            
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
                result = e.getMessage();
                
    			setMensaje(e.getMessage()+":"+e.getCause());
    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                
            }
        }		
		
		selectSql = "SELECT PK_MEDICION_ID FROM CAL_PENDIENTE_COM_TEMP";
		
		objTabla = sql.consulta(selectSql, false);
		
		medicionesPendientesComp = new ArrayList<Medicion>();
		//medicionesPendientesComp.clear();
		
		for (Object[] objFila : objTabla) {
			campoBD = (BigDecimal) objFila[0];
			id = campoBD.longValue();
			
			medicionTemp = service.ReadById(id);
			
			medicionesPendientesComp.add(medicionTemp);
			
		}
		
		
		return "/zonasegura/medicionPendienteListar?faces-redirect=true";
	}	
	
	public Medicion getMedicion() {
		
		return medicion;
	}

	public void setMedicion(Medicion medicion) {
		this.medicion = medicion;
	}
	
		
	public List<SelectItem> getItemsTipoMedicion() {
		TipoMedicionService service=new TipoMedicionService();
		itemsTipoMedicion=new ArrayList<SelectItem>();
		for(TipoMedicion tp:service.getAllRows()){
			itemsTipoMedicion.add(new SelectItem(tp,tp.getCodTipMedicion()+"-"+tp.getDescripcion()));
		}
		return itemsTipoMedicion;
	}

	public void setItemsTipoMedicion(List<SelectItem> itemsTipoMedicion) {
		this.itemsTipoMedicion = itemsTipoMedicion;
	}

	public List<SelectItem> getItemsTipoPunto() {
		TipoPuntoService service=new TipoPuntoService();
		itemsTipoPunto=new ArrayList<SelectItem>();
		for(TipoPunto tp:service.getAllRows()){
			itemsTipoPunto.add(new SelectItem(tp,tp.getCodTipPunto()+"-"+tp.getDescripcion()));
		}
		
		return itemsTipoPunto;
	}


	public void setItemsTipoPunto(List<SelectItem> itemsTipoPunto) {
		this.itemsTipoPunto = itemsTipoPunto;
	}


	public List<SelectItem> getItemsLocalidad() {
		LocalidadService service=new LocalidadService();
		itemsLocalidad=new ArrayList<SelectItem>();
		for(Localidad tp:service.getAllRows()){
			itemsLocalidad.add(new SelectItem(tp,tp.getCodLocalidad()+"-"+tp.getNombre()));
		}
		
		
		return itemsLocalidad;
	}


	public void setItemsLocalidad(List<SelectItem> itemsLocalidad) {
		this.itemsLocalidad = itemsLocalidad;
	}


	public List<SelectItem> getItemsTipoAlimentacion() {
		TipoAlimentacionService service=new TipoAlimentacionService();
		itemsTipoAlimentacion=new ArrayList<SelectItem>();
		for(TipoAlimentacion tp:service.getAllRows()){
			itemsTipoAlimentacion.add(new SelectItem(tp,tp.getCodTipAlimentacion()+"-"+tp.getDescripcion()));
		}
		
		return itemsTipoAlimentacion;
	}


	public void setItemsTipoAlimentacion(List<SelectItem> itemsTipoAlimentacion) {
		this.itemsTipoAlimentacion = itemsTipoAlimentacion;
	}


	public List<SelectItem> getItemsTipoTrabajo() {
		TipoTrabajoService service=new TipoTrabajoService();
		itemsTipoTrabajo=new ArrayList<SelectItem>();
		for(TipoTrabajo tp:service.getAllRows()){
			itemsTipoTrabajo.add(new SelectItem(tp,tp.getCodTipTrabajo()+"-"+tp.getDescripcion()));
		}
		
		return itemsTipoTrabajo;
	}


	public void setItemsTipoTrabajo(List<SelectItem> itemsTipoTrabajo) {
		this.itemsTipoTrabajo = itemsTipoTrabajo;
	}


	public List<SelectItem> getItemsEquipoRegistrador() {
		EquipoRegistradorService service=new EquipoRegistradorService();
		itemsEquipoRegistrador=new ArrayList<SelectItem>();
		for(EquipoRegistrador tp:service.getAllRows()){
			itemsEquipoRegistrador.add(new SelectItem(tp,tp.getCodEquRegistrador()+"-"+tp.getDescripcion()));
		}
		
		return itemsEquipoRegistrador;
	}


	public void setItemsEquipoRegistrador(List<SelectItem> itemsEquipoRegistrador) {
		this.itemsEquipoRegistrador = itemsEquipoRegistrador;
	}


	public List<SelectItem> getItemsParametroMedido() {
		ParametroMedidoService service=new ParametroMedidoService();
		itemsParametroMedido=new ArrayList<SelectItem>();
		for(ParametroMedido tp:service.getAllRows()){
			itemsParametroMedido.add(new SelectItem(tp,tp.getCodParMedido()+"-"+tp.getDescripcion()));
		}
		
		return itemsParametroMedido;
	}


	public void setItemsParametroMedido(List<SelectItem> itemsParametroMedido) {
		this.itemsParametroMedido = itemsParametroMedido;
	}


	public List<SelectItem> getItemsEstadoMedicion() {
		EstadoMedicionService service=new EstadoMedicionService();
		itemsEstadoMedicion=new ArrayList<SelectItem>();
		for(EstadoMedicion tp:service.getAllRows()){
			itemsEstadoMedicion.add(new SelectItem(tp,tp.getCodEstMedicion()+"-"+tp.getDescripcion()));
		}
				
		return itemsEstadoMedicion;
	}


	public void setItemsEstadoMedicion(List<SelectItem> itemsEstadoMedicion) {
		this.itemsEstadoMedicion = itemsEstadoMedicion;
	}
	
	public List<SelectItem> getItemsPerEstadoMedicion() {
		EstadoMedicionService service=new EstadoMedicionService();
		itemsPerEstadoMedicion=new ArrayList<SelectItem>();
		for(EstadoMedicion tp:service.getAllRows()){
			itemsPerEstadoMedicion.add(new SelectItem(tp,tp.getCodEstMedicion()+"-"+tp.getDescripcion()));
		}
		return itemsPerEstadoMedicion;
	}


	public void setItemsPerEstadoMedicion(List<SelectItem> itemsPerEstadoMedicion) {
		this.itemsPerEstadoMedicion = itemsPerEstadoMedicion;
	}


	public List<SelectItem> getItemsCampanaMedicion() {
		CampanaMedicionService service=new CampanaMedicionService();
		itemsCampanaMedicion=new ArrayList<SelectItem>();
		for(CampanaMedicion tp:service.getAllRows()){
			itemsCampanaMedicion.add(new SelectItem(tp,tp.getDescripcion() + " (" + tp.getPeriodoMedicion().getAno() + "-" + tp.getPeriodoMedicion().getPeriodo() + ")"));
		}
		
		return itemsCampanaMedicion;
	}


	public void setItemsCampanaMedicion(List<SelectItem> itemsCamapanaMedicion) {
		this.itemsCampanaMedicion = itemsCamapanaMedicion;
	}	

	public List<SelectItem> getItemsTipoMedicion1() {
		TipoMedicionService service=new TipoMedicionService();
		itemsTipoMedicion1=new ArrayList<SelectItem>();
		for(TipoMedicion tp:service.getAllRows1()){
			itemsTipoMedicion1.add(new SelectItem(tp,tp.getCodTipMedicion()+"-"+tp.getDescripcion()));
		}

		return itemsTipoMedicion1;
	}


	public void setItemsTipoMedicion1(List<SelectItem> itemsTipoMedicion1) {
		this.itemsTipoMedicion1 = itemsTipoMedicion1;
	}


	public List<SelectItem> getItemsTipoPunto1() {
		TipoPuntoService service=new TipoPuntoService();
		itemsTipoPunto1=new ArrayList<SelectItem>();
		for(TipoPunto tp:service.getAllRows1()){
			itemsTipoPunto1.add(new SelectItem(tp,tp.getCodTipPunto()+"-"+tp.getDescripcion()));
		}
		
		return itemsTipoPunto1;
	}


	public void setItemsTipoPunto1(List<SelectItem> itemsTipoPunto1) {
		this.itemsTipoPunto1 = itemsTipoPunto1;
	}


	public List<SelectItem> getItemsLocalidad1() {
		LocalidadService service=new LocalidadService();
		itemsLocalidad1=new ArrayList<SelectItem>();
		for(Localidad tp:service.getAllRows1()){
			itemsLocalidad1.add(new SelectItem(tp,tp.getCodLocalidad()+"-"+tp.getNombre()));
		}
		
		return itemsLocalidad1;
	}


	public void setItemsLocalidad1(List<SelectItem> itemsLocalidad1) {
		this.itemsLocalidad1 = itemsLocalidad1;
	}


	public List<SelectItem> getItemsTipoAlimentacion1() {
		TipoAlimentacionService service=new TipoAlimentacionService();
		itemsTipoAlimentacion1=new ArrayList<SelectItem>();
		for(TipoAlimentacion tp:service.getAllRows1()){
			itemsTipoAlimentacion1.add(new SelectItem(tp,tp.getCodTipAlimentacion()+"-"+tp.getDescripcion()));
		}
		
		return itemsTipoAlimentacion1;
	}


	public void setItemsTipoAlimentacion1(List<SelectItem> itemsTipoAlimentacion1) {
		this.itemsTipoAlimentacion1 = itemsTipoAlimentacion1;
	}


	public List<SelectItem> getItemsTipoTrabajo1() {
		TipoTrabajoService service=new TipoTrabajoService();
		itemsTipoTrabajo1=new ArrayList<SelectItem>();
		for(TipoTrabajo tp:service.getAllRows1()){
			itemsTipoTrabajo1.add(new SelectItem(tp,tp.getCodTipTrabajo()+"-"+tp.getDescripcion()));
		}
		
		return itemsTipoTrabajo1;
	}


	public void setItemsTipoTrabajo1(List<SelectItem> itemsTipoTrabajo1) {
		this.itemsTipoTrabajo1 = itemsTipoTrabajo1;
	}


	public List<SelectItem> getItemsEquipoRegistrador1() {
		EquipoRegistradorService service=new EquipoRegistradorService();
		itemsEquipoRegistrador1=new ArrayList<SelectItem>();
		for(EquipoRegistrador tp:service.getAllRows1()){
			itemsEquipoRegistrador1.add(new SelectItem(tp,tp.getCodEquRegistrador()+"-"+tp.getDescripcion()));
		}
		
		return itemsEquipoRegistrador1;
	}


	public void setItemsEquipoRegistrador1(List<SelectItem> itemsEquipoRegistrador1) {
		this.itemsEquipoRegistrador1 = itemsEquipoRegistrador1;
	}


	public List<SelectItem> getItemsParametroMedido1() {
		ParametroMedidoService service=new ParametroMedidoService();
		itemsParametroMedido1=new ArrayList<SelectItem>();
		for(ParametroMedido tp:service.getAllRows1()){
			itemsParametroMedido1.add(new SelectItem(tp,tp.getCodParMedido()+"-"+tp.getDescripcion()));
		}
		
		
		return itemsParametroMedido1;
	}


	public void setItemsParametroMedido1(List<SelectItem> itemsParametroMedido1) {
		this.itemsParametroMedido1 = itemsParametroMedido1;
	}


	public List<SelectItem> getItemsEstadoMedicion1() {
		EstadoMedicionService service=new EstadoMedicionService();
		itemsEstadoMedicion1=new ArrayList<SelectItem>();
		for(EstadoMedicion tp:service.getAllRows1()){
			itemsEstadoMedicion1.add(new SelectItem(tp,tp.getCodEstMedicion()+"-"+tp.getDescripcion()));
		}
		
		return itemsEstadoMedicion1;
	}
	
	public List<SelectItem> getItemsPerEstadoMedicion1() {
		EstadoMedicionService service=new EstadoMedicionService();
		itemsPerEstadoMedicion1=new ArrayList<SelectItem>();
		for(EstadoMedicion tp:service.getAllRows1()){
			itemsPerEstadoMedicion1.add(new SelectItem(tp,tp.getCodEstMedicion()+"-"+tp.getDescripcion()));
		}
		return itemsPerEstadoMedicion1;
	}


	public void setItemsPerEstadoMedicion1(List<SelectItem> itemsPerEstadoMedicion1) {
		this.itemsPerEstadoMedicion1 = itemsPerEstadoMedicion1;
	}


	public void setItemsEstadoMedicion1(List<SelectItem> itemsEstadoMedicion1) {
		this.itemsEstadoMedicion1 = itemsEstadoMedicion1;
	}


	public List<SelectItem> getItemsCampanaMedicion1() {
		CampanaMedicionService service=new CampanaMedicionService();
		itemsCampanaMedicion1=new ArrayList<SelectItem>();
		for(CampanaMedicion tp:service.getAllRows1()){
			itemsCampanaMedicion1.add(new SelectItem(tp,tp.getDescripcion() + " (" + tp.getPeriodoMedicion().getAno() + "-" + tp.getPeriodoMedicion().getPeriodo() + ")"));
		}
		
		return itemsCampanaMedicion1;
	}


	public void setItemsCampanaMedicion1(List<SelectItem> itemsCampanaMedicion1) {
		this.itemsCampanaMedicion1 = itemsCampanaMedicion1;
	}


	public String prepararCrear(){
		//Esto funciona siempre que el bean tenga ambito de sesion 
		setMedicion(new Medicion());
		getMedicion().setEstado("1");
		getMedicion().setCompensable("PROGRAMADA");
		getMedicion().setFacCorCorriente(new BigDecimal(1));
		getMedicion().setFacCorTension(new BigDecimal(1));
		
		EstadoMedicionService s = new EstadoMedicionService();
		EstadoMedicion estadoMedicion = s.ReadByCod("G");
		
		getMedicion().setEstadoMedicion(estadoMedicion);
		getMedicion().setCampanaMedicion(campanaMedicion);
		
		fecInstalacion = null;
		fecRetiro = null;
		
		return "/zonasegura/medicionCrear?faces-redirect=true";
	}
	
	public String prepararEditar(){
		//TODO: Por implementar carga de la instancia
		//System.out.println("medicionId:"+Utilidad.getParametro("medicionId"));
		
		try {
			cargarMedicionActual();
			return "/zonasegura/medicionEditar?faces-redirect=true";
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());	

			
			return "/zonasegura/medicionListar?faces-redirect=true";
		}
		
		
	}

	public String prepararVer(){
		//TODO: Por implementar carga de la instancia
		//System.out.println("medicionId:"+Utilidad.getParametro("medicionId"));
		try {
			cargarMedicionActual();
			return "/zonasegura/medicionVer?faces-redirect=true";
		} catch (HibernateException e) {
			setMensaje(e.getMessage());			
			return "/zonasegura/medicionListar?faces-redirect=true";
		}
	}
	
	public String prepararVer2(){
		//TODO: Por implementar carga de la instancia
		//System.out.println("medicionId:"+Utilidad.getParametro("medicionId"));
		try {
			cargarMedicionActual();
			return "/zonasegura/medicionVer2?faces-redirect=true";
		} catch (Exception e) {
			setMensaje(e.getMessage());			
			return "/zonasegura/medicionxSuministroListar?faces-redirect=true";
		}		
		
	}
	
	public String salvar(){
		MedicionService service=new MedicionService();
		String result;
		
		
		try {
			
			Sql sql = new Sql("CALIDAD");
			String selectSql = "SELECT 	COUNT(*) FROM CAL_MEDICION WHERE ESTADO = '1' AND NUM_SUM_MEDICION = '" + medicion.getNumSumMedicion().trim() + "' AND " + 
								"PK_CAM_MEDICION_ID = " + String.valueOf(medicion.getCampanaMedicion().getCamMedicionId());
			Object[] objFila = sql.getFila(selectSql);
			
			Boolean pasa = true;
        	BigDecimal cantidadx = (BigDecimal) objFila[0];
        	
        	
        	if (medicion.getTipServicio().equals("R") || medicion.getTipServicio().equals("UR")){
        		String selectSql1 = "SELECT NUM_SUM_MEDICION, UBI_SUM_MEDICION " +
        							"FROM CAL_MEDICION WHERE PK_CAM_MEDICION_ID = " + String.valueOf(medicion.getCampanaMedicion().getCamMedicionId()) + " AND " + 
        									"CODSED = '" + medicion.getCodSed().trim() + "' AND ESTADO = '1' AND NUM_SUM_MEDICION <> '" + medicion.getNumSumMedicion().trim() + "'";
        		List<Object[]> objTabla = sql.consulta(selectSql1, false);  
        		
        		if (objTabla != null){
        			
        			if(objTabla.size() == 0) {
        				pasa = true;
        			}
        			
        			if(objTabla.size() == 1) {
        				String ubicacion = (String) objTabla.get(0) [1];
        				if(ubicacion.equals(medicion.getUbiSumMedicion())){
        					pasa = false;
        				} else {
        					pasa = true;
        				}
        			}
        			
        			if(objTabla.size() >= 2) {
        				pasa = false;
        			}
	        	}else{
	        		pasa = true;
	        	}
        		
 	        } 
        	
                  	
        	if(cantidadx.longValue() > 0 || !pasa){
        			
        		if(!pasa){
        			setMensaje("Verifique que no existan mas de dos suministros para la SED o que no se encuentren en la misma ubicacion.");	
        		}else {
        			setMensaje("Ya Existe el suministro en la misma Campaña.");	
        		}
    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,getMensaje(), getMensaje()));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
    			
    			return "/zonasegura/medicionCrear?faces-redirect=true";
        	} else {
    			service.create(medicion);
        	}
			
			//service.create(medicion);
			
			ConectaDb db = new ConectaDb("CALIDAD");
			Connection cn = db.getConnection();
	        CallableStatement cstmt = null;  
	        try {
	        	cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.GENERAR_SECUENCIA(?)}");
			   	cstmt.setLong(1, campanaMedicion.getCamMedicionId());
				
	            int ctos = cstmt.executeUpdate();;
	            cstmt.close(); 

	            if (ctos == 0) {
	                result = "0 filas afectadas";
	            }

	        } catch (SQLException e) {
	            result = e.getMessage();
				setMensaje(e.getMessage()+":"+e.getCause());			
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	        } finally {
	            try {
	                cn.close();
	            } catch (SQLException e) {
	                result = e.getMessage();
	    			setMensaje(e.getMessage()+":"+e.getCause());			
	    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
	    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            }
	        }			
			
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		
		System.out.println("Se salvo un medicion con exito.");
		MedicionesxCampana();
		return "/zonasegura/medicionListar?faces-redirect=true";
	}
	
	public String actualizar(){
		MedicionService service=new MedicionService();
		System.out.println("INICIANDO ACTUALIZACION\n");
		try {
			
			if(fecInstalacion!=null){
				medicion.setFecInstalacion(new java.sql.Date(fecInstalacion.getTime()));
			}else {
				medicion.setFecInstalacion(null);
			}
			
			if(fecRetiro!=null){
				medicion.setFecRetiro(new java.sql.Timestamp(fecRetiro.getTime()));
			}else {
				medicion.setFecRetiro(null);
			}
			
			if(medicion.getFecInstalacion()!= null && medicion.getFecRetiro()!= null ){
				if(medicion.getFecInstalacion().getTime() > medicion.getFecRetiro().getTime()){
	    			setMensaje("La fecha de Instalación no puede ser posterior a la de Retiro.");			
	    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,getMensaje(), getMensaje()));
	    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				}
			}
				
			
			service.update(medicion);
		} catch (HibernateException e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			
			e.printStackTrace();
			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se completo la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			
			e.printStackTrace();
			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		
		//service.delete(medicion.getComUnitariaId());
		//service.create(medicion);
		//System.out.println("updating:"+medicion.getComUnitariaId());
		System.out.println("Se actualizo un medicion con exito.");
		MedicionesxCampana();
		return "/zonasegura/medicionListar?faces-redirect=true";
	}
	
	public String eliminar(){
		String medicionId=Utilidad.getParametro("medicionId");
		MedicionService service=new MedicionService();
		
		Sql sql = new Sql("CALIDAD");
		String updateSql;
		String resultado;
		
		try {
			medicion=service.ReadById(new Long(medicionId));
			
			/*
			medicion.setEstado("0");
			
			//medicion.setTipoAlimentacion(null);
	
			
			updateSql = "UPDATE CAL_INTERVALO_TENSION SET ESTADO = '0' WHERE PK_MEDICION_ID = " + medicionId;
			
			resultado = sql.ejecuta(updateSql);	
			
			updateSql = "UPDATE CAL_COMPENSACION_TENSION SET ESTADO = '0' WHERE PK_MEDICION_ID = " + medicionId;
			
			resultado = sql.ejecuta(updateSql);	
			
			updateSql = "UPDATE CAL_CRONOGRAMA_MEDICION SET ESTADO = '0' WHERE PK_MEDICION_ID = " + medicionId;
			
			resultado = sql.ejecuta(updateSql);
			
			updateSql = "UPDATE CAL_INTERVALO_FR_TENSION SET ESTADO = '0' WHERE PK_MEDICION_ID = " + medicionId;
			
			resultado = sql.ejecuta(updateSql);
			
			service.update(medicion);	*/
			
			service.delete(medicion.getMedicionId());
			
			MedicionesxCampana();
			System.out.println("Se elimino una medicion con exito.");
			
		} catch (HibernateException e) {
			
			
			System.out.println(e.getMessage()+":"+e.getCause());
			
			e.printStackTrace();
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo +completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			
		} catch (Exception e) {
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		
				
		return "/zonasegura/medicionListar?faces-redirect=true";
	}
	
	public void cargarMedicionActual(){
		String medicionId=Utilidad.getParametro("medicionId");
		MedicionService service=new MedicionService();
		
		
		try {
			medicion=service.ReadById(new Long(medicionId));
			
			if(medicion.getFecInstalacion()!=null) {
				fecInstalacion = new java.util.Date( medicion.getFecInstalacion().getTime());				
			}else {
				fecInstalacion = null;
			}
			
			if(medicion.getFecRetiro()!=null){
				fecRetiro = new java.util.Date(medicion.getFecRetiro().getTime());
			}else {
				fecRetiro = null;
			}
			
		} catch (HibernateException e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se puede cargar Registro.", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			throw new HibernateException("No se puede cargar Medición.", e);
			
			
		} catch (Exception e) {
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se puede cargar Registro.", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
		}

	}
	
	public String procesarAllMedicion(){
		for(Medicion med:mediciones){
			medicion = med;
			
			procesarMedicion(String.valueOf(medicion.getMedicionId()));
			
		}
		
		return "/zonasegura/medicionListar?faces-redirect=true";
	}
	
	public String procesarMedicion(){
		String medicionId=Utilidad.getParametro("medicionId");
		String result = null;
		MedicionService service=new MedicionService();
		
		if(medicionId != null){
			
			medicion=service.ReadById(new Long(medicionId));
			
		}
		
		ConectaDb db = new ConectaDb("CALIDAD");
		
        Connection cn = db.getConnection();
        System.out.print("MEDICION ID2: " + medicion.getMedicionId() + "\n");
        CallableStatement cstmt = null;  
        CallableStatement cstmt2 = null; 
        try {
        	int ctos = 0;
			if(medicion.getEstadoMedicion().getCodEstMedicion().equals("CA")||
					medicion.getEstadoMedicion().getCodEstMedicion().equals("C")||
					medicion.getEstadoMedicion().getCodEstMedicion().equals("F")||
					medicion.getEstadoMedicion().getCodEstMedicion().equals("X")) {
				
				if(medicion.getEstadoMedicion().getCodEstMedicion().equals("F")){
					
					cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.EXEC_PREPARAR_MEDICION2(?,?)}"); 
					
					cstmt.setLong(1, medicion.getMedicionId());
					cstmt.setLong(2, 1);
					
					cstmt.execute();
					cstmt.close();					
					
					medicion=service.ReadById(new Long(medicionId));
					
				}
				
				if(medicion.getTipServicio().equals("U")){
					cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.VALIDAR_MEDICION2(?)}");  
					cstmt.setLong(1, medicion.getMedicionId());
					cstmt.execute();
					cstmt.close();					
					
				}
				
				if(medicion.getTipServicio().equals("R") || medicion.getTipServicio().equals("UR")){
					cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.VALIDAR_MEDICION_SER2(?)}");  
					cstmt.setLong(1, medicion.getMedicionId());
					cstmt.execute();
					cstmt.close();					
				}
				

				medicion=service.ReadById(new Long(medicionId));
				/******************************************/
				if(medicion.getEstadoMedicion().getCodEstMedicion().equals("V")){
					if(medicion.getTipServicio().equals("U")){
						
						cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.EVALUAR_MEDICION(?)}");  
						cstmt.setLong(1, medicion.getMedicionId());
						cstmt.execute();
						cstmt.close();
						
					}
					
					if(medicion.getTipServicio().equals("R") || medicion.getTipServicio().equals("UR")){
						cstmt2 = cn.prepareCall("{CALL CALIDAD_PACKAGE.EVALUAR_MEDICION_SER(?)}");  
						cstmt2.setLong(1, medicion.getMedicionId());
						
						System.out.println("MEDICION ID" + medicion.getMedicionId());
						
						cstmt2.execute();
						cstmt2.close();
					}
					/******************************************/
					
				}
			}
			
			medicion=service.ReadById(new Long(medicionId));
			
			if(medicion.getEstadoMedicion().getCodEstMedicion().equals("V")){
				
				if(medicion.getTipServicio().equals("U")){
				
					cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.EVALUAR_MEDICION(?)}");  
					cstmt.setLong(1, medicion.getMedicionId());
					cstmt.execute();
					cstmt.close();					
				}
				
				if(medicion.getTipServicio().equals("R") || medicion.getTipServicio().equals("UR")){
					cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.EVALUAR_MEDICION_SER(?)}");  
					cstmt.setLong(1, medicion.getMedicionId());
					cstmt.execute();
					cstmt.close();					
				}
				
				//cstmt.execute();
			}
						            
            //cstmt.close(); 
            
            medicion=service.ReadById(new Long(medicionId));

            if (ctos == 0) {
                result = "0 filas afectadas";
            }
            
            MedicionesxCampana();
            
        } catch (SQLException e) {
            result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
            
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
                result = e.getMessage();
    			setMensaje(e.getMessage()+":"+e.getCause());
    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                
            }
        }
		System.out.print(result);
		
		return "/zonasegura/medicionListar?faces-redirect=true";
	}	

	public String procesarMedicion(String medicionId){
		//String medicionId=Utilidad.getParametro("medicionId");
		String result = null;
		MedicionService service=new MedicionService();
		
		if(medicionId != null){
			
			medicion=service.ReadById(new Long(medicionId));
			
		}
		
		ConectaDb db = new ConectaDb("CALIDAD");
		
        Connection cn = db.getConnection();
        System.out.print("MEDICION ID2: " + medicion.getMedicionId() + "\n");
        CallableStatement cstmt = null;  
        
        try {
        	int ctos = 0;
			if(medicion.getEstadoMedicion().getCodEstMedicion().equals("CA")||
					medicion.getEstadoMedicion().getCodEstMedicion().equals("C")||
					medicion.getEstadoMedicion().getCodEstMedicion().equals("F")||
					medicion.getEstadoMedicion().getCodEstMedicion().equals("X")) {
				
				if(medicion.getEstadoMedicion().getCodEstMedicion().equals("F")){
					
					cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.EXEC_PREPARAR_MEDICION2(?,?)}"); 
					
					cstmt.setLong(1, medicion.getMedicionId());
					cstmt.setLong(2, 1);
					
					cstmt.execute();
					
					medicion=service.ReadById(new Long(medicionId));
					
				}
				
				if(medicion.getTipServicio().equals("U")){
					cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.VALIDAR_MEDICION2(?)}");  
					cstmt.setLong(1, medicion.getMedicionId());
					
				}else{
					cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.VALIDAR_MEDICION_SER2(?)}");  
					cstmt.setLong(1, medicion.getMedicionId());
				}
				
				cstmt.execute();
				
				medicion=service.ReadById(new Long(medicionId));
				/******************************************/
				if(medicion.getEstadoMedicion().getCodEstMedicion().equals("V")){
					if(medicion.getTipServicio().equals("U")){
						
						cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.EVALUAR_MEDICION(?)}");  
						cstmt.setLong(1, medicion.getMedicionId());
						
					}else{
						cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.EVALUAR_MEDICION_SER(?)}");  
						cstmt.setLong(1, medicion.getMedicionId());
					}
					/******************************************/
					cstmt.execute();
				}
			}
			
			medicion=service.ReadById(new Long(medicionId));
			
			if(medicion.getEstadoMedicion().getCodEstMedicion().equals("V")){
				
				if(medicion.getTipServicio().equals("U")){
				
					cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.EVALUAR_MEDICION(?)}");  
					cstmt.setLong(1, medicion.getMedicionId());
					
				}else{
					cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.EVALUAR_MEDICION_SER(?)}");  
					cstmt.setLong(1, medicion.getMedicionId());
				}
				
				cstmt.executeUpdate();
			}
						            
            //cstmt.close(); 
            
            medicion=service.ReadById(new Long(medicionId));

            if (ctos == 0) {
                result = "0 filas afectadas";
            }
            
            MedicionesxCampana();
            
        } catch (SQLException e) {
            result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
            
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
                result = e.getMessage();
    			setMensaje(e.getMessage()+":"+e.getCause());
    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                
            }
        }
		System.out.print(result);
		
		return "/zonasegura/medicionListar?faces-redirect=true";
	}	
	
	
	public String procesarPerturbacion(){
		String medicionId=Utilidad.getParametro("medicionId");
		String result = null;
		MedicionService service=new MedicionService();
		
		if(medicionId != null){
			
			medicion=service.ReadById(new Long(medicionId));
			
		}
		
		ConectaDb db = new ConectaDb("CALIDAD");
		
        Connection cn = db.getConnection();
        System.out.print("MEDICION ID2: " + medicion.getMedicionId() + "\n");
        CallableStatement cstmt = null;  
        try {
        	int ctos = 0;
			if(medicion.getPerEstMedicion().getCodEstMedicion().equals("CA")||
					medicion.getPerEstMedicion().getCodEstMedicion().equals("C")||
					medicion.getPerEstMedicion().getCodEstMedicion().equals("F")||
					medicion.getPerEstMedicion().getCodEstMedicion().equals("X")) {
				
				if(medicion.getPerEstMedicion().getCodEstMedicion().equals("F")){
					
					cstmt = cn.prepareCall("{CALL CAL_PERTURBACION_PACKAGE.PREPARAR_PERTURBACION(?,?)}"); 
					
					cstmt.setLong(1, medicion.getMedicionId());
					cstmt.setString(2, "SI");
					
					ctos = cstmt.executeUpdate();
					
					medicion=service.ReadById(new Long(medicionId));
					
				}
				
				if(medicion.getTipServicio().equals("U")){
					cstmt = cn.prepareCall("{CALL CAL_PERTURBACION_PACKAGE.VALIDAR_PERTURBACION(?)}");  
					cstmt.setLong(1, medicion.getMedicionId());
					
				}
				
				ctos = cstmt.executeUpdate();
				/******************************************/
				if(medicion.getTipServicio().equals("U")){
					if(medicion.getPerEstMedicion().getCodEstMedicion().equals("V")){
						cstmt = cn.prepareCall("{CALL CAL_PERTURBACION_PACKAGE.EVALUAR_PERTURBACION(?)}");  
						cstmt.setLong(1, medicion.getMedicionId());
					}
				}
				/******************************************/
				ctos = cstmt.executeUpdate();
			}
			
			medicion=service.ReadById(new Long(medicionId));
			
			if(medicion.getPerEstMedicion().getCodEstMedicion().equals("V")){
				
				if(medicion.getTipServicio().equals("U")){
				
					cstmt = cn.prepareCall("{CALL CAL_PERTURBACION_PACKAGE.EVALUAR_PERTURBACION(?)}");  
					cstmt.setLong(1, medicion.getMedicionId());
				}
				
				cstmt.executeUpdate();
			}
			
			medicion=service.ReadById(new Long(medicionId));
			
			if(medicion.getPerEstMedicion().getCodEstMedicion().equals("X")){
				
				if(medicion.getTipServicio().equals("U")){
				
					cstmt = cn.prepareCall("{CALL CAL_PERTURBACION_PACKAGE.COMPENSAR_PERTURBACION(?)}");  
					cstmt.setLong(1, medicion.getMedicionId());
				}
				
				cstmt.executeUpdate();
			}
			
            cstmt.close(); 

            if (ctos == 0) {
                result = "0 filas afectadas";
            }
            
            MedicionesxCampana();
            
        } catch (SQLException e) {
            result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
            
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
                result = e.getMessage();
    			setMensaje(e.getMessage()+":"+e.getCause());
    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                
            }
        }
		System.out.print(result);
		
		return "/zonasegura/medicionListar?faces-redirect=true";
	}
	
	public void buscarIGea(){
		String numSumMedicion;
		Sql sql = new Sql("CALIDAD");
		
		numSumMedicion = medicion.getNumSumMedicion();
			
		/*
		String s = "SELECT C.LABEL AS SUMINISTRO, C.LABEL AS CODIGO, C.NAME AS NOMBRE, C.PHASES AS FASE, " +
				"C.ADDRESS AS DIRECCION, C.METER AS MEDIDOR, C.RATES_GROUP AS TARIFA, C.KVA AS DEMANDA, " + 
				"S.LABEL AS CODSED, A.LABEL AS CODSALIDAMT, T.LABEL AS CODSET, " + 
				"(SELECT CS.DESCRIPTION FROM ZNET_VOLTAGE CS WHERE S.SECONDARY_VOLTAGE_LEVEL = CS.CLASS_ID||':'||CS.ID AND CS.ONIS_VER = 0) AS TENSION " + 
				"FROM IGEA_DATA.ZNET_CUSTOMERS C, ZNET_TRANSFORMATION_CENTER S, ZNET_MV_LINE A, ZNET_SUBSTATIONS T, ZNET_DELIVERY_POINTS AC " +
				"WHERE AC.TRANSFORMATION_CENTER = S.CLASS_ID||':'||S.ID AND " +  
				"C.FATHER_ELEMENT = AC.CLASS_ID||':'||AC.ID AND " +
				"S.GRANDFATHER_ELEMENT = A.CLASS_ID||':'||A.ID AND " +  
				"A.SUBSTATION = T.CLASS_ID||':'||T.ID AND " +  
				"C.ONIS_VER = 0 AND S.ONIS_VER = 0 AND A.ONIS_VER = 0 AND T.ONIS_VER = 0 AND " + 
				"C.LABEL = '" + numSumMedicion.trim() + "'";
		
		*/
		
		if(!medicion.getTipSuministro().equals("SED")) {
			String s = "SELECT S.CT, S.LINE, S.SED, SUBSTR(S.LOCALIDAD_SALIDA_BT,1,4) AS LOCALIDAD, " + 
					"S.TARIFA, S.SALIDA_BT, CALIDAD_PACKAGE.FN_TIPO_SERVICIO(SUBSTR(S.LOCALIDAD_SALIDA_BT,1,4)) AS TIPSERVICIO, " +
					"S.ADDRESS_SED, TO_CHAR(S.TEN_PRIMARIO_SED) ||'/'|| TO_CHAR(S.TEN_SECUNDARIO_SED) AS TEN_SED " + 
					"FROM SUMINISTRO_IGEA_ICIS S " + 
					"WHERE S.SUMINISTRO = '" + numSumMedicion.trim() + "'";
			
	        Object[] object = sql.getFila(s);
			
	        if (object != null){
		        //medicion.setNumSumMedicion((String) object[0]);
		        //medicion.setNomUsuario((String) object[2]);
		        medicion.setOpcTarifaria((String) object[4]);
		        medicion.setCodSed((String) object[2]);
		        medicion.setCodAlimentador((String) object[1]);
		        medicion.setCodSet((String) object[0]);
		        
		        String codLocalidad = (String) object[3];
		        
		        LocalidadService service = new LocalidadService();
		        Localidad localidad = service.ReadByCod(codLocalidad);
		        
		        medicion.setLocalidad(localidad);
		        medicion.setSalidabt((String) object[5]);
		        medicion.setTipServicio((String) object[6]);
		        
		        medicion.setDireccionSed((String) object[7]);
		        medicion.setTensionSed((String) object[8]);
		      
	        }
	        
	        s = "SELECT SUMINISTRO, NOMBRE, GENERAL_PHONE, HOME_PHONE, "+
	        	"SUPPLY_VOLTAGE, DESC_CALLE, NRO_PREDIO, ADDITIONAL_INFO, CALIDAD_PACKAGE.FN_TIPO_SUMINISTRO(TRIM(CUSTOMER_GROUP)) AS TIPSUMINISTRO, DIR_SUMINISTRO " +
	        	"FROM SUMINISTRO_ICIS_IGEA WHERE SUMINISTRO = " + numSumMedicion.trim();
	        
	        sql = new Sql("CALIDAD");
	        object = sql.getFila(s);
	        
	        if (object != null){
	        	
	        	String telefono;
	        	String nombre = (String) object[1];
	        	
	        	if(nombre.length() > 60) {
	        		nombre = nombre.substring(0, 60);
	        	}
	        	medicion.setNomUsuario(nombre);
	        	medicion.setTipSuministro((String) object[8]);
	        	
		        if(object[2]!=null) {
		        	telefono = (String) object[2];
		        	
		        	if(telefono.length() > 9){
		        		telefono = telefono.substring(0, 9);
		        	}	        	
		        	medicion.setTelUsuario(telefono);
		        } else {
		        	if(object[3]!=null) {
		        		telefono = (String) object[3];
		        		
		        		if(telefono.length() > 9){
		        			telefono = telefono.substring(0, 9);
			        	}
		        		
		        		medicion.setTelUsuario((String) object[3]);
		        	}
		        }
		        
		        if(object[4]!=null){
		        	medicion.setTenEntregada(new Long((String) object[4]));
		        }
		        
		        String direccion = (String) object[9];

		        /*
		        if(object[5]!=null){
		        	direccion = (String) object[5];
		        }
		        
		        if(object[6]!=null){
		        	direccion = direccion + " " + object[6];
		        }	 
		        
		        if(object[7]!=null){
		        	direccion = direccion + " " + object[7];
		        }
		        */
		        if(direccion.length() > 128){
		        	direccion = direccion.substring(0, 128);
	        	}
	        	
		        
		        medicion.setDirUsuario(direccion);
	        }
		} else {
			String s = "SELECT (SELECT TIP_MEDICION_ID FROM CAL_TIPO_MEDICION WHERE COD_TIP_MEDICION = '4'), " +
					"SUBSTR((SELECT SI.CT FROM SUMINISTRO_IGEA_ICIS SI WHERE SI.SED = '" + numSumMedicion.trim() + "' AND ROWNUM = 1),1,7), " +
					"SUBSTR((SELECT SI.LINE FROM SUMINISTRO_IGEA_ICIS SI WHERE SI.SED = '" + numSumMedicion.trim() + "' AND ROWNUM = 1),1,7),  " +
					"(SELECT L.LOCALIDAD_ID FROM CAL_LOCALIDAD L WHERE L.COD_LOCALIDAD = SUBSTR((SELECT SI.LOCALIDAD_SED FROM SUMINISTRO_IGEA_ICIS SI WHERE SI.SED = '" + numSumMedicion.trim() + "' AND ROWNUM = 1),1,4)), " +
					"CALIDAD_PACKAGE.FN_TIPO_SERVICIO(SUBSTR((SELECT SI.LOCALIDAD_SED FROM SUMINISTRO_IGEA_ICIS SI WHERE SI.SED = '" + numSumMedicion.trim() + "' AND ROWNUM = 1),1,4)), " +
					"(SELECT SI.ADDRESS_SED FROM SUMINISTRO_IGEA_ICIS SI WHERE SI.SED = '" + numSumMedicion.trim() + "' AND ROWNUM = 1), " +
					"(SELECT TO_CHAR(SI.TEN_PRIMARIO_SED) ||'/'|| TO_CHAR(SI.TEN_SECUNDARIO_SED) FROM SUMINISTRO_IGEA_ICIS SI WHERE SI.SED = '" + numSumMedicion.trim() + "' AND ROWNUM = 1) " +
					"FROM SUMINISTRO_IGEA_ICIS " +
					"WHERE SED = '" + numSumMedicion.trim() + "' AND ROWNUM = 1"; 
			
	        Object[] object = sql.getFila(s);
			
	        if (object != null){
		        TipoMedicionService tipoMedicionService = new TipoMedicionService();
		        TipoMedicion tipoMedicion = tipoMedicionService.ReadById(((BigDecimal) object[0]).longValue());
		        
		        medicion.setTipoMedicion(tipoMedicion);
		        medicion.setCodSet((String) object[1]);
		        medicion.setCodAlimentador((String) object[2]);
		        
		        if(object[3] != null) {
			        long idLocalidad = ((BigDecimal) object[3]).longValue();
			        
			        LocalidadService service = new LocalidadService();
			        Localidad localidad = service.ReadById(idLocalidad);
			        
			        medicion.setLocalidad(localidad);
		        }
		        medicion.setTipServicio((String) object[4]);
		        medicion.setCodSed(numSumMedicion.trim());
		        medicion.setDireccionSed((String) object[5]);
		        medicion.setTensionSed((String) object[6]);
		      
	        }			
		}
	}
	
	public boolean validarIntervalos(){
		
		
		return true;
	}
	
	public List<Medicion> listarIGea(){
		String numSumMedicion;
		Sql sql = new Sql("IGEA");
		
		numSumMedicion = medicion.getNumSumMedicion();
		
		
		String s = "SELECT C.CODE AS SUMINISTRO, C.LABEL AS CODIGO, C.NAME AS NOMBRE, C.PHASES AS FASE, " +
					"C.ADDRESS AS DIRECCION, C.METER AS MEDIDOR, C.RATES_GROUP AS TARIFA, C.KVA AS DEMANDA, " +
					"S.LABEL AS CODSED, A.LABEL AS CODSALIDAMT, T.LABEL AS CODSET, " +
					"(SELECT CS.REPORT_CODE FROM ZNET_SECONDARY_CODE CS WHERE CS.ID = S.SECONDARY_VOLTAGE_LEVEL_ID AND CS.ONIS_VER = 0) AS TENSION " +
					"FROM IGEA_DATA.ZNET_CUSTOMERS C, ZNET_TRANSFORMATION_CENTER S, ZNET_MV_LINE A, ZNET_SUBSTATIONS T " +
					"WHERE C.TRANSFORMATION_CENTER = S.CLASS_ID||':'||S.ID AND  " +
					"S.GRANDFATHER_ELEMENT = A.CLASS_ID||':'||A.ID AND  " +
					"A.FATHER_ELEMENT = T.CLASS_ID||':'||T.ID AND  " +
					"C.ONIS_VER = 0 AND S.ONIS_VER = 0 AND A.ONIS_VER = 0 AND T.ONIS_VER = 0 AND " +
					"C.CODE LIKE '%" + numSumMedicion + "%'";

        List<Object[]> listObject = sql.consulta(s, false);
		
        Medicion medicionaux;
        List<Medicion> list = new ArrayList<Medicion>();
        
        for (Object[] objects: listObject) {
        	medicionaux = new Medicion();
        	
        	medicionaux.setNumSumMedicion((String) objects[0]);
        	medicionaux.setNomUsuario((String) objects[2]);
        	medicionaux.setOpcTarifaria((String) objects[6]);
        	medicionaux.setCodSed((String) objects[8]);
        	medicionaux.setCodAlimentador((String) objects[9]);
        	medicionaux.setCodSet((String) objects[10]);
        	
            list.add(medicionaux);
            
        }
        
        return list;
	}
	
	  public void execute (ActionEvent event) {
		   download();
	  }
		 
	  // action="#{bean.download}"
	  public String download() {
	    final FacesContext facesContext = FacesContext.getCurrentInstance();
	    
	    long camMedicionId = campanaMedicion.getCamMedicionId();
	    
	    BufferedReader bfUp = null;
	    BufferedReader bfDown = null;
	    BufferedReader bfMid = null;
	    BufferedReader reader = null;
	    FileReader fileReaderUp = null;
	    FileReader fileReaderDown = null;
	    FileReader fileReaderMid = null;
	    FileWriter fileWriter = null;
	    BufferedWriter writer = null;
	    
	    String rutaArchivoOut;
	    
	    String rutaUp = facesContext.getExternalContext().getRealPath("/reportes/xmlup.txt");
	    String rutaDown = facesContext.getExternalContext().getRealPath("/reportes/xmldown.txt");
	    String rutaMid = facesContext.getExternalContext().getRealPath("/reportes/xmlmid.txt");
	    String path = System.getProperty("uploads.folder");
	    String nombreArchivo;

	    System.out.print(path + "\n");
	    
	    nombreArchivo = "filtro_"+new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xml";
	    rutaArchivoOut = path + nombreArchivo;
	    try {
	    	
		    fileReaderUp = new FileReader(rutaUp);
		    fileReaderDown = new FileReader(rutaDown);
		    fileReaderMid = new FileReader(rutaMid);
	        fileWriter = new FileWriter(rutaArchivoOut);
	
	    } catch (IOException e) {
	    	
	    }
	    
	    writer = new BufferedWriter(fileWriter);
	    
	    bfUp = new BufferedReader(fileReaderUp);
	    bfDown = new BufferedReader(fileReaderDown);
	    bfMid = new BufferedReader(fileReaderMid);
	    String sCadena;
	    
	    try {
	    
			while ((sCadena = bfUp.readLine()) != null) {				
				writer.write(sCadena);
	            writer.newLine();
			}
			
			//writer.write("AQUI VA EL FILTRO");
			
	    } catch (IOException e) {
	    
	    } finally {
	    	try {
		    	fileReaderUp.close();
				bfUp.close();
	    	}
	    	catch (IOException e){
	    		
	    	}
	    }
	    
	    //writer.write("AQUI VA EL FILTRO");
	    String result;
	    ConectaDb db = new ConectaDb("CALIDAD");
		
        Connection cn = db.getConnection();
        
        CallableStatement cstmt = null;  
        try {

        	cstmt = cn.prepareCall("{CALL ? := CALIDAD_PACKAGE.FN_GENERA_CONDICION_EN_XML(?)}");
			
			cstmt.registerOutParameter(1, Types.CLOB);
			cstmt.setLong(2, camMedicionId);

			
            int ctos = cstmt.executeUpdate();
            Clob dato = cstmt.getClob(1);
            cstmt.close(); 

            if (ctos == 0) {
                result = "0 filas afectadas";
            }
            
            reader = new BufferedReader(new InputStreamReader(dato.getAsciiStream()));
            
            while((sCadena = reader.readLine()) != null ) { 
				writer.write(sCadena);
	            writer.newLine();
            }
        } catch (IOException e) {
        	
        } catch (SQLException e) {
            result = e.getMessage();
            
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
        } finally {
            try {
            	reader.close();
            } catch (IOException e) {//
                result = e.getMessage();
            }
        }	 
        
	    try {
		    
			while ((sCadena = bfMid.readLine()) != null) {				
				writer.write(sCadena);
	            writer.newLine();
			}
			
	    } catch (IOException e) {
	    
	    } finally {
	    	try {
		    	fileReaderMid.close();
				bfMid.close();
	    	}
	    	catch (IOException e){
	    		
	    	}
	    }    
	    
        try {

        	cstmt = cn.prepareCall("{CALL ? := CALIDAD_PACKAGE.FN_GENERA_CONDICION_EN_XML_ACO(?)}");
			
			cstmt.registerOutParameter(1, Types.CLOB);
			cstmt.setLong(2, camMedicionId);

			
            int ctos = cstmt.executeUpdate();
            Clob dato = cstmt.getClob(1);
            cstmt.close(); 

            if (ctos == 0) {
                result = "0 filas afectadas";
            }
            
            reader = new BufferedReader(new InputStreamReader(dato.getAsciiStream()));
            
            while((sCadena = reader.readLine()) != null ) { 
				writer.write(sCadena);
	            writer.newLine();
            }
        } catch (IOException e) {
        	
        } catch (SQLException e) {
            result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
            
        } finally {
            try {
            	reader.close();
            	cn.close();
            } catch (SQLException e) {//
                result = e.getMessage();
    			setMensaje(e.getMessage()+":"+e.getCause());
    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                
            } catch (IOException e) {//
                result = e.getMessage();
            }
        }		    
	    
	    
	    try {
			while ((sCadena = bfDown.readLine()) != null) {				
				writer.write(sCadena);
	            writer.newLine();
			}
	
	    } catch (IOException e) {
		    
	    } finally {
	    	try {
				fileReaderDown.close();
				bfDown.close();
				writer.close();
	    	}
	    	catch (IOException e) {
	    		
	    	}
	    }
	    
	    String rutaArchivo = rutaArchivoOut;
	    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());
	    // XXX create temp. filecontent ... resultFile
	    writeOutContent(response, new File(rutaArchivo), nombreArchivo, "text/xml");
	    facesContext.responseComplete();
	    // dont use jsf-navigation-rules
	    return null;
	  }
	  
	  public String downloadGrafico() {
	    final FacesContext facesContext = FacesContext.getCurrentInstance();
	    
	    long camMedicionId = campanaMedicion.getCamMedicionId();
	    
	    String rutaArchivoOut;
	    
	    String rutaUp = facesContext.getExternalContext().getRealPath("/reportes/prueba.xlsx");
	    String path = System.getProperty("uploads.folder");
	    String nombreArchivo;
	    
	    nombreArchivo = "filtro_"+new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsx";
	    rutaArchivoOut = path + nombreArchivo;
	    
	    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);

		XSSFSheet objHssfSheet = null;
		objHssfSheet = objHssfWorkbook.getSheetAt(0);

		ModificarExcel.setCellValue(objHssfSheet, 0, 1, "CONTENIDO CAMBIADO");

		ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
	    
	    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());

	    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo, "application/vnd.ms-excel");
	    facesContext.responseComplete();

	    return null;
	  }
	 
	  void writeOutContent(final HttpServletResponse res, final File content, final String theFilename, String contentType) {
	    if (content == null)
	      return;
	    try {
	      
	      res.setHeader("Pragma", "no-cache");
	      res.setDateHeader("Expires", 0);
	      res.setContentType(contentType);
	      res.setHeader("Content-disposition", "attachment; filename=" + theFilename);
	      fastChannelCopy(Channels.newChannel(new FileInputStream(content)), Channels.newChannel(res.getOutputStream()));
	    } catch (final IOException e) {
	      // TODO produce a error message <img src="http://s0.wp.com/wp-includes/images/smilies/icon_smile.gif?m=1129645325g" alt=":)" class="wp-smiley"> 
	    }
	  }
	 
	  void fastChannelCopy(final ReadableByteChannel src, final WritableByteChannel dest) throws IOException {
	    final ByteBuffer buffer = ByteBuffer.allocateDirect(1000 * 1024);
	    while (src.read(buffer) != -1) {
	      buffer.flip();
	      dest.write(buffer);
	      buffer.compact();
	    }
	    buffer.flip();
	    while (buffer.hasRemaining()){
	      dest.write(buffer);
	    }
	  }


	public String getMensaje() {
		return mensaje;
	}


	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getNumSumMedicion() {
		return numSumMedicion;
	}


	public void setNumSumMedicion(String numSumMedicion) {
		this.numSumMedicion = numSumMedicion;
	}
	
	public java.util.Date getFecInstalacion() {
		return fecInstalacion;
	}


	public void setFecInstalacion(java.util.Date fecInstalacion) {
		this.fecInstalacion = fecInstalacion;
	}


	public java.util.Date getFecRetiro() {
		return fecRetiro;
	}


	public void setFecRetiro(java.util.Date fecRetiro) {
		this.fecRetiro = fecRetiro;
	}


	public List<Medicion> getMedicionesxSuministro() {
		return medicionesxSuministro;
	}


	public void setMedicionesxSuministro(List<Medicion> medicionesxSuministro) {
		this.medicionesxSuministro = medicionesxSuministro;
	}

	
	public String generarIdentificador(){	
		String result;
		
		ConectaDb db = new ConectaDb("CALIDAD");
		Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        try {
        	//System.out.println("generarIdentificador" + campanaMedicion.getCamMedicionId());
        	
        	cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.GENERAR_IDENTIFICADOR(?)}");
		   	cstmt.setLong(1, campanaMedicion.getCamMedicionId());
		   	cstmt.execute();
            cstmt.close(); 
        	
        } catch (SQLException e) {
            result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
            
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
                result = e.getMessage();
                
    			setMensaje(e.getMessage()+":"+e.getCause());
    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                
            }
        }			
		
        MedicionesxCampana();
        
        return result = "/zonasegura/medicionListar?faces-redirect=true";	
	}
	
	public String generarAllIdentificador(){	
		String result;
		
		ConectaDb db = new ConectaDb("CALIDAD");
		Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        try {
        	cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.GENERAR_ALL_IDENTIFICADOR(?)}");
		   	cstmt.setLong(1, campanaMedicion.getCamMedicionId());
			
            int ctos = cstmt.executeUpdate();;
            cstmt.close(); 

            if (ctos == 0) {
                result = "0 filas afectadas";
            }

        } catch (SQLException e) {
            result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
            
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
                result = e.getMessage();
                
    			setMensaje(e.getMessage()+":"+e.getCause());
    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                
            }
        }			
        
        MedicionesxCampana();
			
        return result = "/zonasegura/medicionListar?faces-redirect=true";	
	}
	
	public String generarIdentificadorSuministro(){	
		String result;
		
		ConectaDb db = new ConectaDb("CALIDAD");
		Connection cn = db.getConnection();
        CallableStatement cstmt = null;
        
        for (Medicion item_medicion : selectionItems) {
	        try {
	        	cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.GENERAR_IDENTIFICADOR_SUM(?,?)}");
			   	cstmt.setLong(1, campanaMedicion.getCamMedicionId());
			   	cstmt.setLong(2, item_medicion.getMedicionId());
				
	            int ctos = cstmt.executeUpdate();;
	            cstmt.close(); 
	
	            if (ctos == 0) {
	                result = "0 filas afectadas";
	            }
	
	        } catch (SQLException e) {
	            result = e.getMessage();
				setMensaje(e.getMessage()+":"+e.getCause());
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	            
	        } finally {
	            try {
	                cn.close();
	            } catch (SQLException e) {
	                result = e.getMessage();
	                
	    			setMensaje(e.getMessage()+":"+e.getCause());
	    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
	    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	                
	            }
	        }
        }
        
        MedicionesxCampana();
			
        return result = "/zonasegura/medicionListar?faces-redirect=true";	
	}
	
	public String compensarUrbanaSeleccionada(){	
		String result;
		
		ConectaDb db = new ConectaDb("CALIDAD");
		Connection cn = db.getConnection();
        CallableStatement cstmt = null;
        
        for (Medicion item_medicion : selectionItems) {
	        try {
	        	if(item_medicion.getTipServicio().equals("U") && 
	        			(item_medicion.getParametroMedido().getCodParMedido().equals("TP") ||
	        			item_medicion.getParametroMedido().getCodParMedido().equals("TE")) &&
	        			item_medicion.getEstadoMedicion().getCodEstMedicion().equals("X")) {
		        	cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.COMPENSAR_TENSION_UNITARIA(?,?,?,?)}");
		        	cstmt.setString(1, item_medicion.getNumSumMedicion());
				   	cstmt.setLong(2, campanaMedicion.getCamMedicionId());
				   	cstmt.setLong(3, campanaMedicion.getCamMedicionId());
				   	cstmt.setLong(4, campanaMedicion.getCamMedicionId());
				   	
		            cstmt.execute();
		            cstmt.close(); 
		
	        	}
	
	        } catch (SQLException e) {
	            result = e.getMessage();
				setMensaje(e.getMessage()+":"+e.getCause());
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	            
	        } finally {
	            try {
	                cn.close();
	            } catch (SQLException e) {
	                result = e.getMessage();
	                
	    			setMensaje(e.getMessage()+":"+e.getCause());
	    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
	    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	                
	            }
	        }
        }
			
        return result = "/zonasegura/medicionListar?faces-redirect=true";	
	}	
	
	public String compensarRuralSeleccionada(){	
		String result;
		
		ConectaDb db = new ConectaDb("CALIDAD");
		Connection cn = db.getConnection();
        CallableStatement cstmt = null;
        
        for (Medicion item_medicion : selectionItems) {
	        try {
	        	if((item_medicion.getTipServicio().equals("R") || item_medicion.getTipServicio().equals("UR")) &&
	        			(item_medicion.getParametroMedido().getCodParMedido().equals("TP") ||
	    	        	item_medicion.getParametroMedido().getCodParMedido().equals("TE")) &&	        			
	        			item_medicion.getEstadoMedicion().getCodEstMedicion().equals("X")) {
		        	cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.COMPENSAR_TENSION_SER(?)}");
		        	cstmt.setLong(1, item_medicion.getMedicionId());
					
		            cstmt.execute();
		            cstmt.close(); 
		
	        	}
	
	        } catch (SQLException e) {
	            result = e.getMessage();
				setMensaje(e.getMessage()+":"+e.getCause());
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	            
	        } finally {
	            try {
	                cn.close();
	            } catch (SQLException e) {
	                result = e.getMessage();
	                
	    			setMensaje(e.getMessage()+":"+e.getCause());
	    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
	    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	                
	            }
	        }
        }
			
        return result = "/zonasegura/medicionListar?faces-redirect=true";	
	}		
	
	public String agregarRepeticionesFallidas() {
		String result;
		
		ConectaDb db = new ConectaDb("CALIDAD");
		Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        try {
        	cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.COMPLETAR_CAMPANA(?)}");
		   	cstmt.setLong(1, campanaMedicion.getCamMedicionId());
			
            int ctos = cstmt.executeUpdate();;
            cstmt.close(); 

            if (ctos == 0) {
                result = "0 filas afectadas";
            }

        } catch (SQLException e) {
            result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
            
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
                result = e.getMessage();
                
    			setMensaje(e.getMessage()+":"+e.getCause());
    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                
            }
        }			
			
        MedicionesxCampana();
        return "/zonasegura/medicionListar?faces-redirect=true";	
	}
	
	public String agregarFallidas() {
		String result;
		
		ConectaDb db = new ConectaDb("CALIDAD");
		Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        try {
        	cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.COMPLETAR_CAMPANA_FALLIDAS(?)}");
		   	cstmt.setLong(1, campanaMedicion.getCamMedicionId());
			
            int ctos = cstmt.executeUpdate();;
            cstmt.close(); 

            if (ctos == 0) {
                result = "0 filas afectadas";
            }

        } catch (SQLException e) {
            result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
            
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
                result = e.getMessage();
                
    			setMensaje(e.getMessage()+":"+e.getCause());
    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                
            }
        }			
			
        MedicionesxCampana();
        return "/zonasegura/medicionListar?faces-redirect=true";	
	}
	
	public String actualizarDatosSuministros(){
		String result;
		
		ConectaDb db = new ConectaDb("CALIDAD");
		Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        try {
        	cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.ACTUALIZA_SUMINISTROS_CAMPANA(?)}");
		   	cstmt.setLong(1, campanaMedicion.getCamMedicionId());
			
            int ctos = cstmt.executeUpdate();;
            cstmt.close(); 

            if (ctos == 0) {
                result = "0 filas afectadas";
            }

        } catch (SQLException e) {
            result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
            
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
                result = e.getMessage();
                
    			setMensaje(e.getMessage()+":"+e.getCause());
    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                
            }
        }			
		
        MedicionesxCampana();
        return result = "/zonasegura/medicionListar?faces-redirect=true";
		
	}
	
	public String compensarUrbanos(){
		String result;
		
		ConectaDb db = new ConectaDb("CALIDAD");
		Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        try {
        	cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.COMPENSAR_CAMPANA(?)}");
		   	cstmt.setLong(1, campanaMedicion.getCamMedicionId());
			
		   	System.out.println("Campana:" + campanaMedicion.getCamMedicionId());
		   	
/*		   	int ctos = cstmt.executeUpdate();
            if (ctos == 0) {
                result = "0 filas afectadas";
            }
*/
		   	
		   	cstmt.execute();
            cstmt.close(); 


        } catch (SQLException e) {
            result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
            
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
                result = e.getMessage();
                
    			setMensaje(e.getMessage()+":"+e.getCause());
    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                
            }
        }			
		
        return result = "/zonasegura/medicionListar?faces-redirect=true";
		
	}

	public String compensarRurales(){
		String result;
		
		ConectaDb db = new ConectaDb("CALIDAD");
		Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        try {
        	cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.COMPENSAR_CAMPANA_SER(?,?)}");
		   	cstmt.setString(1, campanaMedicion.getPeriodoMedicion().getAno());
		   	cstmt.setString(2, campanaMedicion.getPeriodoMedicion().getSemestre());
			
		   	System.out.println("Campana:" + campanaMedicion.getCamMedicionId());
		   	
/*		   	int ctos = cstmt.executeUpdate();
            if (ctos == 0) {
                result = "0 filas afectadas";
            }
*/
		   	
		   	cstmt.execute();
            cstmt.close(); 


        } catch (SQLException e) {
            result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
            
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
                result = e.getMessage();
                
    			setMensaje(e.getMessage()+":"+e.getCause());
    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                
            }
        }			
		
        return result = "/zonasegura/medicionListar?faces-redirect=true";
		
	}	
	
	public String compensarSuministroTen(){
		String result;
		
		ConectaDb db = new ConectaDb("CALIDAD");
		Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        try {
        	cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.COMPENSAR_CAMPANA(?)}");
		   	cstmt.setLong(1, campanaMedicion.getCamMedicionId());
			
            int ctos = cstmt.executeUpdate();;
            cstmt.close(); 

            if (ctos == 0) {
                result = "0 filas afectadas";
            }

        } catch (SQLException e) {
            result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
            
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
                result = e.getMessage();
                
    			setMensaje(e.getMessage()+":"+e.getCause());
    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                
            }
        }			
		
        return result = "/zonasegura/medicionListar?faces-redirect=true";
		
	}
	
	public String compensarSuministroPer(){
		String result;
		
		ConectaDb db = new ConectaDb("CALIDAD");
		Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        try {
        	cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.COMPENSAR_CAMPANA(?)}");
		   	cstmt.setLong(1, campanaMedicion.getCamMedicionId());
			
            int ctos = cstmt.executeUpdate();;
            cstmt.close(); 

            if (ctos == 0) {
                result = "0 filas afectadas";
            }

        } catch (SQLException e) {
            result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
            
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
                result = e.getMessage();
                
    			setMensaje(e.getMessage()+":"+e.getCause());
    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                
            }
        }			
		
        return result = "/zonasegura/medicionListar?faces-redirect=true";
		
	}
	
	
	public CampanaMedicion getCampanaMedicion() {
		return campanaMedicion;
	}


	public void setCampanaMedicion(CampanaMedicion campanaMedicion) {
		this.campanaMedicion = campanaMedicion;
	}
	
	public void  filtroChanged(AjaxBehaviorEvent event) {  
		
		MedicionesxCampanaFiltro();
	}	
	
	public void  filtroChanged2(AjaxBehaviorEvent event) {  
		
		medicionesPendientes();
	}
	
	public void estadoTensionChanged(AjaxBehaviorEvent event){
		medicion.setCompensable(medicion.getEstadoMedicion().getDescripcion());
	}
	
	public void estadoPerturbacionChanged(AjaxBehaviorEvent event){
		medicion.setPerMensaje(medicion.getPerEstMedicion().getDescripcion());
	}
	

	public String getTipoPunto() {
		return tipoPunto;
	}


	public void setTipoPunto(String tipoPunto) {
		this.tipoPunto = tipoPunto;
	}


	public String getTipServicio() {
		return tipServicio;
	}


	public void setTipServicio(String tipServicio) {
		this.tipServicio = tipServicio;
	}


	public String getNisFilter() {
		return nisFilter;
	}


	public void setNisFilter(String nisFilter) {
		this.nisFilter = nisFilter;
	}


	public String getNomUsuarioFilter() {
		return nomUsuarioFilter;
	}


	public void setNomUsuarioFilter(String nomUsuarioFilter) {
		this.nomUsuarioFilter = nomUsuarioFilter;
	}
	
/**/
	public String getParametroMedidoFilter() {
		return parametroMedidoFilter;
	}


	public void setParametroMedidoFilter(String parametroMedidoFilter) {
		this.parametroMedidoFilter = parametroMedidoFilter;
	}
	
	public String getTipoSuministroFilter() {
		return tipoSuministroFilter;
	}


	public void setTipoSuministroFilter(String tipoSuministroFilter) {
		this.tipoSuministroFilter = tipoSuministroFilter;
	}
	
	public String getTipoPuntoFilter() {
		return tipoPuntoFilter;
	}


	public void setTipoPuntoFilter(String tipoPuntoFilter) {
		this.tipoPuntoFilter = tipoPuntoFilter;
	}

	public String getSedFilter() {
		return sedFilter;
	}


	public void setSedFilter(String sedFilter) {
		this.sedFilter = sedFilter;
	}


	public String getTipoServicioFilter() {
		return tipoServicioFilter;
	}


	public void setTipoServicioFilter(String tipoServicioFilter) {
		this.tipoServicioFilter = tipoServicioFilter;
	}

	public String getLocalidadFilter() {
		return localidadFilter;
	}


	public void setLocalidadFilter(String localidadFilter) {
		this.localidadFilter = localidadFilter;
	}

	
	public List<SelectItem> getItemsParametroMedido2() {
		ParametroMedidoService service=new ParametroMedidoService();
		itemsParametroMedido2=new ArrayList<SelectItem>();
		itemsParametroMedido2.add(new SelectItem("", ""));
		for(ParametroMedido tp:service.getAllRows()){
			itemsParametroMedido2.add(new SelectItem(tp.getCodParMedido(),tp.getCodParMedido()+"-"+tp.getDescripcion()));
		}
		return itemsParametroMedido2;
	}


	public void setItemsParametroMedido2(List<SelectItem> itemsParametroMedido2) {
		this.itemsParametroMedido2 = itemsParametroMedido2;
	}
	
	
	public List<SelectItem> getItemsTipoSuministro2() {
		itemsTipoSuministro2=new ArrayList<SelectItem>();
		itemsTipoSuministro2.add(new SelectItem("", ""));
		itemsTipoSuministro2.add(new SelectItem("MT","MT"));
		itemsTipoSuministro2.add(new SelectItem("SED","SED"));
		itemsTipoSuministro2.add(new SelectItem("BT","BT"));
		return itemsTipoSuministro2;
	}


	public void setItemsTipoSuministro2(List<SelectItem> itemsTipoSuministro2) {
		this.itemsTipoSuministro2 = itemsTipoSuministro2;
	}
	
	
	public List<SelectItem> getItemsLocalidad2() {
		LocalidadService service=new LocalidadService();
		itemsLocalidad2=new ArrayList<SelectItem>();
		itemsLocalidad2.add(new SelectItem("", ""));
		for(Localidad tp:service.getAllRows()){
			itemsLocalidad2.add(new SelectItem(tp.getCodLocalidad(),tp.getCodLocalidad()+"-"+tp.getNombre()));
			
		}
		return itemsLocalidad2;
		
	}


	public void setItemsLocalidad2(List<SelectItem> itemsLocalidad2) {
		this.itemsLocalidad2 = itemsLocalidad2;
	}

	
	public Filter<?> getFilterParametroMedido() {
        return new Filter<Medicion>() {
            public boolean accept(Medicion t) {
            	String parametroMedido = getParametroMedidoFilter();
                if (parametroMedido == null || parametroMedido.length() == 0 || parametroMedido.equals(t.getParametroMedido().getCodParMedido())) {
                	 															
                    return true;
                }
                return false;
            }
        };
    }

	public Filter<?> getFilterTipoSuministro() {
        return new Filter<Medicion>() {
            public boolean accept(Medicion t) {
            	String tipoSuministro = getTipoSuministroFilter();
                if (tipoSuministro == null || tipoSuministro.length() == 0 || tipoSuministro.equals(t.getTipSuministro())) {
                	 															
                    return true;
                }
                return false;
            }
        };
    }
	

	public Filter<?> getFilterTipoPunto() {
        return new Filter<Medicion>() {
            public boolean accept(Medicion t) {
            	String tipoPunto = getTipoPuntoFilter();
                if (tipoPunto == null || tipoPunto.length() == 0 || tipoPunto.equals(t.getTipoPunto().getCodTipPunto())) {
                	 															
                    return true;
                }
                return false;
            }
        };
    }
	
		
	public Filter<?> getFilterTipoServicio() {
        return new Filter<Medicion>() {
            public boolean accept(Medicion t) {
            	String tipoServicio = getTipoServicioFilter();
                if (tipoServicio == null || tipoServicio.length() == 0 || tipoServicio.equals(t.getTipServicio())) {
                	 															
                    return true;
                }
                return false;
            }
        };
    }
	
	public Filter<?> getFilterLocalidad() {
        return new Filter<Medicion>() {
            public boolean accept(Medicion t) {
            	String localidad = getLocalidadFilter();
            	
                if (localidad == null) {
                	return true;
                }
                if (localidad.length() == 0) {
                	return true;
                }	
                if(localidad.length() > 0) {
                	if(t.getLocalidad() == null){
                		return false;
                	}
                	
                	if(localidad.equals(t.getLocalidad().getCodLocalidad())) {
                		return true;
                	}
                }
            	
                return false;
            }
        };
    }
	
	public String getFormato() {
		return formato;
	}


	public void setFormato(String formato) {
		this.formato = formato;
	}


	public List<SelectItem> getItemsFormato() {
		
		itemsFormato=new ArrayList<SelectItem>();
		itemsFormato.add(new SelectItem("PDF", "PDF"));
		itemsFormato.add(new SelectItem("XLSX","XLSX"));
		itemsFormato.add(new SelectItem("DOCX","DOCX"));
		itemsFormato.add(new SelectItem("HTML","HTML"));
		itemsFormato.add(new SelectItem("XML","XML"));
		//itemsFormato.add(new SelectItem("TXT","TXT"));
		
		return itemsFormato;
	}


	public void setItemsFormato(List<SelectItem> itemsFormato) {
		this.itemsFormato = itemsFormato;
	}


	public long getTipoPuntoReporte() {
		return tipoPuntoReporte;
	}


	public void setTipoPuntoReporte(long tipoPuntoReporte) {
		this.tipoPuntoReporte = tipoPuntoReporte;
	}


	public List<SelectItem> getItemsTipoPuntoReporte() {
		itemsTipoPuntoReporte=new ArrayList<SelectItem>();
		itemsTipoPuntoReporte.add(new SelectItem(1, "BASICOS"));
		itemsTipoPuntoReporte.add(new SelectItem(2,"ADICIONALES"));		
		return itemsTipoPuntoReporte;
	}


	public void setItemsTipoPuntoReporte(List<SelectItem> itemsTipoPuntoReporte) {
		this.itemsTipoPuntoReporte = itemsTipoPuntoReporte;
	}


	public long getTipo() {
		return tipo;
	}


	public void setTipo(long tipo) {
		this.tipo = tipo;
	}


	public List<SelectItem> getItemsTipo() {
		itemsTipo=new ArrayList<SelectItem>();
		itemsTipo.add(new SelectItem(1,"CRONOGRAMA DE SUMINISTROS BT URBANOS - TENSION"));
		itemsTipo.add(new SelectItem(2,"CRONOGRAMA DE SUMINISTROS MT URBANOS - TENSION"));
		itemsTipo.add(new SelectItem(3,"CRONOGRAMA DE SUMINISTROS MT/BT RURALES - TENSION"));
		itemsTipo.add(new SelectItem(4,"CRONOGRAMA DE SUMINISTROS BT - PERTURBACIONES"));
		itemsTipo.add(new SelectItem(5,"CRONOGRAMA DE SUMINISTROS MT - PERTURBACIONES"));
		itemsTipo.add(new SelectItem(6,"CRONOGRAMA DE SUMINISTROS SED - PERTURBACIONES"));
		
		return itemsTipo;
	}


	public void setItemsTipo(List<SelectItem> itemsTipo) {
		this.itemsTipo = itemsTipo;
	}
	
	public List<SelectItem> getItemsPreFlicker() {
		itemsPreFlicker=new ArrayList<SelectItem>();
		itemsPreFlicker.add(new SelectItem("", ""));
		itemsPreFlicker.add(new SelectItem("SI","SI"));
		itemsPreFlicker.add(new SelectItem("NO","NO"));
		return itemsPreFlicker;
	}


	public void setItemsPreFlicker(List<SelectItem> itemsPreFlicker) {
		this.itemsPreFlicker = itemsPreFlicker;
	}


	public List<SelectItem> getItemsPreArmonicas() {
		itemsPreArmonicas=new ArrayList<SelectItem>();
		itemsPreArmonicas.add(new SelectItem("", ""));
		itemsPreArmonicas.add(new SelectItem("SI","SI"));
		itemsPreArmonicas.add(new SelectItem("NO","NO"));
		return itemsPreArmonicas;
	}


	public void setItemsPreArmonicas(List<SelectItem> itemsPreArmonicas) {
		this.itemsPreArmonicas = itemsPreArmonicas;
	}



	public List<SelectItem> getItemsTipoEnergia() {
		itemsTipoEnergia=new ArrayList<SelectItem>();
		itemsTipoEnergia.add(new SelectItem("M","MEDIDA"));
		itemsTipoEnergia.add(new SelectItem("E","EVALUADA"));
		return itemsTipoEnergia;
	}

	public void setItemsTipoEnergia(List<SelectItem> itemsTipoEnergia) {
		this.itemsTipoEnergia = itemsTipoEnergia;
	}

	public List<SelectItem> getItemsTipoMedicionEnergia() {
		itemsTipoMedicionEnergia=new ArrayList<SelectItem>();
		itemsTipoMedicionEnergia.add(new SelectItem("MI","MEDICION INDIRECTA"));
		itemsTipoMedicionEnergia.add(new SelectItem("MS","MEDICION SEMIDIRECTA"));
		itemsTipoMedicionEnergia.add(new SelectItem("MD","MEDICION DIRECTA"));
		return itemsTipoMedicionEnergia;
	}

	public void setItemsTipoMedicionEnergia(
			List<SelectItem> itemsTipoMedicionEnergia) {
		this.itemsTipoMedicionEnergia = itemsTipoMedicionEnergia;
	}
	
	public String reporte() throws Exception{
		final FacesContext facesContext = FacesContext.getCurrentInstance();
        String jrxmlFileName = "";
        String jpgFileName = "";

		if(tipo == 1) {
			if(tipoPuntoReporte == 1) jrxmlFileName = "/reportes/crono_tensión_a clientes_bt_al_basicos.jrxml";
			else jrxmlFileName = "/reportes/crono_tensión_a clientes_bt_al_adicionales.jrxml";
		} else if(tipo == 2) {
			if(tipoPuntoReporte == 1) jrxmlFileName = "/reportes/crono_tensión_a clientes_mt_al_basicos.jrxml";
			else jrxmlFileName = "/reportes/crono_tensión_a clientes_mt_al_adicionales.jrxml";
		} else if(tipo == 3) {
			if(tipoPuntoReporte == 1) jrxmlFileName = "/reportes/crono_de_tensión_clientes_mt-bt_rural_basicos.jrxml";
			else jrxmlFileName = "/reportes/crono_de_tensión_clientes_mt-bt_rural_adicionales.jrxml";
		} else if(tipo == 4) {
			if(tipoPuntoReporte == 1) jrxmlFileName = "/reportes/crono_perturbaciones_bt_basicos.jrxml";
			else jrxmlFileName = "/reportes/crono_perturbaciones_bt_adicionales.jrxml";
		} else if(tipo == 5) {
			if(tipoPuntoReporte == 1) jrxmlFileName = "/reportes/crono_perturbaciones_mt_basicos.jrxml";
			else jrxmlFileName = "/reportes/crono_perturbaciones_mt_adicionales.jrxml";
		} else if(tipo == 6) {
			if(tipoPuntoReporte == 1) jrxmlFileName = "/reportes/crono_perturbaciones_sed_basicos.jrxml";
			else jrxmlFileName = "/reportes/crono_perturbaciones_sed_adicionales.jrxml";
		}
		
	    String nombreArchivo = null;
		try {

            File archivoReporte;
            File archivoImagen;
            HashMap<String,Object> hm = null;
            hm = new HashMap<String,Object>();
            
            jpgFileName = "/reportes/electrodunas.jpg";
            archivoReporte = new File(facesContext.getExternalContext().getRealPath(jrxmlFileName));
            archivoImagen = new File(facesContext.getExternalContext().getRealPath(jpgFileName));

       	 	hm.put("MESANO",campanaMedicion.getPeriodoMedicion().getDescripcion());
        	hm.put("DESCRIPCION",campanaMedicion.getDescripcion());
        	hm.put("CAM_MEDICION_ID",campanaMedicion.getCamMedicionId());
        	hm.put("RUTA_IMAGEN",archivoImagen.getPath());
        	
        	ConectaDb db = new ConectaDb("CALIDAD");
			JasperReport jasperReport=JasperCompileManager.compileReport(archivoReporte.getPath());
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, hm,db.getConnection());
			//HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());
        	
    		if (jasperPrint != null) {
    		    if(formato.equals("XLSX")) {
    		    	nombreArchivo = "Cronograma_"+new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsx";
    		    	jrXlsxExp(nombreArchivo, jasperPrint);	
    		    }
    		    else if(formato.equals("PDF")) {
    		    	nombreArchivo = "Cronograma_"+new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".pdf";
    		    	jrPdfExp(nombreArchivo, jasperPrint);	
    		    }
    		}
    		
		} catch (Exception e) {
			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo generar el reporte", "No se pudo generar el reporte"));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		
			e.printStackTrace();
			throw e;
        }
		return null;
	}
	
	
	public void jrXlsxExp(String fileName,JasperPrint jasperPrint) throws JRException, IOException{
		/*
		FacesContext fc = FacesContext.getCurrentInstance();
	    ExternalContext ec = fc.getExternalContext();
	    ec.responseReset();
	    OutputStream out = ec.getResponseOutputStream();
		JRXlsxExporter exporterXls = new JRXlsxExporter();
	    ec.setResponseHeader("Content-Disposition","attachment;filename=\""+ xlsFileNam + "\""); // Configurar cabecera http
		ec.setResponseContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"); 
		exporterXls.setParameter(JRXlsExporterParameter.JASPER_PRINT,jasperPrint);
		exporterXls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM,out);
		exporterXls.exportReport();
		fc.responseComplete();*/
		exportar("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",fileName,jasperPrint,new JRXlsxExporter());
	}
	
	public void jrPdfExp(String fileName,JasperPrint jasperPrint) throws JRException, IOException{
		/*
		FacesContext fc = FacesContext.getCurrentInstance();
	    ExternalContext ec = fc.getExternalContext();
	    ec.responseReset();
	    OutputStream out = ec.getResponseOutputStream();
		JRPdfExporter exporter = new JRPdfExporter(); 
		ec.setResponseContentType("application/pdf");
		ec.setResponseHeader("Content-Disposition", "attachment;filename=\""+pdfFileName+"\"");
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,out);
		exporter.exportReport();
		fc.responseComplete();*/
		exportar("application/pdf",fileName,jasperPrint,new JRPdfExporter());
	}
	
	private void exportar(String contentType, String fileName,JasperPrint jasperPrint,JRAbstractExporter exporter) throws JRException, IOException{
		FacesContext fc = FacesContext.getCurrentInstance();
	    ExternalContext ec = fc.getExternalContext();
	    ec.responseReset();
	    OutputStream out = ec.getResponseOutputStream();
		ec.setResponseContentType(contentType);
		ec.setResponseHeader("Content-Disposition", "attachment;filename=\""+fileName+"\"");
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,out);
		exporter.exportReport();
		fc.responseComplete();
	}
	

	public String reporteOld(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
        String jrxmlFileName = "";
        String jpgFileName = "";
        //int tipo = 4;
		if(tipo == 1) {
			if(tipoPuntoReporte == 1) jrxmlFileName = "/reportes/crono_tensión_a clientes_bt_al_basicos.jasper";
			else jrxmlFileName = "/reportes/crono_tensión_a clientes_bt_al_adicionales.jasper";
			if(formato.equals("PDF")) return "/zonasegura/reporteSum?faces-redirect=true";
		
		}
		
		if(tipo == 2) {
			if(tipoPuntoReporte == 1) jrxmlFileName = "/reportes/crono_tensión_a clientes_mt_al_basicos.jasper";
			else jrxmlFileName = "/reportes/crono_tensión_a clientes_mt_al_adicionales.jasper";
			if(formato.equals("PDF")) return "/zonasegura/reporteSumMT?faces-redirect=true";
		
		}
		
		if(tipo == 3) {
			if(tipoPuntoReporte == 1) jrxmlFileName = "/reportes/crono_de_tensión_clientes_mt-bt_rural_basicos.jasper";
			else jrxmlFileName = "/reportes/crono_de_tensión_clientes_mt-bt_rural_adicionales.jasper";
			if(formato.equals("PDF")) return "/zonasegura/reporteSumMTBTRurales?faces-redirect=true";
		
		}
		
		if(tipo == 4) {
			if(tipoPuntoReporte == 1) jrxmlFileName = "/reportes/crono_perturbaciones_bt_basicos.jasper";
			else jrxmlFileName = "/reportes/crono_perturbaciones_bt_adicionales.jasper";
			if(formato.equals("PDF")) return "/zonasegura/reporteSumPerturbaciones?faces-redirect=true";
		}
			
		if(tipo == 5) {
			if(tipoPuntoReporte == 1) jrxmlFileName = "/reportes/crono_perturbaciones_mt_basicos.jasper";
			else jrxmlFileName = "/reportes/crono_perturbaciones_mt_adicionales.jasper";
			if(formato.equals("PDF")) return "/zonasegura/reporteSumPerturbaciones?faces-redirect=true";
		
		}
		
		if(tipo == 6) {
			if(tipoPuntoReporte == 1) jrxmlFileName = "/reportes/crono_perturbaciones_sed_basicos.jasper";
			else jrxmlFileName = "/reportes/crono_perturbaciones_sed_adicionales.jasper";
			if(formato.equals("PDF")) return "/zonasegura/reporteSumPerturbaciones?faces-redirect=true";
		
		}
		
		String path = System.getProperty("uploads.folder");
	    String nombreArchivo = null;
	    String rutaArchivoOut = null;
	    
	    String type = null;
		
		try {

            File archivoReporte;
            File archivoImagen;
            HashMap hm = null;
            hm = new HashMap();
            
            jpgFileName = "/reportes/electrodunas.jpg";
            archivoReporte = new File(facesContext.getExternalContext().getRealPath(jrxmlFileName));
            archivoImagen = new File(facesContext.getExternalContext().getRealPath(jpgFileName));

       	 	hm.put("MESANO",campanaMedicion.getPeriodoMedicion().getDescripcion());
        	hm.put("DESCRIPCION",campanaMedicion.getDescripcion());
        	hm.put("CAM_MEDICION_ID",campanaMedicion.getCamMedicionId());
        	hm.put("RUTA_IMAGEN",archivoImagen.getPath());
        	
        	ConectaDb db = new ConectaDb("CALIDAD");
        	
        	String printFileName = null;
    		printFileName = JasperFillManager.fillReportToFile(archivoReporte.getPath(),
    	            hm, db.getConnection());
    		
    		if (printFileName != null) {

    		    
    		    if(formato.equals("XLSX")) {
    		    	JRXlsxExporter exporter = new JRXlsxExporter();
    		    	
    		    	nombreArchivo = "Cronograma_"+new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsx";
        		    rutaArchivoOut = path + nombreArchivo;
    		    	
    		    	exporter.setParameter(JRExporterParameter.INPUT_FILE_NAME,
      	                  printFileName);
    		    	exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME,
  	            		rutaArchivoOut);

    		    	exporter.exportReport();
    		    	type = "application/vnd.ms-excel";
    		    }
    		    if(formato.equals("DOCX")) {
    		    	JRDocxExporter exporter = new JRDocxExporter();
    		    	
    		    	nombreArchivo = "Cronograma_"+new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".docx";
        		    rutaArchivoOut = path + nombreArchivo;
    		    	
    		    	exporter.setParameter(JRExporterParameter.INPUT_FILE_NAME,
        	                  printFileName);
      		    	exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME,
    	            		rutaArchivoOut);

      		    	exporter.exportReport();
      		    	type = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    		    }
    		    
    		    if(formato.equals("HTML")) {
    		    	nombreArchivo = "Cronograma_"+new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".html";
        		    rutaArchivoOut = path + nombreArchivo;
    		    	
	                JasperExportManager.exportReportToHtmlFile(printFileName, rutaArchivoOut);
	                type = "text/html";
				
    		    }
    		    
    		    if(formato.equals("XML")) {
    		    	nombreArchivo = "Cronograma_"+new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xml";
        		    rutaArchivoOut = path + nombreArchivo;
    		    	
    		    	JasperExportManager.exportReportToXmlFile( printFileName, rutaArchivoOut, false);
    		    	
    		    	type = "application/xml";
    		    }
    		    if(formato.equals("TXT")) {
    		    	JRTextExporter exporter = new JRTextExporter();
    		    	
    		    	nombreArchivo = "Cronograma_"+new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".txt";
        		    rutaArchivoOut = path + nombreArchivo;
    		    	
    		    	exporter.setParameter(JRExporterParameter.INPUT_FILE_NAME,
        	                  printFileName);
      		    	exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME,
    	            		rutaArchivoOut);

      		    	exporter.exportReport();
      		    	type = "text/plain";
    		    }
    		    
    		}
		} catch (JRException e) {//JRException
			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo generar el reporte", "No se pudo generar el reporte"));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			e.printStackTrace();
			
			return null;
			//e.printStackTrace();
		
        }
		
	    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());	    

    	writeOutContent(response, new File(rutaArchivoOut), nombreArchivo, type);
	    facesContext.responseComplete();
	    
		//return "/zonasegura/medicionListar?faces-redirect=true";
		return null;
	}


	public String getUbiSumMedicionFilter() {
		return ubiSumMedicionFilter;
	}


	public void setUbiSumMedicionFilter(String ubiSumMedicionFilter) {
		this.ubiSumMedicionFilter = ubiSumMedicionFilter;
	}
	
	public Filter<?> getFilterUbiSumMedicion() {
        return new Filter<Medicion>() {
            public boolean accept(Medicion t) {
            	String ubiSumMedicion = getUbiSumMedicionFilter();
                if (ubiSumMedicion == null || ubiSumMedicion.length() == 0 || ubiSumMedicion.equals(t.getUbiSumMedicion())) {
                	 															
                    return true;
                }
                return false;
            }
        };
    }


	public List<SelectItem> getItemsUbiSumMedicion() {
		itemsUbiSumMedicion=new ArrayList<SelectItem>();
		itemsUbiSumMedicion.add(new SelectItem("",""));
		itemsUbiSumMedicion.add(new SelectItem("N","NO CORRESPONDE"));
		itemsUbiSumMedicion.add(new SelectItem("I","TRAMO INICIAL DE LA SED MT/BT"));
		itemsUbiSumMedicion.add(new SelectItem("F","TRAMO FINAL DE LA SED MT/BT"));
		return itemsUbiSumMedicion;
	}


	public void setItemsUbiSumMedicion(List<SelectItem> itemsUbiSumMedicion) {
		this.itemsUbiSumMedicion = itemsUbiSumMedicion;
	}
	
	public String exportartablamedicion(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
	    
	    String rutaArchivoOut;
	    String rutaUp;
	    rutaUp = facesContext.getExternalContext().getRealPath("/reportes/Anexomedicion.xlsx");
	    String path = System.getProperty("uploads.folder");
	    String nombreArchivo;
	    String compensableTemp = null;
	    String estadoMedicionTemp = null;
	    
	    System.out.print(path + "\n");
	    
	    nombreArchivo = "medicion" + "_" + getCampanaMedicion().getDescripcion() + "_" + getCampanaMedicion().getPeriodoMedicion().getPeriodo() + "_"+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsx";
	    rutaArchivoOut = path + nombreArchivo;
	    	    
	    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);

		XSSFSheet objHssfSheet = null;
		
		try  {
	    
		    MedicionService service = new MedicionService();
		    
		    objHssfSheet = objHssfWorkbook.getSheetAt(0);
			
			int intRow = 2;
			int intCell = 0;
			IntervaloTensionService intervaloTensionService = new IntervaloTensionService();
			
			for (Medicion medicion : service.getAllRows1(campanaMedicion.getCamMedicionId())) {
				
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell, medicion.getNumIdentificador());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 1,medicion.getNumSumMedicion());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 2,medicion.getSecuencia());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 3,medicion.getNomUsuario());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 4,medicion.getTipSuministro());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 5,medicion.getCodSed());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 6,medicion.getNumSumReemplazado());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 7,medicion.getTenEntregada());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 8,medicion.getUbiSumMedicion());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 9,medicion.getOpcTarifaria());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 10,medicion.getDescripcion());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 11,medicion.getTipServicio());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 12,medicion.getCodLinea());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 13,medicion.getCodSet());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 14,medicion.getCodAlimentador());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 15,medicion.getTelUsuario());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 16,medicion.getNomArchivo());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 17,medicion.getFacCorTension());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 18,medicion.getFacCorCorriente());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 19,medicion.getFecInstalacion());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 20,medicion.getFecRetiro());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 21,medicion.getPreFlicker());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 22,medicion.getPreArmonicas());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 23,medicion.getObservaciones());

					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 24,medicion.getCampanaMedicion().getDescripcion());
				
					if(medicion.getTipoMedicion() != null){
						ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 25,medicion.getTipoMedicion().getDescripcion());
					}
					if(medicion.getTipoPunto() != null){
						ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 26,medicion.getTipoPunto().getCodTipPunto()+"-" +medicion.getTipoPunto().getDescripcion());
					}
					if(medicion.getLocalidad() != null) {
						ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 27,medicion.getLocalidad().getCodLocalidad()+"-" + medicion.getLocalidad().getNombre());
					}
					if(medicion.getTipoAlimentacion() != null){
						ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 28,medicion.getTipoAlimentacion().getDescripcion());
					}
					if(medicion.getTipoTrabajo() != null){
						ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 29,medicion.getTipoTrabajo().getDescripcion());
					}
					if(medicion.getEquipoRegistrador() != null){
						ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 30,medicion.getEquipoRegistrador().getCodEquRegistrador());
					}
					
					
					if(medicion.getEstadoMedicion() != null){
						if(medicion.getParametroMedido().getCodParMedido().equals("FA")){
							ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 31,estadoMedicionTemp);
						}else{
							ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 31,medicion.getEstadoMedicion().getCodEstMedicion()+"-"+ medicion.getEstadoMedicion().getDescripcion());
						}
					}
							
					if(medicion.getParametroMedido() != null){
						ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 32,medicion.getParametroMedido().getDescripcion());
					}
					
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 33,medicion.getDirUsuario());
					
					if(medicion.getParametroMedido().getCodParMedido().equals("FA")){
						ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 34,compensableTemp);
					}else{
						ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 34,medicion.getCompensable());
					}
					
					
					
					//ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 34,medicion.getCompensable());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 35,medicion.getSalidabt());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 36,medicion.getTensionSed());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 37,medicion.getDireccionSed());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 38,medicion.getTipoEnergia());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 39,medicion.getTipoMedicionEnergia ());
					//PERTUR
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 40,medicion.getNumIdentificadorPer());
					if(medicion.getPerEstMedicion() != null){
						ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 41,medicion.getPerEstMedicion().getCodEstMedicion()+"-"+ medicion.getPerEstMedicion().getDescripcion());
					}
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 42,medicion.getPreFlickerPer());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 43,medicion.getPreArmonicasPer());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 44,medicion.getPerMensaje());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 45,medicion.getParametroPer());
					
					if(medicion.getParametroMedido().getCodParMedido().equals("TP") || 
							medicion.getParametroMedido().getCodParMedido().equals("TE")){
						ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 46,intervaloTensionService.getMaxTension(medicion.getMedicionId()));
						ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 47,intervaloTensionService.getMinTension(medicion.getMedicionId()));
					}
					intRow++;
					
	
			}
			
			ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());

	    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
	    facesContext.responseComplete();	
				
		return null;
	}
	
	public String exportarTablaMedicionPendientes(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
	    
	    String rutaArchivoOut;
	    String rutaUp;
	    rutaUp = facesContext.getExternalContext().getRealPath("/reportes/Anexomedicion.xlsx");
	    String path = System.getProperty("uploads.folder");
	    String nombreArchivo;
	    String compensableTemp = null;
	    String estadoMedicionTemp = null;
	    
	    System.out.print(path + "\n");
	    
	    nombreArchivo = "medicion" + "_" + getCampanaMedicion().getDescripcion() + "_" + getCampanaMedicion().getPeriodoMedicion().getPeriodo() + "_"+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsx";
	    rutaArchivoOut = path + nombreArchivo;
	    	    
	    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);

		XSSFSheet objHssfSheet = null;
		
		try  {
		    
		    objHssfSheet = objHssfWorkbook.getSheetAt(0);
			
			int intRow = 2;
			int intCell = 0;
			
			for (Medicion medicion : medicionesPendientesComp) {
				
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell, medicion.getNumIdentificador());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 1,medicion.getNumSumMedicion());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 2,medicion.getSecuencia());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 3,medicion.getNomUsuario());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 4,medicion.getTipSuministro());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 5,medicion.getCodSed());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 6,medicion.getNumSumReemplazado());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 7,medicion.getTenEntregada());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 8,medicion.getUbiSumMedicion());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 9,medicion.getOpcTarifaria());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 10,medicion.getDescripcion());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 11,medicion.getTipServicio());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 12,medicion.getCodLinea());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 13,medicion.getCodSet());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 14,medicion.getCodAlimentador());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 15,medicion.getTelUsuario());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 16,medicion.getNomArchivo());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 17,medicion.getFacCorTension());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 18,medicion.getFacCorCorriente());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 19,medicion.getFecInstalacion());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 20,medicion.getFecRetiro());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 21,medicion.getPreFlicker());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 22,medicion.getPreArmonicas());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 23,medicion.getObservaciones());

					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 24,medicion.getCampanaMedicion().getDescripcion());
				
					if(medicion.getTipoMedicion() != null){
						ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 25,medicion.getTipoMedicion().getDescripcion());
					}
					if(medicion.getTipoPunto() != null){
						ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 26,medicion.getTipoPunto().getCodTipPunto()+"-" +medicion.getTipoPunto().getDescripcion());
					}
					if(medicion.getLocalidad() != null) {
						ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 27,medicion.getLocalidad().getCodLocalidad()+"-" + medicion.getLocalidad().getNombre());
					}
					if(medicion.getTipoAlimentacion() != null){
						ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 28,medicion.getTipoAlimentacion().getDescripcion());
					}
					if(medicion.getTipoTrabajo() != null){
						ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 29,medicion.getTipoTrabajo().getDescripcion());
					}
					if(medicion.getEquipoRegistrador() != null){
						ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 30,medicion.getEquipoRegistrador().getCodEquRegistrador());
					}
					
					
					if(medicion.getEstadoMedicion() != null){
						if(medicion.getParametroMedido().getCodParMedido().equals("FA")){
							ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 31,estadoMedicionTemp);
						}else{
							ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 31,medicion.getEstadoMedicion().getCodEstMedicion()+"-"+ medicion.getEstadoMedicion().getDescripcion());
						}
					}
							
					if(medicion.getParametroMedido() != null){
						ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 32,medicion.getParametroMedido().getDescripcion());
					}
					
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 33,medicion.getDirUsuario());
					
					if(medicion.getParametroMedido().getCodParMedido().equals("FA")){
						ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 34,compensableTemp);
					}else{
						ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 34,medicion.getCompensable());
					}
					
					
					
					//ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 34,medicion.getCompensable());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 35,medicion.getSalidabt());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 36,medicion.getTensionSed());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 37,medicion.getDireccionSed());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 38,medicion.getTipoEnergia());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 39,medicion.getTipoMedicionEnergia ());
					//PERTUR
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 40,medicion.getNumIdentificadorPer());
					if(medicion.getPerEstMedicion() != null){
						ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 41,medicion.getPerEstMedicion().getCodEstMedicion()+"-"+ medicion.getPerEstMedicion().getDescripcion());
					}
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 42,medicion.getPreFlickerPer ());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 43,medicion.getPreArmonicasPer ());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 44,medicion.getPerMensaje());
					ModificarExcel.setCellValue(objHssfSheet,intRow,intCell + 45,medicion.getParametroPer ());
					intRow++;
					
	
			}
			
			ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());

	    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
	    facesContext.responseComplete();	
				
		return null;
	}

	public String getNumSumFilter() {
		return numSumFilter;
	}

	public void setNumSumFilter(String numSumFilter) {
		this.numSumFilter = numSumFilter;
	}

	public String getNumSum1Filter() {
		return numSum1Filter;
	}

	public void setNumSum1Filter(String numSum1Filter) {
		this.numSum1Filter = numSum1Filter;

	}
	
	public void mostrarMensaje(String mensaje){
        FacesMessage fm = new FacesMessage(mensaje);

        fm.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, fm);
	}
	
	public String generarAllIntervalosSer(){
		String result;
		
		ConectaDb db = new ConectaDb("CALIDAD");
		Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        try {
        	cstmt = cn.prepareCall("{CALL CAL_OTROS_PACKAGE.GENERA_ALL_INTERVALOS_SER(?)}");
		   	cstmt.setLong(1, campanaMedicion.getCamMedicionId());
			
		   	System.out.println("Campana:" + campanaMedicion.getCamMedicionId());
		   	
/*		   	int ctos = cstmt.executeUpdate();
            if (ctos == 0) {
                result = "0 filas afectadas";
            }
*/
		   	
		   	cstmt.execute();
            cstmt.close(); 


        } catch (SQLException e) {
            result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
            
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
                result = e.getMessage();
                
    			setMensaje(e.getMessage()+":"+e.getCause());
    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                
            }
        }			
		
        return result = "/zonasegura/medicionListar?faces-redirect=true";
		
	}		
	public void generarReporteConsumosMT(){
		
		final FacesContext facesContext = FacesContext.getCurrentInstance();
		String rutaUp;
		String rutaArchivoOut;
		String path = System.getProperty("uploads.folder");
	    String nombreArchivo;
	    
	    rutaUp = facesContext.getExternalContext().getRealPath("/reportes/ReporteConsumosMT.xlsx");
	    
		String result;
		ConectaDb db = new ConectaDb("CALIDAD");
		Connection cn = db.getConnection();
        CallableStatement cstmt = null;
        
		String selectSql = null;
		Sql sql = new Sql("CALIDAD");
        

        try {
        	cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.GENERAR_REPORTE_CONSUMOS_MT(?)}");
		   	cstmt.setLong(1, campanaMedicion.getCamMedicionId());
			
            int ctos = cstmt.executeUpdate();;
            cstmt.close(); 

            if (ctos == 0) {
                result = "0 filas afectadas";
            }

        } catch (SQLException e) {
            result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
            
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
                result = e.getMessage();
                
    			setMensaje(e.getMessage()+":"+e.getCause());
    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                
            }
        }
        
		selectSql = "SELECT NUM_SUM_MEDICION, ENE_1, ENE_2, ENE_3, ENE_4, ENE_5, ENE_6, " + 
					"ENE_7, ENE_8, ENE_9, ENE_10, ENE_11, ENE_12 FROM CAL_REPORTE_MT";
		
		List<Object[]> objTabla;
		objTabla = sql.consulta(selectSql, false);
		
		nombreArchivo = "reporte_consumos_mt" + new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsx";
	    rutaArchivoOut = path + nombreArchivo;
	    
	    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);

		XSSFSheet objHssfSheet = null;
		
		objHssfSheet = objHssfWorkbook.getSheetAt(0);
		
		String selectSqlCabecera = "SELECT DISTINCT 'SUMINISTRO', PER_1, PER_2, PER_3, PER_4, PER_5, PER_6, " + 
				"PER_7, PER_8, PER_9, PER_10, PER_11, PER_12 FROM CAL_REPORTE_MT";
	
		Object[] objTablaCabecera;
		objTablaCabecera = sql.getFila(selectSqlCabecera);		
	    
	    if(objTabla != null) {
	    	int col = 0;
	    	int intRow = 1;
			int intCell = 0;

	    	for (Object[] objFila : objTabla) {
				intCell = 0;
				BigDecimal campoBigDecimal;
				String campoString;
				
				if(intRow == 1) {
		    		for (Object objectCabecera : objTablaCabecera) {
						try {
							campoString = (String) objectCabecera;
							ModificarExcel.setCellValue(objHssfSheet, 0, col, campoString);
							
						} catch (Exception e) {
							
							campoBigDecimal = (BigDecimal) objectCabecera;
							ModificarExcel.setCellValue(objHssfSheet, 0, col, campoBigDecimal);
						}	
						col++;
		    		}
				}
				for (Object object : objFila) {					
					try {
						campoString = (String) object;
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell, campoString);
						
					} catch (Exception e) {
						
						campoBigDecimal = (BigDecimal) object;
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell, campoBigDecimal);
					}			
												
					intCell++;
				}
				
				intRow++; 

			}
	    	
	    	ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
	    	
		    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());
		    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
		    facesContext.responseComplete();
	    }      
				
	}
}


