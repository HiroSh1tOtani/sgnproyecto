package pe.com.indra.calidad.mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

import pe.com.indra.calidad.entity.TipoParametro;
import pe.com.indra.calidad.service.TipoParametroService;

@ManagedBean(name="tipoParametroBean")
@SessionScoped
public class TipoParametroBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private TipoParametroService service=null;
	private List<TipoParametro> parametros=null;

	@PostConstruct
	public void init(){
		service=new TipoParametroService();
		parametros=service.getAllRows();
	}
	
	public List<TipoParametro> getParametros() {
		parametros=(List<TipoParametro>) service.getAllRows();
		return parametros;
	}

	public void setParametros(List<TipoParametro> parametros) {
		this.parametros = parametros;
	}
	
}
