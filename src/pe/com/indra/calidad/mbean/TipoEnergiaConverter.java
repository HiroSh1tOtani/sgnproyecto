package pe.com.indra.calidad.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.indra.calidad.entity.TipoEnergia;
import pe.com.indra.calidad.service.TipoEnergiaService;

@FacesConverter(value="tipoEnergiaConverter",forClass=TipoEnergiaConverter.class)

public class TipoEnergiaConverter implements Converter{

	@Override
	public TipoEnergia getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long tipEnergiaId=new Long(cadena);
		TipoEnergiaService service=new TipoEnergiaService();
		TipoEnergia tipoEnergia=new TipoEnergia();
		tipoEnergia=service.ReadById(tipEnergiaId);
		System.out.println(tipoEnergia.getCodTipEnergia()+":"+tipoEnergia.getDescripcion());
		return tipoEnergia;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			TipoEnergia tipoEnergia=(TipoEnergia) objeto;
			//System.out.println("objeto:"+new Long(tipoEnergia.getTipEnergiaId()).toString());
			return new Long(tipoEnergia.getTipEnergiaId()).toString();
		}
		return null;
	}


}