package pe.com.indra.calidad.mbean;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.HibernateException;
import org.richfaces.component.UIExtendedDataTable;
import org.richfaces.model.Filter;

import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.vnrgis.entity.VnrTsuministro;
import pe.com.calidad.vnrgis.service.VnrTsuministroService;
import pe.com.indra.calidad.entity.Empresa;
import pe.com.indra.calidad.entity.ParametroNorma;
import pe.com.indra.calidad.entity.Reporte;
import pe.com.indra.calidad.entity.TipoParametro;
import pe.com.indra.calidad.service.EmpresaService;
import pe.com.indra.calidad.service.ReporteService;
import pe.com.indra.calidad.service.TipoParametroService;
import pe.com.indra.calidad.util.ConectaDb;
import pe.com.indra.calidad.util.ModificarExcel;
import pe.com.indra.calidad.util.Sql;
import pe.com.indra.calidad.util.Utilidad;

@ManagedBean(name="reporteBean")
@SessionScoped
public class ReporteBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<Reporte> reportes;
	private List<Reporte> reportesVNR;
	private Reporte reporte;
	private List<SelectItem> itemsAmbito;
	private List<SelectItem> itemsTipoParametro;	
	private List<SelectItem> itemsFrecuencia;
	private List<SelectItem> itemsAnho;
	private List<SelectItem> itemsPeriodo;
	private Boolean seleccion;
	
	private String mensaje;
	
	private String tipParametroFilter;
	private List<SelectItem> itemsTipParametro;
	
	private String ambitoFilter;
	private List<SelectItem> itemsAmbitos;
	
	private boolean rango;
	private Date fecDesde;
	private Date fecHasta;
	
	private Collection<Object> selection;
	private List<Reporte> selectionItems = new ArrayList<Reporte>();	
	
	public Collection<Object> getSelection() {
		return selection;
	}

	public void setSelection(Collection<Object> selection) {
		this.selection = selection;
	}

	public List<Reporte> getSelectionItems() {
		return selectionItems;
	}

	public void setSelectionItems(List<Reporte> selectionItems) {
		this.selectionItems = selectionItems;
	}

	@PostConstruct 
	public void init(){
		System.out.println("se ejecuto PostConstruct "+new java.util.Date());
		reporte=new Reporte();
		reporte.setEstado("1");
		reporte.setSeleccion(false);
		setSeleccion(true);
		rango = false;
		ReporteService service=new ReporteService();
		
		/*
		String reporteId=Utilidad.getParametro("reporteId");
		if(reporteId==null){
		  reporte=new Reporte();
		  reporte.setEstado("1");
		}else{
			ReporteService service=new ReporteService();
			reporte=service.ReadById(new Long(reporteId));
		}
		*/
	}
	
	public List<Reporte> getReportes() {
		ReporteService service=new ReporteService();
		reportes=service.getAllRows();
		return reportes;
	}

	public void setReportes(List<Reporte> reportes) {
		this.reportes = reportes;
	}

	public List<Reporte> getReportesVNR() {
		ReporteService service=new ReporteService();
		reportes=service.getVNR();
		return reportes;
	}

	public void setReportesVNR(List<Reporte> reportesVNR) {
		this.reportesVNR = reportesVNR;
	}


	public Reporte getReporte() {
		
		return reporte;
	}

	public void setReporte(Reporte reporte) {
		this.reporte = reporte;
	}
	
	public List<SelectItem> getItemsAmbito() {
		itemsAmbito=new ArrayList<SelectItem>();
		itemsAmbito.add(new SelectItem("NTCSE","NTCSE"));
		itemsAmbito.add(new SelectItem("NTCSER","NTCSER"));
		itemsAmbito.add(new SelectItem("PROCEDIMIENTOS","PROCEDIMIENTOS"));
		itemsAmbito.add(new SelectItem("EXPORTACION","EXPORTACION"));
		return itemsAmbito;
	}

	public void setItemsAmbito(List<SelectItem> itemsAmbito) {
		this.itemsAmbito = itemsAmbito;
	}
	
	public List<SelectItem> getItemsFrecuencia() {
		itemsFrecuencia=new ArrayList<SelectItem>();
		itemsFrecuencia.add(new SelectItem("MENSUAL","MENSUAL"));
		itemsFrecuencia.add(new SelectItem("TRIMESTRAL","TRIMESTRAL"));
		itemsFrecuencia.add(new SelectItem("SEMESTRAL","SEMESTRAL"));
		itemsFrecuencia.add(new SelectItem("RANGO","RANGO DE FECHAS (SOLO PARA INTERRUPCIONES)"));
		
		return itemsFrecuencia;
	}

	public void setItemsFrecuencia(List<SelectItem> itemsFrecuencia) {
		this.itemsFrecuencia = itemsFrecuencia;
	}

		
	public List<SelectItem> getItemsAnho() {
		itemsAnho=new ArrayList<SelectItem>();
		itemsAnho.add(new SelectItem("2012","2012"));
		itemsAnho.add(new SelectItem("2013","2013"));
		itemsAnho.add(new SelectItem("2014","2014"));
		itemsAnho.add(new SelectItem("2015","2015"));
		itemsAnho.add(new SelectItem("2016","2016"));
		itemsAnho.add(new SelectItem("2017","2017"));
		itemsAnho.add(new SelectItem("2018","2018"));
		itemsAnho.add(new SelectItem("2019","2019"));
		itemsAnho.add(new SelectItem("2020","2020"));
		itemsAnho.add(new SelectItem("2021","2021"));
		itemsAnho.add(new SelectItem("2022","2022"));
		itemsAnho.add(new SelectItem("2023","2023"));
		itemsAnho.add(new SelectItem("2024","2024"));
		itemsAnho.add(new SelectItem("2025","2025"));
		
		return itemsAnho;
	}

	public void setItemsAnho(List<SelectItem> itemsAnho) {
		this.itemsAnho = itemsAnho;
	}

	public List<SelectItem> getItemsPeriodo() {
		itemsPeriodo=new ArrayList<SelectItem>();
		itemsPeriodo.add(new SelectItem("01","01"));
		itemsPeriodo.add(new SelectItem("02","02"));
		itemsPeriodo.add(new SelectItem("03","03"));
		itemsPeriodo.add(new SelectItem("04","04"));
		itemsPeriodo.add(new SelectItem("05","05"));
		itemsPeriodo.add(new SelectItem("06","06"));
		itemsPeriodo.add(new SelectItem("07","07"));
		itemsPeriodo.add(new SelectItem("08","08"));
		itemsPeriodo.add(new SelectItem("09","09"));
		itemsPeriodo.add(new SelectItem("10","10"));
		itemsPeriodo.add(new SelectItem("11","11"));
		itemsPeriodo.add(new SelectItem("12","12"));
		itemsPeriodo.add(new SelectItem("T1","T1"));
		itemsPeriodo.add(new SelectItem("T2","T2"));
		itemsPeriodo.add(new SelectItem("T3","T3"));
		itemsPeriodo.add(new SelectItem("T4","T4"));
		itemsPeriodo.add(new SelectItem("S1","S1"));
		itemsPeriodo.add(new SelectItem("S2","S2"));
		
		return itemsPeriodo;
	}

	public void setItemsPeriodo(List<SelectItem> itemsPeriodo) {
		this.itemsPeriodo = itemsPeriodo;
	}

	public List<SelectItem> getItemsTipoParametro() {
		TipoParametroService service=new TipoParametroService();
		itemsTipoParametro=new ArrayList<SelectItem>();
		for(TipoParametro tp:service.getAllRows()){
			itemsTipoParametro.add(new SelectItem(tp,tp.getCodTipParametro()+"-"+tp.getDescripcion()));
		}
		return itemsTipoParametro;
	}

	public void setItemsTipoParametro(List<SelectItem> itemsTipoParametro) {
		this.itemsTipoParametro = itemsTipoParametro;
	}

	
	public boolean isRango() {
		return rango;
	}

	public void setRango(boolean rango) {
		this.rango = rango;
	}

	public Date getFecDesde() {
		return fecDesde;
	}

	public void setFecDesde(Date fecDesde) {
		this.fecDesde = fecDesde;
	}

	public Date getFecHasta() {
		return fecHasta;
	}

	public void setFecHasta(Date fecHasta) {
		this.fecHasta = fecHasta;
	}

	public String prepararCrear(){
		//Esto funciona siempre que el bean tenga ambito de sesion 
		setReporte(new Reporte());
		getReporte().setEstado("1");
		return "/zonasegura/reporteCrear?faces-redirect=true";
	}
	
	public String prepararEditar(){
		//TODO: Por implementar carga de la instancia
		//System.out.println("reporteId:"+Utilidad.getParametro("reporteId"));
		//cargarReporteActual();
		//return "/zonasegura/reporteEditar?faces-redirect=true";
		
		try {
			cargarReporteActual();
			return "/zonasegura/reporteEditar?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/reporteListar?faces-redirect=true";
		}
	}

	public String prepararVer(){
		//TODO: Por implementar carga de la instancia
		//System.out.println("reporteId:"+Utilidad.getParametro("reporteId"));
		//cargarReporteActual();
		//return "/zonasegura/reporteVer?faces-redirect=true";
		
		try {
			cargarReporteActual();
			return "/zonasegura/reporteVer?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/reporteListar?faces-redirect=true";
		}
	}
	
	public String salvar(){
		//ReporteService service=new ReporteService();
		//service.create(reporte);
		//System.out.println("Se salvo un reporte con exito.");
		//return "/zonasegura/reporteListar?faces-redirect=true";
		
		ReporteService service=new ReporteService();
		
		try {
			
			if(reporte.getFrecuencia().equals("MENSUAL") &&
				(reporte.getPeriodo().substring(0,1).equals("T") ||
				reporte.getPeriodo().substring(0,1).equals("S"))){
				
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Si la frecuencia es mensual, seleccione un mes.", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				
				return "/zonasegura/reporteEditar?faces-redirect=true";
			}
			
			if(reporte.getFrecuencia().equals("TRIMESTRAL") &&
				!reporte.getPeriodo().substring(0,1).equals("T")){
				
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Si la frecuencia es trimestral, seleccione un trimestre.", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				
				return "/zonasegura/reporteEditar?faces-redirect=true";
			}
			
			if(reporte.getFrecuencia().equals("SEMESTRAL") &&
					!reporte.getPeriodo().substring(0,1).equals("S")){
					
					FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Si la frecuencia es semestral, seleccione un semestre.", getMensaje()));
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					
					return "/zonasegura/reporteEditar?faces-redirect=true";
			}
			
			EmpresaService empresaService = new EmpresaService();
			Empresa empresa = null;
			
			for(Empresa item:empresaService.getAllRows()){
				empresa = item;
			}
			
			String nombre;
			if(!reporte.getFrecuencia().equals("RANGO")) {
				nombre = empresa.getEmpresa() + "A" + reporte.getAnho().substring(2) + reporte.getPeriodo() + "." + reporte.getCodReporte().substring(0,3);
			} else {
				nombre = empresa.getEmpresa() + "Axxxx." + reporte.getCodReporte().substring(0,3);
			}
			
			reporte.setArchivo(nombre);
			
			service.create(reporte);
			
			return "/zonasegura/reporteListar?faces-redirect=true";
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			return "/zonasegura/reporteCrear?faces-redirect=true";

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			return "/zonasegura/reporteCrear?faces-redirect=true";
		}
		
	}
	
	public String actualizar(){
		//ReporteService service=new ReporteService();
		//service.update(reporte);
		//System.out.println("Se actualizo un reporte con exito.");
		//return "/zonasegura/reporteListar?faces-redirect=true";
		
		ReporteService service=new ReporteService();
		
		try {
			
			if(reporte.getFrecuencia().equals("MENSUAL") &&
				(reporte.getPeriodo().substring(0,1).equals("T") ||
				reporte.getPeriodo().substring(0,1).equals("S"))){
				
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Si la frecuencia es mensual, seleccione un mes.", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				
				return "/zonasegura/reporteEditar?faces-redirect=true";
			}
			
			if(reporte.getFrecuencia().equals("TRIMESTRAL") &&
				!reporte.getPeriodo().substring(0,1).equals("T")){
				
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Si la frecuencia es trimestral, seleccione un trimestre.", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				
				return "/zonasegura/reporteEditar?faces-redirect=true";
			}
			
			if(reporte.getFrecuencia().equals("SEMESTRAL") &&
					!reporte.getPeriodo().substring(0,1).equals("S")){
					
					FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Si la frecuencia es semestral, seleccione un semestre.", getMensaje()));
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					
					return "/zonasegura/reporteEditar?faces-redirect=true";
			}
			
			EmpresaService empresaService = new EmpresaService();
			Empresa empresa = null;
			
			for(Empresa item:empresaService.getAllRows()){
				empresa = item;
			}
			
			String nombre;
			if(!reporte.getFrecuencia().equals("RANGO")) {
				nombre = empresa.getEmpresa() + "A" + reporte.getAnho().substring(2) + reporte.getPeriodo() + "." + reporte.getCodReporte().substring(0,3);
			} else {
				nombre = empresa.getEmpresa() + "Axxxx." + reporte.getCodReporte().substring(0,3);
			}
			reporte.setArchivo(nombre);
			
			service.update(reporte);
			
			return "/zonasegura/reporteListar?faces-redirect=true";
			
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

			return "/zonasegura/reporteEditar?faces-redirect=true";
			
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			return "/zonasegura/reporteEditar?faces-redirect=true";
		}
		
	}
	
	public String eliminar(){
		String reporteId=Utilidad.getParametro("reporteId");
		ReporteService service=new ReporteService();
		//reporte=service.ReadById(new Long(reporteId));
		//reporte.setEstado("0");
		//service.delete(reporte.getParNormaId());
		//System.out.println("Se elimino un reporte con exito.");
		//return "/zonasegura/reporteListar?faces-redirect=true";
		
		try {
			reporte=service.ReadById(new Long(reporteId));
			reporte.setEstado("0");
			//service.delete(reporte.getParNormaId());
			service.delete(reporte.getReporteId());
		} catch (HibernateException e) {
			
			
			System.out.println(e.getMessage()+":"+e.getCause());
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			
		} catch (Exception e) {
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/reporteListar?faces-redirect=true";
	}
	
	public void cargarReporteActual(){
		String reporteId=Utilidad.getParametro("reporteId");
		ReporteService service=new ReporteService();
		//reporte=service.ReadById(new Long(reporteId));
		
		try {
			reporte=service.ReadById(new Long(reporteId));
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

			throw new HibernateException("No se puede cargar Reporte.", e);
			
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public Boolean getSeleccion() {
		return seleccion;
	}

	public void setSeleccion(Boolean seleccion) {
		this.seleccion = seleccion;
	}
	
	public void generarReportes() {
		
		String rutaArchivoOut;
		String path = System.getProperty("uploads.folder");
	    String nombreArchivo;
	    	
		String result;
		Sql sql = new Sql("CALIDAD");
		
		Reporte item_reporte;
		ReporteService reporteService = new ReporteService();
		for (Reporte reporte : selectionItems) {
			item_reporte = reporteService.ReadById(reporte.getReporteId());
			Clob clb = item_reporte.getConsultaSql();
			String selectSql = null;
			List<Object[]> objTabla = null;
			
			StringBuffer str = new StringBuffer();
			String strng;
			           			 
			BufferedReader bufferRead = null;
			try {
				bufferRead = new BufferedReader(clb.getCharacterStream());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				while ((strng=bufferRead .readLine())!=null)
				    str.append(strng);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			selectSql = str.toString();

			
//			if(!item_reporte.getTipoParametro().getCodTipParametro().equals("EE")){
			if(!item_reporte.getAmbito().equals("EXPORTACION")){				
				if(item_reporte.getFrecuencia().equals("RANGO")) {
					DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
					
					if(fecDesde != null && fecHasta != null ) {
					
						String dateDesde = dateFormat.format(fecDesde);
						String dateHasta = dateFormat.format(fecHasta);
						
						selectSql = selectSql + " AND TO_DATE(TO_CHAR(I.FEC_INI_PROGRAMADA,'DD/MM/YYYY'),'DD/MM/YYYY') >= TO_DATE('" + dateDesde + "','DD/MM/YYYY') AND TO_DATE(TO_CHAR(I.FEC_INI_PROGRAMADA,'DD/MM/YYYY'),'DD/MM/YYYY') <= TO_DATE('" + dateHasta + "','DD/MM/YYYY')";
					} else {
						return;
					}
				}
				
				if(item_reporte.getFrecuencia().equals("MENSUAL")) {
					selectSql = selectSql + " AND P.ANO = '" + item_reporte.getAnho() + "'" +
											" AND P.PERIODO = '" + item_reporte.getPeriodo() + "'";
					
				}
				
				if(item_reporte.getFrecuencia().equals("SEMESTRAL")) {
					selectSql = selectSql + " AND P.ANO = '" + item_reporte.getAnho() + "'" +
											" AND P.SEMESTRE = '" + item_reporte.getPeriodo() + "'";
				}
				
				if(item_reporte.getFrecuencia().equals("TRIMESTRAL") &&
						item_reporte.getPeriodo().equals("T1")) {
					selectSql = selectSql + " AND P.ANO = '" + item_reporte.getAnho() + "'" +
											" AND P.PERIODO IN ('01','02','03')";
				}
				
				if(item_reporte.getFrecuencia().equals("TRIMESTRAL") &&
						item_reporte.getPeriodo().equals("T2")) {
					selectSql = selectSql + " AND P.ANO = '" + item_reporte.getAnho() + "'" +
											" AND P.PERIODO IN ('04','05','06')";
				}
				
				if(item_reporte.getFrecuencia().equals("TRIMESTRAL") &&
						item_reporte.getPeriodo().equals("T3")) {
					selectSql = selectSql + " AND P.ANO = '" + item_reporte.getAnho() + "'" +
											" AND P.PERIODO IN ('07','08','09')";
				}
				
				if(item_reporte.getFrecuencia().equals("TRIMESTRAL") &&
						item_reporte.getPeriodo().equals("T4")) {
					selectSql = selectSql + " AND P.ANO = '" + item_reporte.getAnho() + "'" +
											" AND P.PERIODO IN ('10','11','12')";
				}
				
				objTabla = sql.consulta(selectSql, false);
			}else {
				
				String entidad = null;
				if(item_reporte.getCodReporte().subSequence(0, 7).equals("VNR_VER") ||
						item_reporte.getCodReporte().subSequence(0, 10).equals("PROC228_VE")){
					if(item_reporte.getCodReporte().equals("VNR_VERTRAMOMT") ||
							item_reporte.getCodReporte().equals("PROC228_VERTRAMOMT")){
						entidad = "VERTRAMOMT";
					}
					if(item_reporte.getCodReporte().equals("VNR_VERTRAMOBT")){
						entidad = "VERTRAMOBT";
					}
					if(item_reporte.getCodReporte().equals("VNR_VERACOMETIDA")){
						entidad = "VERACOMETIDA";
					}
					if(item_reporte.getCodReporte().equals("VNR_VERMANZANA")){
						entidad = "VERMANZANA";
					}
					if(item_reporte.getCodReporte().equals("VNR_VERZONACONCESION")){
						entidad = "VERZONACONCESION";
					}	
					if(item_reporte.getCodReporte().equals("VNR_VERPARQUE")){
						entidad = "VERPARQUE";
					}
					if(item_reporte.getCodReporte().equals("VNR_VERZONAMONUMENTAL")){
						entidad = "VERZONAMONUMENTAL";
					}
					if(item_reporte.getCodReporte().equals("VNR_VERPREDIO")){
						entidad = "VERPREDIO";
					}
					if(item_reporte.getCodReporte().equals("VNR_VERTRAMOVIA")){
						entidad = "VERTRAMOVIA";
					}					

					ConectaDb db = new ConectaDb("CALIDAD");
					Connection cn = db.getConnection();
			        CallableStatement cstmt = null;  
			        try {
			        	cstmt = cn.prepareCall("{CALL CAL_UTILS.GENERA_VERTICES(?,?,?)}");
					   	cstmt.setString(1, entidad);
					   	cstmt.setString(2, item_reporte.getAnho());
					   	cstmt.setString(3, item_reporte.getPeriodo());
						
			            int ctos = cstmt.executeUpdate();;
			            cstmt.close(); 

			            if (ctos == 0) {
			                result = "0 filas afectadas";
			            }

			        } catch (SQLException e) {
			            result = e.getMessage();
						setMensaje(e.getMessage()+":"+e.getCause());			
						FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
						FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			        } finally {
			            try {
			                cn.close();
			            } catch (SQLException e) {
			                result = e.getMessage();
			    			setMensaje(e.getMessage()+":"+e.getCause());			
			    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			            }
			        }					  
				}
				if(item_reporte.getCodReporte().subSequence(0, 5).equals("VNR_T")){
					
					selectSql = selectSql + " And to_char( To_Date(Nvl(Installation_Date, Creation_Date), 'YYYYMMDDHH24MISS') , 'YYYYMM') <= '" + item_reporte.getAnho() + item_reporte.getPeriodo() + "'";  
					/*
					selectSql = selectSql + " AND TRIM(TO_CHAR(EXTRACT(YEAR FROM TO_DATE(NVL (INSTALLATION_DATE,CREATION_DATE),'YYYYMMDDHH24MISS')),'9999')) || " + 
											" TRIM(TO_CHAR(EXTRACT(MONTH FROM TO_DATE(NVL (INSTALLATION_DATE,CREATION_DATE),'YYYYMMDDHH24MISS')),'09')) <= '" + item_reporte.getAnho() + item_reporte.getPeriodo() + "'";
					*/
				}
				
				if(item_reporte.getTipoParametro().getCodTipParametro().equals("A1")){
					objTabla = sql.consulta(selectSql, false);
				}else{
					/*
					if(item_reporte.getCodReporte().equals("VNR_TSUMINISTRO")){
						/*
						ConectaDb db = new ConectaDb("CALIDAD");
						Connection cn = db.getConnection();
				        CallableStatement cstmt = null;  
				        try {
				        	cstmt = cn.prepareCall("{CALL CAL_UTILS.INSERTA_SUNINISTRO(?)}");
						   	cstmt.setString(1, selectSql);
							
				            int ctos = cstmt.executeUpdate();;
				            cstmt.close(); 
	
				            if (ctos == 0) {
				                result = "0 filas afectadas";
				            }
	
				        } catch (SQLException e) {
				            result = e.getMessage();
							setMensaje(e.getMessage()+":"+e.getCause());			
							FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
							FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				        } finally {
				            try {
				                cn.close();
				            } catch (SQLException e) {
				                result = e.getMessage();
				    			setMensaje(e.getMessage()+":"+e.getCause());			
				    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
				    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				            }
				        }
				        
				        objTabla = generaReporteService(item_reporte.getCodReporte());
				       
					} else {

						objTabla = sql.consulta(selectSql, true);
					}
					*/
					objTabla = sql.consulta(selectSql, true);
				}
				
				
			}
			
			System.out.println(item_reporte.getCodReporte() + ":" + selectSql);			
							
			if(item_reporte.getCodReporte().substring(0, 3).equals("VNR")){
				item_reporte.setArchivo(item_reporte.getCodReporte());
			} 
			
			if(item_reporte.getTipoParametro().getCodTipParametro().equals("A1")) {
				item_reporte.setArchivo(item_reporte.getCodReporte());
			} 
			
			nombreArchivo = item_reporte.getArchivo() + new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".txt";
		    rutaArchivoOut = path + nombreArchivo;
		    
		    if(objTabla != null) {
		    	
		    	generarSalida(item_reporte, rutaArchivoOut, nombreArchivo, objTabla);
		    }
	    	
		}
		
	}
	

	
	public List<Object[]> generaReporteService(String codigo){
		List<Object[]> objTabla = new ArrayList<Object[]>();
		
		if(codigo.equals("VNR_TSUMINISTRO")){
			VnrTsuministroService vnrTsuministroService = new VnrTsuministroService();
			
			for (VnrTsuministro vnrTsuministro : vnrTsuministroService.getAllRows()) {
				Object[] fila = new Object[6];//numero de columnas
				fila[0] = vnrTsuministro.getSlabel();
				fila[1] = vnrTsuministro.getPlabel();
				fila[2] = vnrTsuministro.getEstado();
				fila[3] = vnrTsuministro.getFechaservicio();
				fila[4] = vnrTsuministro.getFecharetiro();
				fila[5] = vnrTsuministro.getSed();
				
				objTabla.add(fila);
			}
		}
		
		return objTabla;
	}
	
	public void generarReportesExcel() {
		final FacesContext facesContext = FacesContext.getCurrentInstance();
		String rutaUp;
		String rutaArchivoOut;
		String path = System.getProperty("uploads.folder");
	    String nombreArchivo;
	    
	    rutaUp = facesContext.getExternalContext().getRealPath("/reportes/ReporteNorma.xlsx");
	    
 	    	
		String result;
		Sql sql = new Sql("CALIDAD");
		
		Reporte item_reporte;
		ReporteService reporteService = new ReporteService();
			
		for (Reporte reporte : selectionItems) {
			item_reporte = reporteService.ReadById(reporte.getReporteId());
			Clob clb = item_reporte.getConsultaSql();
			String selectSql = null;
			String selectSql1 = null;
			String selectSql2 = null;
			String campo = null;
			
			if((reporte.getCodReporte().substring(0, 3).equals("APE"))&&(reporte.getAmbito().equals("NTCSE"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSE_APE.xlsx");
			}
			
			
			if((reporte.getCodReporte().substring(0, 3).equals("ATE")) &&(reporte.getAmbito().equals("NTCSE"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSE_ATE.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("BAR")) &&(reporte.getAmbito().equals("NTCSE"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSE_BAR.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("BFL")) &&(reporte.getAmbito().equals("NTCSE"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSE_BFL.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("CAP")) &&(reporte.getAmbito().equals("NTCSE"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSE_CAP.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("CCP")) &&(reporte.getAmbito().equals("NTCSE"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSE_CCP.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("CCT")) &&(reporte.getAmbito().equals("NTCSE"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSE_CCT.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("CI1")) &&(reporte.getAmbito().equals("NTCSE"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSE_CI1.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("CI2")) &&(reporte.getAmbito().equals("NTCSE"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSE_CI2.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("CTE")) &&(reporte.getAmbito().equals("NTCSE"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSE_CTE.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("FAP")) &&(reporte.getAmbito().equals("NTCSE"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSE_FAP.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals( "FTE")) &&(reporte.getAmbito().equals("NTCSE"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSE_FTE.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("MAP")) &&(reporte.getAmbito().equals("NTCSE"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSE_MAP.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("MPE")) &&(reporte.getAmbito().equals("NTCSE"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSE_MPE.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("MTE")) &&(reporte.getAmbito().equals("NTCSE"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSE_MTE.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("PIN")) &&(reporte.getAmbito().equals( "NTCSE"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSE_PIN.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("RAP")) &&(reporte.getAmbito().equals( "NTCSE"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSE_RAP.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("RDI")) &&(reporte.getAmbito().equals("NTCSE"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSE_RDI.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("RIM")) &&(reporte.getAmbito().equals("NTCSE"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSE_RIM.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("RIN")) &&(reporte.getAmbito().equals( "NTCSE"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSE_RIN.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("ATR")) &&(reporte.getAmbito().equals("NTCSER"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSER_ATR.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("CCP")) &&(reporte.getAmbito().equals( "NTCSER"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSER_CCP.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("CCT")) &&(reporte.getAmbito().equals( "NTCSER"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSER_CCT.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("CR1")) &&(reporte.getAmbito().equals( "NTCSER"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSER_CR1.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("CR2")) &&(reporte.getAmbito().equals( "NTCSER"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSER_CR2.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("CR3")) &&(reporte.getAmbito().equals( "NTCSER"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSER_CR3.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("CTR")) &&(reporte.getAmbito().equals( "NTCSER"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSER_CTR.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("FTR")) &&(reporte.getAmbito().equals( "NTCSER"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSER_FTR.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("MTR")) &&(reporte.getAmbito().equals( "NTCSER"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSER_MTR.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("PIN")) &&(reporte.getAmbito().equals( "NTCSER"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSER_PIN.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals( "RDI")) &&(reporte.getAmbito().equals( "NTCSER"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSER_RDI.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("RIN")) &&(reporte.getAmbito().equals( "NTCSER"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/NTCSER_RIN.xlsx");
			}

			if((reporte.getCodReporte().substring(0, 3).equals("RHD")) &&(reporte.getAmbito().equals( "PROCEDIMIENTOS"))){
				rutaUp = facesContext.getExternalContext().getRealPath("/reportes/PROC_078_RHD.xlsx");
			}			
			
			StringBuffer str = new StringBuffer();
			String strng;
			           			 
			BufferedReader bufferRead = null;
			try {
				bufferRead = new BufferedReader(clb.getCharacterStream());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				while ((strng=bufferRead .readLine())!=null)
				    str.append(strng);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			selectSql = str.toString();
			List<Object[]> objTabla;
			
//			if(!item_reporte.getTipoParametro().getCodTipParametro().equals("EE")){			
			if(!item_reporte.getAmbito().equals("EXPORTACION")){		
				if(item_reporte.getFrecuencia().equals("RANGO")) {
					DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
					
					if(fecDesde != null && fecHasta != null ) {
					
						String dateDesde = dateFormat.format(fecDesde);
						String dateHasta = dateFormat.format(fecHasta);
						
						selectSql = selectSql + " AND TO_DATE(TO_CHAR(I.FEC_INI_PROGRAMADA,'DD/MM/YYYY'),'DD/MM/YYYY') >= TO_DATE('" + dateDesde + "','DD/MM/YYYY') AND TO_DATE(TO_CHAR(I.FEC_INI_PROGRAMADA,'DD/MM/YYYY'),'DD/MM/YYYY') <= TO_DATE('" + dateHasta + "','DD/MM/YYYY')";
					} else {
						return;
					}
				}
				
				if(item_reporte.getFrecuencia().equals("MENSUAL")) {
					selectSql = selectSql + " AND P.ANO = '" + item_reporte.getAnho() + "'" +
											" AND P.PERIODO = '" + item_reporte.getPeriodo() + "'";
					
				}
				
				if(item_reporte.getFrecuencia().equals("SEMESTRAL")) {
					selectSql = selectSql + " AND P.ANO = '" + item_reporte.getAnho() + "'" +
											" AND P.SEMESTRE = '" + item_reporte.getPeriodo() + "'";
				}
				
				if(item_reporte.getFrecuencia().equals("TRIMESTRAL") &&
						item_reporte.getPeriodo().equals("T1")) {
					selectSql = selectSql + " AND P.ANO = '" + item_reporte.getAnho() + "'" +
											" AND P.PERIODO IN ('01','02','03')";
				}
				
				if(item_reporte.getFrecuencia().equals("TRIMESTRAL") &&
						item_reporte.getPeriodo().equals("T2")) {
					selectSql = selectSql + " AND P.ANO = '" + item_reporte.getAnho() + "'" +
											" AND P.PERIODO IN ('04','05','06')";
				}
				
				if(item_reporte.getFrecuencia().equals("TRIMESTRAL") &&
						item_reporte.getPeriodo().equals("T3")) {
					selectSql = selectSql + " AND P.ANO = '" + item_reporte.getAnho() + "'" +
											" AND P.PERIODO IN ('07','08','09')";
				}
				
				if(item_reporte.getFrecuencia().equals("TRIMESTRAL") &&
						item_reporte.getPeriodo().equals("T4")) {
					selectSql = selectSql + " AND P.ANO = '" + item_reporte.getAnho() + "'" +
											" AND P.PERIODO IN ('10','11','12')";
				}
				
				System.out.println(item_reporte.getCodReporte() + ":" + selectSql);
				
				selectSql = selectSql.replaceAll("\\|\\|", ",");
				selectSql = selectSql.replaceAll("\\?", "");
				
				/*Pattern p = Pattern.compile("?");
		        // get a matcher object
		        Matcher m = p.matcher(selectSql);
		        selectSql = m.replaceAll("");
				*/
				
				
				while(selectSql.indexOf("FN_NFORMATO") >=0 ){
				
					selectSql1 = selectSql.substring(0,selectSql.indexOf("FN_NFORMATO"));
					selectSql2 = selectSql.substring(selectSql.indexOf("FN_NFORMATO"));
					selectSql2 = selectSql2.substring(selectSql2.indexOf("(") + 1);
					
					campo = selectSql2.substring(0,selectSql2.indexOf(","));
					
					selectSql2 = selectSql2.substring(selectSql2.indexOf(",") + 1);
					selectSql2 = selectSql2.substring(selectSql2.indexOf(")") + 1);
					selectSql = selectSql1 + campo + selectSql2;
				}
				/*
				selectSql2 = selectSql.substring(selectSql.indexOf("FROM"));
				
				selectSql1 = "SELECT * ";
				selectSql = selectSql1 + selectSql2;
				*/
				
				selectSql = selectSql.replaceFirst("CADENA", "*");

				objTabla = sql.consulta(selectSql, false);
			}else {
				String entidad = null;
				if(item_reporte.getCodReporte().subSequence(0, 7).equals("VNR_VER") ||
						item_reporte.getCodReporte().subSequence(0, 10).equals("PROC228_VE")){
					if(item_reporte.getCodReporte().equals("VNR_VERTRAMOMT") ||
							item_reporte.getCodReporte().equals("PROC228_VERTRAMOMT")){
						entidad = "VERTRAMOMT";
					}
					if(item_reporte.getCodReporte().equals("VNR_VERTRAMOBT")){
						entidad = "VERTRAMOBT";
					}
					if(item_reporte.getCodReporte().equals("VNR_VERACOMETIDA")){
						entidad = "VERACOMETIDA";
					}
					if(item_reporte.getCodReporte().equals("VNR_VERMANZANA")){
						entidad = "VERMANZANA";
					}
					if(item_reporte.getCodReporte().equals("VNR_VERZONACONCESION")){
						entidad = "VERZONACONCESION";
					}	
					if(item_reporte.getCodReporte().equals("VNR_VERPARQUE")){
						entidad = "VERPARQUE";
					}
					if(item_reporte.getCodReporte().equals("VNR_VERZONAMONUMENTAL")){
						entidad = "VERZONAMONUMENTAL";
					}
					if(item_reporte.getCodReporte().equals("VNR_VERPREDIO")){
						entidad = "VERPREDIO";
					}
					if(item_reporte.getCodReporte().equals("VNR_VERTRAMOVIA")){
						entidad = "VERTRAMOVIA";
					}					

					ConectaDb db = new ConectaDb("CALIDAD");
					Connection cn = db.getConnection();
			        CallableStatement cstmt = null;  
			        try {
			        	cstmt = cn.prepareCall("{CALL CAL_UTILS.GENERA_VERTICES(?,?,?)}");
					   	cstmt.setString(1, entidad);
					   	cstmt.setString(2, item_reporte.getAnho());
					   	cstmt.setString(3, item_reporte.getPeriodo());
						
			            int ctos = cstmt.executeUpdate();;
			            cstmt.close(); 

			            if (ctos == 0) {
			                result = "0 filas afectadas";
			            }

			        } catch (SQLException e) {
			            result = e.getMessage();
						setMensaje(e.getMessage()+":"+e.getCause());			
						FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
						FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			        } finally {
			            try {
			                cn.close();
			            } catch (SQLException e) {
			                result = e.getMessage();
			    			setMensaje(e.getMessage()+":"+e.getCause());			
			    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			            }
			        }					  
				}				
				
				if(item_reporte.getCodReporte().subSequence(0, 5).equals("VNR_T")){
					selectSql = selectSql + " AND TRIM(TO_CHAR(EXTRACT(YEAR FROM TO_DATE(NVL (INSTALLATION_DATE,CREATION_DATE),'YYYYMMDDHH24MISS')),'9999')) || " + 
											" TRIM(TO_CHAR(EXTRACT(MONTH FROM TO_DATE(NVL (INSTALLATION_DATE,CREATION_DATE),'YYYYMMDDHH24MISS')),'09')) <= '" + item_reporte.getAnho() + item_reporte.getPeriodo() + "'";
				}
				
				if(item_reporte.getTipoParametro().getCodTipParametro().equals("A1")){
					selectSql = selectSql.replaceAll("\\|\\|", ",");
					selectSql = selectSql.replaceAll("\\?", "");					
					
					while(selectSql.indexOf("FN_NFORMATO") >=0 ){
						
						selectSql1 = selectSql.substring(0,selectSql.indexOf("FN_NFORMATO"));
						selectSql2 = selectSql.substring(selectSql.indexOf("FN_NFORMATO"));
						selectSql2 = selectSql2.substring(selectSql2.indexOf("(") + 1);
						
						campo = selectSql2.substring(0,selectSql2.indexOf(","));
						
						selectSql2 = selectSql2.substring(selectSql2.indexOf(",") + 1);
						selectSql2 = selectSql2.substring(selectSql2.indexOf(")") + 1);
						selectSql = selectSql1 + campo + selectSql2;
					}
					/*
					selectSql2 = selectSql.substring(selectSql.indexOf("FROM"));
					
					selectSql1 = "SELECT * ";
					selectSql = selectSql1 + selectSql2;
					*/
					
					selectSql = selectSql.replaceFirst("CADENA", "*");
					
					objTabla = sql.consulta(selectSql, false);
				}else{
					objTabla = sql.consulta(selectSql, true);
				}				
				//objTabla = sql.consulta(selectSql, true);
			}			
			
			System.out.println(selectSql);
			
			if(item_reporte.getCodReporte().substring(0, 3).equals("VNR")){
				item_reporte.setArchivo(item_reporte.getCodReporte());
			} 
			
			if(item_reporte.getTipoParametro().getCodTipParametro().equals("A1")){
				item_reporte.setArchivo(item_reporte.getCodReporte());
			} 
							
			nombreArchivo = item_reporte.getArchivo() + new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsx";
		    rutaArchivoOut = path + nombreArchivo;
		    
		    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);

			XSSFSheet objHssfSheet = null;
			
			objHssfSheet = objHssfWorkbook.getSheetAt(0);
		    
		    if(objTabla != null) {
		    	String fila;
		    	int intRow = 1;
				int intCell = 0;
		    	for (Object[] objFila : objTabla) {
					
					intCell = 0;
					BigDecimal campoBigDecimal;
					String campoString;
					for (Object object : objFila) {
						
						try {
							campoString = (String) object;
							ModificarExcel.setCellValue(objHssfSheet, intRow, intCell, campoString);
							
						} catch (Exception e) {
							
							campoBigDecimal = (BigDecimal) object;
							ModificarExcel.setCellValue(objHssfSheet, intRow, intCell, campoBigDecimal);
						}
						//NumberFormatException
						
						
						intCell++;
					}

					intRow++;


				}
		    	
		    	ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
		    	
			    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());
			    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
			    facesContext.responseComplete();
		    }
	    	
		}
		
	}
	

	
	public void generarSalida(Reporte item_reporte, String rutaArchivoOut, String nombreArchivo, List<Object[]> objTabla){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
		//FacesContext facesContext = FacesContext.getCurrentInstance();
	    String fila;
	    FileWriter fileWriter = null;
	    BufferedWriter writer = null;
	    
	    try {
	    	
	        fileWriter = new FileWriter(rutaArchivoOut);
	
	    } catch (IOException e) {
	    	
	    }
	    
	    writer = new BufferedWriter(fileWriter);
	    
	    try {
		    writer.write(item_reporte.getArchivo());
	        writer.newLine();
	    } catch (IOException e) {
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo generar el archivo:" + item_reporte.getArchivo(), getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		    
	    } finally {
	    	
	    }
        
        String campoString;
        BigDecimal campoBigDecimal;
		for (Object[] objFila : objTabla) {
			fila = "";
			
			for (Object object : objFila) {
				
				campoString = "";
				
				if(object != null){
					try {
						campoString = (String) object;
	
						
					} catch (Exception e) {
						
						campoBigDecimal = (BigDecimal) object;
						campoString = String.valueOf(campoBigDecimal.doubleValue());
						
						if(campoString.length() >= 3){
							if(campoString.substring(campoString.length() - 2).equals(".0")) {
								campoString = campoString.substring(0,campoString.length() - 2);
							}
						}
					}
				}
				
				if(objFila.length == 1) {
					fila = campoString;
				}else {
				
					fila = fila + campoString + "\t";
				}
			}
			
			
			try {
				writer.write(fila);
	            writer.newLine();
				System.out.println(item_reporte.getCodReporte() + ":" + fila);
			
			} catch (IOException e) {
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo generar el archivo:" + item_reporte.getArchivo(), getMensaje()));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			    
		    } finally {
		    	
		    }

		}
		
		try {
    		writer.close();
    	}
    	catch (IOException e){
    		
    	}
		
		//String rutaArchivo = rutaArchivoOut;
		
		System.out.println(rutaArchivoOut + ":" + nombreArchivo);
		
	    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());
	    // XXX create temp. filecontent ... resultFile
	    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo, "text/xml");
	    
	    facesContext.responseComplete();
		
	}
	
	void writeOutContent(final HttpServletResponse res, final File content, final String theFilename, String contentType) {
	    if (content == null)
	      return;
	    try {
	      res.setHeader("Pragma", "no-cache");
	      res.setDateHeader("Expires", 0);
	      res.setContentType(contentType);
	      res.setHeader("Content-disposition", "attachment; filename=" + theFilename);
	      fastChannelCopy(Channels.newChannel(new FileInputStream(content)), Channels.newChannel(res.getOutputStream()));
	    } catch (final IOException e) {
	      // TODO produce a error message <img src="http://s0.wp.com/wp-includes/images/smilies/icon_smile.gif?m=1129645325g" alt=":)" class="wp-smiley"> 
	    }
	  }
	 
	  void fastChannelCopy(final ReadableByteChannel src, final WritableByteChannel dest) throws IOException {
	    final ByteBuffer buffer = ByteBuffer.allocateDirect(1000 * 1024);
	    while (src.read(buffer) != -1) {
	      buffer.flip();
	      dest.write(buffer);
	      buffer.compact();
	    }
	    buffer.flip();
	    while (buffer.hasRemaining()){
	      dest.write(buffer);
	    }
	  }
	  
   public void selectionListener(AjaxBehaviorEvent event) {
        UIExtendedDataTable dataTable = (UIExtendedDataTable) event.getComponent();
        Object originalKey = dataTable.getRowKey();
        selectionItems.clear();
        for (Object selectionKey : selection) {
            dataTable.setRowKey(selectionKey);
            if (dataTable.isRowAvailable()) {
                selectionItems.add((Reporte) dataTable.getRowData());
            }
        }
        dataTable.setRowKey(originalKey);
    }	  
	
	public String  checkBoxChange() {
		String result;
		String reporteId = Utilidad.getParametro("reporteId");
		String seleccion = Utilidad.getParametro("seleccion");
		
		
		ReporteService service = new ReporteService();
		
		Reporte rep = service.ReadById(new Long(reporteId));
		
		if(seleccion.equals("true")){
			rep.setSeleccion(false);
		}else {
			rep.setSeleccion(true);
		}
		
		service.update(rep);
		
		//getReportes();
		
		return result = "/zonasegura/reporteListar?faces-redirect=true";
	}
	
	public String actualizarPeriodos() {
		String result;
		
		Calendar calendar = Calendar.getInstance();
			
		for (Reporte item_reporte : reportes) {
			int inAnho = calendar.get(Calendar.YEAR);
			int inMes = calendar.get(Calendar.MONTH) + 1;
			
			String strAnho = Integer.toString(calendar.get(Calendar.YEAR)).trim();
			String strMes = Integer.toString(calendar.get(Calendar.MONTH) + 1).trim();		
			
			if(strMes.length() == 1){
				strMes = "0" + strMes;
			}
			
			ReporteService service = new ReporteService();
			
			if(item_reporte.getFrecuencia().equals("MENSUAL")) {
				if(inMes == 1){
					inMes = 12;
					inAnho = inAnho - 1;
					strMes = Integer.toString(inMes);
					strAnho = Integer.toString(inAnho);
					
				} else {
					inMes = inMes - 1;
					strMes = Integer.toString(inMes);
				}
			}
			
			if(item_reporte.getFrecuencia().equals("TRIMESTRAL")) {
				if(inMes <= 3){
					strMes = "T4";
					inAnho = inAnho - 1;
					strAnho = Integer.toString(inAnho);
				}
				if(inMes > 3 && inMes <= 6){
					strMes = "T1";
				}
				if(inMes > 6 && inMes <= 9){
					strMes = "T2";
				}
				if(inMes > 9 && inMes <= 12){
					strMes = "T3";
				}
			}
			
			if(item_reporte.getFrecuencia().equals("SEMESTRAL")) {
				if(inMes <= 6){
					strMes = "S2";
					inAnho = inAnho - 1;
					strAnho = Integer.toString(inAnho);
				} else {
					strMes = "S1";
				}
				
			}
			
			if(strMes.trim().length() == 1){
				strMes = "0" + strMes;
			}
			
			item_reporte.setAnho(strAnho);
			item_reporte.setPeriodo(strMes);
			
			EmpresaService serviceEmpresa = new EmpresaService();
			Empresa empresa = null;
			
			for(Empresa item:serviceEmpresa.getAllRows()){
				empresa = item;
			}
			
			String nombre;
			if(!item_reporte.getFrecuencia().equals("RANGO")) {
				nombre = empresa.getEmpresa() + "A" + item_reporte.getAnho().substring(2) + item_reporte.getPeriodo() + "." + item_reporte.getCodReporte().substring(0,3);
				
			} else {
				nombre = empresa.getEmpresa() + "Axxxx." + item_reporte.getCodReporte().substring(0,3);
			}
			item_reporte.setArchivo(nombre);
			
			service.update(item_reporte);
			
		}
		return result = "/zonasegura/reporteListar?faces-redirect=true";
	}
	
	public String seleccionInversa() {
		String result;
			
		for (Reporte item_reporte : reportes) {
			
			ReporteService service = new ReporteService();
			
			if(item_reporte.getSeleccion()) {
				item_reporte.setSeleccion(false);
			} else {
				item_reporte.setSeleccion(true);
			}
			
			service.update(item_reporte);
		}
		return result = "/zonasegura/reporteListar?faces-redirect=true";
	}

	public String seleccionDeseleccion() {
		String result;
			
		for (Reporte item_reporte : reportes) {
			
			ReporteService service = new ReporteService();
			
			if(getSeleccion()) {
				item_reporte.setSeleccion(false);
			} else {
				item_reporte.setSeleccion(true);
			}
			
			service.update(item_reporte);
		}
		
		setSeleccion(!getSeleccion());
		
		return result = "/zonasegura/reporteListar?faces-redirect=true";
	}
	
	public String getTipParametroFilter() {
		return tipParametroFilter;
	}

	public void setTipParametroFilter(String tipParametroFilter) {
		this.tipParametroFilter = tipParametroFilter;
	}
	
	public String getAmbitoFilter() {
		return ambitoFilter;
	}

	public void setAmbitoFilter(String ambitoFilter) {
		this.ambitoFilter = ambitoFilter;
	}

	public List<SelectItem> getItemsTipParametro() {
		itemsTipParametro=new ArrayList<SelectItem>();
		TipoParametroService tipoParametroServie = new TipoParametroService();
		
		itemsTipParametro.add(new SelectItem("", ""));
		for (TipoParametro tipoParametro : tipoParametroServie.getAllRows1()) {
			itemsTipParametro.add(new SelectItem(tipoParametro.getCodTipParametro(),tipoParametro.getDescripcion()));
		}
		/*
		itemsTipParametro.add(new SelectItem("PT","CALIDAD DE PRODUCTO - TENSION"));
		itemsTipParametro.add(new SelectItem("PF","CALIDAD DE PRODUCTO - FRECUENCIA"));
		itemsTipParametro.add(new SelectItem("PP","CALIDAD DE PRODUCTO - PERTURBACIONES"));
		itemsTipParametro.add(new SelectItem("SI","CALIDAD DE SUMINISTRO - INTERRUPCIONES"));
		*/
		return itemsTipParametro;
	}

	public void setItemsTipParametro(List<SelectItem> itemsTipParametro) {
		this.itemsTipParametro = itemsTipParametro;
	}
	
	public List<SelectItem> getItemsAmbitos() {
		itemsAmbitos=new ArrayList<SelectItem>();
		itemsAmbitos.add(new SelectItem("", ""));
		itemsAmbitos.add(new SelectItem("NTCSE","NTCSE"));
		itemsAmbitos.add(new SelectItem("NTCSER","NTCSER"));
		itemsAmbitos.add(new SelectItem("PROCEDIMIENTOS","PROCEDIMIENTOS"));
		itemsAmbitos.add(new SelectItem("EXPORTACION","EXPORTACION"));
		return itemsAmbitos;
	}

	public void setItemsAmbitos(List<SelectItem> itemsAmbitos) {
		this.itemsAmbitos = itemsAmbitos;
	}
	
	public Filter<?> getFilterTipParametro() {
        return new Filter<Reporte>() {
            public boolean accept(Reporte t) {
            	String tipParametro = getTipParametroFilter();
            	
                if (tipParametro == null || tipParametro.length() == 0 || tipParametro.equals(t.getTipoParametro().getCodTipParametro())) {
                    return true;
                }
                return false;
            }
        };
    }
	
	public Filter<?> getFilterAmbito() {
        return new Filter<Reporte>() {
            public boolean accept(Reporte t) {
            	String ambito = getAmbitoFilter();
                if (ambito == null || ambito.length() == 0 || ambito.equals(t.getAmbito())) {
                    return true;
                }
                return false;
            }
        };
    }

}
