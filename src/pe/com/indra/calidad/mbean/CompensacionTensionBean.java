package pe.com.indra.calidad.mbean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import org.hibernate.HibernateException;

import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.entity.SuministroAfectado;
import pe.com.calidad.suministro.service.InterrupcionService;
import pe.com.calidad.suministro.service.SuministroAfectadoService;
import pe.com.indra.calidad.entity.CompensacionTension;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.TipoAlimentacion;
import pe.com.indra.calidad.service.CompensacionTensionService;
import pe.com.indra.calidad.service.TipoAlimentacionService;
import pe.com.indra.calidad.util.Utilidad;

@ManagedBean(name="compensacionTensionBean")
@SessionScoped
public class CompensacionTensionBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	
	private List<CompensacionTension> compensacionTensiones;
	private long medicionId;
	
	private String numSumAfectadoFilter;
	
	
	@PostConstruct
	public void init(){
		
		
		
	}
	//metodo de calidad de tension 
	public String mostrarAfectados(){
		String medicionIdStr = Utilidad.getParametro("medicionId");
		
		CompensacionTensionService service = new CompensacionTensionService();
		
		//compensacionTensiones=(List<CompensacionTension>) service.getAllRows1(Long.valueOf(medicionIdStr));
		compensacionTensiones = (List<CompensacionTension>) service.getAllRows1(Long.valueOf(medicionIdStr));
		
		return "/zonasegura/compensacionTensionListar?faces-redirect=true";
	}
	
	
	public List<CompensacionTension> getCompensacionTensiones() {
		return compensacionTensiones;
	}
	public void setCompensacionTensiones(List<CompensacionTension> compensacionTensiones) {
		this.compensacionTensiones = compensacionTensiones;
	}
	
	
	public long getMedicionId() {
		return medicionId;
	}
	public void setMedicionId(long medicionId) {
		this.medicionId = medicionId;
	}
	public String getNumSumAfectadoFilter() {
		return numSumAfectadoFilter;
	}
	public void setNumSumAfectadoFilter(String numSumAfectadoFilter) {
		this.numSumAfectadoFilter = numSumAfectadoFilter;
	}
	
	
	
	

	
}
