package pe.com.indra.calidad.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.indra.calidad.entity.PeriodoMedicion;
import pe.com.indra.calidad.service.PeriodoMedicionService;

@FacesConverter(value="periodoMedicionConverter",forClass=PeriodoMedicionConverter.class)

public class PeriodoMedicionConverter implements Converter{

	@Override
	public PeriodoMedicion getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long perMedicionId=new Long(cadena);
		PeriodoMedicionService service=new PeriodoMedicionService();
		PeriodoMedicion periodoMedicion=new PeriodoMedicion();
		periodoMedicion=service.ReadById(perMedicionId);
		System.out.println(periodoMedicion.getPerMedicionId()+":"+periodoMedicion.getDescripcion());
		return periodoMedicion;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			PeriodoMedicion periodoMedicion=(PeriodoMedicion) objeto;
			//System.out.println("objeto:"+new Long(periodoMedicion.getPerMedicionId()).toString());
			return new Long(periodoMedicion.getPerMedicionId()).toString();
		}
		return null;
	}


}
