package pe.com.indra.calidad.mbean;



import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;

import com.opencsv.CSVReader;

import pe.com.indra.calidad.util.ProcesarArchivo;
import pe.com.indra.calidad.util.Sql;
import pe.com.indra.calidad.util.ConectaDb;

import java.sql.Connection;
import java.sql.CallableStatement;
import java.sql.SQLException;

public class FileUtilUploadBean implements Serializable{

	    private ArrayList<File> files = new ArrayList<File>();
	   	private int uploadsAvailable = 1;
	    private boolean autoUpload = false;
	    private boolean useFlash = false;
	    //Campos para el proceso del archivo cargado
	    protected String columnas;
	    protected int numColumnas;
	    protected String tipoProceso;
	    protected String nombreColumnas;
	    protected long filaInicial;
	    private String separadorColumnas;
		private String formatoFecha;
	    private Long numColumnsFormatoFecha;
	    //El nombre debe estar relacionado con algun identificador
	    private String nombreArchivoDestino;
	    protected String nombreArchivoBase;
	    protected String nombreArchivoRuta; 
	    private boolean archivoCargado;

		protected List<SelectItem> itemsNombreColumnas;
		protected List<SelectItem> itemsSeparadorColumnas;
		protected List<SelectItem> itemsFormatoFecha;
	

	@PostConstruct
	   public void init(){
		   //nombreArchivoDestino = "proceso228_"+new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".txt";
		   //filaInicial = 2;
		   //columnas = "1,2,3";
	   }
	    
	    public String getColumnas() {
			return columnas;
		}

		public void setColumnas(String columnas) {
			this.columnas = columnas;
		}

		public String getNombreColumnas() {
			return nombreColumnas;
		}

		public void setNombreColumnas(String nombreColumnas) {
			this.nombreColumnas = nombreColumnas;
		}

		public Long getFilaInicial() {
			return filaInicial;
		}

		public void setFilaInicial(Long filaInicial) {
			this.filaInicial = filaInicial;
		}

	    public String getSeparadorColumnas() {
			return separadorColumnas;
		}

		public void setSeparadorColumnas(String separadorColumnas) {
			this.separadorColumnas = separadorColumnas;
		}
		
		public String getFormatoFecha() {
			return formatoFecha;
		}

		public void setFormatoFecha(String formatoFecha) {
			this.formatoFecha = formatoFecha;
		}

		public Long getNumColumnsFormatoFecha() {
			return numColumnsFormatoFecha;
		}

		public void setNumColumnsFormatoFecha(Long numColumnsFormatoFecha) {
			this.numColumnsFormatoFecha = numColumnsFormatoFecha;
		}


		public int getSize() {
	        if (getFiles().size()>0){
	            return getFiles().size();
	        }else 
	        {
	            return 0;
	        }
	    }

	    public FileUtilUploadBean() {
	    }

	    public void paint(OutputStream stream, Object object) throws Exception {
	         System.out.println("Archivo Guardado con Exito!");;	
	    }
	    
	    public void listener(FileUploadEvent event) throws Exception{
	    	if( event.getUploadedFile() == null ){
	    		throw new Exception("ERROR: No se ha cargado con exito el archivo, UploadItem es nulo o File es nulo");
	    	} else {
	    		UploadedFile item=event.getUploadedFile();
	    		String fileName = item.getName();
	    		this.writeFileOnServer(item);	
		    	System.out.println("[Atencion] Se cargo el archivo "+fileName+" con exito en el sistema");
	    	}	
	    }  
	    
	    public void writeFileOnServer(UploadedFile item) throws Exception{
	    	String path = System.getProperty("uploads.folder");
	    	System.out.println("[Atencion] Ruta de Carga: "+path);
	        //FileWriter fileWriter = new FileWriter(path+this.getNombreArchivoDestino());
	    	
	    	this.nombreArchivoDestino = this.nombreArchivoBase + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".txt";
	    	this.nombreArchivoRuta = "D:\\"+ this.nombreArchivoDestino;
	    	FileWriter fileWriter = new FileWriter(this.nombreArchivoRuta);
	    	
	        DataInputStream dataInputStream = new DataInputStream(item.getInputStream());
            //BufferedReader br = new BufferedReader(new InputStreamReader(in));
	        //BufferedReader reader = new BufferedReader(new FileReader(file));
	        BufferedReader reader = new BufferedReader(new InputStreamReader(dataInputStream));
	        BufferedWriter writer =    new BufferedWriter(fileWriter);
	        try {
	            String line = reader.readLine();
	            while (line != null) {
	                //System.out.println(line);
	                writer.write(line);
	                writer.newLine();
	                line = reader.readLine();
	            }	            
	        } finally {
	            reader.close();
	            writer.close();
	        }	        
	    }
	    
	    public String clearUploadData() {
	        files.clear();
	        setUploadsAvailable(5);
	        return null;
	    }
	    
	    public long getTimeStamp(){
	        return System.currentTimeMillis();
	    }
	    
	    
	    public int getUploadsAvailable() {
	        return uploadsAvailable;
	    }

	    public void setUploadsAvailable(int uploadsAvailable) {
	        this.uploadsAvailable = uploadsAvailable;
	    }

	    public boolean isAutoUpload() {
	        return autoUpload;
	    }

	    public void setAutoUpload(boolean autoUpload) {
	        this.autoUpload = autoUpload;
	    }

	    public boolean isUseFlash() {
	        return useFlash;
	    }

	    public void setUseFlash(boolean useFlash) {
	        this.useFlash = useFlash;
	    }
	    
	    public ArrayList<File> getFiles() {
			return files;
		}

		public void setFiles(ArrayList<File> files) {
			this.files = files;
		}
	
		
		public String getNombreArchivoDestino() {
			return nombreArchivoDestino;
		}

		public void setNombreArchivoDestino(String nombreArchivoDestino) {
			this.nombreArchivoDestino = nombreArchivoDestino;
		}

		public boolean isArchivoCargado() {
			String path = System.getProperty("uploads.folder"); //directorio donde esta el archivo
		    String fullNameArchivo=path+this.getNombreArchivoDestino(); //esta es la ruta completa
		    File file=new File(fullNameArchivo);
		    if( file.exists() ){
		    	setArchivoCargado(true);
		    } else { 
		    	setArchivoCargado(false);
		    }
			return archivoCargado;
		}

		public void setArchivoCargado(boolean archivoCargado) {
			this.archivoCargado = archivoCargado;
		}
		
		public List<SelectItem> getItemsSeparadorColumnas() {
			return itemsSeparadorColumnas;
		}

		public void setItemsSeparadorColumnas(List<SelectItem> itemsSeparadorColumnas) {
			this.itemsSeparadorColumnas = itemsSeparadorColumnas;
		}



		public void setItemsFormatoFecha(List<SelectItem> itemsFormatoFecha) {
			this.itemsFormatoFecha = itemsFormatoFecha;
		}
		
		
		public List<SelectItem> getItemsFormatoFecha() {
			return itemsFormatoFecha;
		}


		public List<SelectItem> getItemsNombreColumnas() {
			return itemsNombreColumnas;
		}

		public void setItemsNombreColumnas(List<SelectItem> itemsNombreColumnas2) {
			this.itemsNombreColumnas = itemsNombreColumnas2;
		}
		
		public List<String[]> getListStringDesdeCsv() throws IOException{
			CSVReader reader = new CSVReader(new FileReader(this.nombreArchivoRuta));
	        List<String[]> lst = reader.readAll();
	        reader.close();
	        return lst;
		}
		
		public void registrarCsv() throws IOException{
			CSVReader reader = new CSVReader(new FileReader(this.nombreArchivoRuta));
	        List<String[]> lst = reader.readAll();
	        reader.close();
	        
	        if (lst.size()>0){
	        	Long idCabecera = crearCabecera(lst.size()-1);
	        	lst.remove(0);
	        	crearCuerpo(lst,this.numColumnas,idCabecera);
	        }
		}
		
		private Long crearCabecera(int cantidadRegistros){
			Sql sql = new Sql("CALIDAD");
			Object id = sql.getCampo("SELECT CAL_CARGA_MASIVA_SEQ.NEXTVAL AS IDENTIFICADOR FROM DUAL");
	        String insertSql = "INSERT INTO CAL_CARGA_MASIVA (ID_CAL_CARGA_MASIVA,FECHA_CARGA,TIPO_CARGA,REGISTROS, PROCESADOS) VALUES (" + id + ", SYSDATE,'" + this.tipoProceso + "'," +cantidadRegistros + ",0)";
			System.out.println(insertSql);
			String resultado = sql.ejecuta(insertSql);
			
			return ((BigDecimal)id).longValue();
		}
		
		private void crearCuerpo(List<String[]> lst, int numColumnas, Long idCabecera){
			Sql sql = new Sql("CALIDAD");
			String  insertSql = "";
			int total = lst.size();
			int partes = total/100;
			int resto = total%100;
			
			for (int i=0;i<partes;i=i+1){
				insertSql = getSqlInsertCuerpo(lst,numColumnas,i*100,(i+1)*100,idCabecera);
				String resultado = sql.ejecuta(insertSql);
				System.out.println(insertSql);
			}
			
			if (resto>0){
				insertSql = getSqlInsertCuerpo(lst,numColumnas,partes*100,partes*100 + resto,idCabecera);
				String resultado = sql.ejecuta(insertSql);
				System.out.println(insertSql);
			}
		}
		
		
		private String getSqlInsertCuerpo(List<String[]> lst, int numColumnas, int desde, int hasta, Long idCabecera){
			String sql = "INSERT ALL ";
			for (int i=desde;i<hasta;i=i+1){
				sql = sql + getSqlInsertLinea(lst.get(i),numColumnas,idCabecera,i);
			}
			sql = sql + " SELECT 1 FROM DUAL";
			return sql;
			/*
			INSERT ALL
			   INTO CAL_CARGA_MASIVA_DATA (ID_CAL_CARGA_MASIVA_DATA,ID_CAL_CARGA_MASIVA,C1,C2) VALUES (1, 1, 'VAL1','VAL2')
			   INTO CAL_CARGA_MASIVA_DATA (ID_CAL_CARGA_MASIVA_DATA,ID_CAL_CARGA_MASIVA,C1,C2) VALUES (2, 1, 'VAL1','VAL2')
			SELECT 1 FROM DUAL;*/
		}
		
		private String getSqlInsertLinea(String[] lst, int numColumnas, Long idCabecera, int idFila){
			String sql = "INTO CAL_CARGA_MASIVA_DATA (NUM_FILA,ID_CAL_CARGA_MASIVA";
			
			for (int i=1;i<=numColumnas;i=i+1){
				sql = sql + ",C" + i;
			}
			idFila = idFila+1;
			sql = sql + ") VALUES (" + idFila + "," + idCabecera;
			
			for (int i=0;i<numColumnas;i=i+1){
				sql = sql + ",'" + lst[i] + "'";
			}
			sql = sql + ") ";
			return sql;
		}


}
