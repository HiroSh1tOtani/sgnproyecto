package pe.com.indra.calidad.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.indra.calidad.entity.CampanaMedicion;
import pe.com.indra.calidad.service.CampanaMedicionService;

@FacesConverter(value="campanaMedicionConverter",forClass=CampanaMedicionConverter.class)

public class CampanaMedicionConverter implements Converter{

	@Override
	public CampanaMedicion getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long campanaId=new Long(cadena);
		CampanaMedicionService service=new CampanaMedicionService();
		CampanaMedicion campanaMedicion=new CampanaMedicion();
		campanaMedicion=service.ReadById(campanaId);
		System.out.println(campanaMedicion.getDescripcion());
		return campanaMedicion;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			CampanaMedicion campanaMedicion=(CampanaMedicion) objeto;
			//System.out.println("objeto:"+new Long(campanaMedicion.getCamMedicionId()).toString());
			return new Long(campanaMedicion.getCamMedicionId()).toString();
		}
		return null;
	}


}
