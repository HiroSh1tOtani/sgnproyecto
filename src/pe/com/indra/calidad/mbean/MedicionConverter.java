package pe.com.indra.calidad.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.service.MedicionService;

@FacesConverter(value="medicionConverter",forClass=MedicionConverter.class)

public class MedicionConverter implements Converter{

	@Override
	public Medicion getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long medicionId=new Long(cadena);
		MedicionService service=new MedicionService();
		Medicion medicion=new Medicion();
		medicion=service.ReadById(medicionId);
		System.out.println("Llego a:" + medicion.getMedicionId());
		return medicion;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			Medicion medicion=(Medicion) objeto;
			//System.out.println("objeto:"+new Long(medicion.getMedicionId()).toString());
			return new Long(medicion.getMedicionId()).toString();
		}
		return null;
	}


}
