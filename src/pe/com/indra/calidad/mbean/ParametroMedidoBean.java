package pe.com.indra.calidad.mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

import pe.com.indra.calidad.entity.ParametroMedido;
import pe.com.indra.calidad.service.ParametroMedidoService;

@ManagedBean(name="parametroMedidoBean")
@SessionScoped
public class ParametroMedidoBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ParametroMedidoService service=null;
	
	private List<ParametroMedido> parametros=null;

	@PostConstruct
	public void init(){
		service=new ParametroMedidoService();
		parametros=service.getAllRows();
	}
	
	public List<ParametroMedido> getParametroMedido() {
		parametros=(List<ParametroMedido>) service.getAllRows();
		return parametros;
	}

	public void getParametroMedido(List<ParametroMedido> parametros) {
		this.parametros = parametros;
	}
	
}
