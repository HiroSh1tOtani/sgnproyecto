package pe.com.indra.calidad.mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.entity.Empresa;//Localidad;
import pe.com.indra.calidad.service.EmpresaService;//LocalidadService;
import pe.com.indra.calidad.service.LocalidadService;
import pe.com.indra.calidad.util.Utilidad;

@ManagedBean(name="empresaBean")
@SessionScoped
public class EmpresaBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<Empresa> empresas;
	private Empresa empresa;
	
		
	private String mensaje;
	
	@PostConstruct 
	public void init(){
		System.out.println("se ejecuto PostConstruct "+new java.util.Date());
		empresa=new Empresa();
		
	}
	
			
	public List<Empresa> getEmpresas() {
		EmpresaService service=new EmpresaService();
		empresas= service.getAllRows();
		return empresas;
	}

	public void setEmpresas(List<Empresa> empresas) {
		this.empresas = empresas;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String prepararCrear(){
		//Esto funciona siempre que el bean tenga ambito de sesion 
		setEmpresa(new Empresa());
		return "/zonasegura/empresaCrear?faces-redirect=true";
	}
	
	public String prepararEditar(){
		
		
		try {
			cargarEmpresaActual();
			return "/zonasegura/empresaEditar?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/default?faces-redirect=true";
		}
	}

	public String prepararVer(){
				
		try {
			cargarEmpresaActual();
			return "/zonasegura/empresaVer?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/empresaListar?faces-redirect=true";
		}
	}
	
	public String actualizar(){
		
		EmpresaService service=new EmpresaService();
		
		try {
			service.update(empresa);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/default?faces-redirect=true";
	}
	
	
	
	public void cargarEmpresaActual(){
		//String empresaCod=Utilidad.getParametro("empresa");
		EmpresaService service=new EmpresaService();
				
		try {
			empresa=service.readByCod("ESM");
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

			throw new HibernateException("No se puede cargar empresa.", e);
			
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	
}
