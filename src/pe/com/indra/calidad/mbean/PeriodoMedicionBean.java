	package pe.com.indra.calidad.mbean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.entity.PeriodoMedicion;
import pe.com.indra.calidad.service.PeriodoMedicionService;
import pe.com.indra.calidad.util.Sql;
import pe.com.indra.calidad.util.Utilidad;

@ManagedBean(name="periodoMedicionBean")
@SessionScoped
public class PeriodoMedicionBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<PeriodoMedicion> periodoMediciones;
	private PeriodoMedicion periodoMedicion;
	private List<SelectItem> itemsAno;
	private List<SelectItem> itemsPeriodo;
	
	private String mensaje;

	
	@PostConstruct 
	public void init(){
		System.out.println("se ejecuto PostConstruct "+new java.util.Date());
		periodoMedicion=new PeriodoMedicion();
		periodoMedicion.setEstado("1");
	}

	public List<PeriodoMedicion> getPeriodoMediciones() {
		PeriodoMedicionService service=new PeriodoMedicionService();
		periodoMediciones= service.getAllRows();
		return periodoMediciones;
	}

	public void setPeriodoMediciones(List<PeriodoMedicion> periodoMediciones) {
		this.periodoMediciones = periodoMediciones;
	}

	public PeriodoMedicion getPeriodoMedicion() {
		return periodoMedicion;
	}

	public void setPeriodoMedicion(PeriodoMedicion periodoMedicion) {
		this.periodoMedicion = periodoMedicion;
	}

	public List<SelectItem> getItemsAno() {
		itemsAno=new ArrayList<SelectItem>();
		itemsAno.add(new SelectItem("2012","2012"));
		itemsAno.add(new SelectItem("2013","2013"));
		itemsAno.add(new SelectItem("2014","2014"));
		itemsAno.add(new SelectItem("2015","2015"));
		itemsAno.add(new SelectItem("2016","2016"));
		itemsAno.add(new SelectItem("2017","2017"));
		itemsAno.add(new SelectItem("2018","2018"));
		itemsAno.add(new SelectItem("2019","2019"));
		itemsAno.add(new SelectItem("2020","2020"));
		itemsAno.add(new SelectItem("2021","2021"));
		itemsAno.add(new SelectItem("2022","2022"));
		itemsAno.add(new SelectItem("2023","2023"));
		return itemsAno;
	}

	public void setItemsAno(List<SelectItem> itemsAno) {
		this.itemsAno = itemsAno;
	}

	public List<SelectItem> getItemsPeriodo() {
		itemsPeriodo=new ArrayList<SelectItem>();
		itemsPeriodo.add(new SelectItem("01","ENERO"));
		itemsPeriodo.add(new SelectItem("02","FEBRERO"));
		itemsPeriodo.add(new SelectItem("03","MARZO"));
		itemsPeriodo.add(new SelectItem("04","ABRIL"));
		itemsPeriodo.add(new SelectItem("05","MAYO"));
		itemsPeriodo.add(new SelectItem("06","JUNIO"));
		itemsPeriodo.add(new SelectItem("07","JULIO"));
		itemsPeriodo.add(new SelectItem("08","AGOSTO"));
		itemsPeriodo.add(new SelectItem("09","SEPTIEMBRE"));
		itemsPeriodo.add(new SelectItem("10","OCTUBRE"));
		itemsPeriodo.add(new SelectItem("11","NOVIEMBRE"));
		itemsPeriodo.add(new SelectItem("12","DICIEMBRE"));
		return itemsPeriodo;
	}

	public void setItemsPeriodo(List<SelectItem> itemsPeriodo) {
		this.itemsPeriodo = itemsPeriodo;
	}
	
	public String prepararCrear(){
		//Esto funciona siempre que el bean tenga ambito de sesion 
		setPeriodoMedicion(new PeriodoMedicion());
		getPeriodoMedicion().setEstado("1");
		return "/zonasegura/periodoMedicionCrear?faces-redirect=true";
	}
	
	public String prepararEditar(){
		//TODO: Por implementar carga de la instancia
		//System.out.println("compensacionUnitariaId:"+Utilidad.getParametro("compensacionUnitariaId"));
		//cargarPeriodoMedicionActual();
		//return "/zonasegura/periodoMedicionEditar?faces-redirect=true";
		
		try {
			cargarPeriodoMedicionActual();
			return "/zonasegura/periodoMedicionEditar?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/periodoMedicionListar?faces-redirect=true";
		}
	}

	public String prepararVer(){
		//TODO: Por implementar carga de la instancia
		//System.out.println("compensacionUnitariaId:"+Utilidad.getParametro("compensacionUnitariaId"));
		//cargarPeriodoMedicionActual();
		//return "/zonasegura/periodoMedicionVer?faces-redirect=true";
		
		try {
			cargarPeriodoMedicionActual();
			return "/zonasegura/periodoMedicionVer?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/periodoMedicionListar?faces-redirect=true";
		}
	}
	
	public String salvar(){
		
		PeriodoMedicionService service=new PeriodoMedicionService();
		
		try {
			
			String ano_temp = periodoMedicion.getAno();
			String periodo_temp = periodoMedicion.getPeriodo();
			
			Sql sql = new Sql("CALIDAD"); 
			
			
        	String selectSql = "SELECT 	COUNT(*) FROM CAL_PERIODO_MEDICION WHERE ESTADO = '1' AND ANO = '" + ano_temp + "' AND PERIODO = '" + periodo_temp + "'" ;
        	Object[] objFila = sql.getFila(selectSql);
        	BigDecimal cantidadx = (BigDecimal) objFila[0];
        	
        	if(cantidadx.longValue() > 0){
    			setMensaje("Ya Existe un Periodo para el mismo año y mes.");			
    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,getMensaje(), getMensaje()));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
    			
    			return "/zonasegura/periodoMedicionCrear?faces-redirect=true";
        	} else {
        		if(Long.valueOf(periodoMedicion.getPeriodo()) >= 1 && Long.valueOf(periodoMedicion.getPeriodo()) <= 6) {
        			
        			periodoMedicion.setSemestre("S1");  
				} else {
					periodoMedicion.setSemestre("S2"); 
				}
				service.create(periodoMedicion);
        	}
			
		////	
			
			
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/periodoMedicionListar?faces-redirect=true";
	}
	
	public String actualizar(){
		//PeriodoMedicionService service=new PeriodoMedicionService();
		//service.update(periodoMedicion);
		//System.out.println("Se actualizo un Periodo de Medicion con exito.");
		//return "/zonasegura/periodoMedicionListar?faces-redirect=true";
		
		PeriodoMedicionService service=new PeriodoMedicionService();
		
		try {
			service.update(periodoMedicion);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/periodoMedicionListar?faces-redirect=true";
	}
	
	public String eliminar(){
		String periodoMedicionId=Utilidad.getParametro("perMedicionId");
		PeriodoMedicionService service=new PeriodoMedicionService();
		//periodoMedicion=service.ReadById(new Long(periodoMedicionId));
		//periodoMedicion.setEstado("0");
		//service.update(periodoMedicion);
		//System.out.println("Se elimino una Periodo de Medicion con exito.");
		//return "/zonasegura/periodoMedicionListar?faces-redirect=true";
		
		try {
			periodoMedicion=service.ReadById(new Long(periodoMedicionId));
			periodoMedicion.setEstado("0");
			service.update(periodoMedicion);
		} catch (HibernateException e) {
			
			
			System.out.println(e.getMessage()+":"+e.getCause());
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			
		} catch (Exception e) {
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/periodoMedicionListar?faces-redirect=true";
	}
	
	public void cargarPeriodoMedicionActual(){
		String periodoMedicionId=Utilidad.getParametro("perMedicionId");
		PeriodoMedicionService service=new PeriodoMedicionService();
		//periodoMedicion=service.ReadById(new Long(periodoMedicionId));
		
		try {
			periodoMedicion=service.ReadById(new Long(periodoMedicionId));
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			throw new HibernateException("No se puede cargar Periodo.", e);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
}