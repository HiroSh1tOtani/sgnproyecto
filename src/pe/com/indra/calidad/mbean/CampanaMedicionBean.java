package pe.com.indra.calidad.mbean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.entity.CampanaMedicion;
import pe.com.indra.calidad.entity.CompensacionTension;
import pe.com.indra.calidad.entity.CronogramaMedicion;
import pe.com.indra.calidad.entity.IntervaloFRTension;
import pe.com.indra.calidad.entity.IntervaloTension;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.TipoParametro;
import pe.com.indra.calidad.entity.PeriodoMedicion;
import pe.com.indra.calidad.service.CampanaMedicionService;
import pe.com.indra.calidad.service.TipoParametroService;
import pe.com.indra.calidad.service.PeriodoMedicionService;
import pe.com.indra.calidad.util.ConectaDb;
import pe.com.indra.calidad.util.Sql;
import pe.com.indra.calidad.util.Utilidad;

@ManagedBean(name="campanaMedicionBean")
@SessionScoped
public class CampanaMedicionBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<CampanaMedicion> campanasMedicion;
	private CampanaMedicion campanaMedicion;
	private List<SelectItem> itemsTipoParametro;
	private List<SelectItem> itemsPeriodoMedicion;
	
	private String mensaje;
	
	@PostConstruct 
	public void init(){
		System.out.println("se ejecuto PostConstruct "+new java.util.Date());
		campanaMedicion=new CampanaMedicion();
		campanaMedicion.setEstado("1");
		/*
		String parametroNormaId=Utilidad.getParametro("parametroNormaId");
		if(parametroNormaId==null){
		  parametroNorma=new ParametroNorma();
		  parametroNorma.setEstado("1");
		}else{
			ParametroNormaService service=new ParametroNormaService();
			parametroNorma=service.ReadById(new Long(parametroNormaId));
		}
		*/
	}
	
	public List<CampanaMedicion> getCampanasMedicion() {
		CampanaMedicionService service=new CampanaMedicionService();
		campanasMedicion=service.getAllRows1();
		return campanasMedicion;
	}

	public void setCampanasMedicion(List<CampanaMedicion> campanasMedicion) {
		this.campanasMedicion = campanasMedicion;
	}

	public CampanaMedicion getCampanaMedicion() {
		
		return campanaMedicion;
	}

	public void setCampanaMedicion(CampanaMedicion campanaMedicion) {
		this.campanaMedicion = campanaMedicion;
	}
	
	public List<SelectItem> getItemsTipoParametro() {
		TipoParametroService service=new TipoParametroService();
		itemsTipoParametro=new ArrayList<SelectItem>();
		for(TipoParametro tp:service.getAllRows()){
			itemsTipoParametro.add(new SelectItem(tp,tp.getCodTipParametro()+"-"+tp.getDescripcion()));
		}
		return itemsTipoParametro;
	}

	public void setItemsTipoParametro(List<SelectItem> itemsTipoParametro) {
		this.itemsTipoParametro = itemsTipoParametro;
	}

/////
	
	public List<SelectItem> getItemsPeriodoMedicion() {
		PeriodoMedicionService service=new PeriodoMedicionService();
		itemsTipoParametro=new ArrayList<SelectItem>();
		for(PeriodoMedicion tp:service.getAllRows()){
			itemsTipoParametro.add(new SelectItem(tp,tp.getDescripcion()));
		}
		return itemsTipoParametro;
	}

	public void setItemsPeriodoMedicion(List<SelectItem> itemsPeriodoMedicion) {
		this.itemsPeriodoMedicion = itemsPeriodoMedicion;
	}
	
/////	
	
	public String prepararCrear(){
		//Esto funciona siempre que el bean tenga ambito de sesion 
		setCampanaMedicion(new CampanaMedicion());
		getCampanaMedicion().setEstado("1");
		return "/zonasegura/campanaMedicionCrear?faces-redirect=true";
	}
	
	public String prepararEditar(){
		//TODO: Por implementar carga de la instancia
		//System.out.println("parametroNormaId:"+Utilidad.getParametro("parametroNormaId"));
		//cargarCampanaMedicionActual();
		//return "/zonasegura/campanaMedicionEditar?faces-redirect=true";
		
		try {
			cargarCampanaMedicionActual();
			return "/zonasegura/campanaMedicionEditar?faces-redirect=true";
		} catch (HibernateException e) {
			setMensaje(e.getMessage());
			return "/zonasegura/campanaMedicionListar?faces-redirect=true";
		}
	
		
	}

	public String prepararVer(){
		//TODO: Por implementar carga de la instancia
		//System.out.println("parametroNormaId:"+Utilidad.getParametro("parametroNormaId"));
		try {
			cargarCampanaMedicionActual();
			return "/zonasegura/campanaMedicionVer?faces-redirect=true";
		} catch (HibernateException e) {
			setMensaje(e.getMessage());
			return "/zonasegura/campanaMedicionListar?faces-redirect=true";
		}
	}
	
	public String salvar(){
				
		CampanaMedicionService service=new CampanaMedicionService();
		
		try {
			//para visible solo una campana
			long perMedicionId_temp = campanaMedicion.getPeriodoMedicion().getPerMedicionId();
			long tipParametroId_temp = campanaMedicion.getTipoParametro().getTipParametroId();
			
			Sql sql = new Sql("CALIDAD"); 
			
	        
        	String selectSql = "SELECT 	COUNT(*) FROM CAL_CAMPANA_MEDICION WHERE ESTADO = '1' AND PK_PER_MEDICION_ID = " + String.valueOf(perMedicionId_temp) + " AND PK_TIP_PARAMETRO_ID = " + String.valueOf(tipParametroId_temp);
			
        	Object[] objFila = sql.getFila(selectSql);
			
        	BigDecimal cantidadx = (BigDecimal) objFila[0];
        	
        	if(cantidadx.longValue() > 0){
    			setMensaje("Ya Existe una Campana para el mismo periodo y parametro.");			
    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,getMensaje(), getMensaje()));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
    			
    			return "/zonasegura/campanaMedicionCrear?faces-redirect=true";
        	} else {
    			service.create(campanaMedicion);
        	}
			
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			return "/zonasegura/campanaMedicionCrear?faces-redirect=true";

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		
		return "/zonasegura/campanaMedicionListar?faces-redirect=true";
		
	}
	
	public String actualizar(){
		//CampanaMedicionService service=new CampanaMedicionService();
		//service.update(campanaMedicion);
		//System.out.println("Se actualizo una Campana Medicion con exito.");
		//return "/zonasegura/campanaMedicionListar?faces-redirect=true";
		
		CampanaMedicionService service=new CampanaMedicionService();
		
		try {
			service.update(campanaMedicion);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/campanaMedicionListar?faces-redirect=true";
		
	}
	
	public String eliminar(){
		String campanaMedicionId=Utilidad.getParametro("campanaMedicionId");
		CampanaMedicionService service=new CampanaMedicionService();
		
		Sql sql = new Sql("CALIDAD");
		String updateSql;
		String resultado;
		//campanaMedicion=service.ReadById(new Long(campanaMedicionId));
		//service.delete(campanaMedicion.getCamMedicionId());
		//System.out.println("Se elimino un Campana con exito.");
		//return "/zonasegura/campanaMedicionListar?faces-redirect=true";
		
		try {
			campanaMedicion=service.ReadById(new Long(campanaMedicionId));
			
			for(Medicion m: campanaMedicion.getMediciones()){
				
				updateSql = "UPDATE CAL_INTERVALO_TENSION SET ESTADO = '0' WHERE PK_MEDICION_ID = " + Long.toString(m.getMedicionId());
				
				resultado = sql.ejecuta(updateSql);	
				
				updateSql = "UPDATE CAL_COMPENSACION_TENSION SET ESTADO = '0' WHERE PK_MEDICION_ID = " + Long.toString(m.getMedicionId());
				
				resultado = sql.ejecuta(updateSql);	
				
				updateSql = "UPDATE CAL_CRONOGRAMA_MEDICION SET ESTADO = '0' WHERE PK_MEDICION_ID = " + Long.toString(m.getMedicionId());
				
				resultado = sql.ejecuta(updateSql);
				
				updateSql = "UPDATE CAL_INTERVALO_FR_TENSION SET ESTADO = '0' WHERE PK_MEDICION_ID = " + Long.toString(m.getMedicionId());
				
				resultado = sql.ejecuta(updateSql);
				
				m.setEstado("0");
			}
			
			campanaMedicion.setEstado("0");
			//service.delete(campanaMedicion.getCamMedicionId());
			service.update(campanaMedicion);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		
		return "/zonasegura/campanaMedicionListar?faces-redirect=true";
	}
	
	public void cargarCampanaMedicionActual(){
		String campanaMedicionId=Utilidad.getParametro("campanaMedicionId");
		CampanaMedicionService service=new CampanaMedicionService();
		//campanaMedicion=service.ReadById(new Long(campanaMedicionId));
		
		try {
			campanaMedicion=service.ReadById(new Long(campanaMedicionId));
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			throw new HibernateException("No se puede cargar Medición.", e);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
