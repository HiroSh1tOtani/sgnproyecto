package pe.com.indra.calidad.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.indra.calidad.entity.TipoAlimentacion;
import pe.com.indra.calidad.service.TipoAlimentacionService;

@FacesConverter(value="tipoAlimentacionConverter",forClass=TipoAlimentacionConverter.class)

public class TipoAlimentacionConverter implements Converter{

	@Override
	public TipoAlimentacion getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long tipParametroId=new Long(cadena);
		TipoAlimentacionService service=new TipoAlimentacionService();
		TipoAlimentacion tipoAlimentacion=new TipoAlimentacion();
		tipoAlimentacion=service.ReadById(tipParametroId);
		System.out.println(tipoAlimentacion.getCodTipAlimentacion()+":"+tipoAlimentacion.getDescripcion());
		return tipoAlimentacion;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			TipoAlimentacion tipoAlimentacion=(TipoAlimentacion) objeto;
			//System.out.println("objeto:"+new Long(tipoAlimentacion.getTipAlimentacionId()).toString());
			return new Long(tipoAlimentacion.getTipAlimentacionId()).toString();
		}
		return null;
	}


}
