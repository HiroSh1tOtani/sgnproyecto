package pe.com.indra.calidad.mbean;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import pe.com.indra.calidad.entity.IntervaloPerturbacion;//IntervaloTension;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.service.IntervaloPerturbacionService;//IntervaloTensionService;
import pe.com.indra.calidad.service.MedicionService;
import pe.com.indra.calidad.util.ModificarExcel;
import pe.com.indra.calidad.util.Sql;
import pe.com.indra.calidad.util.Utilidad;

@ManagedBean(name="intervaloPerturbacionBean")
@SessionScoped
public class IntervaloPerturbacionBean {
	
	private List<IntervaloPerturbacion> intervaloPerturbaciones;
	private List<IntervaloPerturbacion> intervaloPerturbaciones1;
	private Medicion medicion;
	
	private IntervaloPerturbacion intervaloPerturbacion;
	private List<SelectItem> itemsMedicion;
	
	private String tension;
	private String medicionId;
	
	private long cantidadFlicker;
	private long cantidadThd;
	private long cantidadArmonicos;
	private BigDecimal compensacionFlicker;
	private BigDecimal compensacionArmonicas;
	
	public IntervaloPerturbacionBean() {
		intervaloPerturbacion = new IntervaloPerturbacion();
		intervaloPerturbacion.setEstado("1");
	}
	
	@PostConstruct 
	public void init(){
		intervaloPerturbacion = new IntervaloPerturbacion();
		intervaloPerturbacion.setEstado("1");
		
		//setTension("R");
		

	}
	
	public String getMedicionId() {
		return medicionId;
	}

	public void setMedicionId(String medicionId) {
		this.medicionId = medicionId;
	}

		
	
	/*public List<IntervaloPerturbacion> getIntervaloPerturbaciones() {
		
		IntervaloPerturbacionService service=new IntervaloPerturbacionService();
		intervaloPerturbaciones= service.getAllRows();
		return intervaloPerturbaciones;
	}
	*/
	

	public Medicion getMedicion() {
		
		if (medicionId != null ){
			
			MedicionService service=new MedicionService();
			medicion=service.ReadById(new Long(medicionId));
		}
	
		return medicion;
	}

	public void setMedicion(Medicion medicion) {
		this.medicion = medicion;
	}

	public void setIntervaloPerturbaciones(List<IntervaloPerturbacion> intervaloPerturbaciones) {
		this.intervaloPerturbaciones = intervaloPerturbaciones;
	}

	public List<IntervaloPerturbacion> getIntervaloPerturbaciones1() {
		IntervaloPerturbacionService service=new IntervaloPerturbacionService();
		
		getMedicion();
		intervaloPerturbaciones1= service.getAllRows1(medicion.getMedicionId());
		if(intervaloPerturbaciones1.size() == 0){
			intervaloPerturbaciones1= service.getAllRows(medicion.getMedicionId());
		}
		
		return intervaloPerturbaciones1;
	}

	public void setIntervaloPerturbaciones1(List<IntervaloPerturbacion> intervaloPerturbaciones1) {
		this.intervaloPerturbaciones1 = intervaloPerturbaciones1;
	}

	public IntervaloPerturbacion getIntervaloPerturbacion() {
		return intervaloPerturbacion;
	}

	public void setIntervaloPerturbacion(IntervaloPerturbacion intervaloPerturbacion) {
		this.intervaloPerturbacion = intervaloPerturbacion;
	}

	public List<SelectItem> getItemsMedicion() {
		return itemsMedicion;
	}

	public void setItemsMedicion(List<SelectItem> itemsMedicion) {
		this.itemsMedicion = itemsMedicion;
	}
	
	public void cargarIntervaloPerturbacionActual(){
		String intPerturbacionId=Utilidad.getParametro("intPerturbacionId");
		IntervaloPerturbacionService service=new IntervaloPerturbacionService();
		intervaloPerturbacion=service.ReadById(new Long(intPerturbacionId));
	}
	
	
	public String getTension() {
		return tension;
	}

	public void setTension(String tension) {
		this.tension = tension;
	}
	
	public long getCantidadFlicker() {
		return cantidadFlicker;
	}

	public void setCantidadFlicker(long cantidadFlicker) {
		this.cantidadFlicker = cantidadFlicker;
	}

	public long getCantidadThd() {
		return cantidadThd;
	}

	public void setCantidadThd(long cantidadThd) {
		this.cantidadThd = cantidadThd;
	}

	public long getCantidadArmonicos() {
		return cantidadArmonicos;
	}

	public void setCantidadArmonicos(long cantidadArmonicos) {
		this.cantidadArmonicos = cantidadArmonicos;
	}

	public BigDecimal getCompensacionFlicker() {
		return compensacionFlicker;
	}

	public void setCompensacionFlicker(BigDecimal compensacionFlicker) {
		this.compensacionFlicker = compensacionFlicker;
	}

	public BigDecimal getCompensacionArmonicas() {
		return compensacionArmonicas;
	}

	public void setCompensacionArmonicas(BigDecimal compensacionArmonicas) {
		this.compensacionArmonicas = compensacionArmonicas;
	}

	public List<IntervaloPerturbacion> getIntervaloPerturbaciones() {
		return intervaloPerturbaciones;
	}

	public String muestraIntervalos(){
		
		medicionId=Utilidad.getParametro("medicionId");
		getIntervaloPerturbaciones1();
		return "/zonasegura/intervaloPerturbacionListar?faces-redirect=true";
	}
	
	public String muestraIntervalos2(){
		
		medicionId=Utilidad.getParametro("medicionId");	
		getIntervaloPerturbaciones1();
		return "/zonasegura/intervaloPerturbacionListar2?faces-redirect=true";
	}
	
	
	public String downloadGraficoFlicker() {
	    final FacesContext facesContext = FacesContext.getCurrentInstance();
	    
	    String rutaArchivoOut;
	    
	    String rutaUp;	    

	    rutaUp = facesContext.getExternalContext().getRealPath("/reportes/ReporteFlicker.xlsm");
	    
	    String path = System.getProperty("uploads.folder");
	    String nombreArchivo;
	    
	    System.out.print(path + "\n");
	    
	    nombreArchivo = "GraficoFli_" + medicion.getNumSumMedicion() + "_" + new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsm";
	    
	    rutaArchivoOut = path + nombreArchivo;
	    	    
	    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);

		XSSFSheet objHssfSheet = null;
		XSSFSheet objHssfSheet2 = null;
		try  {
			
		
		IntervaloPerturbacionService intervaloPerturbacionService = new IntervaloPerturbacionService();
		
		objHssfSheet = objHssfWorkbook.getSheetAt(0);
		
		objHssfSheet2 = objHssfWorkbook.getSheetAt(1);
		
		int intRow = 2;
		int intCell = 0;
		
		ModificarExcel.setCellValue(objHssfSheet, 5, 3, medicion.getNumSumMedicion());	
		ModificarExcel.setCellValue(objHssfSheet, 6, 3, medicion.getCodSed());
		ModificarExcel.setCellValue(objHssfSheet, 7, 3, medicion.getTenEntregada());
		ModificarExcel.setCellValue(objHssfSheet, 10, 3, medicion.getCodAlimentador());
		ModificarExcel.setCellValue(objHssfSheet, 11, 3, medicion.getNomUsuario());		
		ModificarExcel.setCellValue(objHssfSheet, 12, 3, medicion.getCampanaMedicion().getPeriodoMedicion().getAno()+"-"+medicion.getCampanaMedicion().getPeriodoMedicion().getPeriodo());
		
		System.out.println(intervaloPerturbacionService.getAllRows1(medicion.getMedicionId()).size());
		
		for(IntervaloPerturbacion it:intervaloPerturbacionService.getAllRows1(medicion.getMedicionId())){
			System.out.println(getIntervaloPerturbacion().getPstR());
			System.out.println(getIntervaloPerturbacion().getPstS());
			System.out.println(getIntervaloPerturbacion().getPstT());
			ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell, it.getIntervalo());
			ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 1, it.getPstR());
			ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 2, it.getPstS());
			ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 3, it.getPstT());
			ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 4, it.getEneIntervalo());
			intRow++;
		}

		ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
	    
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());

	    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
	    facesContext.responseComplete();

	    return null;
	  }
	
	
	  void writeOutContent(final HttpServletResponse res, final File content, final String theFilename, String contentType) {
		    if (content == null)
		      return;
		    try {
		      res.setHeader("Pragma", "no-cache");
		      res.setDateHeader("Expires", 0);
		      res.setContentType(contentType);
		      res.setHeader("Content-disposition", "attachment; filename=" + theFilename);
		      fastChannelCopy(Channels.newChannel(new FileInputStream(content)), Channels.newChannel(res.getOutputStream()));
		    } catch (final IOException e) {
		      e.printStackTrace(); 
		    } catch (Exception e){
		    	 e.printStackTrace(); 
		    }
	  }	

	  void fastChannelCopy(final ReadableByteChannel src, final WritableByteChannel dest) throws IOException {
	    final ByteBuffer buffer = ByteBuffer.allocateDirect(1000 * 1024);
	    while (src.read(buffer) != -1) {
	      buffer.flip();
	      dest.write(buffer);
	      buffer.compact();
	    }
	    buffer.flip();
	    while (buffer.hasRemaining()){
	      dest.write(buffer);
	    }
	  }
	  
	  public String downloadGraficoArmonicos() {
		    final FacesContext facesContext = FacesContext.getCurrentInstance();
		    
		    String rutaArchivoOut;
		    
		    String rutaUp = null;	    
		    
		    if(medicion.getTipoAlimentacion().getCodTipAlimentacion().equals("MO")){
		    	rutaUp = facesContext.getExternalContext().getRealPath("/reportes/ReporteArmonicosMTBTMono.xlsm");
		    }else{
		    	rutaUp = facesContext.getExternalContext().getRealPath("/reportes/ReporteArmonicosMTBT.xlsm");
		    }
		    
		    String path = System.getProperty("uploads.folder");
		    String nombreArchivo;
		    nombreArchivo = "GraficoArm_" + medicion.getNumSumMedicion() + "_" + new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsm";
		    
		    rutaArchivoOut = path + nombreArchivo;
		    	    
		    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);

			XSSFSheet objHssfSheet = null;
			XSSFSheet objHssfSheet2 = null;
			try  {
				
			
			IntervaloPerturbacionService intervaloPerturbacionService = new IntervaloPerturbacionService();
			
			objHssfSheet = objHssfWorkbook.getSheetAt(0);
			
			objHssfSheet2 = objHssfWorkbook.getSheetAt(1);
			
			int intRow = 1;
			int intCell = 0;
			
			ModificarExcel.setCellValue(objHssfSheet, 6, 3, medicion.getNumSumMedicion());	
			ModificarExcel.setCellValue(objHssfSheet, 7, 3, medicion.getCodSed());
			ModificarExcel.setCellValue(objHssfSheet, 8, 3, medicion.getTenEntregada());
			
			ModificarExcel.setCellValue(objHssfSheet, 9, 3, medicion.getNomUsuario());		
			ModificarExcel.setCellValue(objHssfSheet, 10, 3, medicion.getCampanaMedicion().getPeriodoMedicion().getAno()+"-"+medicion.getCampanaMedicion().getPeriodoMedicion().getPeriodo());
			
			System.out.println(intervaloPerturbacionService.getAllRows1(medicion.getMedicionId()).size());
			
			if(medicion.getTipoAlimentacion().getCodTipAlimentacion().equals("MO")){
				for(IntervaloPerturbacion it:intervaloPerturbacionService.getAllRows1(medicion.getMedicionId())){
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell, it.getIntervalo());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 1, it.getTensionR1());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 2, it.getTensionR2());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 3, it.getTensionR3());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 4, it.getTensionR4());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 5, it.getTensionR5());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 6, it.getTensionR6());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 7, it.getTensionR7());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 8, it.getTensionR8());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 9, it.getTensionR9());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 10, it.getTensionR10());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 11, it.getTensionR11());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 12, it.getTensionR12());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 13, it.getTensionR13());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 14, it.getTensionR14());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 15, it.getTensionR15());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 16, it.getTensionR16());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 17, it.getTensionR17());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 18, it.getTensionR18());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 19, it.getTensionR19());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 20, it.getTensionR20());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 21, it.getTensionR21());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 22, it.getTensionR22());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 23, it.getTensionR23());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 24, it.getTensionR24());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 25, it.getTensionR25());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 26, it.getTensionR26());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 27, it.getTensionR27());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 28, it.getTensionR28());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 29, it.getTensionR29());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 30, it.getTensionR30());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 31, it.getTensionR31());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 32, it.getTensionR32());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 33, it.getTensionR33());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 34, it.getTensionR34());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 35, it.getTensionR35());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 36, it.getTensionR36());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 37, it.getTensionR37());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 38, it.getTensionR38());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 39, it.getTensionR39());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 40, it.getTensionR40());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 121, it.getEneIntervalo());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 122, it.getThdR());
					
					intRow++;
				}
			}else {
				for(IntervaloPerturbacion it:intervaloPerturbacionService.getAllRows1(medicion.getMedicionId())){
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell, it.getIntervalo());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 1, it.getTensionR1());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 2, it.getTensionR2());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 3, it.getTensionR3());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 4, it.getTensionR4());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 5, it.getTensionR5());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 6, it.getTensionR6());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 7, it.getTensionR7());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 8, it.getTensionR8());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 9, it.getTensionR9());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 10, it.getTensionR10());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 11, it.getTensionR11());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 12, it.getTensionR12());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 13, it.getTensionR13());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 14, it.getTensionR14());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 15, it.getTensionR15());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 16, it.getTensionR16());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 17, it.getTensionR17());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 18, it.getTensionR18());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 19, it.getTensionR19());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 20, it.getTensionR20());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 21, it.getTensionR21());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 22, it.getTensionR22());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 23, it.getTensionR23());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 24, it.getTensionR24());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 25, it.getTensionR25());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 26, it.getTensionR26());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 27, it.getTensionR27());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 28, it.getTensionR28());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 29, it.getTensionR29());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 30, it.getTensionR30());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 31, it.getTensionR31());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 32, it.getTensionR32());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 33, it.getTensionR33());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 34, it.getTensionR34());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 35, it.getTensionR35());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 36, it.getTensionR36());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 37, it.getTensionR37());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 38, it.getTensionR38());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 39, it.getTensionR39());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 40, it.getTensionR40());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 41, it.getTensionS1());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 42, it.getTensionS2());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 43, it.getTensionS3());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 44, it.getTensionS4());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 45, it.getTensionS5());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 46, it.getTensionS6());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 47, it.getTensionS7());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 48, it.getTensionS8());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 49, it.getTensionS9());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 50, it.getTensionS10());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 51, it.getTensionS11());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 52, it.getTensionS12());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 53, it.getTensionS13());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 54, it.getTensionS14());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 55, it.getTensionS15());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 56, it.getTensionS16());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 57, it.getTensionS17());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 58, it.getTensionS18());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 59, it.getTensionS19());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 60, it.getTensionS20());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 61, it.getTensionS21());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 62, it.getTensionS22());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 63, it.getTensionS23());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 64, it.getTensionS24());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 65, it.getTensionS25());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 66, it.getTensionS26());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 67, it.getTensionS27());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 68, it.getTensionS28());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 69, it.getTensionS29());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 70, it.getTensionS30());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 71, it.getTensionS31());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 72, it.getTensionS32());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 73, it.getTensionS33());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 74, it.getTensionS34());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 75, it.getTensionS35());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 76, it.getTensionS36());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 77, it.getTensionS37());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 78, it.getTensionS38());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 79, it.getTensionS39());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 80, it.getTensionS40());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 81, it.getTensionT1());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 82, it.getTensionT2());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 83, it.getTensionT3());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 84, it.getTensionT4());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 85, it.getTensionT5());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 86, it.getTensionT6());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 87, it.getTensionT7());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 88, it.getTensionT8());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 89, it.getTensionT9());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 90, it.getTensionT10());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 91, it.getTensionT11());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 92, it.getTensionT12());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 93, it.getTensionT13());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 94, it.getTensionT14());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 95, it.getTensionT15());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 96, it.getTensionT16());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 97, it.getTensionT17());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 98, it.getTensionT18());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 99, it.getTensionT19());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 100, it.getTensionT20());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 101, it.getTensionT21());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 102, it.getTensionT22());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 103, it.getTensionT23());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 104, it.getTensionT24());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 105, it.getTensionT25());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 106, it.getTensionT26());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 107, it.getTensionT27());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 108, it.getTensionT28());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 109, it.getTensionT29());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 110, it.getTensionT30());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 111, it.getTensionT31());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 112, it.getTensionT32());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 113, it.getTensionT33());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 114, it.getTensionT34());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 115, it.getTensionT35());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 116, it.getTensionT36());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 117, it.getTensionT37());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 118, it.getTensionT38());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 119, it.getTensionT39());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 120, it.getTensionT40());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 121, it.getEneIntervalo());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 122, it.getThdR());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 123, it.getThdS());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 124, it.getThdT());
					
					intRow++;
				}
			}

			ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
		    
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());

		    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
		    facesContext.responseComplete();

		    return null;
		  }
	  
	  public String resumenProceso() {
		  String sqlSelect;
		  Sql sql = new Sql("CALIDAD");
		  Object[] objFila;
		  
		  sqlSelect = "SELECT COUNT(*) " +  
				  		"FROM CAL_INTERVALO_PERTURBACION " +
				  		"WHERE PK_MEDICION_ID = " +  String.valueOf(medicion.getMedicionId()) + " AND EN_PER_MEDICION = '1' AND ESTADO = '1' AND " +
				  		"PK_FLI_EST_INTERVALO_ID = (SELECT E.EST_INTERVALO_ID FROM CAL_ESTADO_INTERVALO E WHERE E.COD_EST_INTERVALO = 'F' AND E.ESTADO = '1')";
		  
		  objFila = sql.getFila(sqlSelect);
			
		  if(objFila != null) {
			  cantidadFlicker = ((BigDecimal) objFila[0]).longValue();
		  } else {
			  cantidadFlicker = 0;
		  }
		  
		  sqlSelect = "SELECT COUNT(*) " +  
			  		"FROM CAL_INTERVALO_PERTURBACION " +
			  		"WHERE PK_MEDICION_ID = " +  String.valueOf(medicion.getMedicionId()) + " AND EN_PER_MEDICION = '1' AND ESTADO = '1' AND " +
			  		"PK_ARM_EST_INTERVALO_ID = (SELECT E.EST_INTERVALO_ID FROM CAL_ESTADO_INTERVALO E WHERE E.COD_EST_INTERVALO = 'F' AND E.ESTADO = '1')";
	  
		  objFila = sql.getFila(sqlSelect);
			
		  if(objFila != null) {
			  cantidadThd = ((BigDecimal) objFila[0]).longValue();
		  } else {
			  cantidadThd = 0;
		  }
		  
		  sqlSelect = "SELECT COUNT(*) " +  
			  		"FROM CAL_INTERVALO_PERTURBACION " +
			  		"WHERE PK_MEDICION_ID = " +  String.valueOf(medicion.getMedicionId()) + " AND EN_PER_MEDICION = '1' AND ESTADO = '1' AND " +
			  		"PK_ARMV_EST_INTERVALO_ID = (SELECT E.EST_INTERVALO_ID FROM CAL_ESTADO_INTERVALO E WHERE E.COD_EST_INTERVALO = 'F' AND E.ESTADO = '1')";
	  
		  objFila = sql.getFila(sqlSelect);
			
		  if(objFila != null) {
			  cantidadArmonicos = ((BigDecimal) objFila[0]).longValue();
		  } else {
			  cantidadArmonicos = 0;
		  }

		  sqlSelect = "SELECT ROUND(SUM(COM_FLICKER),2) " +  
			  		"FROM CAL_INTERVALO_PERTURBACION " +
			  		"WHERE PK_MEDICION_ID = " +  String.valueOf(medicion.getMedicionId()) + " AND EN_PER_MEDICION = '1' AND ESTADO = '1' AND " +
			  		"PK_FLI_EST_INTERVALO_ID = (SELECT E.EST_INTERVALO_ID FROM CAL_ESTADO_INTERVALO E WHERE E.COD_EST_INTERVALO = 'F' AND E.ESTADO = '1')";
	  
		  objFila = sql.getFila(sqlSelect);
			
		  if(objFila != null) {
			  if(objFila[0] != null) {
				  compensacionFlicker = (BigDecimal) objFila[0];
			  } else {
				  compensacionFlicker = new BigDecimal(0);
			  }
			
		  } else {
			  compensacionFlicker = new BigDecimal(0);
		  }
		  
		 // limInf = DoubleMath.roundToLong(tenEntregada*(1.05), RoundingMode.HALF_UP);	  
		  
		  
		  sqlSelect = "SELECT ROUND(SUM(COM_ARM),2) " +  
			  		"FROM CAL_INTERVALO_PERTURBACION " +
			  		"WHERE PK_MEDICION_ID = " +  String.valueOf(medicion.getMedicionId()) + " AND EN_PER_MEDICION = '1' AND ESTADO = '1' AND " +
			  		"((PK_ARM_EST_INTERVALO_ID = (SELECT E.EST_INTERVALO_ID FROM CAL_ESTADO_INTERVALO E WHERE E.COD_EST_INTERVALO = 'F' AND ESTADO = '1')) OR " + 
			  		"(PK_ARMV_EST_INTERVALO_ID = (SELECT E.EST_INTERVALO_ID FROM CAL_ESTADO_INTERVALO E WHERE E.COD_EST_INTERVALO = 'F' AND ESTADO = '1')))";
	  
		  objFila = sql.getFila(sqlSelect);
			
		  if(objFila != null) {
			  if(objFila[0] != null) {
				  compensacionArmonicas = (BigDecimal) objFila[0];
			  } else {
				  compensacionArmonicas = new BigDecimal(0);
			  }
		  } else {
			  compensacionArmonicas = new BigDecimal(0);
		  }
		  
		  return "/zonasegura/intervaloPerturbacionResumen?faces-redirect=true";
	  }
}
