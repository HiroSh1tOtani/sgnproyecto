package pe.com.indra.calidad.mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import pe.com.indra.calidad.entity.Rol;
import pe.com.indra.calidad.entity.Usuario;
import pe.com.indra.calidad.service.RolService;
import pe.com.indra.calidad.service.UsuarioService;

@ManagedBean(name="membresiaBean")
@SessionScoped
public class MembresiaBean implements Serializable{

	private List<Rol> selectedRols;
    private Usuario user;
    private List<Rol> availableRols;
	
    @PostConstruct
    public void init(){
    	RolService service = new RolService();
    	availableRols = service.getAllRoles();
    }
    
	public List<Rol> getSelectedRols() {
		return selectedRols;
	}

	public void setSelectedRols(List<Rol> selectedRols) {
		this.selectedRols = selectedRols;
	}
	
	
	public String prepareEditar(Usuario user){
		this.user = user;
		selectedRols = new ArrayList<Rol>();
    	for(Rol available:availableRols){
		   for(Rol rol:user.getRols()){
			   if (available.getRolename().equals(rol.getRolename())) {
				    getSelectedRols().add(available);
		            break;
		        }
		   }	  
		}
    	/*
    	 * The trick is to populate the “left side�? list with all possible values, including 
    	 * the pre-populated “right side�? values.  The framework first renders the result list 
    	 * and removes those matches from the SelectItemList automatically, but only if there is 
    	 * an object to match.
    	 */
    	//getAvailableRols().removeAll(getSelectedRols());
		return "/zonasegura/seguridad/membresiaEditar?faces-redirect=true";		
	}

	public Usuario getUser() {
		return user;
	}

	public void setUser(Usuario user) {
		this.user = user;
	}

	public List<Rol> getAvailableRols() {
       return availableRols;
	}

	public void setAvailableRols(List<Rol> availableRols) {
		this.availableRols = availableRols;
	}
	
	public String update(){
		UsuarioService service = new UsuarioService();
		System.out.println("ROLES SELECCIONADOS:");
		if(getSelectedRols().size()>0){
			for(Rol rol:getSelectedRols()){
				System.out.println(rol.getRolename());
			}		
		   user.setRols(getSelectedRols());
		   service.update(user);
			
			
		} else {
			System.out.println("[ATENCION] No hay roles seleccionados.");
		}
		return "/zonasegura/seguridad/membresiaListar?faces-redirect=true";	
	}
}
