package pe.com.indra.calidad.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.indra.calidad.entity.CronogramaMedicion;
import pe.com.indra.calidad.service.CronogramaMedicionService;

@FacesConverter(value="cronogramaMedicionConverter",forClass=CronogramaMedicionConverter.class)

public class CronogramaMedicionConverter implements Converter{

	@Override
	public CronogramaMedicion getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long campanaId=new Long(cadena);
		CronogramaMedicionService service=new CronogramaMedicionService();
		CronogramaMedicion cronogramaMedicion=new CronogramaMedicion();
		cronogramaMedicion=service.ReadById(campanaId);
		System.out.println(cronogramaMedicion.getCroMedicionId());
		return cronogramaMedicion;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			CronogramaMedicion cronogramaMedicion=(CronogramaMedicion) objeto;
			//System.out.println("objeto:"+new Long(cronogramaMedicion.getCroMedicionId()).toString());
			return new Long(cronogramaMedicion.getCroMedicionId()).toString();
		}
		return null;
	}


}
