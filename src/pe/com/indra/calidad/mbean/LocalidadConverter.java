package pe.com.indra.calidad.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.indra.calidad.entity.Localidad;
import pe.com.indra.calidad.service.LocalidadService;

@FacesConverter(value="localidadConverter",forClass=LocalidadConverter.class)

public class LocalidadConverter implements Converter{

	@Override
	public Localidad getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long tipParametroId=new Long(cadena);
		LocalidadService service=new LocalidadService();
		Localidad localidad=new Localidad();
		localidad=service.ReadById(tipParametroId);
		System.out.println(localidad.getCodLocalidad()+":"+localidad.getNombre());
		return localidad;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			Localidad localidad=(Localidad) objeto;
			//System.out.println("objeto:"+new Long(localidad.getLocalidadId()).toString());
			return new Long(localidad.getLocalidadId()).toString();
		}
		return null;
	}


}
