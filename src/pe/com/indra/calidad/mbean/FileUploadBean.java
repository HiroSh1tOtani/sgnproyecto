package pe.com.indra.calidad.mbean;



import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import org.hibernate.HibernateException;
import org.richfaces.event.FileUploadEvent;
//import org.richfaces.event.UploadEvent;
//import org.richfaces.model.UploadItem;
import org.richfaces.model.UploadedFile;

import com.google.common.math.DoubleMath;

import pe.com.calidad.alumbradopublico.entity.MedicionAp;
import pe.com.calidad.alumbradopublico.entity.ProgramaAp;
import pe.com.calidad.alumbradopublico.service.MedicionApService;
import pe.com.calidad.alumbradopublico.service.ProgramaApService;
import pe.com.indra.calidad.entity.EquipoRegistrador;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.TipoAlimentacion;
import pe.com.indra.calidad.service.EquipoRegistradorService;
import pe.com.indra.calidad.service.MedicionService;
import pe.com.indra.calidad.service.PeriodoMedicionService;
import pe.com.indra.calidad.util.ProcesarArchivo;
import pe.com.indra.calidad.util.Sql;
import pe.com.indra.calidad.util.Utilidad;
import pe.com.indra.calidad.util.ConectaDb;

import java.sql.Connection;
import java.sql.CallableStatement;
import java.sql.SQLException;

@ManagedBean(name="fileUploadBean")
@SessionScoped
public class FileUploadBean implements Serializable{

	    private ArrayList<File> files = new ArrayList<File>();
	   	private int uploadsAvailable = 1;
	    private boolean autoUpload = false;
	    private boolean useFlash = false;
	    //Campos para el proceso del archivo cargado
	    private String columnas;
	    private String nombreColumnas;
	    private long filaInicial;
	    private String separadorColumnas;
		private String formatoFecha;
	    private Long numColumnsFormatoFecha;
	    //El nombre debe estar relacionado con algun identificador
	    private String nombreArchivoDestino;
	    private boolean archivoCargado;
	    private long medicion_id;
	    private long camMedicionId;
		private  BigDecimal facCorTension_temp;
		private  BigDecimal facCorCorriente_temp;
		private Medicion medicion;
		private MedicionAp medicionAp;
		private boolean actualizar;
		private boolean potencia;
		private boolean intercalado;
		private long tipoCronograma;
		private BigDecimal facCorTensionPrim;
		private BigDecimal facCorTensionSec;
		private  BigDecimal facCorCorrientePrim;
		private  BigDecimal facCorCorrienteSec;
		private ProgramaAp programaAp;
		
		private boolean ene_acumulada;
		private boolean wattios;

		private List<SelectItem> itemsNombreColumnas;
		private List<SelectItem> itemsSeparadorColumnas;
		private List<SelectItem> itemsFormatoFecha;
		private List<SelectItem> itemsFormatoFecha2;
		private List<SelectItem> itemsNombreColumnas2;
		private List<SelectItem> itemsNombreColumnas3;
		private List<SelectItem> itemsNombreColumnasAp;
		private List<SelectItem> itemsNombreColumnasAp2;
	

	public Medicion getMedicion() {
			return medicion;
		}

		public void setMedicion(Medicion medicion) {
			this.medicion = medicion;
		}

	@PostConstruct
	   public void init(){
		   nombreArchivoDestino = "mediciones"+new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".txt";
		   actualizar = false;
		   filaInicial = 2;
		   columnas = "1,2,3";
	   }
	    
	    public String getColumnas() {
			return columnas;
		}

		public void setColumnas(String columnas) {
			this.columnas = columnas;
		}

		public String getNombreColumnas() {
			return nombreColumnas;
		}

		public void setNombreColumnas(String nombreColumnas) {
			this.nombreColumnas = nombreColumnas;
		}

		public Long getFilaInicial() {
			return filaInicial;
		}

		public void setFilaInicial(Long filaInicial) {
			this.filaInicial = filaInicial;
		}

	    public String getSeparadorColumnas() {
			return separadorColumnas;
		}

		public void setSeparadorColumnas(String separadorColumnas) {
			this.separadorColumnas = separadorColumnas;
		}
		
		public String getFormatoFecha() {
			return formatoFecha;
		}

		public void setFormatoFecha(String formatoFecha) {
			this.formatoFecha = formatoFecha;
		}

		public Long getNumColumnsFormatoFecha() {
			return numColumnsFormatoFecha;
		}

		public void setNumColumnsFormatoFecha(Long numColumnsFormatoFecha) {
			this.numColumnsFormatoFecha = numColumnsFormatoFecha;
		}

		public boolean isActualizar() {
			return actualizar;
		}

		public void setActualizar(boolean actualizar) {
			this.actualizar = actualizar;
		}

		public int getSize() {
	        if (getFiles().size()>0){
	            return getFiles().size();
	        }else 
	        {
	            return 0;
	        }
	    }

	    public FileUploadBean() {
	    }

	    public void paint(OutputStream stream, Object object) throws Exception {
	       // stream.write(getFiles().get((Integer)object).getData());
	         System.out.println("Archivo Guardado con Exito!");;	
	    }
	    
	    public void listener(FileUploadEvent event) throws Exception{
	    	if( event.getUploadedFile() == null ){
	    		throw new Exception("ERROR: No se ha cargado con exito el archivo, UploadItem es nulo o File es nulo");
	    	} else {
	    		UploadedFile item=event.getUploadedFile();
	    		String fileName = item.getName();
	    		this.writeFileOnServer(item);	
		    	System.out.println("[Atencion] Se cargo el archivo "+fileName+" con exito en el sistema");
	    	}	
	    }  
	    
	    public void writeFileOnServer(UploadedFile item) throws Exception{
	    	String path = System.getProperty("uploads.folder");
	        FileWriter fileWriter = new FileWriter(path+this.getNombreArchivoDestino());
	        DataInputStream dataInputStream = new DataInputStream(item.getInputStream());
            //BufferedReader br = new BufferedReader(new InputStreamReader(in));
	        //BufferedReader reader = new BufferedReader(new FileReader(file));
	        BufferedReader reader = new BufferedReader(new InputStreamReader(dataInputStream));
	        BufferedWriter writer =    new BufferedWriter(fileWriter);
	        try {
	            String line = reader.readLine();
	            while (line != null) {
	                System.out.println(line);
	                writer.write(line);
	                writer.newLine();
	                line = reader.readLine();
	            }	            
	        } finally {
	            reader.close();
	            writer.close();
	        }	        
	    }
	    
	    public String clearUploadData() {
	        files.clear();
	        setUploadsAvailable(5);
	        return null;
	    }
	    
	    public long getTimeStamp(){
	        return System.currentTimeMillis();
	    }
	    
	    

	    public int getUploadsAvailable() {
	        return uploadsAvailable;
	    }

	    public void setUploadsAvailable(int uploadsAvailable) {
	        this.uploadsAvailable = uploadsAvailable;
	    }

	    public boolean isAutoUpload() {
	        return autoUpload;
	    }

	    public void setAutoUpload(boolean autoUpload) {
	        this.autoUpload = autoUpload;
	    }

	    public boolean isUseFlash() {
	        return useFlash;
	    }

	    public void setUseFlash(boolean useFlash) {
	        this.useFlash = useFlash;
	    }
	    
	    public ArrayList<File> getFiles() {
			return files;
		}

		public void setFiles(ArrayList<File> files) {
			this.files = files;
		}
	
		
		public String getNombreArchivoDestino() {
			return nombreArchivoDestino;
		}

		public void setNombreArchivoDestino(String nombreArchivoDestino) {
			this.nombreArchivoDestino = nombreArchivoDestino;
		}

		public boolean isArchivoCargado() {
			String path = System.getProperty("uploads.folder"); //directorio donde esta el archivo
		    String fullNameArchivo=path+this.getNombreArchivoDestino(); //esta es la ruta completa
		    File file=new File(fullNameArchivo);
		    if( file.exists() ){
		    	setArchivoCargado(true);
		    } else { 
		    	setArchivoCargado(false);
		    }
			return archivoCargado;
		}

		public void setArchivoCargado(boolean archivoCargado) {
			this.archivoCargado = archivoCargado;
		}
		
		//acciones
		public String procesarArchivoCargado() throws Exception{
			String campos[];
			String path = System.getProperty("uploads.folder"); 
		    String fullNameArchivo=path+getNombreArchivoDestino(); 
		    String nombreColumnasAux;
		    String columnasAux;
		    int opcion;
		    
		    if( isArchivoCargado() ){
		    	System.out.println("El archivo esta cargado!, aqui lucho poner la llamada a tus metodos");
		    	// TODO: Luchito aqui llamar a los metodos para actualizar la BD
		    	
		    	opcion = Integer.valueOf(nombreColumnas);
		    	switch (opcion) {
				case 1:
					nombreColumnasAux = "INTERVALO, TENSION_R, TENSION_S, TENSION_T";
					break;
				case 2:
					nombreColumnasAux = "INTERVALO, TENSION_R, TENSION_S, TENSION_T, ENERGIA_R, ENERGIA_S, ENERGIA_T";
					break;
				case 3:
					nombreColumnasAux = "INTERVALO, TENSION_R, TENSION_S, TENSION_T, ENE_INTERVALO";
					break;
				case 4:
					nombreColumnasAux = "INTERVALO, TENSION_R, ENERGIA_R";
					break;
				case 5:
					nombreColumnasAux = "INTERVALO, TENSION_R";
					break;
				case 6:
					nombreColumnasAux = "INTERVALO, TENSION_R, TENSION_S, TENSION_T, ARMONICAS, FLICKER";
					break;
				case 7:
					nombreColumnasAux = "INTERVALO, TENSION_R, TENSION_S, TENSION_T, ENERGIA_R, ENERGIA_S, ENERGIA_T, ARMONICAS, FLICKER";
					break;
				case 8:
					nombreColumnasAux = "INTERVALO, TENSION_R, TENSION_S, TENSION_T, ENE_INTERVALO, ARMONICAS, FLICKER";
					break;
				case 9:
					nombreColumnasAux = "INTERVALO, TENSION_R, ENERGIA_R, ARMONICAS, FLICKER";
					break;
				case 10:
					nombreColumnasAux = "INTERVALO, TENSION_R, ARMONICAS, FLICKER";
					break;
				default:
					nombreColumnasAux = "";
					break;
				}    	
		    	
		    	campos = getColumnas().trim().split(",");
		    	
		    	columnasAux = "";
		    	for (String palabra : campos) {
					if(palabra.contains("-")){
						int inicio = Integer.valueOf(palabra.trim().substring(0, palabra.trim().indexOf("-")));
						int fin = Integer.valueOf(palabra.trim().substring(palabra.trim().indexOf("-") + 1));
						
						for(int n=inicio; n <= fin; n++) {
							columnasAux = columnasAux + String.valueOf(n).trim() + ",";
				        }
						
					}else {
						columnasAux = columnasAux + palabra + ",";
					}
				}
		    	
		    	columnasAux = columnasAux.trim().substring(0,columnasAux.length() - 1);
		    	
		    	
		    	ProcesarArchivo procesarArchivo = new ProcesarArchivo(fullNameArchivo,"", separadorColumnas,"",nombreColumnasAux,columnasAux,filaInicial);
		    	List<String[]> ca = null;
		    	
		    	try {
		    		ca = procesarArchivo.cargar();
		    		
				} catch (Exception e) {

					e.printStackTrace();

	    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No es posible cargar el archivo, verificar el formato del mismo", "No es posible cargar el archivo, verificar el formato del mismo"));
	    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	    			
	    			return "/zonasegura/medicion/cargaDatosMediciones?faces-redirect=true";
	    			
				}
		    		
		    	
		    	System.out.println(procesarArchivo.getRuta() + " " + procesarArchivo.getTabla() + " " + procesarArchivo.getCampos()+ " " + Long.toString(medicion_id)+ " " + formatoFecha+ " " + Long.toString(numColumnsFormatoFecha));
		    	
		    
		    	
		    	String resultado = procesarArchivo.insertarIntervaloTension(procesarArchivo.getTabla(), procesarArchivo.getCampos(), ca, medicion_id, formatoFecha, numColumnsFormatoFecha,potencia,opcion,ene_acumulada);
				
		    	if(resultado != null) {
		    		
	    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No es posible insertar los registros de tension, verificar el formato del archivo", "No es posible insertar los registros de tension, verificar el formato del archivo"));
	    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	    			
	    			return "/zonasegura/medicion/cargaDatosMediciones?faces-redirect=true";
	    			
		    	}
		    	
		    	
		    	
		    	String result = null;
		    	ConectaDb db = new ConectaDb("CALIDAD");
				Connection cn = db.getConnection();
		        CallableStatement cstmt = null; 
		        
		    	try {

		    		Sql sql = new Sql("CALIDAD"); 
			        String updSql = "UPDATE CAL_MEDICION SET FAC_COR_TENSION = " + facCorTension_temp + " , " + " FAC_COR_CORRIENTE = " + facCorCorriente_temp + " WHERE MEDICION_ID = " + medicion_id;
			        sql.ejecuta(updSql);
			        
		    		/*
			        cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.EXEC_PREPARAR_MEDICION(?,?,?)}");  
					cstmt.setLong(1, medicion_id);
					cstmt.setLong(2, 1);
					cstmt.setTimestamp(3, null);
					*/
			        
			        System.out.println("Entrando al Execute");
			        
		    		cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.EXEC_PREPARAR_MEDICION2(?,?)}");  
					cstmt.setLong(1, medicion_id);
					cstmt.setLong(2, 1);
					
					int ctos = cstmt.executeUpdate();
		            cstmt.close(); 
		            
		            System.out.println("Saliendo del Execute");
		            
		            if (ctos == 0) {
		                result = "0 filas afectadas";
		            }
		         
		    	} catch (SQLException e) {
		            result = e.getMessage();
		            
		            e.printStackTrace();

		        } finally {
		            try {
		                cn.close();
		            } catch (SQLException e) {
		                result = e.getMessage();
		            }
		        }
		    	
				System.out.print(result);		    	
		    
		    } else { 
				throw new Exception("No existe el archivo destino, asegurese de cargarlo correctamente.");	
		    }
		    return "/zonasegura/medicionListar?faces-redirect=true"; //retornar a la pagina que deseas por Ejemplo a medicionListar.xhtml donde esta el boton
		}
		
		public String prepararMostrar(){
			//TODO: Leer el parametro para el nombre del archivo
			this.setNombreArchivoDestino("mediciones"+new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".txt");
			
			
			String medicion_id_string = Utilidad.getParametro("medicionId");			
			medicion_id = new Long(medicion_id_string);
			
			MedicionService service = new MedicionService();
			EquipoRegistradorService serviceEquipo = new EquipoRegistradorService();
			
			medicion = service.ReadById(medicion_id); //
			
			EquipoRegistrador equipo = null;
			
			if(medicion.getTenEntregada() <= 0){
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Debe asignar primero la tension del punto.", "Debe asignar primero la tension del punto."));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				
				return "/zonasegura/medicionListar?faces-redirect=true";
			}
			
			if(medicion.getTipoAlimentacion() == null || medicion.getTipoAlimentacion().getCodTipAlimentacion().equals("NA")){
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Debe asignar primero el tipo de alimentacion del punto.", "Debe asignar primero el tipo de alimentacion del punto."));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				
				return "/zonasegura/medicionListar?faces-redirect=true";
			}
			
			if(medicion.getTipServicio() == null || medicion.getTipServicio().equals("N")){
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Debe asignar primero el tipo de servicio del punto.", "Debe asignar primero el tipo de servicio del punto."));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				
				return "/zonasegura/medicionListar?faces-redirect=true";
			}
			
			if(medicion.getCronogramaMedicion() == null || medicion.getCronogramaMedicion().size() == 0){
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Debe ingresar primero la fecha planificada en el cronograma.", "Debe ingresar primero la fecha planificada en el cronograma."));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				
				return "/zonasegura/medicionListar?faces-redirect=true";
			}
			
			try {
				equipo = serviceEquipo.ReadById(medicion.getEquipoRegistrador().getEquRegistradorId());
				
			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Debe asignar primero equipo de medicion.", e.getMessage()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				
				return "/zonasegura/medicionListar?faces-redirect=true";
			}
							
			if(equipo!=null) {
				nombreColumnas = equipo.getNombreColumnas();
				columnas = equipo.getColumnas();
				filaInicial =  (long) equipo.getFilaInicial();
			    separadorColumnas = equipo.getSeparadorColumnas();
				formatoFecha = equipo.getFormatoFecha();
			    numColumnsFormatoFecha = (long) equipo.getNumeroColumnasFecha();
			}
			
			Sql sql = new Sql("CALIDAD");
			Object[] fila = sql.getFila("SELECT FAC_COR_TENSION,FAC_COR_CORRIENTE FROM CAL_MEDICION WHERE MEDICION_ID = " + String.valueOf(medicion_id) + "");
			facCorTension_temp = (BigDecimal) fila[0];
			facCorCorriente_temp = (BigDecimal) fila[1];
			facCorCorrientePrim = null;
			facCorCorrienteSec = null;
			facCorTensionPrim = null;
			facCorTensionSec  = null;
			
			/*
			Sql sql = new Sql("CALIDAD");
			
			Object[] fila = sql.getFila("SELECT PK_EQU_REGISTRADOR_ID FROM CAL_MEDICION WHERE MEDICION_ID = " + String.valueOf(medicion_id) + "");
			
			String registradorId = String.valueOf((BigDecimal) fila[0]);
			
			Object[] fila2 = sql.getFila("SELECT NOMBRE_COLUMNAS, COLUMNAS, FILA_INICIAL, SEPARADOR_COLUMNAS, FORMATO_FECHA, NUMERO_COLUMNAS_FECHA FROM CAL_EQUIPO_REGISTRADOR WHERE EQU_REGISTRADOR_ID = " + registradorId + "");
			
			if(fila2!=null) {
				nombreColumnas = (String) fila2[0];
				columnas = (String) fila2[1];
				filaInicial =  ((BigDecimal) fila2[2]).longValue();
			    separadorColumnas = (String) fila2[3];
				formatoFecha = (String) fila2[4];
			    numColumnsFormatoFecha = ((BigDecimal) fila2[5]).longValue();//Integer.parseInt((String) fila2[6]);
			}*/
			
			return "/zonasegura/medicion/cargaDatosMediciones?faces-redirect=true";
		}
		//mvargas - presente en el ultimo fuente de produccion
		public String prepararMostrarSuministro(long tipo){
			//TODO: Leer el parametro para el nombre del archivo
			this.setNombreArchivoDestino("mediciones"+new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".txt");
			
			tipoCronograma = tipo;//1 = Urbano, 2 = Rural
			/*
			String campanaId_string = Utilidad.getParametro("campanaIdx");	
			System.out.println("Campana id:" + campanaId_string);
			setCamMedicionId(new Long(campanaId_string));
			System.out.println("Campana id:" + Long.toString(camMedicionId));*/
			
			return "/zonasegura/cargaDatosSuministros?faces-redirect=true";
		}
		
		public String prepararMostrarPerturbacion(){
			//TODO: Leer el parametro para el nombre del archivo
			this.setNombreArchivoDestino("perturbaciones"+new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".txt");
			
			
			String medicion_id_string = Utilidad.getParametro("medicionId");			
			medicion_id = new Long(medicion_id_string);
			
			MedicionService service = new MedicionService();
			EquipoRegistradorService serviceEquipo = new EquipoRegistradorService();
			
			medicion = service.ReadById(medicion_id); //
			
			EquipoRegistrador equipo = null;
			
			if(medicion.getTenEntregada() <= 0){
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Debe asignar primero la tension del punto.", "Debe asignar primero la tension del punto."));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				
				return "/zonasegura/medicionListar?faces-redirect=true";
			}
			
			if(medicion.getTipoAlimentacion() == null || medicion.getTipoAlimentacion().getCodTipAlimentacion().equals("NA")){
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Debe asignar primero el tipo de alimentacion del punto.", "Debe asignar primero el tipo de alimentacion del punto."));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				
				return "/zonasegura/medicionListar?faces-redirect=true";
			}
			
			if(medicion.getTipServicio() == null || !medicion.getTipServicio().equals("U")){
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Debe asignar primero el tipo de servicio urbano al punto.", "Debe asignar primero el tipo de servicio urbano al punto."));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				
				return "/zonasegura/medicionListar?faces-redirect=true";
			}
			
			if(medicion.getCronogramaMedicion() == null || medicion.getCronogramaMedicion().size() == 0){
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Debe ingresar primero la fecha planificada en el cronograma.", "Debe ingresar primero la fecha planificada en el cronograma."));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				
				return "/zonasegura/medicionListar?faces-redirect=true";
			}
			
			try {
				equipo = serviceEquipo.ReadById(medicion.getEquipoRegistrador().getEquRegistradorId());
				
			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Debe asignar primero equipo de medicion.", e.getMessage()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				
				return "/zonasegura/medicionListar?faces-redirect=true";
			}
							
			if(equipo!=null) {
				//nombreColumnas = equipo.getNombreColumnas();
				columnas = "1-127";
				filaInicial =  (long) equipo.getFilaInicial();
			    separadorColumnas = equipo.getSeparadorColumnas();
				formatoFecha = equipo.getFormatoFecha();
			    numColumnsFormatoFecha = (long) equipo.getNumeroColumnasFecha();
			}
			
			Sql sql = new Sql("CALIDAD");
			Object[] fila = sql.getFila("SELECT FAC_COR_TENSION,FAC_COR_CORRIENTE FROM CAL_MEDICION WHERE MEDICION_ID = " + String.valueOf(medicion_id) + "");
			facCorTension_temp = (BigDecimal) fila[0];
			facCorCorriente_temp = (BigDecimal) fila[1];
			facCorCorrientePrim = null;
			facCorCorrienteSec = null;
			facCorTensionPrim = null;
			facCorTensionSec  = null;
			
			
			return "/zonasegura/medicion/cargaDatosPerturbaciones?faces-redirect=true";
		}
		
		public String prepararMostrarMedicionAp(){
			//TODO: Leer el parametro para el nombre del archivo
			long medicionApId;
			this.setNombreArchivoDestino("medicionesAp"+new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".txt");
			
			
			String medicion_ap_id_string = Utilidad.getParametro("medicionApId");			
			medicionApId = new Long(medicion_ap_id_string);
			
			MedicionApService service = new MedicionApService();
			
			setMedicionAp(service.ReadById(medicionApId)); //
			
			
			if(medicionAp.getViaVano() == null){
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Debe asignar primero el vano.", "Debe asignar primero el vano."));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				
				return "/zonasegura/medicionApListar?faces-redirect=true";
			}
			
			
			return "/zonasegura/alumbradoPublico/cargaDatosMedicionesAp?faces-redirect=true";
		}		

		public String prepararMostrarMedicionesAp(){
			//TODO: Leer el parametro para el nombre del archivo
			long programaApId;
			this.setNombreArchivoDestino("medicionesAp"+new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".txt");
			
			
			String programa_ap_id_string = Utilidad.getParametro("programaApId");			
			programaApId = new Long(programa_ap_id_string);
			
			ProgramaApService service = new ProgramaApService();
			
			setProgramaAp(service.ReadById(programaApId)); //
			
			return "/zonasegura/alumbradoPublico/cargaDatosMedicionesAp2?faces-redirect=true";
		}		
		
		public long getMedicion_id() {
			return medicion_id;
		}

		public void setMedicion_id(long medicion_id) {
			this.medicion_id = medicion_id;
		}
		
		
		public String procesarArchivoCargadoSuministro() throws Exception{
			String campos[];
			String path = System.getProperty("uploads.folder"); 
		    String fullNameArchivo=path+getNombreArchivoDestino(); 
		    System.out.print(fullNameArchivo);
		    
		    String columnasAux;
		    
		    String campanaId_string = Utilidad.getParametro("campanaIdx");	
			setCamMedicionId(new Long(campanaId_string));
			
		    if( isArchivoCargado() ){
		    	System.out.println("El archivo esta cargado!, aqui lucho poner la llamada a tus metodos");
		    	// TODO: Luchito aqui llamar a los metodos para actualizar la BD
		    	
		    	campos = getColumnas().trim().split(",");
		    	
		    	columnasAux = "";
		    	for (String palabra : campos) {
					if(palabra.contains("-")){
						int inicio = Integer.valueOf(palabra.trim().substring(0, palabra.trim().indexOf("-")));
						int fin = Integer.valueOf(palabra.trim().substring(palabra.trim().indexOf("-") + 1));
						
						for(int n=inicio; n <= fin; n++) {
							columnasAux = columnasAux + String.valueOf(n).trim() + ",";
				        }
						
					}else {
						columnasAux = columnasAux + palabra + ",";
					}
				}
		    	
		    	columnasAux = columnasAux.trim().substring(0,columnasAux.length() - 1);
		    	
		    	
		    	ProcesarArchivo procesarArchivo = new ProcesarArchivo(fullNameArchivo,"", separadorColumnas,"",nombreColumnas,columnasAux,filaInicial);
		    	List<String[]> ca = procesarArchivo.cargar();
		    	
		    	System.out.println(procesarArchivo.getRuta() + " " + procesarArchivo.getTabla() + " " + procesarArchivo.getCampos()+ " " + Long.toString(camMedicionId)+""+ formatoFecha);
		    	
		    	procesarArchivo.insertarMedicion(procesarArchivo.getTabla(), procesarArchivo.getCampos(), ca, camMedicionId,tipoCronograma);
		    
		    	//System.out.println(procesarArchivo.getRuta() + " " + procesarArchivo.getTabla() + " " + procesarArchivo.getCampos()+ " " + Long.toString(camMedicionId)+""+ formatoFecha);
		    	procesarArchivo.insertarCronograma("CAL_CRONOGRAMA_MEDICION", procesarArchivo.getCampos(), ca, camMedicionId,formatoFecha,tipoCronograma);	    
		    } else { 
		      throw new Exception("No existe el archivo destino, asegurese de cargarlo correctamente.");	
		    }
		    return "/zonasegura/medicionListar?faces-redirect=true"; //retornar a la pagina que deseas por Ejemplo a medicionListar.xhtml donde esta el boton
		    
		}

		public String procesarArchivoCargadoMedicionAp() throws Exception{
			String campos[];
			String path = System.getProperty("uploads.folder"); 
		    String fullNameArchivo=path+getNombreArchivoDestino(); 
		    System.out.print(fullNameArchivo);
		    
		    String columnasAux;
		    int opcion;
		    String nombreColumnasAux;		    
			
		    if( isArchivoCargado() ){
		    	System.out.println("El archivo esta cargado!, aqui lucho poner la llamada a tus metodos");
		    	// TODO: Luchito aqui llamar a los metodos para actualizar la BD
		    	
		    	opcion = Integer.valueOf(nombreColumnas);
		    	switch (opcion) {
				case 1:
					nombreColumnasAux = "M1, LUGAR, I1, I2, I3, I4, I5, M2, L1, L2, L3, L4, L5, NUM_LUM_POR_KM, SUP_APARENTE, INT_LUMINOSA, REL_LUMINOSA";
					break;
				default:
					nombreColumnasAux = "";
					break;
				}    	
		    	
		    	campos = getColumnas().trim().split(",");
		    	
		    	columnasAux = "";		    	
		    	
		    	for (String palabra : campos) {
					if(palabra.contains("-")){
						int inicio = Integer.valueOf(palabra.trim().substring(0, palabra.trim().indexOf("-")));
						int fin = Integer.valueOf(palabra.trim().substring(palabra.trim().indexOf("-") + 1));
						
						for(int n=inicio; n <= fin; n++) {
							columnasAux = columnasAux + String.valueOf(n).trim() + ",";
				        }
						
					}else {
						columnasAux = columnasAux + palabra + ",";
					}
				}
		    	
		    	columnasAux = columnasAux.trim().substring(0,columnasAux.length() - 1);
		    	
		    	
		    	ProcesarArchivo procesarArchivo = new ProcesarArchivo(fullNameArchivo,"", separadorColumnas,"",nombreColumnasAux,columnasAux,filaInicial);
		    	List<String[]> ca = procesarArchivo.cargar();
		    	
		    	procesarArchivo.setTabla("CAL_MEDICION_AP");
		    	
		    	procesarArchivo.actualizarMedicionAp(procesarArchivo.getTabla(), procesarArchivo.getCampos(), ca, medicionAp.getMedicionApId());
		    
		    	procesarArchivo.setTabla("CAL_PUNTO_AP");
		    	
		    	procesarArchivo.insertarPuntoAp(procesarArchivo.getTabla(), procesarArchivo.getCampos(), ca, medicionAp.getMedicionApId());	    
		    } else { 
		      throw new Exception("No existe el archivo destino, asegurese de cargarlo correctamente.");	
		    }
		    return "/zonasegura/alumbradoPublico/medicionApListar?faces-redirect=true"; //retornar a la pagina que deseas por Ejemplo a medicionListar.xhtml donde esta el boton
		    
		}
		
		public String procesarArchivoCargadoMedicionesAp() throws Exception{
			String campos[];
			String path = System.getProperty("uploads.folder"); 
		    String fullNameArchivo=path+getNombreArchivoDestino(); 
		    System.out.print(fullNameArchivo);
		    
		    String columnasAux;
		    int opcion;
		    String nombreColumnasAux;		    
			
		    if( isArchivoCargado() ){
		    	System.out.println("El archivo esta cargado!, aqui lucho poner la llamada a tus metodos");
		    	// TODO: Luchito aqui llamar a los metodos para actualizar la BD
		    	
		    	opcion = Integer.valueOf(nombreColumnas);
		    	switch (opcion) {
				case 1:
					nombreColumnasAux = "COD_VIA, NRO_VANO, M1, LUGAR, I1, I2, I3, I4, I5, M2, L1, L2, L3, L4, L5, NUM_LUM_POR_KM, SUP_APARENTE, INT_LUMINOSA, REL_LUMINOSA";
					break;
				default:
					nombreColumnasAux = "";
					break;
				}    	
		    	
		    	campos = getColumnas().trim().split(",");
		    	
		    	columnasAux = "";		    	
		    	
		    	for (String palabra : campos) {
					if(palabra.contains("-")){
						int inicio = Integer.valueOf(palabra.trim().substring(0, palabra.trim().indexOf("-")));
						int fin = Integer.valueOf(palabra.trim().substring(palabra.trim().indexOf("-") + 1));
						
						for(int n=inicio; n <= fin; n++) {
							columnasAux = columnasAux + String.valueOf(n).trim() + ",";
				        }
						
					}else {
						columnasAux = columnasAux + palabra + ",";
					}
				}
		    	
		    	columnasAux = columnasAux.trim().substring(0,columnasAux.length() - 1);
		    	
		    	
		    	ProcesarArchivo procesarArchivo = new ProcesarArchivo(fullNameArchivo,"", separadorColumnas,"",nombreColumnasAux,columnasAux,filaInicial);
		    	List<String[]> ca = procesarArchivo.cargar();
		    	
		    	procesarArchivo.setTabla("CAL_MEDICION_AP");
		    	
		    	procesarArchivo.actualizarMedicionesAp(procesarArchivo.getTabla(), procesarArchivo.getCampos(), ca, programaAp.getProgramaApId());
		    
		    	procesarArchivo.setTabla("CAL_PUNTO_AP");
		    	
		    	procesarArchivo.insertarPuntosAp(procesarArchivo.getTabla(), nombreColumnasAux, ca, programaAp.getProgramaApId());	    
		    } else { 
		      throw new Exception("No existe el archivo destino, asegurese de cargarlo correctamente.");	
		    }
		    return "/zonasegura/alumbradoPublico/medicionApListar?faces-redirect=true"; //retornar a la pagina que deseas por Ejemplo a medicionListar.xhtml donde esta el boton
		    
		}			
		
		public long getCamMedicionId() {
			return camMedicionId;
		}

		public void setCamMedicionId(long camMedicionId) {
			this.camMedicionId = camMedicionId;
		}

		public BigDecimal getFacCorTension_temp() {
			return facCorTension_temp;
		}

		public void setFacCorTension_temp(BigDecimal facCorTension_temp) {
			this.facCorTension_temp = facCorTension_temp;
		}

		public BigDecimal getFacCorCorriente_temp() {
			return facCorCorriente_temp;
		}

		public void setFacCorCorriente_temp(BigDecimal facCorCorriente_temp) {
			this.facCorCorriente_temp = facCorCorriente_temp;
		}
		
		public List<SelectItem> getItemsNombreColumnas() {
			itemsNombreColumnas=new ArrayList<SelectItem>();
			itemsNombreColumnas.add(new SelectItem("1","INTERVALO, TENSION_R, TENSION_S, TENSION_T"));
			itemsNombreColumnas.add(new SelectItem("2","INTERVALO,TENSION_R, TENSION_S, TENSION_T, ENERGIA_R, ENERGIA_S, ENERGIA_T"));
			itemsNombreColumnas.add(new SelectItem("3","INTERVALO,TENSION_R, TENSION_S, TENSION_T, ENERGIA_INTERVALO"));
			itemsNombreColumnas.add(new SelectItem("4","INTERVALO, TENSION_R, ENERGIA_R"));
			itemsNombreColumnas.add(new SelectItem("5","INTERVALO, TENSION_R"));
			
		
			itemsNombreColumnas.add(new SelectItem("6","INTERVALO, TENSION_R, TENSION_S, TENSION_T, ARMONICAS, FLICKER"));
			itemsNombreColumnas.add(new SelectItem("7","INTERVALO, TENSION_R, TENSION_S, TENSION_T, ENERGIA_R, ENERGIA_S, ENERGIA_T, ARMONICAS, FLICKER"));
			itemsNombreColumnas.add(new SelectItem("8","INTERVALO, TENSION_R, TENSION_S, TENSION_T, ENE_INTERVALO, ARMONICAS, FLICKER"));
			itemsNombreColumnas.add(new SelectItem("9","INTERVALO, TENSION_R, ENERGIA_R, ARMONICAS, FLICKER"));
			itemsNombreColumnas.add(new SelectItem("10","INTERVALO, TENSION_R, ARMONICAS, FLICKER"));
			
			return itemsNombreColumnas;
		}

		public void setItemsNombreColumnas(List<SelectItem> itemsNombreColumnas) {
			this.itemsNombreColumnas = itemsNombreColumnas;
		}
		
		public List<SelectItem> getItemsSeparadorColumnas() {
			itemsSeparadorColumnas=new ArrayList<SelectItem>();
			itemsSeparadorColumnas.add(new SelectItem("\t","TAB"));
			itemsSeparadorColumnas.add(new SelectItem("","ESPACIO"));
			itemsSeparadorColumnas.add(new SelectItem(",","COMA"));
			itemsSeparadorColumnas.add(new SelectItem("|","BARRA VERTICAL"));
			itemsSeparadorColumnas.add(new SelectItem(";","PUNTO Y COMA"));
			return itemsSeparadorColumnas;
		}

		public void setItemsSeparadorColumnas(List<SelectItem> itemsSeparadorColumnas) {
			this.itemsSeparadorColumnas = itemsSeparadorColumnas;
		}

		public List<SelectItem> getItemsFormatoFecha() {
			itemsFormatoFecha=new ArrayList<SelectItem>();
			itemsFormatoFecha.add(new SelectItem("DD/MM/YYYY HH24:MI","DD/MM/YYYY HH24:MI"));
			itemsFormatoFecha.add(new SelectItem("DD/MM/YYYY HH24:MI:SS.FF","DD/MM/YYYY HH24:MI:SS.FF"));
			itemsFormatoFecha.add(new SelectItem("DD/MM/YYYY HH:MI AM","DD/MM/YYYY HH:MI AM"));
			itemsFormatoFecha.add(new SelectItem("DD/MM/YYYY HH:MI:SS.FF AM","DD/MM/YYYY HH:MI:SS.FF AM"));			
			itemsFormatoFecha.add(new SelectItem("DD/MM/YY HH24:MI","DD/MM/YY HH24:MI"));
			itemsFormatoFecha.add(new SelectItem("DD/MM/YY HH24:MI:SS.FF","DD/MM/YY HH24:MI:SS.FF"));
			itemsFormatoFecha.add(new SelectItem("DD/MM/YY HH:MI AM","DD/MM/YY HH:MI AM"));
			itemsFormatoFecha.add(new SelectItem("DD/MM/YY HH:MI:SS.FF AM","DD/MM/YY HH:MI:SS.FF AM"));
			return itemsFormatoFecha;
		}

		public void setItemsFormatoFecha(List<SelectItem> itemsFormatoFecha) {
			this.itemsFormatoFecha = itemsFormatoFecha;
		}
		
		
		public List<SelectItem> getItemsFormatoFecha2() {
			itemsFormatoFecha2=new ArrayList<SelectItem>();
			itemsFormatoFecha2.add(new SelectItem("DD/MM/YYYY","DD/MM/YYYY"));
			itemsFormatoFecha2.add(new SelectItem("DD/MM/YYYY HH:MI","DD/MM/YYYY HH:MI"));
			itemsFormatoFecha2.add(new SelectItem("DD/MM/YYYY HH:MI:SS","DD/MM/YYYY HH:MI:SS"));	
			itemsFormatoFecha2.add(new SelectItem("DD/MM/YY","DD/MM/YY"));
			itemsFormatoFecha2.add(new SelectItem("DD/MM/YY HH:MI","DD/MM/YY HH:MI"));
			itemsFormatoFecha2.add(new SelectItem("DD/MM/YY HH:MI:SS","DD/MM/YY HH:MI:SS"));
			return itemsFormatoFecha2;
		}

		public void setItemsFormatoFecha2(List<SelectItem> itemsFormatoFecha2) {
			this.itemsFormatoFecha2 = itemsFormatoFecha2;
		}
		
		public List<SelectItem> getItemsNombreColumnas2() {
			itemsNombreColumnas2=new ArrayList<SelectItem>();
			itemsNombreColumnas2.add(new SelectItem("1","SUMINISTRO,FEC_PLANIFICADA,TIP_PUNTO,TIP_ALIMENTACION,PARAMETRO_MEDIDO"));
			itemsNombreColumnas2.add(new SelectItem("2","SUMINISTRO_CABECERA, SUMINISTRO_COLA,FEC_PLANIFICADA,TIP_PUNTO"));
			return itemsNombreColumnas2;
		}

		public void setItemsNombreColumnas2(List<SelectItem> itemsNombreColumnas2) {
			this.itemsNombreColumnas2 = itemsNombreColumnas2;
		}
		
		public List<SelectItem> getItemsNombreColumnas3() {
			itemsNombreColumnas3=new ArrayList<SelectItem>();
			itemsNombreColumnas3.add(new SelectItem("1","ORDEN: FECHA-HORA, ARMONICOS(1-40), FLICKER Y ENERGIA DE LAS FASES RST"));
			itemsNombreColumnas3.add(new SelectItem("15","ORDEN: FECHA-HORA, ARMONICOS(1-40) Y FLICKER DE LAS FASES RST MAS ENERGIA TOTAL"));
			itemsNombreColumnas3.add(new SelectItem("2","ORDEN: FECHA-HORA, ARMONICOS(1-40) Y FLICKER DE LAS FASES RST"));
			itemsNombreColumnas3.add(new SelectItem("3","ORDEN: FECHA-HORA, ARMONICOS(1-40) Y ENERGIA DE LAS FASES RST"));
			itemsNombreColumnas3.add(new SelectItem("16","ORDEN: FECHA-HORA, ARMONICOS(1-40) DE LAS FASES RST Y ENERGIA TOTAL "));
			itemsNombreColumnas3.add(new SelectItem("4","ORDEN: FECHA-HORA Y ARMONICOS(1-40) DE LAS FASES RST"));
			itemsNombreColumnas3.add(new SelectItem("5","ORDEN: FECHA-HORA Y ENERGIA DE LAS FASES RST"));
			itemsNombreColumnas3.add(new SelectItem("17","ORDEN: FECHA-HORA Y ENERGIA TOTAL"));
			itemsNombreColumnas3.add(new SelectItem("6","ORDEN: FECHA-HORA, ARMONICOS(1-40), FLICKER Y ENERGIA DE UNA FASE"));
			itemsNombreColumnas3.add(new SelectItem("7","ORDEN: FECHA-HORA, ARMONICOS(1-40) Y FLICKER DE UNA FASE"));
			itemsNombreColumnas3.add(new SelectItem("8","ORDEN: FECHA-HORA, ARMONICOS(1-40) Y ENERGIA DE UNA FASE"));
			itemsNombreColumnas3.add(new SelectItem("9","ORDEN: FECHA-HORA, ARMONICOS(1-40) DE UNA FASE"));
			itemsNombreColumnas3.add(new SelectItem("10","ORDEN: FECHA-HORA, FLICKER Y ENERGIA DE LAS FASES RST"));
			itemsNombreColumnas3.add(new SelectItem("18","ORDEN: FECHA-HORA Y FLICKER DE LAS FASES RST MAS ENERGIA TOTAL"));
			itemsNombreColumnas3.add(new SelectItem("11","ORDEN: FECHA-HORA Y FLICKER DE LAS FASES RST"));
			itemsNombreColumnas3.add(new SelectItem("12","ORDEN: FECHA-HORA, FLICKER Y ENERGIA DE UNA FASE"));
			itemsNombreColumnas3.add(new SelectItem("13","ORDEN: FECHA-HORA Y FLICKER DE UNA FASE"));
			itemsNombreColumnas3.add(new SelectItem("14","ORDEN: FECHA-HORA Y ENERGIA DE UNA FASE"));
			return itemsNombreColumnas3;
		}

		public void setItemsNombreColumnas3(List<SelectItem> itemsNombreColumnas3) {
			this.itemsNombreColumnas3 = itemsNombreColumnas3;
		}
		
		public List<SelectItem> getItemsNombreColumnasAp() {
			itemsNombreColumnasAp=new ArrayList<SelectItem>();
			itemsNombreColumnasAp.add(new SelectItem("1","M1, LUGAR, I1, I2, I3, I4, I5, M2, L1, L2, L3, L4, L5, NUM_LUM_POR_KM, SUP_APARENTE, INT_LUMINOSA, REL_LUMINOSA"));
			return itemsNombreColumnasAp;
		}

		public void setItemsNombreColumnasAp(List<SelectItem> itemsNombreColumnasAp) {
			this.itemsNombreColumnasAp = itemsNombreColumnasAp;
		}	
		
		public List<SelectItem> getItemsNombreColumnasAp2() {
			itemsNombreColumnasAp2=new ArrayList<SelectItem>();
			itemsNombreColumnasAp2.add(new SelectItem("1","COD_VIA, NRO_VANO, M1, LUGAR, I1, I2, I3, I4, I5, M2, L1, L2, L3, L4, L5, NUM_LUM_POR_KM, SUP_APARENTE, INT_LUMINOSA, REL_LUMINOSA"));
			
			return itemsNombreColumnasAp2;
		}

		public void setItemsNombreColumnasAp2(List<SelectItem> itemsNombreColumnasAp2) {
			this.itemsNombreColumnasAp2 = itemsNombreColumnasAp2;
		}		
		
		public long getTipoCronograma() {
			return tipoCronograma;
		}

		public void setTipoCronograma(long tipoCronograma) {
			this.tipoCronograma = tipoCronograma;
		}

		public boolean isIntercalado() {
			return intercalado;
		}

		public void setIntercalado(boolean intercalado) {
			this.intercalado = intercalado;
		}

		public boolean isPotencia() {
			return potencia;
		}

		public void setPotencia(boolean potencia) {
			this.potencia = potencia;
		}

		public String procesarArchivoCargadoPerturbaciones() throws Exception{
			String campos[];
			String path = System.getProperty("uploads.folder"); 
		    String fullNameArchivo=path+getNombreArchivoDestino(); 
		    String nombreColumnasAux;
		    String columnasAux;
		    int opcion;
		    
		    if( isArchivoCargado() ){
		    	System.out.println("El archivo esta cargado!, aqui lucho poner la llamada a tus metodos");
		    	// TODO: Luchito aqui llamar a los metodos para actualizar la BD
		    	
		    	opcion = Integer.valueOf(nombreColumnas); 
		    	switch (opcion) {
				case 1:
					if(!intercalado){
						nombreColumnasAux = "INTERVALO,TENSION_R1,TENSION_R2,TENSION_R3,TENSION_R4,TENSION_R5,TENSION_R6,TENSION_R7,TENSION_R8,TENSION_R9,TENSION_R10," +
								"TENSION_R11,TENSION_R12,TENSION_R13,TENSION_R14,TENSION_R15,TENSION_R16,TENSION_R17,TENSION_R18,TENSION_R19,TENSION_R20," +
								"TENSION_R21,TENSION_R22,TENSION_R23,TENSION_R24,TENSION_R25,TENSION_R26,TENSION_R27,TENSION_R28,TENSION_R29,TENSION_R30," +
								"TENSION_R31,TENSION_R32,TENSION_R33,TENSION_R34,TENSION_R35,TENSION_R36,TENSION_R37,TENSION_R38,TENSION_R39,TENSION_R40," +
								"TENSION_S1,TENSION_S2,TENSION_S3,TENSION_S4,TENSION_S5,TENSION_S6,TENSION_S7,TENSION_S8,TENSION_S9,TENSION_S10," +
								"TENSION_S11,TENSION_S12,TENSION_S13,TENSION_S14,TENSION_S15,TENSION_S16,TENSION_S17,TENSION_S18,TENSION_S19,TENSION_S20," +
								"TENSION_S21,TENSION_S22,TENSION_S23,TENSION_S24,TENSION_S25,TENSION_S26,TENSION_S27,TENSION_S28,TENSION_S29,TENSION_S30," +
								"TENSION_S31,TENSION_S32,TENSION_S33,TENSION_S34,TENSION_S35,TENSION_S36,TENSION_S37,TENSION_S38,TENSION_S39,TENSION_S40," +
								"TENSION_T1,TENSION_T2,TENSION_T3,TENSION_T4,TENSION_T5,TENSION_T6,TENSION_T7,TENSION_T8,TENSION_T9,TENSION_T10," +
								"TENSION_T11,TENSION_T12,TENSION_T13,TENSION_T14,TENSION_T15,TENSION_T16,TENSION_T17,TENSION_T18,TENSION_T19,TENSION_T20," +
								"TENSION_T21,TENSION_T22,TENSION_T23,TENSION_T24,TENSION_T25,TENSION_T26,TENSION_T27,TENSION_T28,TENSION_T29,TENSION_T30," +
								"TENSION_T31,TENSION_T32,TENSION_T33,TENSION_T34,TENSION_T35,TENSION_T36,TENSION_T37,TENSION_T38,TENSION_T39,TENSION_T40," +
								"PST_R,PST_S,PST_T,ENERGIA_R,ENERGIA_S,ENERGIA_T";
					} else {
						nombreColumnasAux = "INTERVALO,TENSION_R1,TENSION_S1,TENSION_T1,TENSION_R2,TENSION_S2,TENSION_T2,TENSION_R3,TENSION_S3,TENSION_T3,TENSION_R4," +
								"TENSION_S4,TENSION_T4,TENSION_R5,TENSION_S5,TENSION_T5,TENSION_R6,TENSION_S6,TENSION_T6,TENSION_R7,TENSION_S7," +
								"TENSION_T7,TENSION_R8,TENSION_S8,TENSION_T8,TENSION_R9,TENSION_S9,TENSION_T9,TENSION_R10,TENSION_S10,TENSION_T10," +
								"TENSION_R11,TENSION_S11,TENSION_T11,TENSION_R12,TENSION_S12,TENSION_T12,TENSION_R13,TENSION_S13,TENSION_T13,TENSION_R14," +
								"TENSION_S14,TENSION_T14,TENSION_R15,TENSION_S15,TENSION_T15,TENSION_R16,TENSION_S16,TENSION_T16,TENSION_R17,TENSION_S17," +
								"TENSION_T17,TENSION_R18,TENSION_S18,TENSION_T18,TENSION_R19,TENSION_S19,TENSION_T19,TENSION_R20,TENSION_S20,TENSION_T20," +
								"TENSION_R21,TENSION_S21,TENSION_T21,TENSION_R22,TENSION_S22,TENSION_T22,TENSION_R23,TENSION_S23,TENSION_T23,TENSION_R24," +
								"TENSION_S24,TENSION_T24,TENSION_R25,TENSION_S25,TENSION_T25,TENSION_R26,TENSION_S26,TENSION_T26,TENSION_R27,TENSION_S27," +
								"TENSION_T27,TENSION_R28,TENSION_S28,TENSION_T28,TENSION_R29,TENSION_S29,TENSION_T29,TENSION_R30,TENSION_S30,TENSION_T30," +
								"TENSION_R31,TENSION_S31,TENSION_T31,TENSION_R32,TENSION_S32,TENSION_T32,TENSION_R33,TENSION_S33,TENSION_T33,TENSION_R34," +
								"TENSION_S34,TENSION_T34,TENSION_R35,TENSION_S35,TENSION_T35,TENSION_R36,TENSION_S36,TENSION_T36,TENSION_R37,TENSION_S37," +
								"TENSION_T37,TENSION_R38,TENSION_S38,TENSION_T38,TENSION_R39,TENSION_S39,TENSION_T39,TENSION_R40,TENSION_S40,TENSION_T40," +
								"PST_R,PST_S,PST_T,ENERGIA_R,ENERGIA_S,ENERGIA_T";
					}

					break;
				case 2:
					if(!intercalado){
						nombreColumnasAux = "INTERVALO,TENSION_R1,TENSION_R2,TENSION_R3,TENSION_R4,TENSION_R5,TENSION_R6,TENSION_R7,TENSION_R8,TENSION_R9,TENSION_R10," +
								"TENSION_R11,TENSION_R12,TENSION_R13,TENSION_R14,TENSION_R15,TENSION_R16,TENSION_R17,TENSION_R18,TENSION_R19,TENSION_R20," +
								"TENSION_R21,TENSION_R22,TENSION_R23,TENSION_R24,TENSION_R25,TENSION_R26,TENSION_R27,TENSION_R28,TENSION_R29,TENSION_R30," +
								"TENSION_R31,TENSION_R32,TENSION_R33,TENSION_R34,TENSION_R35,TENSION_R36,TENSION_R37,TENSION_R38,TENSION_R39,TENSION_R40," +
								"TENSION_S1,TENSION_S2,TENSION_S3,TENSION_S4,TENSION_S5,TENSION_S6,TENSION_S7,TENSION_S8,TENSION_S9,TENSION_S10," +
								"TENSION_S11,TENSION_S12,TENSION_S13,TENSION_S14,TENSION_S15,TENSION_S16,TENSION_S17,TENSION_S18,TENSION_S19,TENSION_S20," +
								"TENSION_S21,TENSION_S22,TENSION_S23,TENSION_S24,TENSION_S25,TENSION_S26,TENSION_S27,TENSION_S28,TENSION_S29,TENSION_S30," +
								"TENSION_S31,TENSION_S32,TENSION_S33,TENSION_S34,TENSION_S35,TENSION_S36,TENSION_S37,TENSION_S38,TENSION_S39,TENSION_S40," +
								"TENSION_T1,TENSION_T2,TENSION_T3,TENSION_T4,TENSION_T5,TENSION_T6,TENSION_T7,TENSION_T8,TENSION_T9,TENSION_T10," +
								"TENSION_T11,TENSION_T12,TENSION_T13,TENSION_T14,TENSION_T15,TENSION_T16,TENSION_T17,TENSION_T18,TENSION_T19,TENSION_T20," +
								"TENSION_T21,TENSION_T22,TENSION_T23,TENSION_T24,TENSION_T25,TENSION_T26,TENSION_T27,TENSION_T28,TENSION_T29,TENSION_T30," +
								"TENSION_T31,TENSION_T32,TENSION_T33,TENSION_T34,TENSION_T35,TENSION_T36,TENSION_T37,TENSION_T38,TENSION_T39,TENSION_T40," + 
								"PST_R,PST_S,PST_T";
					} else {
						nombreColumnasAux = "INTERVALO,TENSION_R1,TENSION_S1,TENSION_T1,TENSION_R2,TENSION_S2,TENSION_T2,TENSION_R3,TENSION_S3,TENSION_T3,TENSION_R4," +
								"TENSION_S4,TENSION_T4,TENSION_R5,TENSION_S5,TENSION_T5,TENSION_R6,TENSION_S6,TENSION_T6,TENSION_R7,TENSION_S7," +
								"TENSION_T7,TENSION_R8,TENSION_S8,TENSION_T8,TENSION_R9,TENSION_S9,TENSION_T9,TENSION_R10,TENSION_S10,TENSION_T10," +
								"TENSION_R11,TENSION_S11,TENSION_T11,TENSION_R12,TENSION_S12,TENSION_T12,TENSION_R13,TENSION_S13,TENSION_T13,TENSION_R14," +
								"TENSION_S14,TENSION_T14,TENSION_R15,TENSION_S15,TENSION_T15,TENSION_R16,TENSION_S16,TENSION_T16,TENSION_R17,TENSION_S17," +
								"TENSION_T17,TENSION_R18,TENSION_S18,TENSION_T18,TENSION_R19,TENSION_S19,TENSION_T19,TENSION_R20,TENSION_S20,TENSION_T20," +
								"TENSION_R21,TENSION_S21,TENSION_T21,TENSION_R22,TENSION_S22,TENSION_T22,TENSION_R23,TENSION_S23,TENSION_T23,TENSION_R24," +
								"TENSION_S24,TENSION_T24,TENSION_R25,TENSION_S25,TENSION_T25,TENSION_R26,TENSION_S26,TENSION_T26,TENSION_R27,TENSION_S27," +
								"TENSION_T27,TENSION_R28,TENSION_S28,TENSION_T28,TENSION_R29,TENSION_S29,TENSION_T29,TENSION_R30,TENSION_S30,TENSION_T30," +
								"TENSION_R31,TENSION_S31,TENSION_T31,TENSION_R32,TENSION_S32,TENSION_T32,TENSION_R33,TENSION_S33,TENSION_T33,TENSION_R34," +
								"TENSION_S34,TENSION_T34,TENSION_R35,TENSION_S35,TENSION_T35,TENSION_R36,TENSION_S36,TENSION_T36,TENSION_R37,TENSION_S37," +
								"TENSION_T37,TENSION_R38,TENSION_S38,TENSION_T38,TENSION_R39,TENSION_S39,TENSION_T39,TENSION_R40,TENSION_S40,TENSION_T40," + 
								"PST_R,PST_S,PST_T";
					}
					break;
				case 3:
					if(!intercalado){
						nombreColumnasAux = "INTERVALO,TENSION_R1,TENSION_R2,TENSION_R3,TENSION_R4,TENSION_R5,TENSION_R6,TENSION_R7,TENSION_R8,TENSION_R9,TENSION_R10," +
								"TENSION_R11,TENSION_R12,TENSION_R13,TENSION_R14,TENSION_R15,TENSION_R16,TENSION_R17,TENSION_R18,TENSION_R19,TENSION_R20," +
								"TENSION_R21,TENSION_R22,TENSION_R23,TENSION_R24,TENSION_R25,TENSION_R26,TENSION_R27,TENSION_R28,TENSION_R29,TENSION_R30," +
								"TENSION_R31,TENSION_R32,TENSION_R33,TENSION_R34,TENSION_R35,TENSION_R36,TENSION_R37,TENSION_R38,TENSION_R39,TENSION_R40," +
								"TENSION_S1,TENSION_S2,TENSION_S3,TENSION_S4,TENSION_S5,TENSION_S6,TENSION_S7,TENSION_S8,TENSION_S9,TENSION_S10," +
								"TENSION_S11,TENSION_S12,TENSION_S13,TENSION_S14,TENSION_S15,TENSION_S16,TENSION_S17,TENSION_S18,TENSION_S19,TENSION_S20," +
								"TENSION_S21,TENSION_S22,TENSION_S23,TENSION_S24,TENSION_S25,TENSION_S26,TENSION_S27,TENSION_S28,TENSION_S29,TENSION_S30," +
								"TENSION_S31,TENSION_S32,TENSION_S33,TENSION_S34,TENSION_S35,TENSION_S36,TENSION_S37,TENSION_S38,TENSION_S39,TENSION_S40," +
								"TENSION_T1,TENSION_T2,TENSION_T3,TENSION_T4,TENSION_T5,TENSION_T6,TENSION_T7,TENSION_T8,TENSION_T9,TENSION_T10," +
								"TENSION_T11,TENSION_T12,TENSION_T13,TENSION_T14,TENSION_T15,TENSION_T16,TENSION_T17,TENSION_T18,TENSION_T19,TENSION_T20," +
								"TENSION_T21,TENSION_T22,TENSION_T23,TENSION_T24,TENSION_T25,TENSION_T26,TENSION_T27,TENSION_T28,TENSION_T29,TENSION_T30," +
								"TENSION_T31,TENSION_T32,TENSION_T33,TENSION_T34,TENSION_T35,TENSION_T36,TENSION_T37,TENSION_T38,TENSION_T39,TENSION_T40," +
								"ENERGIA_R,ENERGIA_S,ENERGIA_T";
					} else {
						nombreColumnasAux = "INTERVALO,TENSION_R1,TENSION_S1,TENSION_T1,TENSION_R2,TENSION_S2,TENSION_T2,TENSION_R3,TENSION_S3,TENSION_T3,TENSION_R4," +
								"TENSION_S4,TENSION_T4,TENSION_R5,TENSION_S5,TENSION_T5,TENSION_R6,TENSION_S6,TENSION_T6,TENSION_R7,TENSION_S7," +
								"TENSION_T7,TENSION_R8,TENSION_S8,TENSION_T8,TENSION_R9,TENSION_S9,TENSION_T9,TENSION_R10,TENSION_S10,TENSION_T10," +
								"TENSION_R11,TENSION_S11,TENSION_T11,TENSION_R12,TENSION_S12,TENSION_T12,TENSION_R13,TENSION_S13,TENSION_T13,TENSION_R14," +
								"TENSION_S14,TENSION_T14,TENSION_R15,TENSION_S15,TENSION_T15,TENSION_R16,TENSION_S16,TENSION_T16,TENSION_R17,TENSION_S17," +
								"TENSION_T17,TENSION_R18,TENSION_S18,TENSION_T18,TENSION_R19,TENSION_S19,TENSION_T19,TENSION_R20,TENSION_S20,TENSION_T20," +
								"TENSION_R21,TENSION_S21,TENSION_T21,TENSION_R22,TENSION_S22,TENSION_T22,TENSION_R23,TENSION_S23,TENSION_T23,TENSION_R24," +
								"TENSION_S24,TENSION_T24,TENSION_R25,TENSION_S25,TENSION_T25,TENSION_R26,TENSION_S26,TENSION_T26,TENSION_R27,TENSION_S27," +
								"TENSION_T27,TENSION_R28,TENSION_S28,TENSION_T28,TENSION_R29,TENSION_S29,TENSION_T29,TENSION_R30,TENSION_S30,TENSION_T30," +
								"TENSION_R31,TENSION_S31,TENSION_T31,TENSION_R32,TENSION_S32,TENSION_T32,TENSION_R33,TENSION_S33,TENSION_T33,TENSION_R34," +
								"TENSION_S34,TENSION_T34,TENSION_R35,TENSION_S35,TENSION_T35,TENSION_R36,TENSION_S36,TENSION_T36,TENSION_R37,TENSION_S37," +
								"TENSION_T37,TENSION_R38,TENSION_S38,TENSION_T38,TENSION_R39,TENSION_S39,TENSION_T39,TENSION_R40,TENSION_S40,TENSION_T40," +
								"ENERGIA_R,ENERGIA_S,ENERGIA_T";
					}
					break;
				case 4:
					if(!intercalado){
						nombreColumnasAux = "INTERVALO,TENSION_R1,TENSION_R2,TENSION_R3,TENSION_R4,TENSION_R5,TENSION_R6,TENSION_R7,TENSION_R8,TENSION_R9,TENSION_R10," +
								"TENSION_R11,TENSION_R12,TENSION_R13,TENSION_R14,TENSION_R15,TENSION_R16,TENSION_R17,TENSION_R18,TENSION_R19,TENSION_R20," +
								"TENSION_R21,TENSION_R22,TENSION_R23,TENSION_R24,TENSION_R25,TENSION_R26,TENSION_R27,TENSION_R28,TENSION_R29,TENSION_R30," +
								"TENSION_R31,TENSION_R32,TENSION_R33,TENSION_R34,TENSION_R35,TENSION_R36,TENSION_R37,TENSION_R38,TENSION_R39,TENSION_R40," +
								"TENSION_S1,TENSION_S2,TENSION_S3,TENSION_S4,TENSION_S5,TENSION_S6,TENSION_S7,TENSION_S8,TENSION_S9,TENSION_S10," +
								"TENSION_S11,TENSION_S12,TENSION_S13,TENSION_S14,TENSION_S15,TENSION_S16,TENSION_S17,TENSION_S18,TENSION_S19,TENSION_S20," +
								"TENSION_S21,TENSION_S22,TENSION_S23,TENSION_S24,TENSION_S25,TENSION_S26,TENSION_S27,TENSION_S28,TENSION_S29,TENSION_S30," +
								"TENSION_S31,TENSION_S32,TENSION_S33,TENSION_S34,TENSION_S35,TENSION_S36,TENSION_S37,TENSION_S38,TENSION_S39,TENSION_S40," +
								"TENSION_T1,TENSION_T2,TENSION_T3,TENSION_T4,TENSION_T5,TENSION_T6,TENSION_T7,TENSION_T8,TENSION_T9,TENSION_T10," +
								"TENSION_T11,TENSION_T12,TENSION_T13,TENSION_T14,TENSION_T15,TENSION_T16,TENSION_T17,TENSION_T18,TENSION_T19,TENSION_T20," +
								"TENSION_T21,TENSION_T22,TENSION_T23,TENSION_T24,TENSION_T25,TENSION_T26,TENSION_T27,TENSION_T28,TENSION_T29,TENSION_T30," +
								"TENSION_T31,TENSION_T32,TENSION_T33,TENSION_T34,TENSION_T35,TENSION_T36,TENSION_T37,TENSION_T38,TENSION_T39,TENSION_T40";
					} else {
						nombreColumnasAux = "INTERVALO,TENSION_R1,TENSION_S1,TENSION_T1,TENSION_R2,TENSION_S2,TENSION_T2,TENSION_R3,TENSION_S3,TENSION_T3,TENSION_R4," +
								"TENSION_S4,TENSION_T4,TENSION_R5,TENSION_S5,TENSION_T5,TENSION_R6,TENSION_S6,TENSION_T6,TENSION_R7,TENSION_S7," +
								"TENSION_T7,TENSION_R8,TENSION_S8,TENSION_T8,TENSION_R9,TENSION_S9,TENSION_T9,TENSION_R10,TENSION_S10,TENSION_T10," +
								"TENSION_R11,TENSION_S11,TENSION_T11,TENSION_R12,TENSION_S12,TENSION_T12,TENSION_R13,TENSION_S13,TENSION_T13,TENSION_R14," +
								"TENSION_S14,TENSION_T14,TENSION_R15,TENSION_S15,TENSION_T15,TENSION_R16,TENSION_S16,TENSION_T16,TENSION_R17,TENSION_S17," +
								"TENSION_T17,TENSION_R18,TENSION_S18,TENSION_T18,TENSION_R19,TENSION_S19,TENSION_T19,TENSION_R20,TENSION_S20,TENSION_T20," +
								"TENSION_R21,TENSION_S21,TENSION_T21,TENSION_R22,TENSION_S22,TENSION_T22,TENSION_R23,TENSION_S23,TENSION_T23,TENSION_R24," +
								"TENSION_S24,TENSION_T24,TENSION_R25,TENSION_S25,TENSION_T25,TENSION_R26,TENSION_S26,TENSION_T26,TENSION_R27,TENSION_S27," +
								"TENSION_T27,TENSION_R28,TENSION_S28,TENSION_T28,TENSION_R29,TENSION_S29,TENSION_T29,TENSION_R30,TENSION_S30,TENSION_T30," +
								"TENSION_R31,TENSION_S31,TENSION_T31,TENSION_R32,TENSION_S32,TENSION_T32,TENSION_R33,TENSION_S33,TENSION_T33,TENSION_R34," +
								"TENSION_S34,TENSION_T34,TENSION_R35,TENSION_S35,TENSION_T35,TENSION_R36,TENSION_S36,TENSION_T36,TENSION_R37,TENSION_S37," +
								"TENSION_T37,TENSION_R38,TENSION_S38,TENSION_T38,TENSION_R39,TENSION_S39,TENSION_T39,TENSION_R40,TENSION_S40,TENSION_T40";
					}
					break;
				case 5:
					nombreColumnasAux = "INTERVALO,ENERGIA_R,ENERGIA_S,ENERGIA_T";
					break;
				case 6:
					nombreColumnasAux = "INTERVALO,TENSION_R1,TENSION_R2,TENSION_R3,TENSION_R4,TENSION_R5,TENSION_R6,TENSION_R7,TENSION_R8,TENSION_R9,TENSION_R10," +
							"TENSION_R11,TENSION_R12,TENSION_R13,TENSION_R14,TENSION_R15,TENSION_R16,TENSION_R17,TENSION_R18,TENSION_R19,TENSION_R20," +
							"TENSION_R21,TENSION_R22,TENSION_R23,TENSION_R24,TENSION_R25,TENSION_R26,TENSION_R27,TENSION_R28,TENSION_R29,TENSION_R30," +
							"TENSION_R31,TENSION_R32,TENSION_R33,TENSION_R34,TENSION_R35,TENSION_R36,TENSION_R37,TENSION_R38,TENSION_R39,TENSION_R40," + 
							"PST_R,ENERGIA_R";
					break;
				case 7:
					nombreColumnasAux = "INTERVALO,TENSION_R1,TENSION_R2,TENSION_R3,TENSION_R4,TENSION_R5,TENSION_R6,TENSION_R7,TENSION_R8,TENSION_R9,TENSION_R10," +
							"TENSION_R11,TENSION_R12,TENSION_R13,TENSION_R14,TENSION_R15,TENSION_R16,TENSION_R17,TENSION_R18,TENSION_R19,TENSION_R20," +
							"TENSION_R21,TENSION_R22,TENSION_R23,TENSION_R24,TENSION_R25,TENSION_R26,TENSION_R27,TENSION_R28,TENSION_R29,TENSION_R30," +
							"TENSION_R31,TENSION_R32,TENSION_R33,TENSION_R34,TENSION_R35,TENSION_R36,TENSION_R37,TENSION_R38,TENSION_R39,TENSION_R40," + 
							"PST_R";
					break;
				case 8:
					nombreColumnasAux = "INTERVALO,TENSION_R1,TENSION_R2,TENSION_R3,TENSION_R4,TENSION_R5,TENSION_R6,TENSION_R7,TENSION_R8,TENSION_R9,TENSION_R10," +
							"TENSION_R11,TENSION_R12,TENSION_R13,TENSION_R14,TENSION_R15,TENSION_R16,TENSION_R17,TENSION_R18,TENSION_R19,TENSION_R20," +
							"TENSION_R21,TENSION_R22,TENSION_R23,TENSION_R24,TENSION_R25,TENSION_R26,TENSION_R27,TENSION_R28,TENSION_R29,TENSION_R30," +
							"TENSION_R31,TENSION_R32,TENSION_R33,TENSION_R34,TENSION_R35,TENSION_R36,TENSION_R37,TENSION_R38,TENSION_R39,TENSION_R40," + 
							"ENERGIA_R";
					break;
				case 9:
					nombreColumnasAux = "INTERVALO,TENSION_R1,TENSION_R2,TENSION_R3,TENSION_R4,TENSION_R5,TENSION_R6,TENSION_R7,TENSION_R8,TENSION_R9,TENSION_R10," +
							"TENSION_R11,TENSION_R12,TENSION_R13,TENSION_R14,TENSION_R15,TENSION_R16,TENSION_R17,TENSION_R18,TENSION_R19,TENSION_R20," +
							"TENSION_R21,TENSION_R22,TENSION_R23,TENSION_R24,TENSION_R25,TENSION_R26,TENSION_R27,TENSION_R28,TENSION_R29,TENSION_R30," +
							"TENSION_R31,TENSION_R32,TENSION_R33,TENSION_R34,TENSION_R35,TENSION_R36,TENSION_R37,TENSION_R38,TENSION_R39,TENSION_R40";
					break;
				case 10:
					nombreColumnasAux = "INTERVALO,PST_R,PST_S,PST_T,ENERGIA_S,ENERGIA_T,ENERGIA_R";
					break;
				case 11:
					nombreColumnasAux = "INTERVALO,PST_R,PST_S,PST_T";
					break;
				case 12:
					nombreColumnasAux = "INTERVALO,PST_R,ENERGIA_R";
					break;
				case 13:
					nombreColumnasAux = "INTERVALO,PST_R";
					break;
				case 14:
					nombreColumnasAux = "INTERVALO,ENERGIA_R";
					break;
				case 15:
					if(!intercalado){
						nombreColumnasAux = "INTERVALO,TENSION_R1,TENSION_R2,TENSION_R3,TENSION_R4,TENSION_R5,TENSION_R6,TENSION_R7,TENSION_R8,TENSION_R9,TENSION_R10," +
								"TENSION_R11,TENSION_R12,TENSION_R13,TENSION_R14,TENSION_R15,TENSION_R16,TENSION_R17,TENSION_R18,TENSION_R19,TENSION_R20," +
								"TENSION_R21,TENSION_R22,TENSION_R23,TENSION_R24,TENSION_R25,TENSION_R26,TENSION_R27,TENSION_R28,TENSION_R29,TENSION_R30," +
								"TENSION_R31,TENSION_R32,TENSION_R33,TENSION_R34,TENSION_R35,TENSION_R36,TENSION_R37,TENSION_R38,TENSION_R39,TENSION_R40," +
								"TENSION_S1,TENSION_S2,TENSION_S3,TENSION_S4,TENSION_S5,TENSION_S6,TENSION_S7,TENSION_S8,TENSION_S9,TENSION_S10," +
								"TENSION_S11,TENSION_S12,TENSION_S13,TENSION_S14,TENSION_S15,TENSION_S16,TENSION_S17,TENSION_S18,TENSION_S19,TENSION_S20," +
								"TENSION_S21,TENSION_S22,TENSION_S23,TENSION_S24,TENSION_S25,TENSION_S26,TENSION_S27,TENSION_S28,TENSION_S29,TENSION_S30," +
								"TENSION_S31,TENSION_S32,TENSION_S33,TENSION_S34,TENSION_S35,TENSION_S36,TENSION_S37,TENSION_S38,TENSION_S39,TENSION_S40," +
								"TENSION_T1,TENSION_T2,TENSION_T3,TENSION_T4,TENSION_T5,TENSION_T6,TENSION_T7,TENSION_T8,TENSION_T9,TENSION_T10," +
								"TENSION_T11,TENSION_T12,TENSION_T13,TENSION_T14,TENSION_T15,TENSION_T16,TENSION_T17,TENSION_T18,TENSION_T19,TENSION_T20," +
								"TENSION_T21,TENSION_T22,TENSION_T23,TENSION_T24,TENSION_T25,TENSION_T26,TENSION_T27,TENSION_T28,TENSION_T29,TENSION_T30," +
								"TENSION_T31,TENSION_T32,TENSION_T33,TENSION_T34,TENSION_T35,TENSION_T36,TENSION_T37,TENSION_T38,TENSION_T39,TENSION_T40," +
								"PST_R,PST_S,PST_T,ENE_INTERVALO";
					} else {
						nombreColumnasAux = "INTERVALO,TENSION_R1,TENSION_S1,TENSION_T1,TENSION_R2,TENSION_S2,TENSION_T2,TENSION_R3,TENSION_S3,TENSION_T3,TENSION_R4," +
								"TENSION_S4,TENSION_T4,TENSION_R5,TENSION_S5,TENSION_T5,TENSION_R6,TENSION_S6,TENSION_T6,TENSION_R7,TENSION_S7," +
								"TENSION_T7,TENSION_R8,TENSION_S8,TENSION_T8,TENSION_R9,TENSION_S9,TENSION_T9,TENSION_R10,TENSION_S10,TENSION_T10," +
								"TENSION_R11,TENSION_S11,TENSION_T11,TENSION_R12,TENSION_S12,TENSION_T12,TENSION_R13,TENSION_S13,TENSION_T13,TENSION_R14," +
								"TENSION_S14,TENSION_T14,TENSION_R15,TENSION_S15,TENSION_T15,TENSION_R16,TENSION_S16,TENSION_T16,TENSION_R17,TENSION_S17," +
								"TENSION_T17,TENSION_R18,TENSION_S18,TENSION_T18,TENSION_R19,TENSION_S19,TENSION_T19,TENSION_R20,TENSION_S20,TENSION_T20," +
								"TENSION_R21,TENSION_S21,TENSION_T21,TENSION_R22,TENSION_S22,TENSION_T22,TENSION_R23,TENSION_S23,TENSION_T23,TENSION_R24," +
								"TENSION_S24,TENSION_T24,TENSION_R25,TENSION_S25,TENSION_T25,TENSION_R26,TENSION_S26,TENSION_T26,TENSION_R27,TENSION_S27," +
								"TENSION_T27,TENSION_R28,TENSION_S28,TENSION_T28,TENSION_R29,TENSION_S29,TENSION_T29,TENSION_R30,TENSION_S30,TENSION_T30," +
								"TENSION_R31,TENSION_S31,TENSION_T31,TENSION_R32,TENSION_S32,TENSION_T32,TENSION_R33,TENSION_S33,TENSION_T33,TENSION_R34," +
								"TENSION_S34,TENSION_T34,TENSION_R35,TENSION_S35,TENSION_T35,TENSION_R36,TENSION_S36,TENSION_T36,TENSION_R37,TENSION_S37," +
								"TENSION_T37,TENSION_R38,TENSION_S38,TENSION_T38,TENSION_R39,TENSION_S39,TENSION_T39,TENSION_R40,TENSION_S40,TENSION_T40," +
								"PST_R,PST_S,PST_T,ENE_INTERVALO";
					}
					break;
				case 16:
					if(!intercalado){
						nombreColumnasAux = "INTERVALO,TENSION_R1,TENSION_R2,TENSION_R3,TENSION_R4,TENSION_R5,TENSION_R6,TENSION_R7,TENSION_R8,TENSION_R9,TENSION_R10," +
								"TENSION_R11,TENSION_R12,TENSION_R13,TENSION_R14,TENSION_R15,TENSION_R16,TENSION_R17,TENSION_R18,TENSION_R19,TENSION_R20," +
								"TENSION_R21,TENSION_R22,TENSION_R23,TENSION_R24,TENSION_R25,TENSION_R26,TENSION_R27,TENSION_R28,TENSION_R29,TENSION_R30," +
								"TENSION_R31,TENSION_R32,TENSION_R33,TENSION_R34,TENSION_R35,TENSION_R36,TENSION_R37,TENSION_R38,TENSION_R39,TENSION_R40," +
								"TENSION_S1,TENSION_S2,TENSION_S3,TENSION_S4,TENSION_S5,TENSION_S6,TENSION_S7,TENSION_S8,TENSION_S9,TENSION_S10," +
								"TENSION_S11,TENSION_S12,TENSION_S13,TENSION_S14,TENSION_S15,TENSION_S16,TENSION_S17,TENSION_S18,TENSION_S19,TENSION_S20," +
								"TENSION_S21,TENSION_S22,TENSION_S23,TENSION_S24,TENSION_S25,TENSION_S26,TENSION_S27,TENSION_S28,TENSION_S29,TENSION_S30," +
								"TENSION_S31,TENSION_S32,TENSION_S33,TENSION_S34,TENSION_S35,TENSION_S36,TENSION_S37,TENSION_S38,TENSION_S39,TENSION_S40," +
								"TENSION_T1,TENSION_T2,TENSION_T3,TENSION_T4,TENSION_T5,TENSION_T6,TENSION_T7,TENSION_T8,TENSION_T9,TENSION_T10," +
								"TENSION_T11,TENSION_T12,TENSION_T13,TENSION_T14,TENSION_T15,TENSION_T16,TENSION_T17,TENSION_T18,TENSION_T19,TENSION_T20," +
								"TENSION_T21,TENSION_T22,TENSION_T23,TENSION_T24,TENSION_T25,TENSION_T26,TENSION_T27,TENSION_T28,TENSION_T29,TENSION_T30," +
								"TENSION_T31,TENSION_T32,TENSION_T33,TENSION_T34,TENSION_T35,TENSION_T36,TENSION_T37,TENSION_T38,TENSION_T39,TENSION_T40," +
								"ENE_INTERVALO";
					} else {
						nombreColumnasAux = "INTERVALO,TENSION_R1,TENSION_S1,TENSION_T1,TENSION_R2,TENSION_S2,TENSION_T2,TENSION_R3,TENSION_S3,TENSION_T3,TENSION_R4," +
								"TENSION_S4,TENSION_T4,TENSION_R5,TENSION_S5,TENSION_T5,TENSION_R6,TENSION_S6,TENSION_T6,TENSION_R7,TENSION_S7," +
								"TENSION_T7,TENSION_R8,TENSION_S8,TENSION_T8,TENSION_R9,TENSION_S9,TENSION_T9,TENSION_R10,TENSION_S10,TENSION_T10," +
								"TENSION_R11,TENSION_S11,TENSION_T11,TENSION_R12,TENSION_S12,TENSION_T12,TENSION_R13,TENSION_S13,TENSION_T13,TENSION_R14," +
								"TENSION_S14,TENSION_T14,TENSION_R15,TENSION_S15,TENSION_T15,TENSION_R16,TENSION_S16,TENSION_T16,TENSION_R17,TENSION_S17," +
								"TENSION_T17,TENSION_R18,TENSION_S18,TENSION_T18,TENSION_R19,TENSION_S19,TENSION_T19,TENSION_R20,TENSION_S20,TENSION_T20," +
								"TENSION_R21,TENSION_S21,TENSION_T21,TENSION_R22,TENSION_S22,TENSION_T22,TENSION_R23,TENSION_S23,TENSION_T23,TENSION_R24," +
								"TENSION_S24,TENSION_T24,TENSION_R25,TENSION_S25,TENSION_T25,TENSION_R26,TENSION_S26,TENSION_T26,TENSION_R27,TENSION_S27," +
								"TENSION_T27,TENSION_R28,TENSION_S28,TENSION_T28,TENSION_R29,TENSION_S29,TENSION_T29,TENSION_R30,TENSION_S30,TENSION_T30," +
								"TENSION_R31,TENSION_S31,TENSION_T31,TENSION_R32,TENSION_S32,TENSION_T32,TENSION_R33,TENSION_S33,TENSION_T33,TENSION_R34," +
								"TENSION_S34,TENSION_T34,TENSION_R35,TENSION_S35,TENSION_T35,TENSION_R36,TENSION_S36,TENSION_T36,TENSION_R37,TENSION_S37," +
								"TENSION_T37,TENSION_R38,TENSION_S38,TENSION_T38,TENSION_R39,TENSION_S39,TENSION_T39,TENSION_R40,TENSION_S40,TENSION_T40," +
								"ENE_INTERVALO";
					}
					break;
				case 17:
					nombreColumnasAux = "INTERVALO,ENE_INTERVALO";
					break;
				case 18:
					nombreColumnasAux = "INTERVALO,PST_R,PST_S,PST_T,ENE_INTERVALO";
					break;
				default:
					nombreColumnasAux = "";
					break;
				}    	
		    	
		    	campos = getColumnas().trim().split(",");
		    	
		    	columnasAux = "";
		    	for (String palabra : campos) {
					if(palabra.contains("-")){
						int inicio = Integer.valueOf(palabra.trim().substring(0, palabra.trim().indexOf("-")));
						int fin = Integer.valueOf(palabra.trim().substring(palabra.trim().indexOf("-") + 1));
						
						for(int n=inicio; n <= fin; n++) {
							columnasAux = columnasAux + String.valueOf(n).trim() + ",";
				        }
						
					}else {
						columnasAux = columnasAux + palabra + ",";
					}
				}
		    	
		    	columnasAux = columnasAux.trim().substring(0,columnasAux.length() - 1);
		    	
		    	ProcesarArchivo procesarArchivo = new ProcesarArchivo(fullNameArchivo,"", separadorColumnas,"",nombreColumnasAux,columnasAux,filaInicial);
		    	List<String[]> ca = null;
		    	
		    	try {
		    		ca = procesarArchivo.cargar();
				} catch (Exception e) {
	    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No es posible cargar el archivo, verificar el formato del mismo", "No es posible cargar el archivo, verificar el formato del mismo"));
	    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	    			
	    			return "/zonasegura/medicion/cargaDatosPerturbaciones?faces-redirect=true";
	    			
				}
		    	
		    	
		    	//System.out.println(procesarArchivo.getRuta() + " " + procesarArchivo.getTabla() + " " + procesarArchivo.getCampos()+ " " + Long.toString(medicion_id)+ " " + formatoFecha+ " " + Long.toString(numColumnsFormatoFecha));
		    	
		    	
		    	
		    	String resultado = procesarArchivo.insertarIntervaloPerturbacion(procesarArchivo.getTabla(), procesarArchivo.getCampos(), ca, medicion_id, formatoFecha, numColumnsFormatoFecha, actualizar, potencia, opcion, ene_acumulada, wattios);
				
		    	if(resultado != null) {
	    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No es posible insertar los registros de perturbaciones, verificar el formato del archivo", "No es posible insertar los registros de perturbaciones, verificar el formato del archivo"));
	    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	    			
	    			return "/zonasegura/medicion/cargaDatosPerturbaciones?faces-redirect=true";
	    			
		    	}
		    		    	
		    	String result = null;
		    	ConectaDb db = new ConectaDb("CALIDAD");
				Connection cn = db.getConnection();
		        CallableStatement cstmt = null; 
		        
		    	try {

		    		Sql sql = new Sql("CALIDAD"); 
			        String updSql = "UPDATE CAL_MEDICION SET FAC_COR_TENSION = " + facCorTension_temp + " , " + " FAC_COR_CORRIENTE = " + facCorCorriente_temp + " where MEDICION_ID = " + medicion_id;
			        sql.ejecuta(updSql);
			        String dividir1000;
			        
			        dividir1000 = "SI";
			        
			        if(ene_acumulada){
			        	dividir1000 = "NO";
			        }
			        	
			        
		    		cstmt = cn.prepareCall("{CALL CAL_PERTURBACION_PACKAGE.PREPARAR_PERTURBACION(?,?)}");  
					cstmt.setLong(1, medicion_id);
					cstmt.setString(2, dividir1000);
					
					int ctos = cstmt.executeUpdate();
		            cstmt.close(); 
		            
		            
		            if (ctos == 0) {
		                result = "0 filas afectadas";
		            }
		         
		    	} catch (SQLException e) {
		            result = e.getMessage();

		        } finally {
		            try {
		                cn.close();
		            } catch (SQLException e) {
		                result = e.getMessage();
		            }
		        }
		    	
				System.out.print(result);		    	
		    
		    } else { 
		    	FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se ha encontrado el archivo de origen.", "No se ha encontrado el archivo de origen."));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

    			return "/zonasegura/medicion/cargaDatosPerturbaciones?faces-redirect=true";
    			//throw new Exception("No existe el archivo destino, asegurese de cargarlo correctamente.");	
		    }
		    return "/zonasegura/medicionListar?faces-redirect=true"; //retornar a la pagina que deseas por Ejemplo a medicionListar.xhtml donde esta el boton
		}

		public boolean isEne_acumulada() {
			return ene_acumulada;
		}

		public void setEne_acumulada(boolean ene_acumulada) {
			this.ene_acumulada = ene_acumulada;
		}

		public MedicionAp getMedicionAp() {
			return medicionAp;
		}

		public void setMedicionAp(MedicionAp medicionAp) {
			this.medicionAp = medicionAp;
		}

		public BigDecimal getFacCorTensionPrim() {
			return facCorTensionPrim;
		}

		public void setFacCorTensionPrim(BigDecimal facCorTensionPrim) {
			this.facCorTensionPrim = facCorTensionPrim;
		}

		public BigDecimal getFacCorTensionSec() {
			return facCorTensionSec;
		}

		public void setFacCorTensionSec(BigDecimal facCorTensionSec) {
			this.facCorTensionSec = facCorTensionSec;
		}

		public BigDecimal getFacCorCorrientePrim() {
			return facCorCorrientePrim;
		}

		public void setFacCorCorrientePrim(BigDecimal facCorCorrientePrim) {
			this.facCorCorrientePrim = facCorCorrientePrim;
		}

		public BigDecimal getFacCorCorrienteSec() {
			return facCorCorrienteSec;
		}

		public void setFacCorCorrienteSec(BigDecimal facCorCorrienteSec) {
			this.facCorCorrienteSec = facCorCorrienteSec;
		}
		
		public void facCorTensionChanged(AjaxBehaviorEvent event){
			
			if(facCorTensionPrim != null && facCorTensionSec != null){
				if(facCorTensionSec.doubleValue() > 0){
					facCorTension_temp = new BigDecimal(facCorTensionPrim.doubleValue()/facCorTensionSec.doubleValue());
					facCorTension_temp = facCorTension_temp.setScale(4, BigDecimal.ROUND_HALF_UP);							
				}
			}
			
		}
		
		public void facCorCorrienteChanged(AjaxBehaviorEvent event){
			
			if(facCorCorrientePrim != null && facCorCorrienteSec != null){
				if(facCorCorrienteSec.doubleValue() > 0){
					facCorCorriente_temp = new BigDecimal( facCorCorrientePrim.doubleValue()/facCorCorrienteSec.doubleValue());
					facCorCorriente_temp = facCorCorriente_temp.setScale(4, BigDecimal.ROUND_HALF_UP);
				}
			}
			
		}

		public ProgramaAp getProgramaAp() {
			return programaAp;
		}

		public void setProgramaAp(ProgramaAp programaAp) {
			this.programaAp = programaAp;
		}

		public boolean isWattios() {
			return wattios;
		}

		public void setWattios(boolean wattios) {
			this.wattios = wattios;
		}



}
