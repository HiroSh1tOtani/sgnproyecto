package pe.com.indra.calidad.mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.entity.Cuadrilla;
import pe.com.indra.calidad.service.CuadrillaService;
import pe.com.indra.calidad.util.Utilidad;

@ManagedBean(name="cuadrillaBean")
@SessionScoped
public class CuadrillaBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<Cuadrilla> cuadrillas;
	private Cuadrilla cuadrilla;
	private List<Cuadrilla> cuadrillas1;
	
	private String mensaje;
	
	@PostConstruct 
	public void init(){
		System.out.println("se ejecuto PostConstruct "+new java.util.Date());
		cuadrilla=new Cuadrilla();
		cuadrilla.setEstado("1");
		/*
		String compensacionUnitariaId=Utilidad.getParametro("compensacionUnitariaId");
		if(compensacionUnitariaId==null){
		  compensacionUnitaria=new CompensacionUnitaria();
		  compensacionUnitaria.setEstado("1");
		}else{
			CompensacionUnitariaService service=new CompensacionUnitariaService();
			compensacionUnitaria=service.ReadById(new Long(compensacionUnitariaId));
		}
		*/
	}
	
	public List<Cuadrilla> getCuadrillas() {
		CuadrillaService service=new CuadrillaService();
		cuadrillas= service.getAllRows();
		return cuadrillas;
	}

	public void setLocalidades(List<Cuadrilla> cuadrillas) {
		this.cuadrillas = cuadrillas;
	}

	
	public List<Cuadrilla> getCuadrillas1() {
		CuadrillaService service=new CuadrillaService();
		cuadrillas= service.getAllRows1();
		return cuadrillas1;
	}

	public void setCuadrillas1(List<Cuadrilla> cuadrillas1) {
		this.cuadrillas1 = cuadrillas1;
	}
	
	
	public Cuadrilla getCuadrilla() {
		
		return cuadrilla;
	}

	public void setCuadrilla(Cuadrilla cuadrilla) {
		this.cuadrilla = cuadrilla;
	}
	
			
	public String prepararCrear(){
		//Esto funciona siempre que el bean tenga ambito de sesion 
		setCuadrilla(new Cuadrilla());
		getCuadrilla().setEstado("1");
		return "/zonasegura/cuadrillaCrear?faces-redirect=true";
	}
	
	public String prepararEditar(){
		//TODO: Por implementar carga de la instancia
		//System.out.println("compensacionUnitariaId:"+Utilidad.getParametro("compensacionUnitariaId"));
		//cargarCuadrillaActual();
		//return "/zonasegura/cuadrillaEditar?faces-redirect=true";
		
		try {
			cargarCuadrillaActual();
			return "/zonasegura/cuadrillaEditar?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/cuadrillaListar?faces-redirect=true";
		}
	}

	public String prepararVer(){
		//TODO: Por implementar carga de la instancia
		
		//cargarCuadrillaActual();
		//return "/zonasegura/cuadrillaVer?faces-redirect=true";
		
		try {
			cargarCuadrillaActual();
			return "/zonasegura/cuadrillaVer?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/cuadrillaListar?faces-redirect=true";
		}
	}
	
	public String salvar(){
		//CuadrillaService service=new CuadrillaService();
		//service.create(cuadrilla);
		//System.out.println("Se salvo una cuadrilla con exito.");
		//return "/zonasegura/cuadrillaListar?faces-redirect=true";
		
		CuadrillaService service=new CuadrillaService();
		
		try {
			service.create(cuadrilla);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/cuadrillaListar?faces-redirect=true";
	}
	
	public String actualizar(){
		//CuadrillaService service=new CuadrillaService();
		//service.update(cuadrilla);
		//System.out.println("Se actualizo una cuadrilla con exito.");
		//return "/zonasegura/cuadrillaListar?faces-redirect=true";
		
		CuadrillaService service=new CuadrillaService();
		try {
			service.update(cuadrilla);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/cuadrillaListar?faces-redirect=true";
	}
	
	public String eliminar(){
		String cuadrillaId=Utilidad.getParametro("cuadrillaId");
		CuadrillaService service=new CuadrillaService();
		//cuadrilla=service.ReadById(new Long(cuadrillaId));
		//cuadrilla.setEstado("0");
		//service.update(cuadrilla);
		//System.out.println("Se elimino una cuadrilla con exito.");
		//return "/zonasegura/cuadrillaListar?faces-redirect=true";
		
		try {
			cuadrilla=service.ReadById(new Long(cuadrillaId));
			cuadrilla.setEstado("0");
			service.update(cuadrilla);
		} catch (HibernateException e) {
			
			
			System.out.println(e.getMessage()+":"+e.getCause());
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			
		} catch (Exception e) {
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		
		return "/zonasegura/cuadrillaListar?faces-redirect=true";
	}
	
	public void cargarCuadrillaActual(){
		String cuadrillaId=Utilidad.getParametro("cuadrillaId");
		CuadrillaService service=new CuadrillaService();
		//cuadrilla=service.ReadById(new Long(cuadrillaId));
		
		try {
			cuadrilla=service.ReadById(new Long(cuadrillaId));
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

			throw new HibernateException("No se puede cargar Cuadrilla.", e);
			
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	
}
