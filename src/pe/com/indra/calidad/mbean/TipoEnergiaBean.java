package pe.com.indra.calidad.mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

import pe.com.indra.calidad.entity.TipoEnergia;
import pe.com.indra.calidad.service.TipoEnergiaService;

@ManagedBean(name="tipoEnergiaBean")
@SessionScoped
public class TipoEnergiaBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TipoEnergiaService service=null;
	
	private List<TipoEnergia> energias=null;

	@PostConstruct
	public void init(){
		service=new TipoEnergiaService();
		energias=service.getAllRows();
	}
	
	public List<TipoEnergia> getEnergias() {
		energias=(List<TipoEnergia>) service.getAllRows();
		return energias;
	}

	public void getEnergias(List<TipoEnergia> energias) {
		this.energias = energias;
	}
	
}
