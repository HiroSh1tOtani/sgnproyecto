package pe.com.indra.calidad.mbean;

//import org.hibernate.Session;
//import java.util.*;
import pe.com.indra.calidad.dao.*;
import pe.com.indra.calidad.entity.TipoParametro;

import org.hibernate.HibernateException;

/**
 * Parametro de calidad.
 * 
 * @version 1.0 16 Jul 2013
 * @author Juan Flores
 */
public class Controller {
    
    public TipoParametro createTipoParametro(String codTipParametro, String descripcion, String estado)
            throws Exception {
        try {
            HibernateUtil.begin();
            TipoParametro tipoParametro = new TipoParametro(codTipParametro, descripcion, estado);
            HibernateUtil.getSession().save(tipoParametro);
            HibernateUtil.commit();
            HibernateUtil.close();
            return tipoParametro;
        } catch (HibernateException e) {
            throw new Exception("No se puede crear " + codTipParametro, e);
        }
    }
}