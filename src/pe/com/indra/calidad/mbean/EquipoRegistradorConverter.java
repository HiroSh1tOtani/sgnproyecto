package pe.com.indra.calidad.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.indra.calidad.entity.EquipoRegistrador;
import pe.com.indra.calidad.service.EquipoRegistradorService;

@FacesConverter(value="equipoRegistradorConverter",forClass=EquipoRegistradorConverter.class)
public class EquipoRegistradorConverter implements Converter{

	@Override
	public EquipoRegistrador getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long tipParametroId=new Long(cadena);
		
		EquipoRegistradorService service=new EquipoRegistradorService();
		EquipoRegistrador equipoRegistrador=new EquipoRegistrador();
		equipoRegistrador=service.ReadById(tipParametroId);
		System.out.println(equipoRegistrador.getCodEquRegistrador()+":"+equipoRegistrador.getDescripcion());
		return equipoRegistrador;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			EquipoRegistrador equipoRegistrador = (EquipoRegistrador) objeto;
			//System.out.println("objeto:"+new Long(equipoRegistrador.getEquRegistradorId()).toString());
			return new Long(equipoRegistrador.getEquRegistradorId()).toString();
		}
		return null;
	}


}
