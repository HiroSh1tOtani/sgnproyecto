package pe.com.indra.calidad.mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

import pe.com.indra.calidad.entity.TipoMedicion;
import pe.com.indra.calidad.service.TipoMedicionService;

@ManagedBean(name="tipoMedicionBean")
@SessionScoped
public class TipoMedicionBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TipoMedicionService service=null;
	
	private List<TipoMedicion> tipomediciones=null;

	@PostConstruct
	public void init(){
		service=new TipoMedicionService();
		tipomediciones=service.getAllRows();
	}
	
	public List<TipoMedicion> getTipomediciones() {
		tipomediciones=(List<TipoMedicion>) service.getAllRows();
		return tipomediciones;
	}

	public void setTipomediciones(List<TipoMedicion> tipomediciones) {
		this.tipomediciones = tipomediciones;
	}
	
}
