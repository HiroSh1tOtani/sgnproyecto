package pe.com.indra.calidad.mbean;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.time.Month;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import pe.com.indra.calidad.entity.IntervaloTension;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.service.IntervaloTensionService;
import pe.com.indra.calidad.service.MedicionService;
import pe.com.indra.calidad.util.ConectaDb;
import pe.com.indra.calidad.util.ModificarExcel;
import pe.com.indra.calidad.util.Utilidad;

@ManagedBean(name="intervaloTensionBean")
@SessionScoped
public class IntervaloTensionBean {
	
	private static final long serialVersionUID = 1L;
	
	private List<IntervaloTension> intervaloTensiones;
	private List<IntervaloTension> intervaloTensiones1;
	private Medicion medicion;
	private DefaultCategoryDataset graphData;
	private XYDataset datasetXY;
	
	
	private IntervaloTension intervaloTension;
	private List<SelectItem> itemsMedicion;
	
	private String tension;
	private String medicionId;
	
	private List<SelectItem> itemsTension;
	
	
	
	public IntervaloTensionBean() {
		intervaloTension = new IntervaloTension();
		intervaloTension.setEstado("1");
	}
	
	@PostConstruct 
	public void init(){
		intervaloTension = new IntervaloTension();
		intervaloTension.setEstado("1");
		
		setTension("R");
		

	}
	
	public String getMedicionId() {
		return medicionId;
	}

	public void setMedicionId(String medicionId) {
		this.medicionId = medicionId;
	}

	
	public List<SelectItem> getItemsTension() {
		itemsTension=new ArrayList<SelectItem>();
		itemsTension.add(new SelectItem("R","Tension R"));
		itemsTension.add(new SelectItem("S","Tension S"));
		itemsTension.add(new SelectItem("T","Tension T"));
		itemsTension.add(new SelectItem("RST","Tensiones RST"));
		return itemsTension;
	}

	public void setItemsTension(List<SelectItem> itemsTension) {
		this.itemsTension = itemsTension;
	}

	
	/*
	public List<IntervaloTension> getIntervaloTensiones() {
		
		IntervaloTensionService service=new IntervaloTensionService();
		intervaloTensiones= service.getAllRows();
		return intervaloTensiones;
	}
	
	*/

	public Medicion getMedicion() {
		//String medicionId=Utilidad.getParametro("medicionId");
		
		if (medicionId != null ){
			
			MedicionService service=new MedicionService();
			medicion=service.ReadById(new Long(medicionId));
			
		}
	
		return medicion;
	}

	public void setMedicion(Medicion medicion) {
		this.medicion = medicion;
	}

	public void setIntervaloTensiones(List<IntervaloTension> intervaloTensiones) {
		this.intervaloTensiones = intervaloTensiones;
	}

	public List<IntervaloTension> getIntervaloTensiones1() {
		IntervaloTensionService service=new IntervaloTensionService();
		
		//String medicionId=Utilidad.getParametro("medicionId");
		getMedicion();
		
		/*
		if(tipo != null && tipo.equals("1")) {
			intervaloTensiones1= service.getAllRows(medicion.getMedicionId());
			
		}else {
			intervaloTensiones1= service.getAllRows1(medicion.getMedicionId());
		}
		*/
		
		intervaloTensiones1= service.getAllRows1(medicion.getMedicionId());
		
		if(intervaloTensiones1.size() == 0){
			intervaloTensiones1= service.getAllRows(medicion.getMedicionId());
		}
		
		
		return intervaloTensiones1;
	}

	public void setIntervaloTensiones1(List<IntervaloTension> intervaloTensiones1) {
		this.intervaloTensiones1 = intervaloTensiones1;
	}

	public IntervaloTension getIntervaloTension() {
		return intervaloTension;
	}

	public void setIntervaloTension(IntervaloTension intervaloTension) {
		this.intervaloTension = intervaloTension;
	}

	public List<SelectItem> getItemsMedicion() {
		return itemsMedicion;
	}

	public void setItemsMedicion(List<SelectItem> itemsMedicion) {
		this.itemsMedicion = itemsMedicion;
	}
	
	public void cargarIntervaloTensionActual(){
		String intTensionId=Utilidad.getParametro("intTensionId");
		IntervaloTensionService service=new IntervaloTensionService();
		intervaloTension=service.ReadById(new Long(intTensionId));
	}
	
	
	public void setGraphData(DefaultCategoryDataset graphData) {
		this.graphData = graphData;
	}

	public DefaultCategoryDataset getGraphData() {

		DefaultCategoryDataset categoryDataSet=null;

		try{
			categoryDataSet = new DefaultCategoryDataset();
			Double currentTemperature= 25.3;
			Double minTemperature=10.2;
			Double maxTemperature=45.0;
			Double avgTemperature=30.0;
			categoryDataSet.addValue(maxTemperature,"","Highest Temperature");
			categoryDataSet.addValue(minTemperature,"","Lowest Temperature");
			categoryDataSet.addValue(avgTemperature,"","Average Temperature");
			categoryDataSet.addValue(currentTemperature,"","current Temperature");
			graphData = categoryDataSet;
			return graphData;

		}catch(Exception sq){

			System.out.println("exception2 " +sq);
			sq.printStackTrace();

			return null;
		}
	}

	public XYDataset getDatasetXY() {
		TimeSeriesCollection timeSeriesDataSet = new TimeSeriesCollection();
        TimeSeries s1 = new TimeSeries("1", Month.class);
        s1.add(new Month(2, 2001), 181.8);
        s1.add(new Month(3, 2001), 167.3);
        s1.add(new Month(4, 2001), 153.8);
        s1.add(new Month(5, 2001), 167.6);
        s1.add(new Month(6, 2001), 158.8);
        s1.add(new Month(7, 2001), 148.3);
        s1.add(new Month(8, 2001), 153.9);
        s1.add(new Month(9, 2001), 142.7);
        s1.add(new Month(10, 2001), 123.2);
        s1.add(new Month(11, 2001), 131.8);
        s1.add(new Month(12, 2001), 139.6);
        s1.add(new Month(1, 2002), 142.9);
        s1.add(new Month(2, 2002), 138.7);
        s1.add(new Month(3, 2002), 137.3);
        s1.add(new Month(4, 2002), 143.9);
        s1.add(new Month(5, 2002), 139.8);
        s1.add(new Month(6, 2002), 137.0);
        s1.add(new Month(7, 2002), 132.8);
        
        TimeSeries s2 = new TimeSeries("2", Month.class);
        s2.add(new Month(2, 2001), 129.6);
        s2.add(new Month(3, 2001), 123.2);
        s2.add(new Month(4, 2001), 117.2);
        s2.add(new Month(5, 2001), 124.1);
        s2.add(new Month(6, 2001), 122.6);
        s2.add(new Month(7, 2001), 119.2);
        s2.add(new Month(8, 2001), 116.5);
        s2.add(new Month(9, 2001), 112.7);
        s2.add(new Month(10, 2001), 101.5);
        s2.add(new Month(11, 2001), 106.1);
        s2.add(new Month(12, 2001), 110.3);
        s2.add(new Month(1, 2002), 111.7);
        s2.add(new Month(2, 2002), 111.0);
        s2.add(new Month(3, 2002), 109.6);
        s2.add(new Month(4, 2002), 113.2);
        s2.add(new Month(5, 2002), 111.6);
        s2.add(new Month(6, 2002), 108.8);
        s2.add(new Month(7, 2002), 101.6);
        timeSeriesDataSet.addSeries(s1);
        timeSeriesDataSet.addSeries(s2);
		 
        datasetXY = timeSeriesDataSet;
		return datasetXY;
		
	}

	public void setDatasetXY(XYDataset datasetXY) {
		this.datasetXY = datasetXY;
	}

	public String getTension() {
		return tension;
	}

	public void setTension(String tension) {
		this.tension = tension;
	}
	
	public String muestraIntervalos(){
		
		medicionId=Utilidad.getParametro("medicionId");
		
		getIntervaloTensiones1();
		
		return "/zonasegura/intervaloTensionListar?faces-redirect=true";
	}
	
	public String muestraIntervalos2(){
		
		medicionId=Utilidad.getParametro("medicionId");	
		
		getIntervaloTensiones1();
				
		return "/zonasegura/intervaloTensionListar2?faces-redirect=true";
	}
	
	public String downloadGrafico() {
	    final FacesContext facesContext = FacesContext.getCurrentInstance();
	    
	    String rutaArchivoOut;
	    XSSFWorkbook objHssfWorkbook ;
	    String rutaUp;
	    String nombreArchivo;
	    if(medicion.getTipSuministro().equals("BT")){
		    if(medicion.getTipServicio().equals("U")){
		    	if (medicion.getTipoAlimentacion().getCodTipAlimentacion().equals("MO")){
		    		rutaUp = facesContext.getExternalContext().getRealPath("/reportes/GraficoTensionMonofasico.xlsm");
		    	}else{
		    		rutaUp = facesContext.getExternalContext().getRealPath("/reportes/GraficoTensionTrifasica.xlsm");
		    	}
		    }else{
		    	rutaUp = facesContext.getExternalContext().getRealPath("/reportes/GraficoTensionRural.xlsm");
		    }
		    
		    String path = System.getProperty("uploads.folder");
		    //String nombreArchivo;
		    
		    System.out.print(path + "\n");
		    
		    nombreArchivo = "GraficoTen_" + medicion.getNumSumMedicion() + "_" + new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsm";
		    
		    rutaArchivoOut = path + nombreArchivo;
		    	    
		    objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);
	
			XSSFSheet objHssfSheet = null;
			
			IntervaloTensionService intervaloTensionService = new IntervaloTensionService();
			
			if(medicion.getTipServicio().equals("U")){
				objHssfSheet = objHssfWorkbook.getSheetAt(0);
				
				int intRow = 4;
				int intCell = 0;
				
				ModificarExcel.setCellValue(objHssfSheet, 2, 6, medicion.getTenEntregada());
				ModificarExcel.setCellValue(objHssfSheet, 3, 6, medicion.getTipServicio());
				ModificarExcel.setCellValue(objHssfSheet, 4, 6, medicion.getTipSuministro());		
				ModificarExcel.setCellValue(objHssfSheet, 1, 9, medicion.getNumSumMedicion());		
				ModificarExcel.setCellValue(objHssfSheet, 2, 9, medicion.getCodSet());
				ModificarExcel.setCellValue(objHssfSheet, 3, 9, medicion.getCodAlimentador());
				ModificarExcel.setCellValue(objHssfSheet, 4, 9, medicion.getSalidabt());
				ModificarExcel.setCellValue(objHssfSheet, 1, 11, medicion.getNomUsuario());		
				ModificarExcel.setCellValue(objHssfSheet, 2, 11, medicion.getCodAlimentador());
				ModificarExcel.setCellValue(objHssfSheet, 3, 11, medicion.getCodSed());
				ModificarExcel.setCellValue(objHssfSheet, 4, 11, medicion.getCampanaMedicion().getPeriodoMedicion().getAno()+"-"+medicion.getCampanaMedicion().getPeriodoMedicion().getPeriodo());
				
				for(IntervaloTension it:intervaloTensionService.getAllRows1(medicion.getMedicionId())){
					ModificarExcel.setCellValue(objHssfSheet, intRow, intCell, it.getIntervalo());
					ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 1, it.getTensionR());
					ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 2, it.getTensionS());
					ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 3, it.getTensionT());
					intRow++;
				};
			
			}else{
				objHssfSheet = objHssfWorkbook.getSheetAt(0);
				
				int intRow = 9;
				int intCell = 0;
				
				MedicionService medicionService = new MedicionService();
				Medicion medicion2 = medicionService.getMedicionDuoRural(medicion.getCampanaMedicion().getCamMedicionId(), medicion.getMedicionId());
	
				ModificarExcel.setCellValue(objHssfSheet, 2, 2, medicion.getCodSed());
				
				if(medicion2 != null){
					if(medicion.getUbiSumMedicion().equals("I")){
						for(IntervaloTension it:intervaloTensionService.getAllRows1(medicion.getMedicionId())){
							ModificarExcel.setCellValue(objHssfSheet, intRow, intCell, it.getIntervalo());
							ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 1, it.getTenIntervalo());
							
						}
						intCell = 0;
						intRow = 9;
						
						for(IntervaloTension it:intervaloTensionService.getAllRows1(medicion2.getMedicionId())){
							ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 2, it.getTenIntervalo());
							ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 4, medicion.getTenEntregada()*1.075);
							ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 5, medicion.getTenEntregada()*0.925);
							intRow++;
						}           
						
				} else {
					for(IntervaloTension it:intervaloTensionService.getAllRows1(medicion2.getMedicionId())){
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell, it.getIntervalo());
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 2, it.getTenIntervalo());
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 4, medicion.getTenEntregada()*1.075);
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 5, medicion.getTenEntregada()*0.925);
						intRow++;
					}
					
					intCell = 0;
					intRow = 9;
					
					for(IntervaloTension it:intervaloTensionService.getAllRows1(medicion.getMedicionId())){
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 1, it.getTenIntervalo());
						intRow++;
					
						}
					}
					
				}else {
					if(medicion.getUbiSumMedicion().equals("I")){
						for(IntervaloTension it:intervaloTensionService.getAllRows1(medicion.getMedicionId())){
							ModificarExcel.setCellValue(objHssfSheet, intRow, intCell, it.getIntervalo());
							ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 1, it.getTenIntervalo());
							
						}
						intCell = 0;
						intRow = 9;
						
						for(IntervaloTension it:intervaloTensionService.getAllRows1(medicion2.getMedicionId())){
							ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 2, it.getTenIntervalo());
							ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 4, medicion.getTenEntregada()*1.075);
							ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 5, medicion.getTenEntregada()*0.925);
							intRow++;
						}           
						
				} else {
					for(IntervaloTension it:intervaloTensionService.getAllRows1(medicion2.getMedicionId())){
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell, it.getIntervalo());
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 2, it.getTenIntervalo());
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 4, medicion.getTenEntregada()*1.075);
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 5, medicion.getTenEntregada()*0.925);
						intRow++;
					}
					
					intCell = 0;
					intRow = 9;
					
					for(IntervaloTension it:intervaloTensionService.getAllRows1(medicion.getMedicionId())){
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 1, it.getTenIntervalo());
						intRow++;
					
						}
					}
					
				}
				
			}
	    }else {
	    	
	    	if(medicion.getTipServicio().equals("U")){
		    	
	    		rutaUp = facesContext.getExternalContext().getRealPath("/reportes/GraficoTensionMT_U.xlsm");
		    	
		    }else{
		    	rutaUp = facesContext.getExternalContext().getRealPath("/reportes/GraficoTensionMT_R.xlsm");
		    }
		    	
	    	 String path = System.getProperty("uploads.folder");
			 //String nombreArchivo;
			    
			 System.out.print(path + "\n");
			    
			 nombreArchivo = "GraficoTen_MT_" + medicion.getNumSumMedicion() + "_" + new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsm";
			    
			 rutaArchivoOut = path + nombreArchivo;
			 objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);
			 XSSFSheet objHssfSheet = null;
			 XSSFSheet objHssfSheet2 = null;
			 
			 IntervaloTensionService intervaloTensionService = new IntervaloTensionService();
			 
			 objHssfSheet = objHssfWorkbook.getSheetAt(0);
			 objHssfSheet2 = objHssfWorkbook.getSheetAt(1);
				
				int intRow = 2;
				int intCell = 0;
				
				ModificarExcel.setCellValue(objHssfSheet, 2, 3, medicion.getCampanaMedicion().getPeriodoMedicion().getAno()+"-"+medicion.getCampanaMedicion().getPeriodoMedicion().getPeriodo());
				ModificarExcel.setCellValue(objHssfSheet, 3, 3, medicion.getTipServicio());
				ModificarExcel.setCellValue(objHssfSheet, 4, 3, medicion.getOpcTarifaria());
				ModificarExcel.setCellValue(objHssfSheet, 5, 3, medicion.getTipSuministro());
				ModificarExcel.setCellValue(objHssfSheet, 6, 3, medicion.getCodAlimentador());
				ModificarExcel.setCellValue(objHssfSheet, 7, 3, medicion.getTenEntregada());
				
				
				for(IntervaloTension it:intervaloTensionService.getAllRows1(medicion.getMedicionId())){
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell, it.getIntervalo());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 1, it.getTensionR());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 2, it.getTensionS());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 3, it.getTensionT());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 4, it.getEneIntervalo());
					intRow++;
				};
	    }	

		ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
	    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());
	    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
	    facesContext.responseComplete();

	    return null;
	  }

	public String downloadCompensacion() {
	    final FacesContext facesContext = FacesContext.getCurrentInstance();
	    
	    String rutaArchivoOut;
	    String rutaUp;
	    
	    if(medicion.getTipServicio().equals("U")){
	    	rutaUp = facesContext.getExternalContext().getRealPath("/reportes/ReporteCompensacion.xlsx");
	    }else{
	    	rutaUp = facesContext.getExternalContext().getRealPath("/reportes/ReporteCompensacionRural.xlsx");
	    }
	    String path = System.getProperty("uploads.folder");
	    String nombreArchivo;
	    
	    System.out.print(path + "\n");
	    
	    nombreArchivo = "Reporte_Compensacion_" + medicion.getNumSumMedicion() + "_" + new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsx";
	    rutaArchivoOut = path + nombreArchivo;
	    	    
	    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);

		XSSFSheet objHssfSheet = null;
		XSSFSheet objHssfSheet2 = null;
		
		if(medicion.getTipServicio().equals("U")){
		
			objHssfSheet = objHssfWorkbook.getSheetAt(1);
			
			IntervaloTensionService intervaloTensionService = new IntervaloTensionService();
			int intRow = 2;
			int intCell = 0;
			double tenNominal = medicion.getTenEntregada();
			String tipoAlimentacion = medicion.getTipoAlimentacion().getCodTipAlimentacion();
			double dif1;
			double dif2;
			double dif3;
			double mayor;
			String anoMes = medicion.getCampanaMedicion().getPeriodoMedicion().getAno()+medicion.getCampanaMedicion().getPeriodoMedicion().getPeriodo();

			
			String result;
			ConectaDb db = new ConectaDb("CALIDAD");
	
			Connection cn = db.getConnection();
			double dato=0;
			double energia=0;
			
	        CallableStatement cstmt = null;  
	        try {
	
	        	cstmt = cn.prepareCall("{CALL ? := CALIDAD_PACKAGE.FN_NHM(?)}");
				
				cstmt.registerOutParameter(1, Types.DOUBLE);
				cstmt.setString(2, anoMes);
				
	            int ctos = cstmt.executeUpdate();
	            dato = cstmt.getDouble(1);
	            cstmt.close(); 
	
	            cstmt = cn.prepareCall("{CALL ? := CALIDAD_PACKAGE.FN_OBTENER_ENERGIA(?,?,?)}");
				
				cstmt.registerOutParameter(1, Types.DOUBLE);
				cstmt.setLong(2, Long.valueOf(medicion.getNumSumMedicion()));
				cstmt.setString(3, medicion.getCampanaMedicion().getPeriodoMedicion().getAno());
				cstmt.setString(4, medicion.getCampanaMedicion().getPeriodoMedicion().getPeriodo());
				
	            ctos = cstmt.executeUpdate();
	            energia = cstmt.getDouble(1);
	            cstmt.close();
	            
	            if (ctos == 0) {
	                result = "0 filas afectadas";
	            }
	        	
	        } catch (SQLException e) {
	            result = e.getMessage();
	            energia = 0;
	            
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", "No se pudo completar la Transacción"));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	        } finally {
	            try {
	                cn.close();
	            } catch (SQLException e) {
	                result = e.getMessage();
	                
	    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", "No se pudo completar la Transacción"));
	    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	                
	            }				
	        }		
	        
	        objHssfSheet2 = objHssfWorkbook.getSheetAt(1); //agregadoJF
			for(IntervaloTension it:intervaloTensionService.getAllRows1(medicion.getMedicionId())){
				//if(it.getEstadoIntervalo().getCodEstIntervalo().equals("F")) {
					if(tipoAlimentacion.equals("MO")) {
						//mayor = Math.abs(tenNominal - it.getTensionR().longValue());
						mayor = it.getTensionR().doubleValue();
					} else {
						dif1 = Math.abs(tenNominal - it.getTensionR().doubleValue());
						dif2 = Math.abs(tenNominal - it.getTensionS().doubleValue());
						dif3 = Math.abs(tenNominal - it.getTensionT().doubleValue());
						if(dif1>=dif2 ) {
							if(dif1>=dif3){
								mayor = it.getTensionR().doubleValue();
							}else{ 
								mayor= it.getTensionT().doubleValue();
							}
						} else {
							if(dif2>=dif3) {
								mayor=it.getTensionS().doubleValue();
							} else {
								mayor=it.getTensionT().doubleValue();
							}
						}
					}
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell, it.getIntervalo());
					ModificarExcel.setCellValue(objHssfSheet2, intRow, intCell + 1, (double) mayor/1000);// en kilovoltios
					intRow++;
				//}
			};
	
			objHssfSheet = objHssfWorkbook.getSheetAt(0);
			ModificarExcel.setCellValue(objHssfSheet, 3, 2, medicion.getCampanaMedicion().getPeriodoMedicion().getAno()+"-"+medicion.getCampanaMedicion().getPeriodoMedicion().getPeriodo());
			ModificarExcel.setCellValue(objHssfSheet, 8, 3, medicion.getNumSumMedicion());
			ModificarExcel.setCellValue(objHssfSheet, 9, 3, medicion.getCodSed());
			ModificarExcel.setCellValue(objHssfSheet, 10, 3, medicion.getNomUsuario());
			ModificarExcel.setCellValue(objHssfSheet, 11, 3, medicion.getDirUsuario());
			ModificarExcel.setCellValue(objHssfSheet, 12, 3, medicion.getParametroMedido().getDescripcion());
			ModificarExcel.setCellValue(objHssfSheet, 14, 3, (double) tenNominal/1000);
			ModificarExcel.setCellValue(objHssfSheet, 33, 3, dato);
			// VALOR DE LA ENERGIA EN DURO DEBE CAMBIARSE POR LA INTERFACE
			ModificarExcel.setCellValue(objHssfSheet, 35, 3, energia);
		
		}else {
			
			
			objHssfSheet = objHssfWorkbook.getSheetAt(0);
			
				
			IntervaloTensionService intervaloTensionService = new IntervaloTensionService();
			
			int intRow = 56;
			int intCell = 1;
			
			MedicionService medicionService = new MedicionService();
			Medicion medicion2 = medicionService.getMedicionDuoRural(medicion.getCampanaMedicion().getCamMedicionId(), medicion.getMedicionId());
			
			if(medicion.getUbiSumMedicion().equals("I")){
			ModificarExcel.setCellValue(objHssfSheet, 1, 7, medicion.getCodSed());
			ModificarExcel.setCellValue(objHssfSheet, 5, 3, medicion.getNumSumMedicion());
			ModificarExcel.setCellValue(objHssfSheet, 5, 7, medicion2.getNumSumMedicion());
			}else {
				ModificarExcel.setCellValue(objHssfSheet, 1, 7, medicion.getCodSed());
				ModificarExcel.setCellValue(objHssfSheet, 5, 7, medicion.getNumSumMedicion());
				ModificarExcel.setCellValue(objHssfSheet, 5, 3, medicion2.getNumSumMedicion());
			}
			
			if(medicion2 != null) {
				if(medicion.getUbiSumMedicion().equals("I")){
					for(IntervaloTension it:intervaloTensionService.getAllRows1(medicion.getMedicionId())){
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell, it.getIntervalo());
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 1, it.getIntervalo());
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 2, it.getTenIntervalo());
						intRow++;
					
					}
					
					intRow = 56;
					intCell = 1;
					
					for(IntervaloTension it:intervaloTensionService.getAllRows1(medicion2.getMedicionId())){
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 5, it.getTenIntervalo());
						intRow++;
					
					}
					
				} else {
					for(IntervaloTension it:intervaloTensionService.getAllRows1(medicion2.getMedicionId())){
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell, it.getIntervalo());
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 1, it.getIntervalo());
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 2, it.getTenIntervalo());
						intRow++;
					
					}
					
					intRow = 56;
					intCell = 1;
					
					for(IntervaloTension it:intervaloTensionService.getAllRows1(medicion.getMedicionId())){

						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 5, it.getTenIntervalo());
						intRow++;
					
					}
							
				}
				
			} /*else {
				//si medicion esta al inicio o en la cola
				if(medicion.getUbiSumMedicion().equals("I")) {
					for(IntervaloTension it:intervaloTensionService.getAllRows1(medicion.getMedicionId())){
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell, it.getIntervalo());
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 1, it.getIntervalo());
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 2, it.getTenIntervalo());
						intRow++;
					}
					
					intRow = 56;
					intCell = 1;
					
					for(IntervaloTension it:intervaloTensionService.getAllRows1(medicion2.getMedicionId())){
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 5, it.getTenIntervalo());
						intRow++;
					
					}
					
					
				}else {
					for(IntervaloTension it:intervaloTensionService.getAllRows1(medicion2.getMedicionId())){
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell, it.getIntervalo());
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 1, it.getIntervalo());
						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 2, it.getTenIntervalo());
						intRow++;
					}		
					
					intRow = 56;
					intCell = 1;
					
					for(IntervaloTension it:intervaloTensionService.getAllRows1(medicion.getMedicionId())){

						ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 5, it.getTenIntervalo());
						intRow++;
					
					}
				}
				
			}*/
		
		}
		ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
	    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());
	    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
	    facesContext.responseComplete();

	    return null;
	  }

	
	  void writeOutContent(final HttpServletResponse res, final File content, final String theFilename, String contentType) {
	    if (content == null)
	      return;
	    try {
	      res.setHeader("Pragma", "no-cache");
	      res.setDateHeader("Expires", 0);
	      res.setContentType(contentType);
	      res.setHeader("Content-disposition", "attachment; filename=" + theFilename);
	      fastChannelCopy(Channels.newChannel(new FileInputStream(content)), Channels.newChannel(res.getOutputStream()));
	    } catch (final IOException e) {
	      // TODO produce a error message <img src="http://s0.wp.com/wp-includes/images/smilies/icon_smile.gif?m=1129645325g" alt=":)" class="wp-smiley"> 
	    }
	  }
	 
	  void fastChannelCopy(final ReadableByteChannel src, final WritableByteChannel dest) throws IOException {
	    final ByteBuffer buffer = ByteBuffer.allocateDirect(1000 * 1024);
	    while (src.read(buffer) != -1) {
	      buffer.flip();
	      dest.write(buffer);
	      buffer.compact();
	    }
	    buffer.flip();
	    while (buffer.hasRemaining()){
	      dest.write(buffer);
	    }
	  }
}
