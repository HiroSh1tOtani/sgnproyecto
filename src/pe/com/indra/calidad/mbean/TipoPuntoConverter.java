package pe.com.indra.calidad.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.indra.calidad.entity.TipoPunto;
import pe.com.indra.calidad.service.TipoPuntoService;

@FacesConverter(value="tipoPuntoConverter",forClass=TipoPuntoConverter.class)

public class TipoPuntoConverter implements Converter{

	@Override
	public TipoPunto getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long tipMedicionId=new Long(cadena);
		TipoPuntoService service=new TipoPuntoService();
		TipoPunto tipoPunto=new TipoPunto();
		tipoPunto=service.ReadById(tipMedicionId);
		System.out.println(tipoPunto.getCodTipPunto()+":"+tipoPunto.getDescripcion());
		return tipoPunto;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			TipoPunto tipoPunto=(TipoPunto) objeto;
			//System.out.println("objeto:"+new Long(tipoPunto.getTipPuntoId()).toString());
			return new Long(tipoPunto.getTipPuntoId()).toString();
		}
		return null;
	}


}
