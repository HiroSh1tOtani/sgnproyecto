package pe.com.indra.calidad.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.indra.calidad.entity.ParametroMedido;
import pe.com.indra.calidad.service.ParametroMedidoService;

@FacesConverter(value="parametroMedidoConverter",forClass=ParametroMedidoConverter.class)

public class ParametroMedidoConverter implements Converter{

	@Override
	public ParametroMedido getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long tipParametroId=new Long(cadena);
		ParametroMedidoService service=new ParametroMedidoService();
		ParametroMedido parametroMedido=new ParametroMedido();
		parametroMedido=service.ReadById(tipParametroId);
		System.out.println(parametroMedido.getCodParMedido()+":"+parametroMedido.getDescripcion());
		return parametroMedido;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			ParametroMedido parametroMedido=(ParametroMedido) objeto;
			//System.out.println("objeto:"+new Long(parametroMedido.getParMedidoId()).toString());
			return new Long(parametroMedido.getParMedidoId()).toString();
		}
		return null;
	}


}
