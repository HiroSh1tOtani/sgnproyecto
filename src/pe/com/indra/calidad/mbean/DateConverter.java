package pe.com.indra.calidad.mbean;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;



@FacesConverter(value="dateConverter",forClass=ClobConverter.class)

public class DateConverter implements Converter{

	@Override
	public Date getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date date;
		try {
	 
			date = formatter.parse(cadena);
	 
		} catch (ParseException e) {
			e.printStackTrace();
			
			return null;
		}
		
		return date;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			             
			 DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			 Date date = (Date) objeto;
			 String datestring = dateFormat.format(date);
			 
			 return datestring;
		}
		return null;
	}


}
