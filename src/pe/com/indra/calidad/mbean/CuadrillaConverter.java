package pe.com.indra.calidad.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.indra.calidad.entity.Cuadrilla;
import pe.com.indra.calidad.service.CuadrillaService;

@FacesConverter(value="cuadrillaConverter",forClass=CuadrillaConverter.class)

public class CuadrillaConverter implements Converter{

	@Override
	public Cuadrilla getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long cuadrillaId=new Long(cadena);
		CuadrillaService service=new CuadrillaService();
		Cuadrilla cuadrilla=new Cuadrilla();
		cuadrilla=service.ReadById(cuadrillaId);
		System.out.println(cuadrilla.getCuadrillaId());
		return cuadrilla;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			Cuadrilla cuadrilla=(Cuadrilla) objeto;
			//System.out.println("objeto:"+new Long(cuadrilla.getCuadrillaId()).toString());
			return new Long(cuadrilla.getCuadrillaId()).toString();
		}
		return null;
	}


}
