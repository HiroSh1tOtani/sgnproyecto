package pe.com.indra.calidad.mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.hibernate.HibernateException;
import org.richfaces.model.Filter;

import pe.com.calidad.suministro.entity.CausaInterrupcion;
import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.service.CausaInterrupcionService;
import pe.com.indra.calidad.entity.ParametroNorma;
import pe.com.indra.calidad.entity.TipoParametro;
import pe.com.indra.calidad.service.ParametroNormaService;
import pe.com.indra.calidad.service.TipoParametroService;
import pe.com.indra.calidad.util.Utilidad;

@ManagedBean(name="parametroNormaBean")
@SessionScoped
public class ParametroNormaBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<ParametroNorma> parametrosNorma;
	private ParametroNorma parametroNorma;
	private List<SelectItem> itemsSectorTipico;
	private List<SelectItem> itemsTipoParametro;
	
	private List<SelectItem> itemsTipoParNorma;
	
	private String mensaje;
	
	private String tipParametroFilter;
	private List<SelectItem> itemsTipParametro;
	
	@PostConstruct 
	public void init(){
		System.out.println("se ejecuto PostConstruct "+new java.util.Date());
		parametroNorma=new ParametroNorma();
		parametroNorma.setEstado("1");
		/*
		String parametroNormaId=Utilidad.getParametro("parametroNormaId");
		if(parametroNormaId==null){
		  parametroNorma=new ParametroNorma();
		  parametroNorma.setEstado("1");
		}else{
			ParametroNormaService service=new ParametroNormaService();
			parametroNorma=service.ReadById(new Long(parametroNormaId));
		}
		*/
	}
	
	public List<ParametroNorma> getParametrosNorma() {
		ParametroNormaService service=new ParametroNormaService();
		parametrosNorma=service.getAllRows();
		return parametrosNorma;
	}

	public void setParametrosNorma(List<ParametroNorma> parametrosNorma) {
		this.parametrosNorma = parametrosNorma;
	}

	public ParametroNorma getParametroNorma() {
		
		return parametroNorma;
	}

	public void setParametroNorma(ParametroNorma parametroNorma) {
		this.parametroNorma = parametroNorma;
	}
	
	public List<SelectItem> getItemsSectorTipico() {
		itemsSectorTipico=new ArrayList<SelectItem>();
		itemsSectorTipico.add(new SelectItem("U","NTCSE"));
		itemsSectorTipico.add(new SelectItem("R","NTCSER"));
		itemsSectorTipico.add(new SelectItem("P","PROCEDIMIENTOS"));
		return itemsSectorTipico;
	}

	public void setItemsSectorTipico(List<SelectItem> itemsSectorTipico) {
		this.itemsSectorTipico = itemsSectorTipico;
	}
	
	public List<SelectItem> getItemsTipoParNorma() {
		itemsTipoParNorma=new ArrayList<SelectItem>();
		itemsTipoParNorma.add(new SelectItem("C","CONSTANTE"));
		itemsTipoParNorma.add(new SelectItem("I","INDICADOR CON TOLERANCIA"));
		return itemsTipoParNorma;
	}

	public void setItemsTipoParNorma(List<SelectItem> itemsTipoParNorma) {
		this.itemsTipoParNorma = itemsTipoParNorma;
	}

		
	public List<SelectItem> getItemsTipoParametro() {
		TipoParametroService service=new TipoParametroService();
		itemsTipoParametro=new ArrayList<SelectItem>();
		for(TipoParametro tp:service.getAllRows()){
			itemsTipoParametro.add(new SelectItem(tp,tp.getCodTipParametro()+"-"+tp.getDescripcion()));
		}
		return itemsTipoParametro;
	}

	public void setItemsTipoParametro(List<SelectItem> itemsTipoParametro) {
		this.itemsTipoParametro = itemsTipoParametro;
	}

	
	public String prepararCrear(){
		//Esto funciona siempre que el bean tenga ambito de sesion 
		setParametroNorma(new ParametroNorma());
		getParametroNorma().setEstado("1");
		return "/zonasegura/parametroNormaCrear?faces-redirect=true";
	}
	
	public String prepararEditar(){
		//TODO: Por implementar carga de la instancia
		//System.out.println("parametroNormaId:"+Utilidad.getParametro("parametroNormaId"));
		//cargarParametroNormaActual();
		//return "/zonasegura/parametroNormaEditar?faces-redirect=true";
		
		try {
			cargarParametroNormaActual();
			return "/zonasegura/parametroNormaEditar?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/parametroNormaListar?faces-redirect=true";
		}
	}

	public String prepararVer(){
		//TODO: Por implementar carga de la instancia
		//System.out.println("parametroNormaId:"+Utilidad.getParametro("parametroNormaId"));
		//cargarParametroNormaActual();
		//return "/zonasegura/parametroNormaVer?faces-redirect=true";
		
		try {
			cargarParametroNormaActual();
			return "/zonasegura/parametroNormaVer?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/parametroNormaListar?faces-redirect=true";
		}
	}
	
	public String salvar(){
		//ParametroNormaService service=new ParametroNormaService();
		//service.create(parametroNorma);
		//System.out.println("Se salvo un parametroNorma con exito.");
		//return "/zonasegura/parametroNormaListar?faces-redirect=true";
		
		ParametroNormaService service=new ParametroNormaService();
		
		try {
			service.create(parametroNorma);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/parametroNormaListar?faces-redirect=true";
	}
	
	public String actualizar(){
		//ParametroNormaService service=new ParametroNormaService();
		//service.update(parametroNorma);
		//System.out.println("Se actualizo un parametroNorma con exito.");
		//return "/zonasegura/parametroNormaListar?faces-redirect=true";
		
		ParametroNormaService service=new ParametroNormaService();
		
		try {
			service.update(parametroNorma);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/parametroNormaListar?faces-redirect=true";
	}
	
	public String eliminar(){
		String parametroNormaId=Utilidad.getParametro("parametroNormaId");
		ParametroNormaService service=new ParametroNormaService();
		//parametroNorma=service.ReadById(new Long(parametroNormaId));
		//parametroNorma.setEstado("0");
		//service.delete(parametroNorma.getParNormaId());
		//System.out.println("Se elimino un parametroNorma con exito.");
		//return "/zonasegura/parametroNormaListar?faces-redirect=true";
		
		try {
			parametroNorma=service.ReadById(new Long(parametroNormaId));
			parametroNorma.setEstado("0");
			//service.delete(parametroNorma.getParNormaId());
			service.update(parametroNorma);
		} catch (HibernateException e) {
			
			
			System.out.println(e.getMessage()+":"+e.getCause());
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			
		} catch (Exception e) {
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/parametroNormaListar?faces-redirect=true";
	}
	
	public void cargarParametroNormaActual(){
		String parametroNormaId=Utilidad.getParametro("parametroNormaId");
		ParametroNormaService service=new ParametroNormaService();
		//parametroNorma=service.ReadById(new Long(parametroNormaId));
		
		try {
			parametroNorma=service.ReadById(new Long(parametroNormaId));
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

			throw new HibernateException("No se puede cargar Parametro.", e);
			
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public List<SelectItem> getItemsTipParametro() {
		TipoParametroService tipoParametroService = new TipoParametroService();
		
		itemsTipParametro=new ArrayList<SelectItem>();
		itemsTipParametro.add(new SelectItem("", ""));
		
		for (TipoParametro tp : tipoParametroService.getAllRows1()) {
			itemsTipParametro.add(new SelectItem(tp.getCodTipParametro(),tp.getDescripcion()));
		}
		
		return itemsTipParametro;
	}
	
		
	public void setItemsTipParametro(List<SelectItem> itemsTipParametro) {
		this.itemsTipParametro = itemsTipParametro;
	}
	
	
	public String getTipParametroFilter() {
		return tipParametroFilter;
	}

	public void setTipParametroFilter(String tipParametroFilter) {
		this.tipParametroFilter = tipParametroFilter;
	}
	
	public Filter<?> getFilterTipParametro() {
        return new Filter<ParametroNorma>() {
            public boolean accept(ParametroNorma t) {
            	String tipParametro = getTipParametroFilter();
            	
                if (tipParametro == null || tipParametro.length() == 0 || tipParametro.equals(t.getTipoParametro().getCodTipParametro())) {
                    return true;
                }
                return false;
            }
        };
    }
}
