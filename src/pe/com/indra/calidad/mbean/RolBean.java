package pe.com.indra.calidad.mbean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import pe.com.indra.calidad.entity.Rol;
import pe.com.indra.calidad.entity.Rol;
import pe.com.indra.calidad.service.RolService;
import pe.com.indra.calidad.util.Utilidad;

@SuppressWarnings("serial")
@ManagedBean(name="rolBean")
@SessionScoped
public class RolBean implements Serializable{
	
	private Rol rol;
	
	private List<Rol> roles;
	
	public RolBean(){}
	
	
	
	
	@PostConstruct
	public void init() {
			rol =  new Rol();
	}

	public Rol getrol() {
		return rol;
	}

	public void setrol(Rol rol) {
		this.rol = rol;
	}
	
	public String prepareCreate(){
		String forward = "/zonasegura/seguridad/rolCrear?faces-redirect=true";
		rol = new Rol();
		rol.setGrupo("Users");
		return forward;
	}
	
	public String prepareUpdate(Rol rol){
		String forward = "/zonasegura/seguridad/rolEditar?faces-redirect=true";
		// TODO
		this.rol =  rol;
		return forward;
	}
	
	public String prepareView(Rol rol){
		String forward = "/zonasegura/seguridad/rolVer?faces-redirect=true";
		//TODO
		this.rol =  rol;
		return forward;
	}
	
	public String prepareDelete(Rol rol){
		String forward = "/zonasegura/seguridad/rolEliminar?faces-redirect=true";
		//TODO
		this.rol =  rol;
		return forward;
	}
	
	public String prepareListar(){
		String forward = "/zonasegura/seguridad/rolListar?faces-redirect=true";
		//TODO
		return forward;
	}
	
	public String create(){
		String forward = "/zonasegura/seguridad/rolListar?faces-redirect=true";
		RolService service =  new RolService();
		service.create(getrol());
		return forward;
	}
	
	public String update(){
		String forward = "/zonasegura/seguridad/rolListar?faces-redirect=true";
		RolService service =  new RolService();
		Rol temp = rol;
		service.delete(rol);
		service.create(temp);
		return forward;
	}
	
	public String delete(){
		String forward = "/zonasegura/seguridad/rolListar?faces-redirect=true";
		RolService service =  new RolService();
		service.delete(rol);
		return forward;
	}

	public List<Rol> getRoles() {
		RolService service =  new RolService();
		roles = service.getAllRoles();
		return roles;
	}

	public void setRoles(List<Rol> roles) {
		this.roles = roles;
	}
	
}
