package pe.com.indra.calidad.mbean;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.hibernate.HibernateException;
import org.richfaces.model.Filter;

import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.indra.calidad.entity.CronogramaMedicion;
import pe.com.indra.calidad.entity.Cuadrilla;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.service.CronogramaMedicionService;
import pe.com.indra.calidad.service.CuadrillaService;
import pe.com.indra.calidad.service.MedicionService;
import pe.com.indra.calidad.entity.CampanaMedicion;
import pe.com.indra.calidad.service.CampanaMedicionService;
import pe.com.indra.calidad.util.Utilidad;

@ManagedBean(name="cronogramaMedicionBean")
@SessionScoped
public class CronogramaMedicionBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	private CampanaMedicion campanaMedicion;
	private Cuadrilla cuadrilla;
	
	private java.util.Date fecPlanificada;
	
	private CalendarBean calendarBean;
	private Medicion medicion;
	private CronogramaMedicion cronogramaMedicion;
	private List<Medicion> mediciones;
	private List<CronogramaMedicion> cronogramasMedicion;
	private List<CronogramaMedicion> cronogramasMedicion1;
	private List<SelectItem> itemsCampanaMedicion;
	private List<SelectItem> itemsCuadrilla;
	private List<SelectItem> itemsCampanaMedicion1;
	private List<SelectItem> itemsCuadrilla1;
	
	private List<SelectItem> itemsMedicion1;
	private List<SelectItem> itemsMedicion;
	
	private String nisFilter;
	
	private String mensaje;
	private java.util.Date fecPlaniFilter;
	
	
	public List<SelectItem> getItemsMedicion1() {
		
		itemsMedicion1=new ArrayList<SelectItem>();
		
		if (campanaMedicion.getCamMedicionId() > 0) {
		
			MedicionService service=new MedicionService();
			
			for(Medicion tp:service.getAllRows1NoProgramados(campanaMedicion.getCamMedicionId())){
				
				itemsMedicion1.add(new SelectItem(tp,tp.getNumSumMedicion()+"-"+tp.getNomUsuario()));
			}
			
		}
		return itemsMedicion1;
		
	}


	public void setItemsMedicion1(List<SelectItem> itemsMedicion1) {
		this.itemsMedicion1 = itemsMedicion1;
	}

	public List<SelectItem> getItemsMedicion() {
		itemsMedicion = new ArrayList<SelectItem>();
		
		if (campanaMedicion.getCamMedicionId() > 0) {
		
			MedicionService service=new MedicionService();
			
			for(Medicion tp:service.getAllRows1(campanaMedicion.getCamMedicionId())){
				
				itemsMedicion.add(new SelectItem(tp,tp.getNumSumMedicion()+"-"+tp.getNomUsuario()));
			}
			
		}
		return itemsMedicion;
	}


	public void setItemsMedicion(List<SelectItem> itemsMedicion) {
		this.itemsMedicion = itemsMedicion;
	}


	public List<Medicion> getMediciones() {
		return mediciones;
	}

	public void setMediciones(List<Medicion> mediciones) {
		this.mediciones = mediciones;
	}

	public Medicion getMedicion() {
		return medicion;
	}

	public void setMedicion(Medicion medicion) {
		this.medicion = medicion;
	}

	@PostConstruct 
	public void init(){
		cronogramaMedicion = new CronogramaMedicion();
		cronogramaMedicion.setEstado("1");
		cuadrilla = new Cuadrilla();
		campanaMedicion = new CampanaMedicion();
		calendarBean = new CalendarBean();
		medicion = new Medicion();
		
		}
	
	
	public String CronogramaxCampana() {
		CronogramaMedicionService service=new CronogramaMedicionService();
		System.out.println("ANTES!!!");
		
		if(campanaMedicion.getCamMedicionId() > 0) {
			
			nisFilter = "";
			fecPlaniFilter = null;
			cronogramasMedicion= service.getAllRows1(campanaMedicion.getCamMedicionId());
			
		}
		
		
		return "/zonasegura/cronogramaMedicionListar?faces-redirect=true";
	}
	
	
	
	public CalendarBean getCalendarBean() {
		return calendarBean;
	}


	public void setCalendarBean(CalendarBean calendarBean) {
		this.calendarBean = calendarBean;
	}





	public CronogramaMedicion getCronogramaMedicion() {
		return cronogramaMedicion;
	}



	public void setCronogramaMedicion(CronogramaMedicion cronogramaMedicion) {
		this.cronogramaMedicion = cronogramaMedicion;
	}



	public CampanaMedicion getCampanaMedicion() {
		return campanaMedicion;
	}



	public void setCampanaMedicion(CampanaMedicion campanaMedicion) {
		this.campanaMedicion = campanaMedicion;
	}



	public Cuadrilla getCuadrilla() {
		return cuadrilla;
	}



	public void setCuadrilla(Cuadrilla cuadrilla) {
		this.cuadrilla = cuadrilla;
	}



	public java.util.Date getFecPlanificada() {
		return fecPlanificada;
	}



	public void setFecPlanificada(java.util.Date fecPlanificada) {
		this.fecPlanificada = fecPlanificada;
	}



	public List<CronogramaMedicion> getCronogramasMedicion() {
		return cronogramasMedicion;
	}



	public void setCronogramasMedicion(List<CronogramaMedicion> cronogramasMedicion) {
		this.cronogramasMedicion = cronogramasMedicion;
	}



	public List<CronogramaMedicion> getCronogramasMedicion1() {
		CronogramaMedicionService service=new CronogramaMedicionService();
		cronogramasMedicion1 = service.getAllRows1(campanaMedicion.getCamMedicionId());
		return cronogramasMedicion1;
	}



	public void setCronogramasMedicion1(
			List<CronogramaMedicion> cronogramasMedicion1) {
		this.cronogramasMedicion1 = cronogramasMedicion1;
	}



	public List<SelectItem> getItemsCampanaMedicion() {
		CampanaMedicionService service=new CampanaMedicionService();
		itemsCampanaMedicion=new ArrayList<SelectItem>();
		for(CampanaMedicion tp:service.getAllRows()){
			itemsCampanaMedicion.add(new SelectItem(tp,tp.getDescripcion() + " (" + tp.getPeriodoMedicion().getAno() + "-" + tp.getPeriodoMedicion().getPeriodo() + ")"));
		}
		
		return itemsCampanaMedicion;
	}


	public void setItemsCampanaMedicion(List<SelectItem> itemsCamapanaMedicion) {
		this.itemsCampanaMedicion = itemsCamapanaMedicion;
	}	



	public List<SelectItem> getItemsCuadrilla() {
		CuadrillaService service=new CuadrillaService();
		itemsCuadrilla=new ArrayList<SelectItem>();
		for(Cuadrilla tp:service.getAllRows()){
			itemsCuadrilla.add(new SelectItem(tp,tp.getCodCuadrilla()+"-"+tp.getDescripcion()));
		}
		return itemsCuadrilla;
	}

	public void setItemsCuadrilla(List<SelectItem> itemsCuadrilla) {
		this.itemsCuadrilla = itemsCuadrilla;
	}



	public List<SelectItem> getItemsCampanaMedicion1() {
		CampanaMedicionService service=new CampanaMedicionService();
		itemsCampanaMedicion1=new ArrayList<SelectItem>();
		for(CampanaMedicion tp:service.getAllRows1()){
			itemsCampanaMedicion1.add(new SelectItem(tp,tp.getDescripcion()+ " (" + tp.getPeriodoMedicion().getAno() + "-" + tp.getPeriodoMedicion().getPeriodo() + ")")); 
		}
		
		return itemsCampanaMedicion1;
	}


	public void setItemsCampanaMedicion1(List<SelectItem> itemsCampanaMedicion1) {
		this.itemsCampanaMedicion1 = itemsCampanaMedicion1;
	}



	public List<SelectItem> getItemsCuadrilla1() {
		CuadrillaService service=new CuadrillaService();
		itemsCuadrilla1=new ArrayList<SelectItem>();
		for(Cuadrilla tp:service.getAllRows1()){
			itemsCuadrilla1.add(new SelectItem(tp,tp.getCodCuadrilla()+"-"+tp.getDescripcion()));
			
			
		}
		return itemsCuadrilla1;
	}

	public void setItemsCuadrilla1(List<SelectItem> itemsCuadrilla1) {
		this.itemsCuadrilla1 = itemsCuadrilla1;
	}
	
	public String prepararCrear(){
		//Esto funciona siempre que el bean tenga ambito de sesion 
		fecPlanificada = null;
		setCronogramaMedicion(new CronogramaMedicion());
		getCronogramaMedicion().setEstado("1");
		return "/zonasegura/cronogramaMedicionCrear?faces-redirect=true";
	}
	
	public String prepararEditar(){
		//TODO: Por implementar carga de la instancia
		//System.out.println("medicionId:"+Utilidad.getParametro("medicionId"));
		//cargarCronogramaMedicionActual();
		//return "/zonasegura/cronogramaMedicionEditar?faces-redirect=true";
		
		try {
			cargarCronogramaMedicionActual();
			return "/zonasegura/cronogramaMedicionEditar?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/cronogramaMedicionListar?faces-redirect=true";
		}
	}

	public String prepararVer(){
		//TODO: Por implementar carga de la instancia
		//System.out.println("medicionId:"+Utilidad.getParametro("medicionId"));
		//cargarCronogramaMedicionActual();
		//return "/zonasegura/cronogramaMedicionVer?faces-redirect=true";
		
		try {
			cargarCronogramaMedicionActual();
			return "/zonasegura/cronogramaMedicionVer?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/cronogramaMedicionListar?faces-redirect=true";
		}
	}
	
	public String salvar(){
		//CronogramaMedicionService service=new CronogramaMedicionService();
		//service.create(cronogramaMedicion);
		//System.out.println("Se salvo un cronograma con exito.");
		//return "/zonasegura/cronogramaMedicionListar?faces-redirect=true";
		
		
		CronogramaMedicionService service=new CronogramaMedicionService();
		
		if(fecPlanificada!=null){
			cronogramaMedicion.setFecPlanificada(new java.sql.Date(fecPlanificada.getTime()));
		}else {
			cronogramaMedicion.setFecPlanificada(null);
		}
		
		try {
			
			service.create(cronogramaMedicion);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			return "/zonasegura/cronogramaMedicionCrear?faces-redirect=true";

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
					
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		System.out.println("Se salvo un cronograma con exito.");
		
		return CronogramaxCampana();
	}
	
	public String actualizar(){
		//CronogramaMedicionService service=new CronogramaMedicionService();
		//service.update(cronogramaMedicion);
		//System.out.println("Se actualizo un medicion con exito.");
		//return "/zonasegura/cronogramaMedicionListar?faces-redirect=true";
		
		CronogramaMedicionService service=new CronogramaMedicionService();

		if(fecPlanificada!=null){
			cronogramaMedicion.setFecPlanificada(new java.sql.Date(fecPlanificada.getTime()));
		}else {
			cronogramaMedicion.setFecPlanificada(null);
		}
		
		try {
			service.update(cronogramaMedicion);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		
		return CronogramaxCampana();
	}
	
	public String eliminar(){
		String croMedicionId=Utilidad.getParametro("cronogramamedicionId");
		CronogramaMedicionService service=new CronogramaMedicionService();
		
		//cronogramaMedicion=service.ReadById(new Long(croMedicionId));
		//cronogramaMedicion.setEstado("0");
		//service.update(cronogramaMedicion);
		//System.out.println("Se elimino una medicion con exito.");
		//return "/zonasegura/cronogramaMedicionListar?faces-redirect=true";
		
		try {
			cronogramaMedicion=service.ReadById(new Long(croMedicionId));
			cronogramaMedicion.setEstado("0");
			service.update(cronogramaMedicion);
		}catch (HibernateException e) {
			
			
			System.out.println(e.getMessage()+":"+e.getCause());
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			
		} catch (Exception e) {
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/cronogramaMedicionListar?faces-redirect=true";
	}
	
	public void cargarCronogramaMedicionActual(){
		String croMedicionId=Utilidad.getParametro("cronogramamedicionId");
		CronogramaMedicionService service=new CronogramaMedicionService();
		//cronogramaMedicion=service.ReadById(new Long(croMedicionId));
		
		try {
			
			cronogramaMedicion=service.ReadById(new Long(croMedicionId));
			
			if(cronogramaMedicion.getFecPlanificada()!=null) {
				fecPlanificada = new java.util.Date( cronogramaMedicion.getFecPlanificada().getTime());				
			}else {
				fecPlanificada = null;
			}
			
			
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

			throw new HibernateException("No se puede cargar Cronograma.", e);
			
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
	}


	public String getMensaje() {
		return mensaje;
	}


	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}	
	
	public String getNisFilter() {
		return nisFilter;
	}

	public void setNisFilter(String nisFilter) {
		this.nisFilter = nisFilter;
	}

	public java.util.Date getFecPlaniFilter() {
		return fecPlaniFilter;
	}


	public void setFecPlaniFilter(java.util.Date fecPlaniFilter) {
		this.fecPlaniFilter = fecPlaniFilter;
	}

	public Filter<?> getFilterFecPlani() {
        return new Filter<CronogramaMedicion>() {
            public boolean accept(CronogramaMedicion cronogramaMedicion) {
                java.util.Date fechaIngresada =  getFecPlaniFilter();
             
                if (fechaIngresada == null) {
                	
                	return true;
                }else {
                	
                	Calendar calendar1 = Calendar.getInstance();
                	Calendar calendar2 = Calendar.getInstance();
                	
                	calendar1.setTime(fechaIngresada);
                	
                	if(cronogramaMedicion.getFecPlanificada() == null){
                		return false;
                	}
                	
                	calendar2.setTime(cronogramaMedicion.getFecPlanificada()); 
                	
                	if(calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR) && calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH) &&
                			calendar1.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH)){
                		
                		return true;
                		
                	}else{
                		return false;
                	}
                    
                }
            }
        };
	}
	
	
}
