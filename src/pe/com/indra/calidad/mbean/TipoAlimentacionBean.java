package pe.com.indra.calidad.mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

import pe.com.indra.calidad.entity.TipoAlimentacion;
import pe.com.indra.calidad.service.TipoAlimentacionService;

@ManagedBean(name="tipoAlimentacionBean")
@SessionScoped
public class TipoAlimentacionBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TipoAlimentacionService service=null;
	
	private List<TipoAlimentacion> alimentaciones=null;

	@PostConstruct
	public void init(){
		service=new TipoAlimentacionService();
		alimentaciones=service.getAllRows();
	}
	
	public List<TipoAlimentacion> getAlimentaciones() {
		alimentaciones=(List<TipoAlimentacion>) service.getAllRows();
		return alimentaciones;
	}

	public void setAlimentaciones(List<TipoAlimentacion> alimentaciones) {
		this.alimentaciones = alimentaciones;
	}
	
}
