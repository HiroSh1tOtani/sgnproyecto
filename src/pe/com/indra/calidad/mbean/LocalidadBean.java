package pe.com.indra.calidad.mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.entity.Localidad;
import pe.com.indra.calidad.service.LocalidadService;
import pe.com.indra.calidad.util.Utilidad;

@ManagedBean(name="localidadBean")
@SessionScoped
public class LocalidadBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<Localidad> localidades;
	private Localidad localidad;
	
	private String codLocalidadFilter;
	private String nombreFilter;
	
	private String mensaje;
	
	@PostConstruct 
	public void init(){
		System.out.println("se ejecuto PostConstruct "+new java.util.Date());
		localidad=new Localidad();
		localidad.setEstado("1");
		/*
		String compensacionUnitariaId=Utilidad.getParametro("compensacionUnitariaId");
		if(compensacionUnitariaId==null){
		  compensacionUnitaria=new CompensacionUnitaria();
		  compensacionUnitaria.setEstado("1");
		}else{
			CompensacionUnitariaService service=new CompensacionUnitariaService();
			compensacionUnitaria=service.ReadById(new Long(compensacionUnitariaId));
		}
		*/
	}
	
	public List<Localidad> getLocalidades() {
		LocalidadService service=new LocalidadService();
		localidades= service.getAllRows1();
		return localidades;
	}

	public void setLocalidades(List<Localidad> localidades) {
		this.localidades = localidades;
	}

	public Localidad getLocalidad() {
		
		return localidad;
	}

	public void setLocalidad(Localidad localidad) {
		this.localidad = localidad;
	}
	
			
	public String prepararCrear(){
		//Esto funciona siempre que el bean tenga ambito de sesion 
		setLocalidad(new Localidad());
		getLocalidad().setEstado("1");
		return "/zonasegura/localidadCrear?faces-redirect=true";
	}
	
	public String prepararEditar(){
		//TODO: Por implementar carga de la instancia
		//System.out.println("compensacionUnitariaId:"+Utilidad.getParametro("compensacionUnitariaId"));
		//cargarLocalidadActual();
		//return "/zonasegura/localidadEditar?faces-redirect=true";
		
		try {
			cargarLocalidadActual();
			return "/zonasegura/localidadEditar?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/localidadListar?faces-redirect=true";
		}
	}

	public String prepararVer(){
		//TODO: Por implementar carga de la instancia
		//System.out.println("LocalidadId:"+Utilidad.getParametro("compensacionUnitariaId"));
		//cargarLocalidadActual();
		//return "/zonasegura/localidadVer?faces-redirect=true";
		
		try {
			cargarLocalidadActual();
			return "/zonasegura/localidadVer?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/localidadListar?faces-redirect=true";
		}
	}
	
	public String salvar(){
		//LocalidadService service=new LocalidadService();
		//service.create(localidad);
		//System.out.println("Se salvo una localidad con exito.");
		//return "/zonasegura/localidadListar?faces-redirect=true";
		
		LocalidadService service=new LocalidadService();
		
		try {
			service.create(localidad);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/localidadListar?faces-redirect=true";
	}
	
	public String actualizar(){
		//LocalidadService service=new LocalidadService();
		//service.update(localidad);
		//System.out.println("Se actualizo una localidad con exito.");
		//return "/zonasegura/localidadListar?faces-redirect=true";
		
		LocalidadService service=new LocalidadService();
		
		try {
			service.update(localidad);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/localidadListar?faces-redirect=true";
	}
	
	public String eliminar(){
		String localidadId=Utilidad.getParametro("localidadId");
		LocalidadService service=new LocalidadService();
		//localidad=service.ReadById(new Long(localidadId));
		//localidad.setEstado("0");
		//service.delete(localidad.getLocalidadId());
		//System.out.println("Se elimino una localidad con exito.");
		//return "/zonasegura/localidadListar?faces-redirect=true";
		
		try {
			localidad=service.ReadById(new Long(localidadId));
			localidad.setEstado("0");
			//service.delete(localidad.getLocalidadId());
			service.update(localidad);
		} catch (HibernateException e) {
			
			
			System.out.println(e.getMessage()+":"+e.getCause());
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			
		} catch (Exception e) {
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/localidadListar?faces-redirect=true";
	}
	
	public void cargarLocalidadActual(){
		String localidadId=Utilidad.getParametro("localidadId");
		LocalidadService service=new LocalidadService();
		//localidad=service.ReadById(new Long(localidadId));
		
		try {
			localidad=service.ReadById(new Long(localidadId));
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

			throw new HibernateException("No se puede cargar Localidad.", e);
			
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getCodLocalidadFilter() {
		return codLocalidadFilter;
	}

	public void setCodLocalidadFilter(String codLocalidadFilter) {
		this.codLocalidadFilter = codLocalidadFilter;
	}

	public String getNombreFilter() {
		return nombreFilter;
	}

	public void setNombreFilter(String nombreFilter) {
		this.nombreFilter = nombreFilter;
	}
}
