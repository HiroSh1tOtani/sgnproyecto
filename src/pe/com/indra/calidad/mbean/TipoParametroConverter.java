package pe.com.indra.calidad.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.indra.calidad.entity.TipoParametro;
import pe.com.indra.calidad.service.TipoParametroService;

@FacesConverter(value="tipoParametroConverter",forClass=TipoParametroConverter.class)

public class TipoParametroConverter implements Converter{

	@Override
	public TipoParametro getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long tipParametroId=new Long(cadena);
		TipoParametroService service=new TipoParametroService();
		TipoParametro tipoParametro=new TipoParametro();
		tipoParametro=service.ReadById(tipParametroId);
		System.out.println(tipoParametro.getCodTipParametro()+":"+tipoParametro.getDescripcion());
		return tipoParametro;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			TipoParametro tipoParametro=(TipoParametro) objeto;
			//System.out.println("objeto:"+new Long(tipoParametro.getTipParametroId()).toString());
			return new Long(tipoParametro.getTipParametroId()).toString();
		}
		return null;
	}


}
