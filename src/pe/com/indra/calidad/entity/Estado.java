
package pe.com.indra.calidad.entity;

import java.io.Serializable;

/**
 *
 * @author Juan
 */
public class Estado implements Serializable{
    private String estado;
    private String descripcion;

    public Estado() {
    }

    public Estado(String estado, String descripcion) {
        this.estado = estado;
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
