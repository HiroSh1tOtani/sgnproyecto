
package pe.com.indra.calidad.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import pe.com.calidad.alumbradopublico.entity.ProgramaAp;

/**
 *
 * @author Juan
 */
@Entity
@Table(name="CAL_TIPO_PUNTO")
@SequenceGenerator(name = "TipPuntoIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "TipoPuntoSeq")
public class TipoPunto implements Serializable{
    private long tipPuntoId;
    private String codTipPunto;
    private String descripcion;
    private String estado;
    
    private Set<Medicion> mediciones;
    private Set<ProgramaAp> programasAp;

    public TipoPunto(String codTipPunto, String descripcion, String estado) {
        
        this.codTipPunto = codTipPunto;
        this.descripcion = descripcion;
        this.estado = estado;
        
        this.mediciones = new HashSet<Medicion>();
        
    }
    
    public TipoPunto() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TipPuntoIdGenerator")
    @Column(name = "TIP_PUNTO_ID",nullable=false,unique=true)
    public long getTipPuntoId() {
        return tipPuntoId;
    }

    public void setTipPuntoId(long tipPuntoId) {
        this.tipPuntoId = tipPuntoId;
    }

    @Column(name="COD_TIP_PUNTO",nullable=false, unique=true)
    public String getCodTipPunto() {
        return codTipPunto;
    }

    public void setCodTipPunto(String codTipPunto) {
        this.codTipPunto = codTipPunto;
    }
    
    @Column(name="DESCRIPCION",nullable=true, unique=true)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE} ,mappedBy = "tipoPunto")
    public Set<Medicion> getMediciones() {
        return mediciones;
    }

    public void setMediciones(Set<Medicion> mediciones) {
        this.mediciones = mediciones;
    }
    
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE} ,mappedBy = "tipoPunto")
    public Set<ProgramaAp> getProgramasAp() {
		return programasAp;
	}

	public void setProgramasAp(Set<ProgramaAp> programasAp) {
		this.programasAp = programasAp;
	}

    
    public String toString(){
		return new Long(this.getTipPuntoId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getTipPuntoId()==((TipoPunto)obj).getTipPuntoId()){
			return true;
		}else{
			return false;
		}
	}
    
}
