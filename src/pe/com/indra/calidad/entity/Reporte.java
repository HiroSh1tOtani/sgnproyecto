
package pe.com.indra.calidad.entity;

import java.io.Serializable;
import java.sql.Clob;

import javax.persistence.*;
/**
* Parametro de calidad.
*
* @version 1.0 16 Jul 2013
* @author Juan Flores
*/

@Entity
@Table(name="CAL_REPORTE")
@SequenceGenerator (name = "ReporteIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "ReporteSeq")
public class Reporte implements Serializable{

	private static final long serialVersionUID = 7649676133799068370L;
	private long reporteId;
	private String codReporte;
    private String ambito;
    private String nombre;
    private String archivo;
    private String frecuencia;
    private String anho;
    private String periodo;
    private boolean  seleccion;
    private Clob consultaSql;
    private String estado;
    private TipoParametro tipoParametro;

    public Reporte(String codReporte, String ambito, String nombre, String archivo, String frecuencia, String anho, String periodo, boolean  seleccion, String estado, TipoParametro tipoParametro) {
        this.codReporte = codReporte;
    	this.ambito = ambito;
        this.nombre = nombre;
        this.archivo = archivo;
        this.frecuencia = frecuencia;
        this.anho = anho;
        this.periodo = periodo;
        this.seleccion = seleccion;
        this.tipoParametro = tipoParametro;
        this.estado = estado;      
      
    }
 
    public Reporte() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ReporteIdGenerator")
    @Column(name = "REPORTE_ID",nullable=false,unique=true)
    public long getReporteId() {
        return reporteId;
    }

    public void setReporteId(long reporteId) {
        this.reporteId = reporteId;
    }

    @Column(name="COD_REPORTE",nullable=true, unique=false)
    public String getCodReporte() {
		return codReporte;
	}

	public void setCodReporte(String codReporte) {
		this.codReporte = codReporte;
	}

	@Column(name="AMBITO",nullable=true, unique=false)
    public String getAmbito() {
        return ambito;
    }

    public void setAmbito(String ambito) {
        this.ambito = ambito;
    }
    
    @Column(name="NOMBRE",nullable=true, unique=false)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name="ARCHIVO",nullable=true, unique=false)
    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    @Column(name="FRECUENCIA",nullable=true, unique=false)
    public String getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(String frecuencia) {
        this.frecuencia = frecuencia;
    }

    @Column(name="ANHO",nullable=true, unique=false)
    public String getAnho() {
        return anho;
    }

    public void setAnho(String anho) {
        this.anho = anho;
    }

    @Column(name="PERIODO",nullable=true, unique=false)
    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }
    
    @Column(name="SELECCION",nullable=true, unique=false)
    public boolean getSeleccion() {
        return seleccion;
    }

    public void setSeleccion(boolean seleccion) {
        this.seleccion = seleccion;
    } 

    @Column(name="CONSULTA_SQL",nullable=true, unique=false) //, columnDefinition = "Text"
    public Clob getConsultaSql() {
		return consultaSql;
	}

	public void setConsultaSql(Clob consultaSql) {
		this.consultaSql = consultaSql;
	}

	@Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
 
    @ManyToOne
    @JoinColumn(name="PK_TIP_PARAMETRO_ID", nullable=false)
    public TipoParametro getTipoParametro() {
        return tipoParametro;
    }
    
    public void setTipoParametro (TipoParametro tipoParametro) {
        this.tipoParametro = tipoParametro;
    }
    
}
