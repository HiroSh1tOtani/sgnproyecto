
package pe.com.indra.calidad.entity;


import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * *Empresa.
 * 
 * @author Juan
 */

@Entity
@Table(name="EMPRESA")
public class Empresa implements Serializable{
    private String empresa;
    private String nombre;
    private String perSupervision;
    private BigDecimal numAlimentadores;
    private BigDecimal mtMetrado;
    private String datum;
    private String Zona;
    private String nomResponsable;
    private String emailResponsable;
    private BigDecimal maxdememp;
    private BigDecimal maxdemempap;
    private String ano;
   
    public Empresa(String empresa, String nombre, String perSupervision, BigDecimal numAlimentadores, BigDecimal mtMetrado, String datum, String Zona, String nomResponsable, String emailResponsable, BigDecimal maxdememp, BigDecimal maxdemempap,String ano) {
        this.empresa = empresa;
        this.nombre = nombre;
        this.perSupervision = perSupervision;
        this.numAlimentadores = numAlimentadores;
        this.mtMetrado = mtMetrado;
        this.datum =datum;
        this.Zona = Zona;
        this.nomResponsable = nomResponsable;
        this.emailResponsable = emailResponsable;
        this.maxdememp = maxdememp;
        this.maxdemempap = maxdemempap;
        this.ano = ano;
    }

     public Empresa() {
    }
     
    @Id
    @Column(name = "EMPRESA",nullable=false,unique=true) 
    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    @Column(name="NOMBRE",nullable=false)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name="PER_SUPERVISION",nullable=false)
	public String getPerSupervision() {
		return perSupervision;
	}

	public void setPerSupervision(String perSupervision) {
		this.perSupervision = perSupervision;
	}

	@Column(name="NUM_ALIMENTADORES",nullable=false)
	public BigDecimal getNumAlimentadores() {
		return numAlimentadores;
	}

	public void setNumAlimentadores(BigDecimal numAlimentadores) {
		this.numAlimentadores = numAlimentadores;
	}

	@Column(name="MT_METRADO",nullable=false)
	public BigDecimal getMtMetrado() {
		return mtMetrado;
	}

	public void setMtMetrado(BigDecimal mtMetrado) {
		this.mtMetrado = mtMetrado;
	}

	@Column(name="DATUM",nullable=false)
	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}

	@Column(name="ZONA",nullable=false)
	public String getZona() {
		return Zona;
	}

	public void setZona(String zona) {
		Zona = zona;
	}

	@Column(name="NOM_RESPONSABLE",nullable=false)
	public String getNomResponsable() {
		return nomResponsable;
	}

	public void setNomResponsable(String nomResponsable) {
		this.nomResponsable = nomResponsable;
	}

	@Column(name="EMAIL_RESPONSABLE",nullable=false)
	public String getEmailResponsable() {
		return emailResponsable;
	}

	public void setEmailResponsable(String emailResponsable) {
		this.emailResponsable = emailResponsable;
	}

	@Column(name="MAXDEMEMP",nullable=false)
	public BigDecimal getMaxdememp() {
		return maxdememp;
	}

	public void setMaxdememp(BigDecimal maxdememp) {
		this.maxdememp = maxdememp;
	}

	@Column(name="MAXDEMEMPAP",nullable=false)
	public BigDecimal getMaxdemempap() {
		return maxdemempap;
	}

	public void setMaxdemempap(BigDecimal maxdemempap) {
		this.maxdemempap = maxdemempap;
	}

	@Column(name="ANO",nullable=false)
	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}
    
    
}
