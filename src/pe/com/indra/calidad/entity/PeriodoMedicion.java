package pe.com.indra.calidad.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import pe.com.calidad.alumbradopublico.entity.ProgramaAp;
import pe.com.calidad.proc078.entity.HistoricoDeficienciaAp;
import pe.com.calidad.proc228.entity.Deficiencia;
import pe.com.calidad.suministro.entity.Indicador074;
import pe.com.calidad.suministro.entity.Interrupcion;

/**
 *
 * @author Juan
 */

@Entity
@Table(name="CAL_PERIODO_MEDICION")
@SequenceGenerator(name = "PerMedicionIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "PeriodoMedicionSeq")
public class PeriodoMedicion implements Serializable {
	

	private static final long serialVersionUID = -706415361238753942L;
	
	private long perMedicionId;
    private String ano;
    private String periodo;
    private String descripcion;
    private String estado;
    private String semestre;
    private BigDecimal tipCambio;
    
    private Set<CampanaMedicion> campanaMediciones;
    private Set<Interrupcion> interrupciones;
    private Set<Indicador074> indicadores;
    private Set<HistoricoDeficienciaAp> historicoDeficienciasAP;
    private Set<Deficiencia> deficiencias;
    private Set<ProgramaAp> programasAp;
    
    
	public PeriodoMedicion(String ano, String periodo, String descripcion, String estado,String semestre,BigDecimal tipCambio) {
        
        this.ano = ano;
        this.periodo = periodo;
        this.descripcion = descripcion;
        this.estado = estado;
        this.setSemestre(semestre);
        this.campanaMediciones = new HashSet<CampanaMedicion>();
        this.tipCambio = tipCambio;
    }

    public PeriodoMedicion() {
    }
    

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE},mappedBy = "periodoMedicion")
  	public Set<ProgramaAp> getProgramasAp() {
		return programasAp;
	}

	public void setProgramasAp(Set<ProgramaAp> programasAp) {
		this.programasAp = programasAp;
	}

    
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE},mappedBy = "periodoMedicion")
    public Set<Deficiencia> getDeficiencias() {
		return deficiencias;
	}

	public void setDeficiencias(Set<Deficiencia> deficiencias) {
		this.deficiencias = deficiencias;
	}

	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE},mappedBy = "periodoMedicion")
    public Set<HistoricoDeficienciaAp> getHistoricoDeficienciasAP() {
		return historicoDeficienciasAP;
	}

	public void setHistoricoDeficienciasAP(
			Set<HistoricoDeficienciaAp> historicoDeficienciasAP) {
		this.historicoDeficienciasAP = historicoDeficienciasAP;
	}

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PerMedicionIdGenerator")
    @Column(name = "PER_MEDICION_ID",nullable=false,unique=true)
    public long getPerMedicionId() {
        return perMedicionId;
    }

    public void setPerMedicionId(long perMedicionId) {
        this.perMedicionId = perMedicionId;
    }

    @Column(name="ANO",nullable=false)
    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }
    
    @Column(name="PERIODO",nullable=false)
    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    @Column(name="DESCRIPCION",nullable=true, unique=true)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    @Column(name="SEMESTRE",nullable=false)
    public String getSemestre() {
		return semestre;
	}

	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}
	
	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE},mappedBy = "periodoMedicion")
	public Set<Interrupcion> getInterrupciones() {
		return interrupciones;
	}

	public void setInterrupciones(Set<Interrupcion> interrupciones) {
		this.interrupciones = interrupciones;
	}

	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE},mappedBy = "periodoMedicion")
	public Set<Indicador074> getIndicadores() {
		return indicadores;
	}

	public void setIndicadores(Set<Indicador074> indicadores) {
		this.indicadores = indicadores;
	}

	
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE},mappedBy = "periodoMedicion")
    public Set<CampanaMedicion> getCampanaMediciones() {
        return campanaMediciones;
    }

    public void setCampanaMediciones(Set<CampanaMedicion> campanaMediciones) {
        this.campanaMediciones = campanaMediciones;
    }

    @Column(name="TIP_CAMBIO",nullable=false)
    public BigDecimal getTipCambio() {
		return tipCambio;
	}

    public void setTipCambio(BigDecimal tipCambio) {
		this.tipCambio = tipCambio;
	}

	public String toString(){
		return new Long(this.getPerMedicionId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getPerMedicionId()==((PeriodoMedicion)obj).getPerMedicionId()){
			return true;
		}else{
			return false;
		}
	}

	
    
}