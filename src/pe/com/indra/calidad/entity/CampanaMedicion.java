
package pe.com.indra.calidad.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
/**
 *
 * @author Juan
 */
@Entity
@Table(name="CAL_CAMPANA_MEDICION")
@SequenceGenerator (name = "CamMedicionIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "CampanaMedicionSeq")
public class CampanaMedicion implements Serializable {
    private long camMedicionId;
    private TipoParametro tipoParametro;
    private PeriodoMedicion periodoMedicion;
    private String descripcion;
    private String estado;
    private Set<Medicion> mediciones;
    private long processid;

    public CampanaMedicion(PeriodoMedicion periodoMedicion, TipoParametro tipoParametro, String descripcion, String estado, long processid) {
        
        this.periodoMedicion = periodoMedicion;
        this.tipoParametro = tipoParametro;
        this.descripcion = descripcion;
        this.estado = estado;
        this.mediciones = new HashSet<Medicion>();
        this.processid = processid;
    }

    public CampanaMedicion() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CamMedicionIdGenerator")
    @Column(name = "CAM_MEDICION_ID",nullable=false,unique=true)
    public long getCamMedicionId() {
        return camMedicionId;
    }

    public void setCamMedicionId(long camMedicionId) {
        this.camMedicionId = camMedicionId;
    }

    @ManyToOne
    @JoinColumn(name="PK_TIP_PARAMETRO_ID", nullable=false)
    public TipoParametro getTipoParametro() {
        return tipoParametro;
    }

    public void setTipoParametro(TipoParametro tipoParametro) {
        this.tipoParametro = tipoParametro;
    }

    @ManyToOne
    @JoinColumn(name="PK_PER_MEDICION_ID", nullable=false)
    public PeriodoMedicion getPeriodoMedicion() {
        return periodoMedicion;
    }

    public void setPeriodoMedicion(PeriodoMedicion periodoMedicion) {
        this.periodoMedicion = periodoMedicion;
    }

    @Column(name="DESCRIPCION",nullable=true,unique=true)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    @Column(name="PROCESSID",nullable=false)
    public long getProcessid() {
        return processid;
    }

    public void setProcessid(long processid) {
        this.processid = processid;
    }
    
    
    @OneToMany(cascade = {CascadeType.ALL} , mappedBy = "campanaMedicion")
    public Set<Medicion> getMediciones() {
        return mediciones;
    }

    public void setMediciones(Set<Medicion> mediciones) {
        this.mediciones = mediciones;
    }
    
	public String toString(){
		return new Long(this.getCamMedicionId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getCamMedicionId()==((CampanaMedicion)obj).getCamMedicionId()){
			return true;
		}else{
			return false;
		}
	}
}
