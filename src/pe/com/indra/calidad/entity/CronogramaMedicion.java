
package pe.com.indra.calidad.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Juan
 */
@Entity
@Table(name="CAL_CRONOGRAMA_MEDICION")
@SequenceGenerator (name = "CroMedicionIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "CronogramaMedicionSeq")
public class CronogramaMedicion implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -3300850480364683035L;
	private long croMedicionId;
    private Date fecPlanificada;
    private Cuadrilla cuadrilla;
    private Medicion medicion;
    private String estado;


    public CronogramaMedicion(Date fecPlanificada, Cuadrilla cuadrilla, Medicion medicion, String estado) {
        this.fecPlanificada = fecPlanificada;
        this.cuadrilla = cuadrilla;
        this.medicion = medicion;
        this.estado = estado;
    }

    public CronogramaMedicion() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CroMedicionIdGenerator")
    @Column(name = "CRO_MEDICION_ID",nullable=false,unique=true)
    public long getCroMedicionId() {
        return croMedicionId;
    }

    public void setCroMedicionId(long croMedicionId) {
        this.croMedicionId = croMedicionId;
    }

    @Column(name="FEC_PLANIFICADA",nullable=false)
    public Date getFecPlanificada() {
        return fecPlanificada;
    }

    public void setFecPlanificada(Date fecPlanificada) {
        this.fecPlanificada = fecPlanificada;
    }

    @ManyToOne
    @JoinColumn(name="PK_CUADRILLA_ID",nullable=true)
    public Cuadrilla getCuadrilla() {
        return cuadrilla;
    }

    public void setCuadrilla(Cuadrilla cuadrilla) {
        this.cuadrilla = cuadrilla;
    }

    @ManyToOne
    @JoinColumn(name="PK_MEDICION_ID",nullable=false)
    public Medicion getMedicion() {
        return medicion;
    }

    public void setMedicion(Medicion medicion) {
        this.medicion = medicion;
    }

    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    public String toString(){
		return new Long(this.getCroMedicionId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getCroMedicionId()==((CronogramaMedicion)obj).getCroMedicionId()){
			return true;
		}else{
			return false;
		}
	}
    
}
