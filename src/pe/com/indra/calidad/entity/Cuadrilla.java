
package pe.com.indra.calidad.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
/**
 *
 * @author Juan
 */
@Entity
@Table(name="CAL_CUADRILLA")
@SequenceGenerator(name = "CuadrillaIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "CuadrillaSeq")
public class Cuadrilla implements Serializable{
    private long cuadrillaId;
    private String codCuadrilla;
    private String descripcion;
    private String estado;
    
    private Set<CronogramaMedicion> cronogramaMedicion;

    
    public Cuadrilla(String codCuadrilla, String descripcion, String estado) {
        
        this.codCuadrilla = codCuadrilla;
        this.descripcion = descripcion;
        this.estado = estado;
        
        this.cronogramaMedicion = new HashSet<CronogramaMedicion>();
    }

    public Cuadrilla() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CuadrillaIdGenerator")
    @Column(name = "CUADRILLA_ID",nullable=false,unique=true)
    public long getCuadrillaId() {
        return cuadrillaId;
    }

    public void setCuadrillaId(long cuadrillaId) {
        this.cuadrillaId = cuadrillaId;
    }

    @Column(name="COD_CUADRILLA",nullable=false, unique=true)
    public String getCodCuadrilla() {
        return codCuadrilla;
    }

    public void setCodCuadrilla(String codCuadrilla) {
        this.codCuadrilla = codCuadrilla;
    }

    @Column(name="DESCRIPCION",nullable=true, unique=true)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE},mappedBy = "cuadrilla")
    public Set<CronogramaMedicion> getCronogramaMedicion() {
        return cronogramaMedicion;
    }

    public void setCronogramaMedicion(Set<CronogramaMedicion> cronogramaMedicion) {
        this.cronogramaMedicion = cronogramaMedicion;
    }
    
    public String toString(){
		return new Long(this.getCuadrillaId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getCuadrillaId()==((Cuadrilla)obj).getCuadrillaId()){
			return true;
		}else{
			return false;
		}
	}
}
