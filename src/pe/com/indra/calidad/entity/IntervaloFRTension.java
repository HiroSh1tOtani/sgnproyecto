
package pe.com.indra.calidad.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;

/**
 *
 * @author Juan
 */
@Entity
@Table(name="CAL_INTERVALO_FR_TENSION")
@SequenceGenerator (name = "IntFRTensionIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "IntervaloFRTensionSeq")
public class IntervaloFRTension implements Serializable{
    private long intFRTensionId;
    private long rango1;
    private long rango2;
    private long rango3;
    private long rango4;
    private long rango5;
    private long rango6;
    private long rango7;
    private long rango8;
    private long rango9;
    private long rango10;
    private long rango11;
    private long rango12;
    private BigDecimal vso1;
    private BigDecimal vso2;
    private BigDecimal vsu1;
    private BigDecimal vsu2;
    private Medicion medicion;
    private String estado;

   
    public IntervaloFRTension(long rango1, long rango2, long rango3, long rango4, long rango5, long rango6, long rango7, long rango8, long rango9, long rango10, long rango11, long rango12, BigDecimal vso1 , BigDecimal vso2 , BigDecimal vsu1 , BigDecimal vsu2 , Medicion medicion, String estado) {
        
        this.rango1 = rango1;
        this.rango2 = rango2;
        this.rango3 = rango3;
        this.rango4 = rango4;
        this.rango5 = rango5;
        this.rango6 = rango6;
        this.rango7 = rango7;
        this.rango8 = rango8;
        this.rango9 = rango9;
        this.rango10 = rango10;
        this.rango11 = rango11;
        this.rango12 = rango12;
        this.vso1 = vso1;
        this.vso2 = vso2;
        this.vsu1 = vsu1;
        this.vsu2 = vsu2;
        this.medicion = medicion;
        this.estado = estado;
    }

    public IntervaloFRTension() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IntFRTensionIdGenerator")
    @Column(name = "INT_FR_TENSION_ID",nullable=false,unique=true)
    public long getIntFRTensionId() {
        return intFRTensionId;
    }

    public void setIntFRTensionId(long intFRTensionId) {
        this.intFRTensionId = intFRTensionId;
    }

    @Column(name="RANGO_1",nullable=true)
    public long getRango1() {
        return rango1;
    }

    public void setRango1(long rango1) {
        this.rango1 = rango1;
    }

    @Column(name="RANGO_2",nullable=true)
    public long getRango2() {
        return rango2;
    }

    public void setRango2(long rango2) {
        this.rango2 = rango2;
    }

    @Column(name="RANGO_3",nullable=true)
    public long getRango3() {
        return rango3;
    }

    public void setRango3(long rango3) {
        this.rango3 = rango3;
    }

    @Column(name="RANGO_4",nullable=true)
    public long getRango4() {
        return rango4;
    }

    public void setRango4(long rango4) {
        this.rango4 = rango4;
    }

    @Column(name="RANGO_5",nullable=true)
    public long getRango5() {
        return rango5;
    }

    public void setRango5(long rango5) {
        this.rango5 = rango5;
    }

    @Column(name="RANGO_6",nullable=true)
    public long getRango6() {
        return rango6;
    }

    public void setRango6(long rango6) {
        this.rango6 = rango6;
    }

    @Column(name="RANGO_7",nullable=true)
    public long getRango7() {
        return rango7;
    }

    public void setRango7(long rango7) {
        this.rango7 = rango7;
    }

    @Column(name="RANGO_8",nullable=true)
    public long getRango8() {
        return rango8;
    }

    public void setRango8(long rango8) {
        this.rango8 = rango8;
    }

    @Column(name="RANGO_9",nullable=true)
    public long getRango9() {
        return rango9;
    }

    public void setRango9(long rango9) {
        this.rango9 = rango9;
    }

    @Column(name="RANGO_10",nullable=true)
    public long getRango10() {
        return rango10;
    }

    public void setRango10(long rango10) {
        this.rango10 = rango10;
    }

    @Column(name="RANGO_11",nullable=true)
    public long getRango11() {
        return rango11;
    }

    public void setRango11(long rango11) {
        this.rango11 = rango11;
    }

    @Column(name="RANGO_12",nullable=true)
    public long getRango12() {
        return rango12;
    }

    public void setRango12(long rango12) {
        this.rango12 = rango12;
    }
    
    @Column(name="VSO1",nullable=true)
	public BigDecimal getVso1() {
	    return vso1;
	}
	
	public void setVso1(BigDecimal vso1) {
	    this.vso1 = vso1;
	}

   @Column(name="VSO2",nullable=true)
	public BigDecimal getVso2() {
	    return vso2;
	}
	
	public void setVso2(BigDecimal vso2) {
	    this.vso1 = vso2;
	}
	
    @Column(name="VSU1",nullable=true)
	public BigDecimal getVsu1() {
	    return vsu1;
	}
	
	public void setVsu1(BigDecimal vsu1) {
	    this.vsu1 = vsu1;
	}
   
   @Column(name="VSU2",nullable=true)
	public BigDecimal getVsu2() {
	    return vsu2;
	}
	
	public void setVsu2(BigDecimal vsu2) {
	    this.vsu1 = vsu2;
	}

    @ManyToOne
    @JoinColumn(name="PK_MEDICION_ID", nullable=false)
    public Medicion getMedicion() {
        return medicion;
    }

    public void setMedicion(Medicion medicion) {
        this.medicion = medicion;
    }

    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    
}
