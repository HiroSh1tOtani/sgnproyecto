package pe.com.indra.calidad.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;

@Entity
@Table(name="CAL_COMPENSACION_UNITARIA")
@SequenceGenerator (name = "ComUnitariaIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "CompensacionUnitariaSeq")
public class CompensacionUnitaria implements Serializable {
	private long comUnitariaId;
	private String codComUnitaria;
	private String descripcion;
	private BigDecimal valor;
	private TipoParametro tipoParametro;
	private String estado;
	
	public CompensacionUnitaria(long comUnitariaId, String codComUnitaria,
			String descripcion, BigDecimal valor, TipoParametro tipoParametro,
			String estado) {

		this.comUnitariaId = comUnitariaId;
		this.codComUnitaria = codComUnitaria;
		this.descripcion = descripcion;
		this.valor = valor;
		this.tipoParametro = tipoParametro;
		this.estado = estado;
	}

	public CompensacionUnitaria() {
		
	}

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ComUnitariaIdGenerator")
    @Column(name = "COM_UNITARIA_ID",nullable=false,unique=true)	
	public long getComUnitariaId() {
		return comUnitariaId;
	}

	public void setComUnitariaId(long comUnitariaId) {
		this.comUnitariaId = comUnitariaId;
	}

	@Column(name="COD_COM_UNITARIA",nullable=false, unique=false)
	public String getCodComUnitaria() {
		return codComUnitaria;
	}

	public void setCodComUnitaria(String codComUnitaria) {
		this.codComUnitaria = codComUnitaria;
	}

	@Column(name="DESCRIPCION",nullable=false, unique=false)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name="VALOR",nullable=false, unique=false)
	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	@ManyToOne
    @JoinColumn(name="PK_TIP_PARAMETRO_ID", nullable=false)
	public TipoParametro getTipoParametro() {
		return tipoParametro;
	}

	public void setTipoParametro(TipoParametro tipoParametro) {
		this.tipoParametro = tipoParametro;
	}

	@Column(name="ESTADO",nullable=false)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
}
