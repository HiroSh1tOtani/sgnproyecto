
package pe.com.indra.calidad.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

/**
 *
 * @author Juan
 */
@Entity
@Table(name="CAL_MEDICION")
@SequenceGenerator(name = "MedicionIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "MedicionSeq")
public class Medicion implements Serializable {
    
	private static final long serialVersionUID = -6441881639255168218L;
	private long medicionId;
    private long secuencia;
    private String numSumMedicion;
    private String numSumReemplazado;
    private String numIdentificador;
    private String ubiSumMedicion;
    private long tenEntregada;
    private String opcTarifaria;
    private String nomUsuario;
    private String descripcion;
    private String tipServicio;
    private String tipSuministro;
    private String codLinea;
    private String codSet;
    private String codAlimentador;
    private String codSed;
    private String telUsuario;
    private String nomArchivo;
    private BigDecimal facCorTension;
    private BigDecimal facCorCorriente;
    private Date fecInstalacion;
    private Timestamp fecRetiro;
    private String preFlicker;
    private String preArmonicas;
    private String observaciones;
    
    private CampanaMedicion campanaMedicion;
    private TipoMedicion tipoMedicion;
    private TipoPunto tipoPunto;
    private Localidad localidad;
    private TipoAlimentacion tipoAlimentacion;
    private TipoTrabajo tipoTrabajo;
    private EquipoRegistrador equipoRegistrador;
    private EstadoMedicion estadoMedicion;
    private ParametroMedido parametroMedido;
    
    private EstadoMedicion perEstMedicion;
    
    private String estado;
    private String dirUsuario;
    
    private String compensable;
    private String salidabt;
    private String perMensaje;
    private String parametroPer; 
    private String tensionSed;
    private String direccionSed;
    private String preFlickerPer; 
    private String preArmonicasPer; 
    private String tipoEnergia;
    private String tipoMedicionEnergia; 

	private Set<CronogramaMedicion> cronogramaMedicion;
    private Set<FactorCompensacion> factorCompensacion;
    private Set<IntervaloFRTension> intervaloFRTension;
    private Set<CompensacionTension> compensacionTension;
    private Set<IntervaloTension> intervaloTension;
    private Set<IntervaloPerturbacion> intervaloPerturbacion;
    
    private String numIdentificadorPer;
    
	public Medicion(long secuencia, String numSumMedicion, String numSumReemplazado,String numIdentificador,String ubiSumMedicion, long tenEntregada, String opcTarifaria, String nomUsuario, String descripcion, String tipServicio, String tipSuministro, String codLinea, String codSet, String codAlimentador, String codSed, String telUsuario, String nomArchivo, BigDecimal facCorTension, BigDecimal facCorCorriente, Date fecInstalacion, Timestamp fecRetiro, String preFlicker, String preArmonicas , String observaciones, CampanaMedicion campanaMedicion, TipoMedicion tipoMedicion, TipoPunto tipoPunto, Localidad localidad, TipoAlimentacion tipoAlimentacion, TipoTrabajo tipoTrabajo, EquipoRegistrador equipoRegistrador, EstadoMedicion estadoMedicion, ParametroMedido parametroMedido, String estado, String dirUsuario, String compensable, String salidabt ,String perMensaje ,String parametroPer, String numIdentificadorPer, String tensionSed, String direccionSed, String preFlickerPer, String preArmonicasPer, String tipoEnergia, String tipoMedicionEnergia ) {
        
        this.secuencia = secuencia;
        this.numSumMedicion = numSumMedicion;
        this.numSumReemplazado = numSumReemplazado;
        this.numIdentificador = numIdentificador;
        this.ubiSumMedicion = ubiSumMedicion;
        this.tenEntregada = tenEntregada;
        this.opcTarifaria = opcTarifaria;
        this.nomUsuario = nomUsuario;
        this.descripcion = descripcion;
        this.tipServicio = tipServicio;
        this.tipSuministro = tipSuministro;
        this.codLinea = codLinea;
        this.codSet = codSet;
        this.codAlimentador = codAlimentador;
        this.codSed = codSed;
        this.telUsuario = telUsuario;
        this.nomArchivo = nomArchivo;
        this.facCorTension = facCorTension;
        this.facCorCorriente = facCorCorriente;
        this.fecInstalacion = fecInstalacion;
        this.fecRetiro = fecRetiro;
        this.preFlicker = preFlicker;
        this.preArmonicas = preArmonicas;
        this.observaciones = observaciones;
        this.campanaMedicion = campanaMedicion;
        this.tipoMedicion = tipoMedicion;
        this.tipoPunto = tipoPunto;
        this.localidad = localidad;
        this.tipoAlimentacion = tipoAlimentacion;
        this.tipoTrabajo = tipoTrabajo;
        this.equipoRegistrador = equipoRegistrador;
        this.estadoMedicion = estadoMedicion;
        this.parametroMedido = parametroMedido;
        this.estado = estado;
        this.dirUsuario = dirUsuario;
        
        this.compensable = compensable;
        this.salidabt = salidabt;
        this.perMensaje = perMensaje;
        this.parametroPer = parametroPer;
        this.numIdentificadorPer = numIdentificadorPer;
        this.tensionSed = tensionSed;
        this.direccionSed = direccionSed; 
        this.preFlickerPer = preFlickerPer;
        this.preArmonicasPer = preArmonicasPer;
        this.tipoEnergia = tipoEnergia;
        this.tipoMedicionEnergia = tipoMedicionEnergia; 
        
        this.cronogramaMedicion = new HashSet<CronogramaMedicion>();
        this.factorCompensacion = new HashSet<FactorCompensacion>();
        this.intervaloFRTension = new HashSet<IntervaloFRTension>();
        this.compensacionTension = new HashSet<CompensacionTension>();
        this.intervaloTension = new HashSet<IntervaloTension>();
        this.intervaloPerturbacion = new HashSet<IntervaloPerturbacion>();
    }

     public Medicion() {
    }
     
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MedicionIdGenerator")
    @Column(name = "MEDICION_ID",nullable=false,unique=true) 
    public long getMedicionId() {
        return medicionId;
    }

    public void setMedicionId(long medicionId) {
        this.medicionId = medicionId;
    }

    @Column(name="SECUENCIA",nullable=true)
    public long getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(long secuencia) {
        this.secuencia = secuencia;
    }

    @Column(name="NUM_SUM_MEDICION",nullable=false)
    public String getNumSumMedicion() {
        return numSumMedicion;
    }

    public void setNumSumMedicion(String numSumMedicion) {
        this.numSumMedicion = numSumMedicion;
    }

    @Column(name="NUM_SUM_REEMPLAZADO",nullable=true)
    public String getNumSumReemplazado() {
		return numSumReemplazado;
	}

	public void setNumSumReemplazado(String numSumReemplazado) {
		this.numSumReemplazado = numSumReemplazado;
	}
		
    @Column(name="NUM_IDENTIFICADOR",nullable=true)
    public String getNumIdentificador() {
		return numIdentificador;
	}

	public void setNumIdentificador(String numIdentificador) {
		this.numIdentificador = numIdentificador;
	}
    
	@Column(name="NUM_IDENTIFICADOR_PER",nullable=true)  
	public String getNumIdentificadorPer() {
		return numIdentificadorPer;
	}

	public void setNumIdentificadorPer(String numIdentificadorPer) {
		this.numIdentificadorPer = numIdentificadorPer;
	}

	
	@Column(name="UBI_SUM_MEDICION",nullable=true)
    public String getUbiSumMedicion() {
		return ubiSumMedicion;
	}

	public void setUbiSumMedicion(String ubiSumMedicion) {
		this.ubiSumMedicion = ubiSumMedicion;
	}
	
	@Column(name="TEN_ENTREGADA",nullable=true)
    public long getTenEntregada() {
        return tenEntregada;
    }

    public void setTenEntregada(long tenEntregada) {
        this.tenEntregada = tenEntregada;
    }

    @Column(name="OPC_TARIFARIA",nullable=true)
    public String getOpcTarifaria() {
        return opcTarifaria;
    }

    public void setOpcTarifaria(String opcTarifaria) {
        this.opcTarifaria = opcTarifaria;
    }

    @Column(name="NOM_USUARIO",nullable=true)
    public String getNomUsuario() {
        return nomUsuario;
    }

    public void setNomUsuario(String nomUsuario) {
        this.nomUsuario = nomUsuario;
    }

    @Column(name="DESCRIPCION",nullable=true,unique=true)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name="TIP_SERVICIO",nullable=true)
    public String getTipServicio() {
        return tipServicio;
    }

    public void setTipServicio(String tipServicio) {
        this.tipServicio = tipServicio;
    }

    @Column(name="TIP_SUMINISTRO",nullable=true)
    public String getTipSuministro() {
        return tipSuministro;
    }

    public void setTipSuministro(String tipSuministro) {
        this.tipSuministro = tipSuministro;
    }

    @Column(name="COD_LINEA",nullable=true)
    public String getCodLinea() {
        return codLinea;
    }

    public void setCodLinea(String codLinea) {
        this.codLinea = codLinea;
    }

    @Column(name="CODSET",nullable=true)
    public String getCodSet() {
        return codSet;
    }

    public void setCodSet(String codSet) {
        this.codSet = codSet;
    }

    @Column(name="CODALIMENTADOR",nullable=true)
    public String getCodAlimentador() {
        return codAlimentador;
    }

    public void setCodAlimentador(String codAlimentador) {
        this.codAlimentador = codAlimentador;
    }

    @Column(name="CODSED",nullable=true)
    public String getCodSed() {
        return codSed;
    }

    public void setCodSed(String codSed) {
        this.codSed = codSed;
    }

    @Column(name="TEL_USUARIO",nullable=true)
    public String getTelUsuario() {
        return telUsuario;
    }

    public void setTelUsuario(String telUsuario) {
        this.telUsuario = telUsuario;
    }

    @Column(name="NOM_ARCHIVO",nullable=true)
    public String getNomArchivo() {
        return nomArchivo;
    }

    public void setNomArchivo(String nomArchivo) {
        this.nomArchivo = nomArchivo;
    }

    @Column(name="FAC_COR_TENSION",nullable=true)
    public BigDecimal getFacCorTension() {
        return facCorTension;
    }

    public void setFacCorTension(BigDecimal facCorTension) {
        this.facCorTension = facCorTension;
    }

    @Column(name="FAC_COR_CORRIENTE",nullable=true)
    public BigDecimal getFacCorCorriente() {
        return facCorCorriente;
    }

    public void setFacCorCorriente(BigDecimal facCorCorriente) {
        this.facCorCorriente = facCorCorriente;
    }

    @Column(name="FEC_INSTALACION",nullable=true)
    public Date getFecInstalacion() {
        return fecInstalacion;
    }

    public void setFecInstalacion(Date fecInstalacion) {
        this.fecInstalacion = fecInstalacion;
    }

    @Column(name="FEC_RETIRO",nullable=true)
    public Timestamp getFecRetiro() {
        return fecRetiro;
    }

    public void setFecRetiro(Timestamp fecRetiro) {
        this.fecRetiro = fecRetiro;
    }

	@Column(name="PRE_FLICKER",nullable=true)
	public String getPreFlicker() {
	    return preFlicker;
	}
	
	public void setPreFlicker(String preFlicker) {
	    this.preFlicker = preFlicker;
	}
	
	
	@Column(name="PRE_ARMONICAS",nullable=true)
	public String getPreArmonicas() {
	    return preArmonicas;
	}
	
	public void setPreArmonicas(String preArmonicas) {
	    this.preArmonicas = preArmonicas;
	}
	
    
    @Column(name="OBSERVACIONES",nullable=true)
    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @ManyToOne
    @JoinColumn(name="PK_CAM_MEDICION_ID", nullable=false)
    public CampanaMedicion getCampanaMedicion() {
        return campanaMedicion;
    }

    public void setCampanaMedicion(CampanaMedicion campanaMedicion) {
        this.campanaMedicion = campanaMedicion;
    }

    @ManyToOne
    @JoinColumn(name="PK_TIP_MEDICION_ID", nullable=true)
    public TipoMedicion getTipoMedicion() {
        return tipoMedicion;
    }

    public void setTipoMedicion(TipoMedicion tipoMedicion) {
        this.tipoMedicion = tipoMedicion;
    }

    @ManyToOne
    @JoinColumn(name="PK_TIP_PUNTO_ID", nullable=true)
    public TipoPunto getTipoPunto() {
        return tipoPunto;
    }

    public void setTipoPunto(TipoPunto tipoPunto) {
        this.tipoPunto = tipoPunto;
    }

    @ManyToOne
    @JoinColumn(name="PK_LOCALIDAD_ID", nullable=true)
    public Localidad getLocalidad() {
        return localidad;
    }

    public void setLocalidad(Localidad localidad) {
        this.localidad = localidad;
    }

    @ManyToOne
    @JoinColumn(name="PK_TIP_ALIMENTACION_ID", nullable=true)
    public TipoAlimentacion getTipoAlimentacion() {
        return tipoAlimentacion;
    }

    public void setTipoAlimentacion(TipoAlimentacion tipoAlimentacion) {
        this.tipoAlimentacion = tipoAlimentacion;
    }

    @ManyToOne
    @JoinColumn(name="PK_TIP_TRABAJO_ID", nullable=true)
    public TipoTrabajo getTipoTrabajo() {
        return tipoTrabajo;
    }

    public void setTipoTrabajo(TipoTrabajo tipoTrabajo) {
        this.tipoTrabajo = tipoTrabajo;
    }

    @ManyToOne
    @JoinColumn(name="PK_EQU_REGISTRADOR_ID", nullable=true)
    public EquipoRegistrador getEquipoRegistrador() {
        return equipoRegistrador;
    }

    public void setEquipoRegistrador(EquipoRegistrador equipoRegistrador) {
        this.equipoRegistrador = equipoRegistrador;
    }

    @ManyToOne
    @JoinColumn(name="PK_EST_MEDICION_ID", nullable=true)
    public EstadoMedicion getEstadoMedicion() {
        return estadoMedicion;
    }

    public void setEstadoMedicion(EstadoMedicion estadoMedicion) {
        this.estadoMedicion = estadoMedicion;
    }

    @ManyToOne
    @JoinColumn(name="PK_PAR_MEDIDO_ID", nullable=true)
    public ParametroMedido getParametroMedido() {
        return parametroMedido;
    }

    public void setParametroMedido(ParametroMedido parametroMedido) {
        this.parametroMedido = parametroMedido;
    }
    
    @ManyToOne
    @JoinColumn(name="PK_PER_EST_MEDICION_ID", nullable=true)
    public EstadoMedicion getPerEstMedicion() {
		return perEstMedicion;
	}

	public void setPerEstMedicion(EstadoMedicion perEstMedicion) {
		this.perEstMedicion = perEstMedicion;
	}

    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name="DIR_USUARIO",nullable=true)
    public String getDirUsuario() {
        return dirUsuario;
    }

    public void setDirUsuario(String dirUsuario) {
        this.dirUsuario = dirUsuario;
    }
       
    @Column(name="COMPEMSABLE",nullable=true)
    public String getCompensable() {
        return compensable;
    }

    public void setCompensable(String compensable) {
        this.compensable = compensable;
    }
  
    @Column(name="SALIDABT",nullable=true)
    public String getSalidabt() {
        return salidabt;
    }

    public void setSalidabt(String salidabt) {
        this.salidabt = salidabt;
    }
    
    @Column(name="PER_MENSAJE",nullable=true)
	public String getPerMensaje() {
		return perMensaje;
	}

	public void setPerMensaje(String perMensaje) {
		this.perMensaje = perMensaje;
	}
	
	@Column(name="PARAMETRO_PER",nullable=true)
	public String getParametroPer() {
		return parametroPer;
	}

	public void setParametroPer(String parametroPer) {
		this.parametroPer = parametroPer;
	}

	@Column(name="TENSION_SED",nullable=true)
    public String getTensionSed() {
		return tensionSed;
	}

	public void setTensionSed(String tensionSed) {
		this.tensionSed = tensionSed;
	}

	@Column(name="DIRECCION_SED",nullable=true)
	public String getDireccionSed() {
		return direccionSed;
	}

	public void setDireccionSed(String direccionSed) {
		this.direccionSed = direccionSed;
	}
	
	@Column(name="PRE_FLICKER_PER",nullable=true)
 	public String getPreFlickerPer() {
		return preFlickerPer;
	}

	public void setPreFlickerPer(String preFlickerPer) {
		this.preFlickerPer = preFlickerPer;
	}

	@Column(name="PRE_ARMONICAS_PER",nullable=true)
	public String getPreArmonicasPer() {
		return preArmonicasPer;
	}

	public void setPreArmonicasPer(String preArmonicasPer) {
		this.preArmonicasPer = preArmonicasPer;
	}

	@Column(name="TIPO_ENERGIA",nullable=true)
	public String getTipoEnergia() {
		return tipoEnergia;
	}

	public void setTipoEnergia(String tipoEnergia) {
		this.tipoEnergia = tipoEnergia;
	}

	@Column(name="TIPO_MEDICION_ENERGIA",nullable=true)
	public String getTipoMedicionEnergia() {
		return tipoMedicionEnergia;
	}

	public void setTipoMedicionEnergia(String tipoMedicionEnergia) {
		this.tipoMedicionEnergia = tipoMedicionEnergia;
	}

	@OneToMany(cascade ={CascadeType.ALL}, mappedBy = "medicion")
    public Set<CronogramaMedicion> getCronogramaMedicion() {
        return cronogramaMedicion;
    }

    public void setCronogramaMedicion(Set<CronogramaMedicion> cronogramaMedicion) {
        this.cronogramaMedicion = cronogramaMedicion;
    }

  
    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "medicion")
    public Set<FactorCompensacion> getFactorCompensacion() {
        return factorCompensacion;
    }

    public void setFactorCompensacion(Set<FactorCompensacion> factorCompensacion) {
        this.factorCompensacion = factorCompensacion;
    }

  
    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "medicion")
    public Set<IntervaloFRTension> getIntervaloFRTension() {
        return intervaloFRTension;
    }

    public void setIntervaloFRTension(Set<IntervaloFRTension> intervaloFRTension) {
        this.intervaloFRTension = intervaloFRTension;
    }

    
    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "medicion")
    public Set<CompensacionTension> getCompensacionTension() {
        return compensacionTension;
    }

    public void setCompensacionTension(Set<CompensacionTension> compensacionTension) {
        this.compensacionTension = compensacionTension;
    }

    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "medicion")
    public Set<IntervaloTension> getIntervaloTension() {
        return intervaloTension;
    }

    public void setIntervaloTension(Set<IntervaloTension> intervaloTension) {
        this.intervaloTension = intervaloTension;
    }
    
    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "medicion")
    public Set<IntervaloPerturbacion> getIntervaloPerturbacion() {
		return intervaloPerturbacion;
	}

	public void setIntervaloPerturbacion(
			Set<IntervaloPerturbacion> intervaloPerturbacion) {
		this.intervaloPerturbacion = intervaloPerturbacion;
	}
    
	
	public String toString(){
		return new Long(this.getMedicionId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getMedicionId()==((Medicion)obj).getMedicionId()){
			return true;
		}else{
			return false;
		}
	}
}
