
package pe.com.indra.calidad.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
/**
 *
 * @author Juan
 */
@Entity
@Table(name="CAL_TIPO_ENERGIA")
@SequenceGenerator(name = "TipEnergiaIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "TipoEnergiaSeq")
public class TipoEnergia {
    private long tipEnergiaId;
    private String codTipEnergia;
    private String descripcion;
    private String estado;
    
    private Set<CompensacionTension> compensacionTensiones;

    public TipoEnergia(String codTipEnergia, String descripcion, String estado) {
        this.codTipEnergia = codTipEnergia;
        this.descripcion = descripcion;
        this.estado = estado;
        
        this.compensacionTensiones = new HashSet<CompensacionTension>();
    }

        public TipoEnergia() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TipEnergiaIdGenerator")
    @Column(name = "TIP_ENERGIA_ID",nullable=false,unique=true)
    public long getTipEnergiaId() {
        return tipEnergiaId;
    }

    public void setTipEnergiaId(long tipEnergiaId) {
        this.tipEnergiaId = tipEnergiaId;
    }

    @Column(name="COD_TIP_ENERGIA",nullable=false, unique=true)
    public String getCodTipEnergia() {
        return codTipEnergia;
    }

    public void setCodTipEnergia(String codTipEnergia) {
        this.codTipEnergia = codTipEnergia;
    }

    @Column(name="DESCRIPCION",nullable=true, unique=true)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

   
    @OneToMany(cascade ={CascadeType.PERSIST, CascadeType.MERGE} ,mappedBy = "tipoenergia")
    public Set<CompensacionTension> getCompensacionTensiones() {
        return compensacionTensiones;
    }

    public void setCompensacionTensiones(Set<CompensacionTension> compensacionTensiones) {
        this.compensacionTensiones = compensacionTensiones;
    }
    
    public String toString(){
		return new Long(this.getTipEnergiaId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getTipEnergiaId()==((TipoEnergia)obj).getTipEnergiaId()){
			return true;
		}else{
			return false;
		}
	}
    
}
