
package pe.com.indra.calidad.entity;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
* Tipo de parametro de calidad.
*
* @version 1.0 16 Jul 2013
* @author Juan Flores
*/
@Entity
@Table(name="CAL_TIPO_PARAMETRO")
@SequenceGenerator(name = "TipParametroIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "TipoParametroSeq")
public class TipoParametro implements Serializable{
    private long tipParametroId;
    private String codTipParametro;
    private String descripcion;
    private String estado;
    
    private Set<ParametroNorma> parametroNormas;
    private Set<CampanaMedicion> campanaMediciones;
    private Set<CompensacionUnitaria> compensacionUnitarias;
    private Set<Reporte> reportes;

    public TipoParametro(String codTipParametro, String descripcion, String estado) {
        this.codTipParametro = codTipParametro;
        this.descripcion = descripcion;
        this.estado = estado;
        this.parametroNormas = new HashSet<ParametroNorma>();
        this.campanaMediciones = new HashSet<CampanaMedicion>();
        this.compensacionUnitarias = new HashSet<CompensacionUnitaria>();
    }

    public TipoParametro() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TipParametroIdGenerator")
    @Column(name = "TIP_PARAMETRO_ID",nullable=false,unique=true)
    public long getTipParametroId() {
        return tipParametroId;
    }

    public void setTipParametroId(long tipParametroId) {
        this.tipParametroId = tipParametroId;
    }
    
    @Column(name="COD_TIP_PARAMETRO",nullable=false, unique=true)
    public String getCodTipParametro() {
        return codTipParametro;
    }

    public void setCodTipParametro(String codTipParametro) {
        this.codTipParametro = codTipParametro;
    }
    
    @Column(name="DESCRIPCION",nullable=true, unique=true)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

   
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE} ,mappedBy = "tipoParametro")
    public Set<ParametroNorma> getParametroNormas() {
        return parametroNormas;
    }

    public void setParametroNormas(Set<ParametroNorma> parametroNormas) {
        this.parametroNormas = parametroNormas;
    }
    
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE} ,mappedBy = "tipoParametro")
    public Set<Reporte> getReportes() {
		return reportes;
	}

	public void setReportes(Set<Reporte> reportes) {
		this.reportes = reportes;
	}

	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE} ,mappedBy = "tipoParametro")
    public Set<CampanaMedicion> getCampanaMediciones() {
        return campanaMediciones;
    }

    public void setCampanaMediciones(Set<CampanaMedicion> campanaMediciones) {
        this.campanaMediciones = campanaMediciones;
    }

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE} ,mappedBy = "tipoParametro")
	public Set<CompensacionUnitaria> getCompensacionUnitarias() {
		return compensacionUnitarias;
	}

	public void setCompensacionUnitarias(Set<CompensacionUnitaria> compensacionUnitarias) {
		this.compensacionUnitarias = compensacionUnitarias;
	}

	public String toString(){
		return new Long(this.getTipParametroId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getTipParametroId()==((TipoParametro)obj).getTipParametroId()){
			return true;
		}else{
			return false;
		}
	}
}
