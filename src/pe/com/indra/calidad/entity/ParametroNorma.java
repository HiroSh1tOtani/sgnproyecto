
package pe.com.indra.calidad.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;
/**
* Parametro de calidad.
*
* @version 1.0 16 Jul 2013
* @author Juan Flores
*/

@Entity
@Table(name="CAL_PARAMETRO_NORMA")
@SequenceGenerator (name = "ParNormaIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "ParametroNormaSeq")
public class ParametroNorma implements Serializable{
    private long parNormaId;
    private String secTipico;
    private String codParNorma;
    private BigDecimal parNorValor;
    private BigDecimal minIndParNorma;
    private BigDecimal maxIndParNorma;
    private String indParNorma;
    private TipoParametro tipoParametro;
    private String estado;
    private String nomParNorma;
    private String tipoParNorma;

    public ParametroNorma(String secTipico, String codParNorma, BigDecimal parNorValor, BigDecimal minIndParNorma, BigDecimal maxIndParNorma, String indParNorma, TipoParametro tipoParametro, String estado, String nomParNorma, String tipoParNorma) {
        this.secTipico = secTipico;
        this.codParNorma = codParNorma;
        this.parNorValor = parNorValor;
        this.minIndParNorma = minIndParNorma;
        this.maxIndParNorma = maxIndParNorma;
        this.indParNorma = indParNorma;
        this.tipoParametro = tipoParametro;
        this.estado = estado;
        this.nomParNorma = nomParNorma;
        this.tipoParNorma = tipoParNorma;
      
    }
 
    public ParametroNorma() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ParNormaIdGenerator")
    @Column(name = "PAR_NORMA_ID",nullable=false,unique=true)
    public long getParNormaId() {
        return parNormaId;
    }

    public void setParNormaId(long parNormaId) {
        this.parNormaId = parNormaId;
    }

    @Column(name="SEC_TIPICO",nullable=false, unique=false)
    public String getSecTipico() {
        return secTipico;
    }

    public void setSecTipico(String secTipico) {
        this.secTipico = secTipico;
    }
    
    @Column(name="COD_PAR_NORMA",nullable=false, unique=true)
    public String getCodParNorma() {
        return codParNorma;
    }

    public void setCodParNorma(String codParNorma) {
        this.codParNorma = codParNorma;
    }

    @Column(name="PAR_NOR_VALOR",nullable=false)
    public BigDecimal getParNorValor() {
        return parNorValor;
    }

    public void setParNorValor(BigDecimal parNorValor) {
        this.parNorValor = parNorValor;
    }

    @Column(name="MIN_IND_PAR_NORMA",nullable=false)
    public BigDecimal getMinIndParNorma() {
        return minIndParNorma;
    }

    public void setMinIndParNorma(BigDecimal minIndParNorma) {
        this.minIndParNorma = minIndParNorma;
    }

    @Column(name="MAX_IND_PAR_NORMA",nullable=false)
    public BigDecimal getMaxIndParNorma() {
        return maxIndParNorma;
    }

    public void setMaxIndParNorma(BigDecimal maxIndParNorma) {
        this.maxIndParNorma = maxIndParNorma;
    }

    @Column(name="IND_PAR_NORMA",nullable=false)
    public String getIndParNorma() {
        return indParNorma;
    }

    public void setIndParNorma(String indParNorma) {
        this.indParNorma = indParNorma;
    }

    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
 
    @ManyToOne
    @JoinColumn(name="PK_TIP_PARAMETRO_ID", nullable=false)
    public TipoParametro getTipoParametro() {
        return tipoParametro;
    }
    
    public void setTipoParametro (TipoParametro tipoParametro) {
        this.tipoParametro = tipoParametro;
    }

    @Column(name="NOM_PAR_NORMA",nullable=true)
	public String getNomParNorma() {
		return nomParNorma;
	}

	public void setNomParNorma(String nomParNorma) {
		this.nomParNorma = nomParNorma;
	}
	
	@Column(name="TIPO_PAR_NORMA",nullable=true)
	public String getTipoParNorma() {
		return tipoParNorma;
	}

	public void setTipoParNorma(String tipoParNorma) {
		this.tipoParNorma = tipoParNorma;
	}
    
}
