
package pe.com.indra.calidad.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
/**
 *
 * @author Juan
 */
@Entity
@Table(name="CAL_PARAMETRO_MEDIDO")
@SequenceGenerator(name = "ParMedidoIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "ParametroMedidoSeq")
public class ParametroMedido implements Serializable{
    private long parMedidoId;
    private String codParMedido;
    private String descripcion;
    private String estado;
    
    private Set<Medicion> mediciones;

    public ParametroMedido(String codParMedido, String descripcion, String estado) {
        
        this.codParMedido = codParMedido;
        this.descripcion = descripcion;
        this.estado = estado;
        
        this.mediciones = new HashSet<Medicion>();
    }

    public ParametroMedido() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ParMedidoIdGenerator")
    @Column(name = "PAR_MEDIDO_ID",nullable=false,unique=true)
    public long getParMedidoId() {
        return parMedidoId;
    }

    public void setParMedidoId(long parMedidoId) {
        this.parMedidoId = parMedidoId;
    }

    @Column(name="COD_PAR_MEDIDO",nullable=false, unique=true)
    public String getCodParMedido() {
        return codParMedido;
    }

    public void setCodParMedido(String codParMedido) {
        this.codParMedido = codParMedido;
    }

    @Column(name="DESCRIPCION",nullable=true, unique=true)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE} ,mappedBy = "parametroMedido")
    public Set<Medicion> getMediciones() {
        return mediciones;
    }

    public void setMediciones(Set<Medicion> mediciones) {
        this.mediciones = mediciones;
    }
    
    public String toString(){
		return new Long(this.getParMedidoId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getParMedidoId()==((ParametroMedido)obj).getParMedidoId()){
			return true;
		}else{
			return false;
		}
	}
    
}
