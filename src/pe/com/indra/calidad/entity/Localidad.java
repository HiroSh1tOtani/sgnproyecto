
package pe.com.indra.calidad.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import pe.com.calidad.alumbradopublico.entity.ProgramaAp;
import pe.com.calidad.alumbradopublico.entity.Via;
import pe.com.calidad.proc078.entity.HistoricoDeficienciaAp;
import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.entity.SuministroAfectado;

/**
 *
 * @author Juan
 */
@Entity
@Table(name="CAL_LOCALIDAD")
@SequenceGenerator(name = "LocalidadIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "LocalidadSeq")
public class Localidad implements Serializable{
    private long localidadId;
    private String codLocalidad;
    private String nombre;
    private String estado;
    private String secTipico;
    private String sisElectrico;
    private String codOsinerg;
    private BigDecimal maxDemanda;
    private String nombreOsinerg;
    
	private Set<Medicion> mediciones;
    private Set<Interrupcion> interrupciones;
    private Set<SuministroAfectado> suministrosAfectado;
    private Set<HistoricoDeficienciaAp> historicoDeficienciasAP;
    private Set<ProgramaAp> programasAp;
    private Set<Via> vias;

	public Localidad(String codLocalidad, String nombre, String estado, String secTipico, String sisElectrico, String codOsinerg,BigDecimal maxDemanda,String nombreOsinerg) {
        
        this.codLocalidad = codLocalidad;
        this.nombre = nombre;
        this.estado = estado;
        this.secTipico = secTipico;
        this.sisElectrico = sisElectrico;
        this.codOsinerg = codOsinerg;
        this.maxDemanda = maxDemanda;
        this.nombreOsinerg = nombreOsinerg;
        
        this.mediciones = new HashSet<Medicion>();
    }
    
    public Localidad() {
    }
    
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "localidad")
    public Set<Via> getVias() {
		return vias;
	}

	public void setVias(Set<Via> vias) {
		this.vias = vias;
	}
    
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "localidad")
    public Set<ProgramaAp> getProgramasAp() {
		return programasAp;
	}

	public void setProgramasAp(Set<ProgramaAp> programasAp) {
		this.programasAp = programasAp;
	}
    
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "localidad")
    public Set<HistoricoDeficienciaAp> getHistoricoDeficienciasAP() {
		return historicoDeficienciasAP;
	}

	public void setHistoricoDeficienciasAP(
			Set<HistoricoDeficienciaAp> historicoDeficienciasAP) {
		this.historicoDeficienciasAP = historicoDeficienciasAP;
	}

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LocalidadIdGenerator")
    @Column(name = "LOCALIDAD_ID",nullable=false,unique=true)
    public long getLocalidadId() {
        return localidadId;
    }

    public void setLocalidadId(long localidadId) {
        this.localidadId = localidadId;
    }

    @Column(name="COD_LOCALIDAD",nullable=false, unique=true)
    public String getCodLocalidad() {
        return codLocalidad;
    }

    public void setCodLocalidad(String codLocalidad) {
        this.codLocalidad = codLocalidad;
    }

    @Column(name="NOMBRE",nullable=true)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "localidad")
    public Set<Medicion> getMediciones() {
        return mediciones;
    }

    public void setMediciones(Set<Medicion> mediciones) {
        this.mediciones = mediciones;
    }
    
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "localidad")
    public Set<Interrupcion> getInterrupciones() {
		return interrupciones;
	}

	public void setInterrupciones(Set<Interrupcion> interrupciones) {
		this.interrupciones = interrupciones;
	}
	
	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "localidad")
	public Set<SuministroAfectado> getSuministrosAfectado() {
		return suministrosAfectado;
	}

	public void setSuministrosAfectado(Set<SuministroAfectado> suministrosAfectado) {
		this.suministrosAfectado = suministrosAfectado;
	}
	
	@Column(name="SEC_TIPICO",nullable=false)
	public String getSecTipico() {
		return secTipico;
	}

	public void setSecTipico(String secTipico) {
		this.secTipico = secTipico;
	}

	@Column(name="SIS_ELECTRICO",nullable=false)
	public String getSisElectrico() {
		return sisElectrico;
	}

	public void setSisElectrico(String sisElectrico) {
		this.sisElectrico = sisElectrico;
	}
	
	@Column(name="COD_OSINERG",nullable=false)
	public String getCodOsinerg() {
		return codOsinerg;
	}

	public void setCodOsinerg(String codOsinerg) {
		this.codOsinerg = codOsinerg;
	}
    
    public String toString(){
		return new Long(this.getLocalidadId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getLocalidadId()==((Localidad)obj).getLocalidadId()){
			return true;
		}else{
			return false;
		}
	}

	@Column(name="MAX_DEMANDA",nullable=true)
	public BigDecimal getMaxDemanda() {
		return maxDemanda;
	}

	public void setMaxDemanda(BigDecimal maxDemanda) {
		this.maxDemanda = maxDemanda;
	}

	@Column(name="NOMBRE_OSINERG",nullable=false)
	public String getNombreOsinerg() {
		return nombreOsinerg;
	}

	public void setNombreOsinerg(String nombreOsinerg) {
		this.nombreOsinerg = nombreOsinerg;
	}

	

	
}
