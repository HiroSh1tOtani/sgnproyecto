
package pe.com.indra.calidad.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
/**
 *
 * @author Juan
 */
@Entity
@Table(name="CAL_TIPO_TRABAJO")
@SequenceGenerator(name = "TipTrabajoIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "TipoTrabajoSeq")
public class TipoTrabajo implements Serializable{
    private long tipTrabajoId;
    private String codTipTrabajo;
    private String descripcion;
    private String estado;
    
    private Set<Medicion> mediciones;
    
    public TipoTrabajo(String codTipTrabajo, String descripcion, String estado) {
        
        this.codTipTrabajo = codTipTrabajo;
        this.descripcion = descripcion;
        this.estado = estado;
        
        this.mediciones = new HashSet<Medicion>();
    }

    public TipoTrabajo() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TipTrabajoIdGenerator")
    @Column(name = "TIP_TRABAJO_ID",nullable=false,unique=true)
    public long getTipTrabajoId() {
        return tipTrabajoId;
    }

    public void setTipTrabajoId(long tipTrabajoId) {
        this.tipTrabajoId = tipTrabajoId;
    }

    @Column(name="COD_TIP_TRABAJO",nullable=false, unique=true)
    public String getCodTipTrabajo() {
        return codTipTrabajo;
    }

    public void setCodTipTrabajo(String codTipTrabajo) {
        this.codTipTrabajo = codTipTrabajo;
    }

    @Column(name="DESCRIPCION",nullable=true, unique=true)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE} ,mappedBy = "tipoTrabajo")
    public Set<Medicion> getMediciones() {
        return mediciones;
    }

    public void setMediciones(Set<Medicion> mediciones) {
        this.mediciones = mediciones;
    }
    
	public String toString(){
		return new Long(this.getTipTrabajoId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getTipTrabajoId()==((TipoTrabajo)obj).getTipTrabajoId()){
			return true;
		}else{
			return false;
		}
	}

}
