
package pe.com.indra.calidad.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import pe.com.calidad.alumbradopublico.entity.ProgramaAp;
/**
 *
 * @author Juan
 */
@Entity
@Table(name="CAL_TIPO_MEDICION")
@SequenceGenerator(name = "TipMedicionIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "TipoMedicionSeq")
public class TipoMedicion implements Serializable {
    private long tipMedicionId;
    private String codTipMedicion;
    private String descripcion;
    private String estado;

    private Set<Medicion> mediciones;
    private Set<ProgramaAp> programasAp;
    
	public TipoMedicion(String codTipMedicion, String descripcion, String estado) {

        this.codTipMedicion = codTipMedicion;
        this.descripcion = descripcion;
        this.estado = estado;
        
        this.mediciones = new HashSet<Medicion>();
    }

        public TipoMedicion() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TipMedicionIdGenerator")
    @Column(name = "TIP_MEDICION_ID",nullable=false,unique=true)        
    public long getTipMedicionId() {
        return tipMedicionId;
    }

    public void setTipMedicionId(long tipMedicionId) {
        this.tipMedicionId = tipMedicionId;
    }

    @Column(name="COD_TIP_MEDICION",nullable=false, unique=true)
    public String getCodTipMedicion() {
        return codTipMedicion;
    }

    public void setCodTipMedicion(String codTipMedicion) {
        this.codTipMedicion = codTipMedicion;
    }

    @Column(name="DESCRIPCION",nullable=true, unique=true)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE} ,mappedBy = "tipoMedicion")
    public Set<Medicion> getMediciones() {
        return mediciones;
    }

    public void setMediciones(Set<Medicion> mediciones) {
        this.mediciones = mediciones;
    }    
    
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE} ,mappedBy = "tipoMedicion")
    public Set<ProgramaAp> getProgramasAp() {
		return programasAp;
	}

	public void setProgramasAp(Set<ProgramaAp> programasAp) {
		this.programasAp = programasAp;
	}

    
    public String toString(){
		return new Long(this.getTipMedicionId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getTipMedicionId()==((TipoMedicion)obj).getTipMedicionId()){
			return true;
		}else{
			return false;
		}
	}
}
