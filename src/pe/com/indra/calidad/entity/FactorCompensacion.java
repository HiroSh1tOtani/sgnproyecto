
package pe.com.indra.calidad.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Juan
 */
@Entity
@Table(name="CAL_FACTOR_COMPENSACION")
@SequenceGenerator (name = "FacCompensacionIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "FactorCompensacionSeq")
public class FactorCompensacion implements Serializable{
    private long facCompensacionId;
    private Medicion medicion;
    private String estado;


    public FactorCompensacion(Medicion medicion, String estado) {
        
        this.medicion = medicion;
        this.estado = estado;
    }

    
    public FactorCompensacion() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FacCompensacionIdGenerator")
    @Column(name = "FAC_COMPENSACION_ID",nullable=false,unique=true)
    public long getFacCompensacionId() {
        return facCompensacionId;
    }

    public void setFacCompensacionId(long facCompensacionId) {
        this.facCompensacionId = facCompensacionId;
    }

    @ManyToOne
    @JoinColumn(name="PK_MEDICION_ID", nullable=false)
    public Medicion getMedicion() {
        return medicion;
    }

    public void setMedicion(Medicion medicion) {
        this.medicion = medicion;
    }

    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    
}
