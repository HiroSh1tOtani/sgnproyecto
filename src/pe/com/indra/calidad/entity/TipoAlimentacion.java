
package pe.com.indra.calidad.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

/**
 *
 * @author Juan
 */
@Entity
@Table(name="CAL_TIPO_ALIMENTACION")
@SequenceGenerator(name = "TipAlimentacionIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "TipoAlimentacionSeq")
public class TipoAlimentacion implements Serializable{
    private long tipAlimentacionId;
    private String codTipAlimentacion;
    private String descripcion;
    private String estado;
    
    private Set<Medicion> mediciones;

    public TipoAlimentacion(String codTipAlimentacion, String descripcion, String estado) {
        
        this.codTipAlimentacion = codTipAlimentacion;
        this.descripcion = descripcion;
        this.estado = estado;
        
        this.mediciones = new HashSet<Medicion>();
             
    }
    
    public TipoAlimentacion() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TipAlimentacionIdGenerator")
    @Column(name = "TIP_ALIMENTACION_ID",nullable=false,unique=true)
    public long getTipAlimentacionId() {
        return tipAlimentacionId;
    }

    public void setTipAlimentacionId(long tipAlimentacionId) {
        this.tipAlimentacionId = tipAlimentacionId;
    }

    @Column(name="COD_TIP_ALIMENTACION",nullable=false, unique=true)
    public String getCodTipAlimentacion() {
        return codTipAlimentacion;
    }

    public void setCodTipAlimentacion(String codTipAlimentacion) {
        this.codTipAlimentacion = codTipAlimentacion;
    }

    @Column(name="DESCRIPCION",nullable=true, unique=true)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "tipoAlimentacion")
    public Set<Medicion> getMediciones() {
        return mediciones;
    }

    public void setMediciones(Set<Medicion> mediciones) {
        this.mediciones = mediciones;
    }
    
    public String toString(){
		return new Long(this.getTipAlimentacionId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getTipAlimentacionId()==((TipoAlimentacion)obj).getTipAlimentacionId()){
			return true;
		}else{
			return false;
		}
	}
}
