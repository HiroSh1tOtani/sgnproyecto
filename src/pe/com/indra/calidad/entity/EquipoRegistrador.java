
package pe.com.indra.calidad.entity;

import java.io.Serializable;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

/**
 *
 * @author Juan
 */
@Entity
@Table(name="CAL_EQUIPO_REGISTRADOR")
@SequenceGenerator(name = "EquRegistradorIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "EquipoRegistradorSeq")
public class EquipoRegistrador implements Serializable{
    private long equRegistradorId;
    private String codEquRegistrador;
    private String descripcion;
    private String marca;
    private long anoFabricacion;
    private String numSerie;
    private String tipMedicion;
    private Date fecAdquisicion;
    private Date fecCalibracion;
    private String estado;
    private String columnas;
    private String nombreColumnas;
    private int filaInicial;
    private String separadorColumnas;
    private String formatoFecha;
    private int numeroColumnasFecha;
    
    private Set<Medicion> mediciones;

    
    public EquipoRegistrador(String codEquRegistrador, String descripcion, String marca, long anoFabricacion, String numSerie, String tipMedicion , Date fecAdquisicion , Date fecCalibracion, String estado, String columnas, String nombreColumnas, long filaInicial, String separadorColumnas, String formatoFecha, long numeroColumnasFecha) {
        
        this.codEquRegistrador = codEquRegistrador;
        this.descripcion = descripcion;
        this.marca = marca;
        this.anoFabricacion = anoFabricacion;
        this.numSerie = numSerie;
        this.tipMedicion = tipMedicion;
        this.fecAdquisicion = fecAdquisicion;
        this.fecCalibracion = fecCalibracion;
        this.estado = estado;
        this.columnas = columnas;
        this.nombreColumnas = nombreColumnas;
        this.filaInicial = (int) filaInicial;
        this.separadorColumnas = separadorColumnas;
        this.formatoFecha = formatoFecha;
        this.numeroColumnasFecha = (int) numeroColumnasFecha;
        
        this.mediciones = new HashSet<Medicion>();
    }

    public EquipoRegistrador() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EquRegistradorIdGenerator")
    @Column(name = "EQU_REGISTRADOR_ID",nullable=false,unique=true)
    public long getEquRegistradorId() {
        return equRegistradorId;
    }

    public void setEquRegistradorId(long equRegistradorId) {
        this.equRegistradorId = equRegistradorId;
    }

    @Column(name="COD_EQU_REGISTRADOR",nullable=false, unique=true)
    public String getCodEquRegistrador() {
        return codEquRegistrador;
    }

    public void setCodEquRegistrador(String codEquRegistrador) {
        this.codEquRegistrador = codEquRegistrador;
    }

    @Column(name="DESCRIPCION",nullable=true, unique=true)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name="MARCA",nullable=false)
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Column(name="ANO_FABRICACION",nullable=false)
    public long getAnoFabricacion() {
        return anoFabricacion;
    }

    public void setAnoFabricacion(long anoFabricacion) {
        this.anoFabricacion = anoFabricacion;
    }

    @Column(name="NUM_SERIE",nullable=false, unique=true)
    public String getNumSerie() {
        return numSerie;
    }

    public void setNumSerie(String numSerie) {
        this.numSerie = numSerie;
    }

   
    @Column(name="TIP_MEDICION",nullable=false)
    public String getTipMedicion() {
        return tipMedicion;
    }

    public void setTipMedicion(String tipMedicion) {
        this.tipMedicion = tipMedicion;
    }
    
    
    @Column(name="FEC_ADQUISICION",nullable=false)
    public Date getFecAdquisicion() {
        return fecAdquisicion;
    }

    public void setFecAdquisicion(Date fecAdquisicion) {
        this.fecAdquisicion = fecAdquisicion;
    }
    
    
    @Column(name="FEC_CALIBRACION",nullable=true)
    public Date getFecCalibracion() {
        return fecCalibracion;
    }

    public void setFecCalibracion(Date fecCalibracion) {
        this.fecCalibracion = fecCalibracion;
    }

    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    @Column(name="COLUMNAS",nullable=false)
    public String getColumnas() {
        return columnas;
    }

    public void setColumnas(String columnas) {
        this.columnas = columnas;
    }
    
   
    @Column(name="NOMBRE_COLUMNAS",nullable=false)
    public String getNombreColumnas() {
        return nombreColumnas;
    }

    public void setNombreColumnas(String nombreColumnas) {
        this.nombreColumnas = nombreColumnas;
    }
  
   @Column(name="FILA_INICIAL",nullable=false)
   public int getFilaInicial() {
       return filaInicial;
   }

   public void setFilaInicial(int filaInicial) {
       this.filaInicial = filaInicial;
   }

  
   @Column(name="SEPARADOR_COLUMNAS",nullable=false)
   public String getSeparadorColumnas() {
       return separadorColumnas;
   }

   public void setSeparadorColumnas(String separadorColumnas) {
       this.separadorColumnas = separadorColumnas;
   }
  
  
   @Column(name="FORMATO_FECHA",nullable=false)
   public String getFormatoFecha() {
       return formatoFecha;
   }

   public void setFormatoFecha(String formatoFecha) {
       this.formatoFecha = formatoFecha;
   }
 
   @Column(name="NUMERO_COLUMNAS_FECHA",nullable=false)
   public int getNumeroColumnasFecha() {
       return numeroColumnasFecha;
   }

   public void setNumeroColumnasFecha(int numeroColumnasFecha) {
       this.numeroColumnasFecha = numeroColumnasFecha;
   }

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "equipoRegistrador")
    public Set<Medicion> getMediciones() {
        return mediciones;
    }

    public void setMediciones(Set<Medicion> mediciones) {
        this.mediciones = mediciones;
    }
    
    public String toString(){
		return new Long(this.getEquRegistradorId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getEquRegistradorId()==((EquipoRegistrador)obj).getEquRegistradorId()){
			return true;
		}else{
			return false;
		}
	}
}
