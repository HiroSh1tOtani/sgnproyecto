
package pe.com.indra.calidad.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

/**
 *
 * @author Juan
 */
@Entity
@Table(name="CAL_INTERVALO_PERTURBACION")
@SequenceGenerator(name = "IntPerturbacionIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "IntervaloPerturbacionSeq")
public class IntervaloPerturbacion implements Serializable {

	private static final long serialVersionUID = 1L;
	private long intPerturbacionId;
    private Timestamp intervalo;
    private String enPerMedicion;
    
    private BigDecimal tensionR1;
    private BigDecimal tensionR2;
    private BigDecimal tensionR3;
    private BigDecimal tensionR4;
    private BigDecimal tensionR5;
    private BigDecimal tensionR6;
    private BigDecimal tensionR7;
    private BigDecimal tensionR8;
    private BigDecimal tensionR9;
    private BigDecimal tensionR10;
    private BigDecimal tensionR11;
    private BigDecimal tensionR12;
    private BigDecimal tensionR13;
    private BigDecimal tensionR14;
    private BigDecimal tensionR15;
    private BigDecimal tensionR16;
    private BigDecimal tensionR17;
    private BigDecimal tensionR18;
    private BigDecimal tensionR19;
    private BigDecimal tensionR20;
    private BigDecimal tensionR21;
    private BigDecimal tensionR22;
    private BigDecimal tensionR23;
    private BigDecimal tensionR24;
    private BigDecimal tensionR25;
    private BigDecimal tensionR26;
    private BigDecimal tensionR27;
    private BigDecimal tensionR28;
    private BigDecimal tensionR29;
    private BigDecimal tensionR30;
    private BigDecimal tensionR31;
    private BigDecimal tensionR32;
    private BigDecimal tensionR33;
    private BigDecimal tensionR34;
    private BigDecimal tensionR35;
    private BigDecimal tensionR36;
    private BigDecimal tensionR37;
    private BigDecimal tensionR38;
    private BigDecimal tensionR39;
    private BigDecimal tensionR40;
    private BigDecimal tensionS1;
    private BigDecimal tensionS2;
    private BigDecimal tensionS3;
    private BigDecimal tensionS4;
    private BigDecimal tensionS5;
    private BigDecimal tensionS6;
    private BigDecimal tensionS7;
    private BigDecimal tensionS8;
    private BigDecimal tensionS9;
    private BigDecimal tensionS10;
    private BigDecimal tensionS11;
    private BigDecimal tensionS12;
    private BigDecimal tensionS13;
    private BigDecimal tensionS14;
    private BigDecimal tensionS15;
    private BigDecimal tensionS16;
    private BigDecimal tensionS17;
    private BigDecimal tensionS18;
    private BigDecimal tensionS19;
    private BigDecimal tensionS20;
    private BigDecimal tensionS21;
    private BigDecimal tensionS22;
    private BigDecimal tensionS23;
    private BigDecimal tensionS24;
    private BigDecimal tensionS25;
    private BigDecimal tensionS26;
    private BigDecimal tensionS27;
    private BigDecimal tensionS28;
    private BigDecimal tensionS29;
    private BigDecimal tensionS30;
    private BigDecimal tensionS31;
    private BigDecimal tensionS32;
    private BigDecimal tensionS33;
    private BigDecimal tensionS34;
    private BigDecimal tensionS35;
    private BigDecimal tensionS36;
    private BigDecimal tensionS37;
    private BigDecimal tensionS38;
    private BigDecimal tensionS39;
    private BigDecimal tensionS40;
    private BigDecimal tensionT1;
    private BigDecimal tensionT2;
    private BigDecimal tensionT3;
    private BigDecimal tensionT4;
    private BigDecimal tensionT5;
    private BigDecimal tensionT6;
    private BigDecimal tensionT7;
    private BigDecimal tensionT8;
    private BigDecimal tensionT9;
    private BigDecimal tensionT10;
    private BigDecimal tensionT11;
    private BigDecimal tensionT12;
    private BigDecimal tensionT13;
    private BigDecimal tensionT14;
    private BigDecimal tensionT15;
    private BigDecimal tensionT16;
    private BigDecimal tensionT17;
    private BigDecimal tensionT18;
    private BigDecimal tensionT19;
    private BigDecimal tensionT20;
    private BigDecimal tensionT21;
    private BigDecimal tensionT22;
    private BigDecimal tensionT23;
    private BigDecimal tensionT24;
    private BigDecimal tensionT25;
    private BigDecimal tensionT26;
    private BigDecimal tensionT27;
    private BigDecimal tensionT28;
    private BigDecimal tensionT29;
    private BigDecimal tensionT30;
    private BigDecimal tensionT31;
    private BigDecimal tensionT32;
    private BigDecimal tensionT33;
    private BigDecimal tensionT34;
    private BigDecimal tensionT35;
    private BigDecimal tensionT36;
    private BigDecimal tensionT37;
    private BigDecimal tensionT38;
    private BigDecimal tensionT39;
    private BigDecimal tensionT40;
    private BigDecimal tenIntervalo1;
    private BigDecimal tenIntervalo2;
    private BigDecimal tenIntervalo3;
    private BigDecimal tenIntervalo4;
    private BigDecimal tenIntervalo5;
    private BigDecimal tenIntervalo6;
    private BigDecimal tenIntervalo7;
    private BigDecimal tenIntervalo8;
    private BigDecimal tenIntervalo9;
    private BigDecimal tenIntervalo10;
    private BigDecimal tenIntervalo11;
    private BigDecimal tenIntervalo12;
    private BigDecimal tenIntervalo13;
    private BigDecimal tenIntervalo14;
    private BigDecimal tenIntervalo15;
    private BigDecimal tenIntervalo16;
    private BigDecimal tenIntervalo17;
    private BigDecimal tenIntervalo18;
    private BigDecimal tenIntervalo19;
    private BigDecimal tenIntervalo20;
    private BigDecimal tenIntervalo21;
    private BigDecimal tenIntervalo22;
    private BigDecimal tenIntervalo23;
    private BigDecimal tenIntervalo24;
    private BigDecimal tenIntervalo25;
    private BigDecimal tenIntervalo26;
    private BigDecimal tenIntervalo27;
    private BigDecimal tenIntervalo28;
    private BigDecimal tenIntervalo29;
    private BigDecimal tenIntervalo30;
    private BigDecimal tenIntervalo31;
    private BigDecimal tenIntervalo32;
    private BigDecimal tenIntervalo33;
    private BigDecimal tenIntervalo34;
    private BigDecimal tenIntervalo35;
    private BigDecimal tenIntervalo36;
    private BigDecimal tenIntervalo37;
    private BigDecimal tenIntervalo38;
    private BigDecimal tenIntervalo39;
    private BigDecimal tenIntervalo40;
    
    private String estTension1;
    private String estTension2;
    private String estTension3;
    private String estTension4;
    private String estTension5;
    private String estTension6;
    private String estTension7;
    private String estTension8;
    private String estTension9;
    private String estTension10;
    private String estTension11;
    private String estTension12;
    private String estTension13;
    private String estTension14;
    private String estTension15;
    private String estTension16;
    private String estTension17;
    private String estTension18;
    private String estTension19;
    private String estTension20;
    private String estTension21;
    private String estTension22;
    private String estTension23;
    private String estTension24;
    private String estTension25;
    private String estTension26;
    private String estTension27;
    private String estTension28;
    private String estTension29;
    private String estTension30;
    private String estTension31;
    private String estTension32;
    private String estTension33;
    private String estTension34;
    private String estTension35;
    private String estTension36;
    private String estTension37;
    private String estTension38;
    private String estTension39;
    private String estTension40;
    
    private BigDecimal eneIntervalo;
 	private BigDecimal thdR;
    private BigDecimal thdS;
    private BigDecimal thdT;
    private BigDecimal thdMax;
    private BigDecimal dpa;
    private BigDecimal comArm;
    private BigDecimal pstR;
    private BigDecimal pstS;
    private BigDecimal pstT;
    private BigDecimal pstMax;
    private BigDecimal dpf;
    private BigDecimal potencia;
    private BigDecimal comFlicker;

    private Medicion medicion;
    private EstadoIntervalo fliEstIntervalo;
    private EstadoIntervalo armEstIntervalo;
    private EstadoIntervalo armvEstIntervalo;
    private String estado;
    
    private BigDecimal energiaR;
    private BigDecimal energiaS;
    private BigDecimal energiaT;
    


	public IntervaloPerturbacion() {
    }
    
 
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IntPerturbacionIdGenerator")
    @Column(name = "INT_PERTURBACION_ID",nullable=false,unique=true)
    public long getIntPerturbacionId() {
		return intPerturbacionId;
	}

	public void setIntPerturbacionId(long intPerturbacionId) {
		this.intPerturbacionId = intPerturbacionId;
	}

	@Column(name="INTERVALO",nullable=false)
	public Timestamp getIntervalo() {
		return intervalo;
	}


	public void setIntervalo(Timestamp intervalo) {
		this.intervalo = intervalo;
	}


	@Column(name="EN_PER_MEDICION",nullable=true)
	public String getEnPerMedicion() {
		return enPerMedicion;
	}


	public void setEnPerMedicion(String enPerMedicion) {
		this.enPerMedicion = enPerMedicion;
	}


	@Column(name="TENSION_R1",nullable=true)
	public BigDecimal getTensionR1() {
		return tensionR1;
	}


	public void setTensionR1(BigDecimal tensionR1) {
		this.tensionR1 = tensionR1;
	}


	@Column(name="TENSION_R2",nullable=true)
	public BigDecimal getTensionR2() {
		return tensionR2;
	}


	public void setTensionR2(BigDecimal tensionR2) {
		this.tensionR2 = tensionR2;
	}


	@Column(name="TENSION_R3",nullable=true)
	public BigDecimal getTensionR3() {
		return tensionR3;
	}


	public void setTensionR3(BigDecimal tensionR3) {
		this.tensionR3 = tensionR3;
	}


	@Column(name="TENSION_R4",nullable=true)
	public BigDecimal getTensionR4() {
		return tensionR4;
	}


	public void setTensionR4(BigDecimal tensionR4) {
		this.tensionR4 = tensionR4;
	}

	@Column(name="TENSION_R5",nullable=true)
	public BigDecimal getTensionR5() {
		return tensionR5;
	}


	public void setTensionR5(BigDecimal tensionR5) {
		this.tensionR5 = tensionR5;
	}


	@Column(name="TENSION_R6",nullable=true)
	public BigDecimal getTensionR6() {
		return tensionR6;
	}


	public void setTensionR6(BigDecimal tensionR6) {
		this.tensionR6 = tensionR6;
	}


	@Column(name="TENSION_R7",nullable=true)
	public BigDecimal getTensionR7() {
		return tensionR7;
	}


	public void setTensionR7(BigDecimal tensionR7) {
		this.tensionR7 = tensionR7;
	}


	@Column(name="TENSION_R8",nullable=true)
	public BigDecimal getTensionR8() {
		return tensionR8;
	}


	public void setTensionR8(BigDecimal tensionR8) {
		this.tensionR8 = tensionR8;
	}


	@Column(name="TENSION_R9",nullable=true)
	public BigDecimal getTensionR9() {
		return tensionR9;
	}


	public void setTensionR9(BigDecimal tensionR9) {
		this.tensionR9 = tensionR9;
	}


	@Column(name="TENSION_R10",nullable=true)
	public BigDecimal getTensionR10() {
		return tensionR10;
	}


	public void setTensionR10(BigDecimal tensionR10) {
		this.tensionR10 = tensionR10;
	}


	@Column(name="TENSION_R11",nullable=true)
	public BigDecimal getTensionR11() {
		return tensionR11;
	}


	public void setTensionR11(BigDecimal tensionR11) {
		this.tensionR11 = tensionR11;
	}


	@Column(name="TENSION_R12",nullable=true)
	public BigDecimal getTensionR12() {
		return tensionR12;
	}


	public void setTensionR12(BigDecimal tensionR12) {
		this.tensionR12 = tensionR12;
	}


	@Column(name="TENSION_R13",nullable=true)
	public BigDecimal getTensionR13() {
		return tensionR13;
	}


	public void setTensionR13(BigDecimal tensionR13) {
		this.tensionR13 = tensionR13;
	}


	@Column(name="TENSION_R14",nullable=true)
	public BigDecimal getTensionR14() {
		return tensionR14;
	}


	public void setTensionR14(BigDecimal tensionR14) {
		this.tensionR14 = tensionR14;
	}


	@Column(name="TENSION_R15",nullable=true)
	public BigDecimal getTensionR15() {
		return tensionR15;
	}


	public void setTensionR15(BigDecimal tensionR15) {
		this.tensionR15 = tensionR15;
	}


	@Column(name="TENSION_R16",nullable=true)
	public BigDecimal getTensionR16() {
		return tensionR16;
	}


	public void setTensionR16(BigDecimal tensionR16) {
		this.tensionR16 = tensionR16;
	}


	@Column(name="TENSION_R17",nullable=true)
	public BigDecimal getTensionR17() {
		return tensionR17;
	}


	public void setTensionR17(BigDecimal tensionR17) {
		this.tensionR17 = tensionR17;
	}


	@Column(name="TENSION_R18",nullable=true)
	public BigDecimal getTensionR18() {
		return tensionR18;
	}


	public void setTensionR18(BigDecimal tensionR18) {
		this.tensionR18 = tensionR18;
	}


	@Column(name="TENSION_R19",nullable=true)
	public BigDecimal getTensionR19() {
		return tensionR19;
	}


	public void setTensionR19(BigDecimal tensionR19) {
		this.tensionR19 = tensionR19;
	}


	@Column(name="TENSION_R20",nullable=true)
	public BigDecimal getTensionR20() {
		return tensionR20;
	}


	public void setTensionR20(BigDecimal tensionR20) {
		this.tensionR20 = tensionR20;
	}


	@Column(name="TENSION_R21",nullable=true)
	public BigDecimal getTensionR21() {
		return tensionR21;
	}


	public void setTensionR21(BigDecimal tensionR21) {
		this.tensionR21 = tensionR21;
	}


	@Column(name="TENSION_R22",nullable=true)
	public BigDecimal getTensionR22() {
		return tensionR22;
	}


	public void setTensionR22(BigDecimal tensionR22) {
		this.tensionR22 = tensionR22;
	}


	@Column(name="TENSION_R23",nullable=true)
	public BigDecimal getTensionR23() {
		return tensionR23;
	}


	public void setTensionR23(BigDecimal tensionR23) {
		this.tensionR23 = tensionR23;
	}


	@Column(name="TENSION_R24",nullable=true)
	public BigDecimal getTensionR24() {
		return tensionR24;
	}


	public void setTensionR24(BigDecimal tensionR24) {
		this.tensionR24 = tensionR24;
	}


	@Column(name="TENSION_R25",nullable=true)
	public BigDecimal getTensionR25() {
		return tensionR25;
	}


	public void setTensionR25(BigDecimal tensionR25) {
		this.tensionR25 = tensionR25;
	}


	@Column(name="TENSION_R26",nullable=true)
	public BigDecimal getTensionR26() {
		return tensionR26;
	}


	public void setTensionR26(BigDecimal tensionR26) {
		this.tensionR26 = tensionR26;
	}


	@Column(name="TENSION_R27",nullable=true)
	public BigDecimal getTensionR27() {
		return tensionR27;
	}


	public void setTensionR27(BigDecimal tensionR27) {
		this.tensionR27 = tensionR27;
	}


	@Column(name="TENSION_R28",nullable=true)
	public BigDecimal getTensionR28() {
		return tensionR28;
	}


	public void setTensionR28(BigDecimal tensionR28) {
		this.tensionR28 = tensionR28;
	}

	@Column(name="TENSION_R29",nullable=true)
	public BigDecimal getTensionR29() {
		return tensionR29;
	}


	public void setTensionR29(BigDecimal tensionR29) {
		this.tensionR29 = tensionR29;
	}


	@Column(name="TENSION_R30",nullable=true)
	public BigDecimal getTensionR30() {
		return tensionR30;
	}


	public void setTensionR30(BigDecimal tensionR30) {
		this.tensionR30 = tensionR30;
	}


	@Column(name="TENSION_R31",nullable=true)
	public BigDecimal getTensionR31() {
		return tensionR31;
	}


	public void setTensionR31(BigDecimal tensionR31) {
		this.tensionR31 = tensionR31;
	}


	@Column(name="TENSION_R32",nullable=true)
	public BigDecimal getTensionR32() {
		return tensionR32;
	}


	public void setTensionR32(BigDecimal tensionR32) {
		this.tensionR32 = tensionR32;
	}


	@Column(name="TENSION_R33",nullable=true)
	public BigDecimal getTensionR33() {
		return tensionR33;
	}


	public void setTensionR33(BigDecimal tensionR33) {
		this.tensionR33 = tensionR33;
	}


	@Column(name="TENSION_R34",nullable=true)
	public BigDecimal getTensionR34() {
		return tensionR34;
	}


	public void setTensionR34(BigDecimal tensionR34) {
		this.tensionR34 = tensionR34;
	}


	@Column(name="TENSION_R35",nullable=true)
	public BigDecimal getTensionR35() {
		return tensionR35;
	}


	public void setTensionR35(BigDecimal tensionR35) {
		this.tensionR35 = tensionR35;
	}


	@Column(name="TENSION_R36",nullable=true)
	public BigDecimal getTensionR36() {
		return tensionR36;
	}


	public void setTensionR36(BigDecimal tensionR36) {
		this.tensionR36 = tensionR36;
	}


	@Column(name="TENSION_R37",nullable=true)
	public BigDecimal getTensionR37() {
		return tensionR37;
	}


	public void setTensionR37(BigDecimal tensionR37) {
		this.tensionR37 = tensionR37;
	}


	@Column(name="TENSION_R38",nullable=true)
	public BigDecimal getTensionR38() {
		return tensionR38;
	}


	public void setTensionR38(BigDecimal tensionR38) {
		this.tensionR38 = tensionR38;
	}


	@Column(name="TENSION_R39",nullable=true)
	public BigDecimal getTensionR39() {
		return tensionR39;
	}


	public void setTensionR39(BigDecimal tensionR39) {
		this.tensionR39 = tensionR39;
	}


	@Column(name="TENSION_R40",nullable=true)
	public BigDecimal getTensionR40() {
		return tensionR40;
	}


	public void setTensionR40(BigDecimal tensionR40) {
		this.tensionR40 = tensionR40;
	}


	@Column(name="TENSION_S1",nullable=true)
	public BigDecimal getTensionS1() {
		return tensionS1;
	}


	public void setTensionS1(BigDecimal tensionS1) {
		this.tensionS1 = tensionS1;
	}


	@Column(name="TENSION_S2",nullable=true)
	public BigDecimal getTensionS2() {
		return tensionS2;
	}


	public void setTensionS2(BigDecimal tensionS2) {
		this.tensionS2 = tensionS2;
	}


	@Column(name="TENSION_S3",nullable=true)
	public BigDecimal getTensionS3() {
		return tensionS3;
	}


	public void setTensionS3(BigDecimal tensionS3) {
		this.tensionS3 = tensionS3;
	}


	@Column(name="TENSION_S4",nullable=true)
	public BigDecimal getTensionS4() {
		return tensionS4;
	}


	public void setTensionS4(BigDecimal tensionS4) {
		this.tensionS4 = tensionS4;
	}


	@Column(name="TENSION_S5",nullable=true)
	public BigDecimal getTensionS5() {
		return tensionS5;
	}


	public void setTensionS5(BigDecimal tensionS5) {
		this.tensionS5 = tensionS5;
	}


	@Column(name="TENSION_S6",nullable=true)
	public BigDecimal getTensionS6() {
		return tensionS6;
	}


	public void setTensionS6(BigDecimal tensionS6) {
		this.tensionS6 = tensionS6;
	}


	@Column(name="TENSION_S7",nullable=true)
	public BigDecimal getTensionS7() {
		return tensionS7;
	}


	public void setTensionS7(BigDecimal tensionS7) {
		this.tensionS7 = tensionS7;
	}


	@Column(name="TENSION_S8",nullable=true)
	public BigDecimal getTensionS8() {
		return tensionS8;
	}


	public void setTensionS8(BigDecimal tensionS8) {
		this.tensionS8 = tensionS8;
	}


	@Column(name="TENSION_S9",nullable=true)
	public BigDecimal getTensionS9() {
		return tensionS9;
	}


	public void setTensionS9(BigDecimal tensionS9) {
		this.tensionS9 = tensionS9;
	}


	@Column(name="TENSION_S10",nullable=true)
	public BigDecimal getTensionS10() {
		return tensionS10;
	}


	public void setTensionS10(BigDecimal tensionS10) {
		this.tensionS10 = tensionS10;
	}


	@Column(name="TENSION_S11",nullable=true)
	public BigDecimal getTensionS11() {
		return tensionS11;
	}


	public void setTensionS11(BigDecimal tensionS11) {
		this.tensionS11 = tensionS11;
	}


	@Column(name="TENSION_S12",nullable=true)
	public BigDecimal getTensionS12() {
		return tensionS12;
	}


	public void setTensionS12(BigDecimal tensionS12) {
		this.tensionS12 = tensionS12;
	}


	@Column(name="TENSION_S13",nullable=true)
	public BigDecimal getTensionS13() {
		return tensionS13;
	}


	public void setTensionS13(BigDecimal tensionS13) {
		this.tensionS13 = tensionS13;
	}


	@Column(name="TENSION_S14",nullable=true)
	public BigDecimal getTensionS14() {
		return tensionS14;
	}


	public void setTensionS14(BigDecimal tensionS14) {
		this.tensionS14 = tensionS14;
	}


	@Column(name="TENSION_S15",nullable=true)
	public BigDecimal getTensionS15() {
		return tensionS15;
	}


	public void setTensionS15(BigDecimal tensionS15) {
		this.tensionS15 = tensionS15;
	}


	@Column(name="TENSION_S16",nullable=true)
	public BigDecimal getTensionS16() {
		return tensionS16;
	}


	public void setTensionS16(BigDecimal tensionS16) {
		this.tensionS16 = tensionS16;
	}


	@Column(name="TENSION_S17",nullable=true)
	public BigDecimal getTensionS17() {
		return tensionS17;
	}


	public void setTensionS17(BigDecimal tensionS17) {
		this.tensionS17 = tensionS17;
	}


	@Column(name="TENSION_S18",nullable=true)
	public BigDecimal getTensionS18() {
		return tensionS18;
	}


	public void setTensionS18(BigDecimal tensionS18) {
		this.tensionS18 = tensionS18;
	}


	@Column(name="TENSION_S19",nullable=true)
	public BigDecimal getTensionS19() {
		return tensionS19;
	}


	public void setTensionS19(BigDecimal tensionS19) {
		this.tensionS19 = tensionS19;
	}


	@Column(name="TENSION_S20",nullable=true)
	public BigDecimal getTensionS20() {
		return tensionS20;
	}


	public void setTensionS20(BigDecimal tensionS20) {
		this.tensionS20 = tensionS20;
	}


	@Column(name="TENSION_S21",nullable=true)
	public BigDecimal getTensionS21() {
		return tensionS21;
	}


	public void setTensionS21(BigDecimal tensionS21) {
		this.tensionS21 = tensionS21;
	}


	@Column(name="TENSION_S22",nullable=true)
	public BigDecimal getTensionS22() {
		return tensionS22;
	}


	public void setTensionS22(BigDecimal tensionS22) {
		this.tensionS22 = tensionS22;
	}


	@Column(name="TENSION_S23",nullable=true)
	public BigDecimal getTensionS23() {
		return tensionS23;
	}


	public void setTensionS23(BigDecimal tensionS23) {
		this.tensionS23 = tensionS23;
	}


	@Column(name="TENSION_S24",nullable=true)
	public BigDecimal getTensionS24() {
		return tensionS24;
	}


	public void setTensionS24(BigDecimal tensionS24) {
		this.tensionS24 = tensionS24;
	}


	@Column(name="TENSION_S25",nullable=true)
	public BigDecimal getTensionS25() {
		return tensionS25;
	}


	public void setTensionS25(BigDecimal tensionS25) {
		this.tensionS25 = tensionS25;
	}


	@Column(name="TENSION_S26",nullable=true)
	public BigDecimal getTensionS26() {
		return tensionS26;
	}


	public void setTensionS26(BigDecimal tensionS26) {
		this.tensionS26 = tensionS26;
	}


	@Column(name="TENSION_S27",nullable=true)
	public BigDecimal getTensionS27() {
		return tensionS27;
	}


	public void setTensionS27(BigDecimal tensionS27) {
		this.tensionS27 = tensionS27;
	}


	@Column(name="TENSION_S28",nullable=true)
	public BigDecimal getTensionS28() {
		return tensionS28;
	}


	public void setTensionS28(BigDecimal tensionS28) {
		this.tensionS28 = tensionS28;
	}


	@Column(name="TENSION_S29",nullable=true)
	public BigDecimal getTensionS29() {
		return tensionS29;
	}


	public void setTensionS29(BigDecimal tensionS29) {
		this.tensionS29 = tensionS29;
	}


	@Column(name="TENSION_S30",nullable=true)
	public BigDecimal getTensionS30() {
		return tensionS30;
	}


	public void setTensionS30(BigDecimal tensionS30) {
		this.tensionS30 = tensionS30;
	}


	@Column(name="TENSION_S31",nullable=true)
	public BigDecimal getTensionS31() {
		return tensionS31;
	}


	public void setTensionS31(BigDecimal tensionS31) {
		this.tensionS31 = tensionS31;
	}


	@Column(name="TENSION_S32",nullable=true)
	public BigDecimal getTensionS32() {
		return tensionS32;
	}


	public void setTensionS32(BigDecimal tensionS32) {
		this.tensionS32 = tensionS32;
	}


	@Column(name="TENSION_S33",nullable=true)
	public BigDecimal getTensionS33() {
		return tensionS33;
	}


	public void setTensionS33(BigDecimal tensionS33) {
		this.tensionS33 = tensionS33;
	}


	@Column(name="TENSION_S34",nullable=true)
	public BigDecimal getTensionS34() {
		return tensionS34;
	}


	public void setTensionS34(BigDecimal tensionS34) {
		this.tensionS34 = tensionS34;
	}


	@Column(name="TENSION_S35",nullable=true)
	public BigDecimal getTensionS35() {
		return tensionS35;
	}


	public void setTensionS35(BigDecimal tensionS35) {
		this.tensionS35 = tensionS35;
	}


	@Column(name="TENSION_S36",nullable=true)
	public BigDecimal getTensionS36() {
		return tensionS36;
	}


	public void setTensionS36(BigDecimal tensionS36) {
		this.tensionS36 = tensionS36;
	}


	@Column(name="TENSION_S37",nullable=true)
	public BigDecimal getTensionS37() {
		return tensionS37;
	}


	public void setTensionS37(BigDecimal tensionS37) {
		this.tensionS37 = tensionS37;
	}


	@Column(name="TENSION_S38",nullable=true)
	public BigDecimal getTensionS38() {
		return tensionS38;
	}


	public void setTensionS38(BigDecimal tensionS38) {
		this.tensionS38 = tensionS38;
	}


	@Column(name="TENSION_S39",nullable=true)
	public BigDecimal getTensionS39() {
		return tensionS39;
	}


	public void setTensionS39(BigDecimal tensionS39) {
		this.tensionS39 = tensionS39;
	}


	@Column(name="TENSION_S40",nullable=true)
	public BigDecimal getTensionS40() {
		return tensionS40;
	}


	public void setTensionS40(BigDecimal tensionS40) {
		this.tensionS40 = tensionS40;
	}


	@Column(name="TENSION_T1",nullable=true)
	public BigDecimal getTensionT1() {
		return tensionT1;
	}


	public void setTensionT1(BigDecimal tensionT1) {
		this.tensionT1 = tensionT1;
	}


	@Column(name="TENSION_T2",nullable=true)
	public BigDecimal getTensionT2() {
		return tensionT2;
	}


	public void setTensionT2(BigDecimal tensionT2) {
		this.tensionT2 = tensionT2;
	}


	@Column(name="TENSION_T3",nullable=true)
	public BigDecimal getTensionT3() {
		return tensionT3;
	}


	public void setTensionT3(BigDecimal tensionT3) {
		this.tensionT3 = tensionT3;
	}


	@Column(name="TENSION_T4",nullable=true)
	public BigDecimal getTensionT4() {
		return tensionT4;
	}


	public void setTensionT4(BigDecimal tensionT4) {
		this.tensionT4 = tensionT4;
	}


	@Column(name="TENSION_T5",nullable=true)
	public BigDecimal getTensionT5() {
		return tensionT5;
	}


	public void setTensionT5(BigDecimal tensionT5) {
		this.tensionT5 = tensionT5;
	}


	@Column(name="TENSION_T6",nullable=true)
	public BigDecimal getTensionT6() {
		return tensionT6;
	}


	public void setTensionT6(BigDecimal tensionT6) {
		this.tensionT6 = tensionT6;
	}


	@Column(name="TENSION_T7",nullable=true)
	public BigDecimal getTensionT7() {
		return tensionT7;
	}


	public void setTensionT7(BigDecimal tensionT7) {
		this.tensionT7 = tensionT7;
	}


	@Column(name="TENSION_T8",nullable=true)
	public BigDecimal getTensionT8() {
		return tensionT8;
	}


	public void setTensionT8(BigDecimal tensionT8) {
		this.tensionT8 = tensionT8;
	}


	@Column(name="TENSION_T9",nullable=true)
	public BigDecimal getTensionT9() {
		return tensionT9;
	}


	public void setTensionT9(BigDecimal tensionT9) {
		this.tensionT9 = tensionT9;
	}


	@Column(name="TENSION_T10",nullable=true)
	public BigDecimal getTensionT10() {
		return tensionT10;
	}


	public void setTensionT10(BigDecimal tensionT10) {
		this.tensionT10 = tensionT10;
	}


	@Column(name="TENSION_T11",nullable=true)
	public BigDecimal getTensionT11() {
		return tensionT11;
	}


	public void setTensionT11(BigDecimal tensionT11) {
		this.tensionT11 = tensionT11;
	}


	@Column(name="TENSION_T12",nullable=true)
	public BigDecimal getTensionT12() {
		return tensionT12;
	}


	public void setTensionT12(BigDecimal tensionT12) {
		this.tensionT12 = tensionT12;
	}


	@Column(name="TENSION_T13",nullable=true)
	public BigDecimal getTensionT13() {
		return tensionT13;
	}


	public void setTensionT13(BigDecimal tensionT13) {
		this.tensionT13 = tensionT13;
	}


	@Column(name="TENSION_T14",nullable=true)
	public BigDecimal getTensionT14() {
		return tensionT14;
	}


	public void setTensionT14(BigDecimal tensionT14) {
		this.tensionT14 = tensionT14;
	}


	@Column(name="TENSION_T15",nullable=true)
	public BigDecimal getTensionT15() {
		return tensionT15;
	}


	public void setTensionT15(BigDecimal tensionT15) {
		this.tensionT15 = tensionT15;
	}


	@Column(name="TENSION_T16",nullable=true)
	public BigDecimal getTensionT16() {
		return tensionT16;
	}


	public void setTensionT16(BigDecimal tensionT16) {
		this.tensionT16 = tensionT16;
	}


	@Column(name="TENSION_T17",nullable=true)
	public BigDecimal getTensionT17() {
		return tensionT17;
	}


	public void setTensionT17(BigDecimal tensionT17) {
		this.tensionT17 = tensionT17;
	}


	@Column(name="TENSION_T18",nullable=true)
	public BigDecimal getTensionT18() {
		return tensionT18;
	}


	public void setTensionT18(BigDecimal tensionT18) {
		this.tensionT18 = tensionT18;
	}


	@Column(name="TENSION_T19",nullable=true)
	public BigDecimal getTensionT19() {
		return tensionT19;
	}


	public void setTensionT19(BigDecimal tensionT19) {
		this.tensionT19 = tensionT19;
	}


	@Column(name="TENSION_T20",nullable=true)
	public BigDecimal getTensionT20() {
		return tensionT20;
	}


	public void setTensionT20(BigDecimal tensionT20) {
		this.tensionT20 = tensionT20;
	}


	@Column(name="TENSION_T21",nullable=true)
	public BigDecimal getTensionT21() {
		return tensionT21;
	}


	public void setTensionT21(BigDecimal tensionT21) {
		this.tensionT21 = tensionT21;
	}


	@Column(name="TENSION_T22",nullable=true)
	public BigDecimal getTensionT22() {
		return tensionT22;
	}


	public void setTensionT22(BigDecimal tensionT22) {
		this.tensionT22 = tensionT22;
	}


	@Column(name="TENSION_T23",nullable=true)
	public BigDecimal getTensionT23() {
		return tensionT23;
	}


	public void setTensionT23(BigDecimal tensionT23) {
		this.tensionT23 = tensionT23;
	}


	@Column(name="TENSION_T24",nullable=true)
	public BigDecimal getTensionT24() {
		return tensionT24;
	}


	public void setTensionT24(BigDecimal tensionT24) {
		this.tensionT24 = tensionT24;
	}


	@Column(name="TENSION_T25",nullable=true)
	public BigDecimal getTensionT25() {
		return tensionT25;
	}


	public void setTensionT25(BigDecimal tensionT25) {
		this.tensionT25 = tensionT25;
	}


	@Column(name="TENSION_T26",nullable=true)
	public BigDecimal getTensionT26() {
		return tensionT26;
	}


	public void setTensionT26(BigDecimal tensionT26) {
		this.tensionT26 = tensionT26;
	}


	@Column(name="TENSION_T27",nullable=true)
	public BigDecimal getTensionT27() {
		return tensionT27;
	}


	public void setTensionT27(BigDecimal tensionT27) {
		this.tensionT27 = tensionT27;
	}


	@Column(name="TENSION_T28",nullable=true)
	public BigDecimal getTensionT28() {
		return tensionT28;
	}


	public void setTensionT28(BigDecimal tensionT28) {
		this.tensionT28 = tensionT28;
	}


	@Column(name="TENSION_T29",nullable=true)
	public BigDecimal getTensionT29() {
		return tensionT29;
	}


	public void setTensionT29(BigDecimal tensionT29) {
		this.tensionT29 = tensionT29;
	}


	@Column(name="TENSION_T30",nullable=true)
	public BigDecimal getTensionT30() {
		return tensionT30;
	}


	public void setTensionT30(BigDecimal tensionT30) {
		this.tensionT30 = tensionT30;
	}


	@Column(name="TENSION_T31",nullable=true)
	public BigDecimal getTensionT31() {
		return tensionT31;
	}


	public void setTensionT31(BigDecimal tensionT31) {
		this.tensionT31 = tensionT31;
	}


	@Column(name="TENSION_T32",nullable=true)
	public BigDecimal getTensionT32() {
		return tensionT32;
	}


	public void setTensionT32(BigDecimal tensionT32) {
		this.tensionT32 = tensionT32;
	}


	@Column(name="TENSION_T33",nullable=true)
	public BigDecimal getTensionT33() {
		return tensionT33;
	}


	public void setTensionT33(BigDecimal tensionT33) {
		this.tensionT33 = tensionT33;
	}


	@Column(name="TENSION_T34",nullable=true)
	public BigDecimal getTensionT34() {
		return tensionT34;
	}


	public void setTensionT34(BigDecimal tensionT34) {
		this.tensionT34 = tensionT34;
	}


	@Column(name="TENSION_T35",nullable=true)
	public BigDecimal getTensionT35() {
		return tensionT35;
	}


	public void setTensionT35(BigDecimal tensionT35) {
		this.tensionT35 = tensionT35;
	}


	@Column(name="TENSION_T36",nullable=true)
	public BigDecimal getTensionT36() {
		return tensionT36;
	}


	public void setTensionT36(BigDecimal tensionT36) {
		this.tensionT36 = tensionT36;
	}


	@Column(name="TENSION_T37",nullable=true)
	public BigDecimal getTensionT37() {
		return tensionT37;
	}


	public void setTensionT37(BigDecimal tensionT37) {
		this.tensionT37 = tensionT37;
	}


	@Column(name="TENSION_T38",nullable=true)
	public BigDecimal getTensionT38() {
		return tensionT38;
	}


	public void setTensionT38(BigDecimal tensionT38) {
		this.tensionT38 = tensionT38;
	}


	@Column(name="TENSION_T39",nullable=true)
	public BigDecimal getTensionT39() {
		return tensionT39;
	}


	public void setTensionT39(BigDecimal tensionT39) {
		this.tensionT39 = tensionT39;
	}


	@Column(name="TENSION_T40",nullable=true)
	public BigDecimal getTensionT40() {
		return tensionT40;
	}


	public void setTensionT40(BigDecimal tensionT40) {
		this.tensionT40 = tensionT40;
	}


	@Column(name="TEN_INTERVALO_1",nullable=true)
	public BigDecimal getTenIntervalo1() {
		return tenIntervalo1;
	}


	public void setTenIntervalo1(BigDecimal tenIntervalo1) {
		this.tenIntervalo1 = tenIntervalo1;
	}


	@Column(name="TEN_INTERVALO_2",nullable=true)
	public BigDecimal getTenIntervalo2() {
		return tenIntervalo2;
	}


	public void setTenIntervalo2(BigDecimal tenIntervalo2) {
		this.tenIntervalo2 = tenIntervalo2;
	}


	@Column(name="TEN_INTERVALO_3",nullable=true)
	public BigDecimal getTenIntervalo3() {
		return tenIntervalo3;
	}


	public void setTenIntervalo3(BigDecimal tenIntervalo3) {
		this.tenIntervalo3 = tenIntervalo3;
	}


	@Column(name="TEN_INTERVALO_4",nullable=true)
	public BigDecimal getTenIntervalo4() {
		return tenIntervalo4;
	}


	public void setTenIntervalo4(BigDecimal tenIntervalo4) {
		this.tenIntervalo4 = tenIntervalo4;
	}


	@Column(name="TEN_INTERVALO_5",nullable=true)
	public BigDecimal getTenIntervalo5() {
		return tenIntervalo5;
	}


	public void setTenIntervalo5(BigDecimal tenIntervalo5) {
		this.tenIntervalo5 = tenIntervalo5;
	}


	@Column(name="TEN_INTERVALO_6",nullable=true)
	public BigDecimal getTenIntervalo6() {
		return tenIntervalo6;
	}


	public void setTenIntervalo6(BigDecimal tenIntervalo6) {
		this.tenIntervalo6 = tenIntervalo6;
	}


	@Column(name="TEN_INTERVALO_7",nullable=true)
	public BigDecimal getTenIntervalo7() {
		return tenIntervalo7;
	}


	public void setTenIntervalo7(BigDecimal tenIntervalo7) {
		this.tenIntervalo7 = tenIntervalo7;
	}


	@Column(name="TEN_INTERVALO_8",nullable=true)
	public BigDecimal getTenIntervalo8() {
		return tenIntervalo8;
	}


	public void setTenIntervalo8(BigDecimal tenIntervalo8) {
		this.tenIntervalo8 = tenIntervalo8;
	}


	@Column(name="TEN_INTERVALO_9",nullable=true)
	public BigDecimal getTenIntervalo9() {
		return tenIntervalo9;
	}


	public void setTenIntervalo9(BigDecimal tenIntervalo9) {
		this.tenIntervalo9 = tenIntervalo9;
	}


	@Column(name="TEN_INTERVALO_10",nullable=true)
	public BigDecimal getTenIntervalo10() {
		return tenIntervalo10;
	}


	public void setTenIntervalo10(BigDecimal tenIntervalo10) {
		this.tenIntervalo10 = tenIntervalo10;
	}


	@Column(name="TEN_INTERVALO_11",nullable=true)
	public BigDecimal getTenIntervalo11() {
		return tenIntervalo11;
	}


	public void setTenIntervalo11(BigDecimal tenIntervalo11) {
		this.tenIntervalo11 = tenIntervalo11;
	}


	@Column(name="TEN_INTERVALO_12",nullable=true)
	public BigDecimal getTenIntervalo12() {
		return tenIntervalo12;
	}


	public void setTenIntervalo12(BigDecimal tenIntervalo12) {
		this.tenIntervalo12 = tenIntervalo12;
	}


	@Column(name="TEN_INTERVALO_13",nullable=true)
	public BigDecimal getTenIntervalo13() {
		return tenIntervalo13;
	}


	public void setTenIntervalo13(BigDecimal tenIntervalo13) {
		this.tenIntervalo13 = tenIntervalo13;
	}


	@Column(name="TEN_INTERVALO_14",nullable=true)
	public BigDecimal getTenIntervalo14() {
		return tenIntervalo14;
	}


	public void setTenIntervalo14(BigDecimal tenIntervalo14) {
		this.tenIntervalo14 = tenIntervalo14;
	}


	@Column(name="TEN_INTERVALO_15",nullable=true)
	public BigDecimal getTenIntervalo15() {
		return tenIntervalo15;
	}


	public void setTenIntervalo15(BigDecimal tenIntervalo15) {
		this.tenIntervalo15 = tenIntervalo15;
	}


	@Column(name="TEN_INTERVALO_16",nullable=true)
	public BigDecimal getTenIntervalo16() {
		return tenIntervalo16;
	}


	public void setTenIntervalo16(BigDecimal tenIntervalo16) {
		this.tenIntervalo16 = tenIntervalo16;
	}


	@Column(name="TEN_INTERVALO_17",nullable=true)
	public BigDecimal getTenIntervalo17() {
		return tenIntervalo17;
	}


	public void setTenIntervalo17(BigDecimal tenIntervalo17) {
		this.tenIntervalo17 = tenIntervalo17;
	}


	@Column(name="TEN_INTERVALO_18",nullable=true)
	public BigDecimal getTenIntervalo18() {
		return tenIntervalo18;
	}


	public void setTenIntervalo18(BigDecimal tenIntervalo18) {
		this.tenIntervalo18 = tenIntervalo18;
	}


	@Column(name="TEN_INTERVALO_19",nullable=true)
	public BigDecimal getTenIntervalo19() {
		return tenIntervalo19;
	}


	public void setTenIntervalo19(BigDecimal tenIntervalo19) {
		this.tenIntervalo19 = tenIntervalo19;
	}


	@Column(name="TEN_INTERVALO_20",nullable=true)
	public BigDecimal getTenIntervalo20() {
		return tenIntervalo20;
	}


	public void setTenIntervalo20(BigDecimal tenIntervalo20) {
		this.tenIntervalo20 = tenIntervalo20;
	}


	@Column(name="TEN_INTERVALO_21",nullable=true)
	public BigDecimal getTenIntervalo21() {
		return tenIntervalo21;
	}


	public void setTenIntervalo21(BigDecimal tenIntervalo21) {
		this.tenIntervalo21 = tenIntervalo21;
	}


	@Column(name="TEN_INTERVALO_22",nullable=true)
	public BigDecimal getTenIntervalo22() {
		return tenIntervalo22;
	}


	public void setTenIntervalo22(BigDecimal tenIntervalo22) {
		this.tenIntervalo22 = tenIntervalo22;
	}


	@Column(name="TEN_INTERVALO_23",nullable=true)
	public BigDecimal getTenIntervalo23() {
		return tenIntervalo23;
	}


	public void setTenIntervalo23(BigDecimal tenIntervalo23) {
		this.tenIntervalo23 = tenIntervalo23;
	}


	@Column(name="TEN_INTERVALO_24",nullable=true)
	public BigDecimal getTenIntervalo24() {
		return tenIntervalo24;
	}


	public void setTenIntervalo24(BigDecimal tenIntervalo24) {
		this.tenIntervalo24 = tenIntervalo24;
	}


	@Column(name="TEN_INTERVALO_25",nullable=true)
	public BigDecimal getTenIntervalo25() {
		return tenIntervalo25;
	}


	public void setTenIntervalo25(BigDecimal tenIntervalo25) {
		this.tenIntervalo25 = tenIntervalo25;
	}


	@Column(name="TEN_INTERVALO_26",nullable=true)
	public BigDecimal getTenIntervalo26() {
		return tenIntervalo26;
	}


	public void setTenIntervalo26(BigDecimal tenIntervalo26) {
		this.tenIntervalo26 = tenIntervalo26;
	}


	@Column(name="TEN_INTERVALO_27",nullable=true)
	public BigDecimal getTenIntervalo27() {
		return tenIntervalo27;
	}


	public void setTenIntervalo27(BigDecimal tenIntervalo27) {
		this.tenIntervalo27 = tenIntervalo27;
	}


	@Column(name="TEN_INTERVALO_28",nullable=true)
	public BigDecimal getTenIntervalo28() {
		return tenIntervalo28;
	}


	public void setTenIntervalo28(BigDecimal tenIntervalo28) {
		this.tenIntervalo28 = tenIntervalo28;
	}


	@Column(name="TEN_INTERVALO_29",nullable=true)
	public BigDecimal getTenIntervalo29() {
		return tenIntervalo29;
	}


	public void setTenIntervalo29(BigDecimal tenIntervalo29) {
		this.tenIntervalo29 = tenIntervalo29;
	}


	@Column(name="TEN_INTERVALO_30",nullable=true)
	public BigDecimal getTenIntervalo30() {
		return tenIntervalo30;
	}


	public void setTenIntervalo30(BigDecimal tenIntervalo30) {
		this.tenIntervalo30 = tenIntervalo30;
	}


	@Column(name="TEN_INTERVALO_31",nullable=true)
	public BigDecimal getTenIntervalo31() {
		return tenIntervalo31;
	}


	public void setTenIntervalo31(BigDecimal tenIntervalo31) {
		this.tenIntervalo31 = tenIntervalo31;
	}


	@Column(name="TEN_INTERVALO_32",nullable=true)
	public BigDecimal getTenIntervalo32() {
		return tenIntervalo32;
	}


	public void setTenIntervalo32(BigDecimal tenIntervalo32) {
		this.tenIntervalo32 = tenIntervalo32;
	}


	@Column(name="TEN_INTERVALO_33",nullable=true)
	public BigDecimal getTenIntervalo33() {
		return tenIntervalo33;
	}


	public void setTenIntervalo33(BigDecimal tenIntervalo33) {
		this.tenIntervalo33 = tenIntervalo33;
	}


	@Column(name="TEN_INTERVALO_34",nullable=true)
	public BigDecimal getTenIntervalo34() {
		return tenIntervalo34;
	}


	public void setTenIntervalo34(BigDecimal tenIntervalo34) {
		this.tenIntervalo34 = tenIntervalo34;
	}


	@Column(name="TEN_INTERVALO_35",nullable=true)
	public BigDecimal getTenIntervalo35() {
		return tenIntervalo35;
	}


	public void setTenIntervalo35(BigDecimal tenIntervalo35) {
		this.tenIntervalo35 = tenIntervalo35;
	}


	@Column(name="TEN_INTERVALO_36",nullable=true)
	public BigDecimal getTenIntervalo36() {
		return tenIntervalo36;
	}


	public void setTenIntervalo36(BigDecimal tenIntervalo36) {
		this.tenIntervalo36 = tenIntervalo36;
	}


	@Column(name="TEN_INTERVALO_37",nullable=true)
	public BigDecimal getTenIntervalo37() {
		return tenIntervalo37;
	}


	public void setTenIntervalo37(BigDecimal tenIntervalo37) {
		this.tenIntervalo37 = tenIntervalo37;
	}


	@Column(name="TEN_INTERVALO_38",nullable=true)
	public BigDecimal getTenIntervalo38() {
		return tenIntervalo38;
	}


	public void setTenIntervalo38(BigDecimal tenIntervalo38) {
		this.tenIntervalo38 = tenIntervalo38;
	}


	@Column(name="TEN_INTERVALO_39",nullable=true)
	public BigDecimal getTenIntervalo39() {
		return tenIntervalo39;
	}


	public void setTenIntervalo39(BigDecimal tenIntervalo39) {
		this.tenIntervalo39 = tenIntervalo39;
	}


	@Column(name="TEN_INTERVALO_40",nullable=true)
	public BigDecimal getTenIntervalo40() {
		return tenIntervalo40;
	}


	public void setTenIntervalo40(BigDecimal tenIntervalo40) {
		this.tenIntervalo40 = tenIntervalo40;
	}
	
	@Column(name="ENE_INTERVALO",nullable=true)
	public BigDecimal getEneIntervalo() {
		return eneIntervalo;
	}


	public void setEneIntervalo(BigDecimal eneIntervalo) {
		this.eneIntervalo = eneIntervalo;
	}

	
	@Column(name="THD_R",nullable=true)
	public BigDecimal getThdR() {
		return thdR;
	}


	public void setThdR(BigDecimal thdR) {
		this.thdR = thdR;
	}

	@Column(name="THD_S",nullable=true)
	public BigDecimal getThdS() {
		return thdS;
	}


	public void setThdS(BigDecimal thdS) {
		this.thdS = thdS;
	}

	@Column(name="THD_T",nullable=true)
	public BigDecimal getThdT() {
		return thdT;
	}


	public void setThdT(BigDecimal thdT) {
		this.thdT = thdT;
	}

	@Column(name="THD_MAX",nullable=true)
	public BigDecimal getThdMax() {
		return thdMax;
	}


	public void setThdMax(BigDecimal thdMax) {
		this.thdMax = thdMax;
	}

	@Column(name="DPA",nullable=true)
	public BigDecimal getDpa() {
		return dpa;
	}


	public void setDpa(BigDecimal dpa) {
		this.dpa = dpa;
	}

	@Column(name="COM_ARM",nullable=true)
	public BigDecimal getComArm() {
		return comArm;
	}


	public void setComArm(BigDecimal comArm) {
		this.comArm = comArm;
	}

	@Column(name="PST_R",nullable=true)
	public BigDecimal getPstR() {
		return pstR;
	}


	public void setPstR(BigDecimal pstR) {
		this.pstR = pstR;
	}

	@Column(name="PST_S",nullable=true)
	public BigDecimal getPstS() {
		return pstS;
	}


	public void setPstS(BigDecimal pstS) {
		this.pstS = pstS;
	}

	@Column(name="PST_T",nullable=true)
	public BigDecimal getPstT() {
		return pstT;
	}


	public void setPstT(BigDecimal pstT) {
		this.pstT = pstT;
	}

	@Column(name="PST_MAX",nullable=true)
	public BigDecimal getPstMax() {
		return pstMax;
	}


	public void setPstMax(BigDecimal pstMax) {
		this.pstMax = pstMax;
	}

	@Column(name="DPF",nullable=true)
	public BigDecimal getDpf() {
		return dpf;
	}


	public void setDpf(BigDecimal dpf) {
		this.dpf = dpf;
	}

	@Column(name="POTENCIA",nullable=true)
	public BigDecimal getPotencia() {
		return potencia;
	}


	public void setPotencia(BigDecimal potencia) {
		this.potencia = potencia;
	}

	@Column(name="COM_FLICKER",nullable=true)
	public BigDecimal getComFlicker() {
		return comFlicker;
	}


	public void setComFlicker(BigDecimal comFlicker) {
		this.comFlicker = comFlicker;
	}
	
	@Column(name="EST_TENSION_1",nullable=true)
	public String getEstTension1() {
		return estTension1;
	}


	public void setEstTension1(String estTension1) {
		this.estTension1 = estTension1;
	}

	
	@Column(name="EST_TENSION_2",nullable=true)
	public String getEstTension2() {
		return estTension2;
	}


	public void setEstTension2(String estTension2) {
		this.estTension2 = estTension2;
	}

	@Column(name="EST_TENSION_3",nullable=true)
	public String getEstTension3() {
		return estTension3;
	}


	public void setEstTension3(String estTension3) {
		this.estTension3 = estTension3;
	}

	@Column(name="EST_TENSION_4",nullable=true)
	public String getEstTension4() {
		return estTension4;
	}


	public void setEstTension4(String estTension4) {
		this.estTension4 = estTension4;
	}

	@Column(name="EST_TENSION_5",nullable=true)
	public String getEstTension5() {
		return estTension5;
	}


	public void setEstTension5(String estTension5) {
		this.estTension5 = estTension5;
	}

	@Column(name="EST_TENSION_6",nullable=true)
	public String getEstTension6() {
		return estTension6;
	}


	public void setEstTension6(String estTension6) {
		this.estTension6 = estTension6;
	}

	@Column(name="EST_TENSION_7",nullable=true)
	public String getEstTension7() {
		return estTension7;
	}


	public void setEstTension7(String estTension7) {
		this.estTension7 = estTension7;
	}

	@Column(name="EST_TENSION_8",nullable=true)
	public String getEstTension8() {
		return estTension8;
	}


	public void setEstTension8(String estTension8) {
		this.estTension8 = estTension8;
	}

	@Column(name="EST_TENSION_9",nullable=true)
	public String getEstTension9() {
		return estTension9;
	}


	public void setEstTension9(String estTension9) {
		this.estTension9 = estTension9;
	}

	@Column(name="EST_TENSION_10",nullable=true)
	public String getEstTension10() {
		return estTension10;
	}


	public void setEstTension10(String estTension10) {
		this.estTension10 = estTension10;
	}

	@Column(name="EST_TENSION_11",nullable=true)
	public String getEstTension11() {
		return estTension11;
	}


	public void setEstTension11(String estTension11) {
		this.estTension11 = estTension11;
	}

	@Column(name="EST_TENSION_12",nullable=true)
	public String getEstTension12() {
		return estTension12;
	}


	public void setEstTension12(String estTension12) {
		this.estTension12 = estTension12;
	}

	@Column(name="EST_TENSION_13",nullable=true)
	public String getEstTension13() {
		return estTension13;
	}


	public void setEstTension13(String estTension13) {
		this.estTension13 = estTension13;
	}

	@Column(name="EST_TENSION_14",nullable=true)
	public String getEstTension14() {
		return estTension14;
	}


	public void setEstTension14(String estTension14) {
		this.estTension14 = estTension14;
	}

	@Column(name="EST_TENSION_15",nullable=true)
	public String getEstTension15() {
		return estTension15;
	}


	public void setEstTension15(String estTension15) {
		this.estTension15 = estTension15;
	}

	@Column(name="EST_TENSION_16",nullable=true)
	public String getEstTension16() {
		return estTension16;
	}


	public void setEstTension16(String estTension16) {
		this.estTension16 = estTension16;
	}

	@Column(name="EST_TENSION_17",nullable=true)
	public String getEstTension17() {
		return estTension17;
	}


	public void setEstTension17(String estTension17) {
		this.estTension17 = estTension17;
	}

	@Column(name="EST_TENSION_18",nullable=true)
	public String getEstTension18() {
		return estTension18;
	}


	public void setEstTension18(String estTension18) {
		this.estTension18 = estTension18;
	}

	@Column(name="EST_TENSION_19",nullable=true)
	public String getEstTension19() {
		return estTension19;
	}


	public void setEstTension19(String estTension19) {
		this.estTension19 = estTension19;
	}

	@Column(name="EST_TENSION_20",nullable=true)
	public String getEstTension20() {
		return estTension20;
	}


	public void setEstTension20(String estTension20) {
		this.estTension20 = estTension20;
	}

	@Column(name="EST_TENSION_21",nullable=true)
	public String getEstTension21() {
		return estTension21;
	}


	public void setEstTension21(String estTension21) {
		this.estTension21 = estTension21;
	}

	@Column(name="EST_TENSION_22",nullable=true)
	public String getEstTension22() {
		return estTension22;
	}


	public void setEstTension22(String estTension22) {
		this.estTension22 = estTension22;
	}

	@Column(name="EST_TENSION_23",nullable=true)
	public String getEstTension23() {
		return estTension23;
	}


	public void setEstTension23(String estTension23) {
		this.estTension23 = estTension23;
	}

	@Column(name="EST_TENSION_24",nullable=true)
	public String getEstTension24() {
		return estTension24;
	}


	public void setEstTension24(String estTension24) {
		this.estTension24 = estTension24;
	}

	@Column(name="EST_TENSION_25",nullable=true)
	public String getEstTension25() {
		return estTension25;
	}


	public void setEstTension25(String estTension25) {
		this.estTension25 = estTension25;
	}

	@Column(name="EST_TENSION_26",nullable=true)
	public String getEstTension26() {
		return estTension26;
	}


	public void setEstTension26(String estTension26) {
		this.estTension26 = estTension26;
	}

	@Column(name="EST_TENSION_27",nullable=true)
	public String getEstTension27() {
		return estTension27;
	}


	public void setEstTension27(String estTension27) {
		this.estTension27 = estTension27;
	}

	@Column(name="EST_TENSION_28",nullable=true)
	public String getEstTension28() {
		return estTension28;
	}


	public void setEstTension28(String estTension28) {
		this.estTension28 = estTension28;
	}

	@Column(name="EST_TENSION_29",nullable=true)
	public String getEstTension29() {
		return estTension29;
	}


	public void setEstTension29(String estTension29) {
		this.estTension29 = estTension29;
	}

	@Column(name="EST_TENSION_30",nullable=true)
	public String getEstTension30() {
		return estTension30;
	}


	public void setEstTension30(String estTension30) {
		this.estTension30 = estTension30;
	}

	@Column(name="EST_TENSION_31",nullable=true)
	public String getEstTension31() {
		return estTension31;
	}


	public void setEstTension31(String estTension31) {
		this.estTension31 = estTension31;
	}

	@Column(name="EST_TENSION_32",nullable=true)
	public String getEstTension32() {
		return estTension32;
	}


	public void setEstTension32(String estTension32) {
		this.estTension32 = estTension32;
	}

	@Column(name="EST_TENSION_33",nullable=true)
	public String getEstTension33() {
		return estTension33;
	}


	public void setEstTension33(String estTension33) {
		this.estTension33 = estTension33;
	}

	@Column(name="EST_TENSION_34",nullable=true)
	public String getEstTension34() {
		return estTension34;
	}


	public void setEstTension34(String estTension34) {
		this.estTension34 = estTension34;
	}

	@Column(name="EST_TENSION_35",nullable=true)
	public String getEstTension35() {
		return estTension35;
	}


	public void setEstTension35(String estTension35) {
		this.estTension35 = estTension35;
	}

	@Column(name="EST_TENSION_36",nullable=true)
	public String getEstTension36() {
		return estTension36;
	}


	public void setEstTension36(String estTension36) {
		this.estTension36 = estTension36;
	}

	@Column(name="EST_TENSION_37",nullable=true)
	public String getEstTension37() {
		return estTension37;
	}


	public void setEstTension37(String estTension37) {
		this.estTension37 = estTension37;
	}

	@Column(name="EST_TENSION_38",nullable=true)
	public String getEstTension38() {
		return estTension38;
	}


	public void setEstTension38(String estTension38) {
		this.estTension38 = estTension38;
	}

	@Column(name="EST_TENSION_39",nullable=true)
	public String getEstTension39() {
		return estTension39;
	}


	public void setEstTension39(String estTension39) {
		this.estTension39 = estTension39;
	}

	@Column(name="EST_TENSION_40",nullable=true)
	public String getEstTension40() {
		return estTension40;
	}


	public void setEstTension40(String estTension40) {
		this.estTension40 = estTension40;
	}

	
	@ManyToOne
    @JoinColumn(name="PK_FLI_EST_INTERVALO_ID", nullable=true)
	public EstadoIntervalo getFliEstIntervalo() {
		return fliEstIntervalo;
	}


	public void setFliEstIntervalo(EstadoIntervalo fliEstIntervalo) {
		this.fliEstIntervalo = fliEstIntervalo;
	}


	@ManyToOne
    @JoinColumn(name="PK_ARM_EST_INTERVALO_ID", nullable=true)
	public EstadoIntervalo getArmEstIntervalo() {
		return armEstIntervalo;
	}


	public void setArmEstIntervalo(EstadoIntervalo armEstIntervalo) {
		this.armEstIntervalo = armEstIntervalo;
	}
	
	@ManyToOne
    @JoinColumn(name="PK_ARMV_EST_INTERVALO_ID", nullable=true)
	public EstadoIntervalo getArmvEstIntervalo() {
		return armvEstIntervalo;
	}


	public void setArmvEstIntervalo(EstadoIntervalo armvEstIntervalo) {
		this.armvEstIntervalo = armvEstIntervalo;
	}

	
	@ManyToOne
    @JoinColumn(name="PK_MEDICION_ID", nullable=false)
	public Medicion getMedicion() {
		return medicion;
	}


	public void setMedicion(Medicion medicion) {
		this.medicion = medicion;
	}

	@Column(name="ESTADO",nullable=true)
	public String getEstado() {
		return estado;
	}


	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Column(name="ENERGIA_R",nullable=true)
	public BigDecimal getEnergiaR() {
		return energiaR;
	}


	public void setEnergiaR(BigDecimal energiaR) {
		this.energiaR = energiaR;
	}


	@Column(name="ENERGIA_S",nullable=true)
	public BigDecimal getEnergiaS() {
		return energiaS;
	}


	public void setEnergiaS(BigDecimal energiaS) {
		this.energiaS = energiaS;
	}


	@Column(name="ENERGIA_T",nullable=true)
	public BigDecimal getEnergiaT() {
		return energiaT;
	}


	public void setEnergiaT(BigDecimal energiaT) {
		this.energiaT = energiaT;
	}
	
	
}
