
package pe.com.indra.calidad.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import pe.com.calidad.suministro.entity.Interrupcion;
/**
 *
 * @author Juan
 */
@Entity
@Table(name="CAL_ESTADO_INTERVALO")
@SequenceGenerator(name = "EstIntervaloIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "EstadoIntervaloSeq")
public class EstadoIntervalo implements Serializable {
    private long estIntervaloId;
    private String codEstIntervalo;
    private String descripcion;
    private String estado;
    
    private Set<IntervaloTension> intervaloTensiones;
    
    private Set<IntervaloPerturbacion> intervaloPerturbacionesFli;
    private Set<IntervaloPerturbacion> intervaloPerturbacionesArm;
    private Set<IntervaloPerturbacion> intervaloPerturbacionesArmv;
    
 
	public EstadoIntervalo(String codEstIntervalo, String descripcion, String estado) {
        this.codEstIntervalo = codEstIntervalo;
        this.descripcion = descripcion;
        this.estado = estado;
        this.intervaloTensiones = new HashSet<IntervaloTension>();
        this.intervaloPerturbacionesFli = new HashSet<IntervaloPerturbacion>();
        this.intervaloPerturbacionesArm = new HashSet<IntervaloPerturbacion>();
        this.intervaloPerturbacionesArmv = new HashSet<IntervaloPerturbacion>();
    }

    public EstadoIntervalo() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EstIntervaloIdGenerator")
    @Column(name = "EST_INTERVALO_ID",nullable=false,unique=true)
    public long getEstIntervaloId() {
        return estIntervaloId;
    }

    public void setEstIntervaloId(long estIntervaloId) {
        this.estIntervaloId = estIntervaloId;
    }

    @Column(name="COD_EST_INTERVALO",nullable=false, unique=true)
    public String getCodEstIntervalo() {
        return codEstIntervalo;
    }

    public void setCodEstIntervalo(String codEstIntervalo) {
        this.codEstIntervalo = codEstIntervalo;
    }

    @Column(name="DESCRIPCION",nullable=true, unique=true)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "estadoIntervalo")
    public Set<IntervaloTension> getIntervaloTensiones() {
        return intervaloTensiones;
    }

    public void setIntervaloTensiones(Set<IntervaloTension> intervaloTensiones) {
        this.intervaloTensiones = intervaloTensiones;
    }
    
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "fliEstIntervalo")
    public Set<IntervaloPerturbacion> getIntervaloPerturbacionesFli() {
		return intervaloPerturbacionesFli;
	}

	public void setIntervaloPerturbacionesFli(
			Set<IntervaloPerturbacion> intervaloPerturbacionesFli) {
		this.intervaloPerturbacionesFli = intervaloPerturbacionesFli;
	}

	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "armEstIntervalo")
	public Set<IntervaloPerturbacion> getIntervaloPerturbacionesArm() {
		return intervaloPerturbacionesArm;
	}

	public void setIntervaloPerturbacionesArm(
			Set<IntervaloPerturbacion> intervaloPerturbacionesArm) {
		this.intervaloPerturbacionesArm = intervaloPerturbacionesArm;
	}
    
	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "armvEstIntervalo")
	public Set<IntervaloPerturbacion> getIntervaloPerturbacionesArmv() {
		return intervaloPerturbacionesArmv;
	}

	public void setIntervaloPerturbacionesArmv(
			Set<IntervaloPerturbacion> intervaloPerturbacionesArmv) {
		this.intervaloPerturbacionesArmv = intervaloPerturbacionesArmv;
	}
    
}
