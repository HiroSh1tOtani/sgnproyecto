package pe.com.indra.calidad.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the ROL database table.
 * 
 */
@Entity
public class Rol implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String rolename;

	private String description;

	private String grupo;
	

	//bi-directional many-to-many association to Usuario
	@ManyToMany(mappedBy="rols", fetch=FetchType.EAGER)
	private List<Usuario> usuarios;

	public Rol() {
	}

	public String getRolename() {
		return this.rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getGrupo() {
		return this.grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public List<Usuario> getUsuarios() {
		return this.usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}
	
	public String toString(){
		return rolename;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getRolename().equals(((Rol) obj).getRolename())){
			return true;
		}else{
			return false;
		}
	}


}