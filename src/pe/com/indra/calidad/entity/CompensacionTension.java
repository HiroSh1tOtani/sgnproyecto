/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.indra.calidad.entity;

import java.sql.Date;
import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;
/**
 *
 * @author Juan
 */
@Entity
@Table(name="CAL_COMPENSACION_TENSION")
@SequenceGenerator (name = "ComTensionIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "CompensacionTensionSeq")
public class CompensacionTension implements Serializable{
    private long comTensionId;
    private String numSumAfectado;
    private BigDecimal eneSuministrada;
    private BigDecimal eneRangoA1;
    private BigDecimal eneRangoA2;
    private long numIntA1;
    private long numIntA2;
    private BigDecimal sumValAP;
    private BigDecimal sumValAPSO;
    private BigDecimal sumValAPSU;
    private BigDecimal monComCliente;
    private String anaPerCompensacion;
    private String numSumMedido1;
    private String numSumMedido2;
    private String semestre;
    private BigDecimal sasot;
    private BigDecimal sasut;
    private BigDecimal sumAPInicial;
    private BigDecimal sumAPIniDisminuido;
    private BigDecimal sumAPFinal;
    private BigDecimal sumAPFinalAumentado;
    private String tipCompensacion;
    private Medicion medicion;
    private TipoEnergia tipoenergia;
    private String estado;
    private BigDecimal nhInterrupciones;
    private BigDecimal facCompensacionSO;
    private BigDecimal facCompensacionSU;
    private BigDecimal numIntervalosSO;
    private BigDecimal numIntervalosSU;
    private String numSumMedicion;
    private String numIdentificador;
    private String anaPerPorSeCompensa;
    
    public CompensacionTension(String numSumAfectado, BigDecimal eneSuministrada, BigDecimal eneRangoA1, BigDecimal eneRangoA2, long numIntA1, long numIntA2, BigDecimal sumValAP,BigDecimal sumValAPSO,BigDecimal sumValAPSU, BigDecimal monComCliente, String anaPerCompensacion, String numSumMedido1 , String numSumMedido2 , String semestre , BigDecimal sasot, BigDecimal sasut , BigDecimal sumAPInicial , BigDecimal sumAPIniDisminuido , BigDecimal sumAPFinal , BigDecimal sumAPFinalAumentado , String tipCompensacion , Medicion medicion, TipoEnergia tipoenergia, String estado, BigDecimal nhInterrupciones, BigDecimal facCompensacionSO, BigDecimal facCompensacionSU, BigDecimal numIntervalosSO, BigDecimal numIntervalosSU) {
        
        this.numSumAfectado = numSumAfectado;
        this.eneSuministrada = eneSuministrada;
        this.eneRangoA1 = eneRangoA1;
        this.eneRangoA2 = eneRangoA2;
        this.numIntA1 = numIntA1;
        this.numIntA2 = numIntA2;
        this.sumValAP = sumValAP;
        this.sumValAPSO = sumValAPSO;
        this.sumValAPSU = sumValAPSU;
        this.monComCliente = monComCliente;
        this.anaPerCompensacion = anaPerCompensacion;
        this.numSumMedido1 = numSumMedido1;
        this.numSumMedido2 = numSumMedido2;
        this.semestre = semestre;
        this.sasot = sasot;
        this.sasut = sasut;
        this.sumAPInicial = sumAPInicial;
        this.sumAPIniDisminuido = sumAPIniDisminuido;
        this.sumAPFinal = sumAPFinal;
        this.sumAPFinalAumentado = sumAPFinalAumentado;
        this.tipCompensacion = tipCompensacion;
        this.medicion = medicion;
        this.tipoenergia = tipoenergia;
        this.estado = estado;
        this.nhInterrupciones = nhInterrupciones;
        this.facCompensacionSO = facCompensacionSO;
        this.facCompensacionSU = facCompensacionSU;
        this.numIntervalosSO = numIntervalosSO;
        this.numIntervalosSU = numIntervalosSU;
    }

    public CompensacionTension() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ComTensionIdGenerator")
    @Column(name = "COM_TENSION_ID",nullable=false,unique=true)
    public long getComTensionId() {
        return comTensionId;
    }

    public void setComTensionId(long comTensionId) {
        this.comTensionId = comTensionId;
    }

    @Column(name="NUM_SUM_AFECTADO",nullable=false)
    public String getNumSumAfectado() {
        return numSumAfectado;
    }

    public void setNumSumAfectado(String numSumAfectado) {
        this.numSumAfectado = numSumAfectado;
    }

    @Column(name="ENE_SUMINISTRADA",nullable=true)
    public BigDecimal getEneSuministrada() {
        return eneSuministrada;
    }

    public void setEneSuministrada(BigDecimal eneSuministrada) {
        this.eneSuministrada = eneSuministrada;
    }

    @Column(name="ENE_RANGO_A1",nullable=true)
    public BigDecimal getEneRangoA1() {
        return eneRangoA1;
    }

    public void setEneRangoA1(BigDecimal eneRangoA1) {
        this.eneRangoA1 = eneRangoA1;
    }

    @Column(name="ENE_RANGO_A2",nullable=true)
    public BigDecimal getEneRangoA2() {
        return eneRangoA2;
    }

    public void setEneRangoA2(BigDecimal eneRangoA2) {
        this.eneRangoA2 = eneRangoA2;
    }

    @Column(name="NUM_INT_A1",nullable=true)
    public long getNumIntA1() {
        return numIntA1;
    }

    public void setNumIntA1(long numIntA1) {
        this.numIntA1 = numIntA1;
    }

    @Column(name="NUM_INT_A2",nullable=true)
    public long getNumIntA2() {
        return numIntA2;
    }

    public void setNumIntA2(long numIntA2) {
        this.numIntA2 = numIntA2;
    }

    @Column(name="SUM_VAL_AP",nullable=true)
    public BigDecimal getSumValAP() {
        return sumValAP;
    }

    public void setSumValAP(BigDecimal sumValAP) {
        this.sumValAP = sumValAP;
    }
    
    @Column(name="SUM_VAL_AP_SO",nullable=true)
    public BigDecimal getSumValAPSO() {
		return sumValAPSO;
	}

	public void setSumValAPSO(BigDecimal sumValAPSO) {
		this.sumValAPSO = sumValAPSO;
	}

	@Column(name="SUM_VAL_AP_SU",nullable=true)
	public BigDecimal getSumValAPSU() {
		return sumValAPSU;
	}

	public void setSumValAPSU(BigDecimal sumValAPSU) {
		this.sumValAPSU = sumValAPSU;
	}

    @Column(name="MON_COM_CLIENTE",nullable=true)
    public BigDecimal getMonComCliente() {
        return monComCliente;
    }

    public void setMonComCliente(BigDecimal monComCliente) {
        this.monComCliente = monComCliente;
    }

    @Column(name="ANA_PER_COMPENSACION",nullable=true)
    public String getAnaPerCompensacion() {
        return anaPerCompensacion;
    }

    public void setAnaPerCompensacion(String anaPerCompensacion) {
        this.anaPerCompensacion = anaPerCompensacion;
    }

	@Column(name="NUM_SUM_MEDIDO1",nullable=true)
	public String getNumSumMedido1() {
	    return numSumMedido1;
	}
	
	public void setNumSumMedido1(String numSumMedido1) {
	    this.numSumMedido1 = numSumMedido1;
	}
	
	@Column(name="NUM_SUM_MEDIDO2",nullable=true)
	public String getNumSumMedido2() {
	    return numSumMedido2;
	}
	
	public void setNumSumMedido2(String numSumMedido2) {
	    this.numSumMedido2 = numSumMedido2;
	}
		
	@Column(name="SEMESTRE",nullable=true)
	public String getSemestre() {
	    return semestre;
	}
	
	public void setSemestre(String semestre) {
	    this.semestre = semestre;
	}
	
	@Column(name="SASOT",nullable=true)
    public BigDecimal getSasot() {
        return sasot;
    }

    public void setSasot(BigDecimal sasot) {
        this.sasot = sasot;
    }
	
	@Column(name="SASUT",nullable=true)
    public BigDecimal getSasut() {
        return sasut;
    }

    public void setSasut(BigDecimal sasut) {
        this.sasut = sasut;
    }

	@Column(name="SUM_AP_INICIAL",nullable=true)
	public BigDecimal getSumAPInicial() {
	    return sumAPInicial;
	}
	
	public void setSumAPInicial(BigDecimal sumAPInicial) {
	    this.sumAPInicial = sumAPInicial;
	}

	@Column(name="SUM_AP_INI_DISMINUIDO",nullable=true)
	public BigDecimal getSumAPIniDisminuido() {
	    return sumAPIniDisminuido;
	}
	
	public void setSumAPIniDisminuido(BigDecimal sumAPIniDisminuido) {
	    this.sumAPIniDisminuido = sumAPIniDisminuido;
	}


	@Column(name="SUM_AP_FINAL",nullable=true)
	public BigDecimal getSumAPFinal() {
	    return sumAPFinal;
	}
	
	public void setSumAPFinal(BigDecimal sumAPFinal) {
	    this.sumAPFinal = sumAPFinal;
	}

	@Column(name="SUM_AP_FINAL_AUMENTADO",nullable=true)
	public BigDecimal getSumAPFinalAumentado() {
	    return sumAPFinalAumentado;
	}
	
	public void setSumAPFinalAumentado(BigDecimal sumAPFinalAumentado) {
	    this.sumAPFinalAumentado = sumAPFinalAumentado;
	}
		
	
	@Column(name="TIP_COMPENSACION",nullable=true)
	public String getTipCompensacion () {
	    return tipCompensacion;
	}
	
	public void setTipCompensacion(String tipCompensacion) {
	    this.tipCompensacion = tipCompensacion;
	}


	@ManyToOne
    @JoinColumn(name="PK_MEDICION_ID", nullable=true)
    public Medicion getMedicion() {
        return medicion;
    }

    public void setMedicion(Medicion medicion) {
        this.medicion = medicion;
    }

    @ManyToOne
    @JoinColumn(name="PK_TIP_ENERGIA_ID", nullable=true)
    public TipoEnergia getTipoenergia() {
        return tipoenergia;
    }

    public void setTipoenergia(TipoEnergia tipoenergia) {
        this.tipoenergia = tipoenergia;
    }

    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name="NH_INTERRUPCIONES",nullable=true)
	public BigDecimal getNhInterrupciones() {
		return nhInterrupciones;
	}

	public void setNhInterrupciones(BigDecimal nhInterrupciones) {
		this.nhInterrupciones = nhInterrupciones;
	}

	@Column(name="FAC_COMPENSACION_SO",nullable=true)
	public BigDecimal getFacCompensacionSO() {
		return facCompensacionSO;
	}

	public void setFacCompensacionSO(BigDecimal facCompensacionSO) {
		this.facCompensacionSO = facCompensacionSO;
	}

	@Column(name="FAC_COMPENSACION_SU",nullable=true)
	public BigDecimal getFacCompensacionSU() {
		return facCompensacionSU;
	}

	public void setFacCompensacionSU(BigDecimal facCompensacionSU) {
		this.facCompensacionSU = facCompensacionSU;
	}

	@Column(name="NUM_INTERVALOS_SO",nullable=true)
	public BigDecimal getNumIntervalosSO() {
		return numIntervalosSO;
	}

	public void setNumIntervalosSO(BigDecimal numIntervalosSO) {
		this.numIntervalosSO = numIntervalosSO;
	}

	@Column(name="NUM_INTERVALOS_SU",nullable=true)
	public BigDecimal getNumIntervalosSU() {
		return numIntervalosSU;
	}

	public void setNumIntervalosSU(BigDecimal numIntervalosSU) {
		this.numIntervalosSU = numIntervalosSU;
	}
	
	@Column(name="NUM_SUM_MEDICION",nullable=true)
	public String getNumSumMedicion() {
		return numSumMedicion;
	}

	public void setNumSumMedicion(String numSumMedicion) {
		this.numSumMedicion = numSumMedicion;
	}

	@Column(name="NUM_IDENTIFICADOR",nullable=true)
	public String getNumIdentificador() {
		return numIdentificador;
	}

	public void setNumIdentificador(String numIdentificador) {
		this.numIdentificador = numIdentificador;
	}

	@Column(name="ANA_PER_POR_SE_COMPENSA",nullable=true)
	public String getAnaPerPorSeCompensa() {
		return anaPerPorSeCompensa;
	}

	public void setAnaPerPorSeCompensa(String anaPerPorSeCompensa) {
		this.anaPerPorSeCompensa = anaPerPorSeCompensa;
	}
    	
}
