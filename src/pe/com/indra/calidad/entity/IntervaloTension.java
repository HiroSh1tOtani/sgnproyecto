
package pe.com.indra.calidad.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.*;

/**
 *
 * @author Juan
 */
@Entity
@Table(name="CAL_INTERVALO_TENSION")
@SequenceGenerator (name = "IntTensionIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "IntervaloTensionSeq")
public class IntervaloTension implements Serializable{
    /**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	private long intTensionId;
    private Timestamp intervalo;
    private String enPerMedicion;
    private BigDecimal tensionR;
    private BigDecimal tensionS;
    private BigDecimal tensionT;
    private BigDecimal energiaR;
    private BigDecimal energiaS;
    private BigDecimal energiaT;
    private BigDecimal varPorTension;
    private BigDecimal eneIntervalo;
    private BigDecimal facCompensacion;
    private Medicion medicion;
    private EstadoIntervalo estadoIntervalo;
    private String estado;
    private BigDecimal tenIntervalo;
    private BigDecimal flicker;
    private BigDecimal armonicas;
    

    public IntervaloTension(Timestamp intervalo, String enPerMedicion, BigDecimal tensionR, BigDecimal tensionS, BigDecimal tensionT, BigDecimal energiaR, BigDecimal energiaS, BigDecimal energiaT, BigDecimal varPorTension, BigDecimal eneIntervalo, BigDecimal facCompensacion, Medicion medicion, EstadoIntervalo estadoIntervalo, String estado,BigDecimal tenIntervalo) {
        
        this.intervalo = intervalo;
        this.enPerMedicion = enPerMedicion;
        this.tensionR = tensionR;
        this.tensionS = tensionS;
        this.tensionT = tensionT;
        this.energiaR = energiaR;
        this.energiaS = energiaS;
        this.energiaT = energiaT;
        this.varPorTension = varPorTension;
        this.eneIntervalo = eneIntervalo;
        this.setFacCompensacion(facCompensacion);
        this.medicion = medicion;
        this.estadoIntervalo = estadoIntervalo;
        this.estado = estado;
        this.setTenIntervalo(tenIntervalo);
    }

    public IntervaloTension() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IntTensionIdGenerator")
    @Column(name = "INT_TENSION_ID",nullable=false,unique=true)
    public long getIntTensionId() {
        return intTensionId;
    }

    public void setIntTensionId(long intTensionId) {
        this.intTensionId = intTensionId;
    }

    @Column(name="INTERVALO",nullable=false)
    public Timestamp getIntervalo() {
        return intervalo;
    }

    public void setIntervalo(Timestamp intervalo) {
        this.intervalo = intervalo;
    }

    @Column(name="EN_PER_MEDICION",nullable=true)
    public String getEnPerMedicion() {
        return enPerMedicion;
    }

    public void setEnPerMedicion(String enPerMedicion) {
        this.enPerMedicion = enPerMedicion;
    }

    @Column(name="TENSION_R",nullable=false)
    public BigDecimal getTensionR() {
        return tensionR;
    }

    public void setTensionR(BigDecimal tensionR) {
        this.tensionR = tensionR;
    }

    @Column(name="TENSION_S",nullable=true)
    public BigDecimal getTensionS() {
        return tensionS;
    }

    public void setTensionS(BigDecimal tensionS) {
        this.tensionS = tensionS;
    }

    @Column(name="TENSION_T",nullable=true)
    public BigDecimal getTensionT() {
        return tensionT;
    }

    public void setTensionT(BigDecimal tensionT) {
        this.tensionT = tensionT;
    }

    @Column(name="ENERGIA_R",nullable=true)
    public BigDecimal getEnergiaR() {
        return energiaR;
    }

    public void setEnergiaR(BigDecimal energiaR) {
        this.energiaR = energiaR;
    }

    @Column(name="ENERGIA_S",nullable=true)
    public BigDecimal getEnergiaS() {
        return energiaS;
    }

    public void setEnergiaS(BigDecimal energiaS) {
        this.energiaS = energiaS;
    }

    @Column(name="ENERGIA_T",nullable=true)
    public BigDecimal getEnergiaT() {
        return energiaT;
    }

    public void setEnergiaT(BigDecimal energiaT) {
        this.energiaT = energiaT;
    }

    @Column(name="VAR_POR_TENSION",nullable=true)
    public BigDecimal getVarPorTension() {
        return varPorTension;
    }

    public void setVarPorTension(BigDecimal varPorTension) {
        this.varPorTension = varPorTension;
    }

    @Column(name="ENE_INTERVALO",nullable=true)
    public BigDecimal getEneIntervalo() {
        return eneIntervalo;
    }

    public void setEneIntervalo(BigDecimal eneIntervalo) {
        this.eneIntervalo = eneIntervalo;
    }
    
    @Column(name="FAC_COMPENSACION",nullable=true)
    public BigDecimal getFacCompensacion() {
		return facCompensacion;
	}

	public void setFacCompensacion(BigDecimal facCompensacion) {
		this.facCompensacion = facCompensacion;
	}

    @ManyToOne
    @JoinColumn(name="PK_MEDICION_ID", nullable=false)
    public Medicion getMedicion() {
        return medicion;
    }

	public void setMedicion(Medicion medicion) {
        this.medicion = medicion;
    }

    @ManyToOne
    @JoinColumn(name="PK_EST_INTERVALO_ID", nullable=true)
    public EstadoIntervalo getEstadoIntervalo() {
        return estadoIntervalo;
    }

    public void setEstadoIntervalo(EstadoIntervalo estadoIntervalo) {
        this.estadoIntervalo = estadoIntervalo;
    }

    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
 
    @Column(name="TEN_INTERVALO",nullable=true)
	public BigDecimal getTenIntervalo() {
		return tenIntervalo;
	}

	public void setTenIntervalo(BigDecimal tenIntervalo) {
		this.tenIntervalo = tenIntervalo;
	}

	@Column(name="FLICKER",nullable=true)
	public BigDecimal getFlicker() {
		return flicker;
	}

	public void setFlicker(BigDecimal flicker) {
		this.flicker = flicker;
	}

	@Column(name="ARMONICAS",nullable=true)
	public BigDecimal getArmonicas() {
		return armonicas;
	}

	public void setArmonicas(BigDecimal armonicas) {
		this.armonicas = armonicas;
	}
    
}
