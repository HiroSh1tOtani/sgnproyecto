
package pe.com.indra.calidad.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

/**
 *
 * @author Juan
 */
@Entity
@Table(name="CAL_ESTADO_MEDICION")
@SequenceGenerator(name = "EstMedicionIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "EstadoMedicionSeq")
public class EstadoMedicion implements Serializable {
    private long estMedicionId;
    private String codEstMedicion;
    private String descripcion;
    private String estado;
    
    private Set<Medicion> mediciones;
    private Set<Medicion> perMediciones;
    
	public EstadoMedicion(String codEstMedicion, String descripcion, String estado) {
        this.codEstMedicion = codEstMedicion;
        this.descripcion = descripcion;
        this.estado = estado;
        
        this.mediciones = new HashSet<Medicion>();
    }

    public EstadoMedicion() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EstMedicionIdGenerator")
    @Column(name = "EST_MEDICION_ID",nullable=false,unique=true)
    public long getEstMedicionId() {
        return estMedicionId;
    }

    public void setEstMedicionId(long estMedicionId) {
        this.estMedicionId = estMedicionId;
    }

    @Column(name="COD_EST_MEDICION",nullable=false, unique=true)
    public String getCodEstMedicion() {
        return codEstMedicion;
    }

    public void setCodEstMedicion(String codEstMedicion) {
        this.codEstMedicion = codEstMedicion;
    }

    @Column(name="DESCRIPCION",nullable=true, unique=true)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE},mappedBy = "estadoMedicion")
    public Set<Medicion> getMediciones() {
        return mediciones;
    }

    public void setMediciones(Set<Medicion> mediciones) {
        this.mediciones = mediciones;
    }
    
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE},mappedBy = "perEstMedicion")
    public Set<Medicion> getPerMediciones() {
		return perMediciones;
	}

	public void setPerMediciones(Set<Medicion> perMediciones) {
		this.perMediciones = perMediciones;
	}
    
    public String toString(){
		return new Long(this.getEstMedicionId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getEstMedicionId()==((EstadoMedicion)obj).getEstMedicionId()){
			return true;
		}else{
			return false;
		}
	}
}
