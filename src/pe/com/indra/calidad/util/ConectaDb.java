package pe.com.indra.calidad.util;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public final class ConectaDb {

    private final String datasource = "CalidadDS";
    private String dataBase;

    public Connection getConnection() {
        Connection cn = null;

        try {
            Context contexto = new InitialContext();
            
            //DataSource fuenteDatos = (DataSource) contexto.lookup("java:/" + datasource);
            //cn = fuenteDatos.getConnection();
            
            if(dataBase.equals("IGEA")) {
            	/*
	            DriverManager.registerDriver (new oracle.jdbc.driver.OracleDriver());
	            //cn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.40.83:1521:sgcv10sl", "igea_data", "brutal");
	            cn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.40.85:1521:sgcv10sl", "igea_data", "brutal");
	            //cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "igea_data", "brutal");
	            */
	            
	            DataSource fuenteDatos = (DataSource) contexto.lookup("java:jboss/IgeaDS");
	            cn = fuenteDatos.getConnection();  	            
            }
            
            if(dataBase.equals("CALIDAD")) {
            	/*
	            DriverManager.registerDriver (new oracle.jdbc.driver.OracleDriver());
	            //cn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.40.83:1521:sgcv10sl", "calidad_data", "calidad");
	            cn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.40.85:1521:sgcv10sl", "calidad_data", "calidad");
	            //cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "calidad_data", "calidad");
				*/
            	
	            DataSource fuenteDatos = (DataSource) contexto.lookup("java:jboss/" + datasource);
	            cn = fuenteDatos.getConnection();              	
            }
            

        } catch (NamingException e) {
            System.out.println(e.getMessage());
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return cn;  // null si fracasa
    }

    public ConectaDb(String dataBase) {
    	this.dataBase = dataBase;
    }

	public String getDataBase() {
		return dataBase;
	}

	public void setDataBase(String dataBase) {
		this.dataBase = dataBase;
	}
}
