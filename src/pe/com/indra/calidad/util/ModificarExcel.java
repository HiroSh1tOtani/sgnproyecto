package pe.com.indra.calidad.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import pe.com.indra.calidad.entity.IntervaloTension;
import pe.com.indra.calidad.service.IntervaloTensionService;



public class ModificarExcel {

	public static void main(String[] args) {

		XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla("D:/archivoPOI1.xlsm");

		XSSFSheet objHssfSheet = null;
		objHssfSheet = objHssfWorkbook.getSheetAt(2);
		
		IntervaloTensionService intervaloTensionService = new IntervaloTensionService();
		
		int intRow = 4;
		int intCell = 0;
		
		/*
		XSSFRow objRow;
		XSSFCell objCell;
		Object objValue;
		*/
		
		for(IntervaloTension it:intervaloTensionService.getAllRows1(113963)){
			
			ModificarExcel.setCellValue(objHssfSheet, intRow, intCell, it.getIntervalo());
			ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 1, it.getTensionR());
			ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 2, it.getTensionS());
			ModificarExcel.setCellValue(objHssfSheet, intRow, intCell + 3, it.getTensionT());
			
			System.out.print(intRow + "\n");
			
			/*
			objRow = objHssfSheet.getRow(intRow);
			objCell = objRow.getCell(intCell);
			objValue = it.getIntervalo();

			if (!objValue.getClass().getName().equals("java.lang.String")) {
				if ((ModificarExcel.isDouble(objValue.toString()))	|| (ModificarExcel.isNumeric(objValue.toString()))) {
					double objValor = Double.parseDouble(objValue.toString());
					objCell.setCellType(0);
					objCell.setCellValue(objValor);
				} else {
					objCell.setCellValue(objValue.toString());
				}
			} else {
				objCell.setCellValue(objValue.toString());
			}

			objHssfSheet.setForceFormulaRecalculation(true);
			
			System.out.print(intRow + "\n");
			
			*/
			
			intRow++;
			
		};

		//ModificarExcel.setCellValue(objHssfSheet, 0, 1, "CONTENIDO CAMBIADO");

		ModificarExcel.saveFile("D:/archivoPOI1.xlsm", objHssfWorkbook);

	}
	

	public static void setCellValue(XSSFSheet objSheet, int intRow, int intCell, Object objValue) {
		XSSFRow objRow = null;
		XSSFCell objCell = null;
		
		if(objValue == null){
			return;
		}
		
		objRow = objSheet.getRow(intRow);
		
		if(objRow == null){
			objRow = objSheet.createRow(intRow);
		}
		
		
		objCell = objRow.getCell(intCell);
		
		if(objCell == null){
			objCell = objRow.createCell(intCell);
		}
				
		if (!objValue.getClass().getName().equals("java.lang.String")) {
			if ((ModificarExcel.isDouble(objValue.toString()))	|| (ModificarExcel.isNumeric(objValue.toString()))) {
				double objValor = Double.parseDouble(objValue.toString());
				objCell.setCellType(0);
				objCell.setCellValue(objValor);
			} else {
				objCell.setCellValue(objValue.toString());
			}
		} else {
			objCell.setCellValue(objValue.toString());
		}

		objSheet.setForceFormulaRecalculation(true);
	}

	public final static boolean isNumeric(String cadena) {
		try {
			Integer.parseInt(cadena);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}

	public final static boolean isDouble(String cadena) {
		try {
			Double.parseDouble(cadena);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}

	public static void saveFile(String strRuta, XSSFWorkbook objHssfWorkbook) {
		FileOutputStream objFileOutputStream = null;
		try {
			objFileOutputStream = new FileOutputStream(strRuta);
			objHssfWorkbook.write(objFileOutputStream);

			objFileOutputStream.flush();
			objFileOutputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				if (objFileOutputStream != null)
					objFileOutputStream.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} finally {
			try {
				if (objFileOutputStream != null)
					objFileOutputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static XSSFWorkbook getPlantilla(String strRuta) {
		XSSFWorkbook objHssfWorkbook = null;
		InputStream objInputStream = null;
		try {
			objInputStream = new FileInputStream(strRuta);

			//POIFSFileSystem objPoifsFileSystem = new POIFSFileSystem(objInputStream);

			objHssfWorkbook = new XSSFWorkbook(objInputStream);
		} catch (Exception e) {
			e.printStackTrace();
			if (objInputStream != null)
				try {objInputStream.close();} catch (IOException e1) {e1.printStackTrace();}
		} finally {
			if (objInputStream != null) {
				try {objInputStream.close();} catch (IOException e) {e.printStackTrace();}
			}
		}
		return objHssfWorkbook;
	}
}
