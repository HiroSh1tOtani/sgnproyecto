package pe.com.indra.calidad.util;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

public class Utilidad {
	
	public String getUsuarioAutenticado(){
		FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        return externalContext.getUserPrincipal().getName();
    }

	public String transformarArregloACadena(String[] arreglo,String separador){
		String result="";
		for(int i=0;i<arreglo.length;i++){
			result=result+arreglo[i];
			if(i<(arreglo.length-1)){
				result=result+separador;
			}
		}	
		return result;
	}
	
	public static String getParametro(String nombreParametro){
		String stringParametro=null;
		stringParametro=FacesContext.getCurrentInstance()
					                .getExternalContext()
					                .getRequestParameterMap()
					                .get(nombreParametro);
		return stringParametro;
	}
	
	public static void setParametro(String key, String value){

		FacesContext.getCurrentInstance()
					                .getExternalContext()
					                .getRequestParameterMap()
					                .put(key, value);
	}
	
}
