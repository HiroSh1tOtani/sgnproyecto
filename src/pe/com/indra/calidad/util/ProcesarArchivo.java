package pe.com.indra.calidad.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import pe.com.calidad.alumbradopublico.entity.MedicionAp;
import pe.com.calidad.alumbradopublico.service.MedicionApService;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.service.MedicionService;
import pe.com.indra.calidad.util.Sql;
import pe.com.indra.calidad.util.Utilidad;


public class ProcesarArchivo {

	private String ruta; //Ruta del archivo
	private String separadorFila;
	private String separadorColumna;
	private String tabla; //Nombre de la tabla donde deben ser insertada la data
	private String campos;//NombreColumna1, NombreColumna2, NombreColumna3, ...
	private String columnas; //1,3,5,7 .. los campos a extraer
	private long fila;// Fila de inicio del archivo a cargar.

	public ProcesarArchivo(String ruta, String separadorFila,
			String separadorColumna, String tabla, String campos,
			String columnas, long fila) {

		this.ruta = ruta;
		this.separadorFila = separadorFila;
		this.separadorColumna = separadorColumna;
		this.tabla = tabla;
		this.campos = campos;
		this.columnas = columnas;
		this.fila = fila;

	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public String getSeparadorFila() {
		return separadorFila;
	}

	public void setSeparadorFila(String separadorFila) {
		this.separadorFila = separadorFila;
	}

	public String getSeparadorColumna() {
		return separadorColumna;
	}

	public void setSeparadorColumna(String separadorColumna) {
		this.separadorColumna = separadorColumna;
	}

	public String getTabla() {
		return tabla;
	}

	public void setTabla(String tabla) {
		this.tabla = tabla;
	}

	public String getCampos() {
		return campos;
	}

	public void setCampos(String campos) {
		this.campos = campos;
	}

	public String getColumnas() {
		return columnas;
	}

	public void setColumnas(String columnas) {
		this.columnas = columnas;
	}

	public List<String[]> cargar() throws Exception{
		FileReader fr;
		BufferedReader bf;
		String camposX[];
		int indCampos[];
		
		ArrayList<String[]> result = null;
		ArrayList<String[]> ca = new ArrayList<String[]>();
		try {

			fr = new FileReader(getRuta());
			bf = new BufferedReader(fr);
			String sCadena;

			int filaInicio = 1;
			result = new ArrayList<String[]>();
			
			camposX = getColumnas().split(",");//Ejemplo: 1,3,5,7
			
			indCampos = new int[camposX.length];
			
			while ((sCadena = bf.readLine()) != null) {
				//eliminamos espacios en blanco (opcional)
				//sCadena = sCadena.replaceAll("\\s","");
				
				if(sCadena.equals(null)) continue;
				else{
					if(sCadena.trim().equals("")) continue;
				}
				
				if (filaInicio >= getFila()) {
	                //asignamos cada valor al nuevo vector (usamos como separador la coma)
					
					String[] vector = sCadena.split(getSeparadorColumna());
					
					result.add(vector);
				}
                filaInicio++;
                
			}
			fr.close();
			bf.close();		
			
			for(int n=0; n < camposX.length; n++) {
				indCampos[n] =  Integer.parseInt(camposX[n]) - 1;
	        }
			
			boolean agregar;
			for(String[] p : result) {
				agregar = true;
				String obj[] = new String[indCampos.length];
				for(int n=0; n < indCampos.length; n++) {
					try {
						obj[n] = p[indCampos[n]];
					} catch (ArrayIndexOutOfBoundsException e) {
						agregar = false;
						break;
					}
					
		        }
				if(agregar) ca.add(obj);
	        }
			

		} catch (FileNotFoundException fnfe) {
			throw new Exception(fnfe);
		} catch (IOException ioe) {
			throw new Exception(ioe);
		} catch (ArrayIndexOutOfBoundsException aie){
			throw new Exception(aie);
		}
		
		return ca;
	}

	public long getFila() {
		return fila;
	}

	public void setFila(long fila) {
		this.fila = fila;
	}
	
	public void insertarMedicion (String tabla, String campos, List<String[]> data, long id, long tipo){

		String insertSql;
		String selectSql;
		String updateSql;
		String mensaje = "";
		Sql sql = new Sql("CALIDAD");
		Object[] objFila;
		String resultado;
		String cl = System.getProperty("line.separator");
		BigDecimal tension;
		Long i;
		String result = null;
		String tipPunto;
		String tipAlim = "DN";
		String parMedido = "TE";
		
		long tipPuntoId;
		String ubicacion;
		System.out.println("Data" + data.size());
		for(String[] fila:data){			
			String sumi[];
			if(tipo==1){
				sumi = new String[1];
				sumi[0] = fila[0].trim();
				tipPunto = fila[2].trim();
				tipAlim = fila[3].trim();
				parMedido = fila[4].trim();
			}else {
				if(campos.equals("2")) {
					sumi = new String[2];
					sumi[0] = fila[0].trim();
					sumi[1] = fila[1].trim();
					tipPunto = fila[3].trim();
				} else {
					sumi = new String[1];
					sumi[0] = fila[0].trim();
					tipPunto = fila[2].trim();
				}
				
			}
			
			int orden = 0;
			for (String numSumMedicion : sumi) {
				orden = orden + 1;
				if(tipo == 2) {
					if(orden == 1){
						ubicacion = "I";
					} else {
						ubicacion = "F";
					}
				}else {
					ubicacion = "N";
				}
				
				selectSql = "SELECT MEDICION_ID FROM CAL_MEDICION WHERE NUM_SUM_MEDICION = '" + numSumMedicion + "' AND ESTADO = '1' AND PK_CAM_MEDICION_ID = " + String.valueOf(id);
				
				objFila = sql.getFila(selectSql);
				
				if(objFila == null) {
					
					tipPuntoId = 1;
					if(!tipPunto.equals(null) && !tipPunto.equals("")) {
						selectSql = "SELECT TIP_PUNTO_ID FROM CAL_TIPO_PUNTO WHERE COD_TIP_PUNTO = '" + tipPunto + "'";
						
						objFila = sql.getFila(selectSql);
						
						tipPuntoId = ((BigDecimal) objFila[0]).longValue();
						
					}
					
					selectSql = "SELECT TO_NUMBER(SUPPLY_VOLTAGE, '999999') FROM SUMINISTRO_ICIS_IGEA WHERE SUMINISTRO = " + numSumMedicion + "";
					
					objFila = sql.getFila(selectSql);
					
					tension = null;
					if(objFila != null){
						tension = (BigDecimal) objFila[0];
						insertSql = "INSERT INTO CAL_MEDICION (MEDICION_ID, NUM_SUM_MEDICION, TEN_ENTREGADA, PK_CAM_MEDICION_ID, PK_EST_MEDICION_ID, PK_TIP_PUNTO_ID, SECUENCIA, COMPEMSABLE, UBI_SUM_MEDICION, ESTADO) VALUES (MEDICIONSEQ.NEXTVAL,'" + 
								numSumMedicion + "'," + String.valueOf(tension) + ","+ String.valueOf(id) + ", 1," + String.valueOf(tipPuntoId) + ",0 ,'PROGRAMADA','" + ubicacion + "','1')";
					}else {
	
						insertSql = "INSERT INTO CAL_MEDICION (MEDICION_ID, NUM_SUM_MEDICION, TEN_ENTREGADA, PK_CAM_MEDICION_ID, PK_EST_MEDICION_ID, PK_TIP_PUNTO_ID, SECUENCIA, COMPEMSABLE, UBI_SUM_MEDICION, ESTADO) VALUES (MEDICIONSEQ.NEXTVAL,'" + 
								numSumMedicion + "',0 ,"+ String.valueOf(id) + ", 1," + String.valueOf(tipPuntoId) + ",0 ,'PROGRAMADA','" + ubicacion + "','1')";
	
					}
					
					resultado = sql.ejecuta(insertSql);
					
					//System.out.println(insertSql + " - " + resultado);
					
					if(resultado != null){
						mensaje = mensaje + "," + numSumMedicion;
					}
					
					updateSql = "UPDATE CAL_MEDICION SET PK_PAR_MEDIDO_ID = (SELECT PAR_MEDIDO_ID FROM CAL_PARAMETRO_MEDIDO WHERE COD_PAR_MEDIDO = '" + parMedido + "'), FAC_COR_TENSION = 1, FAC_COR_CORRIENTE = 1, PK_TIP_ALIMENTACION_ID = (SELECT TIP_ALIMENTACION_ID FROM CAL_TIPO_ALIMENTACION WHERE COD_TIP_ALIMENTACION = '" + tipAlim + "'), SECUENCIA = 0, PK_TIP_TRABAJO_ID = (SELECT TIP_TRABAJO_ID FROM CAL_TIPO_TRABAJO WHERE COD_TIP_TRABAJO = 'OT') WHERE NUM_SUM_MEDICION = '" + numSumMedicion + "' AND PK_CAM_MEDICION_ID = " + String.valueOf(id);
					
					resultado = sql.ejecuta(updateSql);
					
					if(parMedido.equals("TP") || parMedido.equals("FA") ) {
						updateSql = "UPDATE CAL_MEDICION SET PARAMETRO_PER = 'FA', PK_PER_EST_MEDICION_ID = (SELECT EST_MEDICION_ID FROM CAL_ESTADO_MEDICION WHERE COD_EST_MEDICION = 'G'), PER_MENSAJE = (SELECT DESCRIPCION FROM CAL_ESTADO_MEDICION WHERE COD_EST_MEDICION = 'G') WHERE NUM_SUM_MEDICION = '" + numSumMedicion + "' AND PK_CAM_MEDICION_ID = " + String.valueOf(id);
						
						resultado = sql.ejecuta(updateSql);
					}
					
					updateSql = "UPDATE CAL_MEDICION SET PK_TIP_TRABAJO_ID = (SELECT TIP_TRABAJO_ID FROM CAL_TIPO_TRABAJO WHERE COD_TIP_TRABAJO = 'RT') WHERE PK_TIP_PUNTO_ID = (SELECT TIP_PUNTO_ID FROM CAL_TIPO_PUNTO WHERE COD_TIP_PUNTO = 'X') AND NUM_SUM_MEDICION = '" + numSumMedicion + "' AND PK_CAM_MEDICION_ID = " + String.valueOf(id);
					
					resultado = sql.ejecuta(updateSql);
					
					if(!parMedido.equals("FA")){
					
						updateSql = "MERGE " + cl +
									"INTO   CAL_MEDICION " + cl +
									"USING  SUMINISTRO_ICIS_IGEA " + cl +
									"ON (CAL_MEDICION.NUM_SUM_MEDICION = TRIM(TO_CHAR(SUMINISTRO_ICIS_IGEA.SUMINISTRO,'999999999999999')) AND CAL_MEDICION.NUM_SUM_MEDICION = '"+ numSumMedicion +"' AND CAL_MEDICION.PK_CAM_MEDICION_ID = " + String.valueOf(id) + ") " + cl +
									"WHEN MATCHED THEN " + cl +
									"UPDATE " + cl +
									"SET    NOM_USUARIO = SUBSTR(SUMINISTRO_ICIS_IGEA.NOMBRE,1,60), " + cl +
									       "TEL_USUARIO = SUBSTR(SUMINISTRO_ICIS_IGEA.GENERAL_PHONE,1,9), " +  cl +
									       "DESCRIPCION = SUMINISTRO_ICIS_IGEA.ADDITIONAL_INFO, " + cl +
									       "FAC_COR_TENSION = 1, " + cl +
									       "FAC_COR_CORRIENTE = 1, " + cl +
									       "TEN_ENTREGADA  = TO_NUMBER(SUMINISTRO_ICIS_IGEA.SUPPLY_VOLTAGE, '999999')," + cl +
									       "DIR_USUARIO = SUBSTR(DIR_SUMINISTRO,1,128)," + cl +
									       "TIP_SUMINISTRO = CALIDAD_PACKAGE.FN_TIPO_SUMINISTRO(TRIM(CUSTOMER_GROUP))";
						
						resultado = sql.ejecuta(updateSql);
						
						updateSql = "MERGE " + cl +
								"INTO   CAL_MEDICION " + cl +
								"USING  SUMINISTRO_IGEA_ICIS " + cl +
								"ON (CAL_MEDICION.NUM_SUM_MEDICION = SUMINISTRO_IGEA_ICIS.SUMINISTRO AND CAL_MEDICION.NUM_SUM_MEDICION = '"+ numSumMedicion +"' AND CAL_MEDICION.PK_CAM_MEDICION_ID = " + String.valueOf(id) + ") " + cl +
								"WHEN MATCHED THEN " + cl +
								"UPDATE " + cl +
								"SET    CODSET = SUBSTR(SUMINISTRO_IGEA_ICIS.CT,1,7), " + cl +
								       "CODALIMENTADOR = SUBSTR(SUMINISTRO_IGEA_ICIS.LINE,1,7), " +  cl +
								       "CODSED = SUBSTR(SUMINISTRO_IGEA_ICIS.SED,1,7), " + cl +
								       "OPC_TARIFARIA = SUBSTR(TARIFA,1,5), " + cl + 
								       "FAC_COR_TENSION = 1, " + cl +
									   "FAC_COR_CORRIENTE = 1, " + cl +
									   "SALIDABT = SALIDA_BT, " + cl +
								       "PK_LOCALIDAD_ID = (SELECT L.LOCALIDAD_ID FROM CAL_LOCALIDAD L WHERE L.COD_LOCALIDAD = SUBSTR(SUMINISTRO_IGEA_ICIS.LOCALIDAD_SALIDA_BT,1,4)), " + cl +
								       "TIP_SERVICIO = CALIDAD_PACKAGE.FN_TIPO_SERVICIO(SUBSTR(SUMINISTRO_IGEA_ICIS.LOCALIDAD_SALIDA_BT,1,4)), " + cl +
								       "DIRECCION_SED = SUMINISTRO_IGEA_ICIS.ADDRESS_SED, " + cl +
								       "TENSION_SED = TO_CHAR(SUMINISTRO_IGEA_ICIS.TEN_PRIMARIO_SED) ||'/'|| TO_CHAR(SUMINISTRO_IGEA_ICIS.TEN_SECUNDARIO_SED) ";
					
						resultado = sql.ejecuta(updateSql);
						
						/*
						updateSql =  "UPDATE CAL_MEDICION SET PK_TIP_MEDICION_ID = (SELECT TIP_MEDICION_ID FROM CAL_TIPO_MEDICION WHERE COD_TIP_MEDICION = '4'), " + cl +
							  "CODSET = SUBSTR((SELECT SI.CT FROM SUMINISTRO_IGEA_ICIS SI WHERE SI.SED = NUM_SUM_MED AND ROWNUM = 1),1,7), " + cl +
							  "CODALIMENTADOR = SUBSTR((SELECT SI.LINE FROM SUMINISTRO_IGEA_ICIS SI WHERE SI.SED = NUM_SUM_MED AND ROWNUM = 1),1,7), " + cl +
							  "CODSED = SUBSTR(NUM_SUM_MEDICION,1,7), " + cl +
							  "FAC_COR_TENSION = 1, " + cl +
							  "FAC_COR_CORRIENTE = 1, " + cl +
							  "PK_LOCALIDAD_ID = (SELECT L.LOCALIDAD_ID FROM CAL_LOCALIDAD L WHERE L.COD_LOCALIDAD = SUBSTR((SELECT SI.LOCALIDAD_SED FROM SUMINISTRO_IGEA_ICIS SI WHERE SI.SED = NUM_SUM_MED AND ROWNUM = 1),1,4)), " + cl +
							  "TIP_SERVICIO = CALIDAD_PACKAGE.FN_TIPO_SERVICIO(SUBSTR((SELECT SI.LOCALIDAD_SED FROM SUMINISTRO_IGEA_ICIS SI WHERE SI.SED = NUM_SUM_MED AND ROWNUM = 1),1,4)) " + cl +
							  "WHERE NUM_SUM_MEDICION = '"+ numSumMedicion +"' AND PK_CAM_MEDICION_ID = " + String.valueOf(id) + " AND PK_PAR_MEDIDO_ID = (SELECT PAR_MEDIDO_ID FROM CAL_PARAMETRO_MEDIDO WHERE COD_PAR_MEDIDO IN ('FL','AR','FA'))";
						
						resultado = sql.ejecuta(updateSql);
						*/ 
						
						updateSql = "UPDATE CAL_MEDICION SET PK_TIP_MEDICION_ID = (SELECT TIP_MEDICION_ID FROM CAL_TIPO_MEDICION WHERE COD_TIP_MEDICION = '1') WHERE TIP_SUMINISTRO IN ('MA','AT','MT') AND NUM_SUM_MEDICION = '" + numSumMedicion + "' AND PK_CAM_MEDICION_ID = " + String.valueOf(id);
						
						resultado = sql.ejecuta(updateSql);
						
						updateSql = "UPDATE CAL_MEDICION SET PK_TIP_MEDICION_ID = (SELECT TIP_MEDICION_ID FROM CAL_TIPO_MEDICION WHERE COD_TIP_MEDICION = '2') WHERE TIP_SUMINISTRO IN ('BT') AND NUM_SUM_MEDICION = '" + numSumMedicion + "' AND PK_CAM_MEDICION_ID = " + String.valueOf(id);
						
						resultado = sql.ejecuta(updateSql);
						
						if(parMedido.equals("TP")) {
							updateSql = "UPDATE CAL_MEDICION SET PARAMETRO_PER = 'FA' " + 
										"WHERE NUM_SUM_MEDICION = '" + numSumMedicion + "' AND PK_CAM_MEDICION_ID = " + String.valueOf(id);
							
							resultado = sql.ejecuta(updateSql);
						}
						
					} else {
						
						
						updateSql = "UPDATE CAL_MEDICION SET PK_TIP_MEDICION_ID = (SELECT TIP_MEDICION_ID FROM CAL_TIPO_MEDICION WHERE COD_TIP_MEDICION = '4'), " +
									"CODSET = SUBSTR((SELECT SI.CT FROM SUMINISTRO_IGEA_ICIS SI WHERE SI.SED = '" + numSumMedicion + "' AND ROWNUM = 1),1,7), " + cl +
									"CODALIMENTADOR = SUBSTR((SELECT SI.LINE FROM SUMINISTRO_IGEA_ICIS SI WHERE SI.SED = '" + numSumMedicion + "' AND ROWNUM = 1),1,7), " +  cl +
									"CODSED = SUBSTR(NUM_SUM_MEDICION,1,7), " + cl +
									"FAC_COR_TENSION = 1, " + cl +
									"FAC_COR_CORRIENTE = 1, " + cl +
									"TIP_SUMINISTRO = 'SED', " + cl +
									"PARAMETRO_PER = 'FA'," + cl +
									"PK_LOCALIDAD_ID = (SELECT L.LOCALIDAD_ID FROM CAL_LOCALIDAD L WHERE L.COD_LOCALIDAD = SUBSTR((SELECT SI.LOCALIDAD_SED FROM SUMINISTRO_IGEA_ICIS SI WHERE SI.SED = '" + numSumMedicion + "' AND ROWNUM = 1),1,4))," + cl +
									"TIP_SERVICIO = CALIDAD_PACKAGE.FN_TIPO_SERVICIO(SUBSTR((SELECT SI.LOCALIDAD_SED FROM SUMINISTRO_IGEA_ICIS SI WHERE SI.SED = '" + numSumMedicion + "' AND ROWNUM = 1),1,4)), " +
								    "DIRECCION_SED = (SELECT SI.ADDRESS_SED FROM SUMINISTRO_IGEA_ICIS SI WHERE SI.SED = '" + numSumMedicion + "' AND ROWNUM = 1), " + 
								    "TENSION_SED = (SELECT TO_CHAR(SI.TEN_PRIMARIO_SED) ||'/'|| TO_CHAR(SI.TEN_SECUNDARIO_SED) FROM SUMINISTRO_IGEA_ICIS SI WHERE SI.SED = '" + numSumMedicion + "' AND ROWNUM = 1)	" +								
									"WHERE NUM_SUM_MEDICION = '" + numSumMedicion + "' AND PK_CAM_MEDICION_ID = " + String.valueOf(id);
						
						resultado = sql.ejecuta(updateSql);
						

					}
					
					
				} else {
					mensaje = mensaje + "," + numSumMedicion;
				}
			}
		}
		
		
		ConectaDb db = new ConectaDb("CALIDAD");
		Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        try {
        	cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.GENERAR_SECUENCIA(?)}");
		   	cstmt.setLong(1, id);
			
            int ctos = cstmt.executeUpdate();;
            cstmt.close(); 

            if (ctos == 0) {
                result = "0 filas afectadas";
            }

        } catch (SQLException e) {
            result = e.getMessage();
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
                result = e.getMessage();
            }
        }
		System.out.print(result); 
		
		
	}
	
	public String insertarIntervaloTension (String tabla, String campos, List<String[]> data, long id, String fmtFecha, long nFecha, boolean potencia, int opcion, boolean ene_acumulada) {
		
		String insertSql;
		String updateSql;
		String deleteSql;
		String mensaje = null;
		Sql sql = new Sql("CALIDAD");
		String resultado = null;
		
		deleteSql = "DELETE FROM CAL_INTERVALO_TENSION WHERE PK_MEDICION_ID = " + String.valueOf(id);
		
		resultado = sql.ejecuta(deleteSql);
		
		updateSql = "UPDATE CAL_MEDICION SET PK_EST_MEDICION_ID = NULL WHERE MEDICION_ID = " + String.valueOf(id);
		
		resultado = sql.ejecuta(deleteSql);
		
		boolean ejecuta;
		for(String[] fila:data){
			ejecuta = true;
			insertSql = "INSERT INTO CAL_INTERVALO_TENSION (INT_TENSION_ID," + campos + ", PK_MEDICION_ID, ESTADO) VALUES (INTERVALOTENSIONSEQ.NEXTVAL, TO_TIMESTAMP('"; 
			
			if(nFecha == 1){
				if(fila[0].trim().contains("a.m.") || fila[0].trim().contains("A.M.")) {
					fila[0] = fila[0].trim();
					fila[0] = fila[0].substring(0,fila[0].length() - 5);
					fila[0] = fila[0].trim() + " AM";
				}
				
				if(fila[0].trim().contains("p.m.") || fila[0].trim().contains("P.M.")) {
					fila[0] = fila[0].trim();
					fila[0] = fila[0].substring(0,fila[0].length() - 5);
					fila[0] = fila[0].trim() + " PM";
				}				
				
				
				insertSql = insertSql + fila[0].trim() + "','" + fmtFecha + "'),";
				for(int i=1;i<fila.length;i++) {
					/*
					if(fila[i]==null) {
						ejecuta = false;
						break;
					}else{
						if(fila[i].trim().equals("")){
							ejecuta = false;
							break;
						}
					}
					*/
					
					if(fila[i]==null) {
						fila[i] = "0";
					}else{
						if(fila[i].trim().equals("")){
							fila[i] = "0";
						}
					}
					
					insertSql = insertSql + fila[i].trim() + ",";
				}
				insertSql = insertSql + String.valueOf(id) + ", '1')";
				
			}else{
				if(fila[1].trim().contains("a.m.") || fila[1].trim().contains("A.M.")) {
					fila[1] = fila[1].trim();
					fila[1] = fila[1].substring(0,fila[1].length() - 5);
					fila[1] = fila[1].trim() + " AM";
				}
				
				if(fila[1].trim().contains("p.m.") || fila[1].trim().contains("P.M.")) {
					fila[1] = fila[1].trim();
					fila[1] = fila[1].substring(0,fila[1].length() - 5);
					fila[1] = fila[1].trim() + " PM";
				}				
				
				insertSql = insertSql + fila[0].trim() + " " + fila[1].trim()  +  "','" + fmtFecha + "'),";
				for(int i=2;i<fila.length;i++) {
					/*
					if(fila[i]==null) {
						ejecuta = false;
						break;
					}else{
						if(fila[i].trim().equals("")){
							ejecuta = false;
							break;
						}
					}
					*/
					
					if(fila[i]==null) {
						fila[i] = "0";
					}else{
						if(fila[i].trim().equals("")){
							fila[i] = "0";
						}
					}
					
					insertSql = insertSql + fila[i].trim() + ",";
				}
				insertSql = insertSql + String.valueOf(id) + ", '1')";
			}
			
			if(ejecuta){
				resultado = sql.ejecuta(insertSql);
				//System.out.print(insertSql + " RESULTADO: " + resultado + "\n");
			}
			
			if(resultado != null){
				mensaje = mensaje + "\n" + fila[0];
				
				if(resultado.equals("FALLO")){
					return resultado;
				}
			}		
				
		}
		
		if(mensaje == null){
			updateSql = "UPDATE CAL_MEDICION " + 
						"SET PK_EST_MEDICION_ID = (SELECT EST_MEDICION_ID FROM CAL_ESTADO_MEDICION WHERE COD_EST_MEDICION = 'CA'), " +
						"COMPEMSABLE = (SELECT DESCRIPCION FROM CAL_ESTADO_MEDICION WHERE COD_EST_MEDICION = 'CA') " +
						"WHERE MEDICION_ID = " + String.valueOf(id);
			
			resultado = sql.ejecuta(updateSql);
			
			if(ene_acumulada) {
				String result = null;
				ConectaDb db = new ConectaDb("CALIDAD");
				Connection cn = db.getConnection();
		        CallableStatement cstmt = null;
				try {			        
			        cstmt = cn.prepareCall("{CALL CALIDAD_PACKAGE.DESAGREGAR_ENERGIA(?)}");  
					cstmt.setLong(1, id);
					
					int ctos = cstmt.executeUpdate();
		            cstmt.close(); 
		    	} catch (SQLException e) {
		            result = e.getMessage();
		            
		            e.printStackTrace();

		        } finally {
		            try {
		                cn.close();
		            } catch (SQLException e) {
		                result = e.getMessage();
		            }
		        }		            
			}
			
			if(opcion == 3) {
				updateSql = "UPDATE CAL_INTERVALO_TENSION SET ENERGIA_R = ROUND(ENE_INTERVALO * (1/3),4), ENERGIA_S = ROUND(ENE_INTERVALO * (1/3),4), ENERGIA_T = ROUND(ENE_INTERVALO * (1/3),4) WHERE PK_MEDICION_ID = " + String.valueOf(id);
				
				resultado = sql.ejecuta(updateSql);
			}
			
			if(potencia){
				updateSql = "UPDATE CAL_INTERVALO_TENSION SET ENERGIA_R = ROUND(ENERGIA_R * (1/4),4), ENERGIA_S = ROUND(ENERGIA_S * (1/4),4), ENERGIA_T = ROUND(ENERGIA_T * (1/4),4), ENE_INTERVALO = ROUND(ENE_INTERVALO * (1/4),4) WHERE PK_MEDICION_ID = " + String.valueOf(id);
				
				resultado = sql.ejecuta(updateSql);
				
			}
			
		}
		return mensaje;
	}
	
	public void insertarCronograma (String tabla, String campos, List<String[]> data, long id, String fmtFecha, long tipo){
		String insertSql;
		String selectSql;
		String mensaje = "";
		Sql sql = new Sql("CALIDAD");
		Object[] objFila;
		String resultado;
		String fecha;
		BigDecimal medicionId;
		
		for(String[] fila:data){			
			String sumi[];
			if(tipo==1){
				sumi = new String[1];
				sumi[0] = fila[0].trim();
				fecha  = (String) fila[1];
			}else {
				if(campos.equals("2")) {
					sumi = new String[2];
					sumi[0] = fila[0].trim();
					sumi[1] = fila[1].trim();
					fecha  = (String) fila[2];
				} else {
					sumi = new String[1];
					sumi[0] = fila[0].trim();
					fecha  = (String) fila[1];
				}
			}
			
			for (String numSumMedicion : sumi) {			
				selectSql = "SELECT MEDICION_ID FROM CAL_MEDICION WHERE NUM_SUM_MEDICION = '" + numSumMedicion + "' AND ESTADO = '1' AND PK_CAM_MEDICION_ID = " + String.valueOf(id);
				
				objFila = sql.getFila(selectSql);
				
				if(objFila != null) {
					
					medicionId = (BigDecimal) objFila[0];
					
					String deleteSql = "DELETE FROM CAL_CRONOGRAMA_MEDICION WHERE PK_MEDICION_ID = " + String.valueOf(medicionId);
					
					resultado = sql.ejecuta(deleteSql);	
					
					insertSql = "INSERT INTO CAL_CRONOGRAMA_MEDICION (CRO_MEDICION_ID, FEC_PLANIFICADA, PK_MEDICION_ID, ESTADO) VALUES (CronogramaMedicionSeq.NEXTVAL, TO_DATE('" + fecha + "','" + fmtFecha + "'), "+ String.valueOf(medicionId) + ", '1')";
											
					resultado = sql.ejecuta(insertSql);		
					System.out.println(insertSql + " " + resultado);
								
				} else {
					mensaje = mensaje + "," + numSumMedicion;
				}
			}
		}
	}
	
	public String insertarIntervaloPerturbacion (String tabla, String campos, List<String[]> data, long id, String fmtFecha, long nFecha, boolean actualizar, boolean potencia, int opcion, boolean ene_acumulada, boolean wattios) {
		
		String insertSql;
		String updateSql;
		String deleteSql;
		String mensaje = null;
		Sql sql = new Sql("CALIDAD");
		String resultado = null;
		String cl = System.getProperty("line.separator");

		String nombresCampos[];
		String where;
		
		nombresCampos = campos.split(",");
		
		if(!actualizar){
			deleteSql = "DELETE FROM CAL_INTERVALO_PERTURBACION WHERE PK_MEDICION_ID = " + String.valueOf(id);
			
			resultado = sql.ejecuta(deleteSql);
			
			updateSql = "UPDATE CAL_MEDICION SET PK_PER_EST_MEDICION_ID = NULL WHERE MEDICION_ID = " + String.valueOf(id);
			
			resultado = sql.ejecuta(updateSql);
		}
		
		boolean ejecuta;
		for(String[] fila:data){
			ejecuta = true;
			if(!actualizar){
				insertSql = "INSERT INTO CAL_INTERVALO_PERTURBACION (INT_PERTURBACION_ID," + campos + ", PK_MEDICION_ID, ESTADO) VALUES (INTERVALOPERTURBACIONSEQ.NEXTVAL, TO_TIMESTAMP('"; 
			}else {
				insertSql = "UPDATE CAL_INTERVALO_PERTURBACION SET " + cl;
			}
				
			if(nFecha == 1){
				if(!actualizar){
					
					if(fila[0].trim().contains("a.m.") || fila[0].trim().contains("A.M.")) {
						fila[0] = fila[0].trim();
						fila[0] = fila[0].substring(0,fila[0].length() - 5);
						fila[0] = fila[0].trim() + " AM";
					}
					
					if(fila[0].trim().contains("p.m.") || fila[0].trim().contains("P.M.")) {
						fila[0] = fila[0].trim();
						fila[0] = fila[0].substring(0,fila[0].length() - 5);
						fila[0] = fila[0].trim() + " PM";
					}
					
					insertSql = insertSql + fila[0].trim() + "','" + fmtFecha + "'),";
					for(int i=1;i<fila.length;i++) {
						
						if(fila[i]==null) {
							fila[i] = "0";
						}else{
							if(fila[i].trim().equals("")){
								fila[i] = "0";
							}
						}
						
						insertSql = insertSql + fila[i].trim() + ",";
					}
					insertSql = insertSql + String.valueOf(id) + ", '1')";
				}else {
					if(fila[0].trim().contains("a.m.") || fila[0].trim().contains("A.M.")) {
						fila[0] = fila[0].trim();
						fila[0] = fila[0].substring(0,fila[0].length() - 5);
						fila[0] = fila[0].trim() + " AM";
					}
					
					if(fila[0].trim().contains("p.m.") || fila[0].trim().contains("P.M.")) {
						fila[0] = fila[0].trim();
						fila[0] = fila[0].substring(0,fila[0].length() - 5);
						fila[0] = fila[0].trim() + " PM";
					}					
					
					where = " WHERE INTERVALO = TO_TIMESTAMP('" + fila[0].trim() + "','" + fmtFecha + "') AND PK_MEDICION_ID = " + String.valueOf(id);
					for(int i=1;i<fila.length;i++) {
						if(fila[i]==null) {
							fila[i] = "0";
						}else{
							if(fila[i].trim().equals("")){
								fila[i] = "0";
							}
						}
						
						insertSql = insertSql + nombresCampos[i] + " = " + fila[i].trim() + ",";
					}
					
					insertSql = insertSql.substring(0, insertSql.length() - 1) ;
					insertSql = insertSql + where;
				}
				
			}else{
				if(!actualizar){
					if(fila[1].trim().contains("a.m.") || fila[1].trim().contains("A.M.")) {
						fila[1] = fila[1].trim();
						fila[1] = fila[1].substring(0,fila[1].length() - 5);
						fila[1] = fila[1].trim() + " AM";
					}
					
					if(fila[1].trim().contains("p.m.") || fila[1].trim().contains("P.M.")) {
						fila[1] = fila[1].trim();
						fila[1] = fila[1].substring(0,fila[1].length() - 5);
						fila[1] = fila[1].trim() + " PM";
					}					
					
					insertSql = insertSql + fila[0].trim() + " " + fila[1].trim()  +  "','" + fmtFecha + "'),";
					for(int i=2;i<fila.length;i++) {
						
						if(fila[i]==null) {
							fila[i] = "0";
						}else{
							if(fila[i].trim().equals("")){
								fila[i] = "0";
							}
						}
						
						insertSql = insertSql + fila[i].trim() + ",";
					}
					insertSql = insertSql + String.valueOf(id) + ", '1')";
				}else {
					if(fila[1].trim().contains("a.m.") || fila[1].trim().contains("A.M.")) {
						fila[1] = fila[1].trim();
						fila[1] = fila[1].substring(0,fila[1].length() - 5);
						fila[1] = fila[1].trim() + " AM";
					}
					
					if(fila[1].trim().contains("p.m.") || fila[1].trim().contains("P.M.")) {
						fila[1] = fila[1].trim();
						fila[1] = fila[1].substring(0,fila[1].length() - 5);
						fila[1] = fila[1].trim() + " PM";
					}					
					
					where = " WHERE INTERVALO = TO_TIMESTAMP('" + fila[0].trim() + " " + fila[1].trim() + "','" + fmtFecha + "') AND PK_MEDICION_ID = " + String.valueOf(id);
					for(int i=2;i<fila.length;i++) {
						if(fila[i]==null) {
							fila[i] = "0";
						}else{
							if(fila[i].trim().equals("")){
								fila[i] = "0";
							}
						}
						
						insertSql = insertSql + nombresCampos[i - 1] + " = " + fila[i].trim() + ",";
					}
					
					insertSql = insertSql.substring(0, insertSql.length() - 1) ;
					insertSql = insertSql + where;
				}
			}
			
			if(ejecuta){
				resultado = sql.ejecuta(insertSql);
				//System.out.print(insertSql + " RESULTADO: " + resultado + "\n");
			}
			
			if(resultado != null){
				mensaje = mensaje + "\n" + fila[0];
				
				if(resultado.equals("FALLO")){
					return resultado;
				}
			}		
				
		}
		
		if(mensaje == null){
			if(!actualizar){
				updateSql = "UPDATE CAL_MEDICION SET PK_PER_EST_MEDICION_ID = (SELECT EST_MEDICION_ID FROM CAL_ESTADO_MEDICION WHERE COD_EST_MEDICION = 'CA'), " + 
							"PER_MENSAJE = (SELECT DESCRIPCION FROM CAL_ESTADO_MEDICION WHERE COD_EST_MEDICION = 'CA') " + 
							"WHERE MEDICION_ID = " + String.valueOf(id);
				
				resultado = sql.ejecuta(updateSql);
				
			}else {
				
			}
			
			if(ene_acumulada) {
				String result = null;
				ConectaDb db = new ConectaDb("CALIDAD");
				Connection cn = db.getConnection();
		        CallableStatement cstmt = null;
				try {			        
			        cstmt = cn.prepareCall("{CALL CAL_PERTURBACION_PACKAGE.DESAGREGAR_ENERGIA_PER(?)}");  
					cstmt.setLong(1, id);
					
					int ctos = cstmt.executeUpdate();
		            cstmt.close(); 
		    	} catch (SQLException e) {
		            result = e.getMessage();
		            
		            e.printStackTrace();

		        } finally {
		            try {
		                cn.close();
		            } catch (SQLException e) {
		                result = e.getMessage();
		            }
		        }		            
			}			
			
			if(opcion == 15 || opcion == 16 || opcion == 17 || opcion == 18){
				updateSql = "UPDATE CAL_INTERVALO_PERTURBACION P " + 
							"SET P.ENERGIA_R = ENE_INTERVALO * (1/3), P.ENERGIA_S = ENE_INTERVALO * (1/3), P.ENERGIA_T = ENE_INTERVALO * (1/3) " +  
							"WHERE (SELECT COD_TIP_ALIMENTACION FROM CAL_TIPO_ALIMENTACION WHERE TIP_ALIMENTACION_ID = (SELECT PK_TIP_ALIMENTACION_ID FROM CAL_MEDICION WHERE MEDICION_ID = P.PK_MEDICION_ID)) <> 'MO' AND P.PK_MEDICION_ID = " + String.valueOf(id); 
/*
				"UPDATE CAL_INTERVALO_PERTURBACION SET ENERGIA_R = ENE_INTERVALO * (1/3), ENERGIA_S = ENE_INTERVALO * (1/3), ENERGIA_T = ENE_INTERVALO * (1/3) " + 
							"WHERE PK_MEDICION_ID = " + String.valueOf(id);
				
				resultado = sql.ejecuta(updateSql);
*/				
			}
			
			if(potencia){
				updateSql = "UPDATE CAL_INTERVALO_PERTURBACION SET ENERGIA_R = ENERGIA_R * (1/6), ENERGIA_S = ENERGIA_S * (1/6), ENERGIA_T = ENERGIA_T * (1/6), ENE_INTERVALO = ENE_INTERVALO * (1/6) WHERE PK_MEDICION_ID = " + String.valueOf(id);
				
				resultado = sql.ejecuta(updateSql);
				
			}
			
			if(wattios){
				updateSql = "UPDATE CAL_INTERVALO_PERTURBACION SET ENERGIA_R = ENERGIA_R * (1/1000), ENERGIA_S = ENERGIA_S * (1/1000), ENERGIA_T = ENERGIA_T * (1/1000), ENE_INTERVALO = ENE_INTERVALO * (1/1000) WHERE PK_MEDICION_ID = " + String.valueOf(id);
				
				resultado = sql.ejecuta(updateSql);
				
			}			
			
			
		}
		return mensaje;
	}
	
	public String actualizarMedicionAp(String tabla, String campos, List<String[]> data, long id) {
		String mensaje = null;
		String updateSql = null;
		Sql sql = new Sql("CALIDAD");
		String resultado = null;
		String cl = System.getProperty("line.separator");
		String nombresCampos[];
		String where;
		
		nombresCampos = campos.split(",");		

		for(String[] fila:data){
			updateSql = "UPDATE CAL_MEDICION_AP SET NUM_LUM_POR_KM = " + fila[13].trim() + ", SUP_APARENTE = " + fila[14].trim() + ", INT_LUMINOSA = " + fila[15].trim() + ", REL_LUMINOSA = " + fila[16].trim() + " WHERE MEDICION_AP_ID = " + String.valueOf(id);
			
			break;
		}
		
		resultado = sql.ejecuta(updateSql);
		
		return mensaje;
	}
	
	public String actualizarMedicionesAp(String tabla, String campos, List<String[]> data, long id) {
		String mensaje = null;
		String updateSql = null;
		Sql sql = new Sql("CALIDAD");
		String resultado = null;
		String cl = System.getProperty("line.separator");
		String nombresCampos[];
		String where;
		long medicionApIdAux;
		long medicionApIdAuxAnt;
		MedicionApService medicionApService = new MedicionApService();
		BigDecimal nroVano;
		String strNroVano;
		MedicionAp medicionApAux;
		
		nombresCampos = campos.split(",");		
		
		medicionApIdAuxAnt = 0;
		for(String[] fila:data){
			strNroVano = fila[1].trim();
			nroVano = new BigDecimal(Long.valueOf(strNroVano));
			
			medicionApAux = medicionApService.ReadByNroVano(id, nroVano);
			
			if(medicionApAux == null){
				continue;
			}
			
			medicionApIdAux = medicionApAux.getMedicionApId();
			
			if(medicionApIdAux != medicionApIdAuxAnt){
				updateSql = "UPDATE CAL_MEDICION_AP SET NUM_LUM_POR_KM = " + fila[15].trim() + ", SUP_APARENTE = " + fila[16].trim() + ", INT_LUMINOSA = " + fila[17].trim() + ", REL_LUMINOSA = " + fila[18].trim() + " WHERE MEDICION_AP_ID = " + String.valueOf(medicionApIdAux);
				
				resultado = sql.ejecuta(updateSql);
			}
			
			medicionApIdAuxAnt = medicionApIdAux;
			//break;
		}
		
		
		
		return mensaje;
	}	
	
	public String insertarPuntoAp(String tabla, String campos, List<String[]> data, long id) {
		String mensaje = null;
		String insertSql = null;
		String deleteSql = null;
		String updateSql = null;
		Sql sql = new Sql("CALIDAD");
		String resultado = null;
		String cl = System.getProperty("line.separator");
		String nombresCampos[];
		String where;
		
		deleteSql = "DELETE FROM CAL_PUNTO_AP WHERE PK_MEDICION_AP_ID = " + String.valueOf(id);
		resultado = sql.ejecuta(deleteSql);
		
		nombresCampos = campos.split(",");		

		campos = "";
		for (int i=0;i<nombresCampos.length;i++) {
			
			if(i < 12) {
				campos = campos + nombresCampos[i] + ",";
			}
			
			if(i == 12) {
				campos = campos + nombresCampos[i];
				break;
			}
		}
		
		for(String[] fila:data){
			insertSql = "INSERT INTO CAL_PUNTO_AP (PUNTO_AP_ID," + campos + ", PK_MEDICION_AP_ID, ESTADO) VALUES (PUNTOAPSEQ.NEXTVAL, ";

			for(int i=0;i<fila.length;i++) {
				if(fila[i]==null) {
					fila[i] = "0";
				}else{
					if(fila[i].trim().equals("")){
						fila[i] = "0";
					}
				}
				
				if(i == 0 || i == 1 || i == 7) {
					fila[i] = "'" + fila[i] + "'";
				}
				
				insertSql = insertSql + fila[i].trim() + ",";
				
				if(i == 12){
					break;
				}
			}
			
			insertSql = insertSql + String.valueOf(id) + ", '1')";
			
			resultado = sql.ejecuta(insertSql);
			
			if(resultado != null){
				mensaje = mensaje + "\n" + fila[0];
				
				if(resultado.equals("FALLO")){
					return resultado;
				}
			}					
		}
		
		if(mensaje == null){
			updateSql = "UPDATE CAL_MEDICION_AP SET CALIDAD = 'C' WHERE MEDICION_AP_ID = " + String.valueOf(id);
			
			resultado = sql.ejecuta(updateSql);
			
		}
				
		return mensaje;
	}
	
	public String insertarPuntosAp(String tabla, String campos, List<String[]> data, long id) {
		String mensaje = null;
		String insertSql = null;
		String deleteSql = null;
		String updateSql = null;
		Sql sql = new Sql("CALIDAD");
		String resultado = null;
		String cl = System.getProperty("line.separator");
		String nombresCampos[];
		String where;
		MedicionAp medicionApAux;
		long medicionApId;
		long medicionApIdAnt;
		MedicionApService medicioApService = new MedicionApService();
		
		nombresCampos = campos.split(",");		

		campos = "";
		for (int i=2;i<nombresCampos.length;i++) {
			
			if(i < 14) {
				campos = campos + nombresCampos[i] + ",";
			}
			
			if(i == 14) {
				campos = campos + nombresCampos[i];
				break;
			}
		}
		
		medicionApIdAnt = 0;
		for(String[] fila:data){
			
			medicionApAux = medicioApService.ReadByNroVano(id, new BigDecimal(Long.valueOf(fila[1].trim())));
						
			if(medicionApAux == null) {
				continue;
			}
			
			medicionApId = medicionApAux.getMedicionApId();
			
			if(medicionApId != medicionApIdAnt) {
			
				deleteSql = "DELETE FROM CAL_PUNTO_AP WHERE PK_MEDICION_AP_ID = " + String.valueOf(medicionApId);
				resultado = sql.ejecuta(deleteSql);
			}
			
			insertSql = "INSERT INTO CAL_PUNTO_AP (PUNTO_AP_ID," + campos + ", PK_MEDICION_AP_ID, ESTADO) VALUES (PUNTOAPSEQ.NEXTVAL, ";

			for(int i=2;i<fila.length;i++) {
				if(fila[i]==null) {
					fila[i] = "0";
				}else{
					if(fila[i].trim().equals("")){
						fila[i] = "0";
					}
				}
				
				if(i == 2 || i == 3 || i == 9) {
					fila[i] = "'" + fila[i] + "'";
				}
				
				insertSql = insertSql + fila[i].trim() + ",";
				
				if(i == 14){
					break;
				}
			}
			
			insertSql = insertSql + String.valueOf(medicionApId) + ", '1')";
			
			resultado = sql.ejecuta(insertSql);
			
			if(resultado != null){
				mensaje = mensaje + "\n" + fila[0];
				
				if(resultado.equals("FALLO")){
					return resultado;
				}
			}		
			
			if(mensaje == null){
				if(medicionApId != medicionApIdAnt){
					updateSql = "UPDATE CAL_MEDICION_AP SET CALIDAD = 'C' WHERE MEDICION_AP_ID = " + String.valueOf(medicionApId);
					
					resultado = sql.ejecuta(updateSql);
				}
			}
			
			medicionApIdAnt = medicionApId;
		}
		

				
		return mensaje;
	}	
}