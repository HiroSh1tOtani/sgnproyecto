package pe.com.indra.calidad.util;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author parainformaticos.com
 */
public class Sql {

    ConectaDb db = null;

    public Sql(String dataBase) {
        db = new ConectaDb(dataBase);
    }

    // ejecuta INS, DEL y UPD
    public String ejecuta(
            String sql // viene INSERT, DELETE o UPDATE
            ) {

        String result = null;
        Connection cn = db.getConnection();

        try {
            Statement st = cn.createStatement();
            int ctos = st.executeUpdate(sql);
            
            //System.out.println(sql);
            if (ctos == 0) {
                result = "0 filas afectadas";
            }

        } catch (SQLException e) {
            result = "FALLO";
            
            e.printStackTrace();
            System.out.println(sql);
            
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
            	e.printStackTrace();
            	
                result = e.getMessage();
            }
        }

        return result;
    }   
    
    // ejecuta SELECT
    public List<Object[]> consulta(
            String sql, // viene consulta (SELECT ...)
            boolean titulos // true==consulta retorna titulo de columnas
            ) {

        List<Object[]> list = null;
        Connection cn = db.getConnection();

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            ResultSetMetaData rm = rs.getMetaData();
            int ctascol = rm.getColumnCount();

            list = new ArrayList<Object[]>();

            if (titulos) { // toma titulos o alias de consulta
                String[] titu = new String[ctascol];

                for (int i = 0; i < ctascol; i++) {
                    titu[i] = rm.getColumnLabel(1 + i);
                }

                list.add(titu);
            }

            while (rs.next()) { // tomando la data de la consulta
                Object[] fila = new Object[ctascol];

                for (int i = 0; i < ctascol; i++) {
                    fila[i] = rs.getObject(1 + i);
                }

                list.add(fila);
            }

            if (list.isEmpty()) {
                list = null;
            }

        } catch (SQLException e) {
        	e.printStackTrace();
        } finally {
            try {
                cn.close();
            } catch (Exception e) {
            }
        }

        return list;
    }

    public Object[] getFila(String sql) {
        List<Object[]> list = consulta(sql, false);
        Object[] fila = null;

        if (list != null) {
            fila = (Object[]) list.get(0);
        }

        return fila;
    }

    public Object getCampo(String sql) {
        Object[] fila = getFila(sql);
        Object campo = null;

        if (fila != null) {
            campo = fila[0];
        }

        return campo;
    }
}
