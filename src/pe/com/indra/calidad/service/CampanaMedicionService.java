package pe.com.indra.calidad.service;

import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.dao.CampanaMedicionDao;
import pe.com.indra.calidad.dao.CampanaMedicionDaoImpl;
import pe.com.indra.calidad.dao.MedicionDao;
import pe.com.indra.calidad.dao.MedicionDaoImpl;
import pe.com.indra.calidad.dao.ParametroNormaDao;
import pe.com.indra.calidad.dao.ParametroNormaDaoImpl;
import pe.com.indra.calidad.dao.TipoParametroDao;
import pe.com.indra.calidad.dao.TipoParametroDaoImpl;
import pe.com.indra.calidad.entity.CampanaMedicion;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.PeriodoMedicion;
import pe.com.indra.calidad.entity.TipoParametro;

/**
 *
 * @author Luis
 */
public class CampanaMedicionService {

    public void create(CampanaMedicion campanaMedicion) {
          CampanaMedicionDao dao = new CampanaMedicionDaoImpl();

       // dao.create(campanaMedicion);
        
        try {
        	dao.create(campanaMedicion);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Campa�a.", e);
		}
             
    }

    public CampanaMedicion ReadById(long camMedicionId) {
         CampanaMedicionDao dao = new CampanaMedicionDaoImpl();
         CampanaMedicion campanaMedicion = null;
         
        try {
        	campanaMedicion = dao.ReadById(camMedicionId);
		}  catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Campana Medici�n."
		}
        return campanaMedicion;
                     
    }

    public void update(CampanaMedicion campanaMedicion) {
           CampanaMedicionDao dao = new CampanaMedicionDaoImpl();

       try {
        	dao.update(campanaMedicion);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long camMedicionId) {
            CampanaMedicionDao dao = new CampanaMedicionDaoImpl();

        try {
        	dao.delete(camMedicionId);
		}  catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public TipoParametro selectTipoParametro(long camMedicionId) {
             CampanaMedicionDao dao = new CampanaMedicionDaoImpl();
        
        return dao.selectTipoParametro(camMedicionId);
    }

    public PeriodoMedicion selectPeriodoMedicion(long camMedicionId) {
         CampanaMedicionDao dao = new CampanaMedicionDaoImpl();
         
         return dao.selectPeriodoMedicion(camMedicionId);
    }

    public Set<Medicion> selectMedicion(long camMedicionId) {
        CampanaMedicionDao dao = new CampanaMedicionDaoImpl();
        
        return dao.selectMedicion(camMedicionId);
        
    }
    
    public List<CampanaMedicion> getAllRows(){
    	CampanaMedicionDao dao = new CampanaMedicionDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<CampanaMedicion> getAllRows1(){
    	CampanaMedicionDao dao = new CampanaMedicionDaoImpl();
    	return dao.getAllRows1();
    }
}
