package pe.com.indra.calidad.service;

import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.dao.CompensacionUnitariaDao;
import pe.com.indra.calidad.dao.CompensacionUnitariaDaoImpl;
import pe.com.indra.calidad.dao.EquipoRegistradorDao;
import pe.com.indra.calidad.dao.EquipoRegistradorDaoImpl;
import pe.com.indra.calidad.entity.CompensacionUnitaria;
import pe.com.indra.calidad.entity.EquipoRegistrador;
import pe.com.indra.calidad.entity.Medicion;

/**
 *
 * @author Luis
 */
public class EquipoRegistradorService {

    public void create(EquipoRegistrador equipoRegistrador) {
        EquipoRegistradorDao dao = new EquipoRegistradorDaoImpl();
        
        try {
        	dao.create(equipoRegistrador);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Equipo Registrador.", e);
		}
    }

    public EquipoRegistrador ReadById(long equRegistradorId) {
        EquipoRegistradorDao dao = new EquipoRegistradorDaoImpl();
        EquipoRegistrador equipoRegistrador = null;
        
        
        try {
        	equipoRegistrador = dao.ReadById(equRegistradorId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer equipo."
		}
        return equipoRegistrador;
    }

    public EquipoRegistrador ReadByCod(String codEquRegistrador) {
        EquipoRegistradorDao dao = new EquipoRegistradorDaoImpl();
        EquipoRegistrador equipoRegistrador = null;
        
        try {
        	equipoRegistrador = dao.ReadByCod(codEquRegistrador);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer equipo."
		}
        return equipoRegistrador;
    }

    public void update(EquipoRegistrador equipoRegistrador) {
        EquipoRegistradorDao dao = new EquipoRegistradorDaoImpl();

        dao.update(equipoRegistrador);
        
        try {
        	dao.update(equipoRegistrador);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long equRegistradorId) {
        EquipoRegistradorDao dao = new EquipoRegistradorDaoImpl();

        try {
        	dao.delete(equRegistradorId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public Set<Medicion> selectMedicion(long equRegistradorId) {
        EquipoRegistradorDao dao = new EquipoRegistradorDaoImpl();
        return dao.selectMedicion(equRegistradorId);

    }
    public List<EquipoRegistrador> getAllRows(){
    	EquipoRegistradorDao dao = new EquipoRegistradorDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<EquipoRegistrador> getAllRows1(){
    	EquipoRegistradorDao dao = new EquipoRegistradorDaoImpl();
    	return dao.getAllRows1();
    }
}
