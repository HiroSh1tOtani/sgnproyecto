package pe.com.indra.calidad.service;

import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.dao.TipoMedicionDao;
import pe.com.indra.calidad.dao.TipoMedicionDaoImpl;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.TipoMedicion;

/**
 *
 * @author Luis
 */
public class TipoMedicionService {

    public void create(TipoMedicion tipoMedicion) {
            TipoMedicionDao dao = new TipoMedicionDaoImpl();
        
        try {
        	dao.create(tipoMedicion);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Tipo de medicion", e);
		}
    }

    public TipoMedicion ReadById(long tipMedicionId) {
            TipoMedicionDao dao = new TipoMedicionDaoImpl();
            TipoMedicion tipoMedicion = null;
         
        try {
        	tipoMedicion = dao.ReadById(tipMedicionId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Tipo de medicion."
		}
        return tipoMedicion;
    }

    public TipoMedicion ReadByCod(String codTipMedicion) {
            TipoMedicionDao dao = new TipoMedicionDaoImpl();
            TipoMedicion tipoMedicion = null;
       
        try {
        	tipoMedicion = dao.ReadByCod(codTipMedicion);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Tipo de medicionn."
		}
        return tipoMedicion;
    }

    public void update(TipoMedicion tipoMedicion) {
         TipoMedicionDao dao = new TipoMedicionDaoImpl();
        
        try {
        	dao.update(tipoMedicion);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long tipMedicionId) {
          TipoMedicionDao dao = new TipoMedicionDaoImpl();
        
        try {
        	dao.delete(tipMedicionId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public Set<Medicion> selectMedicion(long tipMedicionId) {
          TipoMedicionDao dao = new TipoMedicionDaoImpl();
        
        return dao.selectMedicion(tipMedicionId);
    }
    
    public List<TipoMedicion> getAllRows(){
    	TipoMedicionDao dao = new TipoMedicionDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<TipoMedicion> getAllRows1(){
    	TipoMedicionDao dao = new TipoMedicionDaoImpl();
    	return dao.getAllRows1();
    }
}
