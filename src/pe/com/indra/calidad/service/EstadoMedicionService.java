package pe.com.indra.calidad.service;

import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.dao.CampanaMedicionDao;
import pe.com.indra.calidad.dao.CampanaMedicionDaoImpl;
import pe.com.indra.calidad.dao.EstadoMedicionDao;
import pe.com.indra.calidad.dao.EstadoMedicionDaoImpl;
import pe.com.indra.calidad.entity.CampanaMedicion;
import pe.com.indra.calidad.entity.EstadoMedicion;
import pe.com.indra.calidad.entity.Medicion;

/**
 *
 * @author Luis
 */
public class EstadoMedicionService {

    public void create(EstadoMedicion estadoMedicion) {
        EstadoMedicionDao dao = new EstadoMedicionDaoImpl();

        try {
        	dao.create(estadoMedicion);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Estado de Medicion.", e);
		}
    }

    public EstadoMedicion ReadById(long estMedicionId) {
        EstadoMedicionDao dao = new EstadoMedicionDaoImpl();

        EstadoMedicion estadoMedicion = null;
        
        try {
        	estadoMedicion = dao.ReadById(estMedicionId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer estado."
		}
        return estadoMedicion;
    }

    public EstadoMedicion ReadByCod(String codEstMedicion) {
        EstadoMedicionDao dao = new EstadoMedicionDaoImpl();

        EstadoMedicion estadoMedicion = null;
         
        try {
        	estadoMedicion = dao.ReadByCod(codEstMedicion);
		}  catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer estado."
		}
        return estadoMedicion;
    }

    public void update(EstadoMedicion estadoMedicion) {
        EstadoMedicionDao dao = new EstadoMedicionDaoImpl();

        try {
        	dao.update(estadoMedicion);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long estMedicionId) {
        EstadoMedicionDao dao = new EstadoMedicionDaoImpl();
        
        try {
        	dao.delete(estMedicionId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public Set<Medicion> selectMedicion(long estMedicionId) {
        EstadoMedicionDao dao = new EstadoMedicionDaoImpl();
        return dao.selectMedicion(estMedicionId);
    }
    
    public List<EstadoMedicion> getAllRows(){
    	EstadoMedicionDao dao = new EstadoMedicionDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<EstadoMedicion> getAllRows1(){
    	EstadoMedicionDao dao = new EstadoMedicionDaoImpl();
    	return dao.getAllRows1();
    }
    
    public Set<Medicion> selectMedicionPer(long estMedicionId){
    	EstadoMedicionDao dao = new EstadoMedicionDaoImpl();
    	return dao.selectMedicionPer(estMedicionId);
    }
}
