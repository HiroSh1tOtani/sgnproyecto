/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.indra.calidad.service;

import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.dao.TipoParametroDao;
import pe.com.indra.calidad.dao.TipoParametroDaoImpl;
import pe.com.indra.calidad.entity.CampanaMedicion;
import pe.com.indra.calidad.entity.ParametroNorma;
import pe.com.indra.calidad.entity.TipoParametro;
import pe.com.indra.calidad.entity.CompensacionUnitaria;

/**
 *
 * @author Luis
 */
public class TipoParametroService {

    public void create(TipoParametro tipoParametro) {
        TipoParametroDao dao = new TipoParametroDaoImpl();
        
        try {
        	dao.create(tipoParametro);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Medici�n.", e);
		}
    }

    public TipoParametro ReadById(long tipParametroId) {
        TipoParametroDao dao = new TipoParametroDaoImpl();
        TipoParametro tipoParametro = null;
        
        try {
        	tipoParametro = dao.ReadById(tipParametroId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer "
		}
        return tipoParametro;
    }

    public TipoParametro ReadByCod(String codTipParametro) {
        TipoParametroDao dao = new TipoParametroDaoImpl();
        TipoParametro tipoParametro = null;
      
        try {
        	tipoParametro = dao.ReadByCod(codTipParametro);
		}  catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer "
		}
        return tipoParametro;
    }

    public List<TipoParametro> getAllRows(){
    	TipoParametroDao dao = new TipoParametroDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<TipoParametro> getAllRows1(){
    	TipoParametroDao dao = new TipoParametroDaoImpl();
    	return dao.getAllRows1();
    }
    
    public void update(TipoParametro tipoParametro) {
        TipoParametroDao dao = new TipoParametroDaoImpl();
        
       
        try {
        	 dao.update(tipoParametro);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long tipParametroId) {
        TipoParametroDao dao = new TipoParametroDaoImpl();
        
        
        try {
        	dao.delete(tipParametroId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public Set<ParametroNorma> selectParametroNormas(long tipParametroId) {
        TipoParametroDao dao = new TipoParametroDaoImpl();
        
        return dao.selectParametroNormas(tipParametroId);
    }
    public Set<CampanaMedicion> selectCampanaMediciones(long tipParametroId) {
        TipoParametroDao dao = new TipoParametroDaoImpl();
        
        return dao.selectCampanaMediciones(tipParametroId);
    }

    public Set<CompensacionUnitaria> selectCompensacionUnitarias(long tipParametroId) {
        TipoParametroDao dao = new TipoParametroDaoImpl();
        return dao.selectCompensacionUnitarias(tipParametroId);
    }
    
    
}
