package pe.com.indra.calidad.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.dao.MedicionDao;
import pe.com.indra.calidad.dao.MedicionDaoImpl;
import pe.com.indra.calidad.entity.CampanaMedicion;
import pe.com.indra.calidad.entity.CompensacionTension;
import pe.com.indra.calidad.entity.CronogramaMedicion;
import pe.com.indra.calidad.entity.EquipoRegistrador;
import pe.com.indra.calidad.entity.EstadoMedicion;
import pe.com.indra.calidad.entity.FactorCompensacion;
import pe.com.indra.calidad.entity.IntervaloFRTension;
import pe.com.indra.calidad.entity.IntervaloTension;
import pe.com.indra.calidad.entity.Localidad;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.ParametroMedido;
import pe.com.indra.calidad.entity.TipoAlimentacion;
import pe.com.indra.calidad.entity.TipoMedicion;
import pe.com.indra.calidad.entity.TipoPunto;
import pe.com.indra.calidad.entity.TipoTrabajo;

/**
 *
 * @author Luis
 */
public class MedicionService {

    public void create(Medicion medicion) {
        MedicionDao dao = new MedicionDaoImpl();
        
        try {
        	dao.create(medicion);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Medici�n.", e);
		}
        
    }

    public Medicion ReadById(long medicionId) {
        MedicionDao dao = new MedicionDaoImpl();
        Medicion medicion = null;
        
        try {
        	medicion = dao.ReadById(medicionId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Medici�n."
		}
        
        return medicion;
    }

    public void update(Medicion medicion) {
        MedicionDao dao = new MedicionDaoImpl();
        
        try {
        	dao.update(medicion);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public void delete(long medicionId) {
        MedicionDao dao = new MedicionDaoImpl();
        
        try {
        	dao.delete(medicionId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public EstadoMedicion selectEstadoMedicion(long medicionId) {
        MedicionDao dao = new MedicionDaoImpl();
        return dao.selectEstadoMedicion(medicionId);
    }

    public ParametroMedido selectParametroMedido(long medicionId) {
        MedicionDao dao = new MedicionDaoImpl();
        return dao.selectParametroMedido(medicionId);
    }

    public EquipoRegistrador selectEquipoRegistrador(long medicionId) {
        MedicionDao dao = new MedicionDaoImpl();
        return dao.selectEquipoRegistrador(medicionId);
    }

    public TipoTrabajo selectTipoTrabajo(long medicionId) {
        MedicionDao dao = new MedicionDaoImpl();
        return dao.selectTipoTrabajo(medicionId);
    }

    public TipoAlimentacion selectTipoAlimentacion(long medicionId) {
        MedicionDao dao = new MedicionDaoImpl();
        return dao.selectTipoAlimentacion(medicionId);
    }

    public Localidad selectLocalidad(long medicionId) {
        MedicionDao dao = new MedicionDaoImpl();
        return dao.selectLocalidad(medicionId);
    }

    public TipoPunto selectTipoPunto(long medicionId) {
        MedicionDao dao = new MedicionDaoImpl();
        return dao.selectTipoPunto(medicionId);
    }

    public TipoMedicion selectTipoMedicion(long medicionId) {
        MedicionDao dao = new MedicionDaoImpl();
        return dao.selectTipoMedicion(medicionId);
    }

    public CampanaMedicion selectCampanaMedicion(long medicionId) {
        MedicionDao dao = new MedicionDaoImpl();
        return dao.selectCampanaMedicion(medicionId);
    }

    public Set<IntervaloTension> selectIntervaloTension(long medicionId) {
        MedicionDao dao = new MedicionDaoImpl();
        return dao.selectIntervaloTension(medicionId);
    }

    public Set<IntervaloFRTension> selectIntervaloFRTension(long medicionId) {
        MedicionDao dao = new MedicionDaoImpl();
        return dao.selectIntervaloFRTension(medicionId);
    }

    public Set<FactorCompensacion> selectFactorCompensacion(long medicionId) {
        MedicionDao dao = new MedicionDaoImpl();
        return dao.selectFactorCompensacion(medicionId);
    }

    public Set<CompensacionTension> selectCompensacionTension(long medicionId) {
        MedicionDao dao = new MedicionDaoImpl();
        return dao.selectCompensacionTension(medicionId);
    }

    public Set<CronogramaMedicion> selectCronogramaMedicion(long medicionId) {
        MedicionDao dao = new MedicionDaoImpl();
        return dao.selectCronogramaMedicion(medicionId);
    }
    
    public Set<Medicion> cargarMediciones (long medicionId, String rutaArchivo, String separadorColumna ) {
        Set<Medicion> mediciones = new HashSet<Medicion>();
              
        return mediciones;
    }
    public List<Medicion> getAllRows1(long camMedicionId){
    	MedicionDao dao = new MedicionDaoImpl();
    	return dao.getMedicionxCampana(camMedicionId);
    }
    
    
    public List<Medicion> getAllRows1NoProgramados(long camMedicionId){
    	MedicionDao dao = new MedicionDaoImpl();
    	return dao.getMedicionxCampanaNoProgramados(camMedicionId);
    }
    
    
    public List<Medicion> getAllRows1(long camMedicionId, String tipServicio, String tipPunto){
    	MedicionDao dao = new MedicionDaoImpl();
    	return dao.getMedicionxCampana(camMedicionId, tipServicio, tipPunto);
    } 
    
    public List<Medicion> getAllRows(){
    	MedicionDao dao = new MedicionDaoImpl();
    	return dao.getMedicionxCampana();
    }  
    
    public List<Medicion> getMedicionxSuministro(String numSumMedicion){
    	MedicionDao dao = new MedicionDaoImpl();
    	List<Medicion> mediciones=null;
    	try {
    		mediciones = dao.getMedicionxSuministro(numSumMedicion);
			
		} catch (HibernateException e) {
			throw new HibernateException("No se puede encontrar Mediciones para el Suministro.", e);
		}
    	  	
    	return mediciones;
    }
    
    public Medicion getMedicionDuoRural(long camMedicionId, long medicionId){
    	MedicionDao dao = new MedicionDaoImpl();
    	Medicion medicion=null;
    	try {
    		medicion = dao.getMedicionDuoRural(camMedicionId,medicionId);
			
		} catch (HibernateException e) {
			throw new HibernateException("No se puede encontrar el Suministro.", e);
		}
    	  	
    	return medicion;
    }
    
    
    public EstadoMedicion selectEstadoMedicionPer(long medicionId) {
    	MedicionDao dao = new MedicionDaoImpl();
    	return dao.selectEstadoMedicionPer(medicionId);
    }
    
    
}
