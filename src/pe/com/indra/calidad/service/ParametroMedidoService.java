package pe.com.indra.calidad.service;

import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.dao.ParametroMedidoDao;
import pe.com.indra.calidad.dao.ParametroMedidoDaoImpl;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.ParametroMedido;


/**
 *
 * @author Luis
 */
public class ParametroMedidoService {

    public void create(ParametroMedido parametroMedido) {
        ParametroMedidoDao dao = new ParametroMedidoDaoImpl();
        
        try {
        	dao.create(parametroMedido);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Parametro.", e);
		}
    }

    public ParametroMedido ReadById(long parMedidoId) {
        ParametroMedidoDao dao = new ParametroMedidoDaoImpl();
        ParametroMedido parametroMedido = null;
        
        try {
        	parametroMedido = dao.ReadById(parMedidoId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer parametro Medido."
		}
        return parametroMedido;
    }

    public ParametroMedido ReadByCod(String codParMedido) {
        ParametroMedidoDao dao = new ParametroMedidoDaoImpl();
        ParametroMedido parametroMedido = null; 
        
        try {
        	parametroMedido = dao.ReadByCod(codParMedido);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer parametro Medido."
		}
        return parametroMedido;
    }

    public void update(ParametroMedido parametroMedido) {
        ParametroMedidoDao dao = new ParametroMedidoDaoImpl();

        
        try {
        	dao.update(parametroMedido);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long parMedidoId) {
        ParametroMedidoDao dao = new ParametroMedidoDaoImpl();
 
        try {
        	dao.delete(parMedidoId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public Set<Medicion> selectMedicion(long parMedidoId) {
        ParametroMedidoDao dao = new ParametroMedidoDaoImpl();
        return dao.selectMedicion(parMedidoId);
    }
    
    public List<ParametroMedido> getAllRows(){
    	ParametroMedidoDao dao = new ParametroMedidoDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<ParametroMedido> getAllRows1(){
    	ParametroMedidoDao dao = new ParametroMedidoDaoImpl();
    	return dao.getAllRows1();
    }
}
