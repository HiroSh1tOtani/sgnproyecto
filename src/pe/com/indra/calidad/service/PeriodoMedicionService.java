package pe.com.indra.calidad.service;

import java.util.List;
import java.util.Set;



import org.hibernate.HibernateException;

import pe.com.indra.calidad.dao.PeriodoMedicionDao;
import pe.com.indra.calidad.dao.PeriodoMedicionDaoImpl;
import pe.com.indra.calidad.entity.CampanaMedicion;
import pe.com.indra.calidad.entity.PeriodoMedicion;

/**
 *
 * @author Luis
 */
public class PeriodoMedicionService {

    public void create(PeriodoMedicion periodoMedicion) {
        PeriodoMedicionDao dao = new PeriodoMedicionDaoImpl();
        
        try {
        	dao.create(periodoMedicion);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Periodo.", e);
		}
    }

    public PeriodoMedicion ReadById(long perMedicionId) {
        PeriodoMedicionDao dao = new PeriodoMedicionDaoImpl();

        PeriodoMedicion periodoMedicion = null;
         
        try {
        	periodoMedicion = dao.ReadById(perMedicionId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer periodo."
		}
        return periodoMedicion;

    }

    public void update(PeriodoMedicion periodoMedicion) {
        PeriodoMedicionDao dao = new PeriodoMedicionDaoImpl();

        try {
        	dao.update(periodoMedicion);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public void delete(long perMedicionId) {
        PeriodoMedicionDao dao = new PeriodoMedicionDaoImpl();
        
        try {
        	dao.delete(perMedicionId);
		}catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public Set<CampanaMedicion> selectCampanaMedicion(long camMedicionId) {
        PeriodoMedicionDao dao = new PeriodoMedicionDaoImpl();

        return dao.selectCampanaMedicion(camMedicionId);
    }
    
    public List<PeriodoMedicion> getAllRows(){
    	PeriodoMedicionDao dao = new PeriodoMedicionDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<PeriodoMedicion> getAllRows1(){
    	PeriodoMedicionDao dao = new PeriodoMedicionDaoImpl();
    	return dao.getAllRows1();
    }
}