package pe.com.indra.calidad.service;

import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.dao.CompensacionUnitariaDao;
import pe.com.indra.calidad.dao.CompensacionUnitariaDaoImpl;
import pe.com.indra.calidad.dao.TipoPuntoDao;
import pe.com.indra.calidad.dao.TipoPuntoDaoImpl;
import pe.com.indra.calidad.entity.CompensacionUnitaria;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.TipoPunto;

/**
 *
 * @author Luis
 */
public class TipoPuntoService {

    public void create(TipoPunto tipoPunto) {
            TipoPuntoDao dao = new TipoPuntoDaoImpl();
            
        try {
        	dao.create(tipoPunto);    
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Tipo de punto.", e);
		}
    }

    public TipoPunto ReadById(long tipPuntoId) {
          TipoPuntoDao dao = new TipoPuntoDaoImpl();
          TipoPunto tipoPunto = null;
         
        try {
        	tipoPunto = dao.ReadById(tipPuntoId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer TipoPunto."
		}
        return tipoPunto;
    }

    public TipoPunto ReadByCod(String codTipPunto) {
          TipoPuntoDao dao = new TipoPuntoDaoImpl();
          TipoPunto tipoPunto = null;
        
        try {
        	tipoPunto =  dao.ReadByCod(codTipPunto);
		}  catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer TipoPunto."
		}
        return tipoPunto;
    }

    public void update(TipoPunto tipoPunto) {
            TipoPuntoDao dao = new TipoPuntoDaoImpl();
        
        try {
        	dao.update(tipoPunto);
		}catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long tipPuntoId) {
             TipoPuntoDao dao = new TipoPuntoDaoImpl();
        
        try {
        	dao.delete(tipPuntoId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public Set<Medicion> selectMedicion(long tipPuntoId) {
        
         TipoPuntoDao dao = new TipoPuntoDaoImpl();
         return dao.selectMedicion(tipPuntoId);
    }
    
    public List<TipoPunto> getAllRows(){
    	TipoPuntoDao dao = new TipoPuntoDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<TipoPunto> getAllRows1(){
    	TipoPuntoDao dao = new TipoPuntoDaoImpl();
    	return dao.getAllRows1();
    }
}
