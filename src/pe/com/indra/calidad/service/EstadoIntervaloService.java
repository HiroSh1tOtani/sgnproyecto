package pe.com.indra.calidad.service;

import java.util.Set;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.dao.EstadoIntervaloDao;
import pe.com.indra.calidad.dao.EstadoIntervaloDaoImpl;
import pe.com.indra.calidad.entity.EstadoIntervalo;
import pe.com.indra.calidad.entity.IntervaloTension;

/**
 *
 * @author Luis
 */
public class EstadoIntervaloService {

    public void create(EstadoIntervalo estadoIntervalo) {
        EstadoIntervaloDao dao = new EstadoIntervaloDaoImpl();

        try {
        	dao.create(estadoIntervalo);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Estado Intervalo", e);
		}
    }

    public EstadoIntervalo ReadById(long estIntervaloId) {
        EstadoIntervaloDao dao = new EstadoIntervaloDaoImpl();
        EstadoIntervalo estadoIntervalo = null;
        
        try {
        	estadoIntervalo = dao.ReadById(estIntervaloId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Estado intervalo"
		}
        return estadoIntervalo;
    }

    public EstadoIntervalo ReadByCod(String codEstIntervalo) {
        EstadoIntervaloDao dao = new EstadoIntervaloDaoImpl();
        EstadoIntervalo estadoIntervalo = null;
                 
        try {
        	estadoIntervalo = dao.ReadByCod(codEstIntervalo);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Estado intervalo"
		}
        return estadoIntervalo;
    }

    public void update(EstadoIntervalo estadoIntervalo) {
        EstadoIntervaloDao dao = new EstadoIntervaloDaoImpl();
        
        try {
        	dao.update(estadoIntervalo);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long estIntervaloId) {
        EstadoIntervaloDao dao = new EstadoIntervaloDaoImpl();
 
        try {
        	dao.delete(estIntervaloId);
		}  catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public Set<IntervaloTension> selectIntervaloTension(long estIntervaloId) {
        EstadoIntervaloDao dao = new EstadoIntervaloDaoImpl();
        return dao.selectIntervaloTension(estIntervaloId);
    }
}
