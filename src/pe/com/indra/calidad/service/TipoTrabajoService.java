package pe.com.indra.calidad.service;

import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.dao.TipoParametroDao;
import pe.com.indra.calidad.dao.TipoParametroDaoImpl;
import pe.com.indra.calidad.dao.TipoTrabajoDao;
import pe.com.indra.calidad.dao.TipoTrabajoDaoImpl;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.TipoParametro;
import pe.com.indra.calidad.entity.TipoTrabajo;

/**
 *
 * @author Luis
 */
public class TipoTrabajoService {

    public void create(TipoTrabajo tipoTrabajo) {
        TipoTrabajoDao dao = new TipoTrabajoDaoImpl();
 
        try {
        	dao.create(tipoTrabajo);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Tipo de trabajo.", e);
		}
    }

    public TipoTrabajo ReadById(long tipTrabajoId) {
        TipoTrabajoDao dao = new TipoTrabajoDaoImpl();
        TipoTrabajo tipoTrabajo = null;
         
        try {
        	tipoTrabajo = dao.ReadById(tipTrabajoId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Tipo de trabajo."
		}
        return tipoTrabajo;
    }

    public TipoTrabajo ReadByCod(String codTipTrabajo) {
        TipoTrabajoDao dao = new TipoTrabajoDaoImpl();
        TipoTrabajo tipoTrabajo = null;
         
        try {
        	tipoTrabajo = dao.ReadByCod(codTipTrabajo);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Tipo de trabajo."
		}
        return tipoTrabajo;
    }

    public void update(TipoTrabajo tipoTrabajo) {
        TipoTrabajoDao dao = new TipoTrabajoDaoImpl();

        
        try {
        	dao.update(tipoTrabajo);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long tipTrabajoId) {
        TipoTrabajoDao dao = new TipoTrabajoDaoImpl();

        try {
        	dao.delete(tipTrabajoId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public Set<Medicion> selectMedicion(long tipTrabajoId) {
        TipoTrabajoDao dao = new TipoTrabajoDaoImpl();
        return dao.selectMedicion(tipTrabajoId);
    }
    
    public List<TipoTrabajo> getAllRows(){
    	TipoTrabajoDao dao = new TipoTrabajoDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<TipoTrabajo> getAllRows1(){
    	TipoTrabajoDao dao = new TipoTrabajoDaoImpl();
    	return dao.getAllRows1();
    }
}
