package pe.com.indra.calidad.service;

import java.util.List;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.dao.IntervaloPerturbacionDao;
import pe.com.indra.calidad.dao.IntervaloPerturbacionDaoImpl;
import pe.com.indra.calidad.dao.IntervaloTensionDao;
import pe.com.indra.calidad.dao.IntervaloTensionDaoImpl;
import pe.com.indra.calidad.entity.EstadoIntervalo;
import pe.com.indra.calidad.entity.IntervaloPerturbacion;
import pe.com.indra.calidad.entity.IntervaloTension;
import pe.com.indra.calidad.entity.Medicion;

/**
 *
 * @author Luis
 */
public class IntervaloPerturbacionService {

    public void create(IntervaloPerturbacion intervaloPerturbacion) {
        IntervaloPerturbacionDao dao = new IntervaloPerturbacionDaoImpl();

        
        try {
        	dao.create(intervaloPerturbacion);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Intervalo Tension", e);
		}
    }

    public IntervaloPerturbacion ReadById(long intPerturbacionId) {
        IntervaloPerturbacionDao dao = new IntervaloPerturbacionDaoImpl();
        IntervaloPerturbacion intervaloPerturbacion = null;
        
         
        try {
        	intervaloPerturbacion = dao.ReadById(intPerturbacionId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer intervalo Perturbacion."
		}
        return intervaloPerturbacion;
    }

    public void update(IntervaloPerturbacion intervaloPerturbacion) {
        IntervaloPerturbacionDao dao = new IntervaloPerturbacionDaoImpl();
   
        try {
        	dao.update(intervaloPerturbacion);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long intPerturbacionId) {
        IntervaloPerturbacionDao dao = new IntervaloPerturbacionDaoImpl();
     
        try {
        	dao.delete(intPerturbacionId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }


    public Medicion selectMedicion(long intPerturbacionId) {
        IntervaloPerturbacionDao dao = new IntervaloPerturbacionDaoImpl();
        return dao.selectMedicion(intPerturbacionId);
    }
    
    public EstadoIntervalo selectEstadoIntervaloFlicker(long intPerturbacionId) {
    	IntervaloPerturbacionDao dao = new IntervaloPerturbacionDaoImpl();
		return dao.selectEstadoIntervaloFlicker(intPerturbacionId) ;
	}


	public EstadoIntervalo selectEstadoIntervaloThd(long intPerturbacionId) {
		IntervaloPerturbacionDao dao = new IntervaloPerturbacionDaoImpl();
		return dao.selectEstadoIntervaloThd(intPerturbacionId);
	}

	public EstadoIntervalo selectEstadoIntervaloV(long intPerturbacionId) {
		IntervaloPerturbacionDao dao = new IntervaloPerturbacionDaoImpl();
		return dao.selectEstadoIntervaloV(intPerturbacionId) ;
	}

	public List<IntervaloPerturbacion> getAllRows1(long medicionId){
		IntervaloPerturbacionDao dao = new IntervaloPerturbacionDaoImpl();
    	return dao.getIntervaloxMedicion(medicionId);
    }   
    
   public List<IntervaloPerturbacion> getAllRows(long medicionId){
    	IntervaloPerturbacionDao dao = new IntervaloPerturbacionDaoImpl();
    	return dao.getAllRows(medicionId);
    }
    
    
}
