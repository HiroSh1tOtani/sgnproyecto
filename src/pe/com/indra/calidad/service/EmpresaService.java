package pe.com.indra.calidad.service;

import java.util.List;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.dao.CampanaMedicionDao;
import pe.com.indra.calidad.dao.CampanaMedicionDaoImpl;
import pe.com.indra.calidad.dao.EmpresaDao;
import pe.com.indra.calidad.dao.EmpresaDaoImpl;
import pe.com.indra.calidad.dao.LocalidadDao;
import pe.com.indra.calidad.dao.LocalidadDaoImpl;
import pe.com.indra.calidad.entity.CampanaMedicion;
import pe.com.indra.calidad.entity.Empresa;
import pe.com.indra.calidad.entity.Localidad;


/**
 *
 * @author Luis
 */
public class EmpresaService {

  
	public List<Empresa> getAllRows() {
		EmpresaDao dao = new EmpresaDaoImpl();
		return dao.getAllRows();
	}
	
	 public void update(Empresa empresa) {
		 EmpresaDao dao = new EmpresaDaoImpl();
     
     try {
     	dao.update(empresa);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
	 }
	 
	 public Empresa readByCod(String empresa) {
		 EmpresaDao dao = new EmpresaDaoImpl();
		 Empresa empresasa = null;
      
     try {
     	empresasa = dao.readByCod(empresa);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Localidad."
		}
     return empresasa;
	 }
}


