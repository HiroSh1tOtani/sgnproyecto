package pe.com.indra.calidad.service;

import java.util.List;

import pe.com.indra.calidad.dao.UsuarioDao;
import pe.com.indra.calidad.dao.UsuarioDaoImpl;
import pe.com.indra.calidad.entity.Usuario;

public class UsuarioService {
	public void create(Usuario user){
		UsuarioDao usuarioDao =  new UsuarioDaoImpl();
		usuarioDao.create(user);
	}
	
	public void update(Usuario user){
		UsuarioDao usuarioDao =  new UsuarioDaoImpl();
		usuarioDao.update(user);
	}
	
	public void delete(Usuario user){
		UsuarioDao usuarioDao =  new UsuarioDaoImpl();
		usuarioDao.delete(user.getUsername());
	}

	public Usuario getUsuarioByUsername(String userName){
		UsuarioDao usuarioDao =  new UsuarioDaoImpl();
		return usuarioDao.ReadById(userName);
	}

	public List<Usuario> getAllUsuarios() {
		// TODO Auto-generated method stub
		UsuarioDao usuarioDao =  new UsuarioDaoImpl();
		return usuarioDao.getAllRows();
	}
}
