package pe.com.indra.calidad.service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;

import javax.servlet.ServletOutputStream;

import pe.com.indra.calidad.entity.CampanaMedicion;
import pe.com.indra.calidad.util.ConectaDb;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
/**
 * Servlet implementation class ServletReporteSum
 */
@WebServlet("/ServletReporteSum")
public class ServletReporteSum extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletReporteSum() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String salida;
        
        JasperReport jasperReport = null;
        JasperDesign jasperDesign = null;
        
        try {
            String jrxmlFileName = "/reportes/crono_tensi�n_a clientes_bt_al.jrxml";
            String jpgFileName = "/reportes/electrodunas.jpg";
            File archivoReporte = new File(request.getSession().getServletContext().getRealPath(jrxmlFileName));
            File archivoImagen = new File(request.getSession().getServletContext().getRealPath(jpgFileName));
            HashMap hm = null;
            hm = new HashMap();
 
            ServletOutputStream servletOutputStream = response.getOutputStream();
 
            byte[] bytes = null;//{'1','2','3','4','5'}
 
            try {
            	
           	 	long cam_medicion_id = new Long(request.getParameter("CAM_MEDICION_ID"));
           	 	
           	 	CampanaMedicionService service = new CampanaMedicionService();
           	 	
           	 	CampanaMedicion campanaMedicion = service.ReadById(cam_medicion_id);
           	 	
            	//String mesano = request.getParameter("MESANO");
            	//String descripcion = request.getParameter("DESCRIPCION");
            	
           	 	hm.put("MESANO",campanaMedicion.getPeriodoMedicion().getDescripcion());
            	hm.put("DESCRIPCION",campanaMedicion.getDescripcion());
            	hm.put("CAM_MEDICION_ID",cam_medicion_id);
            	hm.put("RUTA_IMAGEN",archivoImagen.getPath());
            	
            	ConectaDb db = new ConectaDb("CALIDAD");
            	
            	jasperDesign = JRXmlLoader.load(archivoReporte.getPath());
                jasperReport = JasperCompileManager.compileReport(jasperDesign);
                //bytes = JasperRunManager.runReportToPdf(archivoReporte.getPath(), hm, db.getConnection());
                bytes = JasperRunManager.runReportToPdf(jasperReport, hm, db.getConnection());
            	
                response.setContentType("application/pdf");
                response.setContentLength(bytes.length);
                servletOutputStream.write(bytes, 0, bytes.length);
                servletOutputStream.flush();
                servletOutputStream.close();
            } catch (JRException e) {//JRException
                StringWriter stringWriter = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stringWriter);
                e.printStackTrace(printWriter);
                response.setContentType("text/plain");
                response.getOutputStream().print(stringWriter.toString());
            }
        } catch (Exception e) {
            salida = "Error generando Reporte Jasper, el error del Sistema es " + e;
            System.out.println(salida);
        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
