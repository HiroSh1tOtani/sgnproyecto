package pe.com.indra.calidad.service;

import java.util.Set;
import java.util.List;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.dao.CuadrillaDao;
import pe.com.indra.calidad.dao.CuadrillaDaoImpl;
import pe.com.indra.calidad.entity.CronogramaMedicion;
import pe.com.indra.calidad.entity.Cuadrilla;

/**
 *
 * @author Luis
 */
public class CuadrillaService {

    public void create(Cuadrilla cuadrilla) {
        CuadrillaDao dao = new CuadrillaDaoImpl();

        try {
        	dao.create(cuadrilla);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Cuadrilla.", e);
		}
        
    }

    public Cuadrilla ReadById(long cuadrillaId) {
        CuadrillaDao dao = new CuadrillaDaoImpl();
        
        Cuadrilla cuadrilla = null;
        
        try {
        	cuadrilla = dao.ReadById(cuadrillaId);
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Cuadrilla."
		}
        
        return cuadrilla;
        
    }

    public Cuadrilla ReadByCod(String codCuadrilla) {
        CuadrillaDao dao = new CuadrillaDaoImpl();
        Cuadrilla cuadrilla = null;
        
        try {
        	cuadrilla = dao.ReadByCod(codCuadrilla);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Cuadrilla."
		}
        return cuadrilla;
        
    }

    public void update(Cuadrilla cuadrilla) {
        CuadrillaDao dao = new CuadrillaDaoImpl();
         
        try {
        	 dao.update(cuadrilla);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long cuadrillaId) {
            CuadrillaDao dao = new CuadrillaDaoImpl();
        
        dao.delete(cuadrillaId);
    }

    public Set<CronogramaMedicion> selectCronogramaMedicion(long cuadrillaId) {
        CuadrillaDao dao = new CuadrillaDaoImpl();
        return dao.selectCronogramaMedicion(cuadrillaId);
    }
    
    public List<Cuadrilla>getAllRows(){
    	CuadrillaDao dao = new CuadrillaDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<Cuadrilla>getAllRows1(){
    	CuadrillaDao dao = new CuadrillaDaoImpl();
    	return dao.getAllRows();
    }
}


