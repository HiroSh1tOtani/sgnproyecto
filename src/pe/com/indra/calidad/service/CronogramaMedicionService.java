package pe.com.indra.calidad.service;

import java.util.List;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.dao.CronogramaMedicionDao;
import pe.com.indra.calidad.dao.CronogramaMedicionDaoImpl;
import pe.com.indra.calidad.dao.MedicionDao;
import pe.com.indra.calidad.dao.MedicionDaoImpl;
import pe.com.indra.calidad.dao.ParametroNormaDao;
import pe.com.indra.calidad.dao.ParametroNormaDaoImpl;
import pe.com.indra.calidad.entity.CronogramaMedicion;
import pe.com.indra.calidad.entity.Cuadrilla;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.ParametroNorma;

/**
 *
 * @author Luis
 */
public class CronogramaMedicionService {

    public void create(CronogramaMedicion cronogramaMedicion) {
        CronogramaMedicionDao dao = new CronogramaMedicionDaoImpl();
      
        try {
        	dao.create(cronogramaMedicion);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Cronograma.", e);
		}
    }

    public CronogramaMedicion ReadById(long croMedicionId) {
        CronogramaMedicionDao dao = new CronogramaMedicionDaoImpl();
        CronogramaMedicion cronogramaMedicion = null;
        
        try {
        	cronogramaMedicion = dao.ReadById(croMedicionId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        return cronogramaMedicion;
    }

    public void update(CronogramaMedicion cronogramaMedicion) {
        CronogramaMedicionDao dao = new CronogramaMedicionDaoImpl();
 
        try {
        	dao.update(cronogramaMedicion);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long croMedicionId) {
        CronogramaMedicionDao dao = new CronogramaMedicionDaoImpl();

        try {
        	dao.delete(croMedicionId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public Cuadrilla selectCuadrilla(long croMedicionId) {
        CronogramaMedicionDao dao = new CronogramaMedicionDaoImpl();
        return dao.selectCuadrilla(croMedicionId);
    }

    public Medicion selectMedicion(long croMedicionId) {
        CronogramaMedicionDao dao = new CronogramaMedicionDaoImpl();
        return dao.selectMedicion(croMedicionId);
    }
    
    public List<CronogramaMedicion> getAllRows1(long camMedicionId){
    	CronogramaMedicionDao dao = new CronogramaMedicionDaoImpl();
    	return dao.getCronogramaxCampana(camMedicionId);
    }   
    
    public List<CronogramaMedicion> getAllRows(){
    	CronogramaMedicionDao dao = new CronogramaMedicionDaoImpl();
    	return dao.getCronogramaxCampana();
    }  
    
   
    
}
