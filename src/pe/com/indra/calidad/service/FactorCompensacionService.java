package pe.com.indra.calidad.service;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.dao.FactorCompensacionDao;
import pe.com.indra.calidad.dao.FactorCompensacionDaoImpl;
import pe.com.indra.calidad.entity.FactorCompensacion;
import pe.com.indra.calidad.entity.Medicion;

/**
 *
 * @author Luis
 */
public class FactorCompensacionService {

    public void create(FactorCompensacion factorCompensacion) {
        FactorCompensacionDao dao = new FactorCompensacionDaoImpl();

        
        try {
        	dao.create(factorCompensacion);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Factor de Compensacion", e);
		}
    }

    public FactorCompensacion ReadById(long facCompensacionId) {
        FactorCompensacionDao dao = new FactorCompensacionDaoImpl();
        FactorCompensacion factorCompensacion =null;
         
        try {
        	factorCompensacion = dao.ReadById(facCompensacionId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer factor de compensacion."
		}
        return factorCompensacion;
    }

    public void update(FactorCompensacion factorCompensacion) {
        FactorCompensacionDao dao = new FactorCompensacionDaoImpl();
 
        try {
        	dao.update(factorCompensacion);
		}catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long facCompensacionId) {
        FactorCompensacionDao dao = new FactorCompensacionDaoImpl();
       
        try {
        	dao.delete(facCompensacionId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public Medicion selectMedicion(long facCompensacionId) {
        FactorCompensacionDao dao = new FactorCompensacionDaoImpl();
        return dao.selectMedicion(facCompensacionId);
    }
}
