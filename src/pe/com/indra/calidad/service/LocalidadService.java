package pe.com.indra.calidad.service;

import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.dao.CompensacionUnitariaDao;
import pe.com.indra.calidad.dao.CompensacionUnitariaDaoImpl;
import pe.com.indra.calidad.dao.LocalidadDao;
import pe.com.indra.calidad.dao.LocalidadDaoImpl;
import pe.com.indra.calidad.entity.CompensacionUnitaria;
import pe.com.indra.calidad.entity.Localidad;
import pe.com.indra.calidad.entity.Medicion;

/**
 *
 * @author Luis
 */
public class LocalidadService {

    public void create(Localidad localidad) {
        LocalidadDao dao = new LocalidadDaoImpl();

        try {
        	dao.create(localidad);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Medici�n.", e);
		}
    }

    public Localidad ReadById(long localidadId) {
            LocalidadDao dao = new LocalidadDaoImpl();
            Localidad localidad = null;
         
        try {
        	localidad = dao.ReadById(localidadId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Localidad."
		}
        return localidad;
    }

    public Localidad ReadByCod(String codLocalidad) {
              LocalidadDao dao = new LocalidadDaoImpl();
              Localidad localidad = null;
         
        try {
        	localidad = dao.ReadByCod(codLocalidad);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Localidad."
		}
        return localidad;
    }

    public void update(Localidad localidad) {
            LocalidadDao dao = new LocalidadDaoImpl();
        
        try {
        	dao.update(localidad);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long localidadId) {
            LocalidadDao dao = new LocalidadDaoImpl();
        
        try {
        	 dao.delete(localidadId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public Set<Medicion> selectMedicion(long localidadId) {
        
          LocalidadDao dao = new LocalidadDaoImpl();
          return dao.selectMedicion(localidadId);
    }
    
    public List<Localidad> getAllRows(){
    	LocalidadDao dao = new LocalidadDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<Localidad> getAllRows1(){
    	LocalidadDao dao = new LocalidadDaoImpl();
    	return dao.getAllRows1();
    }
    
}
