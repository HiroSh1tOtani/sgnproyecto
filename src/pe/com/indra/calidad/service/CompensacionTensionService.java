package pe.com.indra.calidad.service;

import java.util.List;

import org.hibernate.HibernateException;

import pe.com.calidad.suministro.dao.SuministroAfectadoDao;
import pe.com.calidad.suministro.dao.SuministroAfectadoDaoImpl;
import pe.com.calidad.suministro.entity.SuministroAfectado;
import pe.com.indra.calidad.dao.CompensacionTensionDao;
import pe.com.indra.calidad.dao.CompensacionTensionDaoImpl;
import pe.com.indra.calidad.entity.CompensacionTension;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.TipoEnergia;

/**
 *
 * @author Luis
 */
public class CompensacionTensionService {

    public void create(CompensacionTension compensacionTension) {
        CompensacionTensionDao dao = new CompensacionTensionDaoImpl();

        
        try {
        	dao.create(compensacionTension);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Compensacion Tension.", e);
		}
    }

    public CompensacionTension ReadById(long comTensionId) {
        CompensacionTensionDao dao = new CompensacionTensionDaoImpl();
        CompensacionTension compensacionTension = null;
        
         
        try {
        	compensacionTension = dao.ReadById(comTensionId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Compensacion tension."
		}
        return compensacionTension;
    } 

    public void update(CompensacionTension compensacionTension) {
        CompensacionTensionDao dao = new CompensacionTensionDaoImpl();

        
        try {
        	dao.update(compensacionTension);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long croMedicionId) {
        CompensacionTensionDao dao = new CompensacionTensionDaoImpl();

        
        try {
        	dao.delete(croMedicionId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public TipoEnergia selectTipoEnergia(long comTensionId) {
        CompensacionTensionDao dao = new CompensacionTensionDaoImpl();
        return dao.selectTipoEnergia(comTensionId);
    }

    public Medicion selectMedicion(long comTensionId) {
        CompensacionTensionDao dao = new CompensacionTensionDaoImpl();
        return dao.selectMedicion(comTensionId);
    }
    
    public List<CompensacionTension> getAllRows1(long medicionId){
    	CompensacionTensionDao dao = new CompensacionTensionDaoImpl();
    	return dao.getAllRows1(medicionId);
    }
}
