package pe.com.indra.calidad.service;

import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.dao.TipoEnergiaDao;
import pe.com.indra.calidad.dao.TipoEnergiaDaoImpl;
import pe.com.indra.calidad.entity.CompensacionTension;
import pe.com.indra.calidad.entity.TipoEnergia;



/**
 *
 * @author Luis
 */
public class TipoEnergiaService {

    public void create(TipoEnergia tipoEnergia) {
        TipoEnergiaDao dao = new TipoEnergiaDaoImpl();

        
        try {
        	dao.create(tipoEnergia);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Tipo de energia.", e);
		}
    }

    public TipoEnergia ReadById(long tipEnergiaId) {
        TipoEnergiaDao dao = new TipoEnergiaDaoImpl();
        TipoEnergia tipoEnergia = null;
       
        try {
        	tipoEnergia = dao.ReadById(tipEnergiaId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer tipo energia."
		}
        return tipoEnergia;
    }

    public TipoEnergia ReadByCod(String codTipEnergia) {
        TipoEnergiaDao dao = new TipoEnergiaDaoImpl();
        TipoEnergia tipoEnergia = null;
        
        try {
        	tipoEnergia = dao.ReadByCod(codTipEnergia);
		} catch (Exception e) {
			// TODO: handle exception
		}
        return tipoEnergia;
    }

    public void update(TipoEnergia tipoEnergia) {
        TipoEnergiaDao dao = new TipoEnergiaDaoImpl();
 
        try {
        	dao.update(tipoEnergia);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long tipEnergiaId) {
        TipoEnergiaDao dao = new TipoEnergiaDaoImpl();

        try {
        	dao.delete(tipEnergiaId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public Set<CompensacionTension> selectTipoEnergia(long tipEnergiaId) {
        TipoEnergiaDao dao = new TipoEnergiaDaoImpl();
        return dao.selectTipoEnergia(tipEnergiaId);
    }
    
    public List<TipoEnergia> getAllRows(){
    	TipoEnergiaDao dao = new TipoEnergiaDaoImpl();
    	return dao.getAllRows();
    }
}
