package pe.com.indra.calidad.service;

import java.util.List;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.dao.ReporteDao;
import pe.com.indra.calidad.dao.ReporteDaoImpl;
import pe.com.indra.calidad.entity.Reporte;
import pe.com.indra.calidad.entity.TipoParametro;

/**
 *
 * @author Luis
 */
public class ReporteService {

    public void create(Reporte reporte) {
        ReporteDao dao = new ReporteDaoImpl();

        try {
        	dao.create(reporte);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Reporte.", e);
		}
    }

    public Reporte ReadById(long reporteId) {
        ReporteDao dao = new ReporteDaoImpl();
        Reporte reporte = null;
     
        try {
        	reporte = dao.ReadById(reporteId);
		}  catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer parametro norma."
		}
        return reporte;
    }

    public Reporte ReadByCod(String codParNorma) {
        ReporteDao dao = new ReporteDaoImpl();
        
        Reporte reporte = null;
        
        try {
        	reporte = dao.ReadByCod(codParNorma);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer parametro norma."
		}
        return reporte;
    }

    public void update(Reporte reporte) {
        ReporteDao dao = new ReporteDaoImpl();

        try {
        	dao.update(reporte);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long reporteId) {
        ReporteDao dao = new ReporteDaoImpl();
        
        try {
        	dao.delete(reporteId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public TipoParametro selectTipoParametro(long reporteId) {
         ReporteDao dao = new ReporteDaoImpl();
        
        return dao.selectTipoParametro(reporteId);
    }

	public List<Reporte> getAllRows() {
		ReporteDao dao = new ReporteDaoImpl();
		return dao.getAllRows();
	}
	
	public List<Reporte> getVNR() {
		ReporteDao dao = new ReporteDaoImpl();
		return dao.getVNR();
	}
}
