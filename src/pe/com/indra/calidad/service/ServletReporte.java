package pe.com.indra.calidad.service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletOutputStream;

import pe.com.indra.calidad.entity.CampanaMedicion;
import pe.com.indra.calidad.util.ConectaDb;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 * Servlet implementation class ServletReporte
 */
@WebServlet("/ServletReporte")
public class ServletReporte extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletReporte() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String salida;
        
        JasperReport jasperReport = null;
        JasperDesign jasperDesign = null;
        
        try {
            String jrxmlFileName = "";
            String jpgFileName = "";
            File archivoReporte;
            File archivoImagen;
            HashMap hm = null;
            hm = new HashMap();
 
            ServletOutputStream servletOutputStream = response.getOutputStream();
 
            byte[] bytes = null;//{'1','2','3','4','5'}
 
            try {
            	
           	 	long cam_medicion_id = new Long(request.getParameter("CAM_MEDICION_ID"));
           	 	long tipo = new Long(request.getParameter("TIPO"));
           	 	long tipoPuntoReporte = new Long(request.getParameter("TIPOPUNTOREPORTE"));
           	 	
           	 	if(tipo==1){
           	 		if(tipoPuntoReporte == 1) jrxmlFileName = "/reportes/crono_tensi�n_a clientes_bt_al_basicos.jrxml";
           	 		else jrxmlFileName = "/reportes/crono_tensi�n_a clientes_bt_al_adicionales.jrxml";
        	 	}
           	 	if(tipo==2){
           	 		if(tipoPuntoReporte == 1) jrxmlFileName = "/reportes/crono_tensi�n_a clientes_mt_al_basicos.jrxml";
           	 		else jrxmlFileName = "/reportes/crono_tensi�n_a clientes_mt_al_adicionales.jrxml";
        	 	}
           	 	if(tipo==3){
           	 		if(tipoPuntoReporte == 1) jrxmlFileName = "/reportes/crono_de_tensi�n_clientes_mt-bt_rural_basicos.jrxml";
           	 		else jrxmlFileName = "/reportes/crono_de_tensi�n_clientes_mt-bt_rural_adicionales.jrxml";
           	 	}
           	 	if(tipo==4){
           	 		if(tipoPuntoReporte == 1) jrxmlFileName = "/reportes/crono_perturbaciones_bt_basicos.jrxml";
           	 		else jrxmlFileName = "/reportes/crono_perturbaciones_bt_adicionales.jrxml";
        	 	}
           	 	if(tipo==5){
           	 		if(tipoPuntoReporte == 1) jrxmlFileName = "/reportes/crono_perturbaciones_mt_basicos.jrxml";
           	 		else jrxmlFileName = "/reportes/crono_perturbaciones_mt_adicionales.jrxml";
        	 	}
           	 	if(tipo==6){
           	 		if(tipoPuntoReporte == 1) jrxmlFileName = "/reportes/crono_perturbaciones_sed_basicos.jrxml";
           	 		else jrxmlFileName = "/reportes/crono_perturbaciones_sed_adicionales.jrxml";
        	 	}
                jpgFileName = "/reportes/electrodunas.jpg";
                archivoReporte = new File(request.getSession().getServletContext().getRealPath(jrxmlFileName));
                archivoImagen = new File(request.getSession().getServletContext().getRealPath(jpgFileName));
           	 	
           	 	CampanaMedicionService service = new CampanaMedicionService();
           	 	
           	 	CampanaMedicion campanaMedicion = service.ReadById(cam_medicion_id);
           	 	
            	//String mesano = request.getParameter("MESANO");
            	//String descripcion = request.getParameter("DESCRIPCION");
            	
           	 	hm.put("MESANO",campanaMedicion.getPeriodoMedicion().getDescripcion());
            	hm.put("DESCRIPCION",campanaMedicion.getDescripcion());
            	hm.put("CAM_MEDICION_ID",cam_medicion_id);
            	hm.put("RUTA_IMAGEN",archivoImagen.getPath());
            	
            	ConectaDb db = new ConectaDb("CALIDAD");
            	
            	
            	jasperDesign = JRXmlLoader.load(archivoReporte.getPath());
                jasperReport = JasperCompileManager.compileReport(jasperDesign);
                
                bytes = JasperRunManager.runReportToPdf(jasperReport, hm, db.getConnection());
                                
                response.setContentType("application/pdf");
                
                response.setContentLength(bytes.length);
                servletOutputStream.write(bytes, 0, bytes.length);
                servletOutputStream.flush();
                servletOutputStream.close();

            	
            } catch (JRException e) {//JRException
                StringWriter stringWriter = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stringWriter);
                e.printStackTrace(printWriter);
                response.setContentType("text/plain");
                response.getOutputStream().print(stringWriter.toString());
            }
        } catch (Exception e) {
            salida = "Error generando Reporte Jasper, el error del Sistema es " + e;
            System.out.println(salida);
        }

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	void writeOutContent(final HttpServletResponse res, final File content, final String theFilename, String contentType) {
	    if (content == null)
	      return;
	    try {
	      res.setHeader("Pragma", "no-cache");
	      res.setDateHeader("Expires", 0);
	      res.setContentType(contentType);
	      res.setHeader("Content-disposition", "attachment; filename=" + theFilename);
	      fastChannelCopy(Channels.newChannel(new FileInputStream(content)), Channels.newChannel(res.getOutputStream()));
	    } catch (final IOException e) {
	      // TODO produce a error message <img src="http://s0.wp.com/wp-includes/images/smilies/icon_smile.gif?m=1129645325g" alt=":)" class="wp-smiley"> 
	    }
	  }
	 
	  void fastChannelCopy(final ReadableByteChannel src, final WritableByteChannel dest) throws IOException {
	    final ByteBuffer buffer = ByteBuffer.allocateDirect(1000 * 1024);
	    while (src.read(buffer) != -1) {
	      buffer.flip();
	      dest.write(buffer);
	      buffer.compact();
	    }
	    buffer.flip();
	    while (buffer.hasRemaining()){
	      dest.write(buffer);
	    }
	  }

}
