package pe.com.indra.calidad.service;

import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.dao.CompensacionUnitariaDao;
import pe.com.indra.calidad.dao.CompensacionUnitariaDaoImpl;
import pe.com.indra.calidad.dao.TipoAlimentacionDao;
import pe.com.indra.calidad.dao.TipoAlimentacionDaoImpl;
import pe.com.indra.calidad.dao.TipoEnergiaDao;
import pe.com.indra.calidad.dao.TipoEnergiaDaoImpl;
import pe.com.indra.calidad.entity.CompensacionUnitaria;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.TipoAlimentacion;
import pe.com.indra.calidad.entity.TipoEnergia;

/**
 *
 * @author Luis
 */
public class TipoAlimentacionService {

    public void create(TipoAlimentacion tipoAlimentacion) {
        TipoAlimentacionDao dao = new TipoAlimentacionDaoImpl();

        
        
        try {
        	dao.create(tipoAlimentacion);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Tipo de Alimentacion", e);
		}
    }

    public TipoAlimentacion ReadById(long tipAlimentacionId) {
        TipoAlimentacionDao dao = new TipoAlimentacionDaoImpl();

        TipoAlimentacion tipoAlimentacion = null;
        
        try {
        	tipoAlimentacion = dao.ReadById(tipAlimentacionId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer tipo alimentacion."
		}
        return tipoAlimentacion;
    }

    public TipoAlimentacion ReadByCod(String codTipAlimentacion) {
        TipoAlimentacionDao dao = new TipoAlimentacionDaoImpl();
        TipoAlimentacion tipoAlimentacion = null;
       
        try {
        	tipoAlimentacion = dao.ReadByCod(codTipAlimentacion);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer tipo alimentacion."
		}
        return tipoAlimentacion;
    }

    public void update(TipoAlimentacion tipoAlimentacion) {
        TipoAlimentacionDao dao = new TipoAlimentacionDaoImpl();

        try {
        	dao.update(tipoAlimentacion);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long tipAlimentacionId) {
        TipoAlimentacionDao dao = new TipoAlimentacionDaoImpl();

        dao.delete(tipAlimentacionId);
        try {
        	dao.delete(tipAlimentacionId);
		}catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public Set<Medicion> selectMedicion(long tipAlimentacionId) {
        TipoAlimentacionDao dao = new TipoAlimentacionDaoImpl();
        return dao.selectMedicion(tipAlimentacionId);
    }
    
    public List<TipoAlimentacion> getAllRows(){
    	TipoAlimentacionDao dao = new TipoAlimentacionDaoImpl();
    	return dao.getAllRows();
    }
   
    public List<TipoAlimentacion> getAllRows1(){
    	TipoAlimentacionDao dao = new TipoAlimentacionDaoImpl();
    	return dao.getAllRows1();
    }
}
