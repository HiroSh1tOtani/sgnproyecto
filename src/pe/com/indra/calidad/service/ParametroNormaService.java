package pe.com.indra.calidad.service;

import java.util.List;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.dao.ParametroNormaDao;
import pe.com.indra.calidad.dao.ParametroNormaDaoImpl;
import pe.com.indra.calidad.dao.TipoParametroDao;
import pe.com.indra.calidad.dao.TipoParametroDaoImpl;
import pe.com.indra.calidad.entity.ParametroNorma;
import pe.com.indra.calidad.entity.TipoParametro;

/**
 *
 * @author Luis
 */
public class ParametroNormaService {

    public void create(ParametroNorma parametroNorma) {
        ParametroNormaDao dao = new ParametroNormaDaoImpl();

        try {
        	dao.create(parametroNorma);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Parametro norma.", e);
		}
    }

    public ParametroNorma ReadById(long parNormaId) {
        ParametroNormaDao dao = new ParametroNormaDaoImpl();
        ParametroNorma parametroNorma = null;
     
        try {
        	parametroNorma = dao.ReadById(parNormaId);
		}  catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer parametro norma."
		}
        return parametroNorma;
    }

    public ParametroNorma ReadByCod(String codParNorma) {
        ParametroNormaDao dao = new ParametroNormaDaoImpl();
        
        ParametroNorma parametroNorma = null;
        
        try {
        	parametroNorma = dao.ReadByCod(codParNorma);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer parametro norma."
		}
        return parametroNorma;
    }

    public void update(ParametroNorma parametroNorma) {
        ParametroNormaDao dao = new ParametroNormaDaoImpl();

        try {
        	dao.update(parametroNorma);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long parNormaId) {
        ParametroNormaDao dao = new ParametroNormaDaoImpl();
        
        try {
        	dao.delete(parNormaId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public TipoParametro selectTipoParametro(long parNormaId) {
         ParametroNormaDao dao = new ParametroNormaDaoImpl();
        
        return dao.selectTipoParametro(parNormaId);
    }

	public List<ParametroNorma> getAllRows() {
		ParametroNormaDao dao = new ParametroNormaDaoImpl();
		return dao.getAllRows();
	}
}
