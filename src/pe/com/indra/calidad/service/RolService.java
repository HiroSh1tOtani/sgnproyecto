package pe.com.indra.calidad.service;

import java.util.List;

import pe.com.indra.calidad.dao.RolDao;
import pe.com.indra.calidad.dao.RolDaoImpl;
import pe.com.indra.calidad.dao.UsuarioDao;
import pe.com.indra.calidad.dao.UsuarioDaoImpl;
import pe.com.indra.calidad.entity.Rol;
import pe.com.indra.calidad.entity.Usuario;


public class RolService {
	public void create(Rol rol){
		RolDao rolDao =  new RolDaoImpl();
		rolDao.create(rol);
	}
	
	public void update(Rol rol){
		RolDao rolDao =  new RolDaoImpl();
		rolDao.update(rol);
	}
	
	public void delete(Rol rol){
		RolDao rolDao =  new RolDaoImpl();
		rolDao.delete(rol.getRolename());
	}

	public Rol ReadById(String roleName){
		RolDao rolDao =  new RolDaoImpl();
		return rolDao.ReadById(roleName);
	}
	
	public List<Rol> getAllRoles() {
		// TODO Auto-generated method stub
		RolDao rolDao =  new RolDaoImpl();
		return rolDao.getAllRows();
	}
}
