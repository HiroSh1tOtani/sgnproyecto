package pe.com.indra.calidad.service;

import java.util.List;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.dao.CompensacionUnitariaDao;
import pe.com.indra.calidad.dao.CompensacionUnitariaDaoImpl;
import pe.com.indra.calidad.dao.TipoParametroDao;
import pe.com.indra.calidad.dao.TipoParametroDaoImpl;
import pe.com.indra.calidad.entity.CompensacionUnitaria;
import pe.com.indra.calidad.entity.TipoParametro;

public class CompensacionUnitariaService {
    public void create(CompensacionUnitaria compensacionUnitaria) {
        CompensacionUnitariaDao dao = new CompensacionUnitariaDaoImpl();

        try {
        	dao.create(compensacionUnitaria);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Compensacion unitaria.", e);
		}
    }

    public CompensacionUnitaria ReadById(long comUnitariaId) {
        CompensacionUnitariaDao dao = new CompensacionUnitariaDaoImpl();
        
        CompensacionUnitaria compensacionUnitaria = null;
                
        try {
        	compensacionUnitaria = dao.ReadById(comUnitariaId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Compensacion Unitaria."
		}

        return compensacionUnitaria;
    }

    public CompensacionUnitaria ReadByCod(String codComUnitaria) {
        CompensacionUnitariaDao dao = new CompensacionUnitariaDaoImpl();

       // return dao.ReadByCod(codComUnitaria);
        CompensacionUnitaria compensacionUnitaria = null;
        
        try {
        	compensacionUnitaria = dao.ReadByCod(codComUnitaria);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Compensacion Unitaria."
		}
        return compensacionUnitaria;
    }

    public void update(CompensacionUnitaria compensacionUnitaria) {
        CompensacionUnitariaDao dao = new CompensacionUnitariaDaoImpl();

        try {
        	dao.update(compensacionUnitaria);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long comUnitariaId) {
        CompensacionUnitariaDao dao = new CompensacionUnitariaDaoImpl();

        try {
        	dao.delete(comUnitariaId);
		}  catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public TipoParametro selectTipoParametro(long comUnitariaId) {
         CompensacionUnitariaDao dao = new CompensacionUnitariaDaoImpl();
        
        return dao.selectTipoParametro(comUnitariaId);
    }
    
    public List<CompensacionUnitaria> getAllRows(){
    	CompensacionUnitariaDao dao = new CompensacionUnitariaDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<CompensacionUnitaria> getAllRows1(){
    	CompensacionUnitariaDao dao = new CompensacionUnitariaDaoImpl();
    	return dao.getAllRows1();
    }
}
