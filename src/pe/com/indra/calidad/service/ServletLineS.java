package pe.com.indra.calidad.service;



import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.awt.Color;
import java.io.OutputStream;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import pe.com.indra.calidad.entity.IntervaloTension;
import pe.com.indra.calidad.service.IntervaloTensionService;



/**
 * Servlet implementation class ServletLine
 */
public class ServletLineS extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
	private XYDataset generaDatos2(long medicionId) {
		long indice;
		
		IntervaloTensionService service=new IntervaloTensionService();
		
		indice = 1;
		XYSeries serie1 = new XYSeries("Tension S");
		XYSeries serie2 = new XYSeries("Limite Superior");
		XYSeries serie3 = new XYSeries("Limite Inferior");
		
		for(IntervaloTension p:service.getAllRows1(medicionId)){
			
		  serie1.add(indice, p.getTensionS());
		  serie2.add(indice, 231);
		  serie3.add(indice, 209);
		  indice = indice + 1;
		}
		 
		 XYSeriesCollection xyseriescollection =
		 new XYSeriesCollection();
		 xyseriescollection.addSeries(serie1);
		 xyseriescollection.addSeries(serie2);
		 xyseriescollection.addSeries(serie3);
		 
		 return xyseriescollection;
	}

	
	private XYDataset generaDatos(long medicionId, String tension) {
		long indice;
		
		IntervaloTensionService service=new IntervaloTensionService();
		
		indice = 1;
		

		XYSeries serie1 = new XYSeries("Tension " + tension);
		XYSeries serie2 = new XYSeries("Limite Superior");
		XYSeries serie3 = new XYSeries("Limite Inferior");
		
		for(IntervaloTension p:service.getAllRows1(medicionId)){
		
		  if(tension.equals("R")){
			  serie1.add(indice, p.getTensionR());
		  }


		  if(tension.equals("S")){
			  serie1.add(indice, p.getTensionS());
		  }		

		  if(tension.equals("T")){
			  serie1.add(indice, p.getTensionT());
		  }		  
		  
		  serie2.add(indice, 231);
		  serie3.add(indice, 209);
		  indice = indice + 1;
		}
		 
		 XYSeriesCollection xyseriescollection =
		 new XYSeriesCollection();
		 xyseriescollection.addSeries(serie1);
		 xyseriescollection.addSeries(serie2);
		 xyseriescollection.addSeries(serie3);
		 
		 return xyseriescollection;
	}	
	
	 private static JFreeChart generaGrafico(XYDataset xydataset) {
	 JFreeChart jfreechart = ChartFactory.createXYLineChart(
	 "Perfil de la Medici�n", "Intervalos (15m)", "Tension (v)",
	 xydataset, PlotOrientation.VERTICAL,
	 true, true, false);
	 
	 XYPlot xyplot = (XYPlot) jfreechart.getPlot();
	 xyplot.setBackgroundPaint(Color.white);
	 xyplot.setDomainGridlinePaint(Color.gray);
	 xyplot.setRangeGridlinePaint(Color.gray);
	 xyplot.getRangeAxis().setRange(200, 240);
	 
	 xyplot.getRenderer().setSeriesPaint(0, Color.green);
	 xyplot.getRenderer().setSeriesPaint(1, Color.red);
	 xyplot.getRenderer().setSeriesPaint(2, Color.red);
	 
	 XYLineAndShapeRenderer xylineandshaperenderer =
	 (XYLineAndShapeRenderer) xyplot.getRenderer();
	 xylineandshaperenderer.setBaseShapesVisible(false);
	 
	 return jfreechart;
	 }
	 
	 protected void processRequest(HttpServletRequest
		 request, HttpServletResponse response)
		 throws ServletException, IOException {
		 
		 String name = "MEDICIONID";

		 long medicionId = new Long(request.getParameter(name));

		
		 //System.out.print("TENSION:" + tension);
		 response.setContentType("image/jpeg");
		 OutputStream out = response.getOutputStream();
		 

		 XYDataset xydataset = generaDatos2(medicionId);
		 JFreeChart grafico = generaGrafico(xydataset);
		 ChartUtilities.writeChartAsJPEG(out, grafico, 650, 550);
		 
		 out.close();
	 }
		 
		 // <editor-fold defaultstate="collapsed" desc="m�todos doGet y doPost creados por NetBeans">
		 /**
		 * Handles the HTTP <code>GET</code> method.
		 * @param request servlet request
		 * @param response servlet response
		 * @throws ServletException if a servlet-specific error occurs
		 * @throws IOException if an I/O error occurs
		 */
	 @Override
	 protected void doGet(HttpServletRequest request, HttpServletResponse response)
	 throws ServletException, IOException {
	 processRequest(request, response);
	 }
		 
		 /**
		 * Handles the HTTP <code>POST</code> method.
		 * @param request servlet request
		 * @param response servlet response
		 * @throws ServletException if a servlet-specific error occurs
		 * @throws IOException if an I/O error occurs
		 */
	 @Override
	 protected void doPost(HttpServletRequest request, HttpServletResponse response)
	 throws ServletException, IOException {
	 processRequest(request, response);
	 }
		 
		 /**
		 * Returns a short description of the servlet.
		 * @return a String containing servlet description
		 */
	 @Override
	 public String getServletInfo() {
	 return "Short description";
	 }// </editor-fold>

}