package pe.com.indra.calidad.service;

import java.util.List;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.dao.IntervaloTensionDao;
import pe.com.indra.calidad.dao.IntervaloTensionDaoImpl;
import pe.com.indra.calidad.entity.EstadoIntervalo;
import pe.com.indra.calidad.entity.IntervaloTension;
import pe.com.indra.calidad.entity.Medicion;

/**
 *
 * @author Luis
 */
public class IntervaloTensionService {

    public void create(IntervaloTension intervaloTension) {
        IntervaloTensionDao dao = new IntervaloTensionDaoImpl();

        
        try {
        	dao.create(intervaloTension);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Intervalo Tension", e);
		}
    }

    public IntervaloTension ReadById(long intTensionId) {
        IntervaloTensionDao dao = new IntervaloTensionDaoImpl();
        IntervaloTension intervaloTension = null;
        
         
        try {
        	intervaloTension = dao.ReadById(intTensionId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer intervalo tension."
		}
        return intervaloTension;
    }

    public void update(IntervaloTension intervaloTension) {
        IntervaloTensionDao dao = new IntervaloTensionDaoImpl();
   
        try {
        	dao.update(intervaloTension);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long intTensionId) {
        IntervaloTensionDao dao = new IntervaloTensionDaoImpl();
     
        try {
        	dao.delete(intTensionId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public EstadoIntervalo selectEstadoIntervalo(long intTensionId) {
        IntervaloTensionDao dao = new IntervaloTensionDaoImpl();
        return dao.selectEstadoIntervalo(intTensionId);

    }

    public Medicion selectMedicion(long intTensionId) {
        IntervaloTensionDao dao = new IntervaloTensionDaoImpl();
        return dao.selectMedicion(intTensionId);
    }
    
    public List<IntervaloTension> getAllRows1(long medicionId){
    	IntervaloTensionDao dao = new IntervaloTensionDaoImpl();
    	return dao.getIntervaloxMedicion(medicionId);
    }   
    
    public List<IntervaloTension> getAllRows(){
    	IntervaloTensionDao dao = new IntervaloTensionDaoImpl();
    	return dao.getIntervaloxMedicion();
    }
    
    public List<IntervaloTension> getAllRows(long medicionId){
    	IntervaloTensionDao dao = new IntervaloTensionDaoImpl();
    	return dao.getAllRows(medicionId);
    }
    
    public double getMinTension(long medicionId){
    	IntervaloTensionDao dao = new IntervaloTensionDaoImpl();
    	return dao.getMinTension(medicionId);
    }
    
    public double getMaxTension(long medicionId){
    	IntervaloTensionDao dao = new IntervaloTensionDaoImpl();
    	return dao.getMaxTension(medicionId);
    }
}
