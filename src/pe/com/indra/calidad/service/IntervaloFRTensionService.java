package pe.com.indra.calidad.service;

import java.util.List;

import org.hibernate.HibernateException;

import pe.com.indra.calidad.dao.CompensacionTensionDao;
import pe.com.indra.calidad.dao.CompensacionTensionDaoImpl;
import pe.com.indra.calidad.dao.IntervaloFRTensionDao;
import pe.com.indra.calidad.dao.IntervaloFRTensionDaoImpl;
import pe.com.indra.calidad.dao.ParametroNormaDao;
import pe.com.indra.calidad.dao.ParametroNormaDaoImpl;
import pe.com.indra.calidad.entity.CompensacionTension;
import pe.com.indra.calidad.entity.IntervaloFRTension;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.ParametroNorma;

/**
 *
 * @author Luis
 */
public class IntervaloFRTensionService {

    public void create(IntervaloFRTension intervaloFRTension) {
        IntervaloFRTensionDao dao = new IntervaloFRTensionDaoImpl();

        
        try {
        	dao.create(intervaloFRTension);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Intervalo FRTension.", e);
		}
    }

    public IntervaloFRTension ReadById(long intFRTensionId) {
        IntervaloFRTensionDao dao = new IntervaloFRTensionDaoImpl();

        IntervaloFRTension intervaloFRTension = null;
        
        try {
        	intervaloFRTension = dao.ReadById(intFRTensionId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer intervalo FRTension."
		}
        return intervaloFRTension;
    }

    public void update(IntervaloFRTension intervaloFRTension) {
        IntervaloFRTensionDao dao = new IntervaloFRTensionDaoImpl();

        try {
        	dao.update(intervaloFRTension);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long intFRTensionId) {
        IntervaloFRTensionDao dao = new IntervaloFRTensionDaoImpl();

        try {
        	dao.delete(intFRTensionId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public Medicion selectMedicion(long intFRTensionId) {
        IntervaloFRTensionDao dao = new IntervaloFRTensionDaoImpl();
        return dao.selectMedicion(intFRTensionId);
    }
    
    public List<IntervaloFRTension> getAllRows() {
    	IntervaloFRTensionDao dao = new IntervaloFRTensionDaoImpl();
		return dao.getAllRows();
	}
    
    public IntervaloFRTension getAllRows1(long medicionId) {
    	IntervaloFRTensionDao dao = new IntervaloFRTensionDaoImpl();
		return dao.getAllRows1(medicionId);
	}
    
    
}
