
package pe.com.indra.calidad.dao;

import java.util.List;

import pe.com.indra.calidad.entity.IntervaloFRTension;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.ParametroNorma;

/**
 *
 * @author Luis
 */
public interface IntervaloFRTensionDao {
    
    public void create(IntervaloFRTension intervaloFRTension);

    public IntervaloFRTension ReadById(long intFRTensionId);

    public void update(IntervaloFRTension intervaloFRTension);

    public void delete(long intFRTensionId);

    public Medicion selectMedicion(long intFRTensionId);
    
    public List<IntervaloFRTension> getAllRows();
    
    public IntervaloFRTension getAllRows1(long medicionId);
    
}
