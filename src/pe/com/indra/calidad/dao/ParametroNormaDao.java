
package pe.com.indra.calidad.dao;

import java.util.List;

import pe.com.indra.calidad.entity.ParametroNorma;
import pe.com.indra.calidad.entity.TipoParametro;

/**
 *
 * @author Juan
 */
public interface ParametroNormaDao {
    public void create(ParametroNorma parametroNorma);
    public ParametroNorma ReadById(long parNormaId);
	public ParametroNorma ReadByCod(String codParNorma);
	public void update(ParametroNorma parametroNorma);
	public void delete(long parNormaId);
    public TipoParametro selectTipoParametro(long parNormaId);
	public List<ParametroNorma> getAllRows();
        
}
