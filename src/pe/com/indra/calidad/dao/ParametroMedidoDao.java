package pe.com.indra.calidad.dao;

/**
 *
 * @author Luis
 */
import java.util.List;

import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.ParametroMedido;


import java.util.Set;

public interface ParametroMedidoDao {

    public void create(ParametroMedido parametroMedido);

    public ParametroMedido ReadById(long parMedidoId);

    public ParametroMedido ReadByCod(String codParMedido);

    public void update(ParametroMedido parametroMedido);

    public void delete(long parMedidoId);

    public Set<Medicion> selectMedicion(long parMedidoId);
    
    public List<ParametroMedido> getAllRows();
    
    public List<ParametroMedido> getAllRows1();
}
