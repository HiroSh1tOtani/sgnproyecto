package pe.com.indra.calidad.dao;

import pe.com.indra.calidad.entity.EstadoIntervalo;
import pe.com.indra.calidad.entity.IntervaloPerturbacion;
import pe.com.indra.calidad.entity.IntervaloTension;

import java.util.List;
import java.util.Set;

/**
 *
 * @author Luis
 */
public interface EstadoIntervaloDao {
    
    public void create(EstadoIntervalo estadoIntervalo);

    public EstadoIntervalo ReadById(long estIntervaloId);

    public EstadoIntervalo ReadByCod(String codEstIntervalo);

    public void update(EstadoIntervalo estadoIntervalo);

    public void delete(long estIntervaloId);
    
    public Set<IntervaloTension> selectIntervaloTension(long estIntervaloId);
    
    public Set<IntervaloPerturbacion> selectIntervaloPerturbacionFlicker(long estIntervaloId);
    public Set<IntervaloPerturbacion> selectIntervaloPerturbacionThd(long estIntervaloId);
    public Set<IntervaloPerturbacion> selectIntervaloPerturbacionV(long estIntervaloId);
}
