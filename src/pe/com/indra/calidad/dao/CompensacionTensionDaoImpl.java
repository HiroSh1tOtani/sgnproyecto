package pe.com.indra.calidad.dao;

/**
 *
 * @author Luis
 */
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import java.util.List;
import java.util.Set;

import pe.com.calidad.suministro.entity.SuministroAfectado;
import pe.com.indra.calidad.entity.CompensacionTension;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.TipoEnergia;

public class CompensacionTensionDaoImpl implements CompensacionTensionDao {

    private static CompensacionTensionDaoImpl instance = null;

    public static CompensacionTensionDaoImpl getInstance() {
        if (instance == null) {
            instance = new CompensacionTensionDaoImpl();
        }

        return instance;
    }

    public CompensacionTensionDaoImpl() {
    }

    @Override
    public void create(CompensacionTension compensacionTension) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(compensacionTension);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Compensación de Tensión.", e);
        }
    }

    @Override
    public CompensacionTension ReadById(long comTensionId) {
        CompensacionTension compensacionTension = null;
        try {
            HibernateUtil.begin();

            /*
             Query q = HibernateUtil.getSession().createQuery("from CAL_COMPENSACION_TENSION where COM_TENSION_ID = :comTensionId");
             q.setLong("comTensionId",comTensionId);
             compensacionTension = (CompensacionTension)q.uniqueResult();*/

            compensacionTension = (CompensacionTension) HibernateUtil.getSession().load(CompensacionTension.class, comTensionId);

            Hibernate.initialize(compensacionTension);
            Hibernate.initialize(compensacionTension.getTipoenergia());
            Hibernate.initialize(compensacionTension.getMedicion());
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Compensacion de Tension.", e);
        }
        return compensacionTension;
    }

    @Override
    public void update(CompensacionTension compensacionTension) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(compensacionTension);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Compensacion de Tension.", e);
        }
    }

    @Override
    public void delete(long croMedicionId) {
        CompensacionTension compensacionTension = null;
        compensacionTension = ReadById(croMedicionId);

        if (compensacionTension != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(compensacionTension);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Compensacion de Tension.", e);
            }
        }
    }

    @Override
    public TipoEnergia selectTipoEnergia(long comTensionId) {
        CompensacionTension compensacionTension = null;
        compensacionTension = ReadById(comTensionId);

        return compensacionTension.getTipoenergia();
    }

    @Override
    public Medicion selectMedicion(long comTensionId) {
        CompensacionTension compensacionTension = null;
        compensacionTension = ReadById(comTensionId);

        return compensacionTension.getMedicion();
    }

	@Override
	public List<CompensacionTension> getAllRows1(long medicionId) {
		List<CompensacionTension> compensacionTension=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from CompensacionTension where estado =:estado and medicion.medicionId = :medicionId");
	            q.setString("estado","1");
	            q.setLong("medicionId",medicionId);
	            
	            compensacionTension=(List<CompensacionTension>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar medicion Afectado.", e);
	        }
		return compensacionTension;
	}
   
}
