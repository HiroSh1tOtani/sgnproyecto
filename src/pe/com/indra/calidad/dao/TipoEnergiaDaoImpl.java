
package pe.com.indra.calidad.dao;

import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import java.util.List;

import pe.com.indra.calidad.entity.CompensacionTension;
import pe.com.indra.calidad.entity.TipoEnergia;

/**
 *
 * @author Luis
 */
public class TipoEnergiaDaoImpl implements TipoEnergiaDao{

    private static TipoEnergiaDaoImpl instance = null;

    public static TipoEnergiaDaoImpl getInstance() {
        if (instance == null) {
            instance = new TipoEnergiaDaoImpl();
        }

        return instance;
    }

    public TipoEnergiaDaoImpl() {
    }
    
    @Override
    public void create(TipoEnergia tipoEnergia) {
           try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(tipoEnergia);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Tipo de Energia.", e);
        }
    }

    @Override
    public TipoEnergia ReadById(long tipEnergiaId) {
         TipoEnergia tipoEnergia = null;
        try {
            HibernateUtil.begin();

            /*
             Query q = HibernateUtil.getSession().createQuery("from CAL_TIPO_PARAMETRO where TIP_PARAMETRO_ID = :tipParametroId");
             q.setLong("tipParametroId",tipParametroId);
             tipoParametro = (TipoParametro)q.uniqueResult();*/

            tipoEnergia = (TipoEnergia) HibernateUtil.getSession().load(TipoEnergia.class, tipEnergiaId);

            Hibernate.initialize(tipoEnergia);
            Hibernate.initialize(tipoEnergia.getCompensacionTensiones());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Tipo de Energía.", e);
        }
        return tipoEnergia;
    }

    @Override
    public TipoEnergia ReadByCod(String codTipEnergia) {
          TipoEnergia tipoEnergia = null;
        try {
            HibernateUtil.begin();

            Query q = HibernateUtil.getSession().createQuery("from TipoEnergia where codTipEnergia = :codTipEnergia and estado = '1'");
            q.setString("codTipEnergia", codTipEnergia);
            tipoEnergia = (TipoEnergia) q.uniqueResult();

            Hibernate.initialize(tipoEnergia);
            Hibernate.initialize(tipoEnergia.getCompensacionTensiones());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Tipo de Energía.", e);
        }
        return tipoEnergia;
    }

    @Override
    public void update(TipoEnergia tipoEnergia) {
            try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(tipoEnergia);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Tipo de Energía.", e);
        }
    }

    @Override
    public void delete(long tipEnergiaId) {
        
        TipoEnergia tipoEnergia = null;
        tipoEnergia = ReadById(tipEnergiaId);

        if (tipoEnergia != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(tipoEnergia);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Tipo de Energía.", e);
            }
        }
    }

    @SuppressWarnings("unchecked")
	@Override
	public List<TipoEnergia> getAllRows() {
		  List<TipoEnergia> tipos=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from TipoEnergia");
	            tipos= (List<TipoEnergia>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Tipo de Energia.", e);
	        } 
		return tipos;
	}
    
    @Override
    public Set<CompensacionTension> selectTipoEnergia(long tipEnergiaId) {
           TipoEnergia tipoEnergia = null;
        tipoEnergia = ReadById(tipEnergiaId);

        return tipoEnergia.getCompensacionTensiones();
    }
    
}
