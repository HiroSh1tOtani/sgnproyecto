package pe.com.indra.calidad.dao;

import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.TipoMedicion;

/**
 *
 * @author Luis
 */
public class TipoMedicionDaoImpl implements TipoMedicionDao {

    private static TipoMedicionDaoImpl instance = null;

    public static TipoMedicionDaoImpl getInstance() {
        if (instance == null) {
            instance = new TipoMedicionDaoImpl();
        }

        return instance;
    }

    public TipoMedicionDaoImpl() {
    }

    @Override
    public void create(TipoMedicion tipoMedicion) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(tipoMedicion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Tipo de Parámetro.", e);
        }
    }

    @Override
    public TipoMedicion ReadById(long tipMedicionId) {
        TipoMedicion tipoMedicion = null;
        try {
            HibernateUtil.begin();

            /*
             Query q = HibernateUtil.getSession().createQuery("from CAL_TIPO_PARAMETRO where TIP_PARAMETRO_ID = :tipParametroId");
             q.setLong("tipParametroId",tipParametroId);
             tipoParametro = (TipoParametro)q.uniqueResult();*/

            tipoMedicion = (TipoMedicion) HibernateUtil.getSession().load(TipoMedicion.class, tipMedicionId);

            Hibernate.initialize(tipoMedicion);
            //Hibernate.initialize(tipoMedicion.getMediciones());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Tipo de Parámetro.", e);
        }
        return tipoMedicion;

    }

    @Override
    public TipoMedicion ReadByCod(String codTipMedicion) {
        TipoMedicion tipoMedicion = null;
        try {
            HibernateUtil.begin();

            Query q = HibernateUtil.getSession().createQuery("from TipoMedicion where codTipMedicion = :codTipMedicion and estado = '1'");
            q.setString("codTipMedicion", codTipMedicion);
            tipoMedicion = (TipoMedicion) q.uniqueResult();

            Hibernate.initialize(tipoMedicion);
            Hibernate.initialize(tipoMedicion.getMediciones());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Tipo de Parámetro.", e);
        }
        return tipoMedicion;
    }

    @Override
    public void update(TipoMedicion tipoMedicion) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(tipoMedicion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Tipo de Parámetro.", e);
        }
    }

    @Override
    public void delete(long tipMedicionId) {
        TipoMedicion tipoMedicion = null;
        tipoMedicion = ReadById(tipMedicionId);

        if (tipoMedicion != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(tipoMedicion);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Tipo de Parámetro.", e);
            }
        }
    }

    @Override
    public Set<Medicion> selectMedicion(long tipMedicionId) {
        TipoMedicion tipoMedicion = null;
        tipoMedicion = ReadById(tipMedicionId);

        return tipoMedicion.getMediciones();
    }

	@Override
	public List<TipoMedicion> getAllRows() {
		List<TipoMedicion> tipoMediciones=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from TipoMedicion");
	            
	            tipoMediciones=(List<TipoMedicion>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Tipo de Mediciones.", e);
	        }
		return tipoMediciones;
	}
	
	@Override
	public List<TipoMedicion> getAllRows1() {
		List<TipoMedicion> tipoMediciones=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from TipoMedicion where estado =:estado");
	            q.setString("estado","1");
	            tipoMediciones=(List<TipoMedicion>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Tipo de Mediciones.", e);
	        }
		return tipoMediciones;
	}
}
