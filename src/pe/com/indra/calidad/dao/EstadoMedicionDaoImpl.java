
package pe.com.indra.calidad.dao;

import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.indra.calidad.entity.CompensacionUnitaria;
import pe.com.indra.calidad.entity.EstadoMedicion;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.TipoParametro;

import java.util.Set;

import org.hibernate.Hibernate;

/**
 *
 * @author Juan
 */
public class EstadoMedicionDaoImpl implements EstadoMedicionDao{

     private static EstadoMedicionDaoImpl instance = null;
    
     public static EstadoMedicionDaoImpl getInstance() {
        if (instance == null) {
            instance = new EstadoMedicionDaoImpl();
        }

        return instance;
    }
     
         public EstadoMedicionDaoImpl() {
    }
     
    @Override
    public void create(EstadoMedicion estadoMedicion) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(estadoMedicion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear el Estado de la Medición.", e);
        } 
    }

    @Override
    public EstadoMedicion ReadById(long estMedicionId) {
         EstadoMedicion estadoMedicion = null;
        try {
            HibernateUtil.begin();
            
            /*
            Query q = HibernateUtil.getSession().createQuery("from CAL_ESTADO_MEDICION where EST_MEDICION_ID = :estMedicionId");
            q.setLong("estMedicionId",estMedicionId);
            estadoMedicion = (TipoParametro)q.uniqueResult();*/
            
            estadoMedicion = (EstadoMedicion) HibernateUtil.getSession().load(EstadoMedicion.class, estMedicionId);
            
            Hibernate.initialize(estadoMedicion);
            //Hibernate.initialize(estadoMedicion.getMediciones()); 
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar el Estado de la Medición.", e);
        } 
        return estadoMedicion;
    }

    @Override
    public EstadoMedicion ReadByCod(String codEstMedicion) {
            EstadoMedicion estadoMedicion = null;
        try {
            HibernateUtil.begin();
            
            Query q = HibernateUtil.getSession().createQuery("from EstadoMedicion where codEstMedicion = :codEstMedicion and estado = '1'");
            q.setString("codEstMedicion",codEstMedicion);
            estadoMedicion = (EstadoMedicion)q.uniqueResult();
            
            Hibernate.initialize(estadoMedicion);
            Hibernate.initialize(estadoMedicion.getMediciones()); 
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Estado de la Medición.", e);
        } 
        return estadoMedicion;
    }

    @Override
    public void update(EstadoMedicion estadoMedicion) {
            try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(estadoMedicion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Estado de la Medición.", e);
        } 

    }

    @Override
    public void delete(long estMedicionId) {
        
        EstadoMedicion estadoMedicion = null;
        estadoMedicion = ReadById(estMedicionId);
        
        if(estadoMedicion != null){               
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(estadoMedicion);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Estado de la Medición.", e); 
            }
        }

    }

    @Override
    public Set<Medicion> selectMedicion(long estMedicionId) {
       /*
        String hql = "from CAL_PARAMETRO_NORMA where PK_TIP_PARAMETRO_ID = :tipParametroId";
        Query query = HibernateUtil.getSession().createQuery(hql);
        query.setLong("tipParametroId",tipParametroId);
        Set<ParametroNorma> results = (Set<ParametroNorma>)query.list();
        
        return results;
    */
        EstadoMedicion estadoMedicion = null;
        estadoMedicion = ReadById(estMedicionId);
        
        return estadoMedicion.getMediciones(); 
    }

	@Override
	public List<EstadoMedicion> getAllRows() {
		List<EstadoMedicion> estadoMediciones=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from EstadoMedicion");
	            
	            estadoMediciones=(List<EstadoMedicion>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Estado de Medicion.", e);
	        }
		return estadoMediciones;
	}
    
	@Override
	public List<EstadoMedicion> getAllRows1() {
		List<EstadoMedicion> estadoMediciones=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from EstadoMedicion where estado =:estado");
	            q.setString("estado","1");
	            estadoMediciones=(List<EstadoMedicion>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Estado de Medicion.", e);
	        }
		return estadoMediciones;
	}

	@Override
	public Set<Medicion> selectMedicionPer(long estMedicionId) {
		EstadoMedicion estadoMedicion = null;
        estadoMedicion = ReadById(estMedicionId);
        
        return estadoMedicion.getPerMediciones(); 
	}
}
