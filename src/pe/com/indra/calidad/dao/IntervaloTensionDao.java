
package pe.com.indra.calidad.dao;

import java.util.List;
import java.util.Set;


import pe.com.indra.calidad.entity.EstadoIntervalo;
import pe.com.indra.calidad.entity.IntervaloTension;
import pe.com.indra.calidad.entity.Medicion;

/**
 *
 * @author Luis
 */
public interface IntervaloTensionDao {
    
    public void create(IntervaloTension intervaloTension);

    public IntervaloTension ReadById(long intTensionId);

    public void update(IntervaloTension intervaloTension);

    public void delete(long intTensionId);

    public EstadoIntervalo selectEstadoIntervalo(long intTensionId);
    public Medicion selectMedicion(long intTensionId);
    
    public List<IntervaloTension> getIntervaloxMedicion(long medicionId);
    public List<IntervaloTension> getIntervaloxMedicion();
    
    public List<IntervaloTension> getAllRows(long medicionId);
    public double getMaxTension(long medicionId);
    public double getMinTension(long medicionId);
    
}
