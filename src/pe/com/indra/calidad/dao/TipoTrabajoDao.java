package pe.com.indra.calidad.dao;

/**
 *
 * @author Luis
 */
import java.util.List;

import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.TipoTrabajo;

import java.util.Set;

public interface TipoTrabajoDao {

    public void create(TipoTrabajo tipoTrabajo);

    public TipoTrabajo ReadById(long tipTrabajoId);

    public TipoTrabajo ReadByCod(String codTipTrabajo);

    public void update(TipoTrabajo tipoTrabajo);

    public void delete(long tipTrabajoId);

    public Set<Medicion> selectMedicion(long tipTrabajoId);
    
    public List<TipoTrabajo> getAllRows();
    
    public List<TipoTrabajo> getAllRows1();
    
   }
