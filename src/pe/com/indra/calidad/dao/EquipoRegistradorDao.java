
package pe.com.indra.calidad.dao;

/**
 *
 * @author Luis
 */
import java.util.List;

import pe.com.indra.calidad.entity.CompensacionUnitaria;
import pe.com.indra.calidad.entity.EquipoRegistrador;
import pe.com.indra.calidad.entity.Medicion;

import java.util.Set;

public interface EquipoRegistradorDao {
    
    public void create(EquipoRegistrador equipoRegistrador);

    public EquipoRegistrador ReadById(long equRegistradorId);
    
    public EquipoRegistrador ReadByCod(String codEquRegistrador);

    public void update(EquipoRegistrador equipoRegistrador);

    public void delete(long equRegistradorId);

    public Set<Medicion> selectMedicion(long equRegistradorId);
    
    public List<EquipoRegistrador> getAllRows();
    
    public List<EquipoRegistrador> getAllRows1();
    
}
