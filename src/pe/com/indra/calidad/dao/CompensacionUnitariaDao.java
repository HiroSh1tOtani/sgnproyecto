package pe.com.indra.calidad.dao;

import java.util.List;

import pe.com.indra.calidad.entity.CompensacionUnitaria;
import pe.com.indra.calidad.entity.TipoParametro;
import pe.com.indra.calidad.entity.TipoTrabajo;

public interface CompensacionUnitariaDao {
    public void create(CompensacionUnitaria compensacionUnitaria);
    public CompensacionUnitaria ReadById(long comUnitariaId);
	public CompensacionUnitaria ReadByCod(String codComUnitaria);
	public void update(CompensacionUnitaria compensacionUnitaria);
	public void delete(long comUnitariaId);
    public TipoParametro selectTipoParametro(long comUnitariaId);
    public List<CompensacionUnitaria> getAllRows();
    public List<CompensacionUnitaria> getAllRows1();
}
