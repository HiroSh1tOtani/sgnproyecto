package pe.com.indra.calidad.dao;

/**
 *
 * @author Luis
 */
import java.util.List;


import pe.com.indra.calidad.entity.Localidad;
import pe.com.indra.calidad.entity.Medicion;

import java.util.Set;

public interface LocalidadDao {

    public void create(Localidad localidad);

    public Localidad ReadById(long localidadId);

    public Localidad ReadByCod(String codLocalidad);

    public void update(Localidad localidad);

    public void delete(long localidadId);

    public Set<Medicion> selectMedicion(long localidadId);
    
    public List<Localidad> getAllRows();
    
    public List<Localidad> getAllRows1();
}
