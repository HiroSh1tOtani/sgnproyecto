
package pe.com.indra.calidad.dao;

import java.util.List;




import pe.com.indra.calidad.entity.Empresa;
import pe.com.indra.calidad.entity.Localidad;


/**
 *
 * @author Luis
 */
public interface EmpresaDao {
    
	public List<Empresa> getAllRows();
	
	public Empresa readByCod(String empresa);
    public void update(Empresa empresa);
    
     
}
