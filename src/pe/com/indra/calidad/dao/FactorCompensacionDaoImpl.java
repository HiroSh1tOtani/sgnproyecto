package pe.com.indra.calidad.dao;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import java.util.Set;

import pe.com.indra.calidad.entity.FactorCompensacion;
import pe.com.indra.calidad.entity.Medicion;

/**
 *
 * @author Luis
 */
public class FactorCompensacionDaoImpl implements FactorCompensacionDao {

    private static FactorCompensacionDaoImpl instance = null;

    public static FactorCompensacionDaoImpl getInstance() {
        if (instance == null) {
            instance = new FactorCompensacionDaoImpl();
        }

        return instance;
    }

    public FactorCompensacionDaoImpl() {
    }

    @Override
    public void create(FactorCompensacion factorCompensacion) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(factorCompensacion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Factor de Compensación.", e);
        }
    }

    @Override
    public FactorCompensacion ReadById(long facCompensacionId) {
        FactorCompensacion factorCompensacion = null;
        try {
            HibernateUtil.begin();

            /*
             Query q = HibernateUtil.getSession().createQuery("from CAL_PARAMETRO_NORMA where PAR_NORMA_ID = :parNormaId");
             q.setLong("parNormaId",parNormaId);
             parametroNorma = (ParametroNorma)q.uniqueResult();*/

            factorCompensacion = (FactorCompensacion) HibernateUtil.getSession().load(FactorCompensacion.class, facCompensacionId);

            Hibernate.initialize(factorCompensacion);
            Hibernate.initialize(factorCompensacion.getMedicion());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Factor de Compesanción.", e);
        }
        return factorCompensacion;
    }

    @Override
    public void update(FactorCompensacion factorCompensacion) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(factorCompensacion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Factor de Compesanción.", e);
        }
    }

    @Override
    public void delete(long facCompensacionId) {
        FactorCompensacion factorCompensacion = null;
        factorCompensacion = ReadById(facCompensacionId);

        if (factorCompensacion != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(factorCompensacion);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Factor de Compesanción.", e);
            }
        }
    }

    @Override
    public Medicion selectMedicion(long facCompensacionId) {
        FactorCompensacion factorCompensacion = null;
        factorCompensacion = ReadById(facCompensacionId);

        return factorCompensacion.getMedicion();
    }
}
