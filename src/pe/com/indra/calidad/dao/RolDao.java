package pe.com.indra.calidad.dao;

import java.util.List;

import pe.com.indra.calidad.entity.Rol;
import pe.com.indra.calidad.entity.Usuario;

public interface RolDao {

	void create(Rol rol);
	void update(Rol rol);
	void delete(String id);
	Rol ReadById(String id);
	List<Rol> getAllRows();
	
}
