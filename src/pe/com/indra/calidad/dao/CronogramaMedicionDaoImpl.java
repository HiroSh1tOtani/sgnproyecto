package pe.com.indra.calidad.dao;

/**
 *
 * @author Luis
 */
import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.indra.calidad.entity.CronogramaMedicion;
import pe.com.indra.calidad.entity.Cuadrilla;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.ParametroNorma;

public class CronogramaMedicionDaoImpl implements CronogramaMedicionDao {

    private static CronogramaMedicionDaoImpl instance = null;

    public static CronogramaMedicionDaoImpl getInstance() {
        if (instance == null) {
            instance = new CronogramaMedicionDaoImpl();
        }

        return instance;
    }

    public CronogramaMedicionDaoImpl() {
    }

    @Override
    public void create(CronogramaMedicion cronogramaMedicion) {
         try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(cronogramaMedicion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Cronograma de Medicion.", e);
        }
    }

    @Override
    public CronogramaMedicion ReadById(long croMedicionId) {
           CronogramaMedicion cronogramaMedicion = null;
        try {
            HibernateUtil.begin();

            /*
             Query q = HibernateUtil.getSession().createQuery("from CAL_CRONOGRAMA_MEDICION where CRO_MEDICION_ID = :croMedicionId");
             q.setLong("croMedicionId",croMedicionId);
             cronogramaMedicion = (CronogramaMedicion)q.uniqueResult();*/

            cronogramaMedicion = (CronogramaMedicion) HibernateUtil.getSession().load(CronogramaMedicion.class, croMedicionId);

            Hibernate.initialize(cronogramaMedicion);
            Hibernate.initialize(cronogramaMedicion.getCuadrilla());
            Hibernate.initialize(cronogramaMedicion.getMedicion());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            //throw new HibernateException("No se puede encontrar Cronograma de Medicion.", e);
            throw new HibernateException(e.getMessage(), e);
        }
        return cronogramaMedicion;
    }

    @Override
    public void update(CronogramaMedicion cronogramaMedicion) {
           try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(cronogramaMedicion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Cronograma de Medicion.", e);
        }
    }

    @Override
    public void delete(long croMedicionId) {
        
        CronogramaMedicion cronogramaMedicion = null;
        cronogramaMedicion = ReadById(croMedicionId);

        if (cronogramaMedicion != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(cronogramaMedicion);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Cronograma de Medicion.", e);
            }
        }
    }

    @Override
    public Cuadrilla selectCuadrilla(long croMedicionId) {
            CronogramaMedicion cronogramaMedicion = null;
        cronogramaMedicion = ReadById(croMedicionId);

        return cronogramaMedicion.getCuadrilla();
    }

    @Override
    public Medicion selectMedicion(long croMedicionId) {
        CronogramaMedicion cronogramaMedicion = null;
        cronogramaMedicion = ReadById(croMedicionId);

        return cronogramaMedicion.getMedicion();
    }
    
    @SuppressWarnings("unchecked")
	@Override
	
    public List<CronogramaMedicion> getCronogramaxCampana(long camMedicionId) {
		 List<CronogramaMedicion> cronogramasMedicion=null;
		 try {
	            HibernateUtil.begin();
                System.out.println("camMedicionId="+camMedicionId);
	            /*Query q = HibernateUtil.getSession().createQuery("from CronogramaMedicion m where m.estado = :estado and m.medicion.campanaMedicion.camMedicionId = :camMedicionId ")
	            		                                      .setString("estado", "1") 
	            		                                       .setLong("camMedicionId", new Long(camMedicionId).longValue());*/
                
                Query q = HibernateUtil.getSession().createQuery("from CronogramaMedicion m where m.estado = '1' and m.medicion.campanaMedicion.camMedicionId = " + Long.toString(camMedicionId));
	            
	            cronogramasMedicion=(List<CronogramaMedicion>) q.list();	            
	            
	            System.out.println("size="+cronogramasMedicion.size());
	            
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Muestras para la Campa�a.", e);
	        }
		return cronogramasMedicion;
	}

	@Override
	public List<CronogramaMedicion> getCronogramaxCampana() {
		List<CronogramaMedicion> cronogramasMedicion=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from CronogramaMedicion");
	            
	            cronogramasMedicion=(List<CronogramaMedicion>) q.list();	            
	            
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar cronogramas para la Campa�a.", e);
	        }
		return cronogramasMedicion;
	}
}
