package pe.com.indra.calidad.dao;

import pe.com.indra.calidad.entity.CronogramaMedicion;
import pe.com.indra.calidad.entity.Cuadrilla;

import java.util.List;
import java.util.Set;

/**
 *
 * @author Luis
 */
public interface CuadrillaDao {

    public void create(Cuadrilla cuadrilla);

    public Cuadrilla ReadById(long cuadrillaId);

    public Cuadrilla ReadByCod(String codCuadrilla);

    public void update(Cuadrilla cuadrilla);

    public void delete(long cuadrillaId);

    public Set<CronogramaMedicion> selectCronogramaMedicion(long cuadrillaId);
    
    public List<Cuadrilla> getAllRows();
    
    
}
