package pe.com.indra.calidad.dao;

import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.indra.calidad.entity.CompensacionUnitaria;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.TipoAlimentacion;
import pe.com.indra.calidad.entity.TipoEnergia;

/**
 *
 * @author Luis
 */
public class TipoAlimentacionDaoImpl implements TipoAlimentacionDao {

    private static TipoAlimentacionDaoImpl instance = null;

    public static TipoAlimentacionDaoImpl getInstance() {
        if (instance == null) {
            instance = new TipoAlimentacionDaoImpl();
        }

        return instance;
    }

    public TipoAlimentacionDaoImpl() {
    }

    @Override
    public void create(TipoAlimentacion tipoAlimentacion) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(tipoAlimentacion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Tipo de Alimentación.", e);
        }
    }

    @Override
    public TipoAlimentacion ReadById(long tipAlimentacionId) {
        TipoAlimentacion tipoAlimentacion = null;
        try {
            HibernateUtil.begin();

            /*
             Query q = HibernateUtil.getSession().createQuery("from CAL_TIPO_PARAMETRO where TIP_PARAMETRO_ID = :tipParametroId");
             q.setLong("tipParametroId",tipParametroId);
             tipoParametro = (TipoParametro)q.uniqueResult();*/

            tipoAlimentacion = (TipoAlimentacion) HibernateUtil.getSession().load(TipoAlimentacion.class, tipAlimentacionId);

            Hibernate.initialize(tipoAlimentacion);
            //Hibernate.initialize(tipoAlimentacion.getMediciones());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Tipo de Alimtentacion.", e);
        }
        return tipoAlimentacion;
    }

    @Override
    public TipoAlimentacion ReadByCod(String codTipAlimentacion) {
        TipoAlimentacion tipoAlimentacion = null;
        try {
            HibernateUtil.begin();

            Query q = HibernateUtil.getSession().createQuery("from TipoAlimentacion where codTipAlimentacion = :codTipAlimentacion and estado = '1'");
            q.setString("codTipAlimentacion", codTipAlimentacion);
            tipoAlimentacion = (TipoAlimentacion) q.uniqueResult();

            Hibernate.initialize(tipoAlimentacion);
            Hibernate.initialize(tipoAlimentacion.getMediciones());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Tipo de Alimentacion.", e);
        }
        return tipoAlimentacion;
    }

    @Override
    public void update(TipoAlimentacion tipoAlimentacion) {

        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(tipoAlimentacion);
            HibernateUtil.commit();
            HibernateUtil.close();
        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Tipo de Alimentacion.", e);
        }
    }

    @Override
    public void delete(long tipAlimentacionId) {
        TipoAlimentacion tipoAlimentacion = null;
        tipoAlimentacion = ReadById(tipAlimentacionId);

        if (tipoAlimentacion != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(tipoAlimentacion);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Tipo de Alimentacion.", e);
            }
        }

    }

    @Override
    public Set<Medicion> selectMedicion(long tipAlimentacionId) {
        TipoAlimentacion tipoAlimentacion = null;
        tipoAlimentacion = ReadById(tipAlimentacionId);

        return tipoAlimentacion.getMediciones();
    }

	@Override
	public List<TipoAlimentacion> getAllRows() {
		List<TipoAlimentacion> tipoAlimentaciones=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from TipoAlimentacion");
	            
	            tipoAlimentaciones=(List<TipoAlimentacion>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Tipo Alimentacion.", e);
	        }
		return tipoAlimentaciones;
	}
	
	@Override
	public List<TipoAlimentacion> getAllRows1() {
		List<TipoAlimentacion> tipoAlimentaciones=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from TipoAlimentacion where estado =:estado");
	            q.setString("estado","1");
	            tipoAlimentaciones=(List<TipoAlimentacion>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Tipo Alimentacion.", e);
	        }
		return tipoAlimentaciones;
	}
    
   
}
