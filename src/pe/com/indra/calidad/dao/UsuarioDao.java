package pe.com.indra.calidad.dao;

import java.util.List;

import pe.com.indra.calidad.entity.Rol;
import pe.com.indra.calidad.entity.Usuario;

public interface UsuarioDao {
	void create(Usuario user);
	void update(Usuario user);
	void delete(String id);
	Usuario ReadById(String id);
	List<Usuario> getAllRows();
}
