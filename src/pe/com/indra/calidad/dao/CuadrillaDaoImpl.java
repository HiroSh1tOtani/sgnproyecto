package pe.com.indra.calidad.dao;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import java.util.List;
import java.util.Set;

import pe.com.indra.calidad.entity.CronogramaMedicion;
import pe.com.indra.calidad.entity.Cuadrilla;

/**
 *
 * @author Luis
 */
public class CuadrillaDaoImpl implements CuadrillaDao {

    private static CuadrillaDaoImpl instance = null;

    public static CuadrillaDaoImpl getInstance() {
        if (instance == null) {
            instance = new CuadrillaDaoImpl();
        }

        return instance;
    }

    public CuadrillaDaoImpl() {
    }

    @Override
    public void create(Cuadrilla cuadrilla) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(cuadrilla);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Cuadrilla.", e);
        }
    }

    @Override
    public Cuadrilla ReadById(long cuadrillaId) {
        Cuadrilla cuadrilla = null;
        try {
            HibernateUtil.begin();

            /*
             Query q = HibernateUtil.getSession().createQuery("from CAL_TIPO_PARAMETRO where TIP_PARAMETRO_ID = :tipParametroId");
             q.setLong("tipParametroId",tipParametroId);
             tipoParametro = (TipoParametro)q.uniqueResult();*/

            cuadrilla = (Cuadrilla) HibernateUtil.getSession().load(Cuadrilla.class, cuadrillaId);

            Hibernate.initialize(cuadrilla);
            //Hibernate.initialize(cuadrilla.getCronogramaMedicion());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Cuadrilla.", e);
        }
        return cuadrilla;
    }

    @Override
    public Cuadrilla ReadByCod(String codCuadrilla) {
        Cuadrilla cuadrilla = null;
        try {
            HibernateUtil.begin();

            Query q = HibernateUtil.getSession().createQuery("from Cuadrilla where codCuadrilla = :codCuadrilla and estado = '1'");
            q.setString("codCuadrilla", codCuadrilla);
            cuadrilla = (Cuadrilla) q.uniqueResult();

            Hibernate.initialize(cuadrilla);
            Hibernate.initialize(cuadrilla.getCronogramaMedicion());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Cuadrilla.", e);
        }
        return cuadrilla;
    }

    @Override
    public void update(Cuadrilla cuadrilla) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(cuadrilla);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Cuadrilla.", e);
        }
    }

    @Override
    public void delete(long cuadrillaId) {

        Cuadrilla cuadrilla = null;
        cuadrilla = ReadById(cuadrillaId);

        if (cuadrilla != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(cuadrilla);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Cuadrilla.", e);
            }
        }
    }

    @Override
    public Set<CronogramaMedicion> selectCronogramaMedicion(long cuadrillaId) {
        Cuadrilla cuadrilla = null;
        cuadrilla = ReadById(cuadrillaId);

        return cuadrilla.getCronogramaMedicion();
    }
    
    @Override
	public List<Cuadrilla> getAllRows() {
		List<Cuadrilla> cuadrillas=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from Cuadrilla where estado =:estado");
	            q.setString("estado","1");
	            cuadrillas=(List<Cuadrilla>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();
 
	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Cuadrilla .", e);
	        }
		return cuadrillas;
	}
}

