/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.indra.calidad.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.indra.calidad.entity.CampanaMedicion;
import pe.com.indra.calidad.entity.CompensacionUnitaria;
import pe.com.indra.calidad.entity.ParametroNorma;
import pe.com.indra.calidad.entity.TipoParametro;

import java.util.Set;

import org.hibernate.Hibernate;

/**
 *
 * @author Juan
 */
public class TipoParametroDaoImpl implements TipoParametroDao {

    private static TipoParametroDaoImpl instance = null;

    public static TipoParametroDaoImpl getInstance() {
        if (instance == null) {
            instance = new TipoParametroDaoImpl();
        }

        return instance;
    }

    public TipoParametroDaoImpl() {
    }

    @Override
    public void create(TipoParametro tipoParametro) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(tipoParametro);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Tipo de Parámetro.", e);
        } 
    }

    @Override
    public TipoParametro ReadById(long tipParametroId) {
        TipoParametro tipoParametro = null;
        try {
            HibernateUtil.begin();
            
            /*
            Query q = HibernateUtil.getSession().createQuery("from CAL_TIPO_PARAMETRO where TIP_PARAMETRO_ID = :tipParametroId");
            q.setLong("tipParametroId",tipParametroId);
            tipoParametro = (TipoParametro)q.uniqueResult();*/
            
            tipoParametro = (TipoParametro) HibernateUtil.getSession().load(TipoParametro.class, tipParametroId);
            
            Hibernate.initialize(tipoParametro);
            Hibernate.initialize(tipoParametro.getParametroNormas()); 
            Hibernate.initialize(tipoParametro.getCampanaMediciones());
            Hibernate.initialize(tipoParametro.getCompensacionUnitarias());
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Tipo de Parámetro.", e);
        } 
        return tipoParametro;
    }

    @Override
    public TipoParametro ReadByCod(String codTipParametro) {
        TipoParametro tipoParametro = null;
        try {
            HibernateUtil.begin();
            
            Query q = HibernateUtil.getSession().createQuery("from TipoParametro where codTipParametro = :codTipParametro and estado = '1'");
            q.setString("codTipParametro",codTipParametro);
            tipoParametro = (TipoParametro)q.uniqueResult();
            
            Hibernate.initialize(tipoParametro);
            Hibernate.initialize(tipoParametro.getParametroNormas()); 
            Hibernate.initialize(tipoParametro.getCampanaMediciones());
            Hibernate.initialize(tipoParametro.getCompensacionUnitarias());
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Tipo de Parámetro.", e);
        } 
        return tipoParametro;
    }

    @Override
    public void update(TipoParametro tipoParametro) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(tipoParametro);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Tipo de Parámetro.", e);
        } 
    }

    @Override
    public void delete(long tipParametroId) {
        
        TipoParametro tipoParametro = null;
        tipoParametro = ReadById(tipParametroId);
        
        if(tipoParametro != null){               
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(tipoParametro);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Tipo de Parámetro.", e); 
            }
        }
    }

    @Override
    public Set<ParametroNorma> selectParametroNormas(long tipParametroId) {
    /*
        String hql = "from CAL_PARAMETRO_NORMA where PK_TIP_PARAMETRO_ID = :tipParametroId";
        Query query = HibernateUtil.getSession().createQuery(hql);
        query.setLong("tipParametroId",tipParametroId);
        Set<ParametroNorma> results = (Set<ParametroNorma>)query.list();
        
        return results;
    */
        TipoParametro tipoParametro = null;
        tipoParametro = ReadById(tipParametroId);
        
        return tipoParametro.getParametroNormas();
    }
    @Override
    public Set<CampanaMedicion> selectCampanaMediciones(long tipParametroId) {
    /*
        String hql = "from CAL_PARAMETRO_NORMA where PK_TIP_PARAMETRO_ID = :tipParametroId";
        Query query = HibernateUtil.getSession().createQuery(hql);
        query.setLong("tipParametroId",tipParametroId);
        Set<ParametroNorma> results = (Set<ParametroNorma>)query.list();
        
        return results;
    */
        TipoParametro tipoParametro = null;
        tipoParametro = ReadById(tipParametroId);
        
        return tipoParametro.getCampanaMediciones();
    }

	@SuppressWarnings("unchecked")
	@Override
	public List<TipoParametro> getAllRows() {
		  List<TipoParametro> tipos=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from TipoParametro");
	            tipos= (List<TipoParametro>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Tipo de Parámetro.", e);
	        } 
		return tipos;
	}
	
	@Override
	public List<TipoParametro> getAllRows1() {
		  List<TipoParametro> tipos=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from TipoParametro where estado =:estado");
	            q.setString("estado","1");
	            tipos= (List<TipoParametro>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Tipo de Parámetro.", e);
	        } 
		return tipos;
	}
	
	@Override
	public Set<CompensacionUnitaria> selectCompensacionUnitarias(long tipParametroId) {
        TipoParametro tipoParametro = null;
        tipoParametro = ReadById(tipParametroId);
        
        return tipoParametro.getCompensacionUnitarias();
	}
	
	
	
}
