package pe.com.indra.calidad.dao;

import pe.com.indra.calidad.entity.CompensacionTension;
import pe.com.indra.calidad.entity.TipoEnergia;


import java.util.List;
import java.util.Set;

/**
 *
 * @author Luis
 */
public interface TipoEnergiaDao {

    public void create(TipoEnergia tipoEnergia);

    public TipoEnergia ReadById(long tipEnergiaId);

    public TipoEnergia ReadByCod(String codTipEnergia);

    public void update(TipoEnergia tipoEnergia);

    public void delete(long tipEnergiaId);
    
    public Set<CompensacionTension> selectTipoEnergia(long tipEnergiaId);
    
    public List<TipoEnergia> getAllRows();
}
