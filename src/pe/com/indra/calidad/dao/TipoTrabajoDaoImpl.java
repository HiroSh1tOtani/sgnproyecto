package pe.com.indra.calidad.dao;

/**
 *
 * @author Luis
 */
import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.TipoParametro;
import pe.com.indra.calidad.entity.TipoTrabajo;

public class TipoTrabajoDaoImpl implements TipoTrabajoDao {

    private static TipoTrabajoDaoImpl instance = null;

    public static TipoTrabajoDaoImpl getInstance() {
        if (instance == null) {
            instance = new TipoTrabajoDaoImpl();
        }

        return instance;
    }

    public TipoTrabajoDaoImpl() {
    }

    @Override
    public void create(TipoTrabajo tipoTrabajo) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(tipoTrabajo);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Tipo de Trabajo.", e);
        }
    }

    @Override
    public TipoTrabajo ReadById(long tipTrabajoId) {
        TipoTrabajo tipoTrabajo = null;
        try {
            HibernateUtil.begin();

            /*
             Query q = HibernateUtil.getSession().createQuery("from CAL_TIPO_PARAMETRO where TIP_PARAMETRO_ID = :tipParametroId");
             q.setLong("tipParametroId",tipParametroId);
             tipoParametro = (TipoParametro)q.uniqueResult();*/

            tipoTrabajo = (TipoTrabajo) HibernateUtil.getSession().load(TipoTrabajo.class, tipTrabajoId);

            Hibernate.initialize(tipoTrabajo);
            //Hibernate.initialize(tipoTrabajo.getMediciones());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Tipo de Alimtentación.", e);
        }
        return tipoTrabajo;
    }

    @Override
    public TipoTrabajo ReadByCod(String codTipTrabajo) {
        TipoTrabajo tipoTrabajo = null;
        try {
            HibernateUtil.begin();

            Query q = HibernateUtil.getSession().createQuery("from TipoTrabajo where codTipTrabajo = :codTipTrabajo and estado = '1'");
            q.setString("codTipTrabajo", codTipTrabajo);
            tipoTrabajo = (TipoTrabajo) q.uniqueResult();

            Hibernate.initialize(tipoTrabajo);
            Hibernate.initialize(tipoTrabajo.getMediciones());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Tipo de Trabajo.", e);
        }
        return tipoTrabajo;
    }

    @Override
    public void update(TipoTrabajo tipoTrabajo) {

        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(tipoTrabajo);
            HibernateUtil.commit();
            HibernateUtil.close();
        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Tipo de Trabajo.", e);
        }
    }

    @Override
    public void delete(long tipTrabajoId) {
        TipoTrabajo tipoTrabajo = null;
        tipoTrabajo = ReadById(tipTrabajoId);

        if (tipoTrabajo != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(tipoTrabajo);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Tipo de Trabajo.", e);
            }
        }

    }

    @Override
    public Set<Medicion> selectMedicion(long tipTrabajoId) {
        TipoTrabajo tipoTrabajo = null;
        tipoTrabajo = ReadById(tipTrabajoId);

        return tipoTrabajo.getMediciones();

    }

	@Override
	public List<TipoTrabajo> getAllRows() {
		 List<TipoTrabajo> listaTipoTrabajo = null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from TipoTrabajo");
	            listaTipoTrabajo = (List<TipoTrabajo>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Tipo de Trabajo.", e);
	        } 
		return listaTipoTrabajo;
	}
	
	@Override
	public List<TipoTrabajo> getAllRows1() {
		 List<TipoTrabajo> listaTipoTrabajo = null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from TipoTrabajo where estado =:estado");
	            q.setString("estado","1");
	            listaTipoTrabajo = (List<TipoTrabajo>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Tipo de Trabajo.", e);
	        } 
		return listaTipoTrabajo;
	}
}
