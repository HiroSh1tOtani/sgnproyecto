package pe.com.indra.calidad.dao;

import java.util.List;
import java.util.Set;

import pe.com.indra.calidad.entity.CampanaMedicion;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.PeriodoMedicion;
import pe.com.indra.calidad.entity.TipoParametro;

/**
 *
 * @author Luis
 */
public interface CampanaMedicionDao {

    public void create(CampanaMedicion campanaMedicion);

    public CampanaMedicion ReadById(long camMedicionId);

    public void update(CampanaMedicion campanaMedicion);

    public void delete(long camMedicionId);

    public TipoParametro selectTipoParametro(long camMedicionId);
    
    public PeriodoMedicion selectPeriodoMedicion(long camMedicionId);
    
    public Set<Medicion> selectMedicion(long camMedicionId);
    
    public List<CampanaMedicion> getAllRows();
    
    public List<CampanaMedicion> getAllRows1();
}
