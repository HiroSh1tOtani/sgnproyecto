package pe.com.indra.calidad.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.indra.calidad.entity.Rol;
import pe.com.indra.calidad.entity.Usuario;

public class RolDaoImpl implements RolDao {

	@Override
	public void create(Rol rol) {
		try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(rol);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear el rol.", e);
        }
	}

	@Override
	public void update(Rol rol) {
		try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(rol);
            HibernateUtil.commit();
            HibernateUtil.close();
        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar el usuario.", e);
        }
	}

	@Override
	public void delete(String id) {
		 Rol rol = null;
	     rol = ReadById(id);
	     if (rol != null) {
	     try {
	          HibernateUtil.begin();
	          HibernateUtil.getSession().delete(rol);
	          HibernateUtil.commit();
	          HibernateUtil.close();
	        } catch (HibernateException e) {
	           HibernateUtil.rollback();
	           throw new HibernateException("No se puede eliminar el rol.", e);
	        }
	     }

	}
	
	@Override
	public Rol ReadById(String id) {
		Rol rol = null;
        try {
            HibernateUtil.begin();
            //rol = (Rol) HibernateUtil.getSession().load(Rol.class, id);
            rol = (Rol) HibernateUtil.getSession().get(Rol.class, id);
            HibernateUtil.commit();
            HibernateUtil.close();
        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar el rol.", e);
        }
        return rol;
	}

	@Override
	public List<Rol> getAllRows() {
		 List<Rol> roles = null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from Rol");
	            roles = (List<Rol>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();
           } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar los Roles.", e);
	        } 
		return roles;
	}

}
