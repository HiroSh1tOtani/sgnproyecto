
package pe.com.indra.calidad.dao;

/**
 *
 * @author Luis
 */
import java.util.List;

import pe.com.indra.calidad.entity.CompensacionUnitaria;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.TipoPunto;

import java.util.Set;

public interface TipoPuntoDao {
    
        public void create(TipoPunto tipoPunto);

        public TipoPunto ReadById(long tipPuntoId);
        
        public TipoPunto ReadByCod(String codTipPunto);

        public void update(TipoPunto tipoPunto);

        public void delete(long tipPuntoId);

        public Set<Medicion> selectMedicion(long tipPuntoId);
        
        public List<TipoPunto> getAllRows();
        
        public List<TipoPunto> getAllRows1();
}
