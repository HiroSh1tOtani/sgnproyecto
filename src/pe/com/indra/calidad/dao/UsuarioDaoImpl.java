package pe.com.indra.calidad.dao;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.indra.calidad.entity.Rol;
import pe.com.indra.calidad.entity.TipoTrabajo;
import pe.com.indra.calidad.entity.Usuario;

public class UsuarioDaoImpl implements UsuarioDao {

	@Override
	public void create(Usuario user) {
		try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(user);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear el usuario.", e);
        }
        
	}

	@Override
	public void update(Usuario user) {
		try {
            HibernateUtil.begin();
            
            HibernateUtil.getSession().update(user);
            //HibernateUtil.getSession().update(user.getRols());
            
            //HibernateUtil.getSession().merge(user);
            HibernateUtil.commit();
            HibernateUtil.close();
        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar el usuario.", e);
        }

	}

	@Override
	public void delete(String id) {
	 Usuario user = null;
     user = ReadById(id);
     if (user != null) {
     try {
          HibernateUtil.begin();
          HibernateUtil.getSession().delete(user);
          HibernateUtil.commit();
          HibernateUtil.close();
        } catch (HibernateException e) {
           HibernateUtil.rollback();
           throw new HibernateException("No se puede eliminar elusuario.", e);
        }
     }
	}

	

	@Override
	public Usuario ReadById(String id) {
		Usuario user = null;
        try {
            HibernateUtil.begin();
            user = (Usuario) HibernateUtil.getSession().load(Usuario.class, id);
            Hibernate.initialize(user);
            Hibernate.initialize(user.getRols());
            
            HibernateUtil.commit();
            HibernateUtil.close();
        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar el usuario.", e);
        }
        return user;
	}

	@Override
	public List<Usuario> getAllRows() {
		 List<Usuario> usuarios = null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from Usuario");
	            usuarios = (List<Usuario>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar los Usuarios.", e);
	        } 
		return usuarios;
	}

}
