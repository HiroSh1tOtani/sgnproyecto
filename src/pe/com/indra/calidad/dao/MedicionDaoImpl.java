package pe.com.indra.calidad.dao;

import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.indra.calidad.entity.CampanaMedicion;
import pe.com.indra.calidad.entity.CompensacionTension;
import pe.com.indra.calidad.entity.CompensacionUnitaria;
import pe.com.indra.calidad.entity.CronogramaMedicion;
import pe.com.indra.calidad.entity.EquipoRegistrador;
import pe.com.indra.calidad.entity.EstadoMedicion;
import pe.com.indra.calidad.entity.FactorCompensacion;
import pe.com.indra.calidad.entity.IntervaloFRTension;
import pe.com.indra.calidad.entity.IntervaloTension;
import pe.com.indra.calidad.entity.Localidad;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.ParametroMedido;
import pe.com.indra.calidad.entity.TipoAlimentacion;
import pe.com.indra.calidad.entity.TipoMedicion;
import pe.com.indra.calidad.entity.TipoPunto;
import pe.com.indra.calidad.entity.TipoTrabajo;

/**
 *
 * @author Luis
 */
public class MedicionDaoImpl implements MedicionDao {

    private static MedicionDaoImpl instance = null;

    public static MedicionDaoImpl getInstance() {
        if (instance == null) {
            instance = new MedicionDaoImpl();
        }

        return instance;
    }

    public MedicionDaoImpl() {
    }

    @Override
    public void create(Medicion medicion) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(medicion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Medición.", e);
        }
    }

    @Override
    public Medicion ReadById(long medicionId) {
        Medicion medicion = null;
        try {
            HibernateUtil.begin();

            /*
             Query q = HibernateUtil.getSession().createQuery("from CAL_PARAMETRO_NORMA where PAR_NORMA_ID = :parNormaId");
             q.setLong("parNormaId",parNormaId);
             parametroNorma = (ParametroNorma)q.uniqueResult();*/

            medicion = (Medicion) HibernateUtil.getSession().load(Medicion.class, medicionId);

            Hibernate.initialize(medicion);
            Hibernate.initialize(medicion.getCampanaMedicion());
            Hibernate.initialize(medicion.getTipoMedicion());
            Hibernate.initialize(medicion.getTipoPunto());
            Hibernate.initialize(medicion.getLocalidad());
            Hibernate.initialize(medicion.getTipoAlimentacion());
            Hibernate.initialize(medicion.getTipoTrabajo());
            Hibernate.initialize(medicion.getEquipoRegistrador());
            Hibernate.initialize(medicion.getParametroMedido());
            Hibernate.initialize(medicion.getCronogramaMedicion());
            Hibernate.initialize(medicion.getFactorCompensacion());
            Hibernate.initialize(medicion.getIntervaloFRTension());
            //Hibernate.initialize(medicion.getIntervaloTension());
            Hibernate.initialize(medicion.getCompensacionTension());
            

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException(e.getMessage(), e);//"No se puede encontrar Medici�n."
        }
        return medicion;
    }

    @Override
    public void update(Medicion medicion) {
        try {
            HibernateUtil.begin();
            
            HibernateUtil.getSession().update(medicion.getTipoAlimentacion());
            HibernateUtil.getSession().update(medicion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
        	
    		HibernateUtil.rollback();
			throw new HibernateException(e.getMessage(), e);
    
        }
    }

    @Override
    public void delete(long medicionId) {
        Medicion medicion = null;
        medicion = ReadById(medicionId);

        if (medicion != null) {
            try {
                HibernateUtil.begin();
                medicion.setTipoAlimentacion(null);
                HibernateUtil.getSession().delete(medicion);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Medici�n.", e);
            }
        }
    }

    @Override
    public EstadoMedicion selectEstadoMedicion(long medicionId) {
        Medicion medicion = null;
        medicion = ReadById(medicionId);

        return medicion.getEstadoMedicion();
    }

    @Override
    public ParametroMedido selectParametroMedido(long medicionId) {
        Medicion medicion = null;
        medicion = ReadById(medicionId);

        return medicion.getParametroMedido();
    }

    @Override
    public EquipoRegistrador selectEquipoRegistrador(long medicionId) {
        Medicion medicion = null;
        medicion = ReadById(medicionId);

        return medicion.getEquipoRegistrador();
    }

    @Override
    public TipoTrabajo selectTipoTrabajo(long medicionId) {
        Medicion medicion = null;
        medicion = ReadById(medicionId);

        return medicion.getTipoTrabajo();
    }

    @Override
    public TipoAlimentacion selectTipoAlimentacion(long medicionId) {
        Medicion medicion = null;
        medicion = ReadById(medicionId);

        return medicion.getTipoAlimentacion();
    }

    @Override
    public Localidad selectLocalidad(long medicionId) {
        Medicion medicion = null;
        medicion = ReadById(medicionId);

        return medicion.getLocalidad();
    }

    @Override
    public TipoPunto selectTipoPunto(long medicionId) {
        Medicion medicion = null;
        medicion = ReadById(medicionId);

        return medicion.getTipoPunto();
    }

    @Override
    public TipoMedicion selectTipoMedicion(long medicionId) {
        Medicion medicion = null;
        medicion = ReadById(medicionId);

        return medicion.getTipoMedicion();
    }

    @Override
    public CampanaMedicion selectCampanaMedicion(long medicionId) {
        Medicion medicion = null;
        medicion = ReadById(medicionId);

        return medicion.getCampanaMedicion();
    }

    @Override
    public Set<IntervaloTension> selectIntervaloTension(long medicionId) {
        Medicion medicion = null;
        medicion = ReadById(medicionId);

        return medicion.getIntervaloTension();
    }

    @Override
    public Set<IntervaloFRTension> selectIntervaloFRTension(long medicionId) {
        Medicion medicion = null;
        medicion = ReadById(medicionId);

        return medicion.getIntervaloFRTension();
    }

    @Override
    public Set<FactorCompensacion> selectFactorCompensacion(long medicionId) {
        Medicion medicion = null;
        medicion = ReadById(medicionId);

        return medicion.getFactorCompensacion();
    }

    @Override
    public Set<CompensacionTension> selectCompensacionTension(long medicionId) {
        Medicion medicion = null;
        medicion = ReadById(medicionId);

        return medicion.getCompensacionTension();
    }

    @Override
    public Set<CronogramaMedicion> selectCronogramaMedicion(long medicionId) {
        Medicion medicion = null;
        medicion = ReadById(medicionId);

        return medicion.getCronogramaMedicion();
    }

	@Override
	public List<Medicion> getMedicionxCampana(long camMedicionId) {
		
		System.out.println("mvargas : " + camMedicionId);
		 List<Medicion> mediciones=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from Medicion m where m.estado = '1' and m.campanaMedicion.camMedicionId = " + Long.toString(camMedicionId) + " order by m.numSumMedicion");
	            
	            mediciones=(List<Medicion>) q.list();	            
	            
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Muestras para la Campa�a.", e);
	        }
		return mediciones;
	}

	@Override
	public List<Medicion> getMedicionxCampanaNoProgramados(long camMedicionId) {
		 List<Medicion> mediciones=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from Medicion m where m.estado = '1' and m.campanaMedicion.camMedicionId = " + Long.toString(camMedicionId) + 
	            		" and m.medicionId not in (select c.medicion.medicionId from CronogramaMedicion c where c.medicion.medicionId = m.medicionId and c.estado = '1') order by m.numSumMedicion");
	            
	            mediciones=(List<Medicion>) q.list();	            
	            
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Muestras para la Campa�a.", e);
	        }
		return mediciones;
	}
	@Override
	public List<Medicion> getMedicionxCampana() {
		List<Medicion> mediciones=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from Medicion");
	            
	            mediciones=(List<Medicion>) q.list();	            
	            
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Muestras para la Campa�a.", e);
	        }
		return mediciones;
	}

	@Override
	public List<Medicion> getMedicionxSuministro(String numSumMedicion) {
		 List<Medicion> mediciones=null;
		 try {
	            HibernateUtil.begin();
	            
	            Query q = HibernateUtil.getSession().createQuery("from Medicion m where m.estado = '1' and m.numSumMedicion = '" + numSumMedicion.trim() + "' order by m.fecInstalacion desc");
	            
	            mediciones=(List<Medicion>) q.list();	            
	            
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Muestras para la Campa�a.", e);
	        }
		return mediciones;
	}

	@Override
	public List<Medicion> getMedicionxCampana(long camMedicionId, String tipServicio, String tipPunto) {
		 List<Medicion> mediciones=null;
		 String query = null;
		 try {
	            HibernateUtil.begin();
	            
	            if(tipServicio.equals("N") && tipPunto.equals("N")){
	            	query = "from Medicion m where m.estado = '1' and m.campanaMedicion.camMedicionId = " + Long.toString(camMedicionId) + " order by m.numSumMedicion";
	            }
	            
	            if(!tipServicio.equals("N") && tipPunto.equals("N")){
	            	query = "from Medicion m where m.estado = '1' and tipServicio = '" + tipServicio + "' and m.campanaMedicion.camMedicionId = " + Long.toString(camMedicionId) + " order by m.numSumMedicion";
	            }
	            
	            if(tipServicio.equals("N") && !tipPunto.equals("N")){
	            	query = "from Medicion m where m.estado = '1' and m.tipoPunto.codTipPunto = '" + tipPunto + "' and m.campanaMedicion.camMedicionId = " + Long.toString(camMedicionId) + " order by m.numSumMedicion";
	            }
	            
	            if(!tipServicio.equals("N") && !tipPunto.equals("N")){
	            	query = "from Medicion m where m.estado = '1' and m.tipoPunto.codTipPunto = '" + tipPunto + "' and tipServicio = '" + tipServicio + "' and m.campanaMedicion.camMedicionId = " + Long.toString(camMedicionId) + " order by m.numSumMedicion";
	            }
	            
	            Query q = HibernateUtil.getSession().createQuery(query);
	            
	            mediciones=(List<Medicion>) q.list();	            
	            
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Muestras para la Campa�a.", e);
	        }
		return mediciones;
	}

	@Override
	public Medicion getMedicionDuoRural(long camMedicionId, long medicionId) {
		Medicion medicion = null;
		 try {
	            HibernateUtil.begin();
	            
	            Query q = HibernateUtil.getSession().createQuery("select m1 from Medicion as m, Medicion as m1 where m1.campanaMedicion.camMedicionId = m.campanaMedicion.camMedicionId  and m.estado = '1' and m1.estado = '1' and m.codSed = m1.codSed and m1.medicionId <> m.medicionId and m.medicionId = " + String.valueOf(medicionId));
	            
	            if(q.list().size() > 0) {
	            
	            	medicion = (Medicion) q.list().get(0);	     
	            }
	            
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar la medicion asociada.", e);
	        }
		return medicion;
	}

	@Override
	public EstadoMedicion selectEstadoMedicionPer(long medicionId) {
		 Medicion medicion = null;
	        medicion = ReadById(medicionId);

	        return medicion.getPerEstMedicion();

	}
}
