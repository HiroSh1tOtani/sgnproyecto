package pe.com.indra.calidad.dao;

import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.indra.calidad.entity.CompensacionUnitaria;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.TipoPunto;

/**
 *
 * @author Luis
 */
public class TipoPuntoDaoImpl implements TipoPuntoDao {

    private static TipoPuntoDaoImpl instance = null;

    public static TipoPuntoDaoImpl getInstance() {
        if (instance == null) {
            instance = new TipoPuntoDaoImpl();
        }

        return instance;
    }

    public TipoPuntoDaoImpl() {
    }

    @Override
    public void create(TipoPunto tipoPunto) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(tipoPunto);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Tipo de Punto.", e);
        }
    }

    @Override
    public TipoPunto ReadById(long tipPuntoId) {
        TipoPunto tipoPunto = null;
        try {
            HibernateUtil.begin();

            /*
             Query q = HibernateUtil.getSession().createQuery("from CAL_TIPO_PARAMETRO where TIP_PARAMETRO_ID = :tipParametroId");
             q.setLong("tipParametroId",tipParametroId);
             tipoParametro = (TipoParametro)q.uniqueResult();*/

            tipoPunto = (TipoPunto) HibernateUtil.getSession().load(TipoPunto.class, tipPuntoId);

            Hibernate.initialize(tipoPunto);
            //Hibernate.initialize(tipoPunto.getMediciones());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Tipo de Punto.", e);
        }
        return tipoPunto;
    }

    @Override
    public TipoPunto ReadByCod(String codTipPunto) {
        TipoPunto tipoPunto = null;
        try {
            HibernateUtil.begin();

            Query q = HibernateUtil.getSession().createQuery("from TipoPunto where codTipPunto = :codTipPunto and estado = '1'");
            q.setString("codTipPunto", codTipPunto);
            tipoPunto = (TipoPunto) q.uniqueResult();

            Hibernate.initialize(tipoPunto);
            Hibernate.initialize(tipoPunto.getMediciones());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Tipo de Punto.", e);
        }
        return tipoPunto;
    }

    @Override
    public void update(TipoPunto tipoPunto) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(tipoPunto);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Tipo de Punto.", e);
        }
    }

    @Override
    public void delete(long tipPuntoId) {
        TipoPunto tipoPunto = null;
        tipoPunto = ReadById(tipPuntoId);

        if (tipoPunto != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(tipoPunto);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Tipo de Punto.", e);
            }
        }
    }

    @Override
    public Set<Medicion> selectMedicion(long tipPuntoId) {
        TipoPunto tipoPunto = null;
        tipoPunto = ReadById(tipPuntoId);

        return tipoPunto.getMediciones();

    }

	@Override
	public List<TipoPunto> getAllRows() {
		List<TipoPunto> tipoPuntos=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from TipoPunto");
	         
	            tipoPuntos=(List<TipoPunto>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Tipo Punto.", e);
	        }
		return tipoPuntos;
	}
	
	@Override
	public List<TipoPunto> getAllRows1() {
		List<TipoPunto> tipoPuntos=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from TipoPunto where estado =:estado");
	            q.setString("estado","1");
	            tipoPuntos=(List<TipoPunto>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Tipo Punto.", e);
	        }
		return tipoPuntos;
	}
}
