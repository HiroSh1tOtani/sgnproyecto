package pe.com.indra.calidad.dao;

import java.util.Set;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Hibernate;

import pe.com.indra.calidad.entity.CampanaMedicion;
import pe.com.indra.calidad.entity.PeriodoMedicion;

/**
 *
 * @author Luis
 */
public class PeriodoMedicionDaoImpl implements PeriodoMedicionDao {

    private static PeriodoMedicionDaoImpl instance = null;

    public static PeriodoMedicionDaoImpl getInstance() {
        if (instance == null) {
            instance = new PeriodoMedicionDaoImpl();
        }

        return instance;
    }

    public PeriodoMedicionDaoImpl() {
    }

    @Override
    public void create(PeriodoMedicion periodoMedicion) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(periodoMedicion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Periodo de Medición.", e);
        }
    }

    @Override
    public PeriodoMedicion ReadById(long perMedicionId) {
        PeriodoMedicion periodoMedicion = null;
        try {
            HibernateUtil.begin();

            /*
             Query q = HibernateUtil.getSession().createQuery("from CAL_TIPO_PARAMETRO where TIP_PARAMETRO_ID = :tipParametroId");
             q.setLong("tipParametroId",tipParametroId);
             tipoParametro = (TipoParametro)q.uniqueResult();*/

            periodoMedicion = (PeriodoMedicion) HibernateUtil.getSession().load(PeriodoMedicion.class, perMedicionId);

            Hibernate.initialize(periodoMedicion);
            Hibernate.initialize(periodoMedicion.getCampanaMediciones());
            Hibernate.initialize(periodoMedicion.getInterrupciones()); 

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Parámetro.", e);
        }
        return periodoMedicion;
    }

    @Override
    public void update(PeriodoMedicion periodoMedicion) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(periodoMedicion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Tipo de Parámetro.", e);
        }
    }

    @Override
    public void delete(long perMedicionId) {
        PeriodoMedicion periodoMedicion = null;
        periodoMedicion = ReadById(perMedicionId);

        if (periodoMedicion != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(periodoMedicion);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Tipo de Parámetro.", e);
            }
        }
    }

    @Override
    public Set<CampanaMedicion> selectCampanaMedicion(long camMedicionId) {
        PeriodoMedicion periodoMedicion = null;
        periodoMedicion = ReadById(camMedicionId);

        return periodoMedicion.getCampanaMediciones();
    }

	@Override
	public List<PeriodoMedicion> getAllRows() {
		List<PeriodoMedicion> periodoMediciones=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from PeriodoMedicion where estado =:estado order by ano desc, periodo desc");
	            q.setString("estado","1");
	            periodoMediciones=(List<PeriodoMedicion>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Periodo Mediciones .", e);
	        }
		return periodoMediciones;
	}
	
	@Override
	public List<PeriodoMedicion> getAllRows1() {
		List<PeriodoMedicion> periodoMediciones=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from PeriodoMedicion where estado =:estado order by ano desc, periodo desc");
	            q.setString("estado","1");
	            periodoMediciones=(List<PeriodoMedicion>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Periodo Medicion.", e);
	        }
		return periodoMediciones;
	}	
}