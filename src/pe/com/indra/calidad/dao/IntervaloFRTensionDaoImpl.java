
package pe.com.indra.calidad.dao;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import java.util.List;
import java.util.Set;

import pe.com.indra.calidad.entity.CompensacionTension;
import pe.com.indra.calidad.entity.IntervaloFRTension;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.ParametroNorma;
/**
 *
 * @author Luis
 */
public class IntervaloFRTensionDaoImpl implements IntervaloFRTensionDao{
    
    private static IntervaloFRTensionDaoImpl instance = null;

    public static IntervaloFRTensionDaoImpl getInstance() {
        if (instance == null) {
            instance = new IntervaloFRTensionDaoImpl();
        }

        return instance;
    }

    public IntervaloFRTensionDaoImpl() {
    }

    @Override
    public void create(IntervaloFRTension intervaloFRTension) {
                 try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(intervaloFRTension);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Intervalo FR de Tensión.", e);
        }
    }

    @Override
    public IntervaloFRTension ReadById(long intFRTensionId) {
                IntervaloFRTension intervaloFRTension = null;
        try {
            HibernateUtil.begin();

            /*
             Query q = HibernateUtil.getSession().createQuery("from CAL_PARAMETRO_NORMA where PAR_NORMA_ID = :parNormaId");
             q.setLong("parNormaId",parNormaId);
             parametroNorma = (ParametroNorma)q.uniqueResult();*/

            intervaloFRTension = (IntervaloFRTension) HibernateUtil.getSession().load(IntervaloFRTension.class, intFRTensionId);

            Hibernate.initialize(intervaloFRTension);
            Hibernate.initialize(intervaloFRTension.getMedicion());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Intervalo FR de Tensión.", e);
        }
        return intervaloFRTension;
    }

    @Override
    public void update(IntervaloFRTension intervaloFRTension) {
            try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(intervaloFRTension);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Intervalo FR de Tensión.", e);
        }
    }

    @Override
    public void delete(long intFRTensionId) {
           IntervaloFRTension intervaloFRTension = null;
        intervaloFRTension = ReadById(intFRTensionId);

        if (intervaloFRTension != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(intervaloFRTension);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Intervalo FR de Tensión.", e);
            }
        }
    }

    @Override
    public Medicion selectMedicion(long intFRTensionId) {
           IntervaloFRTension intervaloFRTension = null;
        intervaloFRTension = ReadById(intFRTensionId);

        return intervaloFRTension.getMedicion();
    }

	@Override
	public List<IntervaloFRTension> getAllRows() {
		 List<IntervaloFRTension> intervalosFRTension=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from IntervaloFRTension");
	            intervalosFRTension= (List<IntervaloFRTension>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar IntervaloFRTension.", e);
	        } 
		return intervalosFRTension;
	}

	@Override
	public IntervaloFRTension getAllRows1(long medicionId) {
		IntervaloFRTension intervaloFRTension=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from IntervaloFRTension where estado =:estado and medicion.medicionId = :medicionId");
	            q.setLong("medicionId",medicionId);
	            q.setString("estado", "1");
	            
	            if(q.list().size() > 0){
	            
	            	intervaloFRTension = (IntervaloFRTension) q.list().get(0);
	            
	            }
	            HibernateUtil.commit();
	            HibernateUtil.close();
	            
		         
	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar IntervaloFRTension.", e);
	        } 
		return intervaloFRTension;	

    
	}
}