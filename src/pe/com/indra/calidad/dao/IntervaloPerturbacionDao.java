
package pe.com.indra.calidad.dao;

import java.util.List;
import java.util.Set;





import pe.com.indra.calidad.entity.EstadoIntervalo;
import pe.com.indra.calidad.entity.IntervaloPerturbacion;
import pe.com.indra.calidad.entity.IntervaloTension;
import pe.com.indra.calidad.entity.Medicion;


/**
 *
 * @author Luis
 */
public interface IntervaloPerturbacionDao {
    
    public void create(IntervaloPerturbacion intervaloPerturbacion);
    public IntervaloPerturbacion ReadById(long intPerturbacionId);
    public void update(IntervaloPerturbacion intervaloPerturbacion);
    public void delete(long intervaloPerturbacion);
 
    public Medicion selectMedicion(long intPerturbacionId);
    public EstadoIntervalo selectEstadoIntervaloFlicker(long intPerturbacionId);
    public EstadoIntervalo selectEstadoIntervaloThd(long intPerturbacionId);
    public EstadoIntervalo selectEstadoIntervaloV(long intPerturbacionId);

    public List<IntervaloPerturbacion> getIntervaloxMedicion(long medicionId);
  
    public List<IntervaloPerturbacion> getAllRows(long medicionId);

}
