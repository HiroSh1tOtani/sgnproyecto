
package pe.com.indra.calidad.dao;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;


public class HibernateUtil {
	
	private static final Logger log = Logger.getAnonymousLogger();
    private static final ThreadLocal session = new ThreadLocal();
    private static final Configuration configuration = new Configuration().configure();
    private static final ServiceRegistry serviceRegistry =  new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
    private static final SessionFactory sessionFactory = buildSessionFactory();
   
   private static SessionFactory buildSessionFactory() {
       try {
           // Creando el session factory desde hibernate.cfg.xml
           return new Configuration().configure().buildSessionFactory(serviceRegistry);
       }
       catch (Throwable ex) {
           System.err.println("Fallo en la creacion inicial de SessionFactory" + ex);
           throw new ExceptionInInitializerError(ex);
       }
   }
   
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
    protected HibernateUtil() {
    }

    public static Session getSession() {
        Session session = (Session) HibernateUtil.session.get();
        if (session == null) {
            session = sessionFactory.openSession();
            HibernateUtil.session.set(session);
        }
        return session;
    }

    public static void begin() {
        getSession().beginTransaction();
    }

    public static void commit() {
        getSession().getTransaction().commit();
    }

    public static void rollback() {
        try {
            getSession().getTransaction().rollback();
        } catch (HibernateException e) {
            log.log(Level.WARNING, "Cannot rollback", e);
        }
        try {
            getSession().close();
        } catch (HibernateException e) {
            log.log(Level.WARNING, "Cannot close", e);
        }
        HibernateUtil.session.set(null);
    }

    public static void close() {
        getSession().close();
        HibernateUtil.session.set(null);
    }
   
}