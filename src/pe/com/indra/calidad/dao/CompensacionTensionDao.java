package pe.com.indra.calidad.dao;

import java.util.List;
import java.util.Set;

import pe.com.calidad.suministro.entity.SuministroAfectado;
import pe.com.indra.calidad.entity.CompensacionTension;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.TipoEnergia;

/**
 *
 * @author Luis
 */
public interface CompensacionTensionDao {

    public void create(CompensacionTension compensacionTension);

    public CompensacionTension ReadById(long comTensionId);

    public void update(CompensacionTension compensacionTension);

    public void delete(long croMedicionId);

    public TipoEnergia selectTipoEnergia(long comTensionId);

    public Medicion selectMedicion(long comTensionId);
    
    public List<CompensacionTension> getAllRows1(long medicionId);
}
