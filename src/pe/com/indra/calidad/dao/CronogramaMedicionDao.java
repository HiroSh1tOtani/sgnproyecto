package pe.com.indra.calidad.dao;

import java.util.List;
import java.util.Set;

import pe.com.indra.calidad.entity.CronogramaMedicion;
import pe.com.indra.calidad.entity.Cuadrilla;
import pe.com.indra.calidad.entity.Medicion;

/**
 *
 * @author Luis
 */
public interface CronogramaMedicionDao {

    public void create(CronogramaMedicion cronogramaMedicion);
    public CronogramaMedicion ReadById(long croMedicionId);
    public void update(CronogramaMedicion cronogramaMedicion);
    public void delete(long croMedicionId);
    public Cuadrilla selectCuadrilla(long croMedicionId);
    public Medicion selectMedicion(long croMedicionId);
    public List<CronogramaMedicion> getCronogramaxCampana(long camMedicionId);
    public List<CronogramaMedicion> getCronogramaxCampana();
}
