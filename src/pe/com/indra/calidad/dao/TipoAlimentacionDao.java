package pe.com.indra.calidad.dao;

/**
 *
 * @author Luis
 */
import java.util.List;

import pe.com.indra.calidad.entity.CompensacionUnitaria;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.TipoAlimentacion;



import java.util.Set;

public interface TipoAlimentacionDao {

    public void create(TipoAlimentacion tipoAlimentacion);

    public TipoAlimentacion ReadById(long tipAlimentacionId);

    public TipoAlimentacion ReadByCod(String codTipAlimentacion);

    public void update(TipoAlimentacion tipoAlimentacion);

    public void delete(long tipAlimentacionId);

    public Set<Medicion> selectMedicion(long tipAlimentacionId);
    
    public List<TipoAlimentacion> getAllRows();
    
    public List<TipoAlimentacion> getAllRows1();
    
}
