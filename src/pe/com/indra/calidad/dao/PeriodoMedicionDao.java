package pe.com.indra.calidad.dao;

/**
 *
 * @author Luis
 */
import java.util.List;

import pe.com.indra.calidad.entity.CampanaMedicion;
import pe.com.indra.calidad.entity.PeriodoMedicion;

import java.util.Set;

public interface PeriodoMedicionDao {

    public void create(PeriodoMedicion periodoMedicion);

    public PeriodoMedicion ReadById(long perMedicionId);

    public void update(PeriodoMedicion periodoMedicion);

    public void delete(long perMedicionId);

    public Set<CampanaMedicion> selectCampanaMedicion(long camMedicionId);
    
    public List<PeriodoMedicion> getAllRows();
    
    public List<PeriodoMedicion> getAllRows1();
}
