package pe.com.indra.calidad.dao;

import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.indra.calidad.entity.CompensacionUnitaria;
import pe.com.indra.calidad.entity.EquipoRegistrador;
import pe.com.indra.calidad.entity.Medicion;

/**
 *
 * @author Luis
 */
public class EquipoRegistradorDaoImpl implements EquipoRegistradorDao {

    private static EquipoRegistradorDaoImpl instance = null;

    public static EquipoRegistradorDaoImpl getInstance() {
        if (instance == null) {
            instance = new EquipoRegistradorDaoImpl();
        }

        return instance;
    }

    public EquipoRegistradorDaoImpl() {
    }

    @Override
    public void create(EquipoRegistrador equipoRegistrador) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(equipoRegistrador);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Equipo Registrador.", e);
        }
    }

    @Override
    public EquipoRegistrador ReadById(long equRegistradorId) {
        EquipoRegistrador equipoRegistrador = null;
        try {
            HibernateUtil.begin();

            /*
             Query q = HibernateUtil.getSession().createQuery("from CAL_TIPO_PARAMETRO where TIP_PARAMETRO_ID = :tipParametroId");
             q.setLong("tipParametroId",tipParametroId);
             tipoParametro = (TipoParametro)q.uniqueResult();*/

            equipoRegistrador = (EquipoRegistrador) HibernateUtil.getSession().load(EquipoRegistrador.class, equRegistradorId);

            Hibernate.initialize(equipoRegistrador);
            //Hibernate.initialize(equipoRegistrador.getMediciones());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Equipo Registrador.", e);
        }
        return equipoRegistrador;
    }

    @Override
    public EquipoRegistrador ReadByCod(String codEquRegistrador) {
        EquipoRegistrador equipoRegistrador = null;
        try {
            HibernateUtil.begin();

            Query q = HibernateUtil.getSession().createQuery("from EquipoRegistrador where codEquRegistrador = :codEquRegistrador  and estado = '1'");
            q.setString("codEquRegistrador", codEquRegistrador);
            equipoRegistrador = (EquipoRegistrador) q.uniqueResult();

            Hibernate.initialize(equipoRegistrador);
            Hibernate.initialize(equipoRegistrador.getMediciones());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Equipo Registrador.", e);
        }
        return equipoRegistrador;
    }

    @Override
    public void update(EquipoRegistrador equipoRegistrador) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(equipoRegistrador);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Equipo Registrador.", e);
        }
    }

    @Override
    public void delete(long equRegistradorId) {
        EquipoRegistrador equipoRegistrador = null;
        equipoRegistrador = ReadById(equRegistradorId);

        if (equipoRegistrador != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(equipoRegistrador);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Equipo Registrador.", e);
            }
        }
    }

    @Override
    public Set<Medicion> selectMedicion(long equRegistradorId) {
         EquipoRegistrador equipoRegistrador = null;
        equipoRegistrador = ReadById(equRegistradorId);

        return equipoRegistrador.getMediciones();
    }

	@Override
	public List<EquipoRegistrador> getAllRows() {
		List<EquipoRegistrador> equipoRegistradores=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from EquipoRegistrador order by numSerie asc");
	            
	            equipoRegistradores=(List<EquipoRegistrador>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Equipo Registrador.", e);
	        }
		return equipoRegistradores;
	}
	
	@Override
	public List<EquipoRegistrador> getAllRows1() {
		List<EquipoRegistrador> equipoRegistradores=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from EquipoRegistrador where estado =:estado order by numSerie asc");
	            q.setString("estado","1");
	            equipoRegistradores=(List<EquipoRegistrador>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Equipo Registrador.", e);
	        }
		return equipoRegistradores;
	}
}

