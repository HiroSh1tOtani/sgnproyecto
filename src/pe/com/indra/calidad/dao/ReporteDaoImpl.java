package pe.com.indra.calidad.dao;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.indra.calidad.entity.Reporte;
import pe.com.indra.calidad.entity.TipoParametro;

public class ReporteDaoImpl implements ReporteDao {

	@Override
	public void create(Reporte reporte) {
	    try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(reporte);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Reporte.", e);
        } 
		
	}

	@Override
	public Reporte ReadById(long reporteId) {
		Reporte reporte = null;
        try {
            HibernateUtil.begin();
            
            /*
            Query q = HibernateUtil.getSession().createQuery("from CAL_PARAMETRO_NORMA where PAR_NORMA_ID = :parNormaId");
            q.setLong("parNormaId",parNormaId);
            parametroNorma = (ParametroNorma)q.uniqueResult();*/
            
            reporte = (Reporte) HibernateUtil.getSession().load(Reporte.class, reporteId);
            
            Hibernate.initialize(reporte);
            Hibernate.initialize(reporte.getTipoParametro()); 
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Reporte.", e);
        } 
        return reporte;
	}

	@Override
	public Reporte ReadByCod(String codReporte) {
		Reporte reporte = null;
        try {
            HibernateUtil.begin();
            
            Query q = HibernateUtil.getSession().createQuery("from Reporte where codReporte = :codReporte");
            q.setString("codReporte",codReporte);
            reporte = (Reporte)q.uniqueResult();
            
            Hibernate.initialize(reporte);
            Hibernate.initialize(reporte.getTipoParametro()); 
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Reporte.", e);
        } 
        return reporte; 
	}

	@Override
	public void update(Reporte reporte) {
		try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(reporte);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Reporte.", e);
        } 
		
	}

	@Override
	public void delete(long reporteId) {
		Reporte reporte = null;
		reporte = ReadById(reporteId);
        
        if(reporte != null){               
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(reporte);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Reporte.", e); 
            }
        }
		
	}

	@Override
	public TipoParametro selectTipoParametro(long reporteId) {
		Reporte reporte = null;
		reporte = ReadById(reporteId);
        
        return reporte.getTipoParametro();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Reporte> getAllRows() {
		List<Reporte> reportes = null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from Reporte p order by p.tipoParametro.codTipParametro, p.codReporte");
	            reportes = (List<Reporte>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se pueden encontrar Reportes.", e);
	        } 
		return reportes;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Reporte> getVNR() {
		List<Reporte> reportes = null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from Reporte p where p.tipoParametro.codTipParametro ='EE' order by p.tipoParametro.codTipParametro, p.codReporte");
	            reportes = (List<Reporte>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se pueden encontrar Reportes.", e);
	        } 
		return reportes;
	}

}
