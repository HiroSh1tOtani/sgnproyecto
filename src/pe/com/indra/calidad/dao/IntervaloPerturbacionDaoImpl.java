package pe.com.indra.calidad.dao;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import java.util.List;
import java.util.Set;

import pe.com.indra.calidad.entity.EstadoIntervalo;
import pe.com.indra.calidad.entity.IntervaloPerturbacion;
import pe.com.indra.calidad.entity.IntervaloTension;
import pe.com.indra.calidad.entity.Medicion;

/**
 *
 * @author Luis
 */
public class IntervaloPerturbacionDaoImpl implements IntervaloPerturbacionDao {

    private static IntervaloPerturbacionDaoImpl instance = null;

    public static IntervaloPerturbacionDaoImpl getInstance() {
        if (instance == null) {
            instance = new IntervaloPerturbacionDaoImpl();
        }

        return instance;
    }

    public IntervaloPerturbacionDaoImpl() {
    }

	@Override
	public void create(IntervaloPerturbacion intervaloPerturbacion) {
		try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(intervaloPerturbacion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Intervalo Perturbacion.", e);
        }
		
	}

	@Override
	public IntervaloPerturbacion ReadById(long intPerturbacionId) {
		IntervaloPerturbacion intervaloPerturbacion = null;
		 try {
	            HibernateUtil.begin();

	         
	            intervaloPerturbacion = (IntervaloPerturbacion) HibernateUtil.getSession().load(IntervaloPerturbacion.class, intPerturbacionId);

	            Hibernate.initialize(intervaloPerturbacion);
	            Hibernate.initialize(intervaloPerturbacion.getFliEstIntervalo());
	            Hibernate.initialize(intervaloPerturbacion.getArmEstIntervalo());
	            Hibernate.initialize(intervaloPerturbacion.getArmvEstIntervalo());
	            Hibernate.initialize(intervaloPerturbacion.getMedicion());

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Intervalo Perturbacion.", e);
	        }
		return null;
	}

	@Override
	public void update(IntervaloPerturbacion intervaloPerturbacion) {
		try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(intervaloPerturbacion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Intervalo Perturbacion.", e);
        }
		
	}

	@Override
	public void delete(long intPerturbacionId) {
		IntervaloPerturbacion intervaloPerturbacion = null;
        intervaloPerturbacion = ReadById(intPerturbacionId);

        if (intervaloPerturbacion != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(intervaloPerturbacion);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Intervalo Perturbacion.", e);
            }
        }
		
	}

	@Override
	public Medicion selectMedicion(long intPerturbacionId) {
		IntervaloPerturbacion intervaloPerturbacion = null;
        intervaloPerturbacion = ReadById(intPerturbacionId);

        return intervaloPerturbacion.getMedicion();
	}

	@Override
	public EstadoIntervalo selectEstadoIntervaloFlicker(long intPerturbacionId) {
		IntervaloPerturbacion intervaloPerturbacion = null;
        intervaloPerturbacion = ReadById(intPerturbacionId);
		return intervaloPerturbacion.getFliEstIntervalo() ;
	}


	@Override
	public EstadoIntervalo selectEstadoIntervaloThd(long intPerturbacionId) {
		IntervaloPerturbacion intervaloPerturbacion = null;
        intervaloPerturbacion = ReadById(intPerturbacionId);
		return intervaloPerturbacion.getArmEstIntervalo();
	}

	@Override
	public EstadoIntervalo selectEstadoIntervaloV(long intPerturbacionId) {
		IntervaloPerturbacion intervaloPerturbacion = null;
        intervaloPerturbacion = ReadById(intPerturbacionId);
		return intervaloPerturbacion.getArmvEstIntervalo();
	}

	

	@Override
	public List<IntervaloPerturbacion> getIntervaloxMedicion(long medicionId) {
		List<IntervaloPerturbacion> intervalos=null;
		try {
            HibernateUtil.begin();

            Query q = HibernateUtil.getSession().createQuery("from IntervaloPerturbacion m where estado = '1' and m.medicion.medicionId = " + Long.toString(medicionId) + " and enPerMedicion = '1' order by m.intervalo ASC");
            
            intervalos=(List<IntervaloPerturbacion>) q.list();	            
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Registros de Perturbacion.", e);
        }
	return intervalos;
	}
	
	

	
	
	@Override
	public List<IntervaloPerturbacion> getAllRows(long medicionId) {
		List<IntervaloPerturbacion> perturbaciones=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from IntervaloPerturbacion m where m.medicion.medicionId = " + Long.toString(medicionId) + " order by m.intervalo ASC");
	            
	            perturbaciones=(List<IntervaloPerturbacion>) q.list();	            
	            
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Registros de Tensi�n Perturbacion.", e);
	        }
		return perturbaciones;
	}
}
