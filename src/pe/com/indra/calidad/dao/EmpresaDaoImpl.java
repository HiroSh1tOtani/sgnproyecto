
package pe.com.indra.calidad.dao;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;






import pe.com.indra.calidad.entity.Empresa;
import pe.com.indra.calidad.entity.Localidad;


/**
 *
 * @author Luis
 */
public class EmpresaDaoImpl implements EmpresaDao {
    
    private static EmpresaDaoImpl instance = null;

    public static EmpresaDaoImpl getInstance() {
        if (instance == null) {
            instance = new EmpresaDaoImpl();
        }

        return instance;
    }

    public EmpresaDaoImpl() {
    }
    
   
   
	@Override
	public List<Empresa> getAllRows() {
		 List<Empresa> empresas=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from Empresa");
	            empresas= (List<Empresa>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Empresa.", e);
	        } 
		return empresas;
	}

	@Override
    public void update(Empresa empresa) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(empresa);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Empresa.", e);
        }
    }
	
	 @Override
	    public Empresa readByCod(String empresa) {
		 Empresa empresasa = null;
	        try {
	            HibernateUtil.begin();

	            empresasa = (Empresa) HibernateUtil.getSession().load(Empresa.class, empresa);

	            Hibernate.initialize(empresasa);
	            //Hibernate.initialize(localidad.getMediciones());

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Empresa.", e);
	        }
	        return empresasa;
	    }
	
}
