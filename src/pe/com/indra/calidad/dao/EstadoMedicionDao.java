
package pe.com.indra.calidad.dao;

import java.util.Set;

import pe.com.indra.calidad.entity.CompensacionUnitaria;
import pe.com.indra.calidad.entity.EstadoMedicion;
import pe.com.indra.calidad.entity.Medicion;

import java.util.List;

/**
 *
 * @author Juan
 */
public interface EstadoMedicionDao {
    public void create(EstadoMedicion estadoMedicion);
    public EstadoMedicion ReadById(long estMedicionId);
	public EstadoMedicion ReadByCod(String codEstMedicion);
	public void update(EstadoMedicion estadoMedicion);
	public void delete(long estMedicionId);
    public Set<Medicion> selectMedicion(long estMedicionId);
    public List<EstadoMedicion> getAllRows();
    public List<EstadoMedicion> getAllRows1();
    
    public Set<Medicion> selectMedicionPer(long estMedicionId);
    
}
