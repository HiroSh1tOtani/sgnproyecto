package pe.com.indra.calidad.dao;

import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Hibernate;

import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.ParametroMedido;
import pe.com.indra.calidad.entity.TipoEnergia;

/**
 *
 * @author Luis
 */
public class ParametroMedidoDaoImpl implements ParametroMedidoDao {

    private static ParametroMedidoDaoImpl instance = null;

    public static ParametroMedidoDaoImpl getInstance() {
        if (instance == null) {
            instance = new ParametroMedidoDaoImpl();
        }

        return instance;
    }

    public ParametroMedidoDaoImpl() {
    }

    @Override
    public void create(ParametroMedido parametroMedido) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(parametroMedido);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear el Parámetro Medido.", e);
        }
    }

    @Override
    public ParametroMedido ReadById(long parMedidoId) {
        ParametroMedido parametroMedido = null;
        try {
            HibernateUtil.begin();

            /*
             Query q = HibernateUtil.getSession().createQuery("from CAL_TIPO_PARAMETRO where TIP_PARAMETRO_ID = :tipParametroId");
             q.setLong("tipParametroId",tipParametroId);
             tipoParametro = (TipoParametro)q.uniqueResult();*/

            parametroMedido = (ParametroMedido) HibernateUtil.getSession().load(ParametroMedido.class, parMedidoId);

            Hibernate.initialize(parametroMedido);
            //Hibernate.initialize(parametroMedido.getMediciones());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Tipo de Parámetro.", e);
        }
        return parametroMedido;
    }

    @Override
    public ParametroMedido ReadByCod(String codParMedido) {
        ParametroMedido parametroMedido = null;
        try {
            HibernateUtil.begin();

            Query q = HibernateUtil.getSession().createQuery("from ParametroMedido where codParMedido = :codParMedido and estado = '1'");
            q.setString("codParMedido", codParMedido);
            parametroMedido = (ParametroMedido) q.uniqueResult();

            Hibernate.initialize(parametroMedido);
            Hibernate.initialize(parametroMedido.getMediciones());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Tipo de Parámetro.", e);
        }
        return parametroMedido;
    }

    @Override
    public void update(ParametroMedido parametroMedido) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(parametroMedido);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Parametro .", e);
        }
    }

    @Override
    public void delete(long parMedidoId) {
        ParametroMedido parametroMedido = null;
        parametroMedido = ReadById(parMedidoId);

        if (parametroMedido != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(parametroMedido);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Parametro.", e);
            }
        }

    }

    @Override
    public Set<Medicion> selectMedicion(long parMedidoId) {
        ParametroMedido parametroMedido = null;
        parametroMedido = ReadById(parMedidoId);

        return parametroMedido.getMediciones();
    }
    
    @SuppressWarnings("unchecked")
	@Override
	public List<ParametroMedido> getAllRows() {
		  List<ParametroMedido> tipos=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from ParametroMedido");
	            tipos= (List<ParametroMedido>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Tipo de Parametro.", e);
	        } 
		return tipos;
	}
    
    @Override
	public List<ParametroMedido> getAllRows1() {
		  List<ParametroMedido> tipos=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from ParametroMedido where estado =:estado");
	            q.setString("estado","1");
	            tipos= (List<ParametroMedido>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Tipo de Parametro.", e);
	        } 
		return tipos;
	}
}
