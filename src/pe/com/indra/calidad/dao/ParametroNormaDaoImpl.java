/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.indra.calidad.dao;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.indra.calidad.entity.ParametroNorma;
import pe.com.indra.calidad.entity.TipoParametro;

/**
 *
 * @author Juan
 */
public class ParametroNormaDaoImpl implements ParametroNormaDao {
    
    private static ParametroNormaDaoImpl instance = null;

    public static ParametroNormaDaoImpl getInstance() {
        if (instance == null) {
            instance = new ParametroNormaDaoImpl();
        }

        return instance;
    }

    public ParametroNormaDaoImpl() {
    }
    
    @Override
    public void create(ParametroNorma parametroNorma) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(parametroNorma);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Parametro.", e);
        } 
    }
    
    @Override
    public ParametroNorma ReadById(long parNormaId) {
        ParametroNorma parametroNorma = null;
        try {
            HibernateUtil.begin();
            
            /*
            Query q = HibernateUtil.getSession().createQuery("from CAL_PARAMETRO_NORMA where PAR_NORMA_ID = :parNormaId");
            q.setLong("parNormaId",parNormaId);
            parametroNorma = (ParametroNorma)q.uniqueResult();*/
            
            parametroNorma = (ParametroNorma) HibernateUtil.getSession().load(ParametroNorma.class, parNormaId);
            
            Hibernate.initialize(parametroNorma);
            Hibernate.initialize(parametroNorma.getTipoParametro()); 
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Parametro.", e);
        } 
        return parametroNorma;        
    }

    @Override
    public ParametroNorma ReadByCod(String codParNorma) {
        ParametroNorma parametroNorma = null;
        try {
            HibernateUtil.begin();
            
            Query q = HibernateUtil.getSession().createQuery("from ParametroNorma where codParNorma = :codParNorma and estado = '1'");
            q.setString("codParNorma",codParNorma);
            parametroNorma = (ParametroNorma)q.uniqueResult();
            
            Hibernate.initialize(parametroNorma);
            Hibernate.initialize(parametroNorma.getTipoParametro()); 
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Parametro.", e);
        } 
        return parametroNorma;   
    }

    @Override
    public void update(ParametroNorma parametroNorma) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(parametroNorma);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Parametro.", e);
        }         
    }

    @Override
    public void delete(long parNormaId) {
        ParametroNorma parametroNorma = null;
        parametroNorma = ReadById(parNormaId);
        
        if(parametroNorma != null){               
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(parametroNorma);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Parametro.", e); 
            }
        }
    }

    @Override
    public TipoParametro selectTipoParametro(long parNormaId) {
        ParametroNorma parametroNorma = null;
        parametroNorma = ReadById(parNormaId);
        
        return parametroNorma.getTipoParametro();
    }

    @SuppressWarnings("unchecked")
	@Override
	public List<ParametroNorma> getAllRows() {
		 List<ParametroNorma> parametrosNorma=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from ParametroNorma p order by p.tipoParametro.codTipParametro, p.codParNorma");
	            parametrosNorma= (List<ParametroNorma>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Tipo de Parametro.", e);
	        } 
		return parametrosNorma;
	}
    
	
	
}
