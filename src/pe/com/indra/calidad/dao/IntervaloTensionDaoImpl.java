package pe.com.indra.calidad.dao;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import pe.com.indra.calidad.entity.EstadoIntervalo;
import pe.com.indra.calidad.entity.IntervaloTension;
import pe.com.indra.calidad.entity.Medicion;

/**
 *
 * @author Luis
 */
public class IntervaloTensionDaoImpl implements IntervaloTensionDao {

    private static IntervaloTensionDaoImpl instance = null;

    public static IntervaloTensionDaoImpl getInstance() {
        if (instance == null) {
            instance = new IntervaloTensionDaoImpl();
        }

        return instance;
    }

    public IntervaloTensionDaoImpl() {
    }

    @Override
    public void create(IntervaloTension intervaloTension) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(intervaloTension);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Intervalo de Tensión.", e);
        }
    }

    @Override
    public IntervaloTension ReadById(long intTensionId) {
        IntervaloTension intervaloTension = null;
        try {
            HibernateUtil.begin();

            /*
             Query q = HibernateUtil.getSession().createQuery("from CAL_PARAMETRO_NORMA where PAR_NORMA_ID = :parNormaId");
             q.setLong("parNormaId",parNormaId);
             parametroNorma = (ParametroNorma)q.uniqueResult();*/

            intervaloTension = (IntervaloTension) HibernateUtil.getSession().load(IntervaloTension.class, intTensionId);

            Hibernate.initialize(intervaloTension);
            Hibernate.initialize(intervaloTension.getEstadoIntervalo());
            Hibernate.initialize(intervaloTension.getMedicion());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Intervalo de Tensión.", e);
        }
        return intervaloTension;
    }

    @Override
    public void update(IntervaloTension intervaloTension) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(intervaloTension);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Intervalo de Tensión.", e);
        }
    }

    @Override
    public void delete(long intTensionId) {
        IntervaloTension intervaloTension = null;
        intervaloTension = ReadById(intTensionId);

        if (intervaloTension != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(intervaloTension);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Intervalo de Tensión.", e);
            }
        }
    }

    @Override
    public EstadoIntervalo selectEstadoIntervalo(long intTensionId) {
        IntervaloTension intervaloTension = null;
        intervaloTension = ReadById(intTensionId);

        return intervaloTension.getEstadoIntervalo();
    }

    @Override
    public Medicion selectMedicion(long intTensionId) {
        IntervaloTension intervaloTension = null;
        intervaloTension = ReadById(intTensionId);

        return intervaloTension.getMedicion();
    }

	@Override
	public List<IntervaloTension> getIntervaloxMedicion(long medicionId) {
		List<IntervaloTension> intervalos=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from IntervaloTension m where estado = '1' and m.medicion.medicionId = " + Long.toString(medicionId) + " and enPerMedicion = '1' order by m.intervalo ASC");
	            
	            intervalos=(List<IntervaloTension>) q.list();	            
	            
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Registros de Tensi�n.", e);
	        }
		return intervalos;
	}

	@Override
	public List<IntervaloTension> getIntervaloxMedicion() {
		List<IntervaloTension> intervalos=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from IntervaloTension");
	            
	            intervalos=(List<IntervaloTension>) q.list();	            
	            
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Registros de Tensi�n.", e);
	        }
		return intervalos;
	}
    
	@Override
	public List<IntervaloTension> getAllRows(long medicionId) {
		List<IntervaloTension> intervalos=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from IntervaloTension m where m.medicion.medicionId = " + Long.toString(medicionId) + " order by m.intervalo ASC");
	            
	            intervalos=(List<IntervaloTension>) q.list();	            
	            
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Registros de Tensi�n.", e);
	        }
		return intervalos;
	}

	@Override 
	public double getMaxTension(long medicionId) {
		Object[] lista = null;
		BigDecimal lista2 = null;
		double max = 0;
		double tensionR;
		double tensionS;
		double tensionT;
		
		Medicion medicion = null;
		MedicionDao dao = new MedicionDaoImpl();
        medicion = dao.ReadById(medicionId);
        String conexion;
        
        if(medicion != null && (medicion.getEstadoMedicion().getCodEstMedicion().equals("V") ||
        						medicion.getEstadoMedicion().getCodEstMedicion().equals("C") ||
        						medicion.getEstadoMedicion().getCodEstMedicion().equals("X")) && 
        	(medicion.getParametroMedido().getCodParMedido().equals("TE") || medicion.getParametroMedido().getCodParMedido().equals("TP"))) {
        	conexion = medicion.getTipoAlimentacion().getCodTipAlimentacion();
        } else {
        	return 0;
        }
        
        try {
            HibernateUtil.begin();
            Query q = null;
            
            if(conexion.equals("MO")){
            	q = HibernateUtil.getSession().createQuery("select max(m.tensionR) from IntervaloTension m where estado = '1' and m.medicion.medicionId = " + Long.toString(medicionId) + " and enPerMedicion = '1' order by m.intervalo ASC");
            	
            	lista2 = (BigDecimal) q.uniqueResult();  
            } else {
            	q = HibernateUtil.getSession().createQuery("select max(m.tensionR), max(m.tensionS), max(m.tensionT) from IntervaloTension m where estado = '1' and m.medicion.medicionId = " + Long.toString(medicionId) + " and enPerMedicion = '1' order by m.intervalo ASC");
            	
            	lista = (Object[]) q.uniqueResult();  
            }
            
            //lista = (List<BigDecimal[]>) q.list();        
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Registros de Tensi�n.", e);
        }
		 
		if(conexion.equals("MO")){
			if(lista2 != null) 
				max = lista2.doubleValue();
			} else {
				if(lista != null) {
					if(lista[0] != null) {
						tensionR = ((BigDecimal) lista[0]).doubleValue(); 
					} else {
						tensionR = 0;
					}
					
					if(lista[1] != null) {
						tensionS = ((BigDecimal) lista[1]).doubleValue(); 
					} else {
						tensionS = 0;
					}
					
					if(lista[2] != null) {
						tensionT = ((BigDecimal) lista[2]).doubleValue(); 
					} else {
						tensionT = 0;
					}
					
					if(tensionR >= tensionS && tensionR >= tensionT) max = tensionR;
					
					if(tensionS >= tensionR && tensionS >= tensionT) max = tensionS;
					
					if(tensionT >= tensionR && tensionT >= tensionS) max = tensionT;
				} else {
					max = 0;
				}
			
		}
		
		return max;
	}

	@Override
	public double getMinTension(long medicionId) {
		Object[] lista = null;
		BigDecimal lista2 = null;
		double max = 0;
		double tensionR;
		double tensionS;
		double tensionT;
		
		Medicion medicion = null;
		MedicionDao dao = new MedicionDaoImpl();
        medicion = dao.ReadById(medicionId);
        String conexion;
        
        if(medicion != null && (medicion.getEstadoMedicion().getCodEstMedicion().equals("V") ||
        						medicion.getEstadoMedicion().getCodEstMedicion().equals("C") ||
        						medicion.getEstadoMedicion().getCodEstMedicion().equals("X")) && 
        	(medicion.getParametroMedido().getCodParMedido().equals("TE") || medicion.getParametroMedido().getCodParMedido().equals("TP"))) {
        	conexion = medicion.getTipoAlimentacion().getCodTipAlimentacion();
        } else {
        	return 0;
        }
        
        try {
            HibernateUtil.begin();
            Query q = null;
            
            if(conexion.equals("MO")){
            	q = HibernateUtil.getSession().createQuery("select min(m.tensionR) from IntervaloTension m where estado = '1' and m.medicion.medicionId = " + Long.toString(medicionId) + " and enPerMedicion = '1' order by m.intervalo ASC");
            	
            	lista2 = (BigDecimal) q.uniqueResult();  
            } else {
            	q = HibernateUtil.getSession().createQuery("select min(m.tensionR), min(m.tensionS), min(m.tensionT) from IntervaloTension m where estado = '1' and m.medicion.medicionId = " + Long.toString(medicionId) + " and enPerMedicion = '1' order by m.intervalo ASC");
            	
            	lista = (Object[]) q.uniqueResult();  
            }
            
            //lista = (List<BigDecimal[]>) q.list();        
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Registros de Tensi�n.", e);
        }
		 
		if(conexion.equals("MO")){
			if(lista2 != null) 
				max = lista2.doubleValue();
			} else {
				if(lista != null) {
					if(lista[0] != null) {
						tensionR = ((BigDecimal) lista[0]).doubleValue(); 
					} else {
						tensionR = 0;
					}
					
					if(lista[1] != null) {
						tensionS = ((BigDecimal) lista[1]).doubleValue(); 
					} else {
						tensionS = 0;
					}
					
					if(lista[2] != null) {
						tensionT = ((BigDecimal) lista[2]).doubleValue(); 
					} else {
						tensionT = 0;
					}
					
					if(tensionR >= tensionS && tensionR >= tensionT) max = tensionR;
					
					if(tensionS >= tensionR && tensionS >= tensionT) max = tensionS;
					
					if(tensionT >= tensionR && tensionT >= tensionS) max = tensionT;
				} else {
					max = 0;
				}
			
		}
		
		return max;
	}
}
