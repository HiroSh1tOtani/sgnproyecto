package pe.com.indra.calidad.dao;

import java.util.List;

import pe.com.indra.calidad.entity.Reporte;
import pe.com.indra.calidad.entity.TipoParametro;

/**
 *
 * @author Juan
 */
public interface ReporteDao {
    public void create(Reporte reporte);
    public Reporte ReadById(long reporteId);
	public Reporte ReadByCod(String codReporte);
	public void update(Reporte reporte);
	public void delete(long reporteId);
    public TipoParametro selectTipoParametro(long reporteId);
	public List<Reporte> getAllRows();
	public List<Reporte> getVNR();
	
        
}