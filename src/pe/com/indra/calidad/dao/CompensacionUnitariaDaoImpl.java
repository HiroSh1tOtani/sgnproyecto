package pe.com.indra.calidad.dao;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.indra.calidad.entity.CompensacionUnitaria;
import pe.com.indra.calidad.entity.TipoParametro;

public class CompensacionUnitariaDaoImpl implements CompensacionUnitariaDao {

	public CompensacionUnitariaDaoImpl() {
    }
	
	@Override
	public void create(CompensacionUnitaria compensacionUnitaria) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(compensacionUnitaria);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear la Compensacion Unitaria.", e);
        }
		
	}

	@Override
	public CompensacionUnitaria ReadById(long comUnitariaId) {
		CompensacionUnitaria compensacionUnitaria = null;
        try {
            HibernateUtil.begin();

            /*
             Query q = HibernateUtil.getSession().createQuery("from CAL_TIPO_PARAMETRO where TIP_PARAMETRO_ID = :tipParametroId");
             q.setLong("tipParametroId",tipParametroId);
             tipoParametro = (TipoParametro)q.uniqueResult();*/

            compensacionUnitaria = (CompensacionUnitaria) HibernateUtil.getSession().load(CompensacionUnitaria.class, comUnitariaId);

            Hibernate.initialize(compensacionUnitaria);
            Hibernate.initialize(compensacionUnitaria.getTipoParametro());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Compensacion Unitaria.", e);
        }
        return compensacionUnitaria;
	}

	@Override
	public CompensacionUnitaria ReadByCod(String codComUnitaria) {
		CompensacionUnitaria compensacionUnitaria = null;
        try {
            HibernateUtil.begin();
            
            Query q = HibernateUtil.getSession().createQuery("from CompensacionUnitaria where codComUnitaria = :codComUnitaria and estado = '1'");
            q.setString("codComUnitaria",codComUnitaria);
            compensacionUnitaria = (CompensacionUnitaria)q.uniqueResult();
            
            Hibernate.initialize(compensacionUnitaria);
            Hibernate.initialize(compensacionUnitaria.getTipoParametro()); 
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Compensacion Unitaria.", e);
        } 
        return compensacionUnitaria;   
	}

	@Override
	public void update(CompensacionUnitaria compensacionUnitaria) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(compensacionUnitaria);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Compensacion Unitaria.", e);
        }   
		
	}

	@Override
	public void delete(long comUnitariaId) {
		CompensacionUnitaria compensacionUnitaria = null;
		compensacionUnitaria = ReadById(comUnitariaId);
        
        if(compensacionUnitaria != null){               
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(compensacionUnitaria);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Compensacion Unitaria.", e); 
            }
        }
		
	}

	@Override
	public TipoParametro selectTipoParametro(long comUnitariaId) {
		CompensacionUnitaria compensacionUnitaria = null;
		compensacionUnitaria = ReadById(comUnitariaId);
        
        return compensacionUnitaria.getTipoParametro();
	}

	@Override
	public List<CompensacionUnitaria> getAllRows() {
		List<CompensacionUnitaria> compensacionUnitarias=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from CompensacionUnitaria");
	            
	            compensacionUnitarias=(List<CompensacionUnitaria>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();
 
	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Compensacion Unitaria .", e);
	        }
		return compensacionUnitarias;
	}
	
	@Override
	public List<CompensacionUnitaria> getAllRows1() {
		List<CompensacionUnitaria> compensacionUnitarias=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from CompensacionUnitaria where estado =:estado");
	            q.setString("estado","1");
	            compensacionUnitarias=(List<CompensacionUnitaria>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();
 
	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Compensacion Unitaria .", e);
	        }
		return compensacionUnitarias;
	}
}
