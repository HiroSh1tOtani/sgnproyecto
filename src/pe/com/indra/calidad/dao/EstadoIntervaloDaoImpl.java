package pe.com.indra.calidad.dao;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import java.util.List;
import java.util.Set;

import pe.com.indra.calidad.entity.EstadoIntervalo;
import pe.com.indra.calidad.entity.IntervaloPerturbacion;
import pe.com.indra.calidad.entity.IntervaloTension;

/**
 *
 * @author Luis
 */
public class EstadoIntervaloDaoImpl implements EstadoIntervaloDao {

    private static EstadoIntervaloDaoImpl instance = null;

    public static EstadoIntervaloDaoImpl getInstance() {
        if (instance == null) {
            instance = new EstadoIntervaloDaoImpl();
        }

        return instance;
    }

    public EstadoIntervaloDaoImpl() {
    }

    @Override
    public void create(EstadoIntervalo estadoIntervalo) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(estadoIntervalo);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Estado.", e);
        }
    }

    @Override
    public EstadoIntervalo ReadById(long estIntervaloId) {
        EstadoIntervalo estadoIntervalo = null;
        try {
            HibernateUtil.begin();

            /*
             Query q = HibernateUtil.getSession().createQuery("from CAL_TIPO_PARAMETRO where TIP_PARAMETRO_ID = :tipParametroId");
             q.setLong("tipParametroId",tipParametroId);
             tipoParametro = (TipoParametro)q.uniqueResult();*/

            estadoIntervalo = (EstadoIntervalo) HibernateUtil.getSession().load(EstadoIntervalo.class, estIntervaloId);

            Hibernate.initialize(estadoIntervalo);
            Hibernate.initialize(estadoIntervalo.getCodEstIntervalo());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Estado.", e);
        }
        return estadoIntervalo;
    }

    @Override
    public EstadoIntervalo ReadByCod(String codEstIntervalo) {
        EstadoIntervalo estadoIntervalo = null;
        try {
            HibernateUtil.begin();

            Query q = HibernateUtil.getSession().createQuery("from EstadoIntervalo where codEstIntervalo = :codEstIntervalo and estado = '1'");
            q.setString("codEstIntervalo", codEstIntervalo);
            estadoIntervalo = (EstadoIntervalo) q.uniqueResult();

            Hibernate.initialize(estadoIntervalo);
            Hibernate.initialize(estadoIntervalo.getCodEstIntervalo());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Estado.", e);
        }
        return estadoIntervalo;
    }

    @Override
    public void update(EstadoIntervalo estadoIntervalo) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(estadoIntervalo);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Estado.", e);
        }
    }

    @Override
    public void delete(long estIntervaloId) {
         EstadoIntervalo estadoIntervalo = null;
        estadoIntervalo = ReadById(estIntervaloId);

        if (estadoIntervalo != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(estadoIntervalo);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Estado.", e);
            }
        }
    }

    @Override
    public Set<IntervaloTension> selectIntervaloTension(long estIntervaloId) {
        EstadoIntervalo estadoIntervalo = null;
        estadoIntervalo = ReadById(estIntervaloId);

        return estadoIntervalo.getIntervaloTensiones();
    }

	@Override
	public Set<IntervaloPerturbacion> selectIntervaloPerturbacionFlicker(long estIntervaloId) {
		EstadoIntervalo estadoIntervalo = null;
		estadoIntervalo = ReadById(estIntervaloId);
		return estadoIntervalo.getIntervaloPerturbacionesFli();
	}

	@Override
	public Set<IntervaloPerturbacion> selectIntervaloPerturbacionThd(long estIntervaloId) {
		EstadoIntervalo estadoIntervalo = null;
		estadoIntervalo = ReadById(estIntervaloId);
		return estadoIntervalo.getIntervaloPerturbacionesArm();
	}

	@Override
	public Set<IntervaloPerturbacion> selectIntervaloPerturbacionV(long estIntervaloId) {
		EstadoIntervalo estadoIntervalo = null;
		estadoIntervalo = ReadById(estIntervaloId);
		return estadoIntervalo.getIntervaloPerturbacionesArmv();
	}
}
