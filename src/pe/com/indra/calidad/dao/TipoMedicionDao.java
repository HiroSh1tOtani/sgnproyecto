package pe.com.indra.calidad.dao;

/**
 *
 * @author Luis
 */

import java.util.List;

import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.TipoMedicion;

import java.util.Set;

public interface TipoMedicionDao {
    
    public void create(TipoMedicion tipoMedicion);

    public TipoMedicion ReadById(long tipMedicionId);
    
    public TipoMedicion ReadByCod(String codTipMedicion);

    public void update(TipoMedicion tipoMedicion);

    public void delete(long tipMedicionId);

    public Set<Medicion> selectMedicion(long tipMedicionId);
    
    public List<TipoMedicion> getAllRows();
    
    public List<TipoMedicion> getAllRows1();
}
