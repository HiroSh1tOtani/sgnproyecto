package pe.com.indra.calidad.dao;

import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.indra.calidad.entity.Localidad;
import pe.com.indra.calidad.entity.Medicion;

/**
 *
 * @author Luis
 */
public class LocalidadDaoImpl implements LocalidadDao {

    private static LocalidadDaoImpl instance = null;

    public static LocalidadDaoImpl getInstance() {
        if (instance == null) {
            instance = new LocalidadDaoImpl();
        }

        return instance;
    }

    public LocalidadDaoImpl() {
    }

    @Override
    public void create(Localidad localidad) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(localidad);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Localidad.", e);
        }
    }

    @Override
    public Localidad ReadById(long localidadId) {
        Localidad localidad = null;
        try {
            HibernateUtil.begin();

            /*
             Query q = HibernateUtil.getSession().createQuery("from CAL_TIPO_PARAMETRO where TIP_PARAMETRO_ID = :tipParametroId");
             q.setLong("tipParametroId",tipParametroId);
             tipoParametro = (TipoParametro)q.uniqueResult();*/

            localidad = (Localidad) HibernateUtil.getSession().load(Localidad.class, localidadId);

            Hibernate.initialize(localidad);
            //Hibernate.initialize(localidad.getMediciones());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Localidad.", e);
        }
        return localidad;
    }

    @Override
    public Localidad ReadByCod(String codLocalidad) {
        Localidad localidad = null;
        try {
            HibernateUtil.begin();

            Query q = HibernateUtil.getSession().createQuery("from Localidad where codLocalidad = :codLocalidad and estado = '1'");
            q.setString("codLocalidad", codLocalidad);
            localidad = (Localidad) q.uniqueResult();

            Hibernate.initialize(localidad);
            Hibernate.initialize(localidad.getMediciones());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Localidad.", e);
        }
        return localidad;
    }

    @Override
    public void update(Localidad localidad) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(localidad);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Localidad.", e);
        }
    }

    @Override
    public void delete(long localidadId) {
        Localidad localidad = null;
        localidad = ReadById(localidadId);

        if (localidad != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(localidad);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Localidad.", e);
            }
        }
    }

    @Override
    public Set<Medicion> selectMedicion(long localidadId) {
        Localidad localidad = null;
        localidad = ReadById(localidadId);

        return localidad.getMediciones();
    }
    
    @Override
	public List<Localidad> getAllRows() {
		List<Localidad> localidades=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from Localidad  order by codLocalidad asc");
	            localidades=(List<Localidad>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();
 
	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Localidad .", e);
	        }
		return localidades;
	}
    
    @Override
	public List<Localidad> getAllRows1() {
		List<Localidad> localidades=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from Localidad where estado =:estado order by nombre asc");
	            q.setString("estado","1");
	            localidades=(List<Localidad>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();
 
	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Localidad .", e);
	        }
		return localidades;
	}
}
