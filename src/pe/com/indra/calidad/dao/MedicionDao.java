
package pe.com.indra.calidad.dao;

import java.util.List;
import java.util.Set;

import pe.com.indra.calidad.entity.CampanaMedicion;
import pe.com.indra.calidad.entity.CompensacionTension;
import pe.com.indra.calidad.entity.CronogramaMedicion;
import pe.com.indra.calidad.entity.EquipoRegistrador;
import pe.com.indra.calidad.entity.EstadoIntervalo;
import pe.com.indra.calidad.entity.EstadoMedicion;
import pe.com.indra.calidad.entity.FactorCompensacion;
import pe.com.indra.calidad.entity.IntervaloFRTension;
import pe.com.indra.calidad.entity.IntervaloTension;
import pe.com.indra.calidad.entity.Localidad;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.ParametroMedido;
import pe.com.indra.calidad.entity.TipoAlimentacion;
import pe.com.indra.calidad.entity.TipoMedicion;
import pe.com.indra.calidad.entity.TipoPunto;
import pe.com.indra.calidad.entity.TipoTrabajo;

/**
 *
 * @author Luis
 */
public interface MedicionDao {
    
    public void create(Medicion medicion);
    public Medicion ReadById(long medicionId);
    public void update(Medicion medicion);
    public void delete(long medicionId);
    
    public EstadoMedicion selectEstadoMedicion(long medicionId);
    public ParametroMedido selectParametroMedido(long medicionId);
    public EquipoRegistrador selectEquipoRegistrador(long medicionId);
    public TipoTrabajo selectTipoTrabajo(long medicionId);
    public TipoAlimentacion selectTipoAlimentacion(long medicionId);
    public Localidad selectLocalidad(long medicionId);
    public TipoPunto selectTipoPunto(long medicionId);
    public TipoMedicion selectTipoMedicion(long medicionId);
    public CampanaMedicion selectCampanaMedicion(long medicionId);
    
    public EstadoMedicion selectEstadoMedicionPer(long medicionId);
    
    public Set<IntervaloTension> selectIntervaloTension(long medicionId);
    public Set<IntervaloFRTension> selectIntervaloFRTension(long medicionId);
    public Set<FactorCompensacion> selectFactorCompensacion(long medicionId);
    public Set<CompensacionTension> selectCompensacionTension(long medicionId);
    public Set<CronogramaMedicion> selectCronogramaMedicion(long medicionId);
    public List<Medicion> getMedicionxCampana(long camMedicionId);
    public List<Medicion> getMedicionxCampanaNoProgramados(long camMedicionId);
    public List<Medicion> getMedicionxCampana(long camMedicionId, String tipServicio, String tipPunto);
    public List<Medicion> getMedicionxCampana();
    public List<Medicion> getMedicionxSuministro(String numSumMedicion);
    public Medicion getMedicionDuoRural(long camMedicionId, long medicionId);
    
}
