package pe.com.indra.calidad.dao;

import pe.com.indra.calidad.entity.FactorCompensacion;
import pe.com.indra.calidad.entity.Medicion;

/**
 *
 * @author Luis
 */
public interface FactorCompensacionDao {

    public void create(FactorCompensacion factorCompensacion);

    public FactorCompensacion ReadById(long facCompensacionId);

    public void update(FactorCompensacion factorCompensacion);

    public void delete(long facCompensacionId);

    public Medicion selectMedicion(long facCompensacionId);
}
