
package pe.com.indra.calidad.dao;

import java.util.List;

import pe.com.indra.calidad.entity.CampanaMedicion;
import pe.com.indra.calidad.entity.ParametroNorma;
import pe.com.indra.calidad.entity.TipoParametro;
import pe.com.indra.calidad.entity.CompensacionUnitaria;

import java.util.Set;
/**
 *
 * @author Juan
 */
public interface TipoParametroDao {
	public void create(TipoParametro tipoParametro);
    public TipoParametro ReadById(long tipParametroId);
	public TipoParametro ReadByCod(String codTipParametro);
	public void update(TipoParametro tipoParametro);
	public void delete(long tipParametroId);
    public Set<ParametroNorma> selectParametroNormas(long tipParametroId);
    public Set<CampanaMedicion> selectCampanaMediciones(long tipParametroId);
    public Set<CompensacionUnitaria> selectCompensacionUnitarias(long tipParametroId);
	public List<TipoParametro> getAllRows();
	public List<TipoParametro> getAllRows1();
}
