/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.indra.calidad.dao;

import java.util.Set;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.indra.calidad.entity.CampanaMedicion;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.PeriodoMedicion;
import pe.com.indra.calidad.entity.TipoParametro;

/**
 *
 * @author Luis
 */
public class CampanaMedicionDaoImpl implements CampanaMedicionDao {

    private static CampanaMedicionDaoImpl instance = null;

    public static CampanaMedicionDaoImpl getInstance() {
        if (instance == null) {
            instance = new CampanaMedicionDaoImpl();
        }

        return instance;
    }

    public CampanaMedicionDaoImpl() {
    }

    @Override
    public void create(CampanaMedicion campanaMedicion) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(campanaMedicion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            System.out.print("Des=" + campanaMedicion.getDescripcion() + " Est:="+ campanaMedicion.getEstado() + "Per=" + campanaMedicion.getPeriodoMedicion().getDescripcion());
            throw new HibernateException("No se puede crear Campa�a de Medicion.", e);
        }
    }

    @Override
    public CampanaMedicion ReadById(long camMedicionId) {
        CampanaMedicion campanaMedicion = null;
        try {
            HibernateUtil.begin();

            /*
             Query q = HibernateUtil.getSession().createQuery("from CAL_CAMPANA_MEDICON where CAM_MEDICION_ID = :camMedicionId");
             q.setLong("camMedicionId",camMedicionId);
             campanaMedicion = (campanaMedicion)q.uniqueResult();*/

            campanaMedicion = (CampanaMedicion) HibernateUtil.getSession().load(CampanaMedicion.class, camMedicionId);

            Hibernate.initialize(campanaMedicion);
            Hibernate.initialize(campanaMedicion.getTipoParametro());
            Hibernate.initialize(campanaMedicion.getPeriodoMedicion());
            Hibernate.initialize(campanaMedicion.getMediciones());
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Campa�a de Medicion.", e);
        }
        return campanaMedicion;
    }

    @Override
    public void update(CampanaMedicion campanaMedicion) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(campanaMedicion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Campa�a.", e);
        }
    }

    @Override
    public void delete(long camMedicionId) {

        CampanaMedicion campanaMedicion = null;
        campanaMedicion = ReadById(camMedicionId);

        if (campanaMedicion != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(campanaMedicion);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Campaña de Medición.", e);
            }
        }
    }

    @Override
    public TipoParametro selectTipoParametro(long camMedicionId) {
        CampanaMedicion campanaMedicion = null;
        campanaMedicion = ReadById(camMedicionId);

        return campanaMedicion.getTipoParametro();
    }

    @Override
    public PeriodoMedicion selectPeriodoMedicion(long camMedicionId) {
        CampanaMedicion campanaMedicion = null;
        campanaMedicion = ReadById(camMedicionId);

        return campanaMedicion.getPeriodoMedicion();
    }

    @Override
    public Set<Medicion> selectMedicion(long camMedicionId) {
        CampanaMedicion campanaMedicion = null;
        campanaMedicion = ReadById(camMedicionId);

        return campanaMedicion.getMediciones();
    }

	@Override
	public List<CampanaMedicion> getAllRows() {
		List<CampanaMedicion> campanaMediciones=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from CampanaMedicion order by periodoMedicion.ano desc, periodoMedicion.periodo desc");
	            
	            campanaMediciones=(List<CampanaMedicion>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Camapa�a Medicion.", e);
	        }
		return campanaMediciones;
	}

	@Override
	public List<CampanaMedicion> getAllRows1() {
		List<CampanaMedicion> campanaMediciones=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from CampanaMedicion where estado =:estado order by periodoMedicion.ano desc, periodoMedicion.periodo desc");
	            q.setString("estado","1");
	            campanaMediciones=(List<CampanaMedicion>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Camapa�a Medicion.", e);
	        }
		return campanaMediciones;
	}
    
    
}
