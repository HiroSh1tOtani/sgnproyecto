package pe.com.indra.calidad.test;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.awt.BasicStroke;
import java.awt.Color;
import java.io.OutputStream;
import java.math.RoundingMode;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import com.google.common.math.DoubleMath;

import pe.com.indra.calidad.entity.IntervaloTension;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.service.IntervaloTensionService;
import pe.com.indra.calidad.service.MedicionService;



/**
 * Servlet implementation class ServletLine
 */
public class ServletLineX extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private XYDataset generaDatos(long medicionId, String tension) {
		long indice;
		long limInf = 0;
		long limSup = 0;
		
		IntervaloTensionService service=new IntervaloTensionService();
		
		MedicionService medicionService=new MedicionService();
		
		Medicion medicion = medicionService.ReadById(medicionId);
		
		Long tenEntregada = medicion.getTenEntregada();
		
		
		if(medicion.getTipServicio().equals("U")){
			limInf = DoubleMath.roundToLong(tenEntregada*(1.05), RoundingMode.HALF_UP);
			limSup = DoubleMath.roundToLong(tenEntregada*(0.95), RoundingMode.HALF_UP);
		}
		
		if(medicion.getTipServicio().equals("R")||medicion.getTipServicio().equals("UR")){
			if(medicion.getTipSuministro().equals("BT")) {
				limInf = DoubleMath.roundToLong(tenEntregada*(1.075), RoundingMode.HALF_UP);
				limSup = DoubleMath.roundToLong(tenEntregada*(0.925), RoundingMode.HALF_UP);
			}
			
			if(medicion.getTipSuministro().equals("MT")) {
				limInf = DoubleMath.roundToLong(tenEntregada*(1.06), RoundingMode.HALF_UP);
				limSup = DoubleMath.roundToLong(tenEntregada*(0.94), RoundingMode.HALF_UP);
			}			
		}
		
		if(medicion.getTipServicio() == null){
			limInf = DoubleMath.roundToLong(tenEntregada*(1.05), RoundingMode.HALF_UP);
			limSup = DoubleMath.roundToLong(tenEntregada*(0.95), RoundingMode.HALF_UP);			
		}
		
		
		indice = 1;
		
		XYSeries serie1 = new XYSeries("Tension R");
		XYSeries serie2 = new XYSeries("Tension S");
		XYSeries serie3 = new XYSeries("Tension T");
		XYSeries serie4 = new XYSeries("Limite Superior");
		XYSeries serie5 = new XYSeries("Limite Inferior");
		
		List<IntervaloTension> lista = service.getAllRows1(medicionId);
		
		if(lista.size() == 0) {
			lista = service.getAllRows(medicionId);
		}
		
		for(IntervaloTension p:lista){
		
		  if(tension.equals("R")){
			  serie1.add(indice, p.getTensionR());
			  
		  }


		  if(tension.equals("S")){
			  serie2.add(indice, p.getTensionS());

		  }		

		  if(tension.equals("T")){
			  serie3.add(indice, p.getTensionT());

		  }		
		  
		  if(tension.equals("RST")){
			  serie1.add(indice, p.getTensionR());
			  serie2.add(indice, p.getTensionS());
			  serie3.add(indice, p.getTensionT());

		  }	
		  
		  serie4.add(indice, limSup);
		  serie5.add(indice, limInf);
		  
		  indice = indice + 1;
		}
		 
		XYSeriesCollection xyseriescollection =
				 new XYSeriesCollection();
		
		if(tension.equals("R")){
		
		 xyseriescollection.addSeries(serie1);
		 xyseriescollection.addSeries(serie4);
		 xyseriescollection.addSeries(serie5);
		}
		
		if(tension.equals("S")){
			 
			 xyseriescollection.addSeries(serie2);
			 xyseriescollection.addSeries(serie4);
			 xyseriescollection.addSeries(serie5);
		}
		
		if(tension.equals("T")){
			 
			 xyseriescollection.addSeries(serie3);
			 xyseriescollection.addSeries(serie4);
			 xyseriescollection.addSeries(serie5);
		}
		
		
		if(tension.equals("RST")){
			 
			 xyseriescollection.addSeries(serie1);
			 xyseriescollection.addSeries(serie2);
			 xyseriescollection.addSeries(serie3);
			 xyseriescollection.addSeries(serie4);
			 xyseriescollection.addSeries(serie5);
		}
		 return xyseriescollection;
	}	
	
	 private static JFreeChart generaGrafico(XYDataset xydataset, long medicionId, String tension) {
		 JFreeChart jfreechart = ChartFactory.createXYLineChart(
		 "Perfil de Tensi�n", "Intervalos (15m)", "Tension (v)",
		 xydataset, PlotOrientation.VERTICAL,
		 true, true, false);
		 
		 
		 XYPlot xyplot = (XYPlot) jfreechart.getPlot();
		 xyplot.setBackgroundPaint(Color.white);
		 xyplot.setDomainGridlinePaint(Color.gray);
		 xyplot.setRangeGridlinePaint(Color.gray);
		 
		 IntervaloTensionService service=new IntervaloTensionService();
			
		 MedicionService medicionService=new MedicionService();		
		 Medicion medicion = medicionService.ReadById(medicionId);		
		 Long tenEntregada = medicion.getTenEntregada();
		 
		 xyplot.getRangeAxis().setRange(DoubleMath.roundToLong(tenEntregada*(1 - 0.1), RoundingMode.HALF_UP), DoubleMath.roundToLong(tenEntregada*(1 + 0.1), RoundingMode.HALF_UP));
		 
		 if(tension.equals("R")) {
			 xyplot.getRenderer().setSeriesPaint(0, Color.blue);
			 xyplot.getRenderer().setSeriesPaint(1, Color.red);
			 xyplot.getRenderer().setSeriesPaint(2, Color.red);
		 }
		 
		 if(tension.equals("S")) {
			 xyplot.getRenderer().setSeriesPaint(0, Color.green);
			 xyplot.getRenderer().setSeriesPaint(1, Color.red);
			 xyplot.getRenderer().setSeriesPaint(2, Color.red);
		 }
		 
		 if(tension.equals("T")) {
			 xyplot.getRenderer().setSeriesPaint(0, Color.yellow);
			 xyplot.getRenderer().setSeriesPaint(1, Color.red);
			 xyplot.getRenderer().setSeriesPaint(2, Color.red);
		 }
		 
		 if(tension.equals("RST")) {
			 xyplot.getRenderer().setSeriesPaint(0, Color.blue);
			 xyplot.getRenderer().setSeriesPaint(1, Color.green);
			 xyplot.getRenderer().setSeriesPaint(2, Color.yellow);
			 xyplot.getRenderer().setSeriesPaint(3, Color.red);
			 xyplot.getRenderer().setSeriesPaint(4, Color.red);

		 }
		 
		 
		 /*
		 xyplot.getRenderer().setSeriesStroke(
		           0, new BasicStroke(
			               2.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER,
			               1.0f, new float[] {10.0f, 6.0f}, 0.0f)
			       );
		 
		 
		 xyplot.getRenderer().setSeriesStroke(
		           1, new BasicStroke(
			               2.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER,
			               1.0f, new float[] {10.0f, 6.0f}, 0.0f)
			       );
		 
		 xyplot.getRenderer().setSeriesStroke(
		           2, new BasicStroke(
			               2.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER,
			               1.0f, new float[] {10.0f, 6.0f}, 0.0f)
			       );
		 
		 xyplot.getRenderer().setSeriesStroke(
		           3, new BasicStroke(
			               2.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER,
			               1.0f, new float[] {10.0f, 6.0f}, 0.0f)
			       );
		 
		 xyplot.getRenderer().setSeriesStroke(
		           4, new BasicStroke(
			               2.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER,
			               1.0f, new float[] {10.0f, 6.0f}, 0.0f)
			       );
		 
		 */
//       renderer.setDrawShapes(true);



		 
		 XYLineAndShapeRenderer xylineandshaperenderer =
		 (XYLineAndShapeRenderer) xyplot.getRenderer();
		 xylineandshaperenderer.setBaseShapesVisible(false);
		 
		 return jfreechart;
	 }
	 
	
	 
	 protected void processRequest(HttpServletRequest
		 request, HttpServletResponse response)
		 throws ServletException, IOException {
		 
		 String name = "MEDICIONID";
		 String name2 = "TENSION";
		 String tension;
		 long medicionId = new Long(request.getParameter(name));
		 tension = new String(request.getParameter(name2));
		
		 System.out.print("TENSION:" + tension);
		 response.setContentType("image/jpeg");
		 OutputStream out = response.getOutputStream();
		 
		 //CategoryDataset  xydataset = generaDatos3(medicionId);
		 XYDataset xydataset = generaDatos(medicionId, tension);
		 JFreeChart grafico = generaGrafico(xydataset, medicionId, tension);
		 ChartUtilities.writeChartAsJPEG(out, grafico, 780, 550);
		 
		 out.close();
	 }
		 
		 // <editor-fold defaultstate="collapsed" desc="m�todos doGet y doPost creados por NetBeans">
		 /**
		 * Handles the HTTP <code>GET</code> method.
		 * @param request servlet request
		 * @param response servlet response
		 * @throws ServletException if a servlet-specific error occurs
		 * @throws IOException if an I/O error occurs
		 */
	 @Override
	 protected void doGet(HttpServletRequest request, HttpServletResponse response)
	 throws ServletException, IOException {
	 processRequest(request, response);
	 }
		 
		 /**
		 * Handles the HTTP <code>POST</code> method.
		 * @param request servlet request
		 * @param response servlet response
		 * @throws ServletException if a servlet-specific error occurs
		 * @throws IOException if an I/O error occurs
		 */
	 @Override
	 protected void doPost(HttpServletRequest request, HttpServletResponse response)
	 throws ServletException, IOException {
	 processRequest(request, response);
	 }
		 
		 /**
		 * Returns a short description of the servlet.
		 * @return a String containing servlet description
		 */
	 @Override
	 public String getServletInfo() {
	 return "Short description";
	 }// </editor-fold>

}
