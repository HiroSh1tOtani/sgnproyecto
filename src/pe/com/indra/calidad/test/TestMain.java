package pe.com.indra.calidad.test;

import pe.com.indra.calidad.dao.*;
import pe.com.indra.calidad.entity.TipoParametro;

public class TestMain {

	public static void main(String[] args) {
		System.out.println("Iniciando el programa");
		TipoParametroDao dao=new TipoParametroDaoImpl();
		for(TipoParametro p:dao.getAllRows()){
		 System.out.println(p.getCodTipParametro()+" "+p.getDescripcion());
		}

	}

}
