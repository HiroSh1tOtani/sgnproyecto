package pe.com.indra.calidad.test;

import java.util.List;

import pe.com.indra.calidad.dao.CronogramaMedicionDao;
import pe.com.indra.calidad.dao.CronogramaMedicionDaoImpl;
import pe.com.indra.calidad.dao.CuadrillaDao;
import pe.com.indra.calidad.dao.CuadrillaDaoImpl;
import pe.com.indra.calidad.dao.MedicionDao;
import pe.com.indra.calidad.dao.MedicionDaoImpl;
import pe.com.indra.calidad.entity.CampanaMedicion;
import pe.com.indra.calidad.entity.CronogramaMedicion;
import pe.com.indra.calidad.entity.Cuadrilla;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.service.CronogramaMedicionService;


public class TestCronogramaMedicion {

	public static void main(String[] args) {
		System.out.println("Iniciando el programa");
		/*
		CronogramaMedicionDao dao = new CronogramaMedicionDaoImpl();
		List<CronogramaMedicion> cronogramasMedicion = dao.getCronogramaxCampana(1);
		
		for(CronogramaMedicion c:cronogramasMedicion) {
			System.out.println(c.getFecPlanificada());
		}
			*/
		
		System.out.println("Iniciando el programa");
		CronogramaMedicionDao dao=new CronogramaMedicionDaoImpl();
		CronogramaMedicionService service = new CronogramaMedicionService();
		
		CuadrillaDao daoCuadrilla =new CuadrillaDaoImpl();
		MedicionDao daoMedicion =new MedicionDaoImpl();
		
		CronogramaMedicion cronogramaMedicion = new CronogramaMedicion();
		
		java.util.Date utilDate = new java.util.Date();
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
		
        Cuadrilla cuadrilla = daoCuadrilla.ReadById(65);
        Medicion medicion = daoMedicion.ReadById(105289);
        
		cronogramaMedicion.setCuadrilla(cuadrilla);
		cronogramaMedicion.setMedicion(medicion);
		cronogramaMedicion.setFecPlanificada(sqlDate);
		cronogramaMedicion.setEstado("1");
		
		//dao.create(cronogramaMedicion);
		service.create(cronogramaMedicion);
		
		System.out.print("Se salvo cronograma con exito\n");
		
		
			
	}

}
