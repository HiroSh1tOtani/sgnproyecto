package pe.com.indra.calidad.test;

import pe.com.indra.calidad.dao.EstadoMedicionDao;
import pe.com.indra.calidad.dao.EstadoMedicionDaoImpl;
import pe.com.indra.calidad.entity.EstadoMedicion;



public class TestEstadoMedicion {

	public static void main(String[] args) {
		System.out.println("Iniciando el programa");
		EstadoMedicionDao dao=new EstadoMedicionDaoImpl();
		
		EstadoMedicion estadoMedicion = dao.ReadByCod("G");
		
		System.out.print(estadoMedicion.getDescripcion());
		
	}

}