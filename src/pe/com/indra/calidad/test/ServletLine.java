package pe.com.indra.calidad.test;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.awt.BasicStroke;
import java.awt.Color;
import java.io.OutputStream;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import pe.com.indra.calidad.entity.IntervaloTension;
import pe.com.indra.calidad.service.IntervaloTensionService;



/**
 * Servlet implementation class ServletLine
 */
public class ServletLine extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	private XYDataset generaDatos(long medicionId) {
		long indice;
		
		IntervaloTensionService service=new IntervaloTensionService();
		
		indice = 1;
		XYSeries serie1 = new XYSeries("Tension R");
		XYSeries serie2 = new XYSeries("Tension S");
		XYSeries serie3 = new XYSeries("Tension T");
		XYSeries serie4 = new XYSeries("Limite Superior");
		XYSeries serie5 = new XYSeries("Limite Inferior");
		
		
		for(IntervaloTension p:service.getAllRows1(medicionId)){
			
		  serie1.add(indice, p.getTensionR());
	      serie2.add(indice, p.getTensionS());		 
		  serie3.add(indice, p.getTensionT());

		  
		  serie4.add(indice, 231);
		  serie5.add(indice, 209);
		  indice = indice + 1;
		}
		 
		 XYSeriesCollection xyseriescollection =
		 new XYSeriesCollection();
		 xyseriescollection.addSeries(serie1);
		 xyseriescollection.addSeries(serie2);
		 xyseriescollection.addSeries(serie3);
		 xyseriescollection.addSeries(serie4);
		 xyseriescollection.addSeries(serie5);
		 
		 return xyseriescollection;
	}

	private CategoryDataset  generaDatos3(long medicionId) {
		long indice;
		
		IntervaloTensionService service=new IntervaloTensionService();
		
		indice = 1;
		String serie1 = "Tension R";
		String serie2 = "Tension S";
		String serie3 = "Tension T";
		String serie4 = "Limite Superior";
		String serie5 = "Limite Inferior";
		
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		
		for(IntervaloTension p:service.getAllRows1(medicionId)){
			
		  Number x = 231;
		  Number y = 209;
		  dataset.addValue(p.getTensionR(), serie1, Long.toString(indice));
		  dataset.addValue(p.getTensionS(), serie2, Long.toString(indice));
		  dataset.addValue(p.getTensionT(), serie3, Long.toString(indice));
		  dataset.addValue(x, serie4, Long.toString(indice));
		  dataset.addValue(y, serie5, Long.toString(indice));
		  
		  indice = indice + 1;
		}
		 
		 return dataset;
	}
	private XYDataset generaDatos2(long medicionId) {
		long indice;
		
		IntervaloTensionService service=new IntervaloTensionService();
		
		indice = 1;
		XYSeries serie1 = new XYSeries("Tension R");
		XYSeries serie2 = new XYSeries("Limite Superior");
		XYSeries serie3 = new XYSeries("Limite Inferior");
		
		for(IntervaloTension p:service.getAllRows1(medicionId)){
			
		  serie1.add(indice, p.getTensionR());
		  serie2.add(indice, 231);
		  serie3.add(indice, 209);
		  indice = indice + 1;
		}
		 
		 XYSeriesCollection xyseriescollection =
		 new XYSeriesCollection();
		 xyseriescollection.addSeries(serie1);
		 xyseriescollection.addSeries(serie2);
		 xyseriescollection.addSeries(serie3);
		 
		 return xyseriescollection;
	}

	
	private XYDataset generaDatos(long medicionId, String tension) {
		long indice;
		
		IntervaloTensionService service=new IntervaloTensionService();
		
		indice = 1;
		

		XYSeries serie1 = new XYSeries("Tension " + tension);
		XYSeries serie2 = new XYSeries("Limite Superior");
		XYSeries serie3 = new XYSeries("Limite Inferior");
		
		for(IntervaloTension p:service.getAllRows1(medicionId)){
		
		  if(tension.equals("R")){
			  serie1.add(indice, p.getTensionR());
		  }


		  if(tension.equals("S")){
			  serie1.add(indice, p.getTensionS());
		  }		

		  if(tension.equals("T")){
			  serie1.add(indice, p.getTensionT());
		  }		  
		  
		  serie2.add(indice, 231);
		  serie3.add(indice, 209);
		  indice = indice + 1;
		}
		 
		 XYSeriesCollection xyseriescollection =
		 new XYSeriesCollection();
		 xyseriescollection.addSeries(serie1);
		 xyseriescollection.addSeries(serie2);
		 xyseriescollection.addSeries(serie3);
		 
		 return xyseriescollection;
	}	
	
	 private static JFreeChart generaGrafico(XYDataset xydataset) {
		 JFreeChart jfreechart = ChartFactory.createXYLineChart(
		 "Perfil de la Medici�n", "Intervalos (15m)", "Tension (v)",
		 xydataset, PlotOrientation.VERTICAL,
		 true, true, false);
		 
		 
		 XYPlot xyplot = (XYPlot) jfreechart.getPlot();
		 xyplot.setBackgroundPaint(Color.white);
		 xyplot.setDomainGridlinePaint(Color.gray);
		 xyplot.setRangeGridlinePaint(Color.gray);
		 xyplot.getRangeAxis().setRange(200, 240);
		 
		 xyplot.getRenderer().setSeriesPaint(0, Color.blue);
		 xyplot.getRenderer().setSeriesPaint(1, Color.green);
		 xyplot.getRenderer().setSeriesPaint(2, Color.yellow);
		 xyplot.getRenderer().setSeriesPaint(3, Color.red);
		 xyplot.getRenderer().setSeriesPaint(4, Color.red);
		 
		 /*
		 xyplot.getRenderer().setSeriesStroke(
		           0, new BasicStroke(
			               2.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER,
			               1.0f, new float[] {10.0f, 6.0f}, 0.0f)
			       );
		 
		 
		 xyplot.getRenderer().setSeriesStroke(
		           1, new BasicStroke(
			               2.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER,
			               1.0f, new float[] {10.0f, 6.0f}, 0.0f)
			       );
		 
		 xyplot.getRenderer().setSeriesStroke(
		           2, new BasicStroke(
			               2.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER,
			               1.0f, new float[] {10.0f, 6.0f}, 0.0f)
			       );
		 
		 xyplot.getRenderer().setSeriesStroke(
		           3, new BasicStroke(
			               2.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER,
			               1.0f, new float[] {10.0f, 6.0f}, 0.0f)
			       );
		 
		 xyplot.getRenderer().setSeriesStroke(
		           4, new BasicStroke(
			               2.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER,
			               1.0f, new float[] {10.0f, 6.0f}, 0.0f)
			       );
		 
		 */
//       renderer.setDrawShapes(true);



		 
		 XYLineAndShapeRenderer xylineandshaperenderer =
		 (XYLineAndShapeRenderer) xyplot.getRenderer();
		 xylineandshaperenderer.setBaseShapesVisible(false);
		 
		 return jfreechart;
	 }
	 
	 private static JFreeChart generaGrafico2(CategoryDataset  xydataset) {
		 JFreeChart jfreechart = ChartFactory.createLineChart(
		 "Perfil de la Medici�n", "Intervalos (15m)", "Tension (v)",
		 xydataset, PlotOrientation.VERTICAL,
		 true, true, false);
		 
		 CategoryPlot xyplot = (CategoryPlot) jfreechart.getPlot();
		 
		 xyplot.setBackgroundPaint(Color.white);
		 xyplot.setDomainGridlinePaint(Color.gray);
		 xyplot.setRangeGridlinePaint(Color.gray);
		 xyplot.getRangeAxis().setRange(200, 240);
		 
		 
		 
		 xyplot.getRenderer().setSeriesPaint(0, Color.blue);
		 xyplot.getRenderer().setSeriesPaint(1, Color.green);
		 xyplot.getRenderer().setSeriesPaint(2, Color.yellow);
		 xyplot.getRenderer().setSeriesPaint(3, Color.red);
		 xyplot.getRenderer().setSeriesPaint(4, Color.red);
		 
		 LineAndShapeRenderer  xylineandshaperenderer =
				 (LineAndShapeRenderer ) xyplot.getRenderer();
		 /*
		 xylineandshaperenderer.setSeriesStroke(
		           0, new BasicStroke(
			               2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND,
			               1.0f, new float[] {10.0f, 6.0f}, 0.0f)
			       );
		 
		 
		 xylineandshaperenderer.setSeriesStroke(
		           1, new BasicStroke(
			               2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND,
			               1.0f, new float[] {10.0f, 6.0f}, 0.0f)
			       );
		 
		 xylineandshaperenderer.setSeriesStroke(
		           2, new BasicStroke(
			               2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND,
			               1.0f, new float[] {10.0f, 6.0f}, 0.0f)
			       );
		 
		 xylineandshaperenderer.setSeriesStroke(
		           3, new BasicStroke(
			               2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND,
			               1.0f, new float[] {10.0f, 6.0f}, 0.0f)
			       );
		 
		 xylineandshaperenderer.setSeriesStroke(
		           4, new BasicStroke(
			               2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND,
			               1.0f, new float[] {10.0f, 6.0f}, 0.0f)
			       );*/
//       renderer.setDrawShapes(true);
	 
		 
		 xylineandshaperenderer.setBaseShapesVisible(false);
		 
		 return jfreechart;
	 }	 
	 
	 protected void processRequest(HttpServletRequest
		 request, HttpServletResponse response)
		 throws ServletException, IOException {
		 
		 String name = "MEDICIONID";
		 //String tension;
		 long medicionId = new Long(request.getParameter(name));
		 //tension = request.getParameter("TENSION");
		
		 //System.out.print("TENSION:" + tension);
		 response.setContentType("image/jpeg");
		 OutputStream out = response.getOutputStream();
		 
		 //CategoryDataset  xydataset = generaDatos3(medicionId);
		 XYDataset xydataset = generaDatos(medicionId);
		 JFreeChart grafico = generaGrafico(xydataset);
		 ChartUtilities.writeChartAsJPEG(out, grafico, 650, 550);
		 
		 out.close();
	 }
		 
		 // <editor-fold defaultstate="collapsed" desc="m�todos doGet y doPost creados por NetBeans">
		 /**
		 * Handles the HTTP <code>GET</code> method.
		 * @param request servlet request
		 * @param response servlet response
		 * @throws ServletException if a servlet-specific error occurs
		 * @throws IOException if an I/O error occurs
		 */
	 @Override
	 protected void doGet(HttpServletRequest request, HttpServletResponse response)
	 throws ServletException, IOException {
	 processRequest(request, response);
	 }
		 
		 /**
		 * Handles the HTTP <code>POST</code> method.
		 * @param request servlet request
		 * @param response servlet response
		 * @throws ServletException if a servlet-specific error occurs
		 * @throws IOException if an I/O error occurs
		 */
	 @Override
	 protected void doPost(HttpServletRequest request, HttpServletResponse response)
	 throws ServletException, IOException {
	 processRequest(request, response);
	 }
		 
		 /**
		 * Returns a short description of the servlet.
		 * @return a String containing servlet description
		 */
	 @Override
	 public String getServletInfo() {
	 return "Short description";
	 }// </editor-fold>

}
