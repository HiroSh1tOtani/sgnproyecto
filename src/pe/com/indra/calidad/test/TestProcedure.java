package pe.com.indra.calidad.test;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import pe.com.indra.calidad.util.ConectaDb;

public class TestProcedure {
	
	public static void main(String[] args) {
		System.out.println("Iniciando el programa");

		ConectaDb db = new ConectaDb("CALIDAD");		
        Connection cn = db.getConnection();
        CallableStatement cstmt = null;  

        String result;
		try {
		
			cstmt = cn.prepareCall("{CALL CAL_SUMINISTRO_PACKAGE.COMPENSAR_LCE2(?,?,?)}"); 
			
			System.out.println("2015");
			cstmt.setString(1,"2015");
			
			System.out.println("S1");
			cstmt.setString(2,"S1");
			
			System.out.println("03");
			cstmt.setString(3,"03");
							
			cstmt.execute();
			cstmt.close();
		
		
		
		} catch (SQLException e) {
	        result = e.getMessage();
	        System.out.println(result);

	            
		} catch (Exception e) {
	        result = e.getMessage();
	        System.out.println(result);

	            	            
	    } finally {
	        try {
	            cn.close();
	        } catch (SQLException e) {
	            result = e.getMessage();
	            System.out.println(result);
	            
	        }
	    }		

	}

}
