package pe.com.indra.calidad.test;


import pe.com.indra.calidad.dao.EmpresaDaoImpl;
import pe.com.indra.calidad.dao.EmpresaDao;
import pe.com.indra.calidad.entity.Empresa;

public class TestEmpresa {
	
	public static void main(String[] args) {
		System.out.println("Iniciando el programa");
		EmpresaDao dao=new EmpresaDaoImpl();
		for(Empresa p:dao.getAllRows()){
		 System.out.println(p.getEmpresa()+" "+p.getNombre());
		}

	}

}
