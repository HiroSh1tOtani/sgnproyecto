package pe.com.indra.calidad.test;

import pe.com.indra.calidad.dao.TipoTrabajoDao;
import pe.com.indra.calidad.dao.TipoTrabajoDaoImpl;
import pe.com.indra.calidad.entity.TipoTrabajo;


public class TestTipoTrabajo {

	public static void main(String[] args) {
		System.out.println("Iniciando el programa");
		TipoTrabajoDao dao=new TipoTrabajoDaoImpl();
		for(TipoTrabajo p:dao.getAllRows()){
		 System.out.println(p.getCodTipTrabajo()+" "+p.getDescripcion());
		}

	}

}
