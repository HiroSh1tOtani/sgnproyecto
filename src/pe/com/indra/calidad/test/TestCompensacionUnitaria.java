package pe.com.indra.calidad.test;

import java.math.BigDecimal;

import pe.com.indra.calidad.dao.CompensacionUnitariaDao;
import pe.com.indra.calidad.dao.CompensacionUnitariaDaoImpl;
import pe.com.indra.calidad.dao.TipoParametroDao;
import pe.com.indra.calidad.dao.TipoParametroDaoImpl;
import pe.com.indra.calidad.entity.CompensacionUnitaria;
import pe.com.indra.calidad.entity.TipoParametro;
import pe.com.indra.calidad.service.CompensacionUnitariaService;

public class TestCompensacionUnitaria {
	
	public static void main(String[] args) {
		System.out.println("Iniciando el programa");
		CompensacionUnitariaDao dao=new CompensacionUnitariaDaoImpl();
		TipoParametroDao daoTip = new TipoParametroDaoImpl();
		/*for(CompensacionUnitaria p:dao.getAllRows()){
		 System.out.println(p.getCodComUnitaria()+" "+p.getDescripcion());
		}*/
		
		/*
		TipoParametro tipoParametro = daoTip.ReadById(1);
		CompensacionUnitaria compensacionUnitaria = new CompensacionUnitaria( (long)4,"d","nuevossssc", new BigDecimal(2.3),tipoParametro,"1");
		
		dao.create(compensacionUnitaria);*/
		
		CompensacionUnitariaService service=new CompensacionUnitariaService();
		CompensacionUnitaria compensacionUnitaria=service.ReadById(new Long(31));//comUnitariaId
		compensacionUnitaria.setEstado("0");
		service.update(compensacionUnitaria);

	}

}
