package pe.com.indra.calidad.test;


import pe.com.indra.calidad.dao.LocalidadDaoImpl;
import pe.com.indra.calidad.dao.LocalidadDao;
import pe.com.indra.calidad.entity.Localidad;

public class TestLocalidad {
	
	public static void main(String[] args) {
		System.out.println("Iniciando el programa");
		LocalidadDao dao=new LocalidadDaoImpl();
		for(Localidad p:dao.getAllRows()){
		 System.out.println(p.getLocalidadId()+" "+p.getCodLocalidad()+" "+p.getNombre());
		}

	}

}
