package pe.com.indra.calidad.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import pe.com.indra.calidad.util.ConectaDb;



public class TestXML {
	
	public static void main(String[] args) {
		
	    String result;
	    String sCadena;
	    BufferedReader reader = null;
	    ConectaDb db = new ConectaDb("CALIDAD");
		
        Connection cn = db.getConnection();
        
        CallableStatement cstmt = null;  
        try {

			//cstmt = cn.prepareCall("{? = CALL CALIDAD_PACKAGE.FN_GENERA_CONDICION_EN_XML(?)}"); 
        	cstmt = cn.prepareCall("{CALL ? := CALIDAD_PACKAGE.FN_GENERA_CONDICION_EN_XML(?)}");
        	
			cstmt.registerOutParameter(1, Types.CLOB);
			cstmt.setLong(2, 1); //camMedicionId

            int ctos = cstmt.executeUpdate();

            Clob dato = cstmt.getClob(1);
            cstmt.close(); 

            if (ctos == 0) {
                result = "0 filas afectadas";
            }
            
            reader = new BufferedReader(new InputStreamReader(dato.getAsciiStream()));
            
            while((sCadena = reader.readLine()) != null ) { 
				System.out.print(sCadena);
            }
        } catch (IOException e) {
        	
        } catch (SQLException e) {
            result = e.getMessage();
        } finally {
            try {
            	reader.close();
            } catch (IOException e) {//
                result = e.getMessage();
            }
        }	

	}

}
