package pe.com.indra.calidad.test;

import java.util.List;

import pe.com.indra.calidad.util.Sql;



public class TestIgea {

	public static void main(String[] args) {
		Sql sql = new Sql("IGEA");
		
		System.out.println("Iniciando el programa");
		
		String s = "SELECT C.CODE AS SUMINISTRO, C.LABEL AS CODIGO, C.NAME AS NOMBRE, C.PHASES AS FASE, " +
				"C.ADDRESS AS DIRECCION, C.METER AS MEDIDOR, C.RATES_GROUP AS TARIFA, C.KVA AS DEMANDA, " +
				"S.LABEL AS CODSED, A.LABEL AS CODSALIDAMT, T.LABEL AS CODSET, " +
				"(SELECT CS.REPORT_CODE FROM ZNET_SECONDARY_CODE CS WHERE CS.ID = S.SECONDARY_VOLTAGE_LEVEL_ID AND CS.ONIS_VER = 0) AS TENSION " +
				"FROM IGEA_DATA.ZNET_CUSTOMERS C, ZNET_TRANSFORMATION_CENTER S, ZNET_MV_LINE A, ZNET_SUBSTATIONS T " +
				"WHERE C.TRANSFORMATION_CENTER = S.CLASS_ID||':'||S.ID AND  " +
				"S.GRANDFATHER_ELEMENT = A.CLASS_ID||':'||A.ID AND  " +
				"A.FATHER_ELEMENT = T.CLASS_ID||':'||T.ID AND  " +
				"C.ONIS_VER = 0 AND S.ONIS_VER = 0 AND A.ONIS_VER = 0 AND T.ONIS_VER = 0 AND " +
				"C.CODE = '501003177'";
		
		/*String s = "SELECT C.CODE AS SUMINISTRO, C.LABEL AS CODIGO, C.NAME AS NOMBRE, C.PHASES AS FASE," +
				   "C.ADDRESS AS DIRECCION, C.METER AS MEDIDOR, C.RATES_GROUP AS TARIFA, C.KVA AS DEMANDA," +
				   "S.LABEL AS CODSED, A.LABEL AS CODSALIDAMT, T.LABEL AS CODSET " +
				   "FROM IGEA_DATA.ZNET_CUSTOMERS C, ZNET_TRANSFORMATION_CENTER S, ZNET_MV_LINE A, ZNET_SUBSTATIONS T " +
				   "WHERE C.TRANSFORMATION_CENTER = S.CLASS_ID||':'||S.ID AND " +
				   "S.GRANDFATHER_ELEMENT = A.CLASS_ID||':'||A.ID AND " +
				   "A.FATHER_ELEMENT = T.CLASS_ID||':'||T.ID AND " +
				   "C.ONIS_VER = 0 AND S.ONIS_VER = 0 AND A.ONIS_VER = 0 AND T.ONIS_VER = 0 AND " +
				   "C.CODE = '501003177'";*/

        List<Object[]> listObject = sql.consulta(s, false);
		
		
        for (Object[] objects: listObject) {
        	System.out.println((String)objects[0] + " " + (String) objects[1]+ " " + (String) objects[2]+ " " + (String) objects[3]);
        } 

	}

}
