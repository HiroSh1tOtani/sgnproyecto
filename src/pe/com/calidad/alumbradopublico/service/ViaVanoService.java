package pe.com.calidad.alumbradopublico.service;


import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;

import pe.com.calidad.alumbradopublico.dao.ViaVanoDao;
import pe.com.calidad.alumbradopublico.dao.ViaVanoDaoImpl;
import pe.com.calidad.alumbradopublico.entity.MedicionAp;
import pe.com.calidad.alumbradopublico.entity.Via;
import pe.com.calidad.alumbradopublico.entity.ViaVano;


/**
 *
 * @author Luis
 */
public class ViaVanoService {

    public void create(ViaVano viaVano) {
    	ViaVanoDao dao = new ViaVanoDaoImpl();
        
        try {
        	dao.create(viaVano);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Vano.", e);
		}
        
    }

    public ViaVano ReadById(long viaVanoId) {
    	ViaVanoDao dao = new ViaVanoDaoImpl();
    	ViaVano viaVano = null;
        
        try {
        	viaVano = dao.ReadById(viaVanoId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); 
		}
        
        return viaVano;
    }

    public void update(ViaVano viaVano) {
    	ViaVanoDao dao = new ViaVanoDaoImpl();
        
        try {
        	dao.update(viaVano);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public void delete(long viaVanoId) {
    	ViaVanoDao dao = new ViaVanoDaoImpl();
        
        try {
        	dao.delete(viaVanoId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }
    
   
    public Via selectVia(long viaVanoId) {
    	ViaVanoDao dao = new ViaVanoDaoImpl();
        return dao.selectVia(viaVanoId);
    }

    
    public Set<MedicionAp> selectViaVano(long viaVanoId) {
    	ViaVanoDao dao = new ViaVanoDaoImpl();
        return dao.selectViaVano(viaVanoId);
    }
    
     
    public List<ViaVano> getAllRows(){
    	ViaVanoDao dao = new ViaVanoDaoImpl();
    	return dao.getAllRows();
    }  
    
    public List<ViaVano> getAllRows1(){
    	ViaVanoDao dao = new ViaVanoDaoImpl();
    	return dao.getAllRows1();
    }   
    
    public List<ViaVano> getAllRows1(long viaId){
    	ViaVanoDao dao = new ViaVanoDaoImpl();
    	return dao.getAllRows1(viaId);
    } 
    
    public List<ViaVano> getAllRows1NoAsignado(long programaApId,long medicionApId){
    	ViaVanoDao dao = new ViaVanoDaoImpl();
    	return dao.getAllRows1NoAsignado(programaApId,medicionApId);
    } 
}

