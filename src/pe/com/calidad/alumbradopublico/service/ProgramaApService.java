package pe.com.calidad.alumbradopublico.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;





import pe.com.calidad.alumbradopublico.dao.ProgramaApDao;
import pe.com.calidad.alumbradopublico.dao.ProgramaApDaoImpl;
import pe.com.calidad.alumbradopublico.entity.MedicionAp;
import pe.com.indra.calidad.entity.Localidad;
import pe.com.calidad.alumbradopublico.entity.ProgramaAp;
import pe.com.indra.calidad.entity.TipoMedicion;
import pe.com.indra.calidad.entity.TipoPunto;
import pe.com.calidad.alumbradopublico.entity.Via;
import pe.com.indra.calidad.entity.PeriodoMedicion;

/**
 *
 * @author Luis
 */
public class ProgramaApService {

    public void create(ProgramaAp programaAp) {
    	ProgramaApDao dao = new ProgramaApDaoImpl();
        
        try {
        	dao.create(programaAp);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear ProgramaAp.", e);
		}
        
    }

    public ProgramaAp ReadById(long programaApId) {
    	ProgramaApDao dao = new ProgramaApDaoImpl();
    	ProgramaAp programaAp = null;
        
        try {
        	programaAp = dao.ReadById(programaApId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Medici�n."
		}
        
        return programaAp;
    }

    public void update(ProgramaAp programaAp) {
    	ProgramaApDao dao = new ProgramaApDaoImpl();
        
        try {
        	dao.update(programaAp);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public void delete(long programaApId) {
    	ProgramaApDao dao = new ProgramaApDaoImpl();
        
        try {
        	dao.delete(programaApId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }
    
    
    public Via selectVia(long programaApId) {
    	ProgramaApDao dao = new ProgramaApDaoImpl();
        return dao.selectVia(programaApId);
    }

    public PeriodoMedicion selectPeriodoMedicion(long programaApId) {
    	ProgramaApDao dao = new ProgramaApDaoImpl();
        return dao.selectPeriodoMedicion(programaApId);
    }
    
    public Localidad selectLocalidad(long programaApId) {
    	ProgramaApDao dao = new ProgramaApDaoImpl();
        return dao.selectLocalidad(programaApId);
    }

    public TipoPunto selectTipoPunto(long programaApId) {
    	ProgramaApDao dao = new ProgramaApDaoImpl();
        return dao.selectTipoPunto(programaApId);
    }

    public TipoMedicion selectTipoMedicion(long programaApId) {
    	ProgramaApDao dao = new ProgramaApDaoImpl();
        return dao.selectTipoMedicion(programaApId);
    }
    
    public Set<MedicionAp> selectMedicionAp(long interrupcionId) {
    	ProgramaApDao dao = new ProgramaApDaoImpl();
        return dao.selectMedicionAp(interrupcionId);
    }
    
    public List<ProgramaAp> getAllRows(){
    	ProgramaApDao dao = new ProgramaApDaoImpl();
    	return dao.getAllRows();
    }  
    
    public List<ProgramaAp> getAllRows1(){
    	ProgramaApDao dao = new ProgramaApDaoImpl();
    	return dao.getAllRows1();
    }   
    
    public List<ProgramaAp> getAllRows1(long perMedicionId){
    	ProgramaApDao dao = new ProgramaApDaoImpl();
    	return dao.getAllRows1(perMedicionId);
    } 
}
