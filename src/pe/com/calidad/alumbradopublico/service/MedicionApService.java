package pe.com.calidad.alumbradopublico.service;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;



import pe.com.calidad.alumbradopublico.dao.MedicionApDao;
import pe.com.calidad.alumbradopublico.dao.MedicionApDaoImpl;
import pe.com.calidad.alumbradopublico.entity.ViaVano;
import pe.com.calidad.alumbradopublico.entity.MedicionAp;
import pe.com.calidad.alumbradopublico.entity.ProgramaAp;


/**
 *
 * @author Luis
 */
public class MedicionApService {
    public void create(MedicionAp medicionAp) {
    	MedicionApDao dao = new MedicionApDaoImpl();
        
        try {
        	dao.create(medicionAp);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear MedicionAp.", e);
		}
        
    }

	public MedicionAp ReadById(long medicionApId) {
    	MedicionApDao dao = new MedicionApDaoImpl();
    	MedicionAp medicionAp = null;
        
        try {
        	medicionAp = dao.ReadById(medicionApId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Medici�n."
		}
        
        return medicionAp;
    }

    public void update(MedicionAp medicionAp) {
    	MedicionApDao dao = new MedicionApDaoImpl();
        
        try {
        	dao.update(medicionAp);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public void delete(long medicionApId) {
    	MedicionApDao dao = new MedicionApDaoImpl();
        
        try {
        	dao.delete(medicionApId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }
     
       
    public ViaVano selectViaVano(long medicionApId) {
    	MedicionApDao dao = new MedicionApDaoImpl();
        return dao.selectViaVano(medicionApId);
    }


    public ProgramaAp selectProgramaAp(long medicionApId) {
    	MedicionApDao dao = new MedicionApDaoImpl();
        return dao.selectProgramaAp(medicionApId);
    }
    
    
    public List<MedicionAp> getAllRows(){
    	MedicionApDao dao = new MedicionApDaoImpl();
    	return dao.getAllRows();
    }  
    
    public List<MedicionAp> getAllRows1(){
    	MedicionApDao dao = new MedicionApDaoImpl();
    	return dao.getAllRows1();
    }   
    
    public List<MedicionAp> getAllRows1(long programaApId){
    	MedicionApDao dao = new MedicionApDaoImpl();
    	return dao.getAllRows1(programaApId);
    } 
    
    public MedicionAp ReadByNroVano(long programaApId, BigDecimal nroVano){
    	MedicionApDao dao = new MedicionApDaoImpl();
    	return dao.ReadByNroVano(programaApId, nroVano);
    }    
    
}
