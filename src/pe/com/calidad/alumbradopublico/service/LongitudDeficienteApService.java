package pe.com.calidad.alumbradopublico.service;

import java.util.List;

import org.hibernate.HibernateException;

import pe.com.calidad.alumbradopublico.dao.LongitudDeficienteApDao;//CompensacionApDao;
import pe.com.calidad.alumbradopublico.dao.LongitudDeficienteApDaoImpl;//CompensacionApDaoImpl;
import pe.com.calidad.alumbradopublico.entity.LongitudDeficienteAp;//CompensacionAp;


/**
 *
 * @author Luis
 */
public class LongitudDeficienteApService {

    public void create(LongitudDeficienteAp longitudDeficienteAp) {
    	LongitudDeficienteApDao dao = new LongitudDeficienteApDaoImpl();

        try {
        	dao.create(longitudDeficienteAp);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear LongitudDeficienteAp.", e);
		}
    }

    public LongitudDeficienteAp ReadById(long lonDeficienteApId) {
    	LongitudDeficienteApDao dao = new LongitudDeficienteApDaoImpl();
    	LongitudDeficienteAp longitudDeficienteAp = null;
     
        try {
        	longitudDeficienteAp = dao.ReadById(lonDeficienteApId);
		}  catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); 
		}
        return longitudDeficienteAp;
    }

    public void update(LongitudDeficienteAp longitudDeficienteAp) {
    	LongitudDeficienteApDao dao = new LongitudDeficienteApDaoImpl();

        try {
        	dao.update(longitudDeficienteAp);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long lonDeficienteApId) {
    	LongitudDeficienteApDao dao = new LongitudDeficienteApDaoImpl();
        
        try {
        	dao.delete(lonDeficienteApId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }
    
    public List<LongitudDeficienteAp> getAllRows() {
    	LongitudDeficienteApDao dao = new LongitudDeficienteApDaoImpl();
		return dao.getAllRows();
	}
    
    public List<LongitudDeficienteAp> getAllRows1() {
    	LongitudDeficienteApDao dao = new LongitudDeficienteApDaoImpl();
		return dao.getAllRows1();
	}
	
 	
}
