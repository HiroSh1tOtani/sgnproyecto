package pe.com.calidad.alumbradopublico.service;

import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;

import pe.com.calidad.alumbradopublico.dao.ViaDao;
import pe.com.calidad.alumbradopublico.dao.ViaDaoImpl;
import pe.com.calidad.alumbradopublico.entity.ProgramaAp;
import pe.com.calidad.alumbradopublico.entity.ViaVano;
import pe.com.indra.calidad.entity.Localidad;
import pe.com.calidad.alumbradopublico.entity.Via;


/**
 *
 * @author Luis
 */
public class ViaService {

    public void create(Via via) {
    	ViaDao dao = new ViaDaoImpl();
        
        try {
        	dao.create(via);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Via.", e);
		}
        
    }

    public Via ReadById(long viaId) {
    	ViaDao dao = new ViaDaoImpl();
    	Via via = null;
        
        try {
        	via = dao.ReadById(viaId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); 
		}
        
        return via;
    }

    public void update(Via via) {
    	ViaDao dao = new ViaDaoImpl();
        
        try {
        	dao.update(via);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public void delete(long viaId) {
    	ViaDao dao = new ViaDaoImpl();
        
        try {
        	dao.delete(viaId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }
    
   
    public Localidad selectLocalidad(long viaId) {
    	ViaDao dao = new ViaDaoImpl();
        return dao.selectLocalidad(viaId);
    }

    
    public Set<ProgramaAp> selectProgramaAp(long viaId) {
    	ViaDao dao = new ViaDaoImpl();
        return dao.selectProgramaAp(viaId);
    }
    
    public Set<ViaVano> selectViaVanos(long viaId) {
    	ViaDao dao = new ViaDaoImpl();
        return dao.selectViaVanos(viaId);
    }
    
    
    public List<Via> getAllRows(){
    	ViaDao dao = new ViaDaoImpl();
    	return dao.getAllRows();
    }  
    
    public List<Via> getAllRows1(){
    	ViaDao dao = new ViaDaoImpl();
    	return dao.getAllRows1();
    }   
    
    public List<Via> getAllRows1(long localidadId){
    	ViaDao dao = new ViaDaoImpl();
    	return dao.getAllRows1(localidadId);
    } 
}
