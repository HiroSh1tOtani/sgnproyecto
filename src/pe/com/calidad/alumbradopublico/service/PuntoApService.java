package pe.com.calidad.alumbradopublico.service;


import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;

import pe.com.calidad.alumbradopublico.dao.PuntoApDao;//ViaVanoDao;
import pe.com.calidad.alumbradopublico.dao.PuntoApDaoImpl;//ViaVanoDaoImpl;
import pe.com.calidad.alumbradopublico.entity.MedicionAp;//Via;
import pe.com.calidad.alumbradopublico.entity.PuntoAp;//ViaVano;


/**
 *
 * @author Luis
 */
public class PuntoApService {

    public void create(PuntoAp puntoAp) {
    	PuntoApDao dao = new PuntoApDaoImpl();
        
        try {
        	dao.create(puntoAp);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear puntoAp.", e);
		}
        
    }

    public PuntoAp ReadById(long puntoApId) {
    	PuntoApDao dao = new PuntoApDaoImpl();
    	PuntoAp puntoAp = null;
        
        try {
        	puntoAp = dao.ReadById(puntoApId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); 
		}
        
        return puntoAp;
    }

    public void update(PuntoAp puntoAp) {
    	PuntoApDao dao = new PuntoApDaoImpl();
        
        try {
        	dao.update(puntoAp);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public void delete(long puntoApId) {
    	PuntoApDao dao = new PuntoApDaoImpl();
        
        try {
        	dao.delete(puntoApId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }
    
   
    public MedicionAp selectVia(long puntoApId) {
    	PuntoApDao dao = new PuntoApDaoImpl();
        return dao.selectVia(puntoApId);
    }

      
    public List<PuntoAp> getAllRows(){
    	PuntoApDao dao = new PuntoApDaoImpl();
    	return dao.getAllRows();
    }  
    
    public List<PuntoAp> getAllRows1(){
    	PuntoApDao dao = new PuntoApDaoImpl();
    	return dao.getAllRows1();
    }   
    
    public List<PuntoAp> getAllRows1(long medicionApId){
    	PuntoApDao dao = new PuntoApDaoImpl();
    	return dao.getAllRows1(medicionApId);
    } 
}
