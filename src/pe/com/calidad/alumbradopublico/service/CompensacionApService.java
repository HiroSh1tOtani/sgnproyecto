package pe.com.calidad.alumbradopublico.service;

import java.util.List;

import org.hibernate.HibernateException;

import pe.com.calidad.alumbradopublico.dao.CompensacionApDao;
import pe.com.calidad.alumbradopublico.dao.CompensacionApDaoImpl;
import pe.com.calidad.alumbradopublico.entity.CompensacionAp;


/**
 *
 * @author Luis
 */
public class CompensacionApService {

    public void create(CompensacionAp compensacionAp) {
    	CompensacionApDao dao = new CompensacionApDaoImpl();

        try {
        	dao.create(compensacionAp);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear CompensacionAp.", e);
		}
    }

    public CompensacionAp ReadById(long compensacioApId) {
    	CompensacionApDao dao = new CompensacionApDaoImpl();
    	CompensacionAp compensacionAp = null;
     
        try {
        	compensacionAp = dao.ReadById(compensacioApId);
		}  catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); 
		}
        return compensacionAp;
    }

    public void update(CompensacionAp compensacionAp) {
    	CompensacionApDao dao = new CompensacionApDaoImpl();

        try {
        	dao.update(compensacionAp);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long compensacioApId) {
    	CompensacionApDao dao = new CompensacionApDaoImpl();
        
        try {
        	dao.delete(compensacioApId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }
    
    public List<CompensacionAp> getAllRows() {
    	CompensacionApDao dao = new CompensacionApDaoImpl();
		return dao.getAllRows();
	}
    
    public List<CompensacionAp> getAllRows1() {
    	CompensacionApDao dao = new CompensacionApDaoImpl();
		return dao.getAllRows1();
	}
	
 	public CompensacionAp getAllRows1(String numSumCliente,String semestre) {
 		CompensacionApDao dao = new CompensacionApDaoImpl();
		return dao.getAllRows1(numSumCliente,semestre);
	}
    
}
