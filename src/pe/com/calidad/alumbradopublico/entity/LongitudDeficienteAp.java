
package pe.com.calidad.alumbradopublico.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;


/**
* 
* @author Luis
*/

@Entity
@Table(name="CAL_LONGITUD_DEFICIENTE_AP")
@SequenceGenerator (name = "lonDeficienteApIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "LongitudDeficienteApSeq")
public class LongitudDeficienteAp implements Serializable{
	
	private static final long serialVersionUID = -6441881639255168218L;
    private long lonDeficienteApId;
    private String sisElectrico;
    private String ano;
    private String semestre;
    private BigDecimal lonMedida;
    private BigDecimal lonDeficiente;
    private BigDecimal porDeficiente;
    private String compensa;
 
  	public LongitudDeficienteAp() {
    }


  	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "lonDeficienteApIdGenerator")
    @Column(name = "LON_DEFICIENTE_AP_ID",nullable=false,unique=true)
	public long getLonDeficienteApId() {
		return lonDeficienteApId;
	}

	public void setLonDeficienteApId(long lonDeficienteApId) {
		this.lonDeficienteApId = lonDeficienteApId;
	}

	@Column(name = "SIS_ELECTRICO",nullable=true)
	public String getSisElectrico() {
		return sisElectrico;
	}

	public void setSisElectrico(String sisElectrico) {
		this.sisElectrico = sisElectrico;
	}

	@Column(name = "ANO",nullable=true)
	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	@Column(name = "SEMESTRE",nullable=true)
	public String getSemestre() {
		return semestre;
	}

	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}

	@Column(name = "LON_MEDIDA",nullable=true)
	public BigDecimal getLonMedida() {
		return lonMedida;
	}

	public void setLonMedida(BigDecimal lonMedida) {
		this.lonMedida = lonMedida;
	}

	@Column(name = "LON_DEFICIENTE",nullable=true)
	public BigDecimal getLonDeficiente() {
		return lonDeficiente;
	}

	public void setLonDeficiente(BigDecimal lonDeficiente) {
		this.lonDeficiente = lonDeficiente;
	}

	@Column(name = "POR_DEFICIENTE",nullable=true)
	public BigDecimal getPorDeficiente() {
		return porDeficiente;
	}

	public void setPorDeficiente(BigDecimal porDeficiente) {
		this.porDeficiente = porDeficiente;
	}

	@Column(name = "COMPENSA",nullable=true)
	public String getCompensa() {
		return compensa;
	}

	public void setCompensa(String compensa) {
		this.compensa = compensa;
	}
	
	public String toString(){
		return new Long(this.getLonDeficienteApId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getLonDeficienteApId()==((LongitudDeficienteAp)obj).getLonDeficienteApId()){
			return true;
		}else{
			return false;
		}
	}

  	
 
	 	
}
