
package pe.com.calidad.alumbradopublico.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.*;

/**
 *
 * @author Lucho
 */
@Entity
@Table(name="CAL_VIA_VANO")
@SequenceGenerator(name = "ViaVanoIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "ViaVanoSeq")
public class ViaVano implements Serializable {
    
	private static final long serialVersionUID = -6441881639255168218L;
	
	private long viaVanoId;
	private BigDecimal nroVano;
	private String tipCalzada;
	private BigDecimal longitud;
	private Via via;
	private String estado;
	private String ptoIni;
	private String ptoFin;
	private String tipAp;
	private BigDecimal canales;

	
	private Set<MedicionAp> medicionAp;  

	public ViaVano() {
    }
     
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ViaVanoIdGenerator")
    @Column(name = "VIA_VANO_ID",nullable=false,unique=true) 
    public long getViaVanoId() {
		return viaVanoId;
	}

	public void setViaVanoId(long viaVanoId) {
		this.viaVanoId = viaVanoId;
	}

    @Column(name="NRO_VANO",nullable=true)
    public BigDecimal getNroVano() {
		return nroVano;
	}

	public void setNroVano(BigDecimal nroVano) {
		this.nroVano = nroVano;
	}

	@Column(name="TIP_CALZADA",nullable=true)
	public String getTipCalzada() {
		return tipCalzada;
	}

	public void setTipCalzada(String tipCalzada) {
		this.tipCalzada = tipCalzada;
	}

	@Column(name="LONGITUD",nullable=true)
	public BigDecimal getLongitud() {
		return longitud;
	}

	public void setLongitud(BigDecimal longitud) {
		this.longitud = longitud;
	}

	@ManyToOne
    @JoinColumn(name="PK_VIA_ID", nullable=true)
	public Via getVia() {
		return via;
	}

	public void setVia(Via via) {
		this.via = via;
	}

	@Column(name="ESTADO",nullable=true)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	
	@Column(name="PTO_INI",nullable=true)
	public String getPtoIni() {
		return ptoIni;
	}

	public void setPtoIni(String ptoIni) {
		this.ptoIni = ptoIni;
	}

	@Column(name="PTO_FIN",nullable=true)
	public String getPtoFin() {
		return ptoFin;
	}

	public void setPtoFin(String ptoFin) {
		this.ptoFin = ptoFin;
	}

	@Column(name="TIP_AP",nullable=true)
	public String getTipAp() {
		return tipAp;
	}

	public void setTipAp(String tipAp) {
		this.tipAp = tipAp;
	}

	@Column(name="CANALES",nullable=true)
	public BigDecimal getCanales() {
		return canales;
	}

	public void setCanales(BigDecimal canales) {
		this.canales = canales;
	}

	@OneToMany(cascade ={CascadeType.ALL}, mappedBy = "viaVano")
	public Set<MedicionAp> getMedicionAp() {
		return medicionAp;
	}

	public void setMedicionAp(Set<MedicionAp> medicionAp) {
		this.medicionAp = medicionAp;
	}
	
	
	public String toString(){
		return new Long(this.getViaVanoId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getViaVanoId()==((ViaVano)obj).getViaVanoId()){
			return true;
		}else{
			return false;
		}
	}
}
