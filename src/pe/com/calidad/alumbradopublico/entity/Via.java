
package pe.com.calidad.alumbradopublico.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;
import javax.persistence.*;


import pe.com.indra.calidad.entity.Localidad;

/**
 *
 * @author Lucho
 */
@Entity
@Table(name="CAL_VIA")
@SequenceGenerator(name = "ViaIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "ViaSeq")
public class Via implements Serializable {
    
	private static final long serialVersionUID = -6441881639255168218L;

	private long viaId;
	private String codVia;
	private BigDecimal canales;
	private String denominacion;
	private String nombre;
	private BigDecimal longitud;
	private BigDecimal nroPuntos;
	private String claZona;
	private String tipVia;
	private String tipAp;
	private BigDecimal secMedAp;
	private Localidad localidad;
	private String estado;
	private Set<ProgramaAp> programaAp;
	private Set<ViaVano> viaVano;
	private String ubigeo;

	public Via() {
    }
     
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ViaIdGenerator")
    @Column(name = "VIA_ID",nullable=false,unique=true) 
    public long getViaId() {
		return viaId;
	}

	public void setViaId(long viaId) {
		this.viaId = viaId;
	}
	
	@Column(name="COD_VIA",nullable=true)
	public String getCodVia() {
		return codVia;
	}

	public void setCodVia(String codVia) {
		this.codVia = codVia;
	}

	@Column(name="CANALES",nullable=true)
	public BigDecimal getCanales() {
		return canales;
	}

	public void setCanales(BigDecimal canales) {
		this.canales = canales;
	}

	@Column(name="DENOMINACION",nullable=true)
	public String getDenominacion() {
		return denominacion;
	}

	public void setDenominacion(String denominacion) {
		this.denominacion = denominacion;
	}

	@Column(name="NOMBRE",nullable=true)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name="LONGITUD",nullable=true)
	public BigDecimal getLongitud() {
		return longitud;
	}

	public void setLongitud(BigDecimal longitud) {
		this.longitud = longitud;
	}

	@Column(name="NRO_PUNTOS",nullable=true)
	public BigDecimal getNroPuntos() {
		return nroPuntos;
	}

	public void setNroPuntos(BigDecimal nroPuntos) {
		this.nroPuntos = nroPuntos;
	}

	@Column(name="CLA_ZONA",nullable=true)
	public String getClaZona() {
		return claZona;
	}

	public void setClaZona(String claZona) {
		this.claZona = claZona;
	}

	@Column(name="TIP_VIA",nullable=true)
	public String getTipVia() {
		return tipVia;
	}

	public void setTipVia(String tipVia) {
		this.tipVia = tipVia;
	}

	@Column(name="TIP_AP",nullable=true)
	public String getTipAp() {
		return tipAp;
	}

	public void setTipAp(String tipAp) {
		this.tipAp = tipAp;
	}

	@Column(name="SEC_MED_AP",nullable=true)
	public BigDecimal getSecMedAp() {
		return secMedAp;
	}

	public void setSecMedAp(BigDecimal secMedAp) {
		this.secMedAp = secMedAp;
	}

	@ManyToOne
    @JoinColumn(name="PK_LOCALIDAD_ID", nullable=true)
	public Localidad getLocalidad() {
		return localidad;
	}

	public void setLocalidad(Localidad localidad) {
		this.localidad = localidad;
	}

	@Column(name="ESTADO",nullable=true)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE},mappedBy = "via")
	public Set<ProgramaAp> getProgramaAp() {
		return programaAp;
	}

	public void setProgramaAp(Set<ProgramaAp> programaAp) {
		this.programaAp = programaAp;
	}

	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE},mappedBy = "via")
	public Set<ViaVano> getViaVano() {
		return viaVano;
	}

	public void setViaVano(Set<ViaVano> viaVano) {
		this.viaVano = viaVano;
	}

	@Column(name="UBIGEO",nullable=true)
	public String getUbigeo() {
		return ubigeo;
	}

	public void setUbigeo(String ubigeo) {
		this.ubigeo = ubigeo;
	}
  	
	public String toString(){
		return new Long(this.getViaId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getViaId()==((Via)obj).getViaId()){
			return true;
		}else{
			return false;
		}
	}
	
}
