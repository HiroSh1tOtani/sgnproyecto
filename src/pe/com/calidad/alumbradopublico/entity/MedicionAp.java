
package pe.com.calidad.alumbradopublico.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.*;

import pe.com.calidad.suministro.entity.SuministroAfectado;

/**
 *
 * @author Lucho
 */
@Entity
@Table(name="CAL_MEDICION_AP")
@SequenceGenerator(name = "MedicionApIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "MedicionApSeq")
public class MedicionAp implements Serializable {
    
	private static final long serialVersionUID = -6441881639255168218L;
	private long medicionApId;
	private String codPosInicial;
    private String codPosFinal;
    private String ubigeoVano;
    private String tipAlumbrado;
    private String tipCalzada;
    private BigDecimal lonMedida;
    private BigDecimal iluMedCalzada;
    private BigDecimal uniMedIluminancia;
    private BigDecimal indComDeslumbramiento;
    private BigDecimal iluMedVereda;
    private BigDecimal lumMedRevSeco; 
    private BigDecimal uniLongitudinal;
    private BigDecimal uniMedia;
    private Timestamp fecMedicion;
    private String calidad;
    private ViaVano viaVano;
    private ProgramaAp programaAp;
    private String estado;

    private BigDecimal altLumInicial;
    private BigDecimal potLamInicial;
    private String tipLumInicial;
    private String tipPasInicial;
    private BigDecimal altLumFinal;
    private BigDecimal potLamFinal;
    private String tipLumFinal;
    private String tipPasFinal;
    private BigDecimal numLumPorKm;
    private BigDecimal supAparente;
    private BigDecimal intLuminosa;
    private BigDecimal relLuminosa;
    
    
    private Set<PuntoAp> puntosAp;

    

    private Set<SuministroAfectadoAP> SuministrosAfectadosAP;

	public MedicionAp (String codPosInicial, String codPosFinal, String ubigeoVano, String tipAlumbrado, String tipCalzada, BigDecimal lonMedida, BigDecimal iluMedCalzada,
    		BigDecimal uniMedIluminancia,BigDecimal indComDeslumbramiento, BigDecimal iluMedVereda, BigDecimal lumMedRevSeco, BigDecimal uniLongitudinal, BigDecimal uniMedia,
    		Timestamp fecMedicion, String calidad, ProgramaAp programaAp, ViaVano viaVano, String estado) {
    

     this.codPosInicial = codPosInicial;   
     this.codPosFinal = codPosFinal;
     this.ubigeoVano = ubigeoVano;
     this.tipAlumbrado = tipAlumbrado;
     this.tipCalzada = tipCalzada;
     this.lonMedida = lonMedida;
     this.iluMedCalzada = iluMedCalzada;
     this.uniMedIluminancia = uniMedIluminancia;
     this.indComDeslumbramiento = indComDeslumbramiento; 
     this.iluMedVereda = iluMedVereda;
     this.lumMedRevSeco = lumMedRevSeco;
     this.uniLongitudinal = uniLongitudinal;
     this.uniMedia = uniMedia;
     this.fecMedicion = fecMedicion;
     this.calidad = calidad;
     this.viaVano = viaVano;
     this.programaAp = programaAp;
     
     this.estado = estado;
     
     
              
    }

	public MedicionAp() {
    }
     
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MedicionApIdGenerator")
    @Column(name = "MEDICION_AP_ID",nullable=false,unique=true) 
    public long getMedicionApId() {
		return medicionApId;
	}

	public void setMedicionApId(long medicionApId) {
		this.medicionApId = medicionApId;
	}


    @Column(name="COD_POS_INICIAL",nullable=true)
    public String getCodPosInicial() {
		return codPosInicial;
	}

	public void setCodPosInicial(String codPosInicial) {
		this.codPosInicial = codPosInicial;
	}

	@Column(name="COD_POS_FINAL",nullable=true)
	public String getCodPosFinal() {
		return codPosFinal;
	}

	public void setCodPosFinal(String codPosFinal) {
		this.codPosFinal = codPosFinal;
	}

	@Column(name="UBIGEO_VANO",nullable=true)
	public String getUbigeoVano() {
		return ubigeoVano;
	}

	public void setUbigeoVano(String ubigeoVano) {
		this.ubigeoVano = ubigeoVano;
	}

	@Column(name="TIP_ALUMBRADO",nullable=true)
	public String getTipAlumbrado() {
		return tipAlumbrado;
	}

	public void setTipAlumbrado(String tipAlumbrado) {
		this.tipAlumbrado = tipAlumbrado;
	}

	@Column(name="TIP_CALZADA",nullable=true)
	public String getTipCalzada() {
		return tipCalzada;
	}

	public void setTipCalzada(String tipCalzada) {
		this.tipCalzada = tipCalzada;
	}

	@Column(name="LON_MEDIDA",nullable=true)
	public BigDecimal getLonMedida() {
		return lonMedida;
	}

	public void setLonMedida(BigDecimal lonMedida) {
		this.lonMedida = lonMedida;
	}

	@Column(name="ILU_MED_CALZADA",nullable=true)
	public BigDecimal getIluMedCalzada() {
		return iluMedCalzada;
	}

	public void setIluMedCalzada(BigDecimal iluMedCalzada) {
		this.iluMedCalzada = iluMedCalzada;
	}

	@Column(name="UNI_MED_ILUMINANCIA",nullable=true)
	public BigDecimal getUniMedIluminancia() {
		return uniMedIluminancia;
	}

	public void setUniMedIluminancia(BigDecimal uniMedIluminancia) {
		this.uniMedIluminancia = uniMedIluminancia;
	}

	@Column(name="IND_CON_DESLUMBRAMIENTO",nullable=true)
	public BigDecimal getIndComDeslumbramiento() {
		return indComDeslumbramiento;
	}

	public void setIndComDeslumbramiento(BigDecimal indComDeslumbramiento) {
		this.indComDeslumbramiento = indComDeslumbramiento;
	}

	@Column(name="ILU_MED_VEREDA",nullable=true)
	public BigDecimal getIluMedVereda() {
		return iluMedVereda;
	}

	public void setIluMedVereda(BigDecimal iluMedVereda) {
		this.iluMedVereda = iluMedVereda;
	}

	@Column(name="LUM_MED_REV_SECO",nullable=true)
	public BigDecimal getLumMedRevSeco() {
		return lumMedRevSeco;
	}

	public void setLumMedRevSeco(BigDecimal lumMedRevSeco) {
		this.lumMedRevSeco = lumMedRevSeco;
	}

	@Column(name="UNI_LONGITUDINAL",nullable=true)
	public BigDecimal getUniLongitudinal() {
		return uniLongitudinal;
	}

	public void setUniLongitudinal(BigDecimal uniLongitudinal) {
		this.uniLongitudinal = uniLongitudinal;
	}

	@Column(name="UNI_MEDIA",nullable=true)
	public BigDecimal getUniMedia() {
		return uniMedia;
	}

	public void setUniMedia(BigDecimal uniMedia) {
		this.uniMedia = uniMedia;
	}

	@Column(name="FEC_MEDICION",nullable=true)
	public Timestamp getFecMedicion() {
		return fecMedicion;
	}

	public void setFecMedicion(Timestamp fecMedicion) {
		this.fecMedicion = fecMedicion;
	}

	@Column(name="CALIDAD",nullable=true)
	public String getCalidad() {
		return calidad;
	}

	public void setCalidad(String calidad) {
		this.calidad = calidad;
	}

	@Column(name="ESTADO",nullable=true)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@ManyToOne
    @JoinColumn(name="PK_VIA_VANO_ID", nullable=true)
	public ViaVano getViaVano() {
		return viaVano;
	}

	public void setViaVano(ViaVano viaVano) {
		this.viaVano = viaVano;
	}

    @ManyToOne
    @JoinColumn(name="PK_PROGRAMA_AP_ID", nullable=true)
    public ProgramaAp getProgramaAp() {
 		return programaAp;
 	}

 	public void setProgramaAp(ProgramaAp programaAp) {
 		this.programaAp = programaAp;
 	}
	
	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "medicionAp")
	public Set<SuministroAfectadoAP> getSuministrosAfectadosAP() {
		return SuministrosAfectadosAP;
	}

	public void setSuministrosAfectadosAP(Set<SuministroAfectadoAP> suministrosAfectadosAP) {
		SuministrosAfectadosAP = suministrosAfectadosAP;
	} 	

	@Column(name="ALT_LUM_INICIAL",nullable=true)
	public BigDecimal getAltLumInicial() {
		return altLumInicial;
	}

	public void setAltLumInicial(BigDecimal altLumInicial) {
		this.altLumInicial = altLumInicial;
	}

	@Column(name="POT_LAM_INICIAL",nullable=true)
	public BigDecimal getPotLamInicial() {
		return potLamInicial;
	}

	public void setPotLamInicial(BigDecimal potLamInicial) {
		this.potLamInicial = potLamInicial;
	}

	@Column(name="TIP_LUM_INICIAL",nullable=true)
	public String getTipLumInicial() {
		return tipLumInicial;
	}

	public void setTipLumInicial(String tipLumInicial) {
		this.tipLumInicial = tipLumInicial;
	}

	@Column(name="TIP_PAS_INICIAL",nullable=true)
	public String getTipPasInicial() {
		return tipPasInicial;
	}

	public void setTipPasInicial(String tipPasInicial) {
		this.tipPasInicial = tipPasInicial;
	}

	@Column(name="ALT_LUM_FINAL",nullable=true)
	public BigDecimal getAltLumFinal() {
		return altLumFinal;
	}

	public void setAltLumFinal(BigDecimal altLumFinal) {
		this.altLumFinal = altLumFinal;
	}

	@Column(name="POT_LAM_FINAL",nullable=true)
	public BigDecimal getPotLamFinal() {
		return potLamFinal;
	}

	public void setPotLamFinal(BigDecimal potLamFinal) {
		this.potLamFinal = potLamFinal;
	}

	@Column(name="TIP_LUM_FINAL",nullable=true)
	public String getTipLumFinal() {
		return tipLumFinal;
	}

	public void setTipLumFinal(String tipLumFinal) {
		this.tipLumFinal = tipLumFinal;
	}

	@Column(name="TIP_PAS_FINAL",nullable=true)
	public String getTipPasFinal() {
		return tipPasFinal;
	}

	public void setTipPasFinal(String tipPasFinal) {
		this.tipPasFinal = tipPasFinal;
	}

	@Column(name="NUM_LUM_POR_KM",nullable=true)
	public BigDecimal getNumLumPorKm() {
		return numLumPorKm;
	}

	public void setNumLumPorKm(BigDecimal numLumPorKm) {
		this.numLumPorKm = numLumPorKm;
	}

	@Column(name="SUP_APARENTE",nullable=true)
	public BigDecimal getSupAparente() {
		return supAparente;
	}

	public void setSupAparente(BigDecimal supAparente) {
		this.supAparente = supAparente;
	}

	@Column(name="INT_LUMINOSA",nullable=true)
	public BigDecimal getIntLuminosa() {
		return intLuminosa;
	}

	public void setIntLuminosa(BigDecimal intLuminosa) {
		this.intLuminosa = intLuminosa;
	}

	@Column(name="REL_LUMINOSA",nullable=true)
	public BigDecimal getRelLuminosa() {
		return relLuminosa;
	}

	public void setRelLuminosa(BigDecimal relLuminosa) {
		this.relLuminosa = relLuminosa;
	}

	
	public String toString(){
		return new Long(this.getMedicionApId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getMedicionApId()==((MedicionAp)obj).getMedicionApId()){
			return true;
		}else{
			return false;
		}
	}


	@OneToMany(cascade = CascadeType.ALL,mappedBy = "medicionAp")
	public Set<PuntoAp> getPuntosAp() {
		return puntosAp;
	}

	public void setPuntosAp(Set<PuntoAp> puntosAp) {
		this.puntosAp = puntosAp;
	}
	
}
