
package pe.com.calidad.alumbradopublico.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;


/**
* 
*@author Luis
*
* 
*/

@Entity
@Table(name="CAL_COMPENSACION_AP")
@SequenceGenerator (name = "CompensacionApIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "CompensacionApSeq")
public class CompensacionAp implements Serializable{
	
	private static final long serialVersionUID = -6441881639255168218L;
    private long compensacioApId;
    private String sisElectrico;
    private String ano;
    private String semestre;
    private String numSumCliente;
    private String tipTension;
    private String tipLocalidad;
    private BigDecimal monPagCliAp;
    private BigDecimal eneProAp;
    private BigDecimal monComCliente;


	public CompensacionAp() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CompensacionApIdGenerator")
    @Column(name = "COMPENSACION_AP_ID",nullable=false,unique=true)
  	public long getCompensacioApId() {
		return compensacioApId;
	}

	public void setCompensacioApId(long compensacioApId) {
		this.compensacioApId = compensacioApId;
	}

	@Column(name = "SIS_ELECTRICO",nullable=true)
	public String getSisElectrico() {
		return sisElectrico;
	}

	public void setSisElectrico(String sisElectrico) {
		this.sisElectrico = sisElectrico;
	}

	@Column(name = "ANO",nullable=true)
	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	@Column(name = "SEMESTRE",nullable=true)
	public String getSemestre() {
		return semestre;
	}

	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}

	@Column(name = "NUM_SUM_CLIENTE",nullable=true)
	public String getNumSumCliente() {
		return numSumCliente;
	}

	public void setNumSumCliente(String numSumCliente) {
		this.numSumCliente = numSumCliente;
	}

	@Column(name = "TIP_TENSION",nullable=true)
	public String getTipTension() {
		return tipTension;
	}

	public void setTipTension(String tipTension) {
		this.tipTension = tipTension;
	}

	@Column(name = "TIP_LOCALIDAD",nullable=true)
	public String getTipLocalidad() {
		return tipLocalidad;
	}

	public void setTipLocalidad(String tipLocalidad) {
		this.tipLocalidad = tipLocalidad;
	}

	@Column(name = "MON_PAG_CLI_AP",nullable=true)
	public BigDecimal getMonPagCliAp() {
		return monPagCliAp;
	}

	public void setMonPagCliAp(BigDecimal monPagCliAp) {
		this.monPagCliAp = monPagCliAp;
	}

	@Column(name = "ENE_PRO_AP",nullable=true)
	public BigDecimal getEneProAp() {
		return eneProAp;
	}

	public void setEneProAp(BigDecimal eneProAp) {
		this.eneProAp = eneProAp;
	}

	@Column(name = "MON_COM_CLIENTE",nullable=true)
	public BigDecimal getMonComCliente() {
		return monComCliente;
	}

	public void setMonComCliente(BigDecimal monComCliente) {
		this.monComCliente = monComCliente;
	}
	
	public String toString(){
		return new Long(this.getCompensacioApId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getCompensacioApId()==((CompensacionAp)obj).getCompensacioApId()){
			return true;
		}else{
			return false;
		}
	}

		 	
}
