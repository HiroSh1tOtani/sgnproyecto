
package pe.com.calidad.alumbradopublico.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import pe.com.indra.calidad.entity.Localidad;
import pe.com.indra.calidad.entity.PeriodoMedicion;
import pe.com.indra.calidad.entity.TipoMedicion;
import pe.com.indra.calidad.entity.TipoPunto;

/**
 *
 * @author Lucho
 */
@Entity
@Table(name="CAL_PROGRAMA_AP")
@SequenceGenerator(name = "ProgramaApIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "ProgramaApSeq")
public class ProgramaAp implements Serializable {
    
	private static final long serialVersionUID = -6441881639255168218L;
	
	private long programaApId;
	private String numIdentificador;
	private String codViaReemplazada;
	private String numSumProximo;
	private BigDecimal numVanos;
	private BigDecimal lonAMedirse;
	private Timestamp fecProgramada;
	private String tipServicio;
	private PeriodoMedicion periodoMedicion;
	private Localidad localidad;
	private TipoMedicion tipoMedicion;
	private TipoPunto tipoPunto;
	private Via via;
	private String estado;
	private Set<MedicionAp> medicionAp;

	public ProgramaAp( long programaApId,String numIdentificador,String codViaReemplazada,String numSumProximo, BigDecimal numVanos, 
			BigDecimal lonAMedirse, Timestamp fecProgramada, String tipServicio, PeriodoMedicion periodoMedicion,Localidad localidad,TipoMedicion tipoMedicion,
			TipoPunto tipoPunto,Via via, String estado) {
        
        
		this.programaApId = programaApId;
		this.numIdentificador = numIdentificador;
		this.numSumProximo = numSumProximo;
		this.numVanos = numVanos;
		this.lonAMedirse = lonAMedirse;
		this.fecProgramada = fecProgramada;
		this.tipServicio = tipServicio;
		this.periodoMedicion = periodoMedicion;
        this.tipoMedicion = tipoMedicion;
        this.tipoPunto = tipoPunto;
        this.localidad = localidad;
        this.via = via;
        this.codViaReemplazada = codViaReemplazada;
        this.estado = estado;
        this.medicionAp = new HashSet<MedicionAp>();
        
    }

     public ProgramaAp() {
    }
     
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ProgramaApIdGenerator")
    @Column(name = "PROGRAMA_AP_ID",nullable=false,unique=true) 
    public long getProgramaApId() {
		return programaApId;
	}

	public void setProgramaApId(long programaApId) {
		this.programaApId = programaApId;
	}

    @Column(name="NUM_IDENTIFICADOR",nullable=true)
    public String getNumIdentificador() {
		return numIdentificador;
	}

	public void setNumIdentificador(String numIdentificador) {
		this.numIdentificador = numIdentificador;
	}
	
	@Column(name="COD_VIA_REEMPLAZADA",nullable=true)
	public String getCodViaReemplazada() {
		return codViaReemplazada;
	}

	public void setCodViaReemplazada(String codViaReemplazada) {
		this.codViaReemplazada = codViaReemplazada;
	}

	@Column(name="NUM_SUM_PROXIMO",nullable=true)
	public String getNumSumProximo() {
		return numSumProximo;
	}

	public void setNumSumProximo(String numSumProximo) {
		this.numSumProximo = numSumProximo;
	}

	@Column(name="NUM_VANOS",nullable=true)
	public BigDecimal getNumVanos() {
		return numVanos;
	}

	public void setNumVanos(BigDecimal numVanos) {
		this.numVanos = numVanos;
	}

	@Column(name="LON_A_MEDIRSE",nullable=true)
	public BigDecimal getLonAMedirse() {
		return lonAMedirse;
	}

	public void setLonAMedirse(BigDecimal lonAMedirse) {
		this.lonAMedirse = lonAMedirse;
	}

	@Column(name="FEC_PROGRAMADA",nullable=true)
	public Timestamp getFecProgramada() {

		return fecProgramada;
	}

	public void setFecProgramada(Timestamp fecProgramada) {
		this.fecProgramada = fecProgramada;
	}

	@Column(name="TIP_SERVICIO",nullable=true)
	public String getTipServicio() {
		return tipServicio;
	}

	public void setTipServicio(String tipServicio) {
		this.tipServicio = tipServicio;
	}


    @ManyToOne
    @JoinColumn(name="PK_PER_MEDICION_ID", nullable=false)
    public PeriodoMedicion getPeriodoMedicion() {
		return periodoMedicion;
	}

	public void setPeriodoMedicion(PeriodoMedicion periodoMedicion) {
		this.periodoMedicion = periodoMedicion;
	}

	@ManyToOne
    @JoinColumn(name="PK_LOCALIDAD_ID", nullable=false)
	public Localidad getLocalidad() {
		return localidad;
	}

	public void setLocalidad(Localidad localidad) {
		this.localidad = localidad;
	}

	@ManyToOne
    @JoinColumn(name="PK_TIP_MEDICION_ID", nullable=false)
	public TipoMedicion getTipoMedicion() {
		return tipoMedicion;
	}

	public void setTipoMedicion(TipoMedicion tipoMedicion) {
		this.tipoMedicion = tipoMedicion;
	}
	
	@ManyToOne
    @JoinColumn(name="PK_VIA_ID", nullable=false)
	public Via getVia() {
		return via;
	}

	public void setVia(Via via) {
		this.via = via;
	}

	@ManyToOne
    @JoinColumn(name="PK_TIP_PUNTO_ID", nullable=false)
	public TipoPunto getTipoPunto() {
		return tipoPunto;
	}

	public void setTipoPunto(TipoPunto tipoPunto) {
		this.tipoPunto = tipoPunto;
	}
    
	@Column(name="ESTADO",nullable=true)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
    
    @OneToMany(cascade ={CascadeType.ALL}, mappedBy = "programaAp")
    public Set<MedicionAp> getMedicionAp() {
		return medicionAp;
	}

	public void setMedicionAp(Set<MedicionAp> medicionAp) {
		this.medicionAp = medicionAp;
	}
	
	public String toString(){
		return new Long(this.getProgramaApId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getProgramaApId()==((ProgramaAp)obj).getProgramaApId()){
			return true;
		}else{
			return false;
		}
	}
	
}
