package pe.com.calidad.alumbradopublico.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CAL_PUNTO_AP")
@SequenceGenerator (name = "puntoApIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "PuntoAPSeq")
public class PuntoAp implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long puntoApId;
	private String m1;
	private String lugar;
	private BigDecimal i1;
	private BigDecimal i2;
	private BigDecimal i3;
	private BigDecimal i4;
	private BigDecimal i5;
	private String m2;
	private BigDecimal l1;
	private BigDecimal l2;
	private BigDecimal l3;
	private BigDecimal l4;
	private BigDecimal l5;
	private String estado;
	
	private MedicionAp medicionAp;
	
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "puntoApIdGenerator")
    @Column(name = "PUNTO_AP_ID",nullable=false,unique=true)	
    public long getPuntoApId() {
		return puntoApId;
	}

	public void setPuntoApId(long puntoApId) {
		this.puntoApId = puntoApId;
	}

	@Column(name = "M1",nullable=true)
	public String getM1() {
		return m1;
	}

	public void setM1(String m1) {
		this.m1 = m1;
	}

	@Column(name = "LUGAR",nullable=true)
	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	@Column(name = "I1",nullable=true)
	public BigDecimal getI1() {
		return i1;
	}

	public void setI1(BigDecimal i1) {
		this.i1 = i1;
	}

	@Column(name = "I2",nullable=true)
	public BigDecimal getI2() {
		return i2;
	}

	public void setI2(BigDecimal i2) {
		this.i2 = i2;
	}

	@Column(name = "I3",nullable=true)
	public BigDecimal getI3() {
		return i3;
	}

	public void setI3(BigDecimal i3) {
		this.i3 = i3;
	}

	@Column(name = "I4",nullable=true)
	public BigDecimal getI4() {
		return i4;
	}

	public void setI4(BigDecimal i4) {
		this.i4 = i4;
	}

	@Column(name = "I5",nullable=true)
	public BigDecimal getI5() {
		return i5;
	}

	public void setI5(BigDecimal i5) {
		this.i5 = i5;
	}
	
	@Column(name = "M2",nullable=true)
	public String getM2() {
		return m2;
	}

	public void setM2(String m2) {
		this.m2 = m2;
	}

	@Column(name = "L1",nullable=true)
	public BigDecimal getL1() {
		return l1;
	}

	public void setL1(BigDecimal l1) {
		this.l1 = l1;
	}

	@Column(name = "L2",nullable=true)
	public BigDecimal getL2() {
		return l2;
	}

	public void setL2(BigDecimal l2) {
		this.l2 = l2;
	}

	@Column(name = "L3",nullable=true)
	public BigDecimal getL3() {
		return l3;
	}

	public void setL3(BigDecimal l3) {
		this.l3 = l3;
	}

	@Column(name = "L4",nullable=true)
	public BigDecimal getL4() {
		return l4;
	}

	public void setL4(BigDecimal l4) {
		this.l4 = l4;
	}

	@Column(name = "L5",nullable=true)
	public BigDecimal getL5() {
		return l5;
	}

	public void setL5(BigDecimal l5) {
		this.l5 = l5;
	}

	@Column(name = "ESTADO",nullable=true)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@ManyToOne
    @JoinColumn(name="PK_MEDICION_AP_ID", nullable=true)
	public MedicionAp getMedicionAp() {
		return medicionAp;
	}

	public void setMedicionAp(MedicionAp medicionAp) {
		this.medicionAp = medicionAp;
	}

	

	

	
}
