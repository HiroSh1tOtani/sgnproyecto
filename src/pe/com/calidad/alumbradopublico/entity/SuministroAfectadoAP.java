package pe.com.calidad.alumbradopublico.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CAL_SUMINISTRO_AFECTADO_AP")
@SequenceGenerator (name = "sumAfectadopApIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "SuministroAfectadoAPSeq")
public class SuministroAfectadoAP implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long sumAfectadoApId;
	private String numSumCliente;
	private String nomCliente;
	private String tipTension;
	private String estado;
	
	private MedicionAp medicionAp;
	
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sumAfectadopApIdGenerator")
    @Column(name = "SUM_AFECTADO_AP_ID",nullable=false,unique=true)	
	public long getSumAfectadoApId() {
		return sumAfectadoApId;
	}

	public void setSumAfectadoApId(long sumAfectadoApId) {
		this.sumAfectadoApId = sumAfectadoApId;
	}

	@Column(name = "NUM_SUM_CLIENTE",nullable=true)
	public String getNumSumCliente() {
		return numSumCliente;
	}

	public void setNumSumCliente(String numSumCliente) {
		this.numSumCliente = numSumCliente;
	}

	@Column(name = "NOM_CLIENTE",nullable=true)
	public String getNomCliente() {
		return nomCliente;
	}

	public void setNomCliente(String nomCliente) {
		this.nomCliente = nomCliente;
	}

	@Column(name = "TIP_TENSION",nullable=true)
	public String getTipTension() {
		return tipTension;
	}

	public void setTipTension(String tipTension) {
		this.tipTension = tipTension;
	}

	@Column(name = "ESTADO",nullable=true)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@ManyToOne
    @JoinColumn(name="PK_MEDICION_AP_ID", nullable=true)
	public MedicionAp getMedicionAp() {
		return medicionAp;
	}

	public void setMedicionAp(MedicionAp medicionAp) {
		this.medicionAp = medicionAp;
	}

	
}
