
package pe.com.calidad.alumbradopublico.dao;

import java.util.List;

import pe.com.calidad.alumbradopublico.entity.CompensacionAp;



/**
 *
 * @author Lucho
 */
public interface CompensacionApDao {
    public void create(CompensacionAp compensacionAp);
    public CompensacionAp ReadById(long compensacioApId);
	public void update(CompensacionAp compensacionAp);
	public void delete(long compensacioApId);
   	public List<CompensacionAp> getAllRows();
	public List<CompensacionAp> getAllRows1();
	public CompensacionAp getAllRows1(String numSumCliente,String semestre);
        
}
