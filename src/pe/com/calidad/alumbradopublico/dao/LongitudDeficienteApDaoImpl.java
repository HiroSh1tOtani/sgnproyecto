package pe.com.calidad.alumbradopublico.dao;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;




import pe.com.calidad.alumbradopublico.entity.CompensacionAp;
import pe.com.calidad.alumbradopublico.entity.LongitudDeficienteAp;
import pe.com.indra.calidad.dao.HibernateUtil;


/*
import pe.com.calidad.alumbradopublico.entity.CompensacionAp;
import pe.com.calidad.suministro.entity.CompensacionSuministro;
import pe.com.indra.calidad.dao.HibernateUtil;
*/
import java.util.List;
import java.util.Set;


/**
 *
 * @author Luis
 */
public class LongitudDeficienteApDaoImpl implements LongitudDeficienteApDao {

    private static LongitudDeficienteApDaoImpl instance = null;

    public static LongitudDeficienteApDaoImpl getInstance() {
        if (instance == null) {
            instance = new LongitudDeficienteApDaoImpl();
        }

        return instance;
    }

    public LongitudDeficienteApDaoImpl() {
    }

	@Override
	public void create(LongitudDeficienteAp longitudDeficienteAp) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(longitudDeficienteAp);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear LongitudDeficienteAp.", e);
        }
    }

	@Override
	public LongitudDeficienteAp ReadById(long lonDeficienteApId) {
		LongitudDeficienteAp longitudDeficienteAp = null;
        try {
            HibernateUtil.begin();

            longitudDeficienteAp = (LongitudDeficienteAp) HibernateUtil.getSession().load(LongitudDeficienteAp.class, lonDeficienteApId);

            Hibernate.initialize(longitudDeficienteAp);
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Longitud DeficienteAp.", e);
        }
        return longitudDeficienteAp;
    }

	@Override
	public void update(LongitudDeficienteAp longitudDeficienteAp) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(longitudDeficienteAp);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Longitud DeficienteAp.", e);
        }
    }

	@Override
	public void delete(long lonDeficienteApId) {
		LongitudDeficienteAp longitudDeficienteAp = null;
		longitudDeficienteAp = ReadById(lonDeficienteApId);

        if (longitudDeficienteAp != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(longitudDeficienteAp);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Longitud DeficienteAp.", e);
            }
        }
    }

	@Override
	public List<LongitudDeficienteAp> getAllRows() {
		List<LongitudDeficienteAp> longitudDeficienteAp=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from LongitudDeficienteAp");
	            
	            longitudDeficienteAp=(List<LongitudDeficienteAp>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Longitud DeficienteAp.", e);
	        }
		return longitudDeficienteAp;
	}

	@Override
	public List<LongitudDeficienteAp> getAllRows1() {
		List<LongitudDeficienteAp> longitudDeficienteAp=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from LongitudDeficienteAp where estado =:estado");
	            q.setString("estado","1");
	            longitudDeficienteAp=(List<LongitudDeficienteAp>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Longitud DeficienteAp .", e);
	        }
		return longitudDeficienteAp;
	}
    
}
