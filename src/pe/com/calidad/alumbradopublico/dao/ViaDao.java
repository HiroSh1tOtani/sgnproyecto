
package pe.com.calidad.alumbradopublico.dao;

import java.util.List;
import java.util.Set;


import pe.com.calidad.alumbradopublico.entity.ProgramaAp;
import pe.com.indra.calidad.entity.Localidad;
import pe.com.calidad.alumbradopublico.entity.Via;
import pe.com.calidad.alumbradopublico.entity.ViaVano;


/**
 *
 * @author Luis
 */
public interface ViaDao {
    
    public void create(Via via);
    public Via ReadById(long viaId);
    public Via ReadByCod(String codVia);
    public void update(Via via);
    public void delete(long viaId);
    

    public Localidad selectLocalidad(long viaId);

    public Set<ProgramaAp> selectProgramaAp(long viaId);
    public Set<ViaVano> selectViaVanos(long viaId);
    
    public List<Via> getAllRows();
    public List<Via> getAllRows1();
    public List<Via> getAllRows1(long localidadId);
   
    
}
