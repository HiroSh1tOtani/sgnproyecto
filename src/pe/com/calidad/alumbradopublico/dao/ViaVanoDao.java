
package pe.com.calidad.alumbradopublico.dao;

import java.util.List;
import java.util.Set;

import pe.com.calidad.alumbradopublico.entity.Via;
import pe.com.calidad.alumbradopublico.entity.ViaVano;
import pe.com.calidad.alumbradopublico.entity.MedicionAp;


/**
 *
 * @author Luis
 */
public interface ViaVanoDao {
    
    public void create(ViaVano viaVano);
    public ViaVano ReadById(long viaVanoId);
    public void update(ViaVano viaVano);
    public void delete(long viaVanoId);

    public Via selectVia(long viaVanoId);

    public Set<MedicionAp> selectViaVano(long viaVanoId);
    
    public List<ViaVano> getAllRows();
    public List<ViaVano> getAllRows1();
    public List<ViaVano> getAllRows1(long viaId);
    public List<ViaVano> getAllRows1NoAsignado(long programaApId, long viaId);
   
    
}
