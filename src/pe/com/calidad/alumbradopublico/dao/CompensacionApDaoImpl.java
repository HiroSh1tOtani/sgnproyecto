package pe.com.calidad.alumbradopublico.dao;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.calidad.alumbradopublico.entity.CompensacionAp;
import pe.com.calidad.suministro.entity.CompensacionSuministro;
import pe.com.indra.calidad.dao.HibernateUtil;

import java.util.List;
import java.util.Set;


/**
 *
 * @author Luis
 */
public class CompensacionApDaoImpl implements CompensacionApDao {

    private static CompensacionApDaoImpl instance = null;

    public static CompensacionApDaoImpl getInstance() {
        if (instance == null) {
            instance = new CompensacionApDaoImpl();
        }

        return instance;
    }

    public CompensacionApDaoImpl() {
    }

	@Override
	public void create(CompensacionAp compensacionAp) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(compensacionAp);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear CompensacionAp.", e);
        }
    }

	@Override
	public CompensacionAp ReadById(long compensacioApId) {
		CompensacionAp compensacionAp = null;
        try {
            HibernateUtil.begin();

            compensacionAp = (CompensacionAp) HibernateUtil.getSession().load(CompensacionAp.class, compensacioApId);

            Hibernate.initialize(compensacionAp);
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar CompensacionAp.", e);
        }
        return compensacionAp;
    }

	@Override
	public void update(CompensacionAp compensacionAp) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(compensacionAp);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar CompensacionAp.", e);
        }
    }

	@Override
	public void delete(long compensacioApId) {
		CompensacionAp compensacionAp = null;
		compensacionAp = ReadById(compensacioApId);

        if (compensacionAp != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(compensacionAp);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar CompensacioAp.", e);
            }
        }
    }

	@Override
	public List<CompensacionAp> getAllRows() {
		List<CompensacionAp> compensacionAp=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from CompensacionAp");
	            
	            compensacionAp=(List<CompensacionAp>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar CompensacionAp.", e);
	        }
		return compensacionAp;
	}

	@Override
	public List<CompensacionAp> getAllRows1() {
		List<CompensacionAp> compensacionAp=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from CompensacionAp where estado =:estado");
	            q.setString("estado","1");
	            compensacionAp=(List<CompensacionAp>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar CompensacionAp .", e);
	        }
		return compensacionAp;
	}

	@Override
	public CompensacionAp getAllRows1(String numSumCliente, String semestre) {	
		CompensacionAp compensacionAp=null;
	 try {
         HibernateUtil.begin();
         Query q = HibernateUtil.getSession().createQuery("from CompensacionAp where estado =:estado and numSumCliente = :numSumCliente and semestre =:semestre ");
         q.setString("numSumCliente",numSumCliente);
         q.setString("semestre",semestre);
         q.setString("estado", "1");
         
         if(q.list().size() > 0){
          
        	 compensacionAp = (CompensacionAp) q.list().get(0);
         
         }
         HibernateUtil.commit();
         HibernateUtil.close();
         
	         
     } catch (HibernateException e) {
         HibernateUtil.rollback();
         throw new HibernateException("No se puede encontrar CompensacionAp.", e);
     } 
	return compensacionAp;	


}

    
	
}
