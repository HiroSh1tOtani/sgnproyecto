/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.alumbradopublico.dao;

import java.util.Set;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.calidad.alumbradopublico.entity.ProgramaAp;
import pe.com.calidad.alumbradopublico.entity.MedicionAp;
import pe.com.indra.calidad.dao.HibernateUtil;
import pe.com.indra.calidad.entity.PeriodoMedicion;
import pe.com.indra.calidad.entity.TipoMedicion;
import pe.com.indra.calidad.entity.Localidad;
import pe.com.calidad.alumbradopublico.entity.Via;
import pe.com.indra.calidad.entity.TipoPunto;



/**
 *
 * @author Luis
 */
public class ProgramaApDaoImpl implements ProgramaApDao {

    private static ProgramaApDaoImpl instance = null;

    public static ProgramaApDaoImpl getInstance() {
        if (instance == null) {
            instance = new ProgramaApDaoImpl();
        }

        return instance;
    }

    public ProgramaApDaoImpl() {
    }

	@Override
	public void create(ProgramaAp programaAp) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(programaAp);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
           
            throw new HibernateException("No se puede crear ProgramaAp.", e);
        }
    }

	@Override
	public ProgramaAp ReadById(long programaApId) {
		ProgramaAp programaAp = null;
        try {
            HibernateUtil.begin();

            programaAp = (ProgramaAp) HibernateUtil.getSession().load(ProgramaAp.class, programaApId);

            Hibernate.initialize(programaAp);
            Hibernate.initialize(programaAp.getTipoMedicion());
     
            Hibernate.initialize(programaAp.getLocalidad());
            Hibernate.initialize(programaAp.getVia());
            Hibernate.initialize(programaAp.getTipoPunto());
            Hibernate.initialize(programaAp.getMedicionAp());
            Hibernate.initialize(programaAp.getPeriodoMedicion());
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar ProgramaAp.", e);
        }
        return programaAp;
    }

	@Override
	public void update(ProgramaAp programaAp) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(programaAp);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar ProgramaAp.", e);
        }
    }

	@Override
	public void delete(long programaApId) {

		ProgramaAp programaAp = null;
		programaAp = ReadById(programaApId);

        if (programaAp != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(programaAp);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar ProgramaAp.", e);
            }
        }
    }

	@Override
	public Via selectVia(long programaApId) {
		ProgramaAp programaAp = null;
		programaAp = ReadById(programaApId);

        return programaAp.getVia();
    }

	@Override
	public PeriodoMedicion selectPeriodoMedicion(long programaApId) {
		ProgramaAp programaAp = null;
		programaAp = ReadById(programaApId);

        return programaAp.getPeriodoMedicion();
    }

	@Override
	public Localidad selectLocalidad(long programaApId) {
		ProgramaAp programaAp = null;
		programaAp = ReadById(programaApId);

        return programaAp.getLocalidad();
    }

	@Override
	public TipoPunto selectTipoPunto(long programaApId) {
		ProgramaAp programaAp = null;
		programaAp = ReadById(programaApId);

        return programaAp.getTipoPunto();
    }

	@Override
	public TipoMedicion selectTipoMedicion(long programaApId) {
		ProgramaAp programaAp = null;
		programaAp = ReadById(programaApId);

        return programaAp.getTipoMedicion();}

	@Override
	public Set<MedicionAp> selectMedicionAp(long programaApId) {
		ProgramaAp programaAp = null;
		programaAp = ReadById(programaApId);

        return programaAp.getMedicionAp();
    }

	@Override
	public List<ProgramaAp> getAllRows() {
		List<ProgramaAp> programasAp=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from ProgramaAp ");
	            
	            programasAp=(List<ProgramaAp>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar ProgramaAp.", e);
	        }
		return programasAp;
	}

	@Override
	public List<ProgramaAp> getAllRows1() {
		List<ProgramaAp> programasAp=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from ProgramaAp where estado =:estado ");
	            q.setString("estado","1");
	            
	            programasAp=(List<ProgramaAp>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar ProgramaAp.", e);
	        }
		return programasAp;
	}

	@Override
	public List<ProgramaAp> getAllRows1(long perMedicionId) {
		List<ProgramaAp> medicionesAp=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from ProgramaAp i where i.estado =:estado and i.periodoMedicion.perMedicionId = :perMedicionId ");
	            q.setString("estado","1");
	            q.setLong("perMedicionId", perMedicionId);
	            
	            medicionesAp=(List<ProgramaAp>) q.list();
	            
	            for(ProgramaAp inter:medicionesAp){
	            	Hibernate.initialize(inter.getMedicionAp());
	            }

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Medicion Ap.", e);
	        }
		return medicionesAp;
	}

    
}
