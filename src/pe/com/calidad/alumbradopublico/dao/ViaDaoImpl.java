/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.alumbradopublico.dao;

import java.util.Set;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.calidad.alumbradopublico.entity.ProgramaAp;
import pe.com.calidad.alumbradopublico.entity.Via;
import pe.com.calidad.alumbradopublico.entity.ViaVano;
import pe.com.indra.calidad.dao.HibernateUtil;
import pe.com.indra.calidad.entity.Localidad;
import pe.com.indra.calidad.entity.ParametroMedido;



/**
 *
 * @author Luis
 */
public class ViaDaoImpl implements ViaDao {

    private static ViaDaoImpl instance = null;

    public static ViaDaoImpl getInstance() {
        if (instance == null) {
            instance = new ViaDaoImpl();
        }

        return instance;
    }

    public ViaDaoImpl() {
    }

	@Override
	public void create(Via via) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(via);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
           
            throw new HibernateException("No se puede crear via.", e);
        }
    }

	@Override
	public Via ReadById(long viaId) {
		Via via = null;
        try {
            HibernateUtil.begin();

            via = (Via) HibernateUtil.getSession().load(Via.class, viaId);

            Hibernate.initialize(via);
            Hibernate.initialize(via.getLocalidad());
            Hibernate.initialize(via.getViaVano());
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar via.", e);
        }
        return via;
    }

	@Override
	public void update(Via via) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(via);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar via.", e);
        }
    }

	@Override
	public void delete(long viaId) {

		Via via = null;
		via = ReadById(viaId);

        if (via != null) {
            try {
                HibernateUtil.begin();
                via.setEstado("0");
                
                for (ViaVano viaVano : via.getViaVano()) {
                	viaVano.setEstado("0");
				}
                
                HibernateUtil.getSession().update(via);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar via.", e);
            }
        }
    }

	@Override
	public Localidad selectLocalidad(long viaId) {
		Via via = null;
		via = ReadById(viaId);

        return via.getLocalidad();
    }

	@Override
	public Set<ProgramaAp> selectProgramaAp(long viaId) {
		Via via = null;
		via = ReadById(viaId);

        return via.getProgramaAp();
    }

	@Override
	public Set<ViaVano> selectViaVanos(long viaId) {
		Via via = null;
		via = ReadById(viaId);

        return via.getViaVano();
    }

	@Override
	public List<Via> getAllRows() {
		List<Via> via=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from Via ");
	            
	            via=(List<Via>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Via.", e);
	        }
		return via;
	}

	@Override
	public List<Via> getAllRows1() {
		List<Via> via=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from Via where estado =:estado ");
	            q.setString("estado","1");
	            
	            via=(List<Via>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Via.", e);
	        }
		return via;
	}

	@Override
	public List<Via> getAllRows1(long localidadId) {
		List<Via> via=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from Via i where i.estado =:estado and i.localidad.localidadId = :localidadId");
	            q.setString("estado","1");
	            q.setLong("localidadId", localidadId);
	            
	            via=(List<Via>) q.list();
	   
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar via.", e);
	        }
		return via;
	}

	@Override
	public Via ReadByCod(String codVia) {
        Via via = null;
        try {
            HibernateUtil.begin();

            Query q = HibernateUtil.getSession().createQuery("from Via where codVia = :codVia and estado = '1'");
            q.setString("codVia", codVia);
            via = (Via) q.uniqueResult();

            Hibernate.initialize(via);
            Hibernate.initialize(via.getLocalidad());
            Hibernate.initialize(via.getViaVano());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Via.", e);
        }
        return via;
	}

	
	    
}
