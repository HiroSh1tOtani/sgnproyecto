
package pe.com.calidad.alumbradopublico.dao;

import java.math.BigDecimal;
import java.util.List;

import pe.com.calidad.alumbradopublico.entity.MedicionAp;
import pe.com.calidad.alumbradopublico.entity.ProgramaAp;
import pe.com.calidad.alumbradopublico.entity.ViaVano;

/**
 *
 * @author Luis
 */
public interface MedicionApDao {
    
    public void create(MedicionAp medicionAp);
    public MedicionAp ReadById(long medicionApId);
    public void update(MedicionAp medicionAp);
    public void delete(long medicionApId);
  
    public ViaVano selectViaVano(long medicionApId);
    public ProgramaAp selectProgramaAp(long medicionApId);
    
    public List<MedicionAp> getAllRows();
    public List<MedicionAp> getAllRows1();
    public List<MedicionAp> getAllRows1(long programaApId);
    
    public MedicionAp ReadByNroVano(long programaApId, BigDecimal nroVano);
    
}
