/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.alumbradopublico.dao;

import java.util.Set;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.calidad.alumbradopublico.entity.MedicionAp;
import pe.com.calidad.alumbradopublico.entity.Via;
import pe.com.calidad.alumbradopublico.entity.ViaVano;
import pe.com.indra.calidad.dao.HibernateUtil;


/**
 *
 * @author Luis
 */
public class ViaVanoDaoImpl implements ViaVanoDao {

    private static ViaVanoDaoImpl instance = null;

    public static ViaVanoDaoImpl getInstance() {
        if (instance == null) {
            instance = new ViaVanoDaoImpl();
        }

        return instance;
    }

    public ViaVanoDaoImpl() {
    }

	@Override
	public void create(ViaVano viaVano) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(viaVano);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
           
            throw new HibernateException("No se puede crear ViaVano.", e);
        }
    }

	@Override
	public ViaVano ReadById(long viaVanoId) {
		ViaVano viaVano = null;
        try {
            HibernateUtil.begin();

            viaVano = (ViaVano) HibernateUtil.getSession().load(ViaVano.class, viaVanoId);

            Hibernate.initialize(viaVano);
            Hibernate.initialize(viaVano.getVia());
            
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar ViaVano.", e);
        }
        return viaVano;
    }

	@Override
	public void update(ViaVano viaVano) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(viaVano);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar ViaVano.", e);
        }
    }

	@Override
	public void delete(long viaVanoId) {

		ViaVano viaVano = null;
		viaVano = ReadById(viaVanoId);

        if (viaVano != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(viaVano);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar viaVano.", e);
            }
        }
    }

	@Override
	public Via selectVia(long viaVanoId) {
		ViaVano viaVano = null;
		viaVano = ReadById(viaVanoId);

        return viaVano.getVia();
    }

	@Override
	public Set<MedicionAp> selectViaVano(long viaVanoId) {
		ViaVano viaVano = null;
		viaVano = ReadById(viaVanoId);

        return viaVano.getMedicionAp();
    }

	@Override
	public List<ViaVano> getAllRows() {
		List<ViaVano> viaVano=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from ViaVano ");
	            
	            viaVano=(List<ViaVano>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar ViaVano.", e);
	        }
		return viaVano;
	}

	@Override
	public List<ViaVano> getAllRows1() {
		List<ViaVano> viaVano=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from ViaVano where estado =:estado ");
	            q.setString("estado","1");
	            
	            viaVano=(List<ViaVano>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar ViaVano.", e);
	        }
		return viaVano;
	}

	@Override
	public List<ViaVano> getAllRows1(long viaId) {
		List<ViaVano> viaVano=null;
		 try {
	            HibernateUtil.begin();

	            
	            Query q = HibernateUtil.getSession().createQuery("from ViaVano i where i.estado =:estado and i.via.viaId = :viaId order by i.nroVano");
	            q.setString("estado","1");
	            q.setLong("viaId", viaId);
	            
	            viaVano=(List<ViaVano>) q.list();
	            
	            for(ViaVano inter:viaVano){
	            	Hibernate.initialize(inter.getVia());
	            }

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar via.", e);
	        }
		return viaVano;
	}

	@Override
	public List<ViaVano> getAllRows1NoAsignado(long programaApId, long viaId) {
		List<ViaVano> viaVano=null;
		 try {
	            HibernateUtil.begin();

	            
	            Query q = HibernateUtil.getSession().createQuery("from ViaVano i where i.estado =:estado and i.via.viaId = :viaId and i.viaVanoId not in (select n.viaVano.viaVanoId from MedicionAp n where n.programaAp.programaApId = :programaApId) order by i.nroVano");
	            q.setString("estado","1");
	            q.setLong("programaApId", programaApId);
	            q.setLong("viaId", viaId);
	            
	            viaVano=(List<ViaVano>) q.list();
	            
	            for(ViaVano inter:viaVano){
	            	Hibernate.initialize(inter.getVia());
	            }

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar vias .", e);
	        }
		return viaVano;
	}

		    
}
