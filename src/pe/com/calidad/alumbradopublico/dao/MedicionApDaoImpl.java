/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.alumbradopublico.dao;

import java.math.BigDecimal;
import java.util.Set;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.calidad.alumbradopublico.entity.MedicionAp;
import pe.com.calidad.alumbradopublico.entity.ProgramaAp;
import pe.com.calidad.alumbradopublico.entity.ViaVano;
import pe.com.indra.calidad.dao.HibernateUtil;


/**
 *
 * @author Luis
 */
public class MedicionApDaoImpl implements MedicionApDao {

    private static MedicionApDaoImpl instance = null;

    public static MedicionApDaoImpl getInstance() {
        if (instance == null) {
            instance = new MedicionApDaoImpl();
        }

        return instance;
    }

    public MedicionApDaoImpl() {
    }

	@Override
	public void create(MedicionAp medicionAp) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(medicionAp);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
           
            throw new HibernateException("No se puede crear MedicionAp.", e);
        }
		
	}

	@Override
	public MedicionAp ReadById(long medicionApId) {
		MedicionAp medicionAp = null;
        try {
            HibernateUtil.begin();

            medicionAp = (MedicionAp) HibernateUtil.getSession().load(MedicionAp.class, medicionApId);

            Hibernate.initialize(medicionAp);
            Hibernate.initialize(medicionAp.getProgramaAp());
            Hibernate.initialize(medicionAp.getViaVano());
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar MedicionAp.", e);
        }
        return medicionAp;
    }

	@Override
	public void update(MedicionAp medicionAp) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(medicionAp);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar MedicionAp.", e);
        }
    }

	@Override
	public void delete(long medicionApId) {

		MedicionAp medicionAp = null;
		medicionAp = ReadById(medicionApId);

        if (medicionAp != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(medicionAp);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar MedicionAp.", e);
            }
        }
    }

	@Override
	public ViaVano selectViaVano(long medicionApId) {
		MedicionAp medicionAp = null;
		medicionAp = ReadById(medicionApId);

        return medicionAp.getViaVano();
    }

	@Override
	public ProgramaAp selectProgramaAp(long medicionApId) {
		MedicionAp medicionAp = null;
		medicionAp = ReadById(medicionApId);

        return medicionAp.getProgramaAp();
    }

	@Override
	public List<MedicionAp> getAllRows() {
		List<MedicionAp> medicionAp=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from MedicionAp");
	            
	            medicionAp=(List<MedicionAp>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar MedicionAp.", e);
	        }
		return medicionAp;
	}

	@Override
	public List<MedicionAp> getAllRows1() {
		List<MedicionAp> medicionAp=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from MedicionAp where estado =:estado");
	            q.setString("estado","1");
	            medicionAp=(List<MedicionAp>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar MedicionAp.", e);
	        }
		return medicionAp;
	}

	@Override
	public List<MedicionAp> getAllRows1(long programaApId) {
		List<MedicionAp> medicionAp=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from MedicionAp where estado =:estado and programaAp.programaApId = :programaApId order by viaVano.nroVano");
	            q.setString("estado","1");
	            q.setLong("programaApId",programaApId);
	            
	            medicionAp=(List<MedicionAp>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Suministro Afectado.", e);
	        }
		return medicionAp;
	}

	@Override
	public MedicionAp ReadByNroVano(long programaApId, BigDecimal nroVano) {
		List<MedicionAp> medicionesAp=null;
		MedicionAp medicionApAux = null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from MedicionAp m where m.estado =:estado and m.programaAp.programaApId = :programaApId and m.viaVano.nroVano = :nroVano");
	            q.setString("estado","1");
	            q.setLong("programaApId",programaApId);
	            q.setBigDecimal("nroVano",nroVano);
	            
	            medicionesAp=(List<MedicionAp>) q.list();
	            
	            if(medicionesAp != null ){
	            	if(medicionesAp.size() > 0){
	            		medicionApAux = medicionesAp.get(0);
	            	}
	            }
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Suministro Afectado.", e);
	        }
		return medicionApAux;
	}

   
    
    
}
