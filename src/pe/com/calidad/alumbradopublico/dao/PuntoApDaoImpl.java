/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.alumbradopublico.dao;

import java.util.Set;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;


import pe.com.calidad.alumbradopublico.entity.MedicionAp;
import pe.com.calidad.alumbradopublico.entity.PuntoAp;
import pe.com.indra.calidad.dao.HibernateUtil;


/**
 *
 * @author Luis
 */
public class PuntoApDaoImpl implements PuntoApDao {

    private static PuntoApDaoImpl instance = null;

    public static PuntoApDaoImpl getInstance() {
        if (instance == null) {
            instance = new PuntoApDaoImpl();
        }

        return instance;
    }

    public PuntoApDaoImpl() {
    }
    
    @Override
	public void create(PuntoAp puntoAp) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(puntoAp);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
           
            throw new HibernateException("No se puede crear PuntoAP.", e);
        }
        
    }
    
    
    @Override
	public PuntoAp ReadById(long puntoApId) {

    	PuntoAp puntoAp = null;
        try {
            HibernateUtil.begin();

            puntoAp = (PuntoAp) HibernateUtil.getSession().load(PuntoAp.class, puntoApId);

            Hibernate.initialize(puntoAp);
            Hibernate.initialize(puntoAp.getMedicionAp());
            
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar PuntoAP.", e);
        }
        		
		return puntoAp;
	}

    
    @Override
	public void update(PuntoAp puntoAp) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(puntoAp);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar PuntoAP.", e);
        }
		
	}
    
    
    @Override
	public void delete(long puntoApId) {

    	PuntoAp puntoAp = null;
    	puntoAp = ReadById(puntoApId);

        if (puntoAp != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(puntoAp);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar PuntoAP.", e);
            }
        }
		
	}
    
    @Override
	public MedicionAp selectVia(long puntoApId) {
    	PuntoAp puntoAp = null;
    	puntoAp = ReadById(puntoApId);

    	return puntoAp.getMedicionAp();
		
	}
    
    @Override
	public List<PuntoAp> getAllRows() {

		List<PuntoAp> puntoAp=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from PuntoAp ");
	            
	            puntoAp=(List<PuntoAp>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar PuntoAP.", e);
	        }
		return puntoAp;
		
	}
    
    @Override
	public List<PuntoAp> getAllRows1() {

		List<PuntoAp> puntoAp=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from PuntoAp where estado =:estado ");
	            q.setString("estado","1");
	            
	            puntoAp=(List<PuntoAp>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar PuntoAP.", e);
	        }
		return puntoAp;
		}
    
    @Override
	public List<PuntoAp> getAllRows1(long medicionApId) {

		List<PuntoAp> puntoAp=null;
		 try {
	            HibernateUtil.begin();

	            
	            Query q = HibernateUtil.getSession().createQuery("from PuntoAp i where i.estado =:estado and i.medicionAp.medicionApId = :medicionApId order by i.puntoApId asc");
	            q.setString("estado","1");
	            q.setLong("medicionApId", medicionApId);
	            
	            puntoAp=(List<PuntoAp>) q.list();
	            
	            /*for(PuntoAp inter:puntoAP){
	            	Hibernate.initialize(inter.getMedicionAp());
	            }*/

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Medicion.", e);
	        }
		return puntoAp;
		
	}
    
		    
}
