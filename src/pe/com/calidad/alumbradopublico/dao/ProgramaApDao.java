
package pe.com.calidad.alumbradopublico.dao;

import java.util.List;
import java.util.Set;


import pe.com.calidad.alumbradopublico.entity.MedicionAp;
import pe.com.indra.calidad.entity.Localidad;
import pe.com.calidad.alumbradopublico.entity.ProgramaAp;
import pe.com.indra.calidad.entity.PeriodoMedicion;
import pe.com.indra.calidad.entity.TipoMedicion;
import pe.com.indra.calidad.entity.TipoPunto;
import pe.com.calidad.alumbradopublico.entity.Via;

/**
 *
 * @author Luis
 */
public interface ProgramaApDao {
    
    public void create(ProgramaAp programaAp);
    public ProgramaAp ReadById(long programaApId);
    public void update(ProgramaAp programaAp);
    public void delete(long programaApId);
    
    public Via selectVia(long programaApId);
    public PeriodoMedicion selectPeriodoMedicion(long programaApId);
    public Localidad selectLocalidad(long programaApId);
    public TipoPunto selectTipoPunto(long programaApId);
    public TipoMedicion selectTipoMedicion(long programaApId);
    public Set<MedicionAp> selectMedicionAp(long programaApId);
    
    public List<ProgramaAp> getAllRows();
    public List<ProgramaAp> getAllRows1();
    public List<ProgramaAp> getAllRows1(long perMedicionId);
   
    
}
