
package pe.com.calidad.alumbradopublico.dao;

import java.util.List;
import java.util.Set;

import pe.com.calidad.alumbradopublico.entity.MedicionAp;
import pe.com.calidad.alumbradopublico.entity.PuntoAp;



/**
 *
 * @author Luis
 */
public interface PuntoApDao {
    
    public void create(PuntoAp puntoAp);
    public PuntoAp ReadById(long puntoApId);
    public void update(PuntoAp puntoAp);
    public void delete(long puntoApId);

    public MedicionAp selectVia(long puntoApId);
    public List<PuntoAp> getAllRows();
    public List<PuntoAp> getAllRows1();
    public List<PuntoAp> getAllRows1(long medicionApId);
   
    
}
