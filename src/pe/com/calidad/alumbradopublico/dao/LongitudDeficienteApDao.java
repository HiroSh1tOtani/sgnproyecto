
package pe.com.calidad.alumbradopublico.dao;

import java.util.List;

import pe.com.calidad.alumbradopublico.entity.LongitudDeficienteAp;



/**
 *
 * @author Lucho
 */
public interface LongitudDeficienteApDao {
    public void create(LongitudDeficienteAp longitudDeficienteAp);
    public LongitudDeficienteAp ReadById(long lonDeficienteApId);
	public void update(LongitudDeficienteAp longitudDeficienteAp);
	public void delete(long lonDeficienteApId);
   	public List<LongitudDeficienteAp> getAllRows();
	public List<LongitudDeficienteAp> getAllRows1();
	
        
}
