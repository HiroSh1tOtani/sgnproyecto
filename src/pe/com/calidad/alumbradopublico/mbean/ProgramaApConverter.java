package pe.com.calidad.alumbradopublico.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.calidad.alumbradopublico.entity.ProgramaAp;
import pe.com.calidad.alumbradopublico.service.ProgramaApService;

@FacesConverter(value="interrupcionConverter",forClass=ProgramaApConverter.class)

public class ProgramaApConverter implements Converter{

	@Override
	public ProgramaAp getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		//System.out.println("Cadena:"+cadena);
		Long programaApId=new Long(cadena);
		ProgramaApService service=new ProgramaApService();
		ProgramaAp programaAp=new ProgramaAp();
		programaAp=service.ReadById(programaApId);
		//System.out.println(programaAp.getProgramaApId());
		return programaAp;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			ProgramaAp programaAp=(ProgramaAp) objeto;
			////System.out.println("objeto:"+new Long(programaAp.getProgramaApId()).toString());
			return new Long(programaAp.getProgramaApId()).toString();
		}
		return null;
	}


}
