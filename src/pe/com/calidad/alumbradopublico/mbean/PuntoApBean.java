package pe.com.calidad.alumbradopublico.mbean;



import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import pe.com.calidad.alumbradopublico.entity.MedicionAp;
import pe.com.calidad.alumbradopublico.entity.PuntoAp;
import pe.com.calidad.alumbradopublico.service.MedicionApService;
import pe.com.calidad.alumbradopublico.service.PuntoApService;
import pe.com.indra.calidad.util.Utilidad;

@ManagedBean(name="puntoApBean")
@SessionScoped
public class PuntoApBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	private List<PuntoAp> puntosAp;
	private long medicionApId;
	private MedicionAp medicionAp;
	
	

	@PostConstruct
	public void init(){
			
		
	}
	
	public String mostrarAfectados(){
		PuntoApService service = new PuntoApService();
		String medicionApIdStr = Utilidad.getParametro("medicionApId");
		
		if(medicionApIdStr != null){
			medicionApId = Long.valueOf(medicionApIdStr);
			
			MedicionApService medicionApService = new MedicionApService();
			
			medicionAp = medicionApService.ReadById(medicionApId);
			
			puntosAp = (List<PuntoAp>) service.getAllRows1(Long.valueOf(medicionApIdStr));
		}else {
			puntosAp = (List<PuntoAp>) service.getAllRows1(medicionApId);
		}
		
		return "/zonasegura/alumbradoPublico/puntoApListar?faces-redirect=true";
	}
	
	

	
	/*public String mostrarAfectados(){
		String medicionApIdStr = Utilidad.getParametro("medicionApId");
		
		PuntoApService service = new PuntoApService();
		
		
		puntosAp = (List<PuntoAp>) service.getAllRows1(Long.valueOf(medicionApIdStr));
		
		return "/zonasegura/alumbradoPublico/puntoApListar?faces-redirect=true";
	}*/

	public List<PuntoAp> getPuntosAp() {
		return puntosAp;
	}

	public void setPuntosAp(List<PuntoAp> puntosAp) {
		this.puntosAp = puntosAp;
	}

	public long getMedicionApId() {
		return medicionApId;
	}

	public void setMedicionApId(long medicionApId) {
		this.medicionApId = medicionApId;
	}

	public MedicionAp getMedicionAp() {
		return medicionAp;
	}

	public void setMedicionAp(MedicionAp medicionAp) {
		this.medicionAp = medicionAp;
	}
	

}
