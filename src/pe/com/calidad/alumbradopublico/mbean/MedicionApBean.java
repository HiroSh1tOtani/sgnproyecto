package pe.com.calidad.alumbradopublico.mbean;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import org.hibernate.HibernateException;
import org.richfaces.component.UIExtendedDataTable;
import org.richfaces.model.Filter;

import pe.com.calidad.alumbradopublico.entity.MedicionAp;
import pe.com.calidad.alumbradopublico.entity.ProgramaAp;
import pe.com.calidad.alumbradopublico.entity.ViaVano;
import pe.com.calidad.alumbradopublico.service.MedicionApService;
import pe.com.calidad.alumbradopublico.service.ProgramaApService;
import pe.com.calidad.alumbradopublico.service.ViaVanoService;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.Reporte;
import pe.com.indra.calidad.service.MedicionService;
import pe.com.indra.calidad.util.ConectaDb;
import pe.com.indra.calidad.util.Utilidad;

@ManagedBean(name="medicionApBean")
@SessionScoped
public class MedicionApBean {
	private List<MedicionAp> medicionesAp;
	private MedicionAp medicionAp;
	private long programaApId;
	private ProgramaAp programaAp;
	
	
	private java.util.Date fecMedicion;
	private String mensaje;
		
	private String tipoAlumbradoFilter;
	private List<SelectItem> itemsTipoAlumbrado;
	private String tipoCalzadaFilter;
	private List<SelectItem> itemsTipoCalzada;
	private String calidadFilter;
	private List<SelectItem> itemsCalidad;
	private List<SelectItem> itemsCalidad1;
	private java.util.Date fecMedicionFilter;
	private List<SelectItem> itemsVanoVia;
	private List<SelectItem> itemsVanoViaNoAsignado;

	private Collection<Object> selection;
	private List<MedicionAp> selectionItems = new ArrayList<MedicionAp>();	
	
	public Collection<Object> getSelection() {
		return selection;
	}

	public void setSelection(Collection<Object> selection) {
		this.selection = selection;
	}

	public List<MedicionAp> getSelectionItems() {
		return selectionItems;
	}

	public void setSelectionItems(List<MedicionAp> selectionItems) {
		this.selectionItems = selectionItems;
	}
	
	public MedicionApBean() {

	}
	
	@PostConstruct
	public void init(){
			
		
	}	
	
	public String mostrarMedicionesAp(){
		MedicionApService service = new MedicionApService();
		String programaApIdStr = Utilidad.getParametro("programaApId");
		
		if(programaApIdStr != null){
			programaApId = Long.valueOf(programaApIdStr);
			
			ProgramaApService programaApService = new ProgramaApService();
			
			setProgramaAp(programaApService.ReadById(programaApId));
			
			medicionesAp = (List<MedicionAp>) service.getAllRows1(programaApId);
		}else {
			medicionesAp = (List<MedicionAp>) service.getAllRows1(programaApId);
		}
		
		
		
		return "/zonasegura/alumbradoPublico/medicionApListar?faces-redirect=true";
	}	
	
	
	public void cargarMedicionApActual(){
		
		String medicionApId=Utilidad.getParametro("medicionApId");
		MedicionApService service=new MedicionApService();
		
		try {
			
			medicionAp=service.ReadById(new Long(medicionApId));
			
			if(medicionAp.getFecMedicion()!=null) {
				fecMedicion = new java.util.Date( medicionAp.getFecMedicion().getTime());				
			}
			
			
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			e.printStackTrace();
			
			throw new HibernateException("No se puede cargar MedicionAP.", e);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Ocurrio un error", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			e.printStackTrace();
		}
		
	}
	
	public String prepararCrear(){
		//Esto funciona siempre que el bean tenga ambito de sesion 
		setMedicionAp(new MedicionAp());
		getMedicionAp().setEstado("1");
		getMedicionAp().setProgramaAp(programaAp);
		getMedicionAp().setAltLumInicial(new BigDecimal(8.5));
		getMedicionAp().setPotLamInicial(new BigDecimal(70));
		getMedicionAp().setTipLumInicial("ECOM 70W");
		getMedicionAp().setTipPasInicial("PA-05");
		getMedicionAp().setAltLumFinal(new BigDecimal(8.5));
		getMedicionAp().setPotLamFinal(new BigDecimal(70));
		getMedicionAp().setTipLumFinal("ECOM 70W");
		getMedicionAp().setTipPasFinal("PA-05");
		getMedicionAp().setTipAlumbrado(programaAp.getVia().getTipAp());
		fecMedicion = programaAp.getFecProgramada();
		getMedicionAp().setFecMedicion(programaAp.getFecProgramada());
		
		return "/zonasegura/alumbradoPublico/medicionApCrear?faces-redirect=true";
	}
	
	public String prepararEditar(){
		
		
		try {
			cargarMedicionApActual();
			return "/zonasegura/alumbradoPublico/medicionApEditar?faces-redirect=true";
		} catch (HibernateException e) {
			setMensaje(e.getMessage());
			return "/zonasegura/alumbradoPublico/medicionApListar?faces-redirect=true";
		}
			
	}
	
	public String prepararVer(){
			
		try {
			cargarMedicionApActual();
			return "/zonasegura/alumbradoPublico/medicionApVer?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/alumbradoPublico/medicionApListar?faces-redirect=true";
		}
	}	
	
	public String salvar(){
		
		MedicionApService service=new MedicionApService();
		
		try {
			
			if(fecMedicion!=null){
				medicionAp.setFecMedicion(new java.sql.Timestamp(fecMedicion.getTime()));
			}

			
		
			service.create(medicionAp);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

			return "/zonasegura/alumbradoPublico/medicionApCrear?faces-redirect=true";
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			return "/zonasegura/alumbradoPublico/medicionApCrear?faces-redirect=true";
		}
		
		mostrarMedicionesAp();
		
		return "/zonasegura/alumbradoPublico/medicionApListar?faces-redirect=true";
	}
	
	public String actualizar(){
	
		MedicionApService service=new MedicionApService();
		
		try {
			if(fecMedicion!=null){
				medicionAp.setFecMedicion(new java.sql.Timestamp(fecMedicion.getTime()));
			}
			
			service.update(medicionAp);
			
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			return "/zonasegura/alumbradoPublico/medicionApEditar?faces-redirect=true";
			
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			return "/zonasegura/alumbradoPublico/medicionApEditar?faces-redirect=true";
		}
		
		mostrarMedicionesAp();
		
		return "/zonasegura/alumbradoPublico/medicionApListar?faces-redirect=true";
	}
	
	public String eliminar(){
		String medicionApId=Utilidad.getParametro("medicionApId");
		MedicionApService service=new MedicionApService();

		
		try {
			/*
			medicionAp=service.ReadById(new Long(medicionApId));
			medicionAp.setEstado("0");
			service.update(medicionAp);
			*/
			medicionAp = service.ReadById(new Long(medicionApId));
			service.delete(medicionAp.getMedicionApId());
			
		} catch (HibernateException e) {
			
			
			System.out.println(e.getMessage()+":"+e.getCause());
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			
		} catch (Exception e) {
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		
		mostrarMedicionesAp();
		return "/zonasegura/alumbradoPublico/medicionApListar?faces-redirect=true";
	}	
	
	public List<MedicionAp> getMedicionesAp() {
		return medicionesAp;
	}
	public void setMedicionesAp(List<MedicionAp> medicionesAp) {
		this.medicionesAp = medicionesAp;
	}
	public MedicionAp getMedicionAp() {
		return medicionAp;
	}
	public void setMedicionAp(MedicionAp medicionAp) {
		this.medicionAp = medicionAp;
	}
	public long getProgramaApId() {
		return programaApId;
	}
	public void setProgramaApId(long programaApId) {
		this.programaApId = programaApId;
	}

	public ProgramaAp getProgramaAp() {
		return programaAp;
	}

	public void setProgramaAp(ProgramaAp programaAp) {
		this.programaAp = programaAp;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public java.util.Date getFecMedicion() {
		return fecMedicion;
	}

	public void setFecMedicion(java.util.Date fecMedicion) {
		this.fecMedicion = fecMedicion;
	}

	public String getTipoAlumbradoFilter() {
		return tipoAlumbradoFilter;
	}

	public void setTipoAlumbradoFilter(String tipoAlumbradoFilter) {
		this.tipoAlumbradoFilter = tipoAlumbradoFilter;
	}

	public Filter<?> getFilterTipoAlumbrando() {
        return new Filter<MedicionAp>() {
            public boolean accept(MedicionAp t) {
            	String tipoAlumbrado = getTipoAlumbradoFilter();
                if (tipoAlumbrado == null || tipoAlumbrado.length() == 0 || tipoAlumbrado.equals(t.getTipAlumbrado())) {
                    return true;
                }
                return false;
            }
        };
    }

	public List<SelectItem> getItemsTipoAlumbrado() {
		itemsTipoAlumbrado=new ArrayList<SelectItem>();
		itemsTipoAlumbrado.add(new SelectItem("", ""));
		itemsTipoAlumbrado.add(new SelectItem("I","TIPO I"));
		itemsTipoAlumbrado.add(new SelectItem("II","TIPO II"));
		itemsTipoAlumbrado.add(new SelectItem("III","TIPO III"));
		itemsTipoAlumbrado.add(new SelectItem("IV","TIPO IV"));
		itemsTipoAlumbrado.add(new SelectItem("V","TIPO V"));
		return itemsTipoAlumbrado;
	}

	public void setItemsTipoAlumbrado(List<SelectItem> itemsTipoAlumbrado) {
		this.itemsTipoAlumbrado = itemsTipoAlumbrado;
	}

	public String getTipoCalzadaFilter() {
		return tipoCalzadaFilter;
	}

	public void setTipoCalzadaFilter(String tipoCalzadaFilter) {
		this.tipoCalzadaFilter = tipoCalzadaFilter;
	}

	public List<SelectItem> getItemsTipoCalzada() {
		itemsTipoCalzada=new ArrayList<SelectItem>();
		itemsTipoCalzada.add(new SelectItem("", ""));
		itemsTipoCalzada.add(new SelectItem("C","C-CLARA"));
		itemsTipoCalzada.add(new SelectItem("O","O-OSCURA"));
		return itemsTipoCalzada;
	}

	public void setItemsTipoCalzada(List<SelectItem> itemsTipoCalzada) {
		this.itemsTipoCalzada = itemsTipoCalzada;
	}
	
	public Filter<?> getFilterTipoCalzada() {
        return new Filter<MedicionAp>() {
            public boolean accept(MedicionAp t) {
            	String tipoCalzada = getTipoCalzadaFilter();
                if (tipoCalzada == null || tipoCalzada.length() == 0 || tipoCalzada.equals(t.getTipCalzada())) {
                    return true;
                }
                return false;
            }
        };
    }

	public String getCalidadFilter() {
		return calidadFilter;
	}

	public void setCalidadFilter(String calidadFilter) {
		this.calidadFilter = calidadFilter;
	}

	public List<SelectItem> getItemsCalidad() {
		itemsCalidad=new ArrayList<SelectItem>();
		itemsCalidad.add(new SelectItem("P","PROGRAMADO"));
		itemsCalidad.add(new SelectItem("S","TRAMO SI CUMPLE CON CALIDAD"));
		itemsCalidad.add(new SelectItem("N","TRAMO NO CUMPLE CON CALIDAD"));
		itemsCalidad.add(new SelectItem("C", "CARGADO PERO NO EVALUADO"));
		return itemsCalidad;
	}

	public void setItemsCalidad(List<SelectItem> itemsCalidad) {
		this.itemsCalidad = itemsCalidad;
	}
	
	public List<SelectItem> getItemsCalidad1() {
		itemsCalidad1=new ArrayList<SelectItem>();
		itemsCalidad1.add(new SelectItem("", ""));
		itemsCalidad1.add(new SelectItem("P","P-PROGRAMADO"));
		itemsCalidad1.add(new SelectItem("S","S-TRAMO SI CUMPLE CON CALIDAD"));
		itemsCalidad1.add(new SelectItem("N","N-TRAMO NO CUMPLE CON CALIDAD"));
		itemsCalidad1.add(new SelectItem("C","C-CARGADO PERO NO EVALUADO"));
		return itemsCalidad1;
	}

	public void setItemsCalidad1(List<SelectItem> itemsCalidad1) {
		this.itemsCalidad1 = itemsCalidad1;
	}

	public Filter<?> getFilterCalidad() {
        return new Filter<MedicionAp>() {
            public boolean accept(MedicionAp t) {
            	String calidad = getCalidadFilter();
                if (calidad == null || calidad.length() == 0 || calidad.equals(t.getCalidad())) {
                    return true;
                }
                return false;
            }
        };
    }

	public java.util.Date getFecMedicionFilter() {
		return fecMedicionFilter;
	}

	public void setFecMedicionFilter(java.util.Date fecMedicionFilter) {
		this.fecMedicionFilter = fecMedicionFilter;
	}
	
	public Filter<?> getFilterFecMedicion() {
        return new Filter<MedicionAp>() {
            public boolean accept(MedicionAp medicionAp) {
                Date fechaMedicion = getFecMedicionFilter();
                if (fechaMedicion == null) {
                	
                	return true;
                }else {
                	
                	Calendar calendar1 = Calendar.getInstance();
                	Calendar calendar2 = Calendar.getInstance();
                	
                	calendar1.setTime(fechaMedicion);
                	
                	if(medicionAp.getFecMedicion() == null){
                		return false;
                	}
                	
                	calendar2.setTime(medicionAp.getFecMedicion()); 
                	
                	if(calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR) && calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH) &&
                			calendar1.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH)){
                		
                		return true;
                		
                	}else{
                		return false;
                	}
                    
                }
            }
        };
	}

	public List<SelectItem> getItemsVanoVia() {

		ViaVanoService service = new ViaVanoService();
		itemsVanoVia = new ArrayList<SelectItem>();
		for(ViaVano tp:service.getAllRows1(programaAp.getVia().getViaId())){
			itemsVanoVia.add(new SelectItem(tp,tp.getPtoIni()+"-"+ tp.getPtoFin()));
		}
			
		return itemsVanoVia;
	}
	
	public void setItemsVanoVia(List<SelectItem> itemsVanoVia) {
		this.itemsVanoVia = itemsVanoVia;
	}
	
	public List<SelectItem> getItemsVanoViaNoAsignado() {
		ViaVanoService service = new ViaVanoService();
		itemsVanoViaNoAsignado = new ArrayList<SelectItem>();
		for(ViaVano tp:service.getAllRows1NoAsignado(programaAp.getProgramaApId(),programaAp.getVia().getViaId())){
			itemsVanoViaNoAsignado.add(new SelectItem(tp,tp.getPtoIni()+"-"+ tp.getPtoFin()));
		}
		return itemsVanoViaNoAsignado;
	}

	public void setItemsVanoViaNoAsignado(List<SelectItem> itemsVanoViaNoAsignado) {
		this.itemsVanoViaNoAsignado = itemsVanoViaNoAsignado;
	}
	
	
	
	public void selectionListener(AjaxBehaviorEvent event) {
		UIExtendedDataTable dataTable = (UIExtendedDataTable) event.getComponent();
		Object originalKey = dataTable.getRowKey();
		selectionItems.clear();
		for (Object selectionKey : selection) {
		    dataTable.setRowKey(selectionKey);
		    if (dataTable.isRowAvailable()) {
		        selectionItems.add((MedicionAp) dataTable.getRowData());
		    }
		}
		dataTable.setRowKey(originalKey);
	}		
	
	public String procesarAllMedicionAp(){
		for(MedicionAp med:medicionesAp){
			
			procesarMedicionAp(med.getMedicionApId());
			
		}
		
		return "/zonasegura/alumbradoPublico/medicionApListar?faces-redirect=true";
	}
	
	public String procesarMedicionAp(){	
		String result;
		
		for (MedicionAp medAp : selectionItems) {
			ConectaDb db = new ConectaDb("CALIDAD");
			Connection cn = db.getConnection();
	        CallableStatement cstmt = null;  
	        try {
	        	cstmt = cn.prepareCall("{CALL CAL_AP_PACKAGE.CALCULAR_INDICADORES_AP(?)}");
			   	cstmt.setLong(1, medAp.getMedicionApId());
				
	            int ctos = cstmt.executeUpdate();;
	            cstmt.close(); 
	
	            if (ctos == 0) {
	                result = "0 filas afectadas";
	            }
	
	        } catch (SQLException e) {
	            result = e.getMessage();
				setMensaje(e.getMessage()+":"+e.getCause());
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	            
	        } finally {
	            try {
	                cn.close();
	            } catch (SQLException e) {
	                result = e.getMessage();
	                
	    			setMensaje(e.getMessage()+":"+e.getCause());
	    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
	    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	                
	            }
	        }			
		}
		
        result = mostrarMedicionesAp();
        
        return result;	
	}
	
	public String procesarMedicionAp(long medicionApId){	
		String result;
		
		ConectaDb db = new ConectaDb("CALIDAD");
		Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        try {
        	cstmt = cn.prepareCall("{CALL CAL_AP_PACKAGE.CALCULAR_INDICADORES_AP(?)}");
		   	cstmt.setLong(1, medicionApId);
			
            int ctos = cstmt.executeUpdate();;
            cstmt.close(); 

            if (ctos == 0) {
                result = "0 filas afectadas";
            }

        } catch (SQLException e) {
            result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
            
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
                result = e.getMessage();
                
    			setMensaje(e.getMessage()+":"+e.getCause());
    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                
            }
        }			

		
        result = mostrarMedicionesAp();
        
        return result;	
	}

	public void vanoChanged(AjaxBehaviorEvent event){
		
		if(medicionAp.getViaVano() != null) {
			medicionAp.setTipCalzada(medicionAp.getViaVano().getTipCalzada());
			medicionAp.setLonMedida(medicionAp.getViaVano().getLongitud());
			medicionAp.setCodPosInicial(medicionAp.getViaVano().getPtoIni());
			medicionAp.setCodPosFinal(medicionAp.getViaVano().getPtoFin());
			
		}
	}
	
}
