package pe.com.calidad.alumbradopublico.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.calidad.alumbradopublico.entity.ViaVano;//Via;
import pe.com.calidad.alumbradopublico.service.ViaVanoService;//ViaService;

@FacesConverter(value="viaVanoConverter",forClass=ViaVanoConverter.class)

public class ViaVanoConverter implements Converter{

	@Override
	public ViaVano getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long viaVanoId=new Long(cadena);
		ViaVanoService service=new ViaVanoService();
		ViaVano viaVano=new ViaVano();
		viaVano=service.ReadById(viaVanoId);
		System.out.println(viaVano.getViaVanoId());
		return viaVano;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			ViaVano viaVano=(ViaVano) objeto;
			////System.out.println("objeto:"+new Long(viaVano.getViaVanoId()).toString());
			return new Long(viaVano.getViaVanoId()).toString();
		}
		return null;
	}


}
