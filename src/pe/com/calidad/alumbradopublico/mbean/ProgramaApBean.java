package pe.com.calidad.alumbradopublico.mbean;


import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import org.hibernate.HibernateException;
import org.richfaces.model.Filter;

import pe.com.calidad.alumbradopublico.entity.MedicionAp;
import pe.com.calidad.alumbradopublico.entity.ProgramaAp;
import pe.com.calidad.alumbradopublico.entity.Via;
import pe.com.calidad.alumbradopublico.service.ProgramaApService;
import pe.com.calidad.alumbradopublico.service.ViaService;
import pe.com.indra.calidad.entity.Localidad;

import pe.com.indra.calidad.entity.PeriodoMedicion;
import pe.com.indra.calidad.entity.TipoMedicion;
import pe.com.indra.calidad.entity.TipoPunto;
import pe.com.indra.calidad.service.LocalidadService;

import pe.com.indra.calidad.service.PeriodoMedicionService;
import pe.com.indra.calidad.service.TipoMedicionService;
import pe.com.indra.calidad.service.TipoPuntoService;
import pe.com.indra.calidad.util.ConectaDb;
import pe.com.indra.calidad.util.Utilidad;



@ManagedBean(name="programaApBean")
@SessionScoped
public class ProgramaApBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<ProgramaAp> programasAp;
	private ProgramaAp programaAp;
	
	
	private List<SelectItem> itemsPeriodoMedicion;
	private List<SelectItem> itemsPeriodoMedicion1;
	
	private List<SelectItem> itemsLocalidad;
	private List<SelectItem> itemsLocalidad1;
	
	
	private int origen;
	private PeriodoMedicion periodoMedicion;
	
	private String operacion;

	private long tipoSin;
	
	private String localidadFilter;
	private List<SelectItem> itemsTiposervicio;
	private List<SelectItem> itemsTipoMedicion1;
	private List<SelectItem> itemsTipoPunto1;
	private List<SelectItem> itemsVia;
	private java.util.Date fecProgramada;
	private List<SelectItem> itemsVia2;
	
	private String viaFilter;
	private String tipoPuntoFilter;
	private String TipoServicioFilter;
	private List<SelectItem> itemsTipoPunto;
	private List<SelectItem> itemsTipoServicios;
	private java.util.Date fecProgramadaFilter;

	private String mensaje;
	
	@PostConstruct 
	public void init(){
		System.out.println("se ejecuto PostConstruct "+new java.util.Date());
		programaAp=new ProgramaAp();
		programaAp.setEstado("1");
		
		tipoSin = 1;
		operacion = "SIRN";
	}
	
	
	public List<ProgramaAp> getProgramasAp() {
		return programasAp;
	}


	public void setProgramasAp(List<ProgramaAp> programasAp) {
		this.programasAp = programasAp;
	}


	public ProgramaAp getProgramaAp() {
		return programaAp;
	}


	public void setProgramaAp(ProgramaAp programaAp) {
		this.programaAp = programaAp;
	}



	
	public List<SelectItem> getItemsPeriodoMedicion() {
		PeriodoMedicionService service=new PeriodoMedicionService();
		itemsPeriodoMedicion = new ArrayList<SelectItem>();
		for(PeriodoMedicion tp:service.getAllRows()){
			itemsPeriodoMedicion.add(new SelectItem(tp,tp.getAno()+"-"+tp.getPeriodo()));
		}
		return itemsPeriodoMedicion;
	}
	
	public void setItemsPeriodoMedicion(List<SelectItem> itemsPeriodoMedicion) {
		this.itemsPeriodoMedicion = itemsPeriodoMedicion;
	}

	public List<SelectItem> getItemsPeriodoMedicion1() {
		PeriodoMedicionService service=new PeriodoMedicionService();
		itemsPeriodoMedicion1 = new ArrayList<SelectItem>();
		for(PeriodoMedicion tp:service.getAllRows1()){
			itemsPeriodoMedicion1.add(new SelectItem(tp,tp.getAno()+"-"+tp.getPeriodo()));
		}
		return itemsPeriodoMedicion1;
	}
	
	public void setItemsPeriodoMedicion1(List<SelectItem> itemsPeriodoMedicion1) {
		this.itemsPeriodoMedicion1 = itemsPeriodoMedicion1;
	}

	public List<SelectItem> getItemsLocalidad() {
		LocalidadService service=new LocalidadService();
		itemsLocalidad=new ArrayList<SelectItem>();
		for(Localidad tp:service.getAllRows()){
			itemsLocalidad.add(new SelectItem(tp,tp.getCodLocalidad()+"-"+tp.getNombre()));
		}
		return itemsLocalidad;
	}


	public void setItemsLocalidad(List<SelectItem> itemsLocalidad) {
		this.itemsLocalidad = itemsLocalidad;
	}


	public List<SelectItem> getItemsLocalidad1() {
		LocalidadService service=new LocalidadService();
		itemsLocalidad1=new ArrayList<SelectItem>();
		itemsLocalidad1.add(new SelectItem("", ""));
		for(Localidad tp:service.getAllRows1()){
			itemsLocalidad1.add(new SelectItem(tp.getCodLocalidad() ,tp.getCodLocalidad()+"-"+tp.getNombre()));
		}
		return itemsLocalidad1;
	}


	public void setItemsLocalidad1(List<SelectItem> itemsLocalidad1) {
		this.itemsLocalidad1 = itemsLocalidad1;
	}

	
	public String prepararCrear(){
		//Esto funciona siempre que el bean tenga ambito de sesion 
		setProgramaAp(new ProgramaAp());
		TipoMedicionService tipoMedicionService = new TipoMedicionService();
		getProgramaAp().setTipoMedicion(tipoMedicionService.ReadByCod("7"));
		//setProgramaAp(new ProgramaAp());
		getProgramaAp().setEstado("1");
		getProgramaAp().setPeriodoMedicion(periodoMedicion);
		fecProgramada = null;
		
		return "/zonasegura/alumbradoPublico/programaApCrear?faces-redirect=true";
	}
	
		
	public String prepararEditar(){
		
		
		try {
			cargarProgramaApActual();
		
			
			return "/zonasegura/alumbradoPublico/programaApEditar?faces-redirect=true";
		} catch (HibernateException e) {
			setMensaje(e.getMessage());
			return "/zonasegura/alumbradoPublico/programaApListar?faces-redirect=true";
		}
		
	}
	
	

	public String prepararVer(){
		//TODO: Por implementar carga de la instancia

		try {
			cargarProgramaApActual();
			return "/zonasegura/alumbradoPublico/programaApVer?faces-redirect=true";
		} catch (HibernateException e) {
			setMensaje(e.getMessage());
			return "/zonasegura/alumbradoPublico/programaApListar?faces-redirect=true";
		}
	}
	
	
	
	public String salvar(){
				
		ProgramaApService service=new ProgramaApService();
		
		try {
				
			if(fecProgramada!=null){
				programaAp.setFecProgramada(new java.sql.Timestamp(fecProgramada.getTime()));
			}else {
				programaAp.setFecProgramada(null);
			}
			
    		service.create(programaAp);
    	   				
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
						
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
						
		}
		
		ProgramaApxPeriodo();
		
		return "/zonasegura/alumbradoPublico/programaApListar?faces-redirect=true";
	}
	
	public String actualizar(){
		
		ProgramaApService service=new ProgramaApService();
		
		try {
			
			if(fecProgramada!=null){
				programaAp.setFecProgramada(new java.sql.Timestamp(fecProgramada.getTime()));
			}else {
				programaAp.setFecProgramada(null);
			}
			
			service.update(programaAp);
			
					
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error desconocido.", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			e.printStackTrace();
		}
		
		ProgramaApxPeriodo();
		
		return "/zonasegura/alumbradoPublico/programaApListar?faces-redirect=true";
	}
	
	public String eliminar(){
		String programaApId=Utilidad.getParametro("programaApId");
		ProgramaApService service=new ProgramaApService();
		
		
		try {
			programaAp=service.ReadById(new Long(programaApId));
			programaAp.setEstado("0");
			
			for(MedicionAp medicionAp:programaAp.getMedicionAp()){
				medicionAp.setEstado("0");
			}
			
			service.update(programaAp);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		
		ProgramaApxPeriodo();
		
		return "/zonasegura/alumbradoPublico/programaApListar?faces-redirect=true";
	}
	
	public void cargarProgramaApActual(){
		String programaApId=Utilidad.getParametro("programaApId");
		//long programaApLong = 0 ;
		
	
		ProgramaApService service=new ProgramaApService();
		
		
		try {
			//programaAp=service.ReadById(programaApLong);
			programaAp=service.ReadById(new Long(programaApId));
			
			if(programaAp.getFecProgramada()!=null) {
				fecProgramada = new java.util.Date( programaAp.getFecProgramada().getTime());				
			}else {
				fecProgramada = null;
			}
			
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			throw new HibernateException("No se puede cargar ProgramaAp.", e);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		
	}
	
	public String ProgramaApxPeriodo() {
		ProgramaApService service=new ProgramaApService();
		
		viaFilter = null;
		tipoPuntoFilter = null;
		TipoServicioFilter = null;
		fecProgramadaFilter = null;
		localidadFilter = null;
		
		if(periodoMedicion.getPerMedicionId() > 0) {
			programasAp = service.getAllRows1(periodoMedicion.getPerMedicionId());
		}
		return "/zonasegura/alumbradoPublico/programaApListar?faces-redirect=true";
	}
	
	
	
	public void  filtroChanged(AjaxBehaviorEvent event) {  
		
		ProgramaApxPeriodo();
	}
	
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}


	public PeriodoMedicion getPeriodoMedicion() {
		return periodoMedicion;
	}


	public void setPeriodoMedicion(PeriodoMedicion periodoMedicion) {
		this.periodoMedicion = periodoMedicion;
	}





	public int getOrigen() {
		return origen;
	}


	public void setOrigen(int origen) {
		this.origen = origen;
	}


	

	public long getTipoSin() {
		return tipoSin;
	}


	public void setTipoSin(long tipoSin) {
		this.tipoSin = tipoSin;
	}


	public String getOperacion() {
		return operacion;
	}


	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getLocalidadFilter() {
		return localidadFilter;
	}


	public void setLocalidadFilter(String localidadFilter) {
		this.localidadFilter = localidadFilter;
	}

	
	public List<SelectItem> getItemsTiposervicio() {
		itemsTiposervicio=new ArrayList<SelectItem>();
		itemsTiposervicio.add(new SelectItem("U", "URBANO"));
		itemsTiposervicio.add(new SelectItem("UR", "URBANO RURAL"));
		itemsTiposervicio.add(new SelectItem("R", "RURAL"));
		return itemsTiposervicio;
	}


	public void setItemsTiposervicio(List<SelectItem> itemsTiposervicio) {
		this.itemsTiposervicio = itemsTiposervicio;
	}


	public List<SelectItem> getItemsTipoMedicion1() {
		TipoMedicionService service=new TipoMedicionService();
		itemsTipoMedicion1=new ArrayList<SelectItem>();
		for(TipoMedicion tp:service.getAllRows1()){
			itemsTipoMedicion1.add(new SelectItem(tp,tp.getCodTipMedicion()+"-"+tp.getDescripcion()));
		}
		return itemsTipoMedicion1;
	}


	public void setItemsTipoMedicion1(List<SelectItem> itemsTipoMedicion1) {
		this.itemsTipoMedicion1 = itemsTipoMedicion1;
	}

	
	public List<SelectItem> getItemsTipoPunto1() {
		TipoPuntoService service=new TipoPuntoService();
		itemsTipoPunto1=new ArrayList<SelectItem>();
		for(TipoPunto tp:service.getAllRows1()){
			itemsTipoPunto1.add(new SelectItem(tp,tp.getCodTipPunto()+"-"+tp.getDescripcion()));
		}
		
		return itemsTipoPunto1;
	}


	public void setItemsTipoPunto1(List<SelectItem> itemsTipoPunto1) {
		this.itemsTipoPunto1 = itemsTipoPunto1;
	}
	
	public List<SelectItem> getItemsVia() {
		ViaService service=new ViaService();
		

		itemsVia=new ArrayList<SelectItem>();
		
		if(programaAp.getLocalidad() != null){
			for(Via tp:service.getAllRows1(programaAp.getLocalidad().getLocalidadId())){
				itemsVia.add(new SelectItem(tp,tp.getCodVia()+"-"+tp.getNombre()));
			}
		}

		
		return itemsVia;
	}


	public void setItemsVia(List<SelectItem> itemsVia) {
		this.itemsVia = itemsVia;
	}

	public void localidadChanged(AjaxBehaviorEvent event){
		ViaService service=new ViaService();
		itemsVia=new ArrayList<SelectItem>();
		for(Via tp:service.getAllRows1(programaAp.getLocalidad().getLocalidadId())){
			itemsVia.add(new SelectItem(tp,tp.getCodVia()+"-"+tp.getNombre()));
			
		}
		
		if(!programaAp.getLocalidad().getSisElectrico().substring(0, 2).equals("SR")) {
			if(programaAp.getLocalidad().getSecTipico().equals("1") || 
					programaAp.getLocalidad().getSecTipico().equals("2") ||
					programaAp.getLocalidad().getSecTipico().equals("3")) {
				
				programaAp.setTipServicio("U");
			}
			
			if(programaAp.getLocalidad().getSecTipico().equals("4") || 
					programaAp.getLocalidad().getSecTipico().equals("5") ||
					programaAp.getLocalidad().getSecTipico().equals("6")) {
				
				programaAp.setTipServicio("R");
			}
		}else {			
			programaAp.setTipServicio("R");
		}
		
	}

	public void viaChanged(AjaxBehaviorEvent event){
		
		if(programaAp.getVia().getViaVano() != null) {
			programaAp.setNumVanos(new BigDecimal(programaAp.getVia().getViaVano().size()));
		}
	}	
	public java.util.Date getFecProgramada() {
		return fecProgramada;
	}


	public void setFecProgramada(java.util.Date fecProgramada) {
		this.fecProgramada = fecProgramada;
	}


	public String getViaFilter() {
		return viaFilter;
	}


	public void setViaFilter(String viaFilter) {
		this.viaFilter = viaFilter;
	}


	public Filter<?> getFilterLocalidad() {
        return new Filter<ProgramaAp>() {
            public boolean accept(ProgramaAp t) {
            	String localidad = getLocalidadFilter();
            	
                if (localidad == null) {
                	return true;
                }
                if (localidad.length() == 0) {
                	return true;
                }	
                if(localidad.length() > 0) {
                	if(t.getLocalidad() == null){
                		return false;
                	}
                	
                	if(localidad.equals(t.getLocalidad().getCodLocalidad())) {
                		return true;
                	}
                }
            	
                return false;
            }
        };
    }
	
	public Filter<?> getFilterVia() {
        return new Filter<ProgramaAp>() {
            public boolean accept(ProgramaAp t) {
            	String via = getViaFilter();
                if (via == null || via.length() == 0 || via.equals(t.getVia().getNombre())) {
                    return true;
                }
                return false;
            }
        };
    }

	public Filter<?> getFilterTipoPunto() {
        return new Filter<ProgramaAp>() {
            public boolean accept(ProgramaAp t) {
            	String tipoPunto = getTipoPuntoFilter();
                if (tipoPunto == null || tipoPunto.length() == 0 || tipoPunto.equals(t.getTipoPunto().getCodTipPunto())) {
                	 															
                    return true;
                }
                return false;
            }
        };
    }
	
	public Filter<?> getFilterTipoServicio() {
        return new Filter<ProgramaAp>() {
            public boolean accept(ProgramaAp t) {
            	String tipoServicio = getTipoServicioFilter();
                if (tipoServicio == null || tipoServicio.length() == 0 || tipoServicio.equals(t.getTipServicio())) {
                	 															
                    return true;
                }
                return false;
            }
        };
    }


	public String getTipoPuntoFilter() {
		return tipoPuntoFilter;
	}


	public void setTipoPuntoFilter(String tipoPuntoFilter) {
		this.tipoPuntoFilter = tipoPuntoFilter;
	}

	public String getTipoServicioFilter() {
		return TipoServicioFilter;
	}


	public void setTipoServicioFilter(String tipoServicioFilter) {
		TipoServicioFilter = tipoServicioFilter;
	}

	public List<SelectItem> getItemsTipoPunto() {
		TipoPuntoService service=new TipoPuntoService();
		itemsTipoPunto=new ArrayList<SelectItem>();
		itemsTipoPunto.add(new SelectItem("", ""));
		for(TipoPunto tp:service.getAllRows1()){
			itemsTipoPunto.add(new SelectItem(tp.getCodTipPunto(),tp.getCodTipPunto() + "-" +tp.getDescripcion()));
		}
		
		return itemsTipoPunto;
	}


	public void setItemsTipoPunto(List<SelectItem> itemsTipoPunto) {
		this.itemsTipoPunto = itemsTipoPunto;
	}


	public List<SelectItem> getItemsVia2() {
		ViaService service=new ViaService();
		itemsVia2=new ArrayList<SelectItem>();
		itemsVia2.add(new SelectItem("", ""));
		for(Via tp:service.getAllRows1()){
			itemsVia2.add(new SelectItem(tp.getCodVia(),tp.getCodVia()+"-"+tp.getNombre()));
		}
		return itemsVia2;
	}


	public void setItemsVia2(List<SelectItem> itemsVia2) {
		this.itemsVia2 = itemsVia2;
	}
	
	public List<SelectItem> getItemsTipoServicios() {
		itemsTipoServicios=new ArrayList<SelectItem>();
		itemsTipoServicios.add(new SelectItem("",""));
		itemsTipoServicios.add(new SelectItem("U","URBANO"));
		itemsTipoServicios.add(new SelectItem("R","RURAL"));
		itemsTipoServicios.add(new SelectItem("UR","URBANO-RURAL"));
		return itemsTipoServicios;
	}


	public void setItemsTipoServicios(List<SelectItem> itemsTipoServicios) {
		this.itemsTipoServicios = itemsTipoServicios;
	}
	
	
	
	public java.util.Date getFecProgramadaFilter() {
		return fecProgramadaFilter;
	}


	public void setFecProgramadaFilter(java.util.Date fecProgramadaFilter) {
		this.fecProgramadaFilter = fecProgramadaFilter;
	}


	public Filter<?> getFilterFecProgramada() {
        return new Filter<ProgramaAp>() {
            public boolean accept(ProgramaAp programaAp) {
                Date fechaProgramada = getFecProgramadaFilter();
                if (fechaProgramada == null) {
                	
                	return true;
                }else {
                	
                	Calendar calendar1 = Calendar.getInstance();
                	Calendar calendar2 = Calendar.getInstance();
                	
                	calendar1.setTime(fechaProgramada);
                	
                	if(programaAp.getFecProgramada() == null){
                		return false;
                	}
                	
                	calendar2.setTime(programaAp.getFecProgramada()); 
                	
                	if(calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR) && calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH) &&
                			calendar1.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH)){
                		
                		return true;
                		
                	}else{
                		return false;
                	}
                    
                }
            }
        };
	}	
	
	
	public String generarIdentificador(){	
		String result;
		
		ConectaDb db = new ConectaDb("CALIDAD");
		Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        try {
        	cstmt = cn.prepareCall("{CALL CAL_AP_PACKAGE.GENERAR_IDENTIFICADOR(?)}");
		   	cstmt.setLong(1, periodoMedicion.getPerMedicionId());
			
            int ctos = cstmt.executeUpdate();;
            cstmt.close(); 

            if (ctos == 0) {
                result = "0 filas afectadas";
            }

        } catch (SQLException e) {
            result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
            
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
                result = e.getMessage();
                
    			setMensaje(e.getMessage()+":"+e.getCause());
    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                
            }
        }			
		
        ProgramaApxPeriodo();
        
        return result = "/zonasegura/alumbradoPublico/programaApListar?faces-redirect=true";	
	}

	public String procesarAllMedicion(){
		for(ProgramaAp prog:programasAp){
			programaAp = prog;
			
			procesarMedicion();
			
		}
		
		return "/zonasegura/alumbradoPublico/programaApListar?faces-redirect=true";
	}
	public String procesarMedicion(){
		
		String result = null;
		ConectaDb db = new ConectaDb("CALIDAD");
		
        Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        try {
        	int ctos = 0;

					
					cstmt = cn.prepareCall("{CALL CAL_AP_PACKAGE.CALCULAR_LONGITUD_DEFICENTE(?,?)}"); 
					
					cstmt.setString(1, periodoMedicion.getAno());
					cstmt.setString(2, periodoMedicion.getSemestre());
					
					System.out.println("ANO:" + periodoMedicion.getAno());
					System.out.println("SEMESTRE:" + periodoMedicion.getSemestre());
					
					ctos = cstmt.executeUpdate();


            if (ctos == 0) {
                result = "0 filas afectadas";
            }
            
            ProgramaApxPeriodo();
            
        } catch (SQLException e) {
            result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
            
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
                result = e.getMessage();
    			setMensaje(e.getMessage()+":"+e.getCause());
    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                
            }
        }
		System.out.print(result);
		
		return "/zonasegura/alumbradoPublico/programaApListar?faces-redirect=true";
	}

}
