package pe.com.calidad.alumbradopublico.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.calidad.alumbradopublico.entity.Via;
import pe.com.calidad.alumbradopublico.service.ViaService;

@FacesConverter(value="viaConverter",forClass=ViaConverter.class)

public class ViaConverter implements Converter{

	@Override
	public Via getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long viaId=new Long(cadena);
		ViaService service=new ViaService();
		Via via=new Via();
		via=service.ReadById(viaId);
		System.out.println(via.getViaId());
		return via;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			Via via=(Via) objeto;
			////System.out.println("objeto:"+new Long(via.getViaId()).toString());
			return new Long(via.getViaId()).toString();
		}
		return null;
	}


}
