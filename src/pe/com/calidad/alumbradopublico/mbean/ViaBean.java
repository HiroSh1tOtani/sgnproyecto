package pe.com.calidad.alumbradopublico.mbean;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import org.hibernate.HibernateException;
import org.richfaces.model.Filter;

import pe.com.calidad.alumbradopublico.entity.MedicionAp;
import pe.com.calidad.alumbradopublico.entity.ProgramaAp;
import pe.com.calidad.alumbradopublico.entity.Via;//MedicionAp;
import pe.com.indra.calidad.entity.Localidad;//ProgramaAp;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.calidad.alumbradopublico.service.ProgramaApService;
import pe.com.calidad.alumbradopublico.service.ViaService;//MedicionApService;
import pe.com.indra.calidad.service.CuadrillaService;
import pe.com.indra.calidad.service.LocalidadService;//ProgramaApService;



import pe.com.indra.calidad.util.Utilidad;

@ManagedBean(name="viaBean")
@SessionScoped


public class ViaBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<Via> vias;
	private Via via;
	private long viaId;
	private Localidad localidad;
	private long programaApId;
	private ProgramaAp programaAp;
	private String mensaje;
	
	private String denominacionFilter;
	private List<SelectItem> itemsDenominacion;
	private List<SelectItem> itemsDenominacion1;
	
	private String zonaFilter;
	private List<SelectItem> itemsZona;
	private List<SelectItem> itemsZona1;
	
	private String tipApFilter;
	private List<SelectItem> itemsTipoAp;
	private List<SelectItem> itemsTipoAp1;
	private List<SelectItem> itemsTipVia;
	private List<SelectItem> itemsTipVia1;
	private List<SelectItem> itemsLocalidad1;
	private String tipViaFilter;
	private String localidadFilter;
	private List<SelectItem> itemsLocalidad2;
	private String nombreFilter;
	private String codViaFilter;
	private String ubigeoFilter;
	
	public ViaBean() {

	}
	
	@PostConstruct
	public void init(){
		via=new Via();
		via.setEstado("1");
		
		vias = new ArrayList<>();
		if(localidad != null) {
			ViaService service=new ViaService();
			vias = service.getAllRows1(localidad.getLocalidadId());
		}
	}	
	
	/*public String mostrarVias(){
		ViaService service = new ViaService();
		String programaApIdStr = Utilidad.getParametro("programaApId");
		
		if(programaApIdStr != null){
			programaApId = Long.valueOf(programaApIdStr);
			
			ProgramaApService programaApService = new ProgramaApService();
			
			setProgramaAp(programaApService.ReadById(programaApId));
			
			vias = (List<Via>) service.getAllRows1(Long.valueOf(programaApIdStr));
		}else {
			vias = (List<Via>) service.getAllRows1(programaApId);
		}
		
		
		
		return "/zonasegura/alumbradoPublico/viaListar?faces-redirect=true";
	}	*/
	
	
	public void cargarViaActual(){
		
		String viaId=Utilidad.getParametro("viaId");
		ViaService service=new ViaService();
		
		try {
			
			via=service.ReadById(new Long(viaId));
			
			
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacci�n", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			e.printStackTrace();
			
			throw new HibernateException("No se puede cargar Via.", e);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Ocurrio un error", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			e.printStackTrace();
		}
		
	}
	
	public String prepararCrear(){
		//Esto funciona siempre que el bean tenga ambito de sesion 
		setVia(new Via());
		getVia().setEstado("1");
		//getVia().setLocalidad(localidad);
		
		return "/zonasegura/alumbradoPublico/viaCrear?faces-redirect=true";
	}
	
	public String prepararEditar(){
		
		
		try {
			cargarViaActual();
			return "/zonasegura/alumbradoPublico/viaEditar?faces-redirect=true";
		} catch (HibernateException e) {
			setMensaje(e.getMessage());
			return "/zonasegura/alumbradoPublico/viaListar?faces-redirect=true";
		}
	
		
	}
	
	public String prepararVer(){
			
		try {
			cargarViaActual();
			return "/zonasegura/alumbradoPublico/viaVer?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/alumbradoPublico/viaListar?faces-redirect=true";
		}
	}	
	
	public String salvar(){
		
		ViaService service=new ViaService();
		
		try {
			
			service.create(via);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacci�n", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

			return "/zonasegura/alumbradoPublico/viaCrear?faces-redirect=true";
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacci�n", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			return "/zonasegura/alumbradoPublico/viaCrear?faces-redirect=true";
		}
		
		vias = service.getAllRows1(localidad.getLocalidadId());
		
		return "/zonasegura/alumbradoPublico/viaListar?faces-redirect=true";
	}
	
	public String actualizar(){
	
		ViaService service=new ViaService();
		
		try {
			
			service.update(via);
			
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacci�n", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			return "/zonasegura/alumbradoPublico/viaEditar?faces-redirect=true";
			
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacci�n", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			return "/zonasegura/alumbradoPublico/viaEditar?faces-redirect=true";
		}
		
		vias = service.getAllRows1(localidad.getLocalidadId());
		
		return "/zonasegura/alumbradoPublico/viaListar?faces-redirect=true";
	}
	
	public String eliminar(){
		String viaId=Utilidad.getParametro("viaId");
		ViaService service=new ViaService();

		
		try {
			via=service.ReadById(new Long(viaId));
			via.setEstado("0");
			service.update(via);
		} catch (HibernateException e) {
			
			
			System.out.println(e.getMessage()+":"+e.getCause());
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacci�n", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			
		} catch (Exception e) {
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacci�n", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		
		vias = service.getAllRows1(localidad.getLocalidadId());
		
		return "/zonasegura/alumbradoPublico/viaListar?faces-redirect=true";
	}
	
	public void  filtroChanged(AjaxBehaviorEvent event) {  
		
		viaxLocalidad();
	}
	
	public String viaxLocalidad() {
		ViaService service=new ViaService();
		
		denominacionFilter = null;
		zonaFilter = null;
		tipApFilter = null;
		tipViaFilter = null;
		localidadFilter = null;
		nombreFilter = null;
		codViaFilter = null;
		ubigeoFilter = null;
		
		if(localidad != null) {
			vias = service.getAllRows1(localidad.getLocalidadId());
		}else{
			vias.clear();
		}
		
		return "/zonasegura/alumbradoPublico/viaListar?faces-redirect=true";
	}
	
	public List<Via> getVias() {
		return vias;
	}
	public void setVias(List<Via> vias) {
		this.vias = vias;
	}
	
	public Via getVia() {
		return via;
	}
	public void setVia(Via via) {
		this.via = via;
	}
	public long getProgramaApId() {
		return programaApId;
	}
	public void setProgramaApId(long programaApId) {
		this.programaApId = programaApId;
	}

	public ProgramaAp getProgramaAp() {
		return programaAp;
	}

	public void setProgramaAp(ProgramaAp programaAp) {
		this.programaAp = programaAp;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public long getViaId() {
		return viaId;
	}

	public void setViaId(long viaId) {
		this.viaId = viaId;
	}

	public Localidad getLocalidad() {
		return localidad;
	}

	public void setLocalidad(Localidad localidad) {
		this.localidad = localidad;
	}

	public String getDenominacionFilter() {
		return denominacionFilter;
	}

	public void setDenominacionFilter(String denominacionFilter) {
		this.denominacionFilter = denominacionFilter;
	}

	public List<SelectItem> getItemsDenominacion() {
		itemsDenominacion=new ArrayList<SelectItem>();
		itemsDenominacion.add(new SelectItem("", ""));
		itemsDenominacion.add(new SelectItem("JR","JIR�N"));
		itemsDenominacion.add(new SelectItem("AV","AVENIDA"));
		itemsDenominacion.add(new SelectItem("VE","V�A EXPRESA"));
		itemsDenominacion.add(new SelectItem("AU","AUTOPISTA"));
		itemsDenominacion.add(new SelectItem("CA","CALLE"));
		itemsDenominacion.add(new SelectItem("CR", "CARRETERA"));
		itemsDenominacion.add(new SelectItem("PS", "PASAJE"));
		itemsDenominacion.add(new SelectItem("OV", "�VALO"));
		itemsDenominacion.add(new SelectItem("MA", "MALEC�N"));
		itemsDenominacion.add(new SelectItem("AL", "ALAMEDA"));
		return itemsDenominacion;
	}

	public void setItemsDenominacion(List<SelectItem> itemsDenominacion) {
		this.itemsDenominacion = itemsDenominacion;
	}

	public Filter<?> getFilterDenominacion() {
        return new Filter<Via>() {
            public boolean accept(Via t) {
            	String denominacion = getDenominacionFilter();
                if (denominacion == null || denominacion.length() == 0 || denominacion.equals(t.getDenominacion())) {
                    return true;
                }
                return false;
            }
        };
    }

	public String getZonaFilter() {
		return zonaFilter;
	}

	public void setZonaFilter(String zonaFilter) {
		this.zonaFilter = zonaFilter;
	}

	public List<SelectItem> getItemsZona() {
		itemsZona=new ArrayList<SelectItem>();
		itemsZona.add(new SelectItem("", ""));
		itemsZona.add(new SelectItem("ST1","ST1"));
		itemsZona.add(new SelectItem("ST2","ST2"));
		itemsZona.add(new SelectItem("ST3","ST3"));
		itemsZona.add(new SelectItem("ST4","ST4"));
		itemsZona.add(new SelectItem("ST5","ST5"));
		itemsZona.add(new SelectItem("ST6","ST6"));
		return itemsZona;
	}

	public void setItemsZona(List<SelectItem> itemsZona) {
		this.itemsZona = itemsZona;
	}

	public Filter<?> getFilterZona() {
        return new Filter<Via>() {
            public boolean accept(Via t) {
            	String zona = getZonaFilter();
                if (zona == null || zona.length() == 0 || zona.equals(t.getClaZona())) {
                    return true;
                }
                return false;
            }
        };
    }

	public String getTipApFilter() {
		return tipApFilter;
	}

	public void setTipApFilter(String tipApFilter) {
		this.tipApFilter = tipApFilter;
	}

	public List<SelectItem> getItemsTipoAp() {
		itemsTipoAp=new ArrayList<SelectItem>();
		itemsTipoAp.add(new SelectItem("", ""));
		itemsTipoAp.add(new SelectItem("I","TIPO I"));
		itemsTipoAp.add(new SelectItem("II","TIPO II"));
		itemsTipoAp.add(new SelectItem("III","TIPO III"));
		itemsTipoAp.add(new SelectItem("IV","TIPO IV"));
		itemsTipoAp.add(new SelectItem("V","TIPO V"));
		return itemsTipoAp;
	}

	public void setItemsTipoAp(List<SelectItem> itemsTipoAp) {
		this.itemsTipoAp = itemsTipoAp;
	}
	
	public Filter<?> getFilterTipAp() {
        return new Filter<Via>() {
            public boolean accept(Via t) {
            	String tipAP = getTipApFilter();
                if (tipAP == null || tipAP.length() == 0 || tipAP.equals(t.getTipAp())) {
                    return true;
                }
                return false;
            }
        };
    }

	public List<SelectItem> getItemsDenominacion1() {
		itemsDenominacion1=new ArrayList<SelectItem>();
		itemsDenominacion1.add(new SelectItem("JR","JIR�N"));
		itemsDenominacion1.add(new SelectItem("AV","AVENIDA"));
		itemsDenominacion1.add(new SelectItem("VE","V�A EXPRESA"));
		itemsDenominacion1.add(new SelectItem("AU","AUTOPISTA"));
		itemsDenominacion1.add(new SelectItem("CA","CALLE"));
		itemsDenominacion1.add(new SelectItem("CR", "CARRETERA"));
		itemsDenominacion1.add(new SelectItem("PS", "PASAJE"));
		itemsDenominacion1.add(new SelectItem("OV", "�VALO"));
		itemsDenominacion1.add(new SelectItem("MA", "MALEC�N"));
		itemsDenominacion1.add(new SelectItem("AL", "ALAMEDA"));
		return itemsDenominacion1;
	}

	public void setItemsDenominacion1(List<SelectItem> itemsDenominacion1) {
		this.itemsDenominacion1 = itemsDenominacion1;
	}

	public List<SelectItem> getItemsZona1() {
		itemsZona1=new ArrayList<SelectItem>();
		itemsZona1.add(new SelectItem("ST1","ST1"));
		itemsZona1.add(new SelectItem("ST2","ST2"));
		itemsZona1.add(new SelectItem("ST3","ST3"));
		itemsZona1.add(new SelectItem("ST4","ST4"));
		itemsZona1.add(new SelectItem("ST5","ST5"));
		itemsZona1.add(new SelectItem("ST6","ST6"));
		return itemsZona1;
	}

	public void setItemsZona1(List<SelectItem> itemsZona1) {
		this.itemsZona1 = itemsZona1;
	}

	public List<SelectItem> getItemsTipoAp1() {
		itemsTipoAp1=new ArrayList<SelectItem>();
		itemsTipoAp1.add(new SelectItem("I","TIPO I"));
		itemsTipoAp1.add(new SelectItem("II","TIPO II"));
		itemsTipoAp1.add(new SelectItem("III","TIPO III"));
		itemsTipoAp1.add(new SelectItem("IV","TIPO IV"));
		itemsTipoAp1.add(new SelectItem("V","TIPO V"));
		return itemsTipoAp1;
	}

	public void setItemsTipoAp1(List<SelectItem> itemsTipoAp1) {
		this.itemsTipoAp1 = itemsTipoAp1;
	}

	public List<SelectItem> getItemsTipVia1() {
		itemsTipVia1=new ArrayList<SelectItem>();
		itemsTipVia1.add(new SelectItem("RE","REGIONAL"));
		itemsTipVia1.add(new SelectItem("SR","SUBREGIONAL"));
		itemsTipVia1.add(new SelectItem("EX","EXPRESA"));
		itemsTipVia1.add(new SelectItem("AR","ARTERIAL"));
		itemsTipVia1.add(new SelectItem("CO","COLECTORA"));
		itemsTipVia1.add(new SelectItem("LR","LOCAL RESIDENCIAL"));
		itemsTipVia1.add(new SelectItem("LC","LOCAL COMERCIAL"));
		itemsTipVia1.add(new SelectItem("LU","LOCAL RURAL"));
		itemsTipVia1.add(new SelectItem("PP","PASAJE PEATONAL Y OTROS"));
		itemsTipVia1.add(new SelectItem("L1","LOCAL RESIDENCIAL 1"));
		itemsTipVia1.add(new SelectItem("L2","LOCAL RESIDENCIAL 2"));
		return itemsTipVia1;
	}

	public void setItemsTipVia1(List<SelectItem> itemsTipVia1) {
		this.itemsTipVia1 = itemsTipVia1;
	}

	public List<SelectItem> getItemsLocalidad1() {

		LocalidadService service=new LocalidadService();
		itemsLocalidad1=new ArrayList<SelectItem>();
		for(Localidad tp:service.getAllRows()){
			itemsLocalidad1.add(new SelectItem(tp,tp.getCodLocalidad()+"-"+tp.getNombre()));
		}
		return itemsLocalidad1;
		
	}

	public void setItemsLocalidad1(List<SelectItem> itemsLocalidad1) {
		this.itemsLocalidad1 = itemsLocalidad1;
	}

	public String getTipViaFilter() {
		return tipViaFilter;
	}

	public void setTipViaFilter(String tipViaFilter) {
		this.tipViaFilter = tipViaFilter;
	}
	
	public Filter<?> getFilterTipVia() {
        return new Filter<Via>() {
            public boolean accept(Via t) {
            	String tipVia = getTipViaFilter();
                if (tipVia == null || tipVia.length() == 0 || tipVia.equals(t.getTipVia())) {
                    return true;
                }
                return false;
            }
        };
    }

	public List<SelectItem> getItemsTipVia() {
		itemsTipVia=new ArrayList<SelectItem>();
		itemsTipVia.add(new SelectItem("",""));
		itemsTipVia.add(new SelectItem("RE","REGIONAL"));
		itemsTipVia.add(new SelectItem("SR","SUBREGIONAL"));
		itemsTipVia.add(new SelectItem("EX","EXPRESA"));
		itemsTipVia.add(new SelectItem("AR","ARTERIAL"));
		itemsTipVia.add(new SelectItem("CO","COLECTORA"));
		itemsTipVia.add(new SelectItem("LR","LOCAL RESIDENCIAL"));
		itemsTipVia.add(new SelectItem("LC","LOCAL COMERCIAL"));
		itemsTipVia.add(new SelectItem("LU","LOCAL RURAL"));
		itemsTipVia.add(new SelectItem("PP","PASAJE PEATONAL Y OTROS"));
		itemsTipVia.add(new SelectItem("L1","LOCAL RESIDENCIAL 1"));
		itemsTipVia.add(new SelectItem("L2","LOCAL RESIDENCIAL 2"));
		return itemsTipVia;
	}

	public void setItemsTipVia(List<SelectItem> itemsTipVia) {
		this.itemsTipVia = itemsTipVia;
	}

	public String getLocalidadFilter() {
		return localidadFilter;
	}

	public void setLocalidadFilter(String localidadFilter) {
		this.localidadFilter = localidadFilter;
	}
	
	public Filter<?> getFilterLocalidad() {
        return new Filter<Via>() {
            public boolean accept(Via t) {
            	String localidad = getLocalidadFilter();
            	
                if (localidad == null) {
                	return true;
                }
                if (localidad.length() == 0) {
                	return true;
                }	
                if(localidad.length() > 0) {
                	if(t.getLocalidad() == null){
                		return false;
                	}
                	
                	if(localidad.equals(t.getLocalidad().getCodLocalidad())) {
                		return true;
                	}
                }
            	
                return false;
            }
        };
    }

	public List<SelectItem> getItemsLocalidad2() {
		LocalidadService service=new LocalidadService();
		itemsLocalidad2=new ArrayList<SelectItem>();
		itemsLocalidad2.add(new SelectItem("", ""));
		for(Localidad tp:service.getAllRows()){
			itemsLocalidad2.add(new SelectItem(tp.getCodLocalidad(),tp.getCodLocalidad()+"-"+tp.getNombre()));
			
		}
		return itemsLocalidad2;
		
	}

	public void setItemsLocalidad2(List<SelectItem> itemsLocalidad2) {
		this.itemsLocalidad2 = itemsLocalidad2;
	}

	
	public String getNombreFilter() {
		return nombreFilter;
	}

	public void setNombreFilter(String nombreFilter) {
		this.nombreFilter = nombreFilter;
	}

	public String getCodViaFilter() {
		return codViaFilter;
	}

	public void setCodViaFilter(String codViaFilter) {
		this.codViaFilter = codViaFilter;
	}

	public String getUbigeoFilter() {
		return ubigeoFilter;
	}

	public void setUbigeoFilter(String ubigeoFilter) {
		this.ubigeoFilter = ubigeoFilter;
	}
}
