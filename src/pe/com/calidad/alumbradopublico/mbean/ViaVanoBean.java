package pe.com.calidad.alumbradopublico.mbean;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.hibernate.HibernateException;
import org.richfaces.model.Filter;

import pe.com.calidad.alumbradopublico.entity.MedicionAp;
import pe.com.calidad.alumbradopublico.entity.Via;
import pe.com.calidad.alumbradopublico.entity.ViaVano;


import pe.com.calidad.alumbradopublico.service.ViaService;
import pe.com.calidad.alumbradopublico.service.ViaVanoService;




import pe.com.calidad.suministro.entity.SuministroAfectado;
import pe.com.calidad.suministro.service.InterrupcionService;
import pe.com.calidad.suministro.service.SuministroAfectadoService;
import pe.com.indra.calidad.entity.Localidad;
import pe.com.indra.calidad.service.LocalidadService;
import pe.com.indra.calidad.util.Utilidad;

@ManagedBean(name="viaVanoBean")
@SessionScoped
public class ViaVanoBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<ViaVano> viaVanos;
	private ViaVano viaVano;
	private long viaVanoId;
	private long viaId;
	private Via via;
	private String mensaje;
	
	private String numVanoFilter;
	private List<SelectItem> itemsTipCalzada;
	private List<SelectItem> itemsVia1;
	private List<SelectItem> itemsTipAp1;
	
	public ViaVanoBean() {

	}
	
	@PostConstruct
	public void init(){
		viaVano=new ViaVano();
		viaVano.setEstado("1");
		
	}	
	
	public Via getVia() {
		return via;
	}
	public void setVia(Via via) {
		this.via = via;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public long getViaId() {
		return viaId;
	}

	public void setViaId(long viaId) {
		this.viaId = viaId;
	}


	public ViaVano getViaVano() {
		return viaVano;
	}

	public void setViaVano(ViaVano viaVano) {
		this.viaVano = viaVano;
	}

	public long getViaVanoId() {
		return viaVanoId;
	}

	public void setViaVanoId(long viaVanoId) {
		this.viaVanoId = viaVanoId;
	}

	public List<ViaVano> getViaVanos() {
		return viaVanos;
	}

	public void setViaVanos(List<ViaVano> viaVanos) {
		this.viaVanos = viaVanos;
	}

	
	public String mostrarViaVanos(){
		ViaVanoService service = new ViaVanoService();
		String viaIdStr = Utilidad.getParametro("viaId");
		
		if(viaIdStr != null){
			viaId = Long.valueOf(viaIdStr);
			
			ViaService viaService = new ViaService();
			
			via = viaService.ReadById(viaId);
			
			viaVanos = (List<ViaVano>) service.getAllRows1(Long.valueOf(viaIdStr));
		}else {
			viaVanos = (List<ViaVano>) service.getAllRows1(viaId);
		}
			
		return "/zonasegura/alumbradoPublico/viaVanoListar?faces-redirect=true";
	}
	
	public void cargarViaVanoActual(){
		
		String viaVanoId=Utilidad.getParametro("viaVanoId");
		ViaVanoService service=new ViaVanoService();
		
		try {
			
			viaVano=service.ReadById(new Long(viaVanoId));
			
			
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			e.printStackTrace();
			
			throw new HibernateException("No se puede cargar ViaVano.", e);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Ocurrio un error", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			e.printStackTrace();
		}
		
	}
	
	public String prepararCrear(){
		//Esto funciona siempre que el bean tenga ambito de sesion 
		setViaVano(new ViaVano());
		getViaVano().setEstado("1");
		getViaVano().setVia(via);
		getViaVano().setTipAp(via.getTipAp());
		getViaVano().setCanales(via.getCanales());
		getViaVano().setLongitud(new BigDecimal(33.33));
		getViaVano().setTipCalzada("O");
		return "/zonasegura/alumbradoPublico/viaVanoCrear?faces-redirect=true";
	}
	
	public String prepararEditar(){
		
		
		try {
			cargarViaVanoActual();
			return "/zonasegura/alumbradoPublico/viaVanoEditar?faces-redirect=true";
		} catch (HibernateException e) {
			setMensaje(e.getMessage());
			return "/zonasegura/alumbradoPublico/viaVanoListar?faces-redirect=true";
		}
	
		
	}
	
	public String prepararVer(){
			
		try {
			cargarViaVanoActual();
			return "/zonasegura/alumbradoPublico/viaVanoVer?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/alumbradoPublico/viaVanoListar?faces-redirect=true";
		}
	}	
	
	public String salvar(){
		
		ViaVanoService service=new ViaVanoService();
		
		try {
			
			service.create(viaVano);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

			return "/zonasegura/alumbradoPublico/viaVanoCrear?faces-redirect=true";
			
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			return "/zonasegura/alumbradoPublico/viaVanoCrear?faces-redirect=true";
		}
		
		
		
		return "/zonasegura/alumbradoPublico/viaVanoListar?faces-redirect=true";
	}
	
	public String actualizar(){
	
		ViaVanoService service=new ViaVanoService();
		
		try {
			
			service.update(viaVano);
			
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			return "/zonasegura/alumbradoPublico/viaVanoEditar?faces-redirect=true";
			
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			return "/zonasegura/alumbradoPublico/viaVanoEditar?faces-redirect=true";
		}
		
		
		
		return "/zonasegura/alumbradoPublico/viaVanoListar?faces-redirect=true";
	}
	
	public String eliminar(){
		String viaVanoId=Utilidad.getParametro("viaVanoId");
		ViaVanoService service=new ViaVanoService();

		
		try {
			viaVano=service.ReadById(new Long(viaVanoId));
			viaVano.setEstado("0");
			service.update(viaVano);
		} catch (HibernateException e) {
			
			
			System.out.println(e.getMessage()+":"+e.getCause());
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			
		} catch (Exception e) {
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		
		return "/zonasegura/alumbradoPublico/viaVanoListar?faces-redirect=true";
	}

	public String getNumVanoFilter() {
		return numVanoFilter;
	}

	public void setNumVanoFilter(String numVanoFilter) {
		this.numVanoFilter = numVanoFilter;
	}

	public List<SelectItem> getItemsTipCalzada() {
		itemsTipCalzada=new ArrayList<SelectItem>();
		itemsTipCalzada.add(new SelectItem("C","CLARA"));
		itemsTipCalzada.add(new SelectItem("O","OSCURA"));
		return itemsTipCalzada;
	}

	public void setItemsTipCalzada(List<SelectItem> itemsTipCalzada) {
		this.itemsTipCalzada = itemsTipCalzada;
	}

	public List<SelectItem> getItemsVia1() {
		ViaService service=new ViaService();
		itemsVia1=new ArrayList<SelectItem>();
		for(Via tp:service.getAllRows()){
			itemsVia1.add(new SelectItem(tp,tp.getCodVia()+"-"+tp.getNombre()));
		}
			return itemsVia1;
	}

	public void setItemsVia1(List<SelectItem> itemsVia1) {
		this.itemsVia1 = itemsVia1;
	}

	public List<SelectItem> getItemsTipAp1() {
		itemsTipAp1=new ArrayList<SelectItem>();
		itemsTipAp1.add(new SelectItem("I","TIPO I"));
		itemsTipAp1.add(new SelectItem("II","TIPO II"));
		itemsTipAp1.add(new SelectItem("III","TIPO III"));
		itemsTipAp1.add(new SelectItem("IV","TIPO IV"));
		itemsTipAp1.add(new SelectItem("V","TIPO V"));
		return itemsTipAp1;
	}

	public void setItemsTipAp1(List<SelectItem> itemsTipAp1) {
		this.itemsTipAp1 = itemsTipAp1;
	}	
	

}
