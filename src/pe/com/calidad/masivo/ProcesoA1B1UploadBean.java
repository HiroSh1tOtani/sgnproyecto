package pe.com.calidad.masivo;

import java.io.Serializable;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

import pe.com.indra.calidad.mbean.FileUtilUploadBean;

@ManagedBean(name="procesoA1B1UploadBean")
@SessionScoped
public class ProcesoA1B1UploadBean extends FileUtilUploadBean implements Serializable {
	
	
		public ProcesoA1B1UploadBean(){
			filaInicial = 2;
			numColumnas = 12;
			columnas = "1,2,3";
			tipoProceso = "3";
			
			nombreColumnas = "NUMSUMINISTRO,FECSUBSANACION,ESTADOSUBSANACION";
			
			itemsNombreColumnas=new ArrayList<SelectItem>();
			itemsNombreColumnas.add(new SelectItem("1",nombreColumnas));
			
			itemsSeparadorColumnas=new ArrayList<SelectItem>();
			itemsSeparadorColumnas.add(new SelectItem("\t","TAB"));
			
			itemsFormatoFecha=new ArrayList<SelectItem>();
			itemsFormatoFecha.add(new SelectItem("YYYY-MM-DD","YYYY-MM-DD"));
			//itemsFormatoFecha.add(new SelectItem("DD/MM/YYYY","DD/MM/YYYY"));
			
			columnas = "1,2,3";
			
			this.nombreArchivoBase = "procesoA1B1";
		}
		

		public String procesar() throws Exception{
			registrarCsv();
		    return "/zonasegura/interrupcion/interrupcionListar?faces-redirect=true"; //retornar a la pagina que deseas por Ejemplo a medicionListar.xhtml donde esta el boton
		}	

		public String prepararMostrarCarga(){
			return "/zonasegura/masivo/cargaMasivaA1B1?faces-redirect=true";
		}
	


}
