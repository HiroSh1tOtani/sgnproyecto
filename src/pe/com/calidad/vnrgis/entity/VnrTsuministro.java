package pe.com.calidad.vnrgis.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;



@Entity
@Table(name="CAL_VNR_TSUMINISTRO")
@SequenceGenerator(name = "VnrTsuministroIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "VnrTsuministroSeq")
public class VnrTsuministro implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long vnrTsuministroId;
	private String slabel;
	private String plabel;
	private String estado;
	private Timestamp fechaservicio;
	private Timestamp fecharetiro;
	private String sed;

	
	public VnrTsuministro() {

	}

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "VnrTsuministroIdGenerator")
    @Column(name = "VNR_TSUMINISTRO_ID",nullable=false,unique=true)
	public long getVnrTsuministroId() {
		return vnrTsuministroId;
	}

	public void setVnrTsuministroId(long vnrTsuministroId) {
		this.vnrTsuministroId = vnrTsuministroId;
	}

	@Column(name="SLABEL",nullable=true)
	public String getSlabel() {
		return slabel;
	}

	public void setSlabel(String slabel) {
		this.slabel = slabel;
	}

	@Column(name="PLABEL",nullable=true)
	public String getPlabel() {
		return plabel;
	}

	public void setPlabel(String plabel) {
		this.plabel = plabel;
	}

	@Column(name="ESTADO",nullable=true)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Column(name="FECHASERVICIO",nullable=true)
	public Timestamp getFechaservicio() {
		return fechaservicio;
	}

	public void setFechaservicio(Timestamp fechaservicio) {
		this.fechaservicio = fechaservicio;
	}

	@Column(name="FECHARETIRO",nullable=true)
	public Timestamp getFecharetiro() {
		return fecharetiro;
	}

	public void setFecharetiro(Timestamp fecharetiro) {
		this.fecharetiro = fecharetiro;
	}

	@Column(name="SED",nullable=true)
	public String getSed() {
		return sed;
	}

	public void setSed(String sed) {
		this.sed = sed;
	}
	
	public String toString(){
		return new Long(this.getVnrTsuministroId()).toString();
	}

	@Override
	public boolean equals(Object obj) {
		if(this.getVnrTsuministroId()==((VnrTsuministro)obj).getVnrTsuministroId()){
			return true;
		}else{
			return false;
		}
	}
	

}
