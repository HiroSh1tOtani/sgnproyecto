package pe.com.calidad.vnrgis.dao;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import java.util.List;


import pe.com.calidad.vnrgis.entity.VnrTsuministro;//Metas;
import pe.com.indra.calidad.dao.HibernateUtil;



/**
 *
 * @author Luis
 */
public class VnrTsuministroDaoImpl implements VnrTsuministroDao {

    private static VnrTsuministroDaoImpl instance = null;

    public static VnrTsuministroDaoImpl getInstance() {
        if (instance == null) {
            instance = new VnrTsuministroDaoImpl();
        }

        return instance;
    }

    public VnrTsuministroDaoImpl() {
    }

	@Override
	public void create(VnrTsuministro vnrTsuministro) {

        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(vnrTsuministro);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear VnrTsuministro.", e);
        }
	}

	@Override
	public VnrTsuministro ReadById(long vnrTsuministroId) {
		VnrTsuministro vnrTsuministro = null;
        try {
            HibernateUtil.begin();

            vnrTsuministro = (VnrTsuministro) HibernateUtil.getSession().load(VnrTsuministro.class, vnrTsuministroId);

            Hibernate.initialize(vnrTsuministro);
         
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar VnrTsuministro.", e);
        }
        return vnrTsuministro;
	}

	@Override
	public void update(VnrTsuministro vnrTsuministro) {
	    try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(vnrTsuministro);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar VnrTsuministro.", e);
        }
		
	}

	@Override
	public void delete(long vnrTsuministroId) {
		VnrTsuministro vnrTsuministro = null;
		vnrTsuministro = ReadById(vnrTsuministroId);

        if (vnrTsuministro != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(vnrTsuministro);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar VnrTsuministro.", e);
            }
        }
	}

	@Override
	public List<VnrTsuministro> getAllRows() {
		List<VnrTsuministro> vnrTsuministro=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from VnrTsuministro");
	            
	            vnrTsuministro=(List<VnrTsuministro>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar VnrTsuministro .", e);
	        }
		return vnrTsuministro;
	}

}
