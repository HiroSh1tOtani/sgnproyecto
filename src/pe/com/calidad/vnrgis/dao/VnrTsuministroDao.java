
package pe.com.calidad.vnrgis.dao;

import java.util.List;


import pe.com.calidad.vnrgis.entity.VnrTsuministro;

/**
 *
 * @author Lucho
 */
public interface VnrTsuministroDao {
    public void create(VnrTsuministro vnrTsuministro);
    public VnrTsuministro ReadById(long vnrTsuministroId);
	public void update(VnrTsuministro vnrTsuministro);
	public void delete(long vnrTsuministroId);
   	public List<VnrTsuministro> getAllRows();
	
        
}
