package pe.com.calidad.vnrgis.service;

import java.util.List;

import org.hibernate.HibernateException;

import pe.com.calidad.vnrgis.dao.VnrTsuministroDao;
import pe.com.calidad.vnrgis.dao.VnrTsuministroDaoImpl;
import pe.com.calidad.vnrgis.entity.VnrTsuministro;


/**
 *
 * @author Luis
 */
public class VnrTsuministroService {

    public void create(VnrTsuministro vnrTsuministro) {
    	VnrTsuministroDao dao = new VnrTsuministroDaoImpl();
        
        try {
        	dao.create(vnrTsuministro);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear VnrTsuministro.", e);
		}
        
    }

    public VnrTsuministro ReadById(long vnrTsuministroId) {
    	VnrTsuministroDao dao = new VnrTsuministroDaoImpl();
    	VnrTsuministro vnrTsuministro = null;
        
        try {
        	vnrTsuministro = dao.ReadById(vnrTsuministroId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); 
		}
        
        return vnrTsuministro;
    }

    public void update(VnrTsuministro vnrTsuministro) {
    	VnrTsuministroDao dao = new VnrTsuministroDaoImpl();
        
        try {
        	dao.update(vnrTsuministro);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public void delete(long vnrTsuministroId) {
    	VnrTsuministroDao dao = new VnrTsuministroDaoImpl();
        
        try {
        	dao.delete(vnrTsuministroId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

     
    public List<VnrTsuministro> getAllRows(){
    	VnrTsuministroDao dao = new VnrTsuministroDaoImpl();
    	return dao.getAllRows();
    }  
    
}
