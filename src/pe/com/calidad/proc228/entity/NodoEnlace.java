package pe.com.calidad.proc228.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CAL_NODO_ENLACE")
@SequenceGenerator(name = "NodEnlaceIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "NodoEnlaceSeq")
public class NodoEnlace  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long nodEnlaceId;
	private String codNodo;
	private String codTramoMt;
	private String codSed;
	private BigDecimal utmEste;
	private BigDecimal utmNorte;
	
	public NodoEnlace() {

	}

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NodEnlaceIdGenerator")
    @Column(name = "NOD_ENLACE_ID",nullable=false,unique=true)
	public long getNodEnlaceId() {
		return nodEnlaceId;
	}

	public void setNodEnlaceId(long nodEnlaceId) {
		this.nodEnlaceId = nodEnlaceId;
	}

	@Column(name="COD_NODO",nullable=true)
	public String getCodNodo() {
		return codNodo;
	}

	public void setCodNodo(String codNodo) {
		this.codNodo = codNodo;
	}

	@Column(name="COD_TRAMO_MT",nullable=true)
	public String getCodTramoMt() {
		return codTramoMt;
	}

	public void setCodTramoMt(String codTramoMt) {
		this.codTramoMt = codTramoMt;
	}

	@Column(name="COD_SED",nullable=true)
	public String getCodSed() {
		return codSed;
	}

	public void setCodSed(String codSed) {
		this.codSed = codSed;
	}

	@Column(name="UTM_ESTE",nullable=true)
	public BigDecimal getUtmEste() {
		return utmEste;
	}

	public void setUtmEste(BigDecimal utmEste) {
		this.utmEste = utmEste;
	}

	@Column(name="UTM_NORTE",nullable=true)
	public BigDecimal getUtmNorte() {
		return utmNorte;
	}

	public void setUtmNorte(BigDecimal utmNorte) {
		this.utmNorte = utmNorte;
	}

	public String toString(){
		return new Long(this.getNodEnlaceId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getNodEnlaceId()==((NodoEnlace)obj).getNodEnlaceId()){
			return true;
		}else{
			return false;
		}
	}
	
}
