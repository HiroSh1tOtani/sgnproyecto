package pe.com.calidad.proc228.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CAL_SED")
@SequenceGenerator(name = "SedIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "SedSeq")
public class Sed  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long sedId;
	private String ubigeo;
	private String codAlimentadorMt;
	private String codSed;
	private String nombre;
	private String codInstalacion;
	private String propietario;
	private BigDecimal tenNomBt;
	private BigDecimal tenNomMt;
	private BigDecimal capTransformacion;
	private BigDecimal utmEste;
	private BigDecimal utmNorte;
	
	public Sed() {

	}

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SedIdGenerator")
    @Column(name = "SED_ID",nullable=false,unique=true)
	public long getSedId() {
		return sedId;
	}

	public void setSedId(long sedId) {
		this.sedId = sedId;
	}

	@Column(name="UBIGEO",nullable=true)
	public String getUbigeo() {
		return ubigeo;
	}

	public void setUbigeo(String ubigeo) {
		this.ubigeo = ubigeo;
	}

	@Column(name="COD_ALIMENTADOR_MT",nullable=true)
	public String getCodAlimentadorMt() {
		return codAlimentadorMt;
	}

	public void setCodAlimentadorMt(String codAlimentadorMt) {
		this.codAlimentadorMt = codAlimentadorMt;
	}

	@Column(name="COD_SED",nullable=true)
	public String getCodSed() {
		return codSed;
	}

	public void setCodSed(String codSed) {
		this.codSed = codSed;
	}

	@Column(name="NOMBRE",nullable=true)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name="COD_INSTALACION",nullable=true)
	public String getCodInstalacion() {
		return codInstalacion;
	}

	public void setCodInstalacion(String codInstalacion) {
		this.codInstalacion = codInstalacion;
	}

	@Column(name="PROPIETARIO",nullable=true)
	public String getPropietario() {
		return propietario;
	}

	public void setPropietario(String propietario) {
		this.propietario = propietario;
	}

	@Column(name="TEN_NOM_BT",nullable=true)
	public BigDecimal getTenNomBt() {
		return tenNomBt;
	}

	public void setTenNomBt(BigDecimal tenNomBt) {
		this.tenNomBt = tenNomBt;
	}

	@Column(name="TEN_NOM_MT",nullable=true)
	public BigDecimal getTenNomMt() {
		return tenNomMt;
	}

	public void setTenNomMt(BigDecimal tenNomMt) {
		this.tenNomMt = tenNomMt;
	}

	@Column(name="CAP_TRANSFORMACION",nullable=true)
	public BigDecimal getCapTransformacion() {
		return capTransformacion;
	}

	public void setCapTransformacion(BigDecimal capTransformacion) {
		this.capTransformacion = capTransformacion;
	}

	@Column(name="UTM_ESTE",nullable=true)
	public BigDecimal getUtmEste() {
		return utmEste;
	}

	public void setUtmEste(BigDecimal utmEste) {
		this.utmEste = utmEste;
	}

	@Column(name="UTM_NORTE",nullable=true)
	public BigDecimal getUtmNorte() {
		return utmNorte;
	}

	public void setUtmNorte(BigDecimal utmNorte) {
		this.utmNorte = utmNorte;
	}

	public String toString(){
		return new Long(this.getSedId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getSedId()==((Sed)obj).getSedId()){
			return true;
		}else{
			return false;
		}
	}
	
}
