package pe.com.calidad.proc228.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CAL_TIPO_PROTECCION_MT")
@SequenceGenerator(name = "TipProteccionMtIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "TipoProteccionMTSeq")
public class TipoProteccionMT  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long tipProteccionMtId;
	private String codTipProteccionMt;
	private String descripcion;
	private String estado;
	
	public TipoProteccionMT() {

	}

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TipProteccionMtIdGenerator")
    @Column(name = "TIP_PROTECCION_MT_ID",nullable=false,unique=true)	
	public long getTipProteccionMtId() {
		return tipProteccionMtId;
	}

	public void setTipProteccionMtId(long tipProteccionMtId) {
		this.tipProteccionMtId = tipProteccionMtId;
	}

	@Column(name="COD_TIP_PROTECCION_MT",nullable=true)
	public String getCodTipProteccionMt() {
		return codTipProteccionMt;
	}

	public void setCodTipProteccionMt(String codTipProteccionMt) {
		this.codTipProteccionMt = codTipProteccionMt;
	}

	@Column(name="DESCRIPCION",nullable=true)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name="ESTADO",nullable=true)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String toString(){
		return new Long(this.getTipProteccionMtId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getTipProteccionMtId()==((TipoProteccionMT)obj).getTipProteccionMtId()){
			return true;
		}else{
			return false;
		}
	}
	
}
