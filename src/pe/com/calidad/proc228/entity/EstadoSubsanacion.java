package pe.com.calidad.proc228.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CAL_ESTADO_SUBSANACION")
@SequenceGenerator(name = "EstSubsanacionIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "EstadoSubsanacionSeq")
public class EstadoSubsanacion  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long estSubsanacionId;
	private String codEstSubsanacion;
	private String descripcion;
	private String estado;
	
	private Set<Deficiencia> deficiencias;
	
	public EstadoSubsanacion() {

	}
	
	
	@OneToMany(cascade ={CascadeType.ALL}, mappedBy = "estadoSubsanacion")
	public Set<Deficiencia> getDeficiencias() {
		return deficiencias;
	}



	public void setDeficiencias(Set<Deficiencia> deficiencias) {
		this.deficiencias = deficiencias;
	}


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EstSubsanacionIdGenerator")
    @Column(name = "EST_SUBSANACION_ID",nullable=false,unique=true)
	public long getEstSubsanacionId() {
		return estSubsanacionId;
	}

	public void setEstSubsanacionId(long estSubsanacionId) {
		this.estSubsanacionId = estSubsanacionId;
	}

	@Column(name="COD_EST_SUBSANACION",nullable=true)
	public String getCodEstSubsanacion() {
		return codEstSubsanacion;
	}

	public void setCodEstSubsanacion(String codEstSubsanacion) {
		this.codEstSubsanacion = codEstSubsanacion;
	}

	@Column(name="DESCRIPCION",nullable=true)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name="ESTADO",nullable=true)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public String toString(){
		return new Long(this.getEstSubsanacionId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getEstSubsanacionId()==((EstadoSubsanacion)obj).getEstSubsanacionId()){
			return true;
		}else{
			return false;
		}
	}

	
}
