package pe.com.calidad.proc228.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CAL_TIPO_INSTALACION")
@SequenceGenerator(name = "TipInstalacionIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "TipoInstalacionSeq")
public class TipoInstalacion  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long tipInstalacionId;
	private String codTipInstalacion;
	private String descripcion;
	private String estado;
	
	private Set<TipoDeficiencia> tipoDeficiencias;
	
	public TipoInstalacion() {

	}


	@OneToMany(cascade ={CascadeType.ALL}, mappedBy = "tipoInstalacion")
	public Set<TipoDeficiencia> getTipoDeficiencias() {
		return tipoDeficiencias;
	}



	public void setTipoDeficiencias(Set<TipoDeficiencia> tipoDeficiencias) {
		this.tipoDeficiencias = tipoDeficiencias;
	}


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TipInstalacionIdGenerator")
    @Column(name = "TIP_INSTALACION_ID",nullable=false,unique=true)
	public long getTipInstalacionId() {
		return tipInstalacionId;
	}

	public void setTipInstalacionId(long tipInstalacionId) {
		this.tipInstalacionId = tipInstalacionId;
	}

	@Column(name="COD_TIP_INSTALACION",nullable=true)
	public String getCodTipInstalacion() {
		return codTipInstalacion;
	}

	public void setCodTipInstalacion(String codTipInstalacion) {
		this.codTipInstalacion = codTipInstalacion;
	}

	@Column(name="DESCRIPCION",nullable=true)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name="ESTADO",nullable=true)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public String toString(){
		return new Long(this.getTipInstalacionId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getTipInstalacionId()==((TipoInstalacion)obj).getTipInstalacionId()){
			return true;
		}else{
			return false;
		}
	}

	
}
