package pe.com.calidad.proc228.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CAL_TIPO_COMPONENTE")
@SequenceGenerator(name = "TipComponenteIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "TipoComponenteSeq")
public class TipoComponente  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long tipComponenteId;
	private String codTipComponente;
	private String descripcion;
	private String estado;
	
	private Set<TipoDeficiencia> tipoDeficiencias;
	
	public TipoComponente() {

	}
	


	@OneToMany(cascade ={CascadeType.ALL}, mappedBy = "tipoComponente")
	public Set<TipoDeficiencia> getTipoDeficiencias() {
		return tipoDeficiencias;
	}




	public void setTipoDeficiencias(Set<TipoDeficiencia> tipoDeficiencias) {
		this.tipoDeficiencias = tipoDeficiencias;
	}



    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TipComponenteIdGenerator")
    @Column(name = "TIP_COMPONENTE_ID",nullable=false,unique=true)
	public long getTipComponenteId() {
		return tipComponenteId;
	}

	public void setTipComponenteId(long tipComponenteId) {
		this.tipComponenteId = tipComponenteId;
	}

	@Column(name="COD_TIP_COMPONENTE",nullable=true)
	public String getCodTipComponente() {
		return codTipComponente;
	}

	public void setCodTipComponente(String codTipComponente) {
		this.codTipComponente = codTipComponente;
	}

	@Column(name="DESCRIPCION",nullable=true)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name="ESTADO",nullable=true)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String toString(){
		return new Long(this.getTipComponenteId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getTipComponenteId()==((TipoComponente)obj).getTipComponenteId()){
			return true;
		}else{
			return false;
		}
	}
	
}
