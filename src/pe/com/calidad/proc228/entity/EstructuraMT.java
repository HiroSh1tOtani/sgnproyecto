package pe.com.calidad.proc228.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CAL_ESTRUCTURA_MT")
@SequenceGenerator(name = "EstructuraMtIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "EstructuraMTSeq")
public class EstructuraMT  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long estructuraMtId;
	private String codAlimentadorMt;
	private String codTramoMt;
	private String codEstructuraMt;
	private String nombre;
	private String codInstalacion;
	private String propietario;
	private BigDecimal utmEste;
	private BigDecimal utmNorte;
	
	public EstructuraMT() {

	}

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EstructuraMtIdGenerator")
    @Column(name = "ESTRUCTURA_MT_ID",nullable=false,unique=true)
	public long getEstructuraMtId() {
		return estructuraMtId;
	}

	public void setEstructuraMtId(long estructuraMtId) {
		this.estructuraMtId = estructuraMtId;
	}

	@Column(name="COD_ALIMENTADOR_MT",nullable=true)
	public String getCodAlimentadorMt() {
		return codAlimentadorMt;
	}

	public void setCodAlimentadorMt(String codAlimentadorMt) {
		this.codAlimentadorMt = codAlimentadorMt;
	}

	@Column(name="COD_TRAMO_MT",nullable=true)
	public String getCodTramoMt() {
		return codTramoMt;
	}

	public void setCodTramoMt(String codTramoMt) {
		this.codTramoMt = codTramoMt;
	}

	@Column(name="COD_ESTRUCTURA_MT",nullable=true)
	public String getCodEstructuraMt() {
		return codEstructuraMt;
	}

	public void setCodEstructuraMt(String codEstructuraMt) {
		this.codEstructuraMt = codEstructuraMt;
	}

	@Column(name="NOMBRE",nullable=true)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name="COD_INSTALACION",nullable=true)
	public String getCodInstalacion() {
		return codInstalacion;
	}

	public void setCodInstalacion(String codInstalacion) {
		this.codInstalacion = codInstalacion;
	}

	@Column(name="PROPIETARIO",nullable=true)
	public String getPropietario() {
		return propietario;
	}

	public void setPropietario(String propietario) {
		this.propietario = propietario;
	}

	@Column(name="UTM_ESTE",nullable=true)
	public BigDecimal getUtmEste() {
		return utmEste;
	}

	public void setUtmEste(BigDecimal utmEste) {
		this.utmEste = utmEste;
	}

	@Column(name="UTM_NORTE",nullable=true)
	public BigDecimal getUtmNorte() {
		return utmNorte;
	}

	public void setUtmNorte(BigDecimal utmNorte) {
		this.utmNorte = utmNorte;
	}

	public String toString(){
		return new Long(this.getEstructuraMtId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getEstructuraMtId()==((EstructuraMT)obj).getEstructuraMtId()){
			return true;
		}else{
			return false;
		}
	}
	
}
