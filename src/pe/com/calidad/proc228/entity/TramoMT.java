package pe.com.calidad.proc228.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CAL_TRAMO_MT")
@SequenceGenerator(name = "TramoMtIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "TramoMTSeq")
public class TramoMT  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long tramoMtId;
	private String codAlimentadorMt;
	private String codTramoMt;
	private String propietario;
	private String tipInstalacion;
	private BigDecimal longitud;
	
	public TramoMT() {

	}

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TramoMtIdGenerator")
    @Column(name = "TRAMO_MT_ID",nullable=false,unique=true)
	public long getTramoMtId() {
		return tramoMtId;
	}

	public void setTramoMtId(long tramoMtId) {
		this.tramoMtId = tramoMtId;
	}

	@Column(name="COD_ALIMENTADOR_MT",nullable=true)
	public String getCodAlimentadorMt() {
		return codAlimentadorMt;
	}

	public void setCodAlimentadorMt(String codAlimentadorMt) {
		this.codAlimentadorMt = codAlimentadorMt;
	}

	@Column(name="COD_TRAMO_MT",nullable=true)
	public String getCodTramoMt() {
		return codTramoMt;
	}

	public void setCodTramoMt(String codTramoMt) {
		this.codTramoMt = codTramoMt;
	}

	@Column(name="PROPIETARIO",nullable=true)
	public String getPropietario() {
		return propietario;
	}

	public void setPropietario(String propietario) {
		this.propietario = propietario;
	}

	@Column(name="TIP_INSTALACION",nullable=true)
	public String getTipInstalacion() {
		return tipInstalacion;
	}

	public void setTipInstalacion(String tipInstalacion) {
		this.tipInstalacion = tipInstalacion;
	}

	@Column(name="LONGITUD",nullable=true)
	public BigDecimal getLongitud() {
		return longitud;
	}

	public void setLongitud(BigDecimal longitud) {
		this.longitud = longitud;
	}

	public String toString(){
		return new Long(this.getTramoMtId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getTramoMtId()==((TramoMT)obj).getTramoMtId()){
			return true;
		}else{
			return false;
		}
	}
}
