package pe.com.calidad.proc228.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CAL_TIPO_CONEXION_MT")
@SequenceGenerator(name = "TipConexionMtIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "TipoConexionMTSeq")
public class TipoConexionMT  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long tipConexionMtId;
	private String codTipConexionMt;
	private String descripcion;
	private String estado;
	
	public TipoConexionMT() {

	}

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TipConexionMtIdGenerator")
    @Column(name = "TIP_CONEXION_MT_ID",nullable=false,unique=true)
	public long getTipConexionMtId() {
		return tipConexionMtId;
	}

	public void setTipConexionMtId(long tipConexionMtId) {
		this.tipConexionMtId = tipConexionMtId;
	}

	@Column(name="COD_TIP_CONEXION_MT",nullable=true)
	public String getCodTipConexionMt() {
		return codTipConexionMt;
	}

	public void setCodTipConexionMt(String codTipConexionMt) {
		this.codTipConexionMt = codTipConexionMt;
	}

	@Column(name="DESCRIPCION",nullable=true)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name="ESTADO",nullable=true)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String toString(){
		return new Long(this.getTipConexionMtId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getTipConexionMtId()==((TipoConexionMT)obj).getTipConexionMtId()){
			return true;
		}else{
			return false;
		}
	}
	
}
