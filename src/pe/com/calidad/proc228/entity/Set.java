package pe.com.calidad.proc228.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CAL_SET")
@SequenceGenerator(name = "SetIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "SetSeq")
public class Set  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long setId;
	private String ubigeo;
	private String codSet;
	private String nombre;
	private long numAlimentadores;
	private BigDecimal capTransformacion;
	private BigDecimal utmEste;
	private BigDecimal utmNorte;
	private BigDecimal tenNomBarra1;
	private BigDecimal tenNomBarra2;
	private BigDecimal tenNomBarra3;
	
	public Set() {

	}

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SetIdGenerator")
    @Column(name = "SET_ID",nullable=false,unique=true)
	public long getSetId() {
		return setId;
	}

	public void setSetId(long setId) {
		this.setId = setId;
	}

	@Column(name="UBIGEO",nullable=true)
	public String getUbigeo() {
		return ubigeo;
	}

	public void setUbigeo(String ubigeo) {
		this.ubigeo = ubigeo;
	}

	@Column(name="COD_SET",nullable=true)
	public String getCodSet() {
		return codSet;
	}

	public void setCodSet(String codSet) {
		this.codSet = codSet;
	}

	@Column(name="NOMBRE",nullable=true)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name="NUM_ALIMENTADORES",nullable=true)
	public long getNumAlimentadores() {
		return numAlimentadores;
	}

	public void setNumAlimentadores(long numAlimentadores) {
		this.numAlimentadores = numAlimentadores;
	}

	@Column(name="CAP_TRANSFORMACION",nullable=true)
	public BigDecimal getCapTransformacion() {
		return capTransformacion;
	}

	public void setCapTransformacion(BigDecimal capTransformacion) {
		this.capTransformacion = capTransformacion;
	}

	@Column(name="UTM_ESTE",nullable=true)
	public BigDecimal getUtmEste() {
		return utmEste;
	}

	public void setUtmEste(BigDecimal utmEste) {
		this.utmEste = utmEste;
	}

	@Column(name="UTM_NORTE",nullable=true)
	public BigDecimal getUtmNorte() {
		return utmNorte;
	}

	public void setUtmNorte(BigDecimal utmNorte) {
		this.utmNorte = utmNorte;
	}

	@Column(name="TEN_NOM_BARRA1",nullable=true)
	public BigDecimal getTenNomBarra1() {
		return tenNomBarra1;
	}

	public void setTenNomBarra1(BigDecimal tenNomBarra1) {
		this.tenNomBarra1 = tenNomBarra1;
	}

	@Column(name="TEN_NOM_BARRA2",nullable=true)
	public BigDecimal getTenNomBarra2() {
		return tenNomBarra2;
	}

	public void setTenNomBarra2(BigDecimal tenNomBarra2) {
		this.tenNomBarra2 = tenNomBarra2;
	}

	@Column(name="TEN_NOM_BARRA3",nullable=true)
	public BigDecimal getTenNomBarra3() {
		return tenNomBarra3;
	}

	public void setTenNomBarra3(BigDecimal tenNomBarra3) {
		this.tenNomBarra3 = tenNomBarra3;
	}

	public String toString(){
		return new Long(this.getSetId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getSetId()==((Set)obj).getSetId()){
			return true;
		}else{
			return false;
		}
	}
}
