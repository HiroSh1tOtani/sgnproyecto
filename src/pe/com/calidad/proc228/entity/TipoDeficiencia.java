package pe.com.calidad.proc228.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CAL_TIPO_DEFICIENCIA")
@SequenceGenerator(name = "TipDeficienciaIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "TipoDeficienciaSeq")
public class TipoDeficiencia  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long tipDeficienciaId;
	private String codTipDeficiencia;
	private String descripcion;
	private String criIdentificacion;
	private String norTrasgredida;
	private String prioridad;

	private String estado;
	
	private TipoInstalacion tipoInstalacion;
	private TipoComponente tipoComponente;
	
	private Set<Deficiencia> deficiencias;
	
	public TipoDeficiencia() {

	}

	
    @ManyToOne
    @JoinColumn(name="PK_TIP_INSTALACION_ID", nullable=false)	
	public TipoInstalacion getTipoInstalacion() {
		return tipoInstalacion;
	}



	public void setTipoInstalacion(TipoInstalacion tipoInstalacion) {
		this.tipoInstalacion = tipoInstalacion;
	}


    @ManyToOne
    @JoinColumn(name="PK_TIP_COMPONENTE_ID", nullable=false)
	public TipoComponente getTipoComponente() {
		return tipoComponente;
	}



	public void setTipoComponente(TipoComponente tipoComponente) {
		this.tipoComponente = tipoComponente;
	}


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TipDeficienciaIdGenerator")
    @Column(name = "TIP_DEFICIENCIA_ID",nullable=false,unique=true)
	public long getTipDeficienciaId() {
		return tipDeficienciaId;
	}

	public void setTipDeficienciaId(long tipDeficienciaId) {
		this.tipDeficienciaId = tipDeficienciaId;
	}

	@Column(name="COD_TIP_DEFICIENCIA",nullable=true)
	public String getCodTipDeficiencia() {
		return codTipDeficiencia;
	}

	public void setCodTipDeficiencia(String codTipDeficiencia) {
		this.codTipDeficiencia = codTipDeficiencia;
	}

	@Column(name="DESCRIPCION",nullable=true)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name="CRI_IDENTIFICACION",nullable=true)
	public String getCriIdentificacion() {
		return criIdentificacion;
	}

	public void setCriIdentificacion(String criIdentificacion) {
		this.criIdentificacion = criIdentificacion;
	}

	@Column(name="NOR_TRASGREDIDA",nullable=true)
	public String getNorTrasgredida() {
		return norTrasgredida;
	}

	public void setNorTrasgredida(String norTrasgredida) {
		this.norTrasgredida = norTrasgredida;
	}

	@Column(name="ESTADO",nullable=true)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}


	@Column(name="PRIORIDAD",nullable=true)
	public String getPrioridad() {
		return prioridad;
	}


	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}
	
	public String toString(){
		return new Long(this.getTipDeficienciaId()).toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this.getTipDeficienciaId()==((TipoDeficiencia)obj).getTipDeficienciaId()){
			return true;
		}else{
			return false;
		}
	}

	@OneToMany(cascade ={CascadeType.ALL}, mappedBy = "tipoDeficiencia")
	public Set<Deficiencia> getDeficiencias() {
		return deficiencias;
	}


	public void setDeficiencias(Set<Deficiencia> deficiencias) {
		this.deficiencias = deficiencias;
	}

}
