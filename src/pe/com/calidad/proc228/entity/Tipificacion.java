package pe.com.calidad.proc228.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CAL_TIPIFICACION")
@SequenceGenerator(name = "TipificacionIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "TipificacionSeq")
public class Tipificacion  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long tipificacionId;
	private String codTipificacion;
	private String descripcion;
	private String estado;

	
	public Tipificacion() {

	}


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TipificacionIdGenerator")
    @Column(name = "TIPIFICACION_ID",nullable=false,unique=true)
    public long getTipificacionId() {
		return tipificacionId;
	}

	public void setTipificacionId(long tipificacionId) {
		this.tipificacionId = tipificacionId;
	}

	@Column(name="COD_TIPIFICACION",nullable=true)
	public String getCodTipificacion() {
		return codTipificacion;
	}

	public void setCodTipificacion(String codTipificacion) {
		this.codTipificacion = codTipificacion;
	}

	@Column(name="DESCRIPCION",nullable=true)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name="ESTADO",nullable=true)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public String toString(){
		return new Long(this.getTipificacionId()).toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this.getTipificacionId()==((Tipificacion)obj).getTipificacionId()){
			return true;
		}else{
			return false;
		}
	}
	
}
