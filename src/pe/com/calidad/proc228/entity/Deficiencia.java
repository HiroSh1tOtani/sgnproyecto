package pe.com.calidad.proc228.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import pe.com.calidad.alumbradopublico.entity.MedicionAp;
import pe.com.indra.calidad.entity.PeriodoMedicion;
@Entity
@Table(name="CAL_DEFICIENCIA")
@SequenceGenerator(name = "DeficienciaIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "DeficienciaSeq")
public class Deficiencia  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long deficienciaId;
	private String codIdentificacion;
	private String codTipInstalacion;
	private String codInstalacion;
	private String codResCumplimiento;
	private String numSuministro;
	private String codDenunciante;
	private Date fecDenuncia;
	private Date fecInspeccion;
	private Date fecSubsanacion;
	private String observaciones;
	private String referencia1;
	private String referencia2;
	private BigDecimal utmEste;
	private BigDecimal utmNorte;

	private String codTipInfractor;
	private String nomInfractor;
	private String dirInfractor;
	private String repLegInfractor;
	private String numSumInfractor;
	private Date fecPueServicio;
	private String motDeficiencia;
	private String numCarta;
	private String estado;
	private String codAlimentador;
	private String secTipico;
	
	private PeriodoMedicion periodoMedicion;
	private EstadoSubsanacion estadoSubsanacion;
	private TipoDeficiencia tipoDeficiencia;
	private String codTipInstalacionR1;
	private String codTipInstalacionR2;
	private String meta;
	
	
	private Metas tipoMeta;
	
	public Deficiencia() {

	}
	
	
    @ManyToOne
    @JoinColumn(name="PK_PER_MEDICION_ID", nullable=false)
	public PeriodoMedicion getPeriodoMedicion() {
		return periodoMedicion;
	}



	public void setPeriodoMedicion(PeriodoMedicion periodoMedicion) {
		this.periodoMedicion = periodoMedicion;
	}


    @ManyToOne
    @JoinColumn(name="PK_EST_SUBSANACION_ID", nullable=false)
	public EstadoSubsanacion getEstadoSubsanacion() {
		return estadoSubsanacion;
	}



	public void setEstadoSubsanacion(EstadoSubsanacion estadoSubsanacion) {
		this.estadoSubsanacion = estadoSubsanacion;
	}


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DeficienciaIdGenerator")
    @Column(name = "DEFICIENCIA_ID",nullable=false,unique=true)
	public long getDeficienciaId() {
		return deficienciaId;
	}

	public void setDeficienciaId(long deficienciaId) {
		this.deficienciaId = deficienciaId;
	}

	@Column(name="COD_IDENTIFICACION",nullable=true)
	public String getCodIdentificacion() {
		return codIdentificacion;
	}

	public void setCodIdentificacion(String codIdentificacion) {
		this.codIdentificacion = codIdentificacion;
	}

	@Column(name="COD_TIP_INSTALACION",nullable=true)
	public String getCodTipInstalacion() {
		return codTipInstalacion;
	}

	public void setCodTipInstalacion(String codTipInstalacion) {
		this.codTipInstalacion = codTipInstalacion;
	}

	@Column(name="COD_INSTALACION",nullable=true)
	public String getCodInstalacion() {
		return codInstalacion;
	}

	public void setCodInstalacion(String codInstalacion) {
		this.codInstalacion = codInstalacion;
	}

	@Column(name="COD_RES_CUMPLIMIENTO",nullable=true)
	public String getCodResCumplimiento() {
		return codResCumplimiento;
	}

	public void setCodResCumplimiento(String codResCumplimiento) {
		this.codResCumplimiento = codResCumplimiento;
	}

	@Column(name="NUM_SUMINISTRO",nullable=true)
	public String getNumSuministro() {
		return numSuministro;
	}

	public void setNumSuministro(String numSuministro) {
		this.numSuministro = numSuministro;
	}

	@Column(name="COD_DENUNCIANTE",nullable=true)
	public String getCodDenunciante() {
		return codDenunciante;
	}

	public void setCodDenunciante(String codDenunciante) {
		this.codDenunciante = codDenunciante;
	}

	@Column(name="FEC_DENUNCIA",nullable=true)
	public Date getFecDenuncia() {
		return fecDenuncia;
	}

	public void setFecDenuncia(Date fecDenuncia) {
		this.fecDenuncia = fecDenuncia;
	}

	@Column(name="FEC_INSPECCION",nullable=true)
	public Date getFecInspeccion() {
		return fecInspeccion;
	}

	public void setFecInspeccion(Date fecInspeccion) {
		this.fecInspeccion = fecInspeccion;
	}

	@Column(name="FEC_SUBSANACION",nullable=true)
	public Date getFecSubsanacion() {
		return fecSubsanacion;
	}

	public void setFecSubsanacion(Date fecSubsanacion) {
		this.fecSubsanacion = fecSubsanacion;
	}

	@Column(name="OBSERVACIONES",nullable=true)
	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@Column(name="REFERENCIA1",nullable=true)
	public String getReferencia1() {
		return referencia1;
	}

	public void setReferencia1(String referencia1) {
		this.referencia1 = referencia1;
	}

	@Column(name="REFERENCIA2",nullable=true)
	public String getReferencia2() {
		return referencia2;
	}

	public void setReferencia2(String referencia2) {
		this.referencia2 = referencia2;
	}

	@Column(name="UTM_ESTE",nullable=true)
	public BigDecimal getUtmEste() {
		return utmEste;
	}

	public void setUtmEste(BigDecimal utmEste) {
		this.utmEste = utmEste;
	}

	@Column(name="UTM_NORTE",nullable=true)
	public BigDecimal getUtmNorte() {
		return utmNorte;
	}

	public void setUtmNorte(BigDecimal utmNorte) {
		this.utmNorte = utmNorte;
	}

	@Column(name="COD_TIP_INFRACTOR",nullable=true)
	public String getCodTipInfractor() {
		return codTipInfractor;
	}

	public void setCodTipInfractor(String codTipInfractor) {
		this.codTipInfractor = codTipInfractor;
	}

	@Column(name="NOM_INFRACTOR",nullable=true)
	public String getNomInfractor() {
		return nomInfractor;
	}

	public void setNomInfractor(String nomInfractor) {
		this.nomInfractor = nomInfractor;
	}

	@Column(name="DIR_INFRACTOR",nullable=true)
	public String getDirInfractor() {
		return dirInfractor;
	}

	public void setDirInfractor(String dirInfractor) {
		this.dirInfractor = dirInfractor;
	}

	@Column(name="REP_LEG_INFRACTOR",nullable=true)
	public String getRepLegInfractor() {
		return repLegInfractor;
	}

	public void setRepLegInfractor(String repLegInfractor) {
		this.repLegInfractor = repLegInfractor;
	}

	@Column(name="NUM_SUM_INFRACTOR",nullable=true)
	public String getNumSumInfractor() {
		return numSumInfractor;
	}

	public void setNumSumInfractor(String numSumInfractor) {
		this.numSumInfractor = numSumInfractor;
	}

	@Column(name="FEC_PUE_SERVICIO",nullable=true)
	public Date getFecPueServicio() {
		return fecPueServicio;
	}

	public void setFecPueServicio(Date fecPueServicio) {
		this.fecPueServicio = fecPueServicio;
	}

	@Column(name="MOT_DEFICIENCIA",nullable=true)
	public String getMotDeficiencia() {
		return motDeficiencia;
	}

	public void setMotDeficiencia(String motDeficiencia) {
		this.motDeficiencia = motDeficiencia;
	}

	@Column(name="NUM_CARTA",nullable=true)
	public String getNumCarta() {
		return numCarta;
	}

	public void setNumCarta(String numCarta) {
		this.numCarta = numCarta;
	}

	@Column(name="ESTADO",nullable=true)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String toString(){
		return new Long(this.getDeficienciaId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getDeficienciaId()==((Deficiencia)obj).getDeficienciaId()){
			return true;
		}else{
			return false;
		}
	}

	@ManyToOne
    @JoinColumn(name="PK_TIP_DEFICIENCIA_ID", nullable=true)
	public TipoDeficiencia getTipoDeficiencia() {
		return tipoDeficiencia;
	}


	public void setTipoDeficiencia(TipoDeficiencia tipoDeficiencia) {
		this.tipoDeficiencia = tipoDeficiencia;
	}

	@Column(name="COD_ALIMENTADOR",nullable=true)
	public String getCodAlimentador() {
		return codAlimentador;
	}


	public void setCodAlimentador(String codAlimentador) {
		this.codAlimentador = codAlimentador;
	}

	@Column(name="SEC_TIPICO",nullable=true)
	public String getSecTipico() {
		return secTipico;
	}


	public void setSecTipico(String secTipico) {
		this.secTipico = secTipico;
	}

	@Column(name="COD_TIP_INSTALACION_R1",nullable=true)
	public String getCodTipInstalacionR1() {
		return codTipInstalacionR1;
	}


	public void setCodTipInstalacionR1(String codTipInstalacionR1) {
		this.codTipInstalacionR1 = codTipInstalacionR1;
	}

	@Column(name="COD_TIP_INSTALACION_R2",nullable=true)
	public String getCodTipInstalacionR2() {
		return codTipInstalacionR2;
	}


	public void setCodTipInstalacionR2(String codTipInstalacionR2) {
		this.codTipInstalacionR2 = codTipInstalacionR2;
	}

	@Column(name="META",nullable=true)
	public String getMeta() {
		return meta;
	}


	public void setMeta(String meta) {
		this.meta = meta;
	}

	@Transient
	public Metas getTipoMeta() {
		return tipoMeta;
	}


	public void setTipoMeta(Metas tipoMeta) {
		this.tipoMeta = tipoMeta;
	}




	
	
	
}
