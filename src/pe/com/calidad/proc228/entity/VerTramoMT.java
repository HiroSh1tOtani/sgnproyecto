package pe.com.calidad.proc228.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CAL_VER_TRAMO_MT")
@SequenceGenerator(name = "VerTramoMtIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "VerTramoMTSeq")
public class VerTramoMT  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long verTramoMtId;
	private String codTramoMt;
	private long secuencia;
	private BigDecimal utmEste;
	private BigDecimal utmNorte;
	
	public VerTramoMT() {

	}

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "VerTramoMtIdGenerator")
    @Column(name = "VER_TRAMO_MT_ID",nullable=false,unique=true)
	public long getVerTramoMtId() {
		return verTramoMtId;
	}

	public void setVerTramoMtId(long verTramoMtId) {
		this.verTramoMtId = verTramoMtId;
	}

	@Column(name="COD_TRAMO_MT",nullable=true)
	public String getCodTramoMt() {
		return codTramoMt;
	}

	public void setCodTramoMt(String codTramoMt) {
		this.codTramoMt = codTramoMt;
	}

	@Column(name="SECUENCIA",nullable=true)
	public long getSecuencia() {
		return secuencia;
	}

	public void setSecuencia(long secuencia) {
		this.secuencia = secuencia;
	}

	@Column(name="UTM_ESTE",nullable=true)
	public BigDecimal getUtmEste() {
		return utmEste;
	}

	public void setUtmEste(BigDecimal utmEste) {
		this.utmEste = utmEste;
	}

	@Column(name="UTM_NORTE",nullable=true)
	public BigDecimal getUtmNorte() {
		return utmNorte;
	}

	public void setUtmNorte(BigDecimal utmNorte) {
		this.utmNorte = utmNorte;
	}

	public String toString(){
		return new Long(this.getVerTramoMtId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getVerTramoMtId()==((VerTramoMT)obj).getVerTramoMtId()){
			return true;
		}else{
			return false;
		}
	}
	
}
