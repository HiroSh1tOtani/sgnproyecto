package pe.com.calidad.proc228.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import pe.com.calidad.alumbradopublico.entity.MedicionAp;

@Entity
@Table(name="CAL_ALIMENTADOR_MT")
@SequenceGenerator(name = "AlimentadorMTIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "AlimentadorMTSeq")
public class AlimentadorMT  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long alimentadorMTId;
	private String sisElectrico;
	private String codSet;
	private String codAlimentadorMT;
	private String nomAlimentador;
	private BigDecimal tenNominal;
	private String tipConexion;
	private String tipProteccion;
	private BigDecimal lonTotal;
	private BigDecimal lonAerea;
	private BigDecimal utmEste;
	private BigDecimal utmNorte;
	
	public AlimentadorMT() {

	}
	
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AlimentadorMTIdGenerator")
    @Column(name = "ALIMENTADOR_MT_ID",nullable=false,unique=true)	
	public long getAlimentadorMTId() {
		return alimentadorMTId;
	}
	public void setAlimentadorMTId(long alimentadorMTId) {
		this.alimentadorMTId = alimentadorMTId;
	}
	
	@Column(name="SIS_ELECTRICO",nullable=true)
	public String getSisElectrico() {
		return sisElectrico;
	}
	public void setSisElectrico(String sisElectrico) {
		this.sisElectrico = sisElectrico;
	}
	
	@Column(name="COD_SET",nullable=true)
	public String getCodSet() {
		return codSet;
	}
	public void setCodSet(String codSet) {
		this.codSet = codSet;
	}
	
	@Column(name="COD_ALIMENTADOR_MT",nullable=true)
	public String getCodAlimentadorMT() {
		return codAlimentadorMT;
	}
	public void setCodAlimentadorMT(String codAlimentadorMT) {
		this.codAlimentadorMT = codAlimentadorMT;
	}
	
	@Column(name="NOM_ALIMENTADOR",nullable=true)
	public String getNomAlimentador() {
		return nomAlimentador;
	}
	public void setNomAlimentador(String nomAlimentador) {
		this.nomAlimentador = nomAlimentador;
	}
	
	@Column(name="TEN_NOMINAL",nullable=true)
	public BigDecimal getTenNominal() {
		return tenNominal;
	}
	public void setTenNominal(BigDecimal tenNominal) {
		this.tenNominal = tenNominal;
	}
	
	@Column(name="TIP_CONEXION",nullable=true)
	public String getTipConexion() {
		return tipConexion;
	}
	public void setTipConexion(String tipConexion) {
		this.tipConexion = tipConexion;
	}
	
	@Column(name="TIP_PROTECCION",nullable=true)
	public String getTipProteccion() {
		return tipProteccion;
	}
	public void setTipProteccion(String tipProteccion) {
		this.tipProteccion = tipProteccion;
	}
	
	@Column(name="LON_TOTAL",nullable=true)
	public BigDecimal getLonTotal() {
		return lonTotal;
	}
	public void setLonTotal(BigDecimal lonTotal) {
		this.lonTotal = lonTotal;
	}
	
	@Column(name="LON_AEREA",nullable=true)
	public BigDecimal getLonAerea() {
		return lonAerea;
	}
	public void setLonAerea(BigDecimal lonAerea) {
		this.lonAerea = lonAerea;
	}
	
	@Column(name="UTM_ESTE",nullable=true)
	public BigDecimal getUtmEste() {
		return utmEste;
	}
	public void setUtmEste(BigDecimal utmEste) {
		this.utmEste = utmEste;
	}
	
	@Column(name="UTM_NORTE",nullable=true)
	public BigDecimal getUtmNorte() {
		return utmNorte;
	}
	public void setUtmNorte(BigDecimal utmNorte) {
		this.utmNorte = utmNorte;
	}
	
	public String toString(){
		return new Long(this.getAlimentadorMTId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getAlimentadorMTId()==((AlimentadorMT)obj).getAlimentadorMTId()){
			return true;
		}else{
			return false;
		}
	}



}
