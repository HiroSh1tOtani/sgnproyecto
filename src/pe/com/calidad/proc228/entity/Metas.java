package pe.com.calidad.proc228.entity;

import java.io.Serializable;
import java.util.Set;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CAL_METAS")
@SequenceGenerator(name = "MetasIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "MetasSeq")
public class Metas  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long metasId;
	private String secTipico;
	private String desMeta;
	private String ano;
	private String codTipDeficiencia;
	private String prioridad;
	private String estado;

	
	public Metas() {

	}


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MetasIdGenerator")
    @Column(name = "METAS_ID",nullable=false,unique=true)
   	public long getMetasId() {
		return metasId;
	}


	public void setMetasId(long metasId) {
		this.metasId = metasId;
	}

	@Column(name="SEC_TIPICO",nullable=true)
	public String getSecTipico() {
		return secTipico;
	}


	public void setSecTipico(String secTipico) {
		this.secTipico = secTipico;
	}


	@Column(name="DES_META",nullable=true)
	public String getDesMeta() {
		return desMeta;
	}


	public void setDesMeta(String desMeta) {
		this.desMeta = desMeta;
	}

	@Column(name="ANO",nullable=true)
	public String getAno() {
		return ano;
	}


	public void setAno(String ano) {
		this.ano = ano;
	}

	@Column(name="COD_TIP_DEFICIENCIA",nullable=true)
	public String getCodTipDeficiencia() {
		return codTipDeficiencia;
	}


	public void setCodTipDeficiencia(String codTipDeficiencia) {
		this.codTipDeficiencia = codTipDeficiencia;
	}

	@Column(name="PRIORIDAD"
			+ "",nullable=true)
	public String getPrioridad() {
		return prioridad;
	}


	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}

	@Column(name="ESTADO",nullable=true)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}	

	public String toString(){
		return new Long(this.getMetasId()).toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this.getMetasId()==((Metas)obj).getMetasId()){
			return true;
		}else{
			return false;
		}
	}
	
}
