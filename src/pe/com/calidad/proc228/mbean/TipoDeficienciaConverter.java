package pe.com.calidad.proc228.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.calidad.proc228.entity.TipoDeficiencia;
import pe.com.calidad.proc228.service.TipoDeficienciaService;

@FacesConverter(value="tipoDeficienciaConverter",forClass=TipoDeficienciaConverter.class)

public class TipoDeficienciaConverter implements Converter{

	@Override
	public TipoDeficiencia getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long tipDeficienciaId=new Long(cadena);
		TipoDeficienciaService service=new TipoDeficienciaService();
		TipoDeficiencia tipoDeficiencia=new TipoDeficiencia();
		tipoDeficiencia=service.ReadById(tipDeficienciaId);
		System.out.println(tipoDeficiencia.getTipDeficienciaId());
		return tipoDeficiencia;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			TipoDeficiencia tipoDeficiencia=(TipoDeficiencia) objeto;
			////System.out.println("objeto:"+new Long(tipoDeficiencia.getTipDeficienciaId()).toString());
			return new Long(tipoDeficiencia.getTipDeficienciaId()).toString();
		}
		return null;
	}


}
