package pe.com.calidad.proc228.mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

import pe.com.indra.calidad.util.Sql;
import pe.com.indra.calidad.mbean.FileUtilUploadBean;

@ManagedBean(name="proceso228UploadBean")
@SessionScoped
public class Proceso228UploadBean extends FileUtilUploadBean implements Serializable {
	
	
		public Proceso228UploadBean(){
			filaInicial = 2;
			numColumnas = 4;
			columnas = "1,2,3";
			tipoProceso = "2";
			
			nombreColumnas = "NUMSUMINISTRO,FECSUBSANACION,ESTADOSUBSANACION";
			
			itemsNombreColumnas=new ArrayList<SelectItem>();
			itemsNombreColumnas.add(new SelectItem("1",nombreColumnas));
			
			itemsSeparadorColumnas=new ArrayList<SelectItem>();
			itemsSeparadorColumnas.add(new SelectItem("\t","TAB"));
			
			itemsFormatoFecha=new ArrayList<SelectItem>();
			itemsFormatoFecha.add(new SelectItem("YYYY-MM-DD","YYYY-MM-DD"));
			//itemsFormatoFecha.add(new SelectItem("DD/MM/YYYY","DD/MM/YYYY"));
			
			columnas = "1,2,3";
			
			this.nombreArchivoBase = "proceso228";
		}
		

		public String procesar() throws Exception{
			registrarCsv();
			/*
		        List<String[]> lst = getListStringDesdeCsv();
		    	for (int i = 1;i<lst.size();i=i+1) {
		    		String[] e = lst.get(i);
		    		if (e[0]!=null && !e[0].isEmpty())
		    			actualizarRegistro(e[0],e[1],e[2],e[3]);
				}*/
		    return "/zonasegura/proc228/deficienciaListar?faces-redirect=true"; //retornar a la pagina que deseas por Ejemplo a medicionListar.xhtml donde esta el boton
		}	

		public String prepararMostrarCarga(){
			return "/zonasegura/proc228/cargaMasiva?faces-redirect=true";
		}

		
		public String actualizarRegistro (String codigoIdentificacion,String numSuministro, String fechaSubsanacion, String estadoSubsanacion) {
			//from Deficiencia i where i.periodoMedicion.perMedicionId = :perMedicionId order by i.fecDenuncia desc
			String updateSql;
			String mensaje = null;
			Sql sql = new Sql("CALIDAD");
			String resultado = null;
			//YYYY-MM-DD
			//TO_DATE('2011-07-28T23:54:14Z',  'YYYY-MM-DD"T"HH24:MI:SS"Z"')
			
			String estadoSql = "SELECT EST_SUBSANACION_ID FROM CAL_ESTADO_SUBSANACION WHERE COD_EST_SUBSANACION = " + estadoSubsanacion;
			String idSql = "SELECT DEFICIENCIA_ID FROM CAL_DEFICIENCIA WHERE NUM_SUMINISTRO = " + numSuministro + " AND COD_IDENTIFICACION='" + codigoIdentificacion + "'";
			updateSql = "UPDATE CAL_DEFICIENCIA SET FEC_SUBSANACION = TO_DATE('" + fechaSubsanacion + "','YYYY-MM-DD'), PK_EST_SUBSANACION_ID = (" + estadoSql + ") WHERE DEFICIENCIA_ID = ("  +  idSql + ")";
			System.out.println(updateSql);
			resultado = sql.ejecuta(updateSql);
			
			return mensaje;
		}
		
		
		
		
		
		
		


}
