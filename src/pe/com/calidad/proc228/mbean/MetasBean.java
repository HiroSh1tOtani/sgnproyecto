package pe.com.calidad.proc228.mbean;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.HibernateException;
import org.richfaces.model.Filter;

import pe.com.calidad.proc228.entity.Metas;
import pe.com.calidad.proc228.service.MetasService;
import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.indra.calidad.entity.PeriodoMedicion;
import pe.com.indra.calidad.service.PeriodoMedicionService;
import pe.com.indra.calidad.util.ConectaDb;
import pe.com.indra.calidad.util.Sql;
import pe.com.indra.calidad.util.Utilidad;



/**
 * @author pmoralesg
 *
 */
@ManagedBean(name="metasBean")
@SessionScoped
public class MetasBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<Metas> meta; 
	private List<Metas> busquedaxMetas;
	private Metas metas;
	private List<SelectItem> itemsAno;
	private List<SelectItem> itemsAnoMeta;
	private String ano;
	private List<SelectItem> itemsPrioridad;
	private List<SelectItem> itemsSectipico;
	private List<SelectItem> itemsTipDeficiencia;
	private String codTipDeficienciaFilter;
	private String secTipicoFilter;
	private String prioridadFilter;
	private List<SelectItem> itemsSecTipico1;
	private List<SelectItem> itemsPrioridad1;

	private String mensaje;
	
	@PostConstruct 
	public void init(){
		System.out.println("se ejecuto PostConstruct "+new java.util.Date());
		metas=new Metas();
				
	}
	
	public List<Metas> getMeta() {
		return meta;
	}

	public void setMeta(List<Metas> meta) {
		this.meta = meta;
	}

	public Metas getMetas() {
		return metas;
	}

	public void setMetas(Metas metas) {
		this.metas = metas;
	}
	
	

	
	public String prepararCrear(){
		//Esto funciona siempre que el bean tenga ambito de sesion 
		setMetas(new Metas());
		getMetas().setEstado("1");
		getMetas().setAno(ano);
		
		//System.out.println("Carga  - Deficiencias: " + deficiencias.size());
		
		return "/zonasegura/proc228/metasCrear?faces-redirect=true";
	}
	
	public String prepararEditar(){
		
		
		try {
			cargarDeficienciaActual();
		
			
			return "/zonasegura/proc228/metasEditar?faces-redirect=true";
		} catch (HibernateException e) {
			setMensaje(e.getMessage());
			return "/zonasegura/proc228/metasListar?faces-redirect=true";
		}
		
	}

	public String prepararVer(){
		//TODO: Por implementar carga de la instancia

		try {
			cargarDeficienciaActual();
			return "/zonasegura/proc228/metasVer?faces-redirect=true";
		} catch (HibernateException e) {
			setMensaje(e.getMessage());
			return "/zonasegura/proc228/metasListar?faces-redirect=true";
		}
	}

	public String salvar(){
				
		MetasService service=new MetasService();
		
		try {
				
			service.create(metas);
    	   				
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
						
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
						
		}
		
		metasxPeriodo();
		
		return "/zonasegura/proc228/metasListar?faces-redirect=true";
	}
	
	public String actualizar(){
		
		MetasService service=new MetasService();
		
		try {
			
			service.update(metas);
			
					
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error desconocido.", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			e.printStackTrace();
		}
		
		metasxPeriodo();
		
		return "/zonasegura/proc228/metasListar?faces-redirect=true";
	}
	
	
	public String eliminar(){
		String metasId=Utilidad.getParametro("metasId");
		MetasService service=new MetasService();
		System.out.println("eliminar:" + metasId);
		try {						
			service.delete(new Long(metasId));
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		
		metasxPeriodo();
		
		return "/zonasegura/proc228/metasListar?faces-redirect=true";
	}
	
	
	public void cargarDeficienciaActual(){
		String metasId=Utilidad.getParametro("metasId");
			
		MetasService service=new MetasService();
		
		
		try {
			
			metas=service.ReadById(new Long(metasId));
			
			System.out.println("Carga Deficiencia Actual - Peridod Medicion: " + metas.getMetasId());
			
			
			
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			throw new HibernateException("No se puede cargar Deficiencia.", e);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		
	}
	
	public String metasxPeriodo() {
		MetasService service=new MetasService();
		
		codTipDeficienciaFilter  = null;
		
		
			
			meta = service.getBusquedaxAno(ano);
		
		return "/zonasegura/proc228/metasListar?faces-redirect=true";
	}
	
	
	
	public void  filtroChanged(AjaxBehaviorEvent event) {  
		
		metasxPeriodo();
	}
	
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}


	
	
	void writeOutContent(final HttpServletResponse res, final File content,
			final String theFilename, String contentType) {
		if (content == null)
			return;
		try {
			res.setHeader("Pragma", "no-cache");
			res.setDateHeader("Expires", 0);
			res.setContentType(contentType);
			res.setHeader("Content-disposition", "attachment; filename="
					+ theFilename);
			fastChannelCopy(Channels.newChannel(new FileInputStream(content)),
					Channels.newChannel(res.getOutputStream()));
		} catch (final IOException e) {
			// TODO produce a error message <img
			// src="http://s0.wp.com/wp-includes/images/smilies/icon_smile.gif?m=1129645325g"
			// alt=":)" class="wp-smiley">
		}
	}

	void fastChannelCopy(final ReadableByteChannel src,
			final WritableByteChannel dest) throws IOException {
		final ByteBuffer buffer = ByteBuffer.allocateDirect(1000 * 1024);
		while (src.read(buffer) != -1) {
			buffer.flip();
			dest.write(buffer);
			buffer.compact();
		}
		buffer.flip();
		while (buffer.hasRemaining()) {
			dest.write(buffer);
		}
	}

	

	public List<Metas> getBusquedaxMetas() {
		return busquedaxMetas;
	}

	public void setBusquedaxMetas(List<Metas> busquedaxMetas) {
		this.busquedaxMetas = busquedaxMetas;
	}

	public List<SelectItem> getItemsAno() {
		itemsAno = new ArrayList<SelectItem>();
		Sql sql = new Sql("CALIDAD");
		String s = "SELECT DISTINCT ANO FROM CAL_PERIODO_MEDICION WHERE ESTADO = '1' ORDER BY ANO DESC";
		List<Object[]> listObject = sql.consulta(s, false);
		for (Object[] objects: listObject) {
			itemsAno.add(new SelectItem((String)objects[0],(String)objects[0]));
		}
		return itemsAno;		
	}
	
	public List<SelectItem> getItemsAnoMeta() {
		itemsAnoMeta = new ArrayList<SelectItem>();
		Sql sql = new Sql("CALIDAD");
		String s = "SELECT DISTINCT ANO FROM CAL_METAS WHERE ANO IS NOT NULL ORDER BY ANO DESC";
		List<Object[]> listObject = sql.consulta(s, false);
		for (Object[] objects: listObject) {
			itemsAnoMeta.add(new SelectItem((String)objects[0],(String)objects[0]));
		}
		return itemsAnoMeta;		
	}

	public void setItemsAno(List<SelectItem> itemsAno) {
		this.itemsAno = itemsAno;
	}
	
	

	public void setItemsAnoMeta(List<SelectItem> itemsAnoMeta) {
		this.itemsAnoMeta = itemsAnoMeta;
	}

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	public List<SelectItem> getItemsPrioridad() {
		itemsPrioridad=new ArrayList<SelectItem>();
		
		itemsPrioridad.add(new SelectItem("", "NO ASIGNADO"));
		itemsPrioridad.add(new SelectItem("PRIMERA", "PRIMERA"));
		itemsPrioridad.add(new SelectItem("SEGUNDA", "SEGUNDA"));
		itemsPrioridad.add(new SelectItem("TERCERA", "TERCERA"));
		itemsPrioridad.add(new SelectItem("CUARTA", "CUARTA"));
		itemsPrioridad.add(new SelectItem("QUINTA", "QUINTA"));
		//Modificacion solicitada por Carlos
		itemsPrioridad.add(new SelectItem("SEXTA", "SEXTA"));
		return itemsPrioridad;
	}

	public void setItemsPrioridad(List<SelectItem> itemsPrioridad) {
		this.itemsPrioridad = itemsPrioridad;
	}

	public List<SelectItem> getItemsSectipico() {
	itemsSectipico=new ArrayList<SelectItem>();
	itemsSectipico.add(new SelectItem("2", "2"));
	itemsSectipico.add(new SelectItem("3", "3"));
	itemsSectipico.add(new SelectItem("4", "4"));
	itemsSectipico.add(new SelectItem("5", "5"));
	itemsSectipico.add(new SelectItem("6", "6"));
		return itemsSectipico;
	}

	public void setItemsSectipico(List<SelectItem> itemsSectipico) {
		this.itemsSectipico = itemsSectipico;
	}

	public List<SelectItem> getItemsTipDeficiencia() {
		itemsTipDeficiencia = new ArrayList<SelectItem>();
		Sql sql = new Sql("CALIDAD");
		String s = "SELECT COD_TIP_DEFICIENCIA , DESCRIPCION FROM CAL_TIPO_DEFICIENCIA WHERE ESTADO = '1'  ORDER BY COD_TIP_DEFICIENCIA DESC";
		List<Object[]> listObject = sql.consulta(s, false);
		itemsTipDeficiencia.add(new SelectItem("","NO ASIGNADO"));
		for (Object[] objects: listObject) {
			itemsTipDeficiencia.add(new SelectItem((String)objects[0],(String)objects[0] + ": " +(String)objects[1]));
		}
		return itemsTipDeficiencia;
	}

	public void setItemsTipDeficiencia(List<SelectItem> itemsTipDeficiencia) {
		this.itemsTipDeficiencia = itemsTipDeficiencia;
	}

	public String getCodTipDeficienciaFilter() {
		return codTipDeficienciaFilter;
	}

	public void setCodTipDeficienciaFilter(String codTipDeficienciaFilter) {
		this.codTipDeficienciaFilter = codTipDeficienciaFilter;
	}

	public String getSecTipicoFilter() {
		return secTipicoFilter;
	}

	public void setSecTipicoFilter(String secTipicoFilter) {
		this.secTipicoFilter = secTipicoFilter;
	}

	public String getPrioridadFilter() {
		return prioridadFilter;
	}

	public void setPrioridadFilter(String prioridadFilter) {
		this.prioridadFilter = prioridadFilter;
	}

		
	public Filter<?> getFilterSecTipico() {
        return new Filter<Metas>() {
            public boolean accept(Metas t) {
            	String secTipico = getSecTipicoFilter();
                if (secTipico == null || secTipico.length() == 0 || secTipico.equals(t.getSecTipico())) {
                    return true;
                }
                return false;
            }
        };
    }
	
	public Filter<?> getFilterPrioridad() {
        return new Filter<Metas>() {
            public boolean accept(Metas t) {
            	String prioridad = getPrioridadFilter();
                if (prioridad == null || prioridad.length() == 0 || prioridad.equals(t.getPrioridad())) {
                    return true;
                }
                return false;
            }
        };
    }

	public List<SelectItem> getItemsSecTipico1() {
		itemsSecTipico1=new ArrayList<SelectItem>();
		itemsSecTipico1.add(new SelectItem("", ""));
		itemsSecTipico1.add(new SelectItem("2", "2"));
		itemsSecTipico1.add(new SelectItem("3", "3"));
		itemsSecTipico1.add(new SelectItem("4", "4"));
		itemsSecTipico1.add(new SelectItem("5", "5"));
		itemsSecTipico1.add(new SelectItem("6", "6"));
		return itemsSecTipico1;
	}

	public void setItemsSecTipico1(List<SelectItem> itemsSecTipico1) {
		this.itemsSecTipico1 = itemsSecTipico1;
	}

	public List<SelectItem> getItemsPrioridad1() {
		itemsPrioridad1=new ArrayList<SelectItem>();
		itemsPrioridad1.add(new SelectItem("", ""));
		itemsPrioridad1.add(new SelectItem("PRIMERA", "PRIMERA"));
		itemsPrioridad1.add(new SelectItem("SEGUNDA", "SEGUNDA"));
		itemsPrioridad1.add(new SelectItem("TERCERA", "TERCERA"));
		itemsPrioridad1.add(new SelectItem("CUARTA", "CUARTA"));
		itemsPrioridad1.add(new SelectItem("QUINTA", "QUINTA"));
		itemsPrioridad1.add(new SelectItem("SEXTA", "SEXTA"));
		return itemsPrioridad1;
	}

	public void setItemsPrioridad1(List<SelectItem> itemsPrioridad1) {
		this.itemsPrioridad1 = itemsPrioridad1;
	}
	
	public void tipoDeficienciaChanged(AjaxBehaviorEvent event){
		Sql sql = new Sql("CALIDAD");
				
		if(metas.getCodTipDeficiencia() != null) {
			String s = "SELECT PRIORIDAD FROM CAL_TIPO_DEFICIENCIA WHERE ESTADO = '1' AND COD_TIP_DEFICIENCIA = '" + metas.getCodTipDeficiencia()+ "'";
			Object[] listObject = sql.getFila(s);
			
			metas.setPrioridad((String)listObject[0]);
		}
	}	
	
	public String ActualizarMetas(){
		ConectaDb db = new ConectaDb("CALIDAD");		
        Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        int ctos;
        String result;
		try {
		
			cstmt = cn.prepareCall("{CALL CAL_UTILS.COPIAR_METAS(?)}"); 
							
			cstmt.setString(1,ano.substring(0, 4));
			
			
			
			ctos = cstmt.executeUpdate();
			cstmt.close();
		
			if (ctos == 0) {
				result = "0 filas afectadas";
			}
		
		} catch (SQLException e) {
	        result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	            
	    } finally {
	        try {
	            cn.close();
	        } catch (SQLException e) {
	            result = e.getMessage();
				setMensaje(e.getMessage()+":"+e.getCause());
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	        }
	    }
		
		metasxPeriodo();
		
		return "/zonasegura/proc228/metasListar?faces-redirect=true";
	}
	
	public String calcularMetas(){
		ConectaDb db = new ConectaDb("CALIDAD");		
        Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        int ctos;
        String result;
		try {
		
			cstmt = cn.prepareCall("{CALL CAL_UTILS.CALCULAR_METAS(?)}"); 
							
			cstmt.setString(1,ano.substring(0, 4));
			
			
			
			ctos = cstmt.executeUpdate();
			cstmt.close();
		
			if (ctos == 0) {
				result = "0 filas afectadas";
			}
		
		} catch (SQLException e) {
	        result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	            
	    } finally {
	        try {
	            cn.close();
	        } catch (SQLException e) {
	            result = e.getMessage();
				setMensaje(e.getMessage()+":"+e.getCause());
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	        }
	    }
		
		metasxPeriodo();
		
		return null;
	}
}
