package pe.com.calidad.proc228.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.calidad.proc228.entity.EstadoSubsanacion;
import pe.com.calidad.proc228.service.EstadoSubsanacionService;
import pe.com.calidad.suministro.entity.ModalidadDeteccion;
import pe.com.calidad.suministro.service.ModalidadDeteccionService;

@FacesConverter(value="estadoSubsanacionConverter",forClass=EstadoSubsanacionConverter.class)

public class EstadoSubsanacionConverter implements Converter{

	@Override
	public EstadoSubsanacion getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long estSubsanacionId=new Long(cadena);
		EstadoSubsanacionService service=new EstadoSubsanacionService();
		EstadoSubsanacion estadoSubsanacion=new EstadoSubsanacion();
		estadoSubsanacion=service.ReadById(estSubsanacionId);
		System.out.println(estadoSubsanacion.getEstSubsanacionId());
		return estadoSubsanacion;
	}
	
	

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			EstadoSubsanacion estadoSubsanacion=(EstadoSubsanacion) objeto;
			////System.out.println("objeto:"+new Long(estadoSubsanacion.getEstSubsanacionId()).toString());
			return new Long(estadoSubsanacion.getEstSubsanacionId()).toString();
		}
		return null;
	}

}
