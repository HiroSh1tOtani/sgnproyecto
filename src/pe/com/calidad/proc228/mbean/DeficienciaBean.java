package pe.com.calidad.proc228.mbean;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.HibernateException;
import org.richfaces.component.UIExtendedDataTable;
import org.richfaces.model.Filter;

import pe.com.calidad.proc078.entity.HistoricoDeficienciaAp;
import pe.com.calidad.proc078.service.HistoricoDeficienciaApService;
import pe.com.calidad.proc228.entity.Deficiencia;
import pe.com.calidad.proc228.entity.EstadoSubsanacion;
import pe.com.calidad.proc228.entity.TipoDeficiencia;
import pe.com.calidad.proc228.service.DeficienciaService;
import pe.com.calidad.proc228.service.EstadoSubsanacionService;
import pe.com.calidad.proc228.service.TipoDeficienciaService;
import pe.com.calidad.suministro.entity.CausaInterrupcion;
import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.service.CausaInterrupcionService;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.PeriodoMedicion;
import pe.com.indra.calidad.entity.TipoPunto;
import pe.com.indra.calidad.service.EmpresaService;
import pe.com.indra.calidad.service.MedicionService;
import pe.com.indra.calidad.service.PeriodoMedicionService;
import pe.com.indra.calidad.service.TipoPuntoService;
import pe.com.indra.calidad.util.ConectaDb;
import pe.com.indra.calidad.util.ModificarExcel;
import pe.com.indra.calidad.util.Utilidad;



@ManagedBean(name="deficienciaBean")
@SessionScoped
public class DeficienciaBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<Deficiencia> deficiencias; //programasAp;
	private List<Deficiencia> deficienciasxSuministro;
	private List<Deficiencia> busquedaxDeficiencia;
	private Deficiencia deficiencia;
	private List<SelectItem> itemsPeriodoMedicion;
	private List<SelectItem> itemsPeriodoMedicion1;
	private PeriodoMedicion periodoMedicion;
	private java.util.Date fecDenuncia;
	private java.util.Date fecInspeccion;
	private java.util.Date fecSubsanacion;
	private java.util.Date fecPueServicio;
	
	private List<SelectItem> itemsCodTipInstalacion;
	private List<SelectItem> itemsCodResCumplimiento;
	private List<SelectItem> itemsCodDenunciante;
	private List<SelectItem> itemsCodTipInfractor;
	
	private List<SelectItem> itemsEstadoSubsanacion;
	private List<SelectItem> itemsTipoDeficiencia;

	private String codTipInstalacionFilter;
	private List<SelectItem> itemsCodTipInstalacion1;
	private String codResCumplimientoFilter;
	private List<SelectItem> itemsCodResCumplimiento1;
	private String numSuministroFilter;
	private String codDenuncianteFilter;
	private List<SelectItem> itemsCodDenunciante1;
	private java.util.Date fecDenunciaFilter;
	private java.util.Date fecInspeccionFilter;
	private java.util.Date fecSubsanacionFilter;
	private java.util.Date fecPueServicioFilter;
	private String numSumInfractorFilter;
	private String  numCartaFilter;
	private String codTipInfractorFilter;
	private List<SelectItem> itemsCodTipInfractor1;
	private String estadoSubsanacionFilter;
	private List<SelectItem> itemsEstadoSubsanacion1;
	private String numSuministro;
	private String codIdentificacion;
	private String mensaje;
	private String codIdentificacionFilter;
	private List<SelectItem> itemsSecTipico;
	private long filasTotal;
	private List<Deficiencia> selectionItems = new ArrayList<Deficiencia>();
	private Collection<Object> selection;
	private String codAlimentadorFilter;
	private String secTipicoFilter;
	private List<SelectItem> itemsSecTipico1;
	private List<SelectItem> itemsCodTipInstalacionR1;
	private List<SelectItem> itemsCodTipInstalacionR2;
	private String estadoFilter;
	private List<SelectItem> itemsEstado;
	
	@PostConstruct 
	public void init(){
		System.out.println("se ejecuto PostConstruct "+new java.util.Date());
		deficiencia=new Deficiencia();
		deficiencia.setEstado("1");
		
	}
	
	public Collection<Object> getSelection() {
		return selection;
	}

	public void setSelection(Collection<Object> selection) {
		this.selection = selection;
	}
	
	public List<Deficiencia> getSelectionItems() {
		return selectionItems;
	}

	public void setSelectionItems(List<Deficiencia> selectionItems) {
		this.selectionItems = selectionItems;
	}	
	
	
	
	public List<Deficiencia> getDeficiencias() {
		return deficiencias;
	}



	public void setDeficiencias(List<Deficiencia> deficiencias) {
		this.deficiencias = deficiencias;
	}



	public Deficiencia getDeficiencia() {
		return deficiencia;
	}



	public void setDeficiencia(Deficiencia deficiencia) {
		this.deficiencia = deficiencia;
	}


	
	public List<SelectItem> getItemsPeriodoMedicion() {
		PeriodoMedicionService service=new PeriodoMedicionService();
		itemsPeriodoMedicion = new ArrayList<SelectItem>();
		for(PeriodoMedicion tp:service.getAllRows()){
			itemsPeriodoMedicion.add(new SelectItem(tp,tp.getAno()+"-"+tp.getPeriodo()));
		}
		return itemsPeriodoMedicion;
	}
	
	public void setItemsPeriodoMedicion(List<SelectItem> itemsPeriodoMedicion) {
		this.itemsPeriodoMedicion = itemsPeriodoMedicion;
	}

	public List<SelectItem> getItemsPeriodoMedicion1() {
		PeriodoMedicionService service=new PeriodoMedicionService();
		itemsPeriodoMedicion1 = new ArrayList<SelectItem>();
		for(PeriodoMedicion tp:service.getAllRows1()){
			itemsPeriodoMedicion1.add(new SelectItem(tp,tp.getAno()+"-"+tp.getPeriodo()));
		}
		return itemsPeriodoMedicion1;
	}
	
	public void setItemsPeriodoMedicion1(List<SelectItem> itemsPeriodoMedicion1) {
		this.itemsPeriodoMedicion1 = itemsPeriodoMedicion1;
	}

	
	public String prepararCrear(){
		//Esto funciona siempre que el bean tenga ambito de sesion 
		setDeficiencia(new Deficiencia());
		getDeficiencia().setEstado("1");
		getDeficiencia().setPeriodoMedicion(periodoMedicion);
		
		//System.out.println("Carga  - Deficiencias: " + deficiencias.size());
		
		return "/zonasegura/proc228/deficienciaCrear?faces-redirect=true";
	}
	
	public String prepararEditar(){
		
		
		try {
			cargarDeficienciaActual();
		
			if(deficiencia.getEstado().equals("0")){
				return "/zonasegura/proc228/deficienciaVer?faces-redirect=true";
			}else {
				return "/zonasegura/proc228/deficienciaEditar?faces-redirect=true";
			}
			
			
		} catch (HibernateException e) {
			setMensaje(e.getMessage());
			return "/zonasegura/proc228/deficienciaListar?faces-redirect=true";
		}
		
	}

	public String prepararVer(){
		//TODO: Por implementar carga de la instancia

		try {
			cargarDeficienciaActual();
			return "/zonasegura/proc228/deficienciaVer?faces-redirect=true";
		} catch (HibernateException e) {
			setMensaje(e.getMessage());
			return "/zonasegura/proc228/deficienciaListar?faces-redirect=true";
		}
	}

	public String salvar(){
				
		DeficienciaService service=new DeficienciaService();
		
		try {
				
			if(fecDenuncia!=null){
				deficiencia.setFecDenuncia(new java.sql.Date(fecDenuncia.getTime()));
			}else {
				deficiencia.setFecDenuncia(null);
			}
			if(fecInspeccion!=null){
				deficiencia.setFecInspeccion(new java.sql.Date(fecInspeccion.getTime()));
			}else {
				deficiencia.setFecInspeccion(null);
			}
			if(fecSubsanacion!=null){
				deficiencia.setFecSubsanacion(new java.sql.Date(fecSubsanacion.getTime()));
			}else {
				deficiencia.setFecSubsanacion(null);
			}
			if(fecPueServicio!=null){
				deficiencia.setFecPueServicio(new java.sql.Date(fecPueServicio.getTime()));
			}else{
				deficiencia.setFecPueServicio(null);
			}
    		service.create(deficiencia);
    		deficiencia.setCodIdentificacion("D" + String.valueOf(deficiencia.getDeficienciaId()));
    		service.update(deficiencia);
    	   				
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
						
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
						
		}
		
		DeficienciaxPeriodo();
		
		return "/zonasegura/proc228/deficienciaListar?faces-redirect=true";
	}
	
	public String actualizar(){
		
		DeficienciaService service=new DeficienciaService();
		
		try {
			
			if(fecDenuncia!=null){
				deficiencia.setFecDenuncia(new java.sql.Date(fecDenuncia.getTime()));
			}else {
				deficiencia.setFecDenuncia(null);
			}
			if(fecInspeccion!=null){
				deficiencia.setFecInspeccion(new java.sql.Date(fecInspeccion.getTime()));
			}else {
				deficiencia.setFecInspeccion(null);
			}
			if(fecSubsanacion!=null){
				deficiencia.setFecSubsanacion(new java.sql.Date(fecSubsanacion.getTime()));
			}else {
				deficiencia.setFecSubsanacion(null);
			}
			if(fecPueServicio!=null){
				deficiencia.setFecPueServicio(new java.sql.Date(fecPueServicio.getTime()));
			}else{
				deficiencia.setFecPueServicio(null);
			}
			service.update(deficiencia);
			
					
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error desconocido.", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			e.printStackTrace();
		}
		
		DeficienciaxPeriodo();
		
		return "/zonasegura/proc228/deficienciaListar?faces-redirect=true";
	}
	
	
	public String eliminar(){
		String deficienciaId=Utilidad.getParametro("deficienciaId");
		DeficienciaService service=new DeficienciaService();
		
		
		try {
			deficiencia=service.ReadById(new Long(deficienciaId));
			deficiencia.setEstado("0");
			
						
			service.update(deficiencia);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		
		DeficienciaxPeriodo();
		
		return "/zonasegura/proc228/deficienciaListar?faces-redirect=true";
	}
	
	
	public void cargarDeficienciaActual(){
		String deficienciaId=Utilidad.getParametro("deficienciaId");
			
		DeficienciaService service=new DeficienciaService();
		
		
		try {
			
			deficiencia=service.ReadById(new Long(deficienciaId));
			
			System.out.println("Carga Deficiencia Actual - Peridod Medicion: " + deficiencia.getPeriodoMedicion().getPerMedicionId());
			
			if(deficiencia.getFecDenuncia()!=null) {
				fecDenuncia = new java.util.Date( deficiencia.getFecDenuncia().getTime());				
			}else {
				fecDenuncia = null;
			}
			if(deficiencia.getFecInspeccion()!=null) {
				fecInspeccion = new java.util.Date( deficiencia.getFecInspeccion().getTime());				
			}else {
				fecInspeccion = null;
			}
			if(deficiencia.getFecSubsanacion()!=null) {
				fecSubsanacion = new java.util.Date( deficiencia.getFecSubsanacion().getTime());				
			}else {
				fecSubsanacion = null;
			}
			if(deficiencia.getFecPueServicio()!=null){
				fecPueServicio = new java.util.Date(deficiencia.getFecPueServicio().getTime());
			}else{
				fecPueServicio = null;
			}
			
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			throw new HibernateException("No se puede cargar Deficiencia.", e);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		
	}
	
	public String DeficienciaxPeriodo() {
		DeficienciaService service=new DeficienciaService();
		
		codTipInstalacionFilter  = null;
		codResCumplimientoFilter  = null;
		numSuministroFilter = null;
		codDenuncianteFilter = null;
		fecDenunciaFilter = null;
		fecInspeccionFilter = null;
		fecSubsanacionFilter = null;
		fecPueServicioFilter = null;
		numSumInfractorFilter = null;
		numCartaFilter = null;
		codTipInfractorFilter = null;
		estadoSubsanacionFilter = null;
		codIdentificacionFilter = null;
		codAlimentadorFilter = null;
		secTipicoFilter = null;
		estadoFilter = null;
		
		//System.out.println("perido:"+ getPeriodoMedicion().getPerMedicionId());
		
		if(periodoMedicion.getPerMedicionId() > 0) {
			
			deficiencias = service.getAllRows(periodoMedicion.getPerMedicionId());
		}
		return "/zonasegura/proc228/deficienciaListar?faces-redirect=true";
	}
	
	/*---original---
	 * public String DeficienciaxPeriodo() {
		DeficienciaService service=new DeficienciaService();
		
		codTipInstalacionFilter  = null;
		codResCumplimientoFilter  = null;
		numSuministroFilter = null;
		codDenuncianteFilter = null;
		fecDenunciaFilter = null;
		fecInspeccionFilter = null;
		fecSubsanacionFilter = null;
		fecPueServicioFilter = null;
		numSumInfractorFilter = null;
		numCartaFilter = null;
		codTipInfractorFilter = null;
		estadoSubsanacionFilter = null;
		codIdentificacionFilter = null;
		codAlimentadorFilter = null;
		secTipicoFilter =null;
		
		System.out.println("perido:"+ getPeriodoMedicion().getPerMedicionId());
		
		if(periodoMedicion.getPerMedicionId() > 0) {
			
			deficiencias = service.getAllRows1(periodoMedicion.getPerMedicionId());
		}
		return "/zonasegura/proc228/deficienciaListar?faces-redirect=true";
	}
	 */
	
	public void  filtroChanged(AjaxBehaviorEvent event) {  
		
		DeficienciaxPeriodo();
	}
	
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}


	public PeriodoMedicion getPeriodoMedicion() {
		return periodoMedicion;
	}


	public void setPeriodoMedicion(PeriodoMedicion periodoMedicion) {
		this.periodoMedicion = periodoMedicion;
	}



	public java.util.Date getFecSubsanacion() {
		return fecSubsanacion;
	}

	public void setFecSubsanacion(java.util.Date fecSubsanacion) {
		this.fecSubsanacion = fecSubsanacion;
	}

	public java.util.Date getFecDenuncia() {
		return fecDenuncia;
	}

	public void setFecDenuncia(java.util.Date fecDenuncia) {
		this.fecDenuncia = fecDenuncia;
	}

	public java.util.Date getFecInspeccion() {
		return fecInspeccion;
	}

	public void setFecInspeccion(java.util.Date fecInspeccion) {
		this.fecInspeccion = fecInspeccion;
	}

	public java.util.Date getFecPueServicio() {
		return fecPueServicio;
	}

	public void setFecPueServicio(java.util.Date fecPueServicio) {
		this.fecPueServicio = fecPueServicio;
	}

	public List<SelectItem> getItemsCodTipInstalacion() {
		itemsCodTipInstalacion=new ArrayList<SelectItem>();
		itemsCodTipInstalacion.add(new SelectItem("1", "SED"));
		itemsCodTipInstalacion.add(new SelectItem("2", "EMT"));
		itemsCodTipInstalacion.add(new SelectItem("3", "TMT"));
		itemsCodTipInstalacion.add(new SelectItem("4", "EBT"));
		itemsCodTipInstalacion.add(new SelectItem("5", "TBT"));
		return itemsCodTipInstalacion;
	}

	public void setItemsCodTipInstalacion(List<SelectItem> itemsCodTipInstalacion) {
		this.itemsCodTipInstalacion = itemsCodTipInstalacion;
	}

	public List<SelectItem> getItemsCodResCumplimiento() {
		itemsCodResCumplimiento=new ArrayList<SelectItem>();
		itemsCodResCumplimiento.add(new SelectItem("0", "CONCESIONARIA"));
		itemsCodResCumplimiento.add(new SelectItem("1", "TERCERO"));
		return itemsCodResCumplimiento;
	}

	public void setItemsCodResCumplimiento(List<SelectItem> itemsCodResCumplimiento) {
		this.itemsCodResCumplimiento = itemsCodResCumplimiento;
	}

	public List<SelectItem> getItemsCodDenunciante() {
		itemsCodDenunciante=new ArrayList<SelectItem>();
		itemsCodDenunciante.add(new SelectItem("1", "CONCESIONARIA"));
		itemsCodDenunciante.add(new SelectItem("2", "USUARIO"));
		itemsCodDenunciante.add(new SelectItem("3", "AUTORIDADES"));
		itemsCodDenunciante.add(new SelectItem("4", "OSINERGMIN"));
		return itemsCodDenunciante;
	}

	public void setItemsCodDenunciante(List<SelectItem> itemsCodDenunciante) {
		this.itemsCodDenunciante = itemsCodDenunciante;
	}

	public List<SelectItem> getItemsEstadoSubsanacion() {
		EstadoSubsanacionService service = new EstadoSubsanacionService();
		itemsEstadoSubsanacion = new ArrayList<SelectItem>();
		for(EstadoSubsanacion tp:service.getAllRows1()){
			itemsEstadoSubsanacion.add(new SelectItem(tp,tp.getCodEstSubsanacion()+"-"+tp.getDescripcion()));
		}
		return itemsEstadoSubsanacion;
	}

	public void setItemsEstadoSubsanacion(List<SelectItem> itemsEstadoSubsanacion) {
		this.itemsEstadoSubsanacion = itemsEstadoSubsanacion;
	}

	public List<SelectItem> getItemsCodTipInfractor() {
		itemsCodTipInfractor=new ArrayList<SelectItem>();
		itemsCodTipInfractor.add(new SelectItem("0", "OTRAS"));
		itemsCodTipInfractor.add(new SelectItem("1", "EMPRESA DE COMUNICACIONES"));
		return itemsCodTipInfractor;
	}

	public void setItemsCodTipInfractor(List<SelectItem> itemsCodTipInfractor) {
		this.itemsCodTipInfractor = itemsCodTipInfractor;
	}

	public List<SelectItem> getItemsTipoDeficiencia() {

		TipoDeficienciaService service = new TipoDeficienciaService();
		itemsTipoDeficiencia = new ArrayList<SelectItem>();
		for(TipoDeficiencia tp:service.getAllRows1()){
			itemsTipoDeficiencia.add(new SelectItem(tp,tp.getTipoInstalacion().getDescripcion()+": "+tp.getCodTipDeficiencia()+"-"+tp.getDescripcion()));
		}
		return itemsTipoDeficiencia;
	}

	public void setItemsTipoDeficiencia(List<SelectItem> itemsTipoDeficiencia) {
		this.itemsTipoDeficiencia = itemsTipoDeficiencia;
	}

	public String getCodTipInstalacionFilter() {
		return codTipInstalacionFilter;
	}

	public void setCodTipInstalacionFilter(String codTipInstalacionFilter) {
		this.codTipInstalacionFilter = codTipInstalacionFilter;
	}

	public Filter<?> getFiltercodTipInstalacion() {
        return new Filter<Deficiencia>() {
            public boolean accept(Deficiencia t) {
            	String codTipInstalacion = getCodTipInstalacionFilter();
                if (codTipInstalacion == null || codTipInstalacion.length() == 0 || codTipInstalacion.equals(t.getCodTipInstalacion())) {
                    return true;
                }
                return false;
            }
        };
    }

	public List<SelectItem> getItemsCodTipInstalacion1() {
		itemsCodTipInstalacion1=new ArrayList<SelectItem>();
		itemsCodTipInstalacion1.add(new SelectItem("", ""));
		itemsCodTipInstalacion1.add(new SelectItem("1", "1-SED"));
		itemsCodTipInstalacion1.add(new SelectItem("2", "2-EMT"));
		itemsCodTipInstalacion1.add(new SelectItem("3", "3-TMT"));
		itemsCodTipInstalacion1.add(new SelectItem("4", "4-EBT"));
		itemsCodTipInstalacion1.add(new SelectItem("5", "5-TBT"));
		return itemsCodTipInstalacion1;
	}

	public void setItemsCodTipInstalacion1(List<SelectItem> itemsCodTipInstalacion1) {
		this.itemsCodTipInstalacion1 = itemsCodTipInstalacion1;
	}

	public String getCodResCumplimientoFilter() {
		return codResCumplimientoFilter;
	}

	public void setCodResCumplimientoFilter(String codResCumplimientoFilter) {
		this.codResCumplimientoFilter = codResCumplimientoFilter;
	}

	public Filter<?> getFiltercodResCumplimiento() {
        return new Filter<Deficiencia>() {
            public boolean accept(Deficiencia t) {
            	String codResCumplimiento = getCodResCumplimientoFilter();
                if (codResCumplimiento == null || codResCumplimiento.length() == 0 || codResCumplimiento.equals(t.getCodResCumplimiento())) {
                    return true;
                }
                return false;
            }
        };
    }

	public List<SelectItem> getItemsCodResCumplimiento1() {
		itemsCodResCumplimiento1=new ArrayList<SelectItem>();
		itemsCodResCumplimiento1.add(new SelectItem("", ""));
		itemsCodResCumplimiento1.add(new SelectItem("0", "0-CONCESIONARIA"));
		itemsCodResCumplimiento1.add(new SelectItem("1", "1-TERCERO"));
		return itemsCodResCumplimiento1;
	}

	public void setItemsCodResCumplimiento1(
			List<SelectItem> itemsCodResCumplimiento1) {
		this.itemsCodResCumplimiento1 = itemsCodResCumplimiento1;
	}

	public String getNumSuministroFilter() {
		return numSuministroFilter;
	}

	public void setNumSuministroFilter(String numSuministroFilter) {
		this.numSuministroFilter = numSuministroFilter;
	}

	public String getCodDenuncianteFilter() {
		return codDenuncianteFilter;
	}

	public void setCodDenuncianteFilter(String codDenuncianteFilter) {
		this.codDenuncianteFilter = codDenuncianteFilter;
	}

	public Filter<?> getFiltercodDenunciante() {
        return new Filter<Deficiencia>() {
            public boolean accept(Deficiencia t) {
            	String codDenunciante = getCodDenuncianteFilter();
                if (codDenunciante == null || codDenunciante.length() == 0 || codDenunciante.equals(t.getCodDenunciante())) {
                    return true;
                }
                return false;
            }
        };
    }
	
	public List<SelectItem> getItemsCodDenunciante1() {
		itemsCodDenunciante1=new ArrayList<SelectItem>();
		itemsCodDenunciante1.add(new SelectItem("", ""));
		itemsCodDenunciante1.add(new SelectItem("1", "1-CONCESIONARIA"));
		itemsCodDenunciante1.add(new SelectItem("2", "2-USUARIO"));
		itemsCodDenunciante1.add(new SelectItem("3", "3-AUTORIDADES"));
		itemsCodDenunciante1.add(new SelectItem("4", "4-OSINERGMIN"));
		return itemsCodDenunciante1;
	}

	public void setItemsCodDenunciante1(List<SelectItem> itemsCodDenunciante1) {
		this.itemsCodDenunciante1 = itemsCodDenunciante1;
	}

	public java.util.Date getFecDenunciaFilter() {
		return fecDenunciaFilter;
	}

	public void setFecDenunciaFilter(java.util.Date fecDenunciaFilter) {
		this.fecDenunciaFilter = fecDenunciaFilter;
	}

	public java.util.Date getFecInspeccionFilter() {
		return fecInspeccionFilter;
	}

	public void setFecInspeccionFilter(java.util.Date fecInspeccionFilter) {
		this.fecInspeccionFilter = fecInspeccionFilter;
	}

	public java.util.Date getFecSubsanacionFilter() {
		return fecSubsanacionFilter;
	}

	public void setFecSubsanacionFilter(java.util.Date fecSubsanacionFilter) {
		this.fecSubsanacionFilter = fecSubsanacionFilter;
	}

	public java.util.Date getFecPueServicioFilter() {
		return fecPueServicioFilter;
	}

	public void setFecPueServicioFilter(java.util.Date fecPueServicioFilter) {
		this.fecPueServicioFilter = fecPueServicioFilter;
	}
	
	public Filter<?> getFilterFecDenuncia() {
        return new Filter<Deficiencia>() {
            public boolean accept(Deficiencia deficiencia) {
                Date fechaDenuncia = getFecDenunciaFilter();
                if (fechaDenuncia == null) {
                	
                	return true;
                }else {
                	
                	Calendar calendar1 = Calendar.getInstance();
                	Calendar calendar2 = Calendar.getInstance();
                	
                	calendar1.setTime(fechaDenuncia);
                	
                	if(deficiencia.getFecDenuncia() == null){
                		return false;
                	}
                	
                	calendar2.setTime(deficiencia.getFecDenuncia()); 
                	
                	if(calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR) && calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH) &&
                			calendar1.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH)){
                		
                		return true;
                		
                	}else{
                		return false;
                	}
                    
                }
            }
        };
	}
	
	public Filter<?> getFilterFecInspeccion() {
        return new Filter<Deficiencia>() {
            public boolean accept(Deficiencia deficiencia) {
                Date fechaInspeccion = getFecInspeccionFilter();
                if (fechaInspeccion == null) {
                	
                	return true;
                }else {
                	
                	Calendar calendar1 = Calendar.getInstance();
                	Calendar calendar2 = Calendar.getInstance();
                	
                	calendar1.setTime(fechaInspeccion);
                	
                	if(deficiencia.getFecInspeccion() == null){
                		return false;
                	}
                	
                	calendar2.setTime(deficiencia.getFecInspeccion()); 
                	
                	if(calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR) && calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH) &&
                			calendar1.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH)){
                		
                		return true;
                		
                	}else{
                		return false;
                	}
                    
                }
            }
        };
	}
	
	public Filter<?> getFilterFecSubsanacion() {
        return new Filter<Deficiencia>() {
            public boolean accept(Deficiencia deficiencia) {
                Date fechaSubsanacion = getFecSubsanacionFilter();
                if (fechaSubsanacion == null) {
                	
                	return true;
                }else {
                	
                	Calendar calendar1 = Calendar.getInstance();
                	Calendar calendar2 = Calendar.getInstance();
                	
                	calendar1.setTime(fechaSubsanacion);
                	
                	if(deficiencia.getFecSubsanacion() == null){
                		return false;
                	}
                	
                	calendar2.setTime(deficiencia.getFecSubsanacion()); 
                	
                	if(calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR) && calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH) &&
                			calendar1.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH)){
                		
                		return true;
                		
                	}else{
                		return false;
                	}
                    
                }
            }
        };
	}
	
	public Filter<?> getFilterFecPueServicio() {
        return new Filter<Deficiencia>() {
            public boolean accept(Deficiencia deficiencia) {
                Date fechaServicio = getFecPueServicioFilter();
                if (fechaServicio == null) {
                	
                	return true;
                }else {
                	
                	Calendar calendar1 = Calendar.getInstance();
                	Calendar calendar2 = Calendar.getInstance();
                	
                	calendar1.setTime(fechaServicio);
                	
                	if(deficiencia.getFecPueServicio() == null){
                		return false;
                	}
                	
                	calendar2.setTime(deficiencia.getFecPueServicio()); 
                	
                	if(calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR) && calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH) &&
                			calendar1.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH)){
                		
                		return true;
                		
                	}else{
                		return false;
                	}
                    
                }
            }
        };
	}

	public String getNumSumInfractorFilter() {
		return numSumInfractorFilter;
	}

	public void setNumSumInfractorFilter(String numSumInfractorFilter) {
		this.numSumInfractorFilter = numSumInfractorFilter;
	}

	public String getNumCartaFilter() {
		return numCartaFilter;
	}

	public void setNumCartaFilter(String numCartaFilter) {
		this.numCartaFilter = numCartaFilter;
	}

	public String getCodTipInfractorFilter() {
		return codTipInfractorFilter;
	}

	public void setCodTipInfractorFilter(String codTipInfractorFilter) {
		this.codTipInfractorFilter = codTipInfractorFilter;
	}
	
	public Filter<?> getFiltercodTipInfractor() {
        return new Filter<Deficiencia>() {
            public boolean accept(Deficiencia t) {
            	String codTipInfractor = getCodTipInfractorFilter();
                if (codTipInfractor == null || codTipInfractor.length() == 0 || codTipInfractor.equals(t.getCodTipInfractor())) {
                    return true;
                }
                return false;
            }
        };
    }

	public List<SelectItem> getItemsCodTipInfractor1() {
		itemsCodTipInfractor1=new ArrayList<SelectItem>();
		itemsCodTipInfractor1.add(new SelectItem("", ""));
		itemsCodTipInfractor1.add(new SelectItem("0", "0-OTRAS"));
		itemsCodTipInfractor1.add(new SelectItem("1", "1-EMPRESA DE COMUNICACIONES"));
		return itemsCodTipInfractor1;
	}

	public void setItemsCodTipInfractor1(List<SelectItem> itemsCodTipInfractor1) {
		this.itemsCodTipInfractor1 = itemsCodTipInfractor1;
	}

	public String getEstadoSubsanacionFilter() {
		return estadoSubsanacionFilter;
	}

	public void setEstadoSubsanacionFilter(String estadoSubsanacionFilter) {
		this.estadoSubsanacionFilter = estadoSubsanacionFilter;
	}
	
	public Filter<?> getFilterEstadoSubsanacion() {
        return new Filter<Deficiencia>() {
            public boolean accept(Deficiencia t) {
            	String estadoSubsanacion = getEstadoSubsanacionFilter();
                if (estadoSubsanacion == null || estadoSubsanacion.length() == 0 || estadoSubsanacion.equals(t.getEstadoSubsanacion().getCodEstSubsanacion())) {
                    return true;
                }
                return false;
            }
        };
    }


	public List<SelectItem> getItemsEstadoSubsanacion1() {
		EstadoSubsanacionService service = new EstadoSubsanacionService();
		itemsEstadoSubsanacion1 = new ArrayList<SelectItem>();
		itemsEstadoSubsanacion1.add(new SelectItem("", ""));
		for(EstadoSubsanacion tp:service.getAllRows1()){
			itemsEstadoSubsanacion1.add(new SelectItem(tp.getCodEstSubsanacion(),tp.getCodEstSubsanacion()+"-"+tp.getDescripcion()));
		}
		return itemsEstadoSubsanacion1;
	}

	public void setItemsEstadoSubsanacion1(List<SelectItem> itemsEstadoSubsanacion1) {
		this.itemsEstadoSubsanacion1 = itemsEstadoSubsanacion1;
	}
	
	public String exportarDeficiencias(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
	    
	    String rutaArchivoOut;
	    String rutaUp;
	    rutaUp = facesContext.getExternalContext().getRealPath("/reportes/AnexoDeficiencias.xlsx");
	    String path = System.getProperty("uploads.folder");
	    String nombreArchivo;
	    
	    System.out.print(path + "\n");
	    
	    	    
	    nombreArchivo = "Deficiencia" + "_" + getDeficiencia().getCodDenunciante()  + "_"+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsx";
	    rutaArchivoOut = path + nombreArchivo;
	    	    
	    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);

		XSSFSheet objHssfSheet = null;
		
		try  {
	 
	 //   HistoricoDeficienciaApService service = new HistoricoDeficienciaApService();
		DeficienciaService service = new DeficienciaService();
	    
	    objHssfSheet = objHssfWorkbook.getSheetAt(0);
		
		int intRow = 1;
		int intCell = 0;
		
		EmpresaService empService = new EmpresaService();
		
		for (Deficiencia deficiencia : service.getAllRows1(periodoMedicion.getPerMedicionId())) {	
			
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell, deficiencia.getCodIdentificacion());//codIdentificacion
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 1, deficiencia.getCodTipInstalacion());//codTipInstalacion
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 2, deficiencia.getCodInstalacion());//codInstalacion
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 3, deficiencia.getCodResCumplimiento());//codResCumplimiento
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 4, deficiencia.getNumSuministro());//numSuministro
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 5, deficiencia.getCodDenunciante());//codDenunciante
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 6, deficiencia.getFecDenuncia());//fecDenuncia
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 7, deficiencia.getFecInspeccion());//fecInspeccion
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 8, deficiencia.getFecSubsanacion());//fecSubsanacion
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 9, deficiencia.getObservaciones());//observaciones
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 10, deficiencia.getReferencia1());//referencia1
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 11, deficiencia.getReferencia2());//referencia2
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 12, deficiencia.getUtmEste());//utmEste
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 13, deficiencia.getUtmNorte());//utmNorte
				if(deficiencia.getEstadoSubsanacion() != null){
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 14, deficiencia.getEstadoSubsanacion().getCodEstSubsanacion()+"-"+deficiencia.getEstadoSubsanacion().getDescripcion());
				}
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 15, deficiencia.getCodTipInfractor());//codTipInfractor
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 16, deficiencia.getNomInfractor());//nomInfractor
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 17, deficiencia.getDirInfractor());//dirInfractor
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 18, deficiencia.getRepLegInfractor());//repLegInfractor
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 19, deficiencia.getNumSumInfractor());//numSumInfractor
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 20, deficiencia.getFecPueServicio());//fecPueServicio
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 21, deficiencia.getMotDeficiencia());//motDeficiencia
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 22, deficiencia.getNumCarta());//numCarta
				if(deficiencia.getTipoDeficiencia() != null){
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 23, deficiencia.getTipoDeficiencia().getCodTipDeficiencia()+"-"+deficiencia.getTipoDeficiencia().getDescripcion());
				}
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 24, deficiencia.getCodAlimentador());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 25, deficiencia.getSecTipico());
				
				intRow++;
				

		}
		
		ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());

	    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
	    facesContext.responseComplete();	
				
		return null;
	}
	
	public String exportarDeficienciasMeta(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
	    
	    String rutaArchivoOut;
	    String rutaUp;
	    rutaUp = facesContext.getExternalContext().getRealPath("/reportes/AnexoDeficienciasMetas.xlsx");
	    String path = System.getProperty("uploads.folder");
	    String nombreArchivo;
	    
	    System.out.print(path + "\n");
	    
	    	    
	    nombreArchivo = "Deficiencia" + "_" + getDeficiencia().getCodDenunciante()  + "_"+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsx";
	    rutaArchivoOut = path + nombreArchivo;
	    	    
	    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);

		XSSFSheet objHssfSheet = null;
		
		try  {
	 
	 //   HistoricoDeficienciaApService service = new HistoricoDeficienciaApService();
		DeficienciaService service = new DeficienciaService();
	    
	    objHssfSheet = objHssfWorkbook.getSheetAt(0);
		
		int intRow = 1;
		int intCell = 0;
		
		EmpresaService empService = new EmpresaService();
		
		for (Deficiencia deficiencia : service.getAllRowsEstado(periodoMedicion.getAno())) {	
			
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell, deficiencia.getCodIdentificacion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 1, deficiencia.getCodTipInstalacion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 2, deficiencia.getCodInstalacion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 3, deficiencia.getCodResCumplimiento());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 4, deficiencia.getNumSuministro());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 5, deficiencia.getCodDenunciante());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 6, deficiencia.getFecDenuncia());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 7, deficiencia.getFecInspeccion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 8, deficiencia.getFecSubsanacion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 9, deficiencia.getObservaciones());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 10, deficiencia.getReferencia1());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 11, deficiencia.getReferencia2());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 12, deficiencia.getUtmEste());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 13, deficiencia.getUtmNorte());
					if(deficiencia.getEstadoSubsanacion() != null){
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 14, deficiencia.getEstadoSubsanacion().getCodEstSubsanacion()+"-"+deficiencia.getEstadoSubsanacion().getDescripcion());
					}
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 15, deficiencia.getCodTipInfractor());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 16, deficiencia.getNomInfractor());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 17, deficiencia.getDirInfractor());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 18, deficiencia.getRepLegInfractor());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 19, deficiencia.getNumSumInfractor());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 20, deficiencia.getFecPueServicio());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 21, deficiencia.getMotDeficiencia());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 22, deficiencia.getNumCarta());
					if(deficiencia.getTipoDeficiencia() != null){
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 23, deficiencia.getTipoDeficiencia().getCodTipDeficiencia()+"-"+deficiencia.getTipoDeficiencia().getDescripcion());
					}
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 24, deficiencia.getCodAlimentador());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 25, deficiencia.getSecTipico());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 26, deficiencia.getMeta());
					/*
					if(deficiencia.getTipoDeficiencia() != null){
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 27, deficiencia.getTipoDeficiencia().getPrioridad());
					}*/
					if(deficiencia.getTipoMeta() != null){
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 27, deficiencia.getTipoMeta().getPrioridad());
					}
					intRow++;
			

		}
		
		ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());

	    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
	    facesContext.responseComplete();	
				
		return null;
	}
	
	
	void writeOutContent(final HttpServletResponse res, final File content,
			final String theFilename, String contentType) {
		if (content == null)
			return;
		try {
			res.setHeader("Pragma", "no-cache");
			res.setDateHeader("Expires", 0);
			res.setContentType(contentType);
			res.setHeader("Content-disposition", "attachment; filename="
					+ theFilename);
			fastChannelCopy(Channels.newChannel(new FileInputStream(content)),
					Channels.newChannel(res.getOutputStream()));
		} catch (final IOException e) {
			// TODO produce a error message <img
			// src="http://s0.wp.com/wp-includes/images/smilies/icon_smile.gif?m=1129645325g"
			// alt=":)" class="wp-smiley">
		}
	}

	void fastChannelCopy(final ReadableByteChannel src,
			final WritableByteChannel dest) throws IOException {
		final ByteBuffer buffer = ByteBuffer.allocateDirect(1000 * 1024);
		while (src.read(buffer) != -1) {
			buffer.flip();
			dest.write(buffer);
			buffer.compact();
		}
		buffer.flip();
		while (buffer.hasRemaining()) {
			dest.write(buffer);
		}
	}

	public String BusquedaxDeficiencia() {
		DeficienciaService service=new DeficienciaService();
		
		try {
			
			if(!codIdentificacion.trim().equals("")) {
				busquedaxDeficiencia= service.getBusquedaxDeficiencia(codIdentificacion);		
			} else {
				busquedaxDeficiencia = null;
			}
				
			
		} catch (HibernateException e) {
			setMensaje(e.getMessage());
		}
		
		
		return "/zonasegura/proc228/deficienciaxSuministroListar?faces-redirect=true";
	}

	public String getNumSuministro() {
		return numSuministro;
	}

	public void setNumSuministro(String numSuministro) {
		this.numSuministro = numSuministro;
	}

	public List<Deficiencia> getDeficienciasxSuministro() {
		return deficienciasxSuministro;
	}

	public void setDeficienciasxSuministro(List<Deficiencia> deficienciasxSuministro) {
		this.deficienciasxSuministro = deficienciasxSuministro;
	}

	public List<Deficiencia> getBusquedaxDeficiencia() {
		return busquedaxDeficiencia;
	}

	public void setBusquedaxDeficiencia(List<Deficiencia> busquedaxDeficiencia) {
		this.busquedaxDeficiencia = busquedaxDeficiencia;
	}

	public String getCodIdentificacion() {
		return codIdentificacion;
	}

	public void setCodIdentificacion(String codIdentificacion) {
		this.codIdentificacion = codIdentificacion;
	}
	
	public void obtenerCoordenada(){
		String result;
        ConectaDb db = new ConectaDb("CALIDAD");
        
        Connection cn = db.getConnection();
        BigDecimal utmEste;
        BigDecimal utmNorte;
        String coordenada = null;
        String codAlimentador = null;
        String secTipico = null;
                
        CallableStatement cstmt = null;  
        try {

            //la variable deficiencia debe tener la deficiencia que se esta editando o creando
            cstmt = cn.prepareCall("{CALL ? := CAL_SUMINISTRO_PACKAGE.FN_OBTENER_COORDENADA2(?,?,?,?,?,?)}");

            if(!deficiencia.getCodTipInstalacion().equals("3") && !deficiencia.getCodTipInstalacion().equals("5")) {
                    
                    cstmt.registerOutParameter(1, Types.VARCHAR);
                    cstmt.setString(2, deficiencia.getCodTipInstalacion());
                    cstmt.setString(3, deficiencia.getCodInstalacion());
                    cstmt.setString(4, null);
                    cstmt.setString(5, null);
                    cstmt.setString(6, null);
                    cstmt.setString(7, null);
            }
            else{
                    cstmt.registerOutParameter(1, Types.VARCHAR);
                    cstmt.setString(2, deficiencia.getCodTipInstalacion());
                    cstmt.setString(3, deficiencia.getCodInstalacion());
                    cstmt.setString(4, deficiencia.getCodTipInstalacionR1());
                    cstmt.setString(5, deficiencia.getReferencia1());
                    cstmt.setString(6, deficiencia.getCodTipInstalacionR2());
                    cstmt.setString(7, deficiencia.getReferencia2());
            }
                        
            cstmt.execute();
            coordenada= cstmt.getString(1);
            cstmt.close();
            
            String lista[] = coordenada.split(":");
            /*
            utmEste = BigDecimal.valueOf(Double.parseDouble(coordenada.substring(0, coordenada.indexOf(":"))));
            utmNorte = BigDecimal.valueOf(Double.parseDouble(coordenada.substring(coordenada.indexOf(":") + 1)));
			*/
            
            utmEste = BigDecimal.valueOf(Double.parseDouble(lista[0]));
            utmNorte = BigDecimal.valueOf(Double.parseDouble(lista[1]));
            
            utmEste = utmEste.divide(new BigDecimal(100));
            utmNorte = utmNorte.divide(new BigDecimal(100));
            
            if(!lista[2].equals("NULL")){
            	codAlimentador = lista[2];
            }
            
            if(!lista[3].equals("NULL")){
            	secTipico = lista[3];
            }
            
        } catch (SQLException e) {
            result = e.getMessage();
            coordenada= "";
            utmEste = null;
            utmNorte = null;
            
            FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", "No se pudo completar la Transacción"));
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

        } finally {
           try {
                 cn.close();
                 } catch (SQLException e) {
                   result = e.getMessage();
        
                         setMensaje(e.getMessage()+":"+e.getCause());
                         FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", "No se pudo completar la Transacción"));
                         FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        
                 }
        }    
        
        deficiencia.setUtmEste(utmEste);
        deficiencia.setUtmNorte(utmNorte);
        deficiencia.setCodAlimentador(codAlimentador);
        deficiencia.setSecTipico(secTipico);
        
	}

	public String getCodIdentificacionFilter() {
		return codIdentificacionFilter;
	}

	public void setCodIdentificacionFilter(String codIdentificacionFilter) {
		this.codIdentificacionFilter = codIdentificacionFilter;
	}

	public List<SelectItem> getItemsSecTipico() {
		itemsSecTipico=new ArrayList<SelectItem>();
		itemsSecTipico.add(new SelectItem("", "NO ASIGNADO"));
		itemsSecTipico.add(new SelectItem("2", "SECTOR TIPICO 2"));
		itemsSecTipico.add(new SelectItem("3", "SECTOR TIPICO 3"));
		itemsSecTipico.add(new SelectItem("4", "SECTOR TIPICO 4"));
		itemsSecTipico.add(new SelectItem("5", "SECTOR TIPICO 5"));
		itemsSecTipico.add(new SelectItem("6", "SECTOR TIPICO 6"));
		return itemsSecTipico;
	}

	public void setItemsSecTipico(List<SelectItem> itemsSecTipico) {
		this.itemsSecTipico = itemsSecTipico;
	}

	public long getFilasTotal() {
		return filasTotal;
	}

	public void setFilasTotal(long filasTotal) {
		this.filasTotal = filasTotal;
	}
	
	public void handleEvent(ComponentSystemEvent event){
		UIExtendedDataTable dataTable = (UIExtendedDataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("idform-tableDeficiencia");
		
		filasTotal = dataTable.getRowCount();		
		
		System.out.println(dataTable.getRowCount() + "-" + dataTable.getClientRows() + "-" + dataTable.getChildCount() + "-" + dataTable.getRows() + "-" + dataTable.getResourceBundleMap().size());

	}
	
	public void actualizaFilas(AjaxBehaviorEvent event){
		//UIExtendedDataTable dataTable = (UIExtendedDataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("idform-tableMedicion");
		UIExtendedDataTable dataTable = (UIExtendedDataTable) event.getComponent().getParent().getParent().getParent();
		filasTotal = dataTable.getRowCount();		
		
		System.out.println(dataTable.getRowCount() + "-" + dataTable.getClientRows() + "-" + dataTable.getChildCount() + "-" + dataTable.getRows() + "-" + dataTable.getResourceBundleMap().size());

	}

	
	public void actualizaFilas2(AjaxBehaviorEvent event){
		//UIExtendedDataTable dataTable = (UIExtendedDataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("idform-tableMedicion");
		UIExtendedDataTable dataTable = (UIExtendedDataTable) event.getComponent();
		filasTotal = dataTable.getRowCount();
		
		Object originalKey = dataTable.getRowKey();
        selectionItems.clear();
        for (Object selectionKey : selection) {
            dataTable.setRowKey(selectionKey);
            if (dataTable.isRowAvailable()) {
                selectionItems.add((Deficiencia) dataTable.getRowData());
            }
        }
        dataTable.setRowKey(originalKey);		
		
		System.out.println(dataTable.getRowCount() + "-" + dataTable.getClientRows() + "-" + dataTable.getChildCount() + "-" + dataTable.getRows() + "-" + dataTable.getResourceBundleMap().size());

	}

	public String getCodAlimentadorFilter() {
		return codAlimentadorFilter;
	}

	public void setCodAlimentadorFilter(String codAlimentadorFilter) {
		this.codAlimentadorFilter = codAlimentadorFilter;
	}

	public String getSecTipicoFilter() {
		return secTipicoFilter;
	}

	public void setSecTipicoFilter(String secTipicoFilter) {
		this.secTipicoFilter = secTipicoFilter;
	}
	
	public Filter<?> getFiltersecTipico() {
        return new Filter<Deficiencia>() {
            public boolean accept(Deficiencia t) {
            	String secTipico = getSecTipicoFilter();
                if (secTipico == null || secTipico.length() == 0 || secTipico.equals(t.getSecTipico())) {
                    return true;
                }
                return false;
            }
        };
    }

	public List<SelectItem> getItemsSecTipico1() {
		itemsSecTipico1=new ArrayList<SelectItem>();
		itemsSecTipico1.add(new SelectItem("", ""));
		itemsSecTipico1.add(new SelectItem("2", "SECTOR TIPICO 2"));
		itemsSecTipico1.add(new SelectItem("3", "SECTOR TIPICO 3"));
		itemsSecTipico1.add(new SelectItem("4", "SECTOR TIPICO 4"));
		itemsSecTipico1.add(new SelectItem("5", "SECTOR TIPICO 5"));
		itemsSecTipico1.add(new SelectItem("6", "SECTOR TIPICO 6"));
		return itemsSecTipico1;
	}

	public void setItemsSecTipico1(List<SelectItem> itemsSecTipico1) {
		this.itemsSecTipico1 = itemsSecTipico1;
	}

	public List<SelectItem> getItemsCodTipInstalacionR1() {
		itemsCodTipInstalacionR1=new ArrayList<SelectItem>();
		itemsCodTipInstalacionR1.add(new SelectItem("", "NO ASIGNADO"));
		itemsCodTipInstalacionR1.add(new SelectItem("1", "SED"));
		itemsCodTipInstalacionR1.add(new SelectItem("2", "EMT"));
		itemsCodTipInstalacionR1.add(new SelectItem("4", "EBT"));
		return itemsCodTipInstalacionR1;
	}

	public void setItemsCodTipInstalacionR1(
			List<SelectItem> itemsCodTipInstalacionR1) {
		this.itemsCodTipInstalacionR1 = itemsCodTipInstalacionR1;
	}

	public List<SelectItem> getItemsCodTipInstalacionR2() {
		itemsCodTipInstalacionR2=new ArrayList<SelectItem>();
		itemsCodTipInstalacionR2.add(new SelectItem("", "NO ASIGNADO"));
		itemsCodTipInstalacionR2.add(new SelectItem("1", "SED"));
		itemsCodTipInstalacionR2.add(new SelectItem("2", "EMT"));
		itemsCodTipInstalacionR2.add(new SelectItem("4", "EBT"));
		return itemsCodTipInstalacionR2;
	}

	public void setItemsCodTipInstalacionR2(
			List<SelectItem> itemsCodTipInstalacionR2) {
		this.itemsCodTipInstalacionR2 = itemsCodTipInstalacionR2;
	}

	public String getEstadoFilter() {
		return estadoFilter;
	}

	public void setEstadoFilter(String estadoFilter) {
		this.estadoFilter = estadoFilter;
	}
	
	public Filter<?> getFilterEstado() {
        return new Filter<Deficiencia>() {
            public boolean accept(Deficiencia t) {
            	String estado = getEstadoFilter();
                if (estado == null || estado.length() == 0 || estado.equals(t.getEstado())) {
                    return true;
                }
                return false;
            }
        };
    }

	public List<SelectItem> getItemsEstado() {
		itemsEstado=new ArrayList<SelectItem>();
		itemsEstado.add(new SelectItem("", ""));
		itemsEstado.add(new SelectItem("1","ACTIVO"));
		itemsEstado.add(new SelectItem("0","ELIMINADO"));
		return itemsEstado;
	}

	public void setItemsEstado(List<SelectItem> itemsEstado) {
		this.itemsEstado = itemsEstado;
	}
}
