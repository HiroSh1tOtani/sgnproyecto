/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.proc228.dao;


import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;



import pe.com.calidad.proc228.entity.TramoMT;//TipoProteccionMT;
import pe.com.indra.calidad.dao.HibernateUtil;

import java.util.Set;



/**
 *
 * @author Luis
 */
public class TramoMTDaoImpl implements TramoMTDao {

    private static TramoMTDaoImpl instance = null;

    public static TramoMTDaoImpl getInstance() {
        if (instance == null) {
            instance = new TramoMTDaoImpl();
        }

        return instance;
    }

    public TramoMTDaoImpl() {
    }

	@Override
	public List<TramoMT> getAllRows() {
		  List<TramoMT> tramoMT=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from TramoMT ");
	            tramoMT= (List<TramoMT>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar TramoMT.", e);
	        } 
		return tramoMT;
	}

	@Override
	public List<TramoMT> getAllRows1() {
		  List<TramoMT> tramoMT=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from TramoMT where estado =:estado ");
	            q.setString("estado","1");
	            tramoMT= (List<TramoMT>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar tramoMT.", e);
	        } 
		return tramoMT;
	}



}
