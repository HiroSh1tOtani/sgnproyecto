/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.proc228.dao;


import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;


import pe.com.calidad.proc228.entity.TipoProteccionMT;//Sed;
import pe.com.indra.calidad.dao.HibernateUtil;


import java.util.Set;



/**
 *
 * @author Luis
 */
public class TipoProteccionMTDaoImpl implements TipoProteccionMTDao {

    private static TipoProteccionMTDaoImpl instance = null;

    public static TipoProteccionMTDaoImpl getInstance() {
        if (instance == null) {
            instance = new TipoProteccionMTDaoImpl();
        }

        return instance;
    }

    public TipoProteccionMTDaoImpl() {
    }

	@Override
	public List<TipoProteccionMT> getAllRows() {
		  List<TipoProteccionMT> tipoProteccionMT=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from TipoProteccionMT ");
	            tipoProteccionMT= (List<TipoProteccionMT>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar tipoProteccionMT.", e);
	        } 
		return tipoProteccionMT;
	}

	@Override
	public List<TipoProteccionMT> getAllRows1() {
		  List<TipoProteccionMT> tipoProteccionMT=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from TipoProteccionMT where estado =:estado ");
	            q.setString("estado","1");
	            tipoProteccionMT= (List<TipoProteccionMT>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar sed.", e);
	        } 
		return tipoProteccionMT;
	}



}
