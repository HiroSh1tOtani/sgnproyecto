package pe.com.calidad.proc228.dao;

/**
 *
 * @author Luis
 */
import java.util.List;





import pe.com.calidad.proc228.entity.Deficiencia;
import pe.com.calidad.proc228.entity.EstadoSubsanacion;
import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.PeriodoMedicion;

import java.util.Set;

public interface DeficienciaDao {

    public void create(Deficiencia deficiencia);
    public Deficiencia ReadById(long deficienciaId);
    public Deficiencia ReadByCod(String codIdentificacion);
    public void update(Deficiencia deficiencia);
    public void delete(long deficienciaId);
    
    public EstadoSubsanacion selectEstadoSubsanacion(long deficienciaId);
    public PeriodoMedicion selectPeriodo(long deficienciaId);
  
    
    public List<Deficiencia> getAllRows1(long perMedicionId);
    public List<Deficiencia> getAllRows1();    
    public List<Deficiencia> getDeficienciaxSuministro(String numSuministro);
    public List<Deficiencia> getBusquedaxDeficiencia(String codIdentificacion);
    public List<Deficiencia> getAllRows(long perMedicionId);
    public List<Deficiencia> getAllRowsEstado(String ano); 
}
