/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.proc228.dao;


import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;


import pe.com.calidad.proc228.entity.Sed;//NodoEnlace;
import pe.com.indra.calidad.dao.HibernateUtil;





import java.util.Set;



/**
 *
 * @author Luis
 */
public class SedDaoImpl implements SedDao {

    private static SedDaoImpl instance = null;

    public static SedDaoImpl getInstance() {
        if (instance == null) {
            instance = new SedDaoImpl();
        }

        return instance;
    }

    public SedDaoImpl() {
    }

	@Override
	public List<Sed> getAllRows() {
		  List<Sed> sed=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from Sed ");
	            sed= (List<Sed>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Sed.", e);
	        } 
		return sed;
	}

	@Override
	public List<Sed> getAllRows1() {
		  List<Sed> sed=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from Sed where estado =:estado ");
	            q.setString("estado","1");
	            sed= (List<Sed>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar sed.", e);
	        } 
		return sed;
	}
		
}
