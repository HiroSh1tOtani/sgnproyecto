/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.proc228.dao;


import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;



import pe.com.calidad.proc228.entity.EstadoSubsanacion;
import pe.com.calidad.proc228.entity.TipoComponente;
import pe.com.calidad.proc228.entity.TipoDeficiencia;
import pe.com.indra.calidad.dao.HibernateUtil;


/**
 *
 * @author Luis
 */
public class TipoComponenteDaoImpl implements TipoComponenteDao {

    private static TipoComponenteDaoImpl instance = null;

    public static TipoComponenteDaoImpl getInstance() {
        if (instance == null) {
            instance = new TipoComponenteDaoImpl();
        }

        return instance;
    }

    public TipoComponenteDaoImpl() {
    }

	/*@Override
	public void create(TipoComponente tipoComponente) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(tipoComponente);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear tipo Componente.", e);
        } 
    }

	@Override
	public TipoComponente ReadById(long tipComponenteId) {
		TipoComponente tipoComponente = null;
        try {
            HibernateUtil.begin();
           
            
            tipoComponente = (TipoComponente) HibernateUtil.getSession().load(TipoComponente.class, tipComponenteId);
            
            Hibernate.initialize(tipoComponente);
            Hibernate.initialize(tipoComponente.getTipoDeficiencias()); 
        
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar tipo Componente.", e);
        } 
        return tipoComponente;
    }

	@Override
	public TipoComponente ReadByCod(String codTipComponente) {
		TipoComponente tipoComponente = null;
        try {
            HibernateUtil.begin();
            
            Query q = HibernateUtil.getSession().createQuery("from TipoComponente where codTipComponente = :codTipComponente  and estado = '1'");
            q.setString("codIdentificacion",codTipComponente);
            tipoComponente = (TipoComponente)q.uniqueResult();
            
            Hibernate.initialize(tipoComponente);
            Hibernate.initialize(tipoComponente.getTipoDeficiencias()); 
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar tipo Componente.", e);
        } 
        return tipoComponente;
    }

	@Override
	public void update(TipoComponente tipoComponente) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(tipoComponente);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar tipo Componente.", e);
        } 
    }

	@Override
	public void delete(long tipComponenteId) {
        
		TipoComponente tipoComponente = null;
		tipoComponente = ReadById(tipComponenteId);
        
        if(tipoComponente != null){               
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(tipoComponente);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar TipoComponente.", e); 
            }
        }
    }

	@Override
	public Set<TipoDeficiencia> selectTipoDeficiencia(long tipComponenteId) {
		TipoComponente tipoComponente = null;
		tipoComponente = ReadById(tipComponenteId);

        return tipoComponente.getTipoDeficiencias();
    }
	*/
    
    @Override
	public List<TipoComponente> getAllRows() {
		  List<TipoComponente> tipoComponente=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from TipoComponente ");
	            tipoComponente= (List<TipoComponente>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar TipoComponente.", e);
	        } 
		return tipoComponente;
	}

	

	@Override
	public List<TipoComponente> getAllRows1() {
		List<TipoComponente> tipoComponente=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from TipoComponente i where i.estado =:estado");
	            q.setString("estado","1");
	            
	            tipoComponente=(List<TipoComponente>) q.list();
	            
	           
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Tipo Componente.", e);
	        }
		return tipoComponente;
	}
		
}
