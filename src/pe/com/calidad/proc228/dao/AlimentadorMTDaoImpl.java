/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.proc228.dao;


import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Hibernate;

import pe.com.calidad.proc228.entity.AlimentadorMT;

import pe.com.indra.calidad.dao.HibernateUtil;

import java.util.Set;



/**
 *
 * @author Luis
 */
public class AlimentadorMTDaoImpl implements AlimentadorMTDao {

    private static AlimentadorMTDaoImpl instance = null;

    public static AlimentadorMTDaoImpl getInstance() {
        if (instance == null) {
            instance = new AlimentadorMTDaoImpl();
        }

        return instance;
    }

    public AlimentadorMTDaoImpl() {
    }

	@Override
	public List<AlimentadorMT> getAllRows() {
		  List<AlimentadorMT> alimentadorMT=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from AlimentadorMT ");
	            alimentadorMT= (List<AlimentadorMT>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar AlimentadorMT.", e);
	        } 
		return alimentadorMT;
	}

	@Override
	public List<AlimentadorMT> getAllRows1() {
		  List<AlimentadorMT> alimentadorMT=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from AlimentadorMT where estado =:estado ");
	            q.setString("estado","1");
	            alimentadorMT= (List<AlimentadorMT>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar AlimentadorMT.", e);
	        } 
		return alimentadorMT;
	}

	
}
