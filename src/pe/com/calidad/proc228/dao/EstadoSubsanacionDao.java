package pe.com.calidad.proc228.dao;


import pe.com.calidad.proc078.entity.HistoricoDeficienciaAp;
import pe.com.calidad.proc228.entity.EstadoSubsanacion;



import java.util.List;
import java.util.Set;

/**
 *
 * @author Luis
 */
public interface EstadoSubsanacionDao {

	public EstadoSubsanacion ReadById(long estSubsanacionId);
    public List<EstadoSubsanacion> getAllRows();
    public List<EstadoSubsanacion> getAllRows1();
    
    
}
