package pe.com.calidad.proc228.dao;

import pe.com.calidad.proc228.entity.TipoDeficiencia;
import pe.com.calidad.proc228.entity.TipoComponente;


import java.util.List;
import java.util.Set;

/**
 *
 * @author Luis
 */
public interface TipoComponenteDao {

    /*public void create(TipoComponente tipoComponente);
    public TipoComponente ReadById(long tipComponenteId);
    public TipoComponente ReadByCod(String codTipComponente);
    public void update(TipoComponente tipoComponente);
    public void delete(long tipComponenteId);
    public Set<TipoDeficiencia> selectTipoDeficiencia(long tipComponenteId);
   */
    public List<TipoComponente> getAllRows();
    public List<TipoComponente> getAllRows1();
    
}
