package pe.com.calidad.proc228.dao;

import pe.com.calidad.proc228.entity.TipoDeficiencia;
import pe.com.calidad.proc228.entity.TipoInstalacion;


import java.util.List;
import java.util.Set;

/**
 *
 * @author Luis
 */
public interface TipoInstalacionDao {

    public void create(TipoInstalacion tipoInstalacion);
    public TipoInstalacion ReadById(long tipInstalacionId);
    public TipoInstalacion ReadByCod(String codTipInstalacion);
    public void update(TipoInstalacion tipoInstalacion);
    public void delete(long tipInstalacionId);
    
    public Set<TipoDeficiencia> selectTipoDeficiencia(long tipInstalacionId);
    
    public List<TipoInstalacion> getAllRows1();
    
}
