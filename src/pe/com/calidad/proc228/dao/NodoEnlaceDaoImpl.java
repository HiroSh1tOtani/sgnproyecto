/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.proc228.dao;


import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;


import pe.com.calidad.proc228.entity.NodoEnlace;
import pe.com.indra.calidad.dao.HibernateUtil;





import java.util.Set;



/**
 *
 * @author Luis
 */
public class NodoEnlaceDaoImpl implements NodoEnlaceDao {

    private static NodoEnlaceDaoImpl instance = null;

    public static NodoEnlaceDaoImpl getInstance() {
        if (instance == null) {
            instance = new NodoEnlaceDaoImpl();
        }

        return instance;
    }

    public NodoEnlaceDaoImpl() {
    }

	@Override
	public List<NodoEnlace> getAllRows() {
		  List<NodoEnlace> nodoEnlace=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from NodoEnlace ");
	            nodoEnlace= (List<NodoEnlace>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar NodoEnlace.", e);
	        } 
		return nodoEnlace;
	}

	@Override
	public List<NodoEnlace> getAllRows1() {
		  List<NodoEnlace> nodoEnlace=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from NodoEnlace where estado =:estado ");
	            q.setString("estado","1");
	            nodoEnlace= (List<NodoEnlace>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar NodoEnlace.", e);
	        } 
		return nodoEnlace;
	}
		
}
