package pe.com.calidad.proc228.dao;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import java.util.List;


import pe.com.calidad.proc228.entity.Metas;//RelevadorRechazoCarga;
import pe.com.indra.calidad.dao.HibernateUtil;
import pe.com.indra.calidad.entity.Medicion;


/**
 *
 * @author Luis
 */
public class MetasDaoImpl implements MetasDao {

    private static MetasDaoImpl instance = null;

    public static MetasDaoImpl getInstance() {
        if (instance == null) {
            instance = new MetasDaoImpl();
        }

        return instance;
    }

    public MetasDaoImpl() {
    }

	@Override
	public void create(Metas metas) {

        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(metas);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Metas.", e);
        }
  	
	}

	@Override
	public Metas ReadById(long metasId) {
		Metas metas = null;
        try {
            HibernateUtil.begin();

            metas = (Metas) HibernateUtil.getSession().load(Metas.class, metasId);

            Hibernate.initialize(metas);
         
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Relevador RechazoCarga.", e);
        }
        return metas;
    }

	@Override
	public void update(Metas metas) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(metas);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Metas.", e);
        }
    }

	@Override
	public void delete(long metasId) {
		Metas metas = null;
		metas = ReadById(metasId);

        if (metas != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(metas);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Metas.", e);
            }
        }
    }

	@Override
	public List<Metas> getAllRows() {
		List<Metas> metas=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from Metas");
	            
	            metas=(List<Metas>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Metas .", e);
	        }
		return metas;
	}

	@Override
	public List<Metas> getAllRows1() {
		List<Metas> metas=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from Metas where estado =:estado");
	            q.setString("estado","1");
	            metas=(List<Metas>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Metas .", e);
	        }
		return metas;
	}

	@Override
	public List<Metas> getBusquedaxAno(String ano) {
		List<Metas> metas=null;
		 try {
	            HibernateUtil.begin();
	            
	            Query q = HibernateUtil.getSession().createQuery("from Metas where ano = '" + ano + "' order by secTipico asc, codTipDeficiencia asc");
	            
	            metas = (List<Metas>) q.list();	     
	            for (Metas m : metas) {
	            	System.out.print(m.getCodTipDeficiencia());
	            	System.out.print(" ");
	            	System.out.print(m.getPrioridad());
	            	System.out.print(" ");
	            	System.out.print(m.getSecTipico());
	            	System.out.println(" ");
	            }
	            	
	            
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Metas.", e);
	        }
		return metas;
	}

	
}
