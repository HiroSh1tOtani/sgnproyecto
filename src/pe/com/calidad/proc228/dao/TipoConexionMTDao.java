package pe.com.calidad.proc228.dao;


import pe.com.calidad.proc228.entity.TipoConexionMT;

import java.util.List;
import java.util.Set;

/**
 *
 * @author Luis
 */
public interface TipoConexionMTDao {

 
   
    public List<TipoConexionMT> getAllRows();
    public List<TipoConexionMT> getAllRows1();
  
    
}
