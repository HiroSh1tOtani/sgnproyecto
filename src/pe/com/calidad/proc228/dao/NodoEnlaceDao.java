package pe.com.calidad.proc228.dao;


import pe.com.calidad.proc228.entity.NodoEnlace;

import java.util.List;
import java.util.Set;

/**
 *
 * @author Luis
 */
public interface NodoEnlaceDao {

 
   
    public List<NodoEnlace> getAllRows();
    public List<NodoEnlace> getAllRows1();
  
    
}
