package pe.com.calidad.proc228.dao;

/**
 *
 * @author Luis
 */
import java.util.List;

import pe.com.calidad.proc228.entity.EstadoSubsanacion;
import pe.com.calidad.proc228.entity.TipoDeficiencia;


public interface TipoDeficienciaDao {

 
	public TipoDeficiencia ReadById(long tipDeficienciaId);
	public List<TipoDeficiencia> getAllRows1();
    public List<TipoDeficiencia> getAllRows1(long tipInstalacionId);
    
}
