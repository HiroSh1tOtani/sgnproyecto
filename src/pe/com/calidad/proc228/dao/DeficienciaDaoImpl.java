/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.proc228.dao;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Hibernate;

import pe.com.calidad.proc228.entity.Deficiencia;//AlimentadorMT;
import pe.com.calidad.proc228.entity.EstadoSubsanacion;
import pe.com.calidad.proc228.entity.Metas;
import pe.com.calidad.proc228.service.MetasService;
import pe.com.calidad.suministro.entity.CausaInterrupcion;
import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.indra.calidad.dao.HibernateUtil;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.PeriodoMedicion;

import java.util.Set;



/**
 *
 * @author Luis
 */
public class DeficienciaDaoImpl implements DeficienciaDao {

    private static DeficienciaDaoImpl instance = null;

    public static DeficienciaDaoImpl getInstance() {
        if (instance == null) {
            instance = new DeficienciaDaoImpl();
        }

        return instance;
    }

    public DeficienciaDaoImpl() {
    }

	@Override
	public void create(Deficiencia deficiencia) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(deficiencia);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear deficiencia.", e);
        } 
    }

	@Override
	public Deficiencia ReadById(long deficienciaId) {
		Deficiencia deficiencia = null;
        try {
            HibernateUtil.begin();
           
            
            deficiencia = (Deficiencia) HibernateUtil.getSession().load(Deficiencia.class, deficienciaId);
            
            Hibernate.initialize(deficiencia);
            Hibernate.initialize(deficiencia.getPeriodoMedicion()); 
            Hibernate.initialize(deficiencia.getEstadoSubsanacion()); 
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar deficiencia.", e);
        } 
        return deficiencia;
    }
	
	@Override
	public Deficiencia ReadByCod(String codIdentificacion) {
		Deficiencia deficiencia = null;
        try {
            HibernateUtil.begin();
            
            Query q = HibernateUtil.getSession().createQuery("from Deficiencia where codIdentificacion = :codIdentificacion and estado = '1'");
            q.setString("codIdentificacion",codIdentificacion);
            deficiencia = (Deficiencia)q.uniqueResult();
            
            Hibernate.initialize(deficiencia);
            //Hibernate.initialize(deficiencia.getPeriodoMedicion()); 
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar deficiencia.", e);
        } 
        return deficiencia;
    }


	@Override
	public void update(Deficiencia deficiencia) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(deficiencia);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar deficiencia.", e);
        } 
    }

	@Override
	public void delete(long deficienciaId) {
        
		Deficiencia deficiencia = null;
		deficiencia = ReadById(deficienciaId);
        
        if(deficiencia != null){               
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(deficiencia);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar deficiencia.", e); 
            }
        }
    }

	@Override
	public EstadoSubsanacion selectEstadoSubsanacion(long deficienciaId) {
		Deficiencia deficiencia = null;
		deficiencia = ReadById(deficienciaId);

        return deficiencia.getEstadoSubsanacion();
    }

	@Override
	public PeriodoMedicion selectPeriodo(long deficienciaId) {
		Deficiencia deficiencia = null;
		deficiencia = ReadById(deficienciaId);

        return deficiencia.getPeriodoMedicion();
    }

	
	
	@Override
	public List<Deficiencia> getAllRows1(long perMedicionId) {
		List<Deficiencia> deficiencia=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from Deficiencia i where i.estado =:estado and i.periodoMedicion.perMedicionId = :perMedicionId order by i.fecDenuncia desc");
	            
	            q.setString("estado","1");
	            q.setLong("perMedicionId", perMedicionId);
	            
	            deficiencia=(List<Deficiencia>) q.list();
	            
	           
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Deficiencia.", e);
	        }
		return deficiencia;
	}
	
	@Override
	public List<Deficiencia> getAllRows1() {
		List<Deficiencia> deficiencia=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from Deficiencia i where i.estado =:estado order by i.fecDenuncia desc");
	            q.setString("estado","1");
	            
	            
	            deficiencia=(List<Deficiencia>) q.list();
	            
	           
	            HibernateUtil.commit();
	            HibernateUtil.close();
	            
	            //agregarTipoMeta(deficiencia);

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Deficiencia.", e);
	        }
		return deficiencia;
	}	

	@Override
	public List<Deficiencia> getDeficienciaxSuministro(String numSuministro) {

		 List<Deficiencia> deficiencia=null;
		 try {
	            HibernateUtil.begin();
	            
	            Query q = HibernateUtil.getSession().createQuery("from Deficiencia m where m.estado = '1' and m.numSuministro = '" + numSuministro.trim() + "' order by m.fecDenuncia desc");
	            
	            deficiencia=(List<Deficiencia>) q.list();	            
	            
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Deficiencia.", e);
	        }
		return deficiencia;
	
	}

	@Override
	public List<Deficiencia> getBusquedaxDeficiencia(String codIdentificacion) {
		 List<Deficiencia> deficiencia=null;
		 try {
	            HibernateUtil.begin();
	            
	            Query q = HibernateUtil.getSession().createQuery("from Deficiencia m where m.estado = '1' and m.codIdentificacion = '" + codIdentificacion.trim() + "' order by m.fecDenuncia desc");
	            
	            deficiencia=(List<Deficiencia>) q.list();	            
	            
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Deficiencia.", e);
	        }
		return deficiencia;
	}

	@Override
	public List<Deficiencia> getAllRows(long perMedicionId) {
		List<Deficiencia> deficiencia=null;
		 try {
	            HibernateUtil.begin();
	            
	            Query q = HibernateUtil.getSession().createQuery("from Deficiencia i where i.periodoMedicion.perMedicionId = :perMedicionId order by i.fecDenuncia desc");
	            q.setLong("perMedicionId", perMedicionId);
	            
	            
	            
	            
	            deficiencia=(List<Deficiencia>) q.list();
	            
	            
	           
	            HibernateUtil.commit();
	            HibernateUtil.close();
	            
	            

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Deficiencia.", e);
	        }
		return deficiencia;
	}

	@Override
	public List<Deficiencia> getAllRowsEstado(String ano) {
		List<Deficiencia> deficiencia=null;
	 try {
         HibernateUtil.begin();
         
         Query q = HibernateUtil.getSession().createQuery("from Deficiencia i where i.estado =:estado and i.estadoSubsanacion.codEstSubsanacion = '0' or (i.estadoSubsanacion.codEstSubsanacion in ('1','2') and year(i.fecSubsanacion) = :ano) order by i.fecDenuncia desc");
         q.setString("estado","1");
         q.setString("ano",ano);
         
         /*
         Query q = HibernateUtil.getSession().createQuery("from Deficiencia i where i.codIdentificacion =:estado order by i.fecDenuncia desc");
         q.setString("estado","ELSM46028");
         */
         deficiencia=(List<Deficiencia>) q.list();
         
        
         HibernateUtil.commit();
         HibernateUtil.close();
         agregarTipoMeta(deficiencia);
     } catch (HibernateException e) {
         HibernateUtil.rollback();
         throw new HibernateException("No se puede encontrar Deficiencia Estado por subsanar.", e);
     }
	return deficiencia;
	}

	
	private void agregarTipoMeta(List<Deficiencia> lstDeficiencia){
		MetasService metaService=new MetasService();
		List<Metas> lst = metaService.getAllRows1();
		Map<String,Metas> map = new HashMap<String,Metas>();
		for (Metas m : lst) {
			String key = m.getCodTipDeficiencia() + "_" + m.getAno() + "_" + m.getSecTipico();
			map.put(key, m);
		}
		
		for (Deficiencia d : lstDeficiencia) {
			String key = d.getTipoDeficiencia().getCodTipDeficiencia() + "_" + d.getMeta() + "_" + d.getSecTipico();
			if (map.containsKey(key)){
				d.setTipoMeta(map.get(key));
			}
		}
	}
	
	
}
