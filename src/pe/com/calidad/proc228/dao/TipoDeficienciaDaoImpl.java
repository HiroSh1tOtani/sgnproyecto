/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.proc228.dao;


import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;





import pe.com.calidad.proc078.entity.HistoricoDeficienciaAp;
import pe.com.calidad.proc228.entity.Deficiencia;
import pe.com.calidad.proc228.entity.EstadoSubsanacion;
import pe.com.calidad.proc228.entity.TipoComponente;
import pe.com.calidad.proc228.entity.TipoDeficiencia;
import pe.com.indra.calidad.dao.HibernateUtil;


/**
 *
 * @author Luis
 */
public class TipoDeficienciaDaoImpl implements TipoDeficienciaDao {

    private static TipoDeficienciaDaoImpl instance = null;

    public static TipoDeficienciaDaoImpl getInstance() {
        if (instance == null) {
            instance = new TipoDeficienciaDaoImpl();
        }

        return instance;
    }

    public TipoDeficienciaDaoImpl() {
    }

	@Override
	public List<TipoDeficiencia> getAllRows1(long tipInstalacionId) {
		List<TipoDeficiencia> tipoDeficiencia=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from TipoDeficiencia i where i.estado =:estado and i.tipoInstalacion.tipInstalacionId = :tipInstalacionId");
	            q.setString("estado","1");
	            q.setLong("perMedicionId", tipInstalacionId);
	            
	            tipoDeficiencia=(List<TipoDeficiencia>) q.list();
	            
	           
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar tipoDeficiencia.", e);
	        }
		return tipoDeficiencia;
	}

	@Override
	public TipoDeficiencia ReadById(long tipDeficienciaId) {
		TipoDeficiencia tipoDeficiencia = null;
        try {
            HibernateUtil.begin();

            tipoDeficiencia = (TipoDeficiencia) HibernateUtil.getSession().load(TipoDeficiencia.class, tipDeficienciaId);

            Hibernate.initialize(tipoDeficiencia);
            Hibernate.initialize(tipoDeficiencia.getDeficiencias());
                       
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar tipo Deficiencia.", e);
        }
        return tipoDeficiencia;
    }

	@Override
	public List<TipoDeficiencia> getAllRows1() {
		List<TipoDeficiencia> tipoDeficiencia=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from TipoDeficiencia where estado =:estado");
	            q.setString("estado","1");
	            tipoDeficiencia=(List<TipoDeficiencia>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Tipo Deficiencia.", e);
	        }
		return tipoDeficiencia;
	}

	
		
}
