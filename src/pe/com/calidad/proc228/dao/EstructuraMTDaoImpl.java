/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.proc228.dao;


import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;


import pe.com.calidad.proc228.entity.EstructuraMT;
import pe.com.indra.calidad.dao.HibernateUtil;





import java.util.Set;



/**
 *
 * @author Luis
 */
public class EstructuraMTDaoImpl implements EstructuraMTDao {

    private static EstructuraMTDaoImpl instance = null;

    public static EstructuraMTDaoImpl getInstance() {
        if (instance == null) {
            instance = new EstructuraMTDaoImpl();
        }

        return instance;
    }

    public EstructuraMTDaoImpl() {
    }

	@Override
	public List<EstructuraMT> getAllRows() {
		  List<EstructuraMT> estructuraMT=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from EstructuraMT ");
	            estructuraMT= (List<EstructuraMT>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar EstructuraMT.", e);
	        } 
		return estructuraMT;
	}

	@Override
	public List<EstructuraMT> getAllRows1() {
		  List<EstructuraMT> estructuraMT=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from EstructuraMT where estado =:estado ");
	            q.setString("estado","1");
	            estructuraMT= (List<EstructuraMT>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar EstructuraMT.", e);
	        } 
		return estructuraMT;
	}

	
	

	
}
