/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.proc228.dao;


import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Hibernate;




import pe.com.calidad.proc078.entity.HistoricoDeficienciaAp;
import pe.com.calidad.proc228.entity.EstadoSubsanacion;
import pe.com.calidad.suministro.entity.CausaInterrupcion;
import pe.com.indra.calidad.dao.HibernateUtil;






import java.util.Set;



/**
 *
 * @author Luis
 */
public class EstadoSubsanacionDaoImpl implements EstadoSubsanacionDao {

    private static EstadoSubsanacionDaoImpl instance = null;

    public static EstadoSubsanacionDaoImpl getInstance() {
        if (instance == null) {
            instance = new EstadoSubsanacionDaoImpl();
        }

        return instance;
    }

    public EstadoSubsanacionDaoImpl() {
    }

	@Override
	public List<EstadoSubsanacion> getAllRows() {
		  List<EstadoSubsanacion> estadoSubsanacion=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from EstadoSubsanacion ");
	            estadoSubsanacion= (List<EstadoSubsanacion>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar EstadoSubsanacion.", e);
	        } 
		return estadoSubsanacion;
	}

	@Override
	public List<EstadoSubsanacion> getAllRows1() {
		  List<EstadoSubsanacion> estadoSubsanacion=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from EstadoSubsanacion where estado =:estado ");
	            q.setString("estado","1");
	            estadoSubsanacion= (List<EstadoSubsanacion>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar EstadoSubsanacion.", e);
	        } 
		return estadoSubsanacion;
	}

	@Override
	public EstadoSubsanacion ReadById(long estSubsanacionId) {
		EstadoSubsanacion estadoSubsanacion = null;
        try {
            HibernateUtil.begin();

            estadoSubsanacion = (EstadoSubsanacion) HibernateUtil.getSession().load(EstadoSubsanacion.class, estSubsanacionId);

            Hibernate.initialize(estadoSubsanacion);
            Hibernate.initialize(estadoSubsanacion.getDeficiencias());
                       
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar estado Subsanacion.", e);
        }
        return estadoSubsanacion;
    }

	
}
