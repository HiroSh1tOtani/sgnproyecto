
package pe.com.calidad.proc228.dao;

import java.util.List;


import pe.com.calidad.proc228.entity.Metas;

/**
 *
 * @author Lucho
 */
public interface MetasDao {
    public void create(Metas metas);
    public Metas ReadById(long metasId);
	public void update(Metas metas);
	public void delete(long metasId);
   	public List<Metas> getAllRows();
	public List<Metas> getAllRows1();
	public List<Metas> getBusquedaxAno(String ano);
	
        
}
