/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.proc228.dao;


import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.calidad.proc228.entity.Set;
import pe.com.indra.calidad.dao.HibernateUtil;


/**
 *
 * @author Luis
 */
public class SetDaoImpl implements SetDao {

    private static SetDaoImpl instance = null;

    public static SetDaoImpl getInstance() {
        if (instance == null) {
            instance = new SetDaoImpl();
        }

        return instance;
    }

    public SetDaoImpl() {
    }

	@Override
	public List<Set> getAllRows() {
		  List<Set> set=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from Set ");
	            set= (List<Set>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Set.", e);
	        } 
		return set;
	}

	@Override
	public List<Set> getAllRows1() {
		  List<Set> set=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from Set where estado =:estado ");
	            q.setString("estado","1");
	            set= (List<Set>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar set.", e);
	        } 
		return set;
	}

		
}
