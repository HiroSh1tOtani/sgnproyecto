/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.proc228.dao;


import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;




import pe.com.calidad.proc228.entity.TipoComponente;
import pe.com.calidad.proc228.entity.TipoInstalacion;//TipoComponente;
import pe.com.calidad.proc228.entity.TipoDeficiencia;
import pe.com.indra.calidad.dao.HibernateUtil;


/**
 *
 * @author Luis
 */
public class TipoInstalacionDaoImpl implements TipoInstalacionDao {

    private static TipoInstalacionDaoImpl instance = null;

    public static TipoInstalacionDaoImpl getInstance() {
        if (instance == null) {
            instance = new TipoInstalacionDaoImpl();
        }

        return instance;
    }

    public TipoInstalacionDaoImpl() {
    }

	@Override
	public void create(TipoInstalacion tipoInstalacion) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(tipoInstalacion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear tipo Instalacion.", e);
        } 
    }

	@Override
	public TipoInstalacion ReadById(long tipInstalacionId) {
		TipoInstalacion tipoInstalacion = null;
        try {
            HibernateUtil.begin();
           
            
            tipoInstalacion = (TipoInstalacion) HibernateUtil.getSession().load(TipoInstalacion.class, tipInstalacionId);
            
            Hibernate.initialize(tipoInstalacion);
            Hibernate.initialize(tipoInstalacion.getTipoDeficiencias()); 
        
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar tipo Instalacion.", e);
        } 
        return tipoInstalacion;
    }

	@Override
	public TipoInstalacion ReadByCod(String codTipInstalacion) {
		TipoInstalacion tipoInstalacion = null;
        try {
            HibernateUtil.begin();
            
            Query q = HibernateUtil.getSession().createQuery("from tipoInstalacion where codTipInstalacion = :codTipInstalacion and estado = '1'");
            q.setString("codTipInstalacion",codTipInstalacion);
            tipoInstalacion = (TipoInstalacion)q.uniqueResult();
            
            Hibernate.initialize(tipoInstalacion);
            Hibernate.initialize(tipoInstalacion.getTipoDeficiencias()); 
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar tipo Instalacion.", e);
        } 
        return tipoInstalacion;
    }

	@Override
	public void update(TipoInstalacion tipoInstalacion) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(tipoInstalacion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar tipo Instalacion.", e);
        } 
    }

	@Override
	public void delete(long tipInstalacionId) {
        
		TipoInstalacion tipoInstalacion = null;
		tipoInstalacion = ReadById(tipInstalacionId);
        
        if(tipoInstalacion != null){               
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(tipoInstalacion);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar TipoInstalacion.", e); 
            }
        }
    }

	@Override
	public Set<TipoDeficiencia> selectTipoDeficiencia(long tipInstalacionId) {
		TipoInstalacion tipoInstalacion = null;
		tipoInstalacion = ReadById(tipInstalacionId);

        return tipoInstalacion.getTipoDeficiencias();
    }

	@Override
	public List<TipoInstalacion> getAllRows1() {
		List<TipoInstalacion> tipoInstalacion=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from TipoInstalacion i where i.estado =:estado ");
	            q.setString("estado","1");
	            	            
	            tipoInstalacion=(List<TipoInstalacion>) q.list();
	            
	           
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Tipo Instalacion.", e);
	        }
		return tipoInstalacion;
	}

	
		
}
