package pe.com.calidad.proc228.dao;


import pe.com.calidad.proc228.entity.TipoProteccionMT;

import java.util.List;
import java.util.Set;

/**
 *
 * @author Luis
 */
public interface TipoProteccionMTDao {

 
   
    public List<TipoProteccionMT> getAllRows();
    public List<TipoProteccionMT> getAllRows1();
  
    
}
