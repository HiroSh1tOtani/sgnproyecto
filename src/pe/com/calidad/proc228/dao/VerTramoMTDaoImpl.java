/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.proc228.dao;


import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;



import pe.com.calidad.proc228.entity.VerTramoMT;//TramoMT;
import pe.com.indra.calidad.dao.HibernateUtil;

import java.util.Set;



/**
 *
 * @author Luis
 */
public class VerTramoMTDaoImpl implements VerTramoMTDao {

    private static VerTramoMTDaoImpl instance = null;

    public static VerTramoMTDaoImpl getInstance() {
        if (instance == null) {
            instance = new VerTramoMTDaoImpl();
        }

        return instance;
    }

    public VerTramoMTDaoImpl() {
    }

	@Override
	public List<VerTramoMT> getAllRows() {
		  List<VerTramoMT> verTramoMT=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from VerTramoMT ");
	            verTramoMT= (List<VerTramoMT>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar VerTramoMT.", e);
	        } 
		return verTramoMT;
	}

	@Override
	public List<VerTramoMT> getAllRows1() {
		  List<VerTramoMT> verTramoMT=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from VerTramoMT where estado =:estado ");
	            q.setString("estado","1");
	            verTramoMT= (List<VerTramoMT>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar VerTramoMT.", e);
	        } 
		return verTramoMT;
	}



}
