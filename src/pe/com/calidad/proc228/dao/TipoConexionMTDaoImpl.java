/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.proc228.dao;


import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.calidad.proc228.entity.Set;
import pe.com.calidad.proc228.entity.TipoConexionMT;//Set;
import pe.com.indra.calidad.dao.HibernateUtil;


/**
 *
 * @author Luis
 */
public class TipoConexionMTDaoImpl implements TipoConexionMTDao {

    private static TipoConexionMTDaoImpl instance = null;

    public static TipoConexionMTDaoImpl getInstance() {
        if (instance == null) {
            instance = new TipoConexionMTDaoImpl();
        }

        return instance;
    }

    public TipoConexionMTDaoImpl() {
    }

	@Override
	public List<TipoConexionMT> getAllRows() {
		  List<TipoConexionMT> tipoConexionMT=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from TipoConexionMT ");
	            tipoConexionMT = (List<TipoConexionMT>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Tipo ConexionMT.", e);
	        } 
		return tipoConexionMT;
	}

	@Override
	public List<TipoConexionMT> getAllRows1() {
		  List<TipoConexionMT> tipoConexionMT=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from TipoConexionMT where estado =:estado ");
	            q.setString("estado","1");
	            tipoConexionMT= (List<TipoConexionMT>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Tipo ConexionMT.", e);
	        } 
		return tipoConexionMT;
	}

	
		
}
