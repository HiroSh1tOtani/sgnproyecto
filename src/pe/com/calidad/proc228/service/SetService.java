package pe.com.calidad.proc228.service;


import java.util.List;


import pe.com.calidad.proc228.dao.SetDao;
import pe.com.calidad.proc228.dao.SetDaoImpl;
import pe.com.calidad.proc228.entity.Set;

/**
 *
 * @author Lucho
 */
public class SetService {

    
    public List<Set>getAllRows(){
    	SetDao dao = new SetDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<Set>getAllRows1(){
    	SetDao dao = new SetDaoImpl();
    	return dao.getAllRows1();
    }
}


