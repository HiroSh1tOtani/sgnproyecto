package pe.com.calidad.proc228.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;

import pe.com.calidad.proc228.dao.DeficienciaDao;
import pe.com.calidad.proc228.dao.DeficienciaDaoImpl;
import pe.com.calidad.proc228.entity.Deficiencia;
import pe.com.calidad.proc228.entity.EstadoSubsanacion;
import pe.com.indra.calidad.dao.MedicionDao;
import pe.com.indra.calidad.dao.MedicionDaoImpl;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.PeriodoMedicion;

/**
 *
 * @author Luis
 */
public class DeficienciaService {

    public void create(Deficiencia deficiencia) {
    	DeficienciaDao dao = new DeficienciaDaoImpl();
        
        try {
        	dao.create(deficiencia);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Deficiencia.", e);
		}
        
    }

    public Deficiencia ReadById(long deficienciaId) {
    	DeficienciaDao dao = new DeficienciaDaoImpl();
    	Deficiencia deficiencia = null;
        
        try {
        	deficiencia = dao.ReadById(deficienciaId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); 
		}
        
        return deficiencia;
    }

    public Deficiencia ReadByCod(String codIdentificacion) {
    	DeficienciaDao dao = new DeficienciaDaoImpl();
    	Deficiencia deficiencia = null;
        
        try {
        	deficiencia = dao.ReadByCod(codIdentificacion);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); 
		}
        
        return deficiencia;
    }
    
    public void update(Deficiencia deficiencia) {
    	DeficienciaDao dao = new DeficienciaDaoImpl();
        
        try {
        	dao.update(deficiencia);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public void delete(long deficienciaId) {
    	DeficienciaDao dao = new DeficienciaDaoImpl();
        
        try {
        	dao.delete(deficienciaId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public PeriodoMedicion selectPeriodo(long deficienciaId) {
    	DeficienciaDao dao = new DeficienciaDaoImpl();
        return dao.selectPeriodo(deficienciaId);
    }
    

    public EstadoSubsanacion selectEstadoSubsanacion(long deficienciaId) {
    	DeficienciaDao dao = new DeficienciaDaoImpl();
        return dao.selectEstadoSubsanacion(deficienciaId);
    }

     
    
    public List<Deficiencia> getAllRows1(long perMedicionId){
    	DeficienciaDao dao = new DeficienciaDaoImpl();
    	return dao.getAllRows1(perMedicionId);
    } 
    
    public List<Deficiencia> getAllRows1(){
    	DeficienciaDao dao = new DeficienciaDaoImpl();
    	return dao.getAllRows1();
    }     
    
    public List<Deficiencia> getDeficienciaxSuministro(String numSuministro){
    	DeficienciaDao dao = new DeficienciaDaoImpl();
    	List<Deficiencia> deficiencia=null;
    	try {
    		deficiencia = dao.getDeficienciaxSuministro(numSuministro);
			
		} catch (HibernateException e) {
			throw new HibernateException("No se puede encontrar Deficiencias para el Suministro.", e);
		}
    	  	
    	return deficiencia;
    }
    
    public List<Deficiencia> getBusquedaxDeficiencia(String codIdentificacion){
    	DeficienciaDao dao = new DeficienciaDaoImpl();
    	List<Deficiencia> deficiencia=null;
    	try {
    		deficiencia = dao.getBusquedaxDeficiencia(codIdentificacion);
			
		} catch (HibernateException e) {
			throw new HibernateException("No se puede encontrar Deficiencia .", e);
		}
    	  	
    	return deficiencia;
    }
    
    public List<Deficiencia> getAllRows(long perMedicionId){
    	DeficienciaDao dao = new DeficienciaDaoImpl();
    	return dao.getAllRows(perMedicionId);
    }  
    
    public List<Deficiencia> getAllRowsEstado(String ano){
    	DeficienciaDao dao = new DeficienciaDaoImpl();
    	List<Deficiencia> deficiencia=null;
    	try {
    		deficiencia = dao.getAllRowsEstado(ano);
			
		} catch (HibernateException e) {
			throw new HibernateException("No se puede encontrar Deficiencia Estado por subsanar1.", e);
		}
    	return deficiencia;
    } 
}
