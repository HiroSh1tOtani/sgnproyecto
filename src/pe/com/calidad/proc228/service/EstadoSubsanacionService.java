package pe.com.calidad.proc228.service;

import java.util.Set;
import java.util.List;

import org.hibernate.HibernateException;

import pe.com.calidad.proc228.dao.EstadoSubsanacionDao;
import pe.com.calidad.proc228.dao.EstadoSubsanacionDaoImpl;
import pe.com.calidad.proc228.entity.EstadoSubsanacion;
import pe.com.calidad.suministro.dao.CausaInterrupcionDao;
import pe.com.calidad.suministro.dao.CausaInterrupcionDaoImpl;
import pe.com.calidad.suministro.entity.CausaInterrupcion;

/**
 *
 * @author Lucho
 */
public class EstadoSubsanacionService {

	 public EstadoSubsanacion ReadById(long estSubsanacionId) {
		 EstadoSubsanacionDao dao = new EstadoSubsanacionDaoImpl();
	        
		 EstadoSubsanacion estadoSubsanacion = null;
	        
	        try {
	        	estadoSubsanacion = dao.ReadById(estSubsanacionId);
			} catch (HibernateException e) {
				e.printStackTrace();
				throw new HibernateException(e.getMessage(), e); //"No se puede leer estadoSubsanacion."
			}
	        
	        return estadoSubsanacion;
	        
	    }
	
    public List<EstadoSubsanacion>getAllRows(){
    	EstadoSubsanacionDao dao = new EstadoSubsanacionDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<EstadoSubsanacion>getAllRows1(){
    	EstadoSubsanacionDao dao = new EstadoSubsanacionDaoImpl();
    	return dao.getAllRows1();
    }
}


