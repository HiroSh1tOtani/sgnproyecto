package pe.com.calidad.proc228.service;


import java.util.List;


import pe.com.calidad.proc228.dao.TipoConexionMTDao;
import pe.com.calidad.proc228.dao.TipoConexionMTDaoImpl;
import pe.com.calidad.proc228.entity.TipoConexionMT;

/**
 *
 * @author Lucho
 */
public class TipoConexionMTService {

    
    public List<TipoConexionMT>getAllRows(){
    	TipoConexionMTDao dao = new TipoConexionMTDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<TipoConexionMT>getAllRows1(){
    	TipoConexionMTDao dao = new TipoConexionMTDaoImpl();
    	return dao.getAllRows1();
    }
}


