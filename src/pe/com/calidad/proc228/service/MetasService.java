package pe.com.calidad.proc228.service;

import java.util.List;

import org.hibernate.HibernateException;

import pe.com.calidad.proc228.dao.MetasDao;//RelevadorRechazoCargaDao;
import pe.com.calidad.proc228.dao.MetasDaoImpl;//RelevadorRechazoCargaDaoImpl;
import pe.com.calidad.proc228.entity.Metas;//RelevadorRechazoCarga;
import pe.com.indra.calidad.dao.MedicionDao;
import pe.com.indra.calidad.dao.MedicionDaoImpl;
import pe.com.indra.calidad.entity.Medicion;


/**
 *
 * @author Luis
 */
public class MetasService {

    public void create(Metas metas) {
    	MetasDao dao = new MetasDaoImpl();
        
        try {
        	dao.create(metas);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Metas.", e);
		}
        
    }

    public Metas ReadById(long metasId) {
    	MetasDao dao = new MetasDaoImpl();
    	Metas metas = null;
        
        try {
        	metas = dao.ReadById(metasId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); 
		}
        
        return metas;
    }

    public void update(Metas metas) {
    	MetasDao dao = new MetasDaoImpl();
        
        try {
        	dao.update(metas);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public void delete(long metasId) {
    	MetasDao dao = new MetasDaoImpl();
        
        try {
        	dao.delete(metasId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

     
    public List<Metas> getAllRows(){
    	MetasDao dao = new MetasDaoImpl();
    	return dao.getAllRows();
    }  
    
    public List<Metas> getAllRows1(){
    	MetasDao dao = new MetasDaoImpl();
    	return dao.getAllRows();
    }   
    
    public List<Metas> getBusquedaxAno(String ano) {
    	MetasDao dao = new MetasDaoImpl();
    	List<Metas> metas=null;
    	try {
    		metas = dao.getBusquedaxAno(ano);
			
		} catch (HibernateException e) {
			throw new HibernateException("No se puede encontrar Metas para el a�o .", e);
		}
    	  	
    	return metas;
    	
    	
    }
   
}
