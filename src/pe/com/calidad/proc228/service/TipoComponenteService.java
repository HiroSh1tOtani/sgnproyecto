package pe.com.calidad.proc228.service;


import java.util.List;


import pe.com.calidad.proc228.dao.TipoComponenteDao;
import pe.com.calidad.proc228.dao.TipoComponenteDaoImpl;
import pe.com.calidad.proc228.entity.TipoComponente;

/**
 *
 * @author Lucho
 */
public class TipoComponenteService {

    
    public List<TipoComponente>getAllRows(){
    	TipoComponenteDao dao = new TipoComponenteDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<TipoComponente>getAllRows1(){
    	TipoComponenteDao dao = new TipoComponenteDaoImpl();
    	return dao.getAllRows1();
    }
}


