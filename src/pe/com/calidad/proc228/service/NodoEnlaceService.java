package pe.com.calidad.proc228.service;

import java.util.Set;
import java.util.List;

import org.hibernate.HibernateException;

import pe.com.calidad.proc228.dao.NodoEnlaceDao;
import pe.com.calidad.proc228.dao.NodoEnlaceDaoImpl;
import pe.com.calidad.proc228.entity.NodoEnlace;

/**
 *
 * @author Lucho
 */
public class NodoEnlaceService {

    
    public List<NodoEnlace>getAllRows(){
    	NodoEnlaceDao dao = new NodoEnlaceDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<NodoEnlace>getAllRows1(){
    	NodoEnlaceDao dao = new NodoEnlaceDaoImpl();
    	return dao.getAllRows1();
    }
}


