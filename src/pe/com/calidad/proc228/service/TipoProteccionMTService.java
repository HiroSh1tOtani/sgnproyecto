package pe.com.calidad.proc228.service;


import java.util.List;


import pe.com.calidad.proc228.dao.TipoProteccionMTDao;
import pe.com.calidad.proc228.dao.TipoProteccionMTDaoImpl;
import pe.com.calidad.proc228.entity.TipoProteccionMT;

/**
 *
 * @author Lucho
 */
public class TipoProteccionMTService {

    
    public List<TipoProteccionMT>getAllRows(){
    	TipoProteccionMTDao dao = new TipoProteccionMTDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<TipoProteccionMT>getAllRows1(){
    	TipoProteccionMTDao dao = new TipoProteccionMTDaoImpl();
    	return dao.getAllRows1();
    }
}


