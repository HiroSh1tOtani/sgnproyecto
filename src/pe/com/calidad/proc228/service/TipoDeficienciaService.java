package pe.com.calidad.proc228.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;


import pe.com.calidad.proc228.dao.TipoDeficienciaDao;
import pe.com.calidad.proc228.dao.TipoDeficienciaDaoImpl;
import pe.com.calidad.proc228.entity.TipoDeficiencia;

/**
 *
 * @author Luis
 */
public class TipoDeficienciaService {

	
	 public TipoDeficiencia ReadById(long tipDeficienciaId) {
		 TipoDeficienciaDao dao = new TipoDeficienciaDaoImpl();
	        
		 TipoDeficiencia tipoDeficiencia = null;
	        
	        try {
	        	tipoDeficiencia = dao.ReadById(tipDeficienciaId);
			} catch (HibernateException e) {
				e.printStackTrace();
				throw new HibernateException(e.getMessage(), e); //"No se puede leer TipoDeficiencia."
			}
	        
	        return tipoDeficiencia;
	        
	    }
    
    public List<TipoDeficiencia> getAllRows1(long tipInstalacionId){
    	TipoDeficienciaDao dao = new TipoDeficienciaDaoImpl();
    	return dao.getAllRows1(tipInstalacionId);
    } 
    
    public List<TipoDeficiencia> getAllRows1(){
    	TipoDeficienciaDao dao = new TipoDeficienciaDaoImpl();
    	return dao.getAllRows1();
    } 
  
    
}
