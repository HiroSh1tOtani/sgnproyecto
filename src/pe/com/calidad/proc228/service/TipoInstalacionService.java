package pe.com.calidad.proc228.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;

import pe.com.calidad.proc228.dao.TipoInstalacionDao;//DeficienciaDao;
import pe.com.calidad.proc228.dao.TipoInstalacionDaoImpl;//DeficienciaDaoImpl;
import pe.com.calidad.proc228.entity.TipoDeficiencia;
import pe.com.calidad.proc228.entity.TipoInstalacion;//Deficiencia;
import pe.com.calidad.suministro.dao.CausaInterrupcionDao;
import pe.com.calidad.suministro.dao.CausaInterrupcionDaoImpl;
import pe.com.calidad.suministro.entity.Interrupcion;


/**
 *
 * @author Luis
 */
public class TipoInstalacionService {

    public void create(TipoInstalacion tipoInstalacion) {
    	TipoInstalacionDao dao = new TipoInstalacionDaoImpl();
        
        try {
        	dao.create(tipoInstalacion);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Tipo Instalacion.", e);
		}
        
    }

    public TipoInstalacion ReadById(long tipInstalacionId) {
    	TipoInstalacionDao dao = new TipoInstalacionDaoImpl();
    	TipoInstalacion tipoInstalacion = null;
        
        try {
        	tipoInstalacion = dao.ReadById(tipInstalacionId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); 
		}
        
        return tipoInstalacion;
    }

    public TipoInstalacion ReadByCod(String codTipInstalacion) {
    	TipoInstalacionDao dao = new TipoInstalacionDaoImpl();
    	TipoInstalacion tipoInstalacion = null;
        
        try {
        	tipoInstalacion = dao.ReadByCod(codTipInstalacion);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); 
		}
        
        return tipoInstalacion;
    }
    
    public void update(TipoInstalacion tipoInstalacion) {
    	TipoInstalacionDao dao = new TipoInstalacionDaoImpl();
        
        try {
        	dao.update(tipoInstalacion);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public void delete(long tipInstalacionId) {
    	TipoInstalacionDao dao = new TipoInstalacionDaoImpl();
        
        try {
        	dao.delete(tipInstalacionId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public Set<TipoDeficiencia> selectTipoDeficiencia(long tipInstalacionId){
    	TipoInstalacionDao dao = new TipoInstalacionDaoImpl();
    	return dao.selectTipoDeficiencia(tipInstalacionId);
    }
    
    
    
    public List<TipoInstalacion> getAllRows1(){
    	TipoInstalacionDao dao = new TipoInstalacionDaoImpl();
    	return dao.getAllRows1();
    } 
    
  
    
}
