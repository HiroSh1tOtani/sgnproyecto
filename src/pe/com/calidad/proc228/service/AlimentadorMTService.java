package pe.com.calidad.proc228.service;

import java.util.List;

import org.hibernate.HibernateException;

import pe.com.calidad.proc228.dao.AlimentadorMTDao;
import pe.com.calidad.proc228.dao.AlimentadorMTDaoImpl;
import pe.com.calidad.proc228.entity.AlimentadorMT;


/**
 *
 * @author Luis
 */
public class AlimentadorMTService {

        
	public List<AlimentadorMT> getAllRows() {
		AlimentadorMTDao dao = new AlimentadorMTDaoImpl();
		return dao.getAllRows();
	}
	
	public List<AlimentadorMT> getAllRows1() {
		AlimentadorMTDao dao = new AlimentadorMTDaoImpl();
		return dao.getAllRows1();
	}
    
}
