package pe.com.calidad.proc228.service;


import java.util.List;


import pe.com.calidad.proc228.dao.VerTramoMTDao;
import pe.com.calidad.proc228.dao.VerTramoMTDaoImpl;
import pe.com.calidad.proc228.entity.VerTramoMT;

/**
 *
 * @author Lucho
 */
public class VerTramoMTService {

    
    public List<VerTramoMT>getAllRows(){
    	VerTramoMTDao dao = new VerTramoMTDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<VerTramoMT>getAllRows1(){
    	VerTramoMTDao dao = new VerTramoMTDaoImpl();
    	return dao.getAllRows1();
    }
}


