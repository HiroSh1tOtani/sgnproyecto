package pe.com.calidad.proc228.service;


import java.util.List;


import pe.com.calidad.proc228.dao.TramoMTDao;
import pe.com.calidad.proc228.dao.TramoMTDaoImpl;
import pe.com.calidad.proc228.entity.TramoMT;

/**
 *
 * @author Lucho
 */
public class TramoMTService {

    
    public List<TramoMT>getAllRows(){
    	TramoMTDao dao = new TramoMTDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<TramoMT>getAllRows1(){
    	TramoMTDao dao = new TramoMTDaoImpl();
    	return dao.getAllRows1();
    }
}


