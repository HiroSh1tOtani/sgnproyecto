package pe.com.calidad.proc228.service;

import java.util.Set;
import java.util.List;

import org.hibernate.HibernateException;

import pe.com.calidad.proc228.dao.EstructuraMTDao;
import pe.com.calidad.proc228.dao.EstructuraMTDaoImpl;
import pe.com.calidad.proc228.entity.EstructuraMT;

/**
 *
 * @author Lucho
 */
public class EstructuraMTService {

    
    public List<EstructuraMT>getAllRows(){
    	EstructuraMTDao dao = new EstructuraMTDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<EstructuraMT>getAllRows1(){
    	EstructuraMTDao dao = new EstructuraMTDaoImpl();
    	return dao.getAllRows1();
    }
}


