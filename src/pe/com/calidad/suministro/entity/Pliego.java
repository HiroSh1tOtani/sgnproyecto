
package pe.com.calidad.suministro.entity;


import java.io.Serializable;
import java.sql.Date;
import java.util.Set;

import javax.persistence.*;

/**

* @author Luis
*/
@Entity
@Table(name="CAL_PLIEGO")
@SequenceGenerator(name = "PliegoIdIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "PliegoSeq")
public class Pliego implements Serializable{
    
	private long pliegoId;
	private String descripcion;
	private Date fecVigHasta;
	private Date fecVigDesde;
	private String estado;
    private Set<CargoTarifa> cargosTarifa;
   
    public Pliego() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PliegoIdIdGenerator")
    @Column(name = "PLIEGO_ID",nullable=false,unique=true)
    public long getPliegoId() {
		return pliegoId;
	}

	public void setPliegoId(long pliegoId) {
		this.pliegoId = pliegoId;
	}

	@Column(name="DESCRIPCION",nullable=true)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name="FEC_VIG_HASTA",nullable=true)
	public Date getFecVigHasta() {
		return fecVigHasta;
	}

	public void setFecVigHasta(Date fecVigHasta) {
		this.fecVigHasta = fecVigHasta;
	}

	@Column(name="FEC_VIG_DESDE",nullable=true)
	public Date getFecVigDesde() {
		return fecVigDesde;
	}

	public void setFecVigDesde(Date fecVigDesde) {
		this.fecVigDesde = fecVigDesde;
	}

	@Column(name="ESTADO",nullable=true)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	 @OneToMany(cascade = CascadeType.ALL,mappedBy = "pliego")
	public Set<CargoTarifa> getCargosTarifa() {
		return cargosTarifa;
	}

	public void setCargosTarifa(Set<CargoTarifa> cargosTarifa) {
		this.cargosTarifa = cargosTarifa;
	}
   
	public String toString(){
		return new Long(this.getPliegoId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getPliegoId()==((Pliego)obj).getPliegoId()){
			return true;
		}else{
			return false;
		}
	}
}
