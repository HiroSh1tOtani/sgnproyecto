
package pe.com.calidad.suministro.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;


import pe.com.indra.calidad.entity.PeriodoMedicion;
/**
 * Indicador 074
*
* @version 1.0 10 Marz 2020
* @author Hiroshi
 */
@Entity
@Table(name="CAL_CLIENTEXALIMENTADOR")
@SequenceGenerator (name = "CliexAlimentadorIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "ClientexAlimentadorSeq")
public class ClientexAlimentador implements Serializable {
    
	private long cliexAlimentadorId;
	private String alimentador; 
 	private Long sumCantidad;
 		
    private PeriodoMedicion periodoMedicion;
    
    
	public ClientexAlimentador() {
    }
    
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CliexAlimentadorIdGenerator")
    @Column(name = "CLIXALIMENTADOR_ID",nullable=false,unique=true)
	public long getCliexAlimentadorId() {
		return cliexAlimentadorId;
	}

	public void setCliexAlimentadorId(long cliexAlimentadorId) {
		this.cliexAlimentadorId = cliexAlimentadorId;
	}

	@Column(name = "ALIMENTADOR",nullable=true, length = 20)
	public String getAlimentador() {
		return alimentador;
	}

	public void setAlimentador(String alimentador) {
		this.alimentador = alimentador;
	}

	@Column(name = "SUM_CANTIDAD",nullable=true)
	public Long getSumCantidad() {
		return sumCantidad;
	}

	public void setSumCantidad(Long sumCantidad) {
		this.sumCantidad = sumCantidad;
	}


	@ManyToOne
	@JoinColumn(name = "PK_PER_MEDICION_ID",nullable=true)
	public PeriodoMedicion getPeriodoMedicion() {
		return periodoMedicion;
	}

	public void setPeriodoMedicion(PeriodoMedicion periodoMedicion) {
		this.periodoMedicion = periodoMedicion;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this.getCliexAlimentadorId()==((ClientexAlimentador)obj).getCliexAlimentadorId()){
			return true;
		}else{
			return false;
		}
	}
		
}
