
package pe.com.calidad.suministro.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;


import pe.com.indra.calidad.entity.PeriodoMedicion;
/**
 * Indicador 074
*
* @version 1.0 10 Marz 2020
* @author Hiroshi
 */
@Entity
@Table(name="CAL_CLIENTEXSECTORTIPICO")
@SequenceGenerator (name = "CliexSectorTipicoIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "ClientexSectorTipicoSeq")
public class ClientexSectorTipico implements Serializable {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long cliexSectorTipicoId;
 	private Integer sectorTipico; 
 	private Long sumCantidad;
 		
    private PeriodoMedicion periodoMedicion;
    
    
	public ClientexSectorTipico() {
    }
    
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CliexSectorTipicoIdGenerator")
    @Column(name = "CLIXSECTIPICO_ID",nullable=false,unique=true)
	public long getCliexSectorTipicoId() {
		return cliexSectorTipicoId;
	}

	public void setCliexSectorTipicoId(long cliexSectorTipicoId) {
		this.cliexSectorTipicoId = cliexSectorTipicoId;
	}

	@Column(name = "SECTOR_TIPICO",nullable=true)
	public Integer getSectorTipico() {
		return sectorTipico;
	}

	public void setSectorTipico(Integer sectorTipico) {
		this.sectorTipico = sectorTipico;
	}

	@Column(name = "SUM_CANTIDAD",nullable=true)
	public Long getSumCantidad() {
		return sumCantidad;
	}

	public void setSumCantidad(Long sumCantidad) {
		this.sumCantidad = sumCantidad;
	}


	@ManyToOne
	@JoinColumn(name = "PK_PER_MEDICION_ID",nullable=true)
	public PeriodoMedicion getPeriodoMedicion() {
		return periodoMedicion;
	}

	public void setPeriodoMedicion(PeriodoMedicion periodoMedicion) {
		this.periodoMedicion = periodoMedicion;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this.getCliexSectorTipicoId()==((ClientexSectorTipico)obj).getCliexSectorTipicoId()){
			return true;
		}else{
			return false;
		}
	}
		
}
