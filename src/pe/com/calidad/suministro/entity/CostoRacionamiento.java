
package pe.com.calidad.suministro.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.*;


/**

* 
* @author Luis
*/

@Entity
@Table(name="CAL_COSTO_RACIONAMIENTO")
@SequenceGenerator (name = "CosRacionamientoIdIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "CostoRacionamientoSeq")
public class CostoRacionamiento implements Serializable{
   
    private long cosRacionamientoId;
    private BigDecimal cosRacionamiento;
    private Date fecVigDesde;
    private Date fecVigHasta;
    private String estado;

    
  	public CostoRacionamiento() {
    }


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CosRacionamientoIdIdGenerator")
    @Column(name = "COS_RACIONAMIENTO_ID",nullable=false,unique=true)
    public long getCosRacionamientoId() {
		return cosRacionamientoId;
	}

	public void setCosRacionamientoId(long cosRacionamientoId) {
		this.cosRacionamientoId = cosRacionamientoId;
	}


	@Column(name = "COS_RACIONAMIENTO",nullable=true)
	public BigDecimal getCosRacionamiento() {
		return cosRacionamiento;
	}

	public void setCosRacionamiento(BigDecimal cosRacionamiento) {
		this.cosRacionamiento = cosRacionamiento;
	}

	@Column(name = "FEC_VIG_DESDE",nullable=true)
	public Date getFecVigDesde() {
		return fecVigDesde;
	}

	public void setFecVigDesde(Date fecVigDesde) {
		this.fecVigDesde = fecVigDesde;
	}

	@Column(name = "FEC_VIG_HASTA",nullable=true)
	public Date getFecVigHasta() {
		return fecVigHasta;
	}

	public void setFecVigHasta(Date fecVigHasta) {
		this.fecVigHasta = fecVigHasta;
	}

	@Column(name = "ESTADO",nullable=true)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	 	
}
