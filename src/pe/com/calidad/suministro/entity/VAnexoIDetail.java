
package pe.com.calidad.suministro.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.math.BigDecimal;

/**
 * Suministro Afectado Suministro.
*
* @version 1.0 05 Mar 2014
* @author Luis
 */
public class VAnexoIDetail implements Serializable {
    
	private static final long serialVersionUID = 1L;
	
	private String codEmpresa;
	private String sisElectrico;
	private String codSecOsinerg;
	private Long codInterrupcion;
	private Integer tipInsSalio;
	private String codInsSalio;
	private Integer tipInsFalla;
	private String codInsFalla;
	private Timestamp fecIniInterrupcion;
	private Timestamp fecFinComSalio;
	private Timestamp fecFinInterrupcion;
	private Long numSumRegAfectados;
	private Long potIntEstimada;
	private BigDecimal durHorSuministro;
	private String naturaleza;
	private String nivelTension;
	private String propiedad;
	private String responsable;
	private String codCausa074;
	private String solFueMayor;
	private String tipoRelevador;
	private Long codosi;
	private Long nroClieSisElectrico;
	private BigDecimal saifi;
	private BigDecimal saidi;
	private String causa;
	private String responsabilidad;
	private String meses;
	private Integer sectorTipico;
	private Long clientesXSectorTipico;
	private BigDecimal saifiSt;
	private BigDecimal saidiSt;
	private Long clientesElsm;
	private BigDecimal saifiElsm;
	private BigDecimal saidiElsm;
	private String zonal;
	private String tipoInterrupcion;
	private BigDecimal duracion;
	private String ser;
	private BigDecimal nic;
	private BigDecimal factorK;
	private BigDecimal dic;
	private String semestre;
	private String alimentadorMt;
	private Long nroClieAlimentador;
	private BigDecimal saifiAlimentador;
	private BigDecimal saidiAlimentador;
	
  
	public VAnexoIDetail() {
    }
	
	public VAnexoIDetail(String codEmpresa, String sisElectrico, String codSecOsinerg, Long codInterrupcion,
			Integer tipInsSalio, String codInsSalio, Integer tipInsFalla, String codInsFalla, Timestamp fecIniInterrupcion,
			Timestamp fecFinComSalio, Timestamp fecFinInterrupcion, Long numSumRegAfectados, Long potIntEstimada,
			BigDecimal durHorSuministro, String naturaleza, String nivelTension, String propiedad, String responsable,
			String codCausa074, String solFueMayor, String tipoRelevador, Long codosi, Long nroClieSisElectrico,
			BigDecimal saifi, BigDecimal saidi, String causa, String responsabilidad, String meses,
			Integer sectorTipico, Long clientesXSectorTipico, BigDecimal saifiSt, BigDecimal saidiSt, Long clientesElsm,
			BigDecimal saifiElsm, BigDecimal saidiElsm, String zonal, String tipoInterrupcion, BigDecimal duracion,
			String ser, BigDecimal nic, BigDecimal factorK, BigDecimal dic, String semestre, String alimentadorMt,
			Long nroClieAlimentador, BigDecimal saifiAlimentador, BigDecimal saidiAlimentador) {
		super();
		this.codEmpresa = codEmpresa;
		this.sisElectrico = sisElectrico;
		this.codSecOsinerg = codSecOsinerg;
		this.codInterrupcion = codInterrupcion;
		this.tipInsSalio = tipInsSalio;
		this.codInsSalio = codInsSalio;
		this.tipInsFalla = tipInsFalla;
		this.codInsFalla = codInsFalla;
		this.fecIniInterrupcion = fecIniInterrupcion;
		this.fecFinComSalio = fecFinComSalio;
		this.fecFinInterrupcion = fecFinInterrupcion;
		this.numSumRegAfectados = numSumRegAfectados;
		this.potIntEstimada = potIntEstimada;
		this.durHorSuministro = durHorSuministro;
		this.naturaleza = naturaleza;
		this.nivelTension = nivelTension;
		this.propiedad = propiedad;
		this.responsable = responsable;
		this.codCausa074 = codCausa074;
		this.solFueMayor = solFueMayor;
		this.tipoRelevador = tipoRelevador;
		this.codosi = codosi;
		this.nroClieSisElectrico = nroClieSisElectrico;
		this.saifi = saifi;
		this.saidi = saidi;
		this.causa = causa;
		this.responsabilidad = responsabilidad;
		this.meses = meses;
		this.sectorTipico = sectorTipico;
		this.clientesXSectorTipico = clientesXSectorTipico;
		this.saifiSt = saifiSt;
		this.saidiSt = saidiSt;
		this.clientesElsm = clientesElsm;
		this.saifiElsm = saifiElsm;
		this.saidiElsm = saidiElsm;
		this.zonal = zonal;
		this.tipoInterrupcion = tipoInterrupcion;
		this.duracion = duracion;
		this.ser = ser;
		this.nic = nic;
		this.factorK = factorK;
		this.dic = dic;
		this.semestre = semestre;
		this.alimentadorMt = alimentadorMt;
		this.nroClieAlimentador = nroClieAlimentador;
		this.saifiAlimentador = saifiAlimentador;
		this.saidiAlimentador = saidiAlimentador;
	}



	public String getCodEmpresa() {
		return codEmpresa;
	}

	public void setCodEmpresa(String codEmpresa) {
		this.codEmpresa = codEmpresa;
	}

	public String getSisElectrico() {
		return sisElectrico;
	}

	public void setSisElectrico(String sisElectrico) {
		this.sisElectrico = sisElectrico;
	}

	public String getCodSecOsinerg() {
		return codSecOsinerg;
	}

	public void setCodSecOsinerg(String codSecOsinerg) {
		this.codSecOsinerg = codSecOsinerg;
	}

	public Long getCodInterrupcion() {
		return codInterrupcion;
	}

	public void setCodInterrupcion(Long codInterrupcion) {
		this.codInterrupcion = codInterrupcion;
	}

	public Integer getTipInsSalio() {
		return tipInsSalio;
	}

	public void setTipInsSalio(Integer tipInsSalio) {
		this.tipInsSalio = tipInsSalio;
	}

	public String getCodInsSalio() {
		return codInsSalio;
	}

	public void setCodInsSalio(String codInsSalio) {
		this.codInsSalio = codInsSalio;
	}

	public Integer getTipInsFalla() {
		return tipInsFalla;
	}

	public void setTipInsFalla(Integer tipInsFalla) {
		this.tipInsFalla = tipInsFalla;
	}

	public String getCodInsFalla() {
		return codInsFalla;
	}

	public void setCodInsFalla(String codInsFalla) {
		this.codInsFalla = codInsFalla;
	}

	public Timestamp getFecIniInterrupcion() {
		return fecIniInterrupcion;
	}

	public void setFecIniInterrupcion(Timestamp fecIniInterrupcion) {
		this.fecIniInterrupcion = fecIniInterrupcion;
	}

	public Timestamp getFecFinComSalio() {
		return fecFinComSalio;
	}

	public void setFecFinComSalio(Timestamp fecFinComSalio) {
		this.fecFinComSalio = fecFinComSalio;
	}

	public Timestamp getFecFinInterrupcion() {
		return fecFinInterrupcion;
	}

	public void setFecFinInterrupcion(Timestamp fecFinInterrupcion) {
		this.fecFinInterrupcion = fecFinInterrupcion;
	}

	public Long getNumSumRegAfectados() {
		return numSumRegAfectados;
	}

	public void setNumSumRegAfectados(Long numSumRegAfectados) {
		this.numSumRegAfectados = numSumRegAfectados;
	}

	public Long getPotIntEstimada() {
		return potIntEstimada;
	}

	public void setPotIntEstimada(Long potIntEstimada) {
		this.potIntEstimada = potIntEstimada;
	}

	public BigDecimal getDurHorSuministro() {
		return durHorSuministro;
	}

	public void setDurHorSuministro(BigDecimal durHorSuministro) {
		this.durHorSuministro = durHorSuministro;
	}

	public String getNaturaleza() {
		return naturaleza;
	}

	public void setNaturaleza(String naturaleza) {
		this.naturaleza = naturaleza;
	}

	public String getNivelTension() {
		return nivelTension;
	}

	public void setNivelTension(String nivelTension) {
		this.nivelTension = nivelTension;
	}

	public String getPropiedad() {
		return propiedad;
	}

	public void setPropiedad(String propiedad) {
		this.propiedad = propiedad;
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public String getCodCausa074() {
		return codCausa074;
	}

	public void setCodCausa074(String codCausa074) {
		this.codCausa074 = codCausa074;
	}

	public String getSolFueMayor() {
		return solFueMayor;
	}

	public void setSolFueMayor(String solFueMayor) {
		this.solFueMayor = solFueMayor;
	}

	public String getTipoRelevador() {
		return tipoRelevador;
	}

	public void setTipoRelevador(String tipoRelevador) {
		this.tipoRelevador = tipoRelevador;
	}

	public Long getCodosi() {
		return codosi;
	}

	public void setCodosi(Long codosi) {
		this.codosi = codosi;
	}

	public Long getNroClieSisElectrico() {
		return nroClieSisElectrico;
	}

	public void setNroClieSisElectrico(Long nroClieSisElectrico) {
		this.nroClieSisElectrico = nroClieSisElectrico;
	}

	public BigDecimal getSaifi() {
		return saifi;
	}

	public void setSaifi(BigDecimal saifi) {
		this.saifi = saifi;
	}

	public BigDecimal getSaidi() {
		return saidi;
	}

	public void setSaidi(BigDecimal saidi) {
		this.saidi = saidi;
	}

	public String getCausa() {
		return causa;
	}

	public void setCausa(String causa) {
		this.causa = causa;
	}

	public String getResponsabilidad() {
		return responsabilidad;
	}

	public void setResponsabilidad(String responsabilidad) {
		this.responsabilidad = responsabilidad;
	}

	public String getMeses() {
		return meses;
	}

	public void setMeses(String meses) {
		this.meses = meses;
	}

	public Integer getSectorTipico() {
		return sectorTipico;
	}

	public void setSectorTipico(Integer sectorTipico) {
		this.sectorTipico = sectorTipico;
	}

	public Long getClientesXSectorTipico() {
		return clientesXSectorTipico;
	}

	public void setClientesXSectorTipico(Long clientesXSectorTipico) {
		this.clientesXSectorTipico = clientesXSectorTipico;
	}

	public BigDecimal getSaifiSt() {
		return saifiSt;
	}

	public void setSaifiSt(BigDecimal saifiSt) {
		this.saifiSt = saifiSt;
	}

	public BigDecimal getSaidiSt() {
		return saidiSt;
	}

	public void setSaidiSt(BigDecimal saidiSt) {
		this.saidiSt = saidiSt;
	}

	public Long getClientesElsm() {
		return clientesElsm;
	}

	public void setClientesElsm(Long clientesElsm) {
		this.clientesElsm = clientesElsm;
	}

	public BigDecimal getSaifiElsm() {
		return saifiElsm;
	}

	public void setSaifiElsm(BigDecimal saifiElsm) {
		this.saifiElsm = saifiElsm;
	}

	public BigDecimal getSaidiElsm() {
		return saidiElsm;
	}

	public void setSaidiElsm(BigDecimal saidiElsm) {
		this.saidiElsm = saidiElsm;
	}

	public String getZonal() {
		return zonal;
	}

	public void setZonal(String zonal) {
		this.zonal = zonal;
	}

	public String getTipoInterrupcion() {
		return tipoInterrupcion;
	}

	public void setTipoInterrupcion(String tipoInterrupcion) {
		this.tipoInterrupcion = tipoInterrupcion;
	}

	public BigDecimal getDuracion() {
		return duracion;
	}

	public void setDuracion(BigDecimal duracion) {
		this.duracion = duracion;
	}

	public String getSer() {
		return ser;
	}

	public void setSer(String ser) {
		this.ser = ser;
	}

	public BigDecimal getNIC() {
		return nic;
	}

	public void setNIC(BigDecimal nic) {
		this.nic = nic;
	}

	public BigDecimal getFactorK() {
		return factorK;
	}

	public void setFactorK(BigDecimal factorK) {
		this.factorK = factorK;
	}

	public BigDecimal getDic() {
		return dic;
	}

	public void setDic(BigDecimal dic) {
		this.dic = dic;
	}

	public String getSemestre() {
		return semestre;
	}

	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}

	public BigDecimal getNic() {
		return nic;
	}

	public void setNic(BigDecimal nic) {
		this.nic = nic;
	}

	public String getAlimentadorMt() {
		return alimentadorMt;
	}

	public void setAlimentadorMt(String alimentadorMt) {
		this.alimentadorMt = alimentadorMt;
	}

	public Long getNroClieAlimentador() {
		return nroClieAlimentador;
	}

	public void setNroClieAlimentador(Long nroClieAlimentador) {
		this.nroClieAlimentador = nroClieAlimentador;
	}

	public BigDecimal getSaifiAlimentador() {
		return saifiAlimentador;
	}

	public void setSaifiAlimentador(BigDecimal saifiAlimentador) {
		this.saifiAlimentador = saifiAlimentador;
	}

	public BigDecimal getSaidiAlimentador() {
		return saidiAlimentador;
	}

	public void setSaidiAlimentador(BigDecimal saidiAlimentador) {
		this.saidiAlimentador = saidiAlimentador;
	}
		
}
