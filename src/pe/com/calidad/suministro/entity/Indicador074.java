
package pe.com.calidad.suministro.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import java.math.BigDecimal;

import javax.persistence.*;

import pe.com.indra.calidad.entity.Localidad;
import pe.com.indra.calidad.entity.PeriodoMedicion;
/**
 * Indicador 074
*
* @version 1.0 26 jun 2014
* @author Luis
 */
@Entity
@Table(name="CAL_INDICADOR_074")
@SequenceGenerator (name = "CalIndicadorIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "Indicador074Seq")
public class Indicador074 implements Serializable {
    
	private long calIndicadorId;
 
	private String sisElectrico; 
	private long nroCliSisElectrico;
	private BigDecimal saifiSisElectrico; 
	private BigDecimal saifiIntProgramadas;
	private BigDecimal saifiIntNoProgramadas;  
	private BigDecimal saifiRecCarga;
	private BigDecimal saifiInsDistribucion;
	private BigDecimal saifiInsTransmision; 
	private BigDecimal saifiInsGeneracion;
	private BigDecimal SaifiCauPropias;
	private BigDecimal SaifiCauTerceros; 
	private BigDecimal SaifiCauOtras;       
	private BigDecimal SaifiCauFenomenos;   
	private BigDecimal SaifiFueMayor;      
	private BigDecimal SaidiSisElectrico; 
	private BigDecimal SaidiIntProgramadas; 
	private BigDecimal SaidiIntNoProgramadas;
	private BigDecimal SaidiRecCarga;     
	private BigDecimal SaidiInsDistribucion;   
	private BigDecimal SaidiInsTransmision;  
	private BigDecimal SaidiInsGeneracion;   
	private BigDecimal SaidiCauPropias;     
	private BigDecimal SaidiCauTerceros;    
	private BigDecimal SaidiCauOtras;       
	private BigDecimal SaidiCauFenomenos;   
	private BigDecimal SaidiFueMayor;        

	private String estado;
    private PeriodoMedicion periodoMedicion;
    
    
	public Indicador074() {
    }
    
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CalIndicadorIdGenerator")
    @Column(name = "CAL_INDICADOR_ID",nullable=false,unique=true)
	public long getCalIndicadorId() {
		return calIndicadorId;
	}

	public void setCalIndicadorId(long calIndicadorId) {
		this.calIndicadorId = calIndicadorId;
	}

	@Column(name = "SIS_ELECTRICO",nullable=true)
	public String getSisElectrico() {
		return sisElectrico;
	}

	public void setSisElectrico(String sisElectrico) {
		this.sisElectrico = sisElectrico;
	}

	@Column(name = "NRO_CLI_SIS_ELECTRICO",nullable=true)
	public long getNroCliSisElectrico() {
		return nroCliSisElectrico;
	}

	public void setNroCliSisElectrico(long nroCliSisElectrico) {
		this.nroCliSisElectrico = nroCliSisElectrico;
	}

	@Column(name = "SAIFI_SIS_ELECTRICO",nullable=true)
	public BigDecimal getSaifiSisElectrico() {
		return saifiSisElectrico;
	}

	public void setSaifiSisElectrico(BigDecimal saifiSisElectrico) {
		this.saifiSisElectrico = saifiSisElectrico;
	}

	@Column(name = "SAIFI_INT_PROGRAMADAS",nullable=true)
	public BigDecimal getSaifiIntProgramadas() {
		return saifiIntProgramadas;
	}

	public void setSaifiIntProgramadas(BigDecimal saifiIntProgramadas) {
		this.saifiIntProgramadas = saifiIntProgramadas;
	}

	@Column(name = "SAIFI_INT_NO_PROGRAMADAS",nullable=true)
	public BigDecimal getSaifiIntNoProgramadas() {
		return saifiIntNoProgramadas;
	}

	public void setSaifiIntNoProgramadas(BigDecimal saifiIntNoProgramadas) {
		this.saifiIntNoProgramadas = saifiIntNoProgramadas;
	}

	@Column(name = "SAIFI_REC_CARGA",nullable=true)
	public BigDecimal getSaifiRecCarga() {
		return saifiRecCarga;
	}

	public void setSaifiRecCarga(BigDecimal saifiRecCarga) {
		this.saifiRecCarga = saifiRecCarga;
	}

	@Column(name = "SAIFI_INS_DISTRIBUCION",nullable=true)
	public BigDecimal getSaifiInsDistribucion() {
		return saifiInsDistribucion;
	}

	public void setSaifiInsDistribucion(BigDecimal saifiInsDistribucion) {
		this.saifiInsDistribucion = saifiInsDistribucion;
	}

	@Column(name = "SAIFI_INS_TRANSMISION",nullable=true)
	public BigDecimal getSaifiInsTransmision() {
		return saifiInsTransmision;
	}

	public void setSaifiInsTransmision(BigDecimal saifiInsTransmision) {
		this.saifiInsTransmision = saifiInsTransmision;
	}

	@Column(name = "SAIFI_INS_GENERACION",nullable=true)
	public BigDecimal getSaifiInsGeneracion() {
		return saifiInsGeneracion;
	}

	public void setSaifiInsGeneracion(BigDecimal saifiInsGeneracion) {
		this.saifiInsGeneracion = saifiInsGeneracion;
	}

	@Column(name = "SAIFI_CAU_PROPIAS",nullable=true)
	public BigDecimal getSaifiCauPropias() {
		return SaifiCauPropias;
	}

	public void setSaifiCauPropias(BigDecimal saifiCauPropias) {
		SaifiCauPropias = saifiCauPropias;
	}

	@Column(name = "SAIFI_CAU_TERCEROS",nullable=true)
	public BigDecimal getSaifiCauTerceros() {
		return SaifiCauTerceros;
	}

	public void setSaifiCauTerceros(BigDecimal saifiCauTerceros) {
		SaifiCauTerceros = saifiCauTerceros;
	}

	@Column(name = "SAIFI_CAU_OTRAS",nullable=true)
	public BigDecimal getSaifiCauOtras() {
		return SaifiCauOtras;
	}

	public void setSaifiCauOtras(BigDecimal saifiCauOtras) {
		SaifiCauOtras = saifiCauOtras;
	}

	@Column(name = "SAIFI_CAU_FENOMENOS",nullable=true)
	public BigDecimal getSaifiCauFenomenos() {
		return SaifiCauFenomenos;
	}

	public void setSaifiCauFenomenos(BigDecimal saifiCauFenomenos) {
		SaifiCauFenomenos = saifiCauFenomenos;
	}

	@Column(name = "SAIFI_FUE_MAYOR",nullable=true)
	public BigDecimal getSaifiFueMayor() {
		return SaifiFueMayor;
	}

	public void setSaifiFueMayor(BigDecimal saifiFueMayor) {
		SaifiFueMayor = saifiFueMayor;
	}

	@Column(name = "SAIDI_SIS_ELECTRICO",nullable=true)
	public BigDecimal getSaidiSisElectrico() {
		return SaidiSisElectrico;
	}

	public void setSaidiSisElectrico(BigDecimal saidiSisElectrico) {
		SaidiSisElectrico = saidiSisElectrico;
	}

	@Column(name = "SAIDI_INT_PROGRAMADAS",nullable=true)
	public BigDecimal getSaidiIntProgramadas() {
		return SaidiIntProgramadas;
	}

	public void setSaidiIntProgramadas(BigDecimal saidiIntProgramadas) {
		SaidiIntProgramadas = saidiIntProgramadas;
	}

	@Column(name = "SAIDI_INT_NO_PROGRAMADAS",nullable=true)
	public BigDecimal getSaidiIntNoProgramadas() {
		return SaidiIntNoProgramadas;
	}

	public void setSaidiIntNoProgramadas(BigDecimal saidiIntNoProgramadas) {
		SaidiIntNoProgramadas = saidiIntNoProgramadas;
	}

	@Column(name = "SAIDI_REC_CARGA",nullable=true)
	public BigDecimal getSaidiRecCarga() {
		return SaidiRecCarga;
	}

	public void setSaidiRecCarga(BigDecimal saidiRecCarga) {
		SaidiRecCarga = saidiRecCarga;
	}

	@Column(name = "SAIDI_INS_DISTRIBUCION",nullable=true)
	public BigDecimal getSaidiInsDistribucion() {
		return SaidiInsDistribucion;
	}

	public void setSaidiInsDistribucion(BigDecimal saidiInsDistribucion) {
		SaidiInsDistribucion = saidiInsDistribucion;
	}

	@Column(name = "SAIDI_INS_TRANSMISION",nullable=true)
	public BigDecimal getSaidiInsTransmision() {
		return SaidiInsTransmision;
	}

	public void setSaidiInsTransmision(BigDecimal saidiInsTransmision) {
		SaidiInsTransmision = saidiInsTransmision;
	}

	@Column(name = "SAIDI_INS_GENERACION",nullable=true)
	public BigDecimal getSaidiInsGeneracion() {
		return SaidiInsGeneracion;
	}

	public void setSaidiInsGeneracion(BigDecimal saidiInsGeneracion) {
		SaidiInsGeneracion = saidiInsGeneracion;
	}

	@Column(name = "SAIDI_CAU_PROPIAS",nullable=true)
	public BigDecimal getSaidiCauPropias() {
		return SaidiCauPropias;
	}

	public void setSaidiCauPropias(BigDecimal saidiCauPropias) {
		SaidiCauPropias = saidiCauPropias;
	}

	@Column(name = "SAIDI_CAU_TERCEROS",nullable=true)
	public BigDecimal getSaidiCauTerceros() {
		return SaidiCauTerceros;
	}

	public void setSaidiCauTerceros(BigDecimal saidiCauTerceros) {
		SaidiCauTerceros = saidiCauTerceros;
	}

	@Column(name = "SAIDI_CAU_OTRAS",nullable=true)
	public BigDecimal getSaidiCauOtras() {
		return SaidiCauOtras;
	}

	public void setSaidiCauOtras(BigDecimal saidiCauOtras) {
		SaidiCauOtras = saidiCauOtras;
	}

	@Column(name = "SAIDI_CAU_FENOMENOS",nullable=true)
	public BigDecimal getSaidiCauFenomenos() {
		return SaidiCauFenomenos;
	}

	public void setSaidiCauFenomenos(BigDecimal saidiCauFenomenos) {
		SaidiCauFenomenos = saidiCauFenomenos;
	}

	@Column(name = "SAIDI_FUE_MAYOR",nullable=true)
	public BigDecimal getSaidiFueMayor() {
		return SaidiFueMayor;
	}

	public void setSaidiFueMayor(BigDecimal saidiFueMayor) {
		SaidiFueMayor = saidiFueMayor;
	}

	@ManyToOne
	@JoinColumn(name = "PK_PER_MEDICION_ID",nullable=true)
	public PeriodoMedicion getPeriodoMedicion() {
		return periodoMedicion;
	}

	public void setPeriodoMedicion(PeriodoMedicion periodoMedicion) {
		this.periodoMedicion = periodoMedicion;
	}

	@Column(name = "ESTADO",nullable=false)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}


	public String toString(){
		return new Long(this.getCalIndicadorId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getCalIndicadorId()==((Indicador074)obj).getCalIndicadorId()){
			return true;
		}else{
			return false;
		}
	}

	

	

		
}
