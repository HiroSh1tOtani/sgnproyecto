
package pe.com.calidad.suministro.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import pe.com.indra.calidad.entity.Localidad;
/**
 * Suministro Afectado Suministro.
*
* @version 1.0 05 Mar 2014
* @author Luis
 */
@Entity
@Table(name="CAL_SUMINISTRO_AFECTADO")
@SequenceGenerator (name = "SumAfectadoIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "SuministroAfectadoSeq")
public class SuministroAfectado implements Serializable {
    private long sumAfectadoId;
    private String numSumAfectado;
    private String ubiSuministro;
    private String tenSuministro;
    private String codSed;
    private String codSet;
    private Timestamp fecIniIntReal;
    private Timestamp fecFinIntReal;
    //private Set<CompensacionSuministro> compensacionSuministro;
    private Interrupcion interrupcion;
    private String estado;
    private Localidad localidad;
    private String codAlimentador;
    private String opcTarifa;
    private String semestre;
    private BigDecimal eneSemSuministrada;
    private String sincronizar;
 
    private BigDecimal conEneTeorica;
    private BigDecimal potFacturada;
    private BigDecimal eneCompensar;
    private BigDecimal potCompensar;
    private BigDecimal comEnergia;
    private BigDecimal comPotencia;
    
    private String tipServicio;
    private String perteneceSer;
    
    private BigDecimal nhInt;
    private BigDecimal nhs;
    
    public SuministroAfectado(long sumAfectadoId,String numSumAfectado,String ubiSuministro,String tenSuministro,String codSed,String codSet,Timestamp fecIniIntReal,Timestamp fecFinIntReal,Interrupcion interrupcion,String estado, String codAlimentador, String opcTarifa, String semestre, BigDecimal eneSemSuministrada, String sincronizar,BigDecimal conEneTeorica,BigDecimal potFacturada,BigDecimal eneCompensar,BigDecimal potCompensar,BigDecimal comEnergia,BigDecimal comPotencia,String tipServicio,String perteneceSer) {
        
    	this.numSumAfectado = numSumAfectado;
        this.ubiSuministro  = ubiSuministro;
        this.tenSuministro  = tenSuministro;
        this.codSed  = codSed;
        this.codSet  = codSet;
        this.fecIniIntReal = fecIniIntReal;
        this.fecFinIntReal = fecFinIntReal;
        this.estado = estado;
        this.interrupcion = interrupcion;
        this.codAlimentador = codAlimentador;
        this.opcTarifa = opcTarifa;
        this.semestre = semestre;
        this.eneSemSuministrada = eneSemSuministrada;
        this.sincronizar = sincronizar;
       // this.compensacionSuministro  = new HashSet<CompensacionSuministro>();
        this.conEneTeorica = conEneTeorica ;
        this.potFacturada = potFacturada;
        this.eneCompensar = eneCompensar;
        this.potCompensar = potCompensar;
        this.comEnergia = comEnergia;
        this.comPotencia = comPotencia;
        this.tipServicio = tipServicio;
        this.perteneceSer = perteneceSer;
        
    }

    public SuministroAfectado() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SumAfectadoIdGenerator")
    @Column(name = "SUM_AFECTADO_ID",nullable=false,unique=true)
    public long getSumAfectadoId() {
        return sumAfectadoId;
    }

    public void setSumAfectadoId(long sumAfectadoId) {
        this.sumAfectadoId = sumAfectadoId;
    }

    @Column(name="NUM_SUM_AFECTADO",nullable=true)
    public String getNumSumAfectado() {
		return numSumAfectado;
	}

	public void setNumSumAfectado(String numSumAfectado) {
		this.numSumAfectado = numSumAfectado;
	}

	@Column(name="UBI_SUMINISTRO",nullable=true)
	public String getUbiSuministro() {
		return ubiSuministro;
	}

	public void setUbiSuministro(String ubiSuministro) {
		this.ubiSuministro = ubiSuministro;
	}

	@Column(name="TEN_SUMINISTRO",nullable=true)
	public String getTenSuministro() {
		return tenSuministro;
	}

	public void setTenSuministro(String tenSuministro) {
		this.tenSuministro = tenSuministro;
	}

	@Column(name="COD_SED",nullable=true)
	public String getCodSed() {
		return codSed;
	}

	public void setCodSed(String codSed) {
		this.codSed = codSed;
	}

	@Column(name="COD_SET",nullable=true)
	public String getCodSet() {
		return codSet;
	}

	public void setCodSet(String codSet) {
		this.codSet = codSet;
	}

	@Column(name="FEC_INI_INT_REAL",nullable=true)
	public Timestamp getFecIniIntReal() {
		return fecIniIntReal;
	}

	public void setFecIniIntReal(Timestamp fecIniIntReal) {
		this.fecIniIntReal = fecIniIntReal;
	}

	@Column(name="FEC_FIN_INT_REAL",nullable=true)
	public Timestamp getFecFinIntReal() {
		return fecFinIntReal;
	}

	public void setFecFinIntReal(Timestamp fecFinIntReal) {
		this.fecFinIntReal = fecFinIntReal;
	}

	/*@OneToMany(cascade = CascadeType.ALL,mappedBy = "suministroAfectado")
	public Set<CompensacionSuministro> getCompensacionSuministro() {
		return compensacionSuministro;
	}

	public void setCompensacionSuministro(Set<CompensacionSuministro> compensacionSuministro) {
		this.compensacionSuministro = compensacionSuministro;
	}*/

	@ManyToOne
	@JoinColumn(name="PK_INTERRUPCION_ID",nullable=true)
	public Interrupcion getInterrupcion() {
		return interrupcion;
	}

	public void setInterrupcion(Interrupcion interrupcion) {
		this.interrupcion = interrupcion;
	}

	@Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    @ManyToOne
    @JoinColumn(name="PK_LOCALIDAD_ID", nullable=true)
    public Localidad getLocalidad() {
		return localidad;
	}

	public void setLocalidad(Localidad localidad) {
		this.localidad = localidad;
	}
    
	@Column(name="CODALIMENTADOR",nullable=true)
	public String getCodAlimentador() {
		return codAlimentador;
	}

	public void setCodAlimentador(String codAlimentador) {
		this.codAlimentador = codAlimentador;
	}

	@Column(name="OPC_TARIFARIA",nullable=true)
	public String getOpcTarifa() {
		return opcTarifa;
	}

	public void setOpcTarifa(String opcTarifa) {
		this.opcTarifa = opcTarifa;
	}
	
	@Column(name="SEMESTRE",nullable=true)
	public String getSemestre() {
		return semestre;
	}

	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}
	
	@Column(name="ENE_SEM_SUMINISTRO",nullable=true)
	public BigDecimal getEneSemSuministrada() {
		return eneSemSuministrada;
	}

	public void setEneSemSuministrada(BigDecimal eneSemSuministrada) {
		this.eneSemSuministrada = eneSemSuministrada;
	}
	
	@Column(name="SINCRONIZAR",nullable=true)
	public String getSincronizar() {
		return sincronizar;
	}

	public void setSincronizar(String sincronizar) {
		this.sincronizar = sincronizar;
	}
	
	
	@Column(name="CON_ENE_TEORICA",nullable=true)
	public BigDecimal getConEneTeorica() {
		return conEneTeorica;
	}

	public void setConEneTeorica(BigDecimal conEneTeorica) {
		this.conEneTeorica = conEneTeorica;
	}

	@Column(name="POT_FACTURADA",nullable=true)
	public BigDecimal getPotFacturada() {
		return potFacturada;
	}

	public void setPotFacturada(BigDecimal potFacturada) {
		this.potFacturada = potFacturada;
	}

	@Column(name="ENE_COMPENSAR",nullable=true)
	public BigDecimal getEneCompensar() {
		return eneCompensar;
	}

	public void setEneCompensar(BigDecimal eneCompensar) {
		this.eneCompensar = eneCompensar;
	}

	@Column(name="POT_COMPENSAR",nullable=true)
	public BigDecimal getPotCompensar() {
		return potCompensar;
	}

	public void setPotCompensar(BigDecimal potCompensar) {
		this.potCompensar = potCompensar;
	}

	@Column(name="COM_ENERGIA",nullable=true)
	public BigDecimal getComEnergia() {
		return comEnergia;
	}

	public void setComEnergia(BigDecimal comEnergia) {
		this.comEnergia = comEnergia;
	}

	@Column(name="COM_POTENCIA",nullable=true)
	public BigDecimal getComPotencia() {
		return comPotencia;
	}

	public void setComPotencia(BigDecimal comPotencia) {
		this.comPotencia = comPotencia;
	}

	@Column(name="TIP_SERVICIO",nullable=true)
	public String getTipServicio() {
		return tipServicio;
	}

	public void setTipServicio(String tipServicio) {
		this.tipServicio = tipServicio;
	}
	
	@Column(name="PERTENECE_SER",nullable=true)
	public String getPerteneceSer() {
		return perteneceSer;
	}

	public void setPerteneceSer(String perteneceSer) {
		this.perteneceSer = perteneceSer;
	}

	@Column(name="NH_INT",nullable=true)
	public BigDecimal getNhInt() {
		return nhInt;
	}

	public void setNhInt(BigDecimal nhInt) {
		this.nhInt = nhInt;
	}

	@Column(name="NHS",nullable=true)
	public BigDecimal getNhs() {
		return nhs;
	}

	public void setNhs(BigDecimal nhs) {
		this.nhs = nhs;
	}

	public String toString(){
		return new Long(this.getSumAfectadoId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getSumAfectadoId()==((SuministroAfectado)obj).getSumAfectadoId()){
			return true;
		}else{
			return false;
		}
	}

		
}
