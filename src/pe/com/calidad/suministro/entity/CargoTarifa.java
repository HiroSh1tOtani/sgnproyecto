
package pe.com.calidad.suministro.entity;

import java.io.Serializable;
import java.math.BigDecimal;


import javax.persistence.*;




/**
* Cargo tarifa
*
* @version 1.0 05 
* @author Luis
*/



@Entity
@Table(name="CAL_CARGO_TARIFA")
@SequenceGenerator (name = "CarTarifaIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "CargoTarifaSeq")
public class CargoTarifa implements Serializable{
	
    private long carTarifaId;
    private String codTarifa;
    private String secTipico;
    private BigDecimal carEneFuePunta;
    private BigDecimal carEnePunta;
    private BigDecimal carPotFuePunta;
    private BigDecimal carPotPunta;
    private String estado;
    private Pliego pliego;

	public CargoTarifa() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CarTarifaIdGenerator")
    @Column(name = "CAR_TARIFA_ID",nullable=false,unique=true)
	public long getCarTarifaId() {
		return carTarifaId;
	}

	public void setCarTarifaId(long carTarifaId) {
		this.carTarifaId = carTarifaId;
	}

	@Column(name = "COD_TARIFA",nullable=true)
	public String getCodTarifa() {
		return codTarifa;
	}

	public void setCodTarifa(String codTarifa) {
		this.codTarifa = codTarifa;
	}

	@Column(name = "SEC_TIPICO",nullable=true)
	public String getSecTipico() {
		return secTipico;
	}

	public void setSecTipico(String secTipico) {
		this.secTipico = secTipico;
	}

	@Column(name = "CAR_ENE_FUE_PUNTA",nullable=true)
	public BigDecimal getCarEneFuePunta() {
		return carEneFuePunta;
	}

	public void setCarEneFuePunta(BigDecimal carEneFuePunta) {
		this.carEneFuePunta = carEneFuePunta;
	}

	@Column(name = "CAR_ENE_PUNTA",nullable=true)
	public BigDecimal getCarEnePunta() {
		return carEnePunta;
	}

	public void setCarEnePunta(BigDecimal carEnePunta) {
		this.carEnePunta = carEnePunta;
	}

	@Column(name = "CAR_POT_FUE_PUNTA",nullable=true)
	public BigDecimal getCarPotFuePunta() {
		return carPotFuePunta;
	}

	public void setCarPotFuePunta(BigDecimal carPotFuePunta) {
		this.carPotFuePunta = carPotFuePunta;
	}

	@Column(name = "CAR_POT_PUNTA",nullable=true)
	public BigDecimal getCarPotPunta() {
		return carPotPunta;
	}

	public void setCarPotPunta(BigDecimal carPotPunta) {
		this.carPotPunta = carPotPunta;
	}

	@ManyToOne
	@JoinColumn(name = "PK_PLIEGO_ID",nullable=true)
	public Pliego getPliego() {
		return pliego;
	}

	public void setPliego(Pliego pliego) {
		this.pliego = pliego;
	}

	@Column(name = "ESTADO",nullable=true)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	 	
}
