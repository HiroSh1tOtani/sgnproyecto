
package pe.com.calidad.suministro.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import java.math.BigDecimal;

import javax.persistence.*;



/**
* Parametro Anual
*
* @version 1.0 26 dic 2014
* @author Luis
 */
@Entity
@Table(name="CAL_PARAMETRO_ANUAL")
@SequenceGenerator (name = "ParAnualIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "ParametroAnualSeq")
public class ParametroAnual implements Serializable {
    
	private long parAnualId;
 	private String ano; 
	private BigDecimal nhubt; 
	private BigDecimal cosRascionamiento;
	private String estado;
	
    
    
	public ParametroAnual() {
    }
    
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ParAnualIdGenerator")
    @Column(name = "PAR_ANUAL_ID",nullable=false,unique=true)
	public long getParAnualId() {
		return parAnualId;
	}

	public void setParAnualId(long parAnualId) {
		this.parAnualId = parAnualId;
	}

	@Column(name = "ANO",nullable=true)
	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	@Column(name = "NHUBT",nullable=true)
	public BigDecimal getNhubt() {
		return nhubt;
	}

	public void setNhubt(BigDecimal nhubt) {
		this.nhubt = nhubt;
	}

	@Column(name = "COS_RASCIONAMIENTO",nullable=true)
	public BigDecimal getCosRascionamiento() {
		return cosRascionamiento;
	}

	public void setCosRascionamiento(BigDecimal cosRascionamiento) {
		this.cosRascionamiento = cosRascionamiento;
	}


	public String toString(){
		return new Long(this.getParAnualId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getParAnualId()==((ParametroAnual)obj).getParAnualId()){
			return true;
		}else{
			return false;
		}
	}

	@Column(name = "ESTADO",nullable=true)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

			
}
