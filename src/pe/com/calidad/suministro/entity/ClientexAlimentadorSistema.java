package pe.com.calidad.suministro.entity;

import java.math.BigDecimal;

public class ClientexAlimentadorSistema {

	private BigDecimal cliexAlimentadorId;
	private String alimentador; 
 	private BigDecimal sumCantidad;
 	private String sistemaElectrico;
 	private BigDecimal sector;
 	
	public ClientexAlimentadorSistema(BigDecimal cliexAlimentadorId, String alimentador, BigDecimal sumCantidad,
			String sistemaElectrico, BigDecimal sector) {
		super();
		this.cliexAlimentadorId = cliexAlimentadorId;
		this.alimentador = alimentador;
		this.sumCantidad = sumCantidad;
		this.sistemaElectrico = sistemaElectrico;
		this.sector = sector;
	}
	
	public BigDecimal getCliexAlimentadorId() {
		return cliexAlimentadorId;
	}
	public void setCliexAlimentadorId(BigDecimal cliexAlimentadorId) {
		this.cliexAlimentadorId = cliexAlimentadorId;
	}
	public String getAlimentador() {
		return alimentador;
	}
	public void setAlimentador(String alimentador) {
		this.alimentador = alimentador;
	}
	public BigDecimal getSumCantidad() {
		return sumCantidad;
	}
	public void setSumCantidad(BigDecimal sumCantidad) {
		this.sumCantidad = sumCantidad;
	}
	public String getSistemaElectrico() {
		return sistemaElectrico;
	}
	public void setSistemaElectrico(String sistemaElectrico) {
		this.sistemaElectrico = sistemaElectrico;
	}
	public BigDecimal getSector() {
		return sector;
	}
	public void setSector(BigDecimal sector) {
		this.sector = sector;
	}
	
}
