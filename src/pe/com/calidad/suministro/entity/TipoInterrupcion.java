
package pe.com.calidad.suministro.entity;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
* Tipo Interrupcion de Suministro.
*
* @version 1.0 05 Mar 2014
* @author Lucho
*/
@Entity
@Table(name="CAL_TIPO_INTERRUPCION")
@SequenceGenerator(name = "TipInterrupcionIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "TipoInterrupcionSeq")
public class TipoInterrupcion implements Serializable{
    private long tipInterrupcionId;
    private String codTipInterrupcion;
    private String descripcion;
    private String estado;
    
    private Set<Interrupcion> interrupciones;
    

    public TipoInterrupcion(String codTipInterrupcion, String descripcion, String estado) {
        this.codTipInterrupcion = codTipInterrupcion;
        this.descripcion = descripcion;
        this.estado = estado;
        this.interrupciones = new HashSet<Interrupcion>();
        
    }

    public TipoInterrupcion() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TipInterrupcionIdGenerator")
    @Column(name = "TIP_INTERRUPCION_ID",nullable=false,unique=true)
    public long getTipInterrupcionId() {
        return tipInterrupcionId;
    }

    public void setTipInterrupcionId(long tipInterrupcionId) {
        this.tipInterrupcionId = tipInterrupcionId;
    }
    
    @Column(name="COD_TIP_INTERRUPCION",nullable=true)
    public String getCodTipInterrupcion() {
        return codTipInterrupcion;
    }

    public void setCodTipInterrupcion(String codTipInterrupcion) {
        this.codTipInterrupcion = codTipInterrupcion;
    }
    
    @Column(name="DESCRIPCION",nullable=true)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "tipoInterrupcion")
    public Set<Interrupcion> getInterrupciones() {
        return interrupciones;
    }

    public void setInterrupciones(Set<Interrupcion> interrupciones) {
        this.interrupciones = interrupciones;
    }

   	public String toString(){
		return new Long(this.getTipInterrupcionId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getTipInterrupcionId()==((TipoInterrupcion)obj).getTipInterrupcionId()){
			return true;
		}else{
			return false;
		}
	}
}
