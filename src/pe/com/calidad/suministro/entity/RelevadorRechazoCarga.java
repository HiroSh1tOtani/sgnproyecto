
package pe.com.calidad.suministro.entity;

import java.io.Serializable;
import java.math.BigDecimal;


import javax.persistence.*;


/**

* 
* @author Luis
*/

@Entity
@Table(name="CAL_RELEVADOR_RECHAZO_CARGA")
@SequenceGenerator (name = "RelRechazoIdIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "RelevadorRechazoCargaSeq")
public class RelevadorRechazoCarga implements Serializable{
   
    private long relRechazoId;
    private String ano;
    private String semestre;
    private String codRelevador;
    private String codAlimentador;
    private BigDecimal montoRepartir;
    private BigDecimal eneNoSuministrada;
    private BigDecimal nroIntRecCarga;
    private BigDecimal nroHorIntRecCarga;
    private String estado;


    
  	public RelevadorRechazoCarga() {
    }


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RelRechazoIdIdGenerator")
    @Column(name = "REL_RECHAZO_ID",nullable=false,unique=true)
    public long getRelRechazoId() {
		return relRechazoId;
	}


	public void setRelRechazoId(long relRechazoId) {
		this.relRechazoId = relRechazoId;
	}

	@Column(name = "ANO",nullable=true)
	public String getAno() {
		return ano;
	}


	public void setAno(String ano) {
		this.ano = ano;
	}


	@Column(name = "SEMESTRE",nullable=true)
	public String getSemestre() {
		return semestre;
	}


	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}

	@Column(name = "COD_RELEVADOR",nullable=true)
	public String getCodRelevador() {
		return codRelevador;
	}


	public void setCodRelevador(String codRelevador) {
		this.codRelevador = codRelevador;
	}

	@Column(name = "COD_ALIMENTADOR",nullable=true)
	public String getCodAlimentador() {
		return codAlimentador;
	}


	public void setCodAlimentador(String codAlimentador) {
		this.codAlimentador = codAlimentador;
	}

	@Column(name = "MONTO_REPARTIR",nullable=true)
	public BigDecimal getMontoRepartir() {
		return montoRepartir;
	}


	public void setMontoRepartir(BigDecimal montoRepartir) {
		this.montoRepartir = montoRepartir;
	}

	@Column(name = "ENE_NO_SUMINISTRADA",nullable=true)
	public BigDecimal getEneNoSuministrada() {
		return eneNoSuministrada;
	}


	public void setEneNoSuministrada(BigDecimal eneNoSuministrada) {
		this.eneNoSuministrada = eneNoSuministrada;
	}

	@Column(name = "NRO_INT_REC_CARGA",nullable=true)
	public BigDecimal getNroIntRecCarga() {
		return nroIntRecCarga;
	}


	public void setNroIntRecCarga(BigDecimal nroIntRecCarga) {
		this.nroIntRecCarga = nroIntRecCarga;
	}

	@Column(name = "NRO_HOR_INT_REC_CARGA",nullable=true)
	public BigDecimal getNroHorIntRecCarga() {
		return nroHorIntRecCarga;
	}


	public void setNroHorIntRecCarga(BigDecimal nroHorIntRecCarga) {
		this.nroHorIntRecCarga = nroHorIntRecCarga;
	}
	 	
	@Column(name = "ESTADO",nullable=true)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

}
