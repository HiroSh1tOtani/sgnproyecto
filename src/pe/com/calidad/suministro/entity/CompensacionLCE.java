
package pe.com.calidad.suministro.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.*;


/**

* 
* @author Luis
*/

@Entity
@Table(name="CAL_COMPENSACION_LCE")
@SequenceGenerator (name = "CompensacionLCDIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "CompensacionLCESeq")
public class CompensacionLCE implements Serializable{
   
    private long comLceId;
    private String codInterrupcion;
    private BigDecimal duracion;
    private BigDecimal duracionHp;
    private BigDecimal duracionHfp;
    private BigDecimal horasMes;
    private String suministro;
    private String ano;
    private String mes;
    private BigDecimal cosRacionamiento;
    private String tarifa;
    private BigDecimal potDistribucion;
    private BigDecimal potDisCargo;
    private BigDecimal potDisCompensar;
    private BigDecimal comPotDistribucion;
    private BigDecimal potDisHfp;
    private BigDecimal potDisHfpCargo;
    private BigDecimal potDisHfpCompensar;
    private BigDecimal comPotDisHfp;
    private BigDecimal potDisHp;
    private BigDecimal potDisHpCargo;
    private BigDecimal potDisHpCompensar;
    private BigDecimal comPotDisHp;
    private BigDecimal potGeneracion;
    private BigDecimal potGenCargo;
    private BigDecimal potGenCompensar;
    private BigDecimal comPotGeneracion;
    private BigDecimal eneActiva;
    private BigDecimal eneActCargo;
    private BigDecimal eneActCompensar;
    private BigDecimal comEneActiva;
    private BigDecimal eneActHp;
    private BigDecimal eneActHpCargo;
    private BigDecimal eneActHpCompensar;
    private BigDecimal comEneActHp;
    private BigDecimal eneActHfp;
    private BigDecimal eneActHfpCargo;
    private BigDecimal eneActHfpCompensar;
    private BigDecimal comEneActHfp;
    private BigDecimal comTotal;
    private String semestre;
    private Timestamp fecIniInterrupcion;
    private Timestamp fecFinInterrupcion;
    private BigDecimal tipCambio;
    private BigDecimal comTotDolares;

    
    
  	public CompensacionLCE() {
    }


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CompensacionLCDIdGenerator")
    @Column(name = "COM_LCE_ID",nullable=false,unique=true)
    public long getComLceId() {
		return comLceId;
	}

	public void setComLceId(long comLceId) {
		this.comLceId = comLceId;
	}

	@Column(name="COD_INTERRUPCION",nullable=true)
	public String getCodInterrupcion() {
		return codInterrupcion;
	}

	
	public void setCodInterrupcion(String codInterrupcion) {
		this.codInterrupcion = codInterrupcion;
	}

	@Column(name="DURACION",nullable=true)
	public BigDecimal getDuracion() {
		return duracion;
	}


	public void setDuracion(BigDecimal duracion) {
		this.duracion = duracion;
	}

	@Column(name="DURACION_HP",nullable=true)
	public BigDecimal getDuracionHp() {
		return duracionHp;
	}


	public void setDuracionHp(BigDecimal duracionHp) {
		this.duracionHp = duracionHp;
	}

	@Column(name="DURACION_HFP",nullable=true)
	public BigDecimal getDuracionHfp() {
		return duracionHfp;
	}


	public void setDuracionHfp(BigDecimal duracionHfp) {
		this.duracionHfp = duracionHfp;
	}

	@Column(name="HORAS_MES",nullable=true)
	public BigDecimal getHorasMes() {
		return horasMes;
	}


	public void setHorasMes(BigDecimal horasMes) {
		this.horasMes = horasMes;
	}

	@Column(name="SUMINISTRO",nullable=true)
	public String getSuministro() {
		return suministro;
	}


	public void setSuministro(String suministro) {
		this.suministro = suministro;
	}

	@Column(name="ANO",nullable=true)
	public String getAno() {
		return ano;
	}


	public void setAno(String ano) {
		this.ano = ano;
	}

	@Column(name="MES",nullable=true)
	public String getMes() {
		return mes;
	}


	public void setMes(String mes) {
		this.mes = mes;
	}

	@Column(name="COS_RACIONAMIENTO",nullable=true)
	public BigDecimal getCosRacionamiento() {
		return cosRacionamiento;
	}


	public void setCosRacionamiento(BigDecimal cosRacionamiento) {
		this.cosRacionamiento = cosRacionamiento;
	}

	@Column(name="TARIFA",nullable=true)
	public String getTarifa() {
		return tarifa;
	}


	public void setTarifa(String tarifa) {
		this.tarifa = tarifa;
	}

	@Column(name="POT_DISTRIBUCION",nullable=true)
	public BigDecimal getPotDistribucion() {
		return potDistribucion;
	}


	public void setPotDistribucion(BigDecimal potDistribucion) {
		this.potDistribucion = potDistribucion;
	}

	@Column(name="POT_DIS_CARGO",nullable=true)
	public BigDecimal getPotDisCargo() {
		return potDisCargo;
	}


	public void setPotDisCargo(BigDecimal potDisCargo) {
		this.potDisCargo = potDisCargo;
	}

	@Column(name="POT_DIS_COMPENSAR",nullable=true)
	public BigDecimal getPotDisCompensar() {
		return potDisCompensar;
	}


	public void setPotDisCompensar(BigDecimal potDisCompensar) {
		this.potDisCompensar = potDisCompensar;
	}

	@Column(name="COM_POT_DISTRIBUCION",nullable=true)
	public BigDecimal getComPotDistribucion() {
		return comPotDistribucion;
	}


	public void setComPotDistribucion(BigDecimal comPotDistribucion) {
		this.comPotDistribucion = comPotDistribucion;
	}

	@Column(name="POT_DIS_HFP",nullable=true)
	public BigDecimal getPotDisHfp() {
		return potDisHfp;
	}


	public void setPotDisHfp(BigDecimal potDisHfp) {
		this.potDisHfp = potDisHfp;
	}

	@Column(name="POT_DIS_HFP_CARGO",nullable=true)
	public BigDecimal getPotDisHfpCargo() {
		return potDisHfpCargo;
	}


	public void setPotDisHfpCargo(BigDecimal potDisHfpCargo) {
		this.potDisHfpCargo = potDisHfpCargo;
	}

	@Column(name="POT_DIS_HFP_COMPENSAR",nullable=true)
	public BigDecimal getPotDisHfpCompensar() {
		return potDisHfpCompensar;
	}


	public void setPotDisHfpCompensar(BigDecimal potDisHfpCompensar) {
		this.potDisHfpCompensar = potDisHfpCompensar;
	}

	@Column(name="COM_POT_DIS_HFP",nullable=true)
	public BigDecimal getComPotDisHfp() {
		return comPotDisHfp;
	}


	public void setComPotDisHfp(BigDecimal comPotDisHfp) {
		this.comPotDisHfp = comPotDisHfp;
	}

	@Column(name="POT_DIS_HP",nullable=true)
	public BigDecimal getPotDisHp() {
		return potDisHp;
	}


	public void setPotDisHp(BigDecimal potDisHp) {
		this.potDisHp = potDisHp;
	}

	@Column(name="POT_DIS_HP_CARGO",nullable=true)
	public BigDecimal getPotDisHpCargo() {
		return potDisHpCargo;
	}


	public void setPotDisHpCargo(BigDecimal potDisHpCargo) {
		this.potDisHpCargo = potDisHpCargo;
	}

	@Column(name="POT_DIS_HP_COMPENSAR",nullable=true)
	public BigDecimal getPotDisHpCompensar() {
		return potDisHpCompensar;
	}


	public void setPotDisHpCompensar(BigDecimal potDisHpCompensar) {
		this.potDisHpCompensar = potDisHpCompensar;
	}

	@Column(name="COM_POT_DIS_HP",nullable=true)
	public BigDecimal getComPotDisHp() {
		return comPotDisHp;
	}


	public void setComPotDisHp(BigDecimal comPotDisHp) {
		this.comPotDisHp = comPotDisHp;
	}

	@Column(name="POT_GENERACION",nullable=true)
	public BigDecimal getPotGeneracion() {
		return potGeneracion;
	}


	public void setPotGeneracion(BigDecimal potGeneracion) {
		this.potGeneracion = potGeneracion;
	}

	@Column(name="POT_GEN_CARGO",nullable=true)
	public BigDecimal getPotGenCargo() {
		return potGenCargo;
	}


	public void setPotGenCargo(BigDecimal potGenCargo) {
		this.potGenCargo = potGenCargo;
	}

	@Column(name="POT_GEN_COMPENSAR",nullable=true)
	public BigDecimal getPotGenCompensar() {
		return potGenCompensar;
	}


	public void setPotGenCompensar(BigDecimal potGenCompensar) {
		this.potGenCompensar = potGenCompensar;
	}
	
	@Column(name="COM_POT_GENERACION",nullable=true)
	public BigDecimal getComPotGeneracion() {
		return comPotGeneracion;
	}

	public void setComPotGeneracion(BigDecimal comPotGeneracion) {
		this.comPotGeneracion = comPotGeneracion;
	}

	@Column(name="ENE_ACTIVA",nullable=true)
	public BigDecimal getEneActiva() {
		return eneActiva;
	}


	public void setEneActiva(BigDecimal eneActiva) {
		this.eneActiva = eneActiva;
	}

	@Column(name="ENE_ACT_CARGO",nullable=true)
	public BigDecimal getEneActCargo() {
		return eneActCargo;
	}


	public void setEneActCargo(BigDecimal eneActCargo) {
		this.eneActCargo = eneActCargo;
	}

	@Column(name="ENE_ACT_COMPENSAR",nullable=true)
	public BigDecimal getEneActCompensar() {
		return eneActCompensar;
	}


	public void setEneActCompensar(BigDecimal eneActCompensar) {
		this.eneActCompensar = eneActCompensar;
	}

	@Column(name="COM_ENE_ACTIVA",nullable=true)
	public BigDecimal getComEneActiva() {
		return comEneActiva;
	}


	public void setComEneActiva(BigDecimal comEneActiva) {
		this.comEneActiva = comEneActiva;
	}

	@Column(name="ENE_ACT_HP",nullable=true)
	public BigDecimal getEneActHp() {
		return eneActHp;
	}


	public void setEneActHp(BigDecimal eneActHp) {
		this.eneActHp = eneActHp;
	}

	@Column(name="ENE_ACT_HP_CARGO",nullable=true)
	public BigDecimal getEneActHpCargo() {
		return eneActHpCargo;
	}


	public void setEneActHpCargo(BigDecimal eneActHpCargo) {
		this.eneActHpCargo = eneActHpCargo;
	}

	@Column(name="ENE_ACT_HP_COMPENSAR",nullable=true)
	public BigDecimal getEneActHpCompensar() {
		return eneActHpCompensar;
	}


	public void setEneActHpCompensar(BigDecimal eneActHpCompensar) {
		this.eneActHpCompensar = eneActHpCompensar;
	}

	@Column(name="COM_ENE_ACT_HP",nullable=true)
	public BigDecimal getComEneActHp() {
		return comEneActHp;
	}


	public void setComEneActHp(BigDecimal comEneActHp) {
		this.comEneActHp = comEneActHp;
	}

	@Column(name="ENE_ACT_HFP",nullable=true)
	public BigDecimal getEneActHfp() {
		return eneActHfp;
	}


	public void setEneActHfp(BigDecimal eneActHfp) {
		this.eneActHfp = eneActHfp;
	}

	@Column(name="ENE_ACT_HFP_CARGO",nullable=true)
	public BigDecimal getEneActHfpCargo() {
		return eneActHfpCargo;
	}


	public void setEneActHfpCargo(BigDecimal eneActHfpCargo) {
		this.eneActHfpCargo = eneActHfpCargo;
	}

	@Column(name="ENE_ACT_HFP_COMPENSAR",nullable=true)
	public BigDecimal getEneActHfpCompensar() {
		return eneActHfpCompensar;
	}


	public void setEneActHfpCompensar(BigDecimal eneActHfpCompensar) {
		this.eneActHfpCompensar = eneActHfpCompensar;
	}

	@Column(name="COM_ENE_ACT_HFP",nullable=true)
	public BigDecimal getComEneActHfp() {
		return comEneActHfp;
	}


	public void setComEneActHfp(BigDecimal comEneActHfp) {
		this.comEneActHfp = comEneActHfp;
	}

	@Column(name="COM_TOTAL",nullable=true)
	public BigDecimal getComTotal() {
		return comTotal;
	}


	public void setComTotal(BigDecimal comTotal) {
		this.comTotal = comTotal;
	}

	@Column(name="SEMESTRE",nullable=true)
	public String getSemestre() {
		return semestre;
	}


	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}

	@Column(name="FEC_INI_INTERRUPCION",nullable=true)
	public Timestamp getFecIniInterrupcion() {
		return fecIniInterrupcion;
	}


	public void setFecIniInterrupcion(Timestamp fecIniInterrupcion) {
		this.fecIniInterrupcion = fecIniInterrupcion;
	}

	@Column(name="FEC_FIN_INTERRUPCION",nullable=true)
	public Timestamp getFecFinInterrupcion() {
		return fecFinInterrupcion;
	}


	public void setFecFinInterrupcion(Timestamp fecFinInterrupcion) {
		this.fecFinInterrupcion = fecFinInterrupcion;
	}

	@Column(name="TIP_CAMBIO",nullable=true)
	public BigDecimal getTipCambio() {
		return tipCambio;
	}


	public void setTipCambio(BigDecimal tipCambio) {
		this.tipCambio = tipCambio;
	}

	@Column(name="COM_TOT_DOLARES",nullable=true)
	public BigDecimal getComTotDolares() {
		return comTotDolares;
	}


	public void setComTotDolares(BigDecimal comTotDolares) {
		this.comTotDolares = comTotDolares;
	}

	public String toString(){
		return new Long(this.getComLceId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getComLceId()==((CompensacionLCE)obj).getComLceId()){
			return true;
		}else{
			return false;
		}
	}

	
}
