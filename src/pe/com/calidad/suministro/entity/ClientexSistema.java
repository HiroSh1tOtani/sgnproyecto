
package pe.com.calidad.suministro.entity;

import java.io.Serializable;

import javax.persistence.*;

import pe.com.indra.calidad.entity.PeriodoMedicion;


/**
 * Indicador 074
*
* @version 1.0 26 jun 2014
* @author Luis
 */
@Entity
@Table(name="CAL_CLIENTEXSISTEMA")
@SequenceGenerator (name = "CliexSistemaIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "ClientexSistemaSeq")
public class ClientexSistema implements Serializable {
    
	private static final long serialVersionUID = 1L;
	
	private long cliexSistemaId;
 	private String sisElectrico; 
 	private Long sumCantidad;
 	private Long sumCantidadSinSer;
 	private Long sumCantidadSinSerMt;
 	private Long sumCantidadSinSerBt;
 		
    private PeriodoMedicion periodoMedicion;
    
    
	public ClientexSistema() {
    }
    
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CliexSistemaIdGenerator")
    @Column(name = "CLIXSISTEMA_ID",nullable=false,unique=true)
	public long getCliexSistemaId() {
		return cliexSistemaId;
	}

	public void setCliexSistemaId(long cliexSistemaId) {
		this.cliexSistemaId = cliexSistemaId;
	}

	@Column(name = "SIS_ELECTRICO",nullable=true)
	public String getSisElectrico() {
		return sisElectrico;
	}

	public void setSisElectrico(String sisElectrico) {
		this.sisElectrico = sisElectrico;
	}

	@Column(name = "SUM_CANTIDAD",nullable=true)
	public Long getSumCantidad() {
		return sumCantidad;
	}

	public void setSumCantidad(Long sumCantidad) {
		this.sumCantidad = sumCantidad;
	}


	@ManyToOne
	@JoinColumn(name = "PK_PER_MEDICION_ID",nullable=true)
	public PeriodoMedicion getPeriodoMedicion() {
		return periodoMedicion;
	}

	public void setPeriodoMedicion(PeriodoMedicion periodoMedicion) {
		this.periodoMedicion = periodoMedicion;
	}


	@Column(name = "SUM_CANTIDAD_SIN_SER",nullable=true)
	public Long getSumCantidadSinSer() {
		return sumCantidadSinSer;
	}

	public void setSumCantidadSinSer(Long sumCantidadSinSer) {
		this.sumCantidadSinSer = sumCantidadSinSer;
	}

	@Column(name = "SUM_CANTIDAD_SIN_SER_MT",nullable=true)
	public Long getSumCantidadSinSerMt() {
		return sumCantidadSinSerMt;
	}

	public void setSumCantidadSinSerMt(Long sumCantidadSinSerMt) {
		this.sumCantidadSinSerMt = sumCantidadSinSerMt;
	}

	@Column(name = "SUM_CANTIDAD_SIN_SER_BT",nullable=true)
	public Long getSumCantidadSinSerBt() {
		return sumCantidadSinSerBt;
	}

	public void setSumCantidadSinSerBt(Long sumCantidadSinSerBt) {
		this.sumCantidadSinSerBt = sumCantidadSinSerBt;
	}

	public String toString(){
		return new Long(this.getCliexSistemaId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getCliexSistemaId()==((ClientexSistema)obj).getCliexSistemaId()){
			return true;
		}else{
			return false;
		}
	}
		
}
