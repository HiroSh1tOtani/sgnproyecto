
package pe.com.calidad.suministro.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;


/**
* Compensacion Suministro Suministro.
*
* @version 1.0 05 Mar 2014
* @author Luis
*/

@Entity
@Table(name="CAL_COMPENSACION_SUMINISTRO")
@SequenceGenerator (name = "ComSuministroIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "CompensacionSuministroSeq")
public class CompensacionSuministro implements Serializable{
    private long codSuministroId;
    private String codRelevador;
    private long numIntNoProgramadas;
    private long numIntProMantenimiento;
    private long numIntProExpReforzamiento;
    private BigDecimal durIntNoProgramadas;
    private BigDecimal durIntProMantenimiento;
    private BigDecimal durIntProExpReforzamiento;
    private BigDecimal eneRegSemestre;
    private BigDecimal monComIntPtoEntrega;
    private BigDecimal monComLeyConcesiones;
    private String estado;
    private String numSumAfectado;
    private String semestre;
    private BigDecimal indicadorN;
    private BigDecimal indicadorD;
    private String ano;
    private BigDecimal monComRecCarga;
    private long numIntRecCarga;
    private BigDecimal durIntRecCarga;
    private String tipCompensacion;
    private String codAlimentador;
    private String sisElectrico;
    private BigDecimal nicIntNoProgramadas;
    private BigDecimal nicIntProMantenimiento;
    private BigDecimal nicIntProExpReforzamiento;
    private BigDecimal dicIntNoProgramadas;
    private BigDecimal dicIntProMantenimiento;
    private BigDecimal dicIntProExpReforzamiento; 
    private String nivelTension;
    private String ubiSuministro;
    private String codLocalidad;
    private BigDecimal afectados;
    private String codSed;
    private String compensado;

	public CompensacionSuministro (long codSuministroId,String codRelevador,long numIntNoProgramadas,long numIntProMantenimiento,long numIntProExpReforzamiento,
    		BigDecimal durIntNoProgramadas, BigDecimal durIntProExpReforzamiento, BigDecimal durIntProMantenimiento, BigDecimal eneRegSemestre,
    		BigDecimal monComIntPtoEntrega, BigDecimal monComLeyConcesiones,
    		String estado, String numSumAfectado, String semestre, BigDecimal indicadorN, BigDecimal indicadorD, String ano, BigDecimal monComRecCarga, long numIntRecCarga,
    		BigDecimal durIntRecCarga, String tipCompensacion, String codAlimentador, String sisElectrico, BigDecimal nicIntNoProgramadas, 
    		BigDecimal nicIntProMantenimiento, BigDecimal nicIntProExpReforzamiento, BigDecimal dicIntNoProgramadas, BigDecimal dicIntProMantenimiento,
    		BigDecimal dicIntProExpReforzamiento, String nivelTension,String ubiSuministro,String codLocalidad,BigDecimal afectados,String codSed,String compensado) {
    	
    	this.codSuministroId = codSuministroId;
    	this.codRelevador = codRelevador;
    	this.numIntNoProgramadas = numIntNoProgramadas ;
    	this.numIntProMantenimiento = numIntProMantenimiento;
    	this.numIntProExpReforzamiento = numIntProExpReforzamiento;
    	this.durIntNoProgramadas = durIntNoProgramadas;
    	this.durIntProMantenimiento = durIntProMantenimiento;
    	this.durIntProExpReforzamiento = durIntProExpReforzamiento;
    	this.eneRegSemestre = eneRegSemestre;
    	this.monComIntPtoEntrega = monComIntPtoEntrega;
    	this.monComLeyConcesiones = monComLeyConcesiones;
    	this.estado = estado;
    	this.numSumAfectado = numSumAfectado;
    	this.semestre = semestre;
    	this.indicadorN = indicadorN;
    	this.indicadorD = indicadorD;
    	this.ano = ano;
    	this.monComRecCarga = monComRecCarga;
    	this.numIntRecCarga = numIntRecCarga;
    	this.durIntRecCarga = durIntRecCarga;
    	this.tipCompensacion = tipCompensacion;
    	this.codAlimentador = codAlimentador;
    	this.sisElectrico = sisElectrico;
    	this.nicIntNoProgramadas = nicIntNoProgramadas;
    	this.nicIntProMantenimiento = nicIntProMantenimiento;
    	this.nicIntProExpReforzamiento = nicIntProExpReforzamiento;
    	this.dicIntNoProgramadas = dicIntNoProgramadas;
    	this.dicIntProMantenimiento = dicIntProMantenimiento;
    	this.dicIntProExpReforzamiento = dicIntProExpReforzamiento;
    	this.nivelTension = nivelTension;
    	this.ubiSuministro = ubiSuministro;
    	this.codLocalidad = codLocalidad;
    	this.afectados = afectados;
    	this.codSed = codSed;
    	this.compensado = compensado;
      
    }
 
  	public CompensacionSuministro() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ComSuministroIdGenerator")
    @Column(name = "COM_SUMINISTRO_ID",nullable=false,unique=true)
    public long getCodSuministroId() {
        return codSuministroId;
    }

    public void setCodSuministroId(long codSuministroId) {
        this.codSuministroId = codSuministroId;
    }

    @Column(name = "COD_RELEVADOR",nullable=true)
    public String getCodRelevador() {
		return codRelevador;
	}

	public void setCodRelevador(String codRelevador) {
		this.codRelevador = codRelevador;
	}
   
    
	@Column(name="NUM_INT_NO_PROGRAMADAS",nullable=true)
    public long getNumIntNoProgramadas() {
		return numIntNoProgramadas;
	}

	public void setNumIntNoProgramadas(long numIntNoProgramadas) {
		this.numIntNoProgramadas = numIntNoProgramadas;
	}

	@Column(name = "NUM_INT_PRO_MANTENIMIENTO",nullable=true)
	public long getNumIntProMantenimiento() {
		return numIntProMantenimiento;
	}

	public void setNumIntProMantenimiento(long numIntProMantenimiento) {
		this.numIntProMantenimiento = numIntProMantenimiento;
	}

	@Column(name = "NUM_INT_PRO_EXP_REFORZAMIENTO",nullable=true)
	public long getNumIntProExpReforzamiento() {
		return numIntProExpReforzamiento;
	}

	public void setNumIntProExpReforzamiento(long numIntProExpReforzamiento) {
		this.numIntProExpReforzamiento = numIntProExpReforzamiento;
	}

	@Column(name = "DUR_INT_NO_PROGRAMADAS",nullable=true)
	public BigDecimal getDurIntNoProgramadas() {
		return durIntNoProgramadas;
	}

	public void setDurIntNoProgramadas(BigDecimal durIntNoProgramadas) {
		this.durIntNoProgramadas = durIntNoProgramadas;
	}

	@Column(name = "DUR_INT_PRO_MANTENIMIENTO",nullable=true)
	public BigDecimal getDurIntProMantenimiento() {
		return durIntProMantenimiento;
	}

	public void setDurIntProMantenimiento(BigDecimal durIntProMantenimiento) {
		this.durIntProMantenimiento = durIntProMantenimiento;
	}

	@Column(name = "DUR_INT_PRO_EXP_REFORZAMIENTO",nullable=true)
	public BigDecimal getDurIntProExpReforzamiento() {
		return durIntProExpReforzamiento;
	}

	public void setDurIntProExpReforzamiento(BigDecimal durIntProExpReforzamiento) {
		this.durIntProExpReforzamiento = durIntProExpReforzamiento;
	}

	@Column(name = "ENE_REG_SEMESTRE",nullable=true)
	public BigDecimal getEneRegSemestre() {
		return eneRegSemestre;
	}

	public void setEneRegSemestre(BigDecimal eneRegSemestre) {
		this.eneRegSemestre = eneRegSemestre;
	}

	@Column(name = "MON_COM_INT_PTO_ENTREGA",nullable=true)
	public BigDecimal getMonComIntPtoEntrega() {
		return monComIntPtoEntrega;
	}

	public void setMonComIntPtoEntrega(BigDecimal monComIntPtoEntrega) {
		this.monComIntPtoEntrega = monComIntPtoEntrega;
	}

	@Column(name = "MON_COM_LEY_CONCESIONES",nullable=true)
	public BigDecimal getMonComLeyConcesiones() {
		return monComLeyConcesiones;
	}

	public void setMonComLeyConcesiones(BigDecimal monComLeyConcesiones) {
		this.monComLeyConcesiones = monComLeyConcesiones;
	}

	@Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name="NUM_SUM_AFECTADO",nullable=true)
	public String getNumSumAfectado() {
		return numSumAfectado;
	}

	public void setNumSumAfectado(String numSumAfectado) {
		this.numSumAfectado = numSumAfectado;
	}

	@Column(name="SEMESTRE",nullable=true)
	public String getSemestre() {
		return semestre;
	}

	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}

	@Column(name="INDICADOR_N",nullable=true)
	public BigDecimal getIndicadorN() {
		return indicadorN;
	}

	public void setIndicadorN(BigDecimal indicadorN) {
		this.indicadorN = indicadorN;
	}

	@Column(name="INDICADOR_D",nullable=true)
	public BigDecimal getIndicadorD() {
		return indicadorD;
	}

	public void setIndicadorD(BigDecimal indicadorD) {
		this.indicadorD = indicadorD;
	}

	@Column(name="ANO",nullable=true)
	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	@Column(name="MON_COM_REC_CARGA",nullable=true)
	public BigDecimal getMonComRecCarga() {
		return monComRecCarga;
	}

	public void setMonComRecCarga(BigDecimal monComRecCarga) {
		this.monComRecCarga = monComRecCarga;
	}

	@Column(name="NUM_INT_REC_CARGA",nullable=true)
	public long getNumIntRecCarga() {
		return numIntRecCarga;
	}

	public void setNumIntRecCarga(long numIntRecCarga) {
		this.numIntRecCarga = numIntRecCarga;
	}

	@Column(name="DUR_INT_REC_CARGA",nullable=true)
	public BigDecimal getDurIntRecCarga() {
		return durIntRecCarga;
	}

	public void setDurIntRecCarga(BigDecimal durIntRecCarga) {
		this.durIntRecCarga = durIntRecCarga;
	}

	@Column(name="TIP_COMPENSACION",nullable=true)
	public String getTipCompensacion() {
		return tipCompensacion;
	}

	public void setTipCompensacion(String tipCompensacion) {
		this.tipCompensacion = tipCompensacion;
	}

	@Column(name="COD_ALIMENTADOR",nullable=true)
	public String getCodAlimentador() {
		return codAlimentador;
	}

	public void setCodAlimentador(String codAlimentador) {
		this.codAlimentador = codAlimentador;
	}

	@Column(name="SIS_ELECTRICO",nullable=true)
	public String getSisElectrico() {
		return sisElectrico;
	}

	public void setSisElectrico(String sisElectrico) {
		this.sisElectrico = sisElectrico;
	}
	
	@Column(name="NIC_INT_NO_PROGRAMADAS",nullable=true)
	public BigDecimal getNicIntNoProgramadas() {
		return nicIntNoProgramadas;
	}

	public void setNicIntNoProgramadas(BigDecimal nicIntNoProgramadas) {
		this.nicIntNoProgramadas = nicIntNoProgramadas;
	}

	@Column(name="NIC_INT_PRO_MANTENIMIENTO",nullable=true)
	public BigDecimal getNicIntProMantenimiento() {
		return nicIntProMantenimiento;
	}

	public void setNicIntProMantenimiento(BigDecimal nicIntProMantenimiento) {
		this.nicIntProMantenimiento = nicIntProMantenimiento;
	}

	@Column(name="NIC_INT_PRO_EXP_REFORZAMIENTO",nullable=true)
	public BigDecimal getNicIntProExpReforzamiento() {
		return nicIntProExpReforzamiento;
	}

	public void setNicIntProExpReforzamiento(BigDecimal nicIntProExpReforzamiento) {
		this.nicIntProExpReforzamiento = nicIntProExpReforzamiento;
	}

	@Column(name="DIC_INT_NO_PROGRAMADAS",nullable=true)
	public BigDecimal getDicIntNoProgramadas() {
		return dicIntNoProgramadas;
	}

	public void setDicIntNoProgramadas(BigDecimal dicIntNoProgramadas) {
		this.dicIntNoProgramadas = dicIntNoProgramadas;
	}

	@Column(name="DIC_INT_PRO_MANTENIMIENTO",nullable=true)
	public BigDecimal getDicIntProMantenimiento() {
		return dicIntProMantenimiento;
	}

	public void setDicIntProMantenimiento(BigDecimal dicIntProMantenimiento) {
		this.dicIntProMantenimiento = dicIntProMantenimiento;
	}

	@Column(name="DIC_INT_PRO_EXP_REFORZAMIENTO",nullable=true)
	public BigDecimal getDicIntProExpReforzamiento() {
		return dicIntProExpReforzamiento;
	}

	public void setDicIntProExpReforzamiento(BigDecimal dicIntProExpReforzamiento) {
		this.dicIntProExpReforzamiento = dicIntProExpReforzamiento;
	}

	@Column(name="NIVEL_TENSION",nullable=true)
	public String getNivelTension() {
		return nivelTension;
	}

	public void setNivelTension(String nivelTension) {
		this.nivelTension = nivelTension;
	}

	@Column(name="UBI_SUMINISTRO",nullable=true)
	public String getUbiSuministro() {
		return ubiSuministro;
	}

	public void setUbiSuministro(String ubiSuministro) {
		this.ubiSuministro = ubiSuministro;
	}

	@Column(name="COD_LOCALIDAD",nullable=true)
	public String getCodLocalidad() {
		return codLocalidad;
	}

	public void setCodLocalidad(String codLocalidad) {
		this.codLocalidad = codLocalidad;
	}


	@Column(name="COD_SED",nullable=true)
	public String getCodSed() {
		return codSed;
	}

	public void setCodSed(String codSed) {
		this.codSed = codSed;
	}

	@Column(name="AFECTADOS",nullable=true)
	public BigDecimal getAfectados() {
		return afectados;
	}

	public void setAfectados(BigDecimal afectados) {
		this.afectados = afectados;
	}

	@Column(name="COMPENSADO",nullable=true)
	public String getCompensado() {
		return compensado;
	}

	public void setCompensado(String compensado) {
		this.compensado = compensado;
	}
   
	 	
}
