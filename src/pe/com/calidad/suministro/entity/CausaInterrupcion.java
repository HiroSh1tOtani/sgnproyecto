
package pe.com.calidad.suministro.entity;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
* Causa Interrupcion Suministro.
*
* @version 1.0 05 Mar 2014
* @author Luis
*/
@Entity
@Table(name="CAL_CAUSA_INTERRUPCION")
@SequenceGenerator(name = "CauInterrupcionIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "CausaInterrupcionSeq")
public class CausaInterrupcion implements Serializable{
    private long cauInterrupcionId;
    private String codCauInterrupcion;
    private String descripcion;
    private String estado;
    
    private Set<Interrupcion> interrupciones;
 

    public CausaInterrupcion(String codCauInterrupcion, String descripcion, String estado) {
        this.codCauInterrupcion = codCauInterrupcion;
        this.descripcion = descripcion;
        this.estado = estado;
        this.interrupciones = new HashSet<Interrupcion>();
        
    }

    public CausaInterrupcion() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CauInterrupcionIdGenerator")
    @Column(name = "CAU_INTERRUPCION_ID",nullable=false,unique=true)
    public long getCauInterrupcionId() {
        return cauInterrupcionId;
    }

    public void setCauInterrupcionId(long cauInterrupcionId) {
        this.cauInterrupcionId = cauInterrupcionId;
    }
    
    @Column(name="COD_CAU_INTERRUPCION",nullable=true)
    public String getCodCauInterrupcion() {
        return codCauInterrupcion;
    }

    public void setCodCauInterrupcion(String codCauInterrupcion) {
        this.codCauInterrupcion = codCauInterrupcion;
    }
    
    @Column(name="DESCRIPCION",nullable=true)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "causaInterrupcion")
    public Set<Interrupcion> getInterrupciones() {
        return interrupciones;
    }

    public void setInterrupciones(Set<Interrupcion> interrupciones) {
        this.interrupciones = interrupciones;
    }

    
	public String toString(){
		return new Long(this.getCauInterrupcionId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getCauInterrupcionId()==((CausaInterrupcion)obj).getCauInterrupcionId()){
			return true;
		}else{
			return false;
		}
	}
}
