
package pe.com.calidad.suministro.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import java.math.BigDecimal;

import javax.persistence.*;

import pe.com.indra.calidad.entity.Localidad;
import pe.com.indra.calidad.entity.PeriodoMedicion;
/**
 * Suministro Afectado Suministro.
*
* @version 1.0 05 Mar 2014
* @author Luis
 */
@Entity
@Table(name="CAL_INTERRUPCION")
@SequenceGenerator (name = "InterrupcionIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "InterrupcionSeq")
public class Interrupcion implements Serializable {
    
	private long interrupcionId;
    private String codInterrupcion;
    private Timestamp fecIniProgramada;
    private Timestamp fecFinProgramada;
    private Timestamp fecNotificacion1;
    private Timestamp fecNotificacion2;
    private String ubiPtoIntProgramado;
    private String nomResponsable;
    private String resumen;
    private String sustentacion;
    private String solFueMayor;
    private Timestamp fecIniInterrupcion;
    private Timestamp fecFinInterrupcion;
    private String fasInterrumpida;
    private BigDecimal potIntEstimada;
    private BigDecimal EneNoSumEstimada;
    private long numSumRegAfectados ;
    private long numCliLibAfectados;
    private String UbiFalla;
    private String motFalla;
    private String LocFalla;
    private String codRelevador;
    private BigDecimal eneTeoNoSuministrada;
    private String recCarga;
    private String desZonAfectada;
    private String semestre;
    private String zonAfectada;
    private String oriInterrupcion;
    
    
    private CausaInterrupcion causaInterrupcion;
    private TipoInterrupcion tipoInterrupcion;
    private TipoNotificacion tipoNotificacion1;
    private TipoNotificacion tipoNotificacion2;
 
    private ModalidadDeteccion modalidadDeteccion;
    private PeriodoMedicion periodoMedicion;
    private long numSumRegEstimados;
    private String estado;
    
    private String nroOficio;
    private Timestamp fecOficio;
    private String codosi;
    private String nroExp;
    private String nroResol;
    private Timestamp fecResol;
    private String estResol;
    private String tenInterrupcion;
    private String tipoRelevador;
    private BigDecimal duracionExoFum;
    private String sincronizar;
    private Timestamp fecFinExoneracion;
    
    private Set<SuministroAfectado> suministrosAfectado;
    private Localidad localidad;
    private String sisElectrico;
    private long nroClieSisElectrico;
    private String secTipico;

    private String codInsSalio;
    private String tipInsSalio;
    private String codInsFalla;
    private Timestamp fecFinComSalio ;
    private String codOriInterrupcion ;
    private String propiedad ;
    private String responsable;
    private String codCausa074 ;
    private String intImportante;
    private String naturaleza;
    private BigDecimal durHorSuministro;
    private String codSecOsinerg;
    private String anexo1074;
    private String tipInsFalla;
    private String leyConcesiones;
    private BigDecimal cargoEnergiaB1;
    private BigDecimal cargoPotenciaB2;
    private BigDecimal cargoEnergiaB1BT6;
    private BigDecimal cargoPotenciaB2BT6;
    private BigDecimal nhubt;
    private BigDecimal cosRacionamiento;
    
    private BigDecimal cargoEnergiaB1BT5C;
    private BigDecimal cargoPotenciaB2BT5C;
    private BigDecimal cargoEnergiaB1BT5D;
    private BigDecimal cargoPotenciaB2BT5D;
    
    private String alimentador;
  
	public Interrupcion() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "InterrupcionIdGenerator")
    @Column(name = "INTERRUPCION_ID",nullable=false,unique=true)
    public long getInterrupcionId() {
		return interrupcionId;
	}

	public void setInterrupcionId(long interrupcionId) {
		this.interrupcionId = interrupcionId;
	}

	@Column(name = "COD_INTERRUPCION",nullable=true)
	public String getCodInterrupcion() {
		return codInterrupcion;
	}

	public void setCodInterrupcion(String codInterrupcion) {
		this.codInterrupcion = codInterrupcion;
	}

	@Column(name = "FEC_INI_PROGRAMADA",nullable=true)
	public Timestamp getFecIniProgramada() {
		return fecIniProgramada;
	}

	public void setFecIniProgramada(Timestamp fecIniProgramada) {
		this.fecIniProgramada = fecIniProgramada;
	}

	@Column(name = "FEC_FIN_PROGRAMADA",nullable=true)
	public Timestamp getFecFinProgramada() {
		return fecFinProgramada;
	}

	public void setFecFinProgramada(Timestamp fecFinProgramada) {
		this.fecFinProgramada = fecFinProgramada;
	}

	@Column(name = "FEC_NOTIFICACION_1",nullable=true)
	public Timestamp getFecNotificacion1() {
		return fecNotificacion1;
	}

	public void setFecNotificacion1(Timestamp fecNotificacion1) {
		this.fecNotificacion1 = fecNotificacion1;
	}

	@Column(name = "FEC_NOTIFICACION_2",nullable=true)
	public Timestamp getFecNotificacion2() {
		return fecNotificacion2;
	}

	public void setFecNotificacion2(Timestamp fecNotificacion2) {
		this.fecNotificacion2 = fecNotificacion2;
	}

	@Column(name = "UBI_PTO_INT_PROGRAMADO",nullable=true)
	public String getUbiPtoIntProgramado() {
		return ubiPtoIntProgramado;
	}

	public void setUbiPtoIntProgramado(String ubiPtoIntProgramado) {
		this.ubiPtoIntProgramado = ubiPtoIntProgramado;
	}

	@Column(name = "NOM_RESPONSABLE",nullable=true)
	public String getNomResponsable() {
		return nomResponsable;
	}

	public void setNomResponsable(String nomResponsable) {
		this.nomResponsable = nomResponsable;
	}

	@Column(name = "RESUMEN",nullable=true)
	public String getResumen() {
		return resumen;
	}

	public void setResumen(String resumen) {
		this.resumen = resumen;
	}

	@Column(name = "SUSTENTACION",nullable=true)
	public String getSustentacion() {
		return sustentacion;
	}

	public void setSustentacion(String sustentacion) {
		this.sustentacion = sustentacion;
	}

	@Column(name = "SOL_FUE_MAYOR",nullable=true)
	public String getSolFueMayor() {
		return solFueMayor;
	}

	public void setSolFueMayor(String solFueMayor) {
		this.solFueMayor = solFueMayor;
	}

	@Column(name = "FEC_INI_INTERRUPCION",nullable=true)
	public Timestamp getFecIniInterrupcion() {
		return fecIniInterrupcion;
	}

	public void setFecIniInterrupcion(Timestamp fecIniInterrupcion) {
		this.fecIniInterrupcion = fecIniInterrupcion;
	}

	@Column(name = "FEC_FIN_INTERRUPCION",nullable=true)
	public Timestamp getFecFinInterrupcion() {
		return fecFinInterrupcion;
	}

	public void setFecFinInterrupcion(Timestamp fecFinInterrupcion) {
		this.fecFinInterrupcion = fecFinInterrupcion;
	}

	@Column(name = "FAS_INTERRUMPIDA",nullable=true)
	public String getFasInterrumpida() {
		return fasInterrumpida;
	}

	public void setFasInterrumpida(String fasInterrumpida) {
		this.fasInterrumpida = fasInterrumpida;
	}

	@Column(name = "POT_INT_ESTIMADA",nullable=true)
	public BigDecimal getPotIntEstimada() {
		return potIntEstimada;
	}

	public void setPotIntEstimada(BigDecimal potIntEstimada) {
		this.potIntEstimada = potIntEstimada;
	}

	@Column(name = "ENE_NO_SUM_ESTIMADA",nullable=true)
	public BigDecimal getEneNoSumEstimada() {
		return EneNoSumEstimada;
	}

	public void setEneNoSumEstimada(BigDecimal eneNoSumEstimada) {
		EneNoSumEstimada = eneNoSumEstimada;
	}

	@Column(name = "NUM_SUM_REG_AFECTADOS",nullable=true)
	public long getNumSumRegAfectados() {
		return numSumRegAfectados;
	}

	public void setNumSumRegAfectados(long numSumRegAfectados) {
		this.numSumRegAfectados = numSumRegAfectados;
	}

	@Column(name = "NUM_CLI_LIB_AFECTADOS",nullable=true)
	public long getNumCliLibAfectados() {
		return numCliLibAfectados;
	}

	public void setNumCliLibAfectados(long numCliLibAfectados) {
		this.numCliLibAfectados = numCliLibAfectados;
	}

	@Column(name = "UBI_FALLA",nullable=true)
	public String getUbiFalla() {
		return UbiFalla;
	}

	public void setUbiFalla(String ubiFalla) {
		UbiFalla = ubiFalla;
	}

	@Column(name = "MOT_FALLA",nullable=true)
	public String getMotFalla() {
		return motFalla;
	}

	public void setMotFalla(String motFalla) {
		this.motFalla = motFalla;
	}

	@Column(name = "LOC_FALLA",nullable=true)
	public String getLocFalla() {
		return LocFalla;
	}

	public void setLocFalla(String locFalla) {
		LocFalla = locFalla;
	}

	@Column(name = "COD_RELEVADOR",nullable=true)
	public String getCodRelevador() {
		return codRelevador;
	}

	public void setCodRelevador(String codRelevador) {
		this.codRelevador = codRelevador;
	}
	

	@Column(name = "ENE_TEO_NO_SUMINISTRADA",nullable=true)
	public BigDecimal getEneTeoNoSuministrada() {
		return eneTeoNoSuministrada;
	}

	public void setEneTeoNoSuministrada(BigDecimal eneTeoNoSuministrada) {
		this.eneTeoNoSuministrada = eneTeoNoSuministrada;
	}

	@Column(name = "REC_CARGA",nullable=true)
	public String getRecCarga() {
		return recCarga;
	}

	public void setRecCarga(String recCarga) {
		this.recCarga = recCarga;
	}

	@Column(name = "DES_ZON_AFECTADA",nullable=true)
	public String getDesZonAfectada() {
		return desZonAfectada;
	}

	public void setDesZonAfectada(String desZonAfectada) {
		this.desZonAfectada = desZonAfectada;
	}

	@ManyToOne
	@JoinColumn(name = "PK_CAU_INTERRUPCION_ID",nullable=true)
	public CausaInterrupcion getCausaInterrupcion() {
		return causaInterrupcion;
	}

	public void setCausaInterrupcion(CausaInterrupcion causaInterrupcion) {
		this.causaInterrupcion = causaInterrupcion;
	}

	@ManyToOne
	@JoinColumn(name = "PK_TIP_INTERRUPCION_ID",nullable=true)
	public TipoInterrupcion getTipoInterrupcion() {
		return tipoInterrupcion;
	}

	public void setTipoInterrupcion(TipoInterrupcion tipoInterrupcion) {
		this.tipoInterrupcion = tipoInterrupcion;
	}

	@ManyToOne
	@JoinColumn(name = "PK_TIP_NOTIFICACION_1_ID",nullable=true)
	public TipoNotificacion getTipoNotificacion1() {
		return tipoNotificacion1;
	}

	public void setTipoNotificacion1(TipoNotificacion tipoNotificacion1) {
		this.tipoNotificacion1 = tipoNotificacion1;
	}
	
	@ManyToOne
	@JoinColumn(name = "PK_TIP_NOTIFICACION_2_ID",nullable=true)
	public TipoNotificacion getTipoNotificacion2() {
		return tipoNotificacion2;
	}

	public void setTipoNotificacion2(TipoNotificacion tipoNotificacion2) {
		this.tipoNotificacion2 = tipoNotificacion2;
	}

	@ManyToOne
	@JoinColumn(name = "PK_MOD_DETECCION_ID",nullable=true)
	public ModalidadDeteccion getModalidadDeteccion() {
		return modalidadDeteccion;
	}

	public void setModalidadDeteccion(ModalidadDeteccion modalidadDeteccion) {
		this.modalidadDeteccion = modalidadDeteccion;
	}

		
	@ManyToOne
	@JoinColumn(name = "PK_PER_MEDICION_ID",nullable=true)
	public PeriodoMedicion getPeriodoMedicion() {
		return periodoMedicion;
	}

	public void setPeriodoMedicion(PeriodoMedicion periodoMedicion) {
		this.periodoMedicion = periodoMedicion;
	}


	@Column(name = "ESTADO",nullable=false)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	
	@OneToMany(cascade = CascadeType.ALL,mappedBy = "interrupcion")
	public Set<SuministroAfectado> getSuministrosAfectado() {
		return suministrosAfectado;
	}

	public void setSuministrosAfectado(Set<SuministroAfectado> suministrosAfectado) {
		this.suministrosAfectado = suministrosAfectado;
	}
    
	@Column(name = "NUM_SUM_REG_ESTIMADOS",nullable=true)
	public long getNumSumRegEstimados() {
		return numSumRegEstimados;
	}

	public void setNumSumRegEstimados(long numSumRegEstimados) {
		this.numSumRegEstimados = numSumRegEstimados;
	}

	@Column(name = "SEMESTRE",nullable=true)
	public String getSemestre() {
		return semestre;
	}

	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}
	
	@Column(name = "ZON_AFECTADA",nullable=true)
	public String getZonAfectada() {
		return zonAfectada;
	}

	public void setZonAfectada(String zonAfectada) {
		this.zonAfectada = zonAfectada;
	}

	@Column(name = "ORI_INTERRUPCION",nullable=true)
	public String getOriInterrupcion() {
		return oriInterrupcion;
	}

	public void setOriInterrupcion(String oriInterrupcion) {
		this.oriInterrupcion = oriInterrupcion;
	}
	
	@Column(name = "NRO_OFICIO",nullable=true)
	public String getNroOficio() {
		return nroOficio;
	}

	public void setNroOficio(String nroOficio) {
		this.nroOficio = nroOficio;
	}

	@Column(name = "FEC_OFICIO",nullable=true)
	public Timestamp getFecOficio() {
		return fecOficio;
	}

	public void setFecOficio(Timestamp fecOficio) {
		this.fecOficio = fecOficio;
	}

	@Column(name = "CODOSI",nullable=true)
	public String getCodosi() {
		return codosi;
	}

	public void setCodosi(String codosi) {
		this.codosi = codosi;
	}

	@Column(name = "NRO_EXP",nullable=true)
	public String getNroExp() {
		return nroExp;
	}

	public void setNroExp(String nroExp) {
		this.nroExp = nroExp;
	}

	@Column(name = "NRO_RESOL",nullable=true)
	public String getNroResol() {
		return nroResol;
	}

	public void setNroResol(String nroResol) {
		this.nroResol = nroResol;
	}

	@Column(name = "FEC_RESOL",nullable=true)
	public Timestamp getFecResol() {
		return fecResol;
	}

	public void setFecResol(Timestamp fecResol) {
		this.fecResol = fecResol;
	}

	@Column(name = "EST_RESOL",nullable=true)
	public String getEstResol() {
		return estResol;
	}

	public void setEstResol(String estResol) {
		this.estResol = estResol;
	}
	
	@ManyToOne
    @JoinColumn(name="PK_LOCALIDAD_ID", nullable=true)
	public Localidad getLocalidad() {
		return localidad;
	}

	public void setLocalidad(Localidad localidad) {
		this.localidad = localidad;
	}

	@Column(name = "TEN_INTERRUPCION",nullable=true)
	public String getTenInterrupcion() {
		return tenInterrupcion;
	}

	public void setTenInterrupcion(String tenInterrupcion) {
		this.tenInterrupcion = tenInterrupcion;
	}

	
	@Column(name = "TIPO_RELEVADOR",nullable=true)
	public String getTipoRelevador() {
		return tipoRelevador;
	}

	public void setTipoRelevador(String tipoRelevador) {
		this.tipoRelevador = tipoRelevador;
	}
	
	@Column(name = "DURACION_EXO_FUM",nullable=true)
	public BigDecimal getDuracionExoFum() {
		return duracionExoFum;
	}

	public void setDuracionExoFum(BigDecimal duracionExoFum) {
		this.duracionExoFum = duracionExoFum;
	}

	@Column(name = "SINCRONIZAR",nullable=true)
	public String getSincronizar() {
		return sincronizar;
	}

	public void setSincronizar(String sincronizar) {
		this.sincronizar = sincronizar;
	}
	
	@Column(name = "FEC_FIN_EXONERACION",nullable=true)
	public Timestamp getFecFinExoneracion() {
		return fecFinExoneracion;
	}

	public void setFecFinExoneracion(Timestamp fecFinExoneracion) {
		this.fecFinExoneracion = fecFinExoneracion;
	}
	
	@Column(name = "SIS_ELECTRICO",nullable=true)
	public String getSisElectrico() {
		return sisElectrico;
	}

	public void setSisElectrico(String sisElectrico) {
		this.sisElectrico = sisElectrico;
	}

	@Column(name = "NRO_CLIE_SIS_ELECTRICO",nullable=true)
	public long getNroClieSisElectrico() {
		return nroClieSisElectrico;
	}

	public void setNroClieSisElectrico(long nroClieSisElectrico) {
		this.nroClieSisElectrico = nroClieSisElectrico;
	}

	
	@Column(name = "SEC_TIPICO",nullable=true)
	public String getSecTipico() {
		return secTipico;
	}

	public void setSecTipico(String secTipico) {
		this.secTipico = secTipico;
	}
	
	@Column(name = "COD_INS_SALIO",nullable=true)
	public String getCodInsSalio() {
		return codInsSalio;
	}

	public void setCodInsSalio(String codInsSalio) {
		this.codInsSalio = codInsSalio;
	}

	@Column(name = "TIP_INS_SALIO",nullable=true)
	public String getTipInsSalio() {
		return tipInsSalio;
	}

	public void setTipInsSalio(String tipInsSalio) {
		this.tipInsSalio = tipInsSalio;
	}

	@Column(name = "COD_INS_FALLA",nullable=true)
	public String getCodInsFalla() {
		return codInsFalla;
	}

	public void setCodInsFalla(String codInsFalla) {
		this.codInsFalla = codInsFalla;
	}

	@Column(name = "FEC_FIN_COM_SALIO",nullable=true)
	public Timestamp getFecFinComSalio() {
		return fecFinComSalio;
	}

	public void setFecFinComSalio(Timestamp fecFinComSalio) {
		this.fecFinComSalio = fecFinComSalio;
	}

	@Column(name = "COD_ORI_INTERRUPCION",nullable=true)
	public String getCodOriInterrupcion() {
		return codOriInterrupcion;
	}

	public void setCodOriInterrupcion(String codOriInterrupcion) {
		this.codOriInterrupcion = codOriInterrupcion;
	}

	@Column(name = "PROPIEDAD",nullable=true)
	public String getPropiedad() {
		return propiedad;
	}

	public void setPropiedad(String propiedad) {
		this.propiedad = propiedad;
	}

	@Column(name = "RESPONSABLE",nullable=true)
	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	@Column(name = "COD_CAUSA_074",nullable=true)
	public String getCodCausa074() {
		return codCausa074;
	}

	public void setCodCausa074(String codCausa074) {
		this.codCausa074 = codCausa074;
	}

	@Column(name = "INT_IMPORTANTE",nullable=true)
	public String getIntImportante() {
		return intImportante;
	}

	public void setIntImportante(String intImportante) {
		this.intImportante = intImportante;
	}
	
	@Column(name = "NATURALEZA",nullable=true)
	public String getNaturaleza() {
		return naturaleza;
	}

	public void setNaturaleza(String naturaleza) {
		this.naturaleza = naturaleza;
	}

	@Column(name = "DUR_HOR_SUMINISTRO",nullable=true)
	public BigDecimal getDurHorSuministro() {
		return durHorSuministro;
	}

	public void setDurHorSuministro(BigDecimal durHorSuministro) {
		this.durHorSuministro = durHorSuministro;
	}

	@Column(name = "COD_SEC_OSINERG",nullable=true)
	public String getCodSecOsinerg() {
		return codSecOsinerg;
	}

	public void setCodSecOsinerg(String codSecOsinerg) {
		this.codSecOsinerg = codSecOsinerg;
	}

	@Column(name = "ANEXO1_074",nullable=true)
	public String getAnexo1074() {
		return anexo1074;
	}

	public void setAnexo1074(String anexo1074) {
		this.anexo1074 = anexo1074;
	}

	@Column(name = "TIP_INS_FALLA",nullable=true)
	public String getTipInsFalla() {
		return tipInsFalla;
	}

	public void setTipInsFalla(String tipInsFalla) {
		this.tipInsFalla = tipInsFalla;
	}
	
	@Column(name = "LEY_CONCESIONES",nullable=true)
	public String getLeyConcesiones() {
		return leyConcesiones;
	}

	public void setLeyConcesiones(String leyConcesiones) {
		this.leyConcesiones = leyConcesiones;
	}

	@Column(name = "CARGO_ENERGIA_B1",nullable=true)
	public BigDecimal getCargoEnergiaB1() {
		return cargoEnergiaB1;
	}

	public void setCargoEnergiaB1(BigDecimal cargoEnergiaB1) {
		this.cargoEnergiaB1 = cargoEnergiaB1;
	}

	@Column(name = "CARGO_POTENCIA_B2",nullable=true)
	public BigDecimal getCargoPotenciaB2() {
		return cargoPotenciaB2;
	}

	public void setCargoPotenciaB2(BigDecimal cargoPotenciaB2) {
		this.cargoPotenciaB2 = cargoPotenciaB2;
	}

	@Column(name = "CARGO_ENERGIA_B1_BT6",nullable=true)
	public BigDecimal getCargoEnergiaB1BT6() {
		return cargoEnergiaB1BT6;
	}

	public void setCargoEnergiaB1BT6(BigDecimal cargoEnergiaB1BT6) {
		this.cargoEnergiaB1BT6 = cargoEnergiaB1BT6;
	}

	@Column(name = "CARGO_POTENCIA_B2_BT6",nullable=true)
	public BigDecimal getCargoPotenciaB2BT6() {
		return cargoPotenciaB2BT6;
	}

	public void setCargoPotenciaB2BT6(BigDecimal cargoPotenciaB2BT6) {
		this.cargoPotenciaB2BT6 = cargoPotenciaB2BT6;
	}

	@Column(name = "NHUBT",nullable=true)
	public BigDecimal getNhubt() {
		return nhubt;
	}

	public void setNhubt(BigDecimal nhubt) {
		this.nhubt = nhubt;
	}

	@Column(name = "COS_RACIONAMIENTO",nullable=true)
	public BigDecimal getCosRacionamiento() {
		return cosRacionamiento;
	}

	public void setCosRacionamiento(BigDecimal cosRacionamiento) {
		this.cosRacionamiento = cosRacionamiento;
	}

	@Column(name = "CARGO_ENERGIA_B1_BT5C",nullable=true)
	public BigDecimal getCargoEnergiaB1BT5C() {
		return cargoEnergiaB1BT5C;
	}

	public void setCargoEnergiaB1BT5C(BigDecimal cargoEnergiaB1BT5C) {
		this.cargoEnergiaB1BT5C = cargoEnergiaB1BT5C;
	}

	@Column(name = "CARGO_POTENCIA_B2_BT5C",nullable=true)
	public BigDecimal getCargoPotenciaB2BT5C() {
		return cargoPotenciaB2BT5C;
	}

	public void setCargoPotenciaB2BT5C(BigDecimal cargoPotenciaB2BT5C) {
		this.cargoPotenciaB2BT5C = cargoPotenciaB2BT5C;
	}

	@Column(name = "CARGO_ENERGIA_B1_BT5D",nullable=true)
	public BigDecimal getCargoEnergiaB1BT5D() {
		return cargoEnergiaB1BT5D;
	}

	public void setCargoEnergiaB1BT5D(BigDecimal cargoEnergiaB1BT5D) {
		this.cargoEnergiaB1BT5D = cargoEnergiaB1BT5D;
	}

	@Column(name = "CARGO_POTENCIA_B2_BT5D",nullable=true)
	public BigDecimal getCargoPotenciaB2BT5D() {
		return cargoPotenciaB2BT5D;
	}
	
	public void setAlimentador(String alimentador) {
		this.alimentador = alimentador;
	}

	@Column(name = "ALIMENTADOR",nullable=true, length=20)
	public String getAlimentador() {
		return alimentador;
	}

	public void setCargoPotenciaB2BT5D(BigDecimal cargoPotenciaB2BT5D) {
		this.cargoPotenciaB2BT5D = cargoPotenciaB2BT5D;
	}

	public String toString(){
		return new Long(this.getInterrupcionId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getInterrupcionId()==((Interrupcion)obj).getInterrupcionId()){
			return true;
		}else{
			return false;
		}
	}

	

	

		
}
