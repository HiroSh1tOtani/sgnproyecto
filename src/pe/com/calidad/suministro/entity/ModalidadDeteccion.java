
package pe.com.calidad.suministro.entity;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
* Modalidad Deteccion Suministro.
*
* @version 1.0 05 Mar 2014
* @author Luis
*/
@Entity
@Table(name="CAL_MODALIDAD_DETECCION")
@SequenceGenerator(name = "ModDeteccionIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "ModalidadDeteccionSeq")
public class ModalidadDeteccion implements Serializable{
    private long modDeteccionId;
    private String codModDeteccion;
    private String descripcion;
    private String estado;
    
    private Set<Interrupcion> interrupciones;
    

    public ModalidadDeteccion(String codModDeteccion, String descripcion, String estado) {
        this.codModDeteccion = codModDeteccion;
        this.descripcion = descripcion;
        this.estado = estado;
        this.interrupciones = new HashSet<Interrupcion>();
        
    }

    public ModalidadDeteccion() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ModDeteccionIdGenerator")
    @Column(name = "MOD_DETECCION_ID",nullable=false,unique=true)
    public long getModDeteccionId() {
        return modDeteccionId;
    }

    public void setModDeteccionId(long modDeteccionId) {
        this.modDeteccionId = modDeteccionId;
    }
    
    @Column(name="COD_MOD_DETECCION",nullable=true)
    public String getCodModDeteccion() {
        return codModDeteccion;
    }

    public void setCodModDeteccion(String codModDeteccion) {
        this.codModDeteccion = codModDeteccion;
    }
    
    @Column(name="DESCRIPCION",nullable=true)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "modalidadDeteccion")
    public Set<Interrupcion> getInterrupciones() {
        return interrupciones;
    }

    public void setInterrupciones(Set<Interrupcion> interrupciones) {
        this.interrupciones = interrupciones;
    }

   
	public String toString(){
		return new Long(this.getModDeteccionId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getModDeteccionId()==((ModalidadDeteccion)obj).getModDeteccionId()){
			return true;
		}else{
			return false;
		}
	}
}
