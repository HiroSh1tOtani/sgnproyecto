
package pe.com.calidad.suministro.entity;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
* Tipo Notificacion de Suministro.
*
* @version 1.0 05 Mar 2014
* @author Lucho
*/
@Entity
@Table(name="CAL_TIPO_NOTIFICACION")
@SequenceGenerator(name = "TipParametroIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "TipoNotificacionSeq")
public class TipoNotificacion implements Serializable{
    private long tipNotificacionId;
    private String codTipNotificacion;
   
    private String descripcion;
    private String estado;
    
    private Set<Interrupcion> interrupciones;
    private Set<Interrupcion> interrupciones2;
    
    public TipoNotificacion(String codTipNotificacion,String codTipNotificacion2, String descripcion, String estado) {
        this.codTipNotificacion = codTipNotificacion;
       
        this.descripcion = descripcion;
        this.estado = estado;
        this.interrupciones = new HashSet<Interrupcion>();
    
    }

    public TipoNotificacion() {
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TipParametroIdGenerator")
    @Column(name = "TIP_NOTIFICACION_ID",nullable=false,unique=true)
    public long getTipNotificacionId() {
        return tipNotificacionId;
    }

    public void setTipNotificacionId(long tipNotificacionId) {
        this.tipNotificacionId = tipNotificacionId;
    }
    
    @Column(name="COD_TIP_NOTIFICACION",nullable=true)
    public String getCodTipNotificacion() {
        return codTipNotificacion;
    }

    public void setCodTipNotificacion(String codTipNotificacion) {
        this.codTipNotificacion = codTipNotificacion;
    }
    
       
    @Column(name="DESCRIPCION",nullable=true)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    @Column(name="ESTADO",nullable=false)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "tipoNotificacion1")
    public Set<Interrupcion> getInterrupciones() {
        return interrupciones;
    }

    public void setInterrupciones(Set<Interrupcion> interrupciones) {
        this.interrupciones = interrupciones;
    }
    
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "tipoNotificacion2")
    public Set<Interrupcion> getInterrupciones2() {
		return interrupciones2;
	}

	public void setInterrupciones2(Set<Interrupcion> interrupciones2) {
		this.interrupciones2 = interrupciones2;
	}
    
	public String toString(){
		return new Long(this.getTipNotificacionId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getTipNotificacionId()==((TipoNotificacion)obj).getTipNotificacionId()){
			return true;
		}else{
			return false;
		}
	}

	

}
