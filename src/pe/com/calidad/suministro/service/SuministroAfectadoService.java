package pe.com.calidad.suministro.service;

import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;

import pe.com.calidad.suministro.dao.SuministroAfectadoDao;
import pe.com.calidad.suministro.dao.SuministroAfectadoDaoImpl;
import pe.com.calidad.suministro.entity.SuministroAfectado;
import pe.com.calidad.suministro.entity.CompensacionSuministro;
import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.indra.calidad.dao.MedicionDao;
import pe.com.indra.calidad.dao.MedicionDaoImpl;
import pe.com.indra.calidad.entity.Medicion;


/**
 *
 * @author Luis
 */
public class SuministroAfectadoService {

    public void create(SuministroAfectado suministroAfectado) {
    	SuministroAfectadoDao dao = new SuministroAfectadoDaoImpl();

       // dao.create(suministroAfectado);
        
        try {
        	dao.create(suministroAfectado);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Suministro Afectado.", e);
		}
             
    }

    public SuministroAfectado ReadById(long sumAfectadoId) {
    	SuministroAfectadoDao dao = new SuministroAfectadoDaoImpl();
    	SuministroAfectado suministroAfectado = null;
         
        try {
        	suministroAfectado = dao.ReadById(sumAfectadoId);
		}  catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Suministro Afectado."
		}
        return suministroAfectado;
                     
    }

    public void update(SuministroAfectado suministroAfectado) {
    	SuministroAfectadoDao dao = new SuministroAfectadoDaoImpl();

       try {
        	dao.update(suministroAfectado);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long sumAfectadoId) {
    	SuministroAfectadoDao dao = new SuministroAfectadoDaoImpl();

        try {
        	dao.delete(sumAfectadoId);
		}  catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public Interrupcion selectInterrupcion(long sumAfectadoId) {
    	SuministroAfectadoDao dao = new SuministroAfectadoDaoImpl();
         
         return dao.selectInterrupcion(sumAfectadoId);
    }

       
    public List<SuministroAfectado> getAllRows(){
    	SuministroAfectadoDao dao = new SuministroAfectadoDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<SuministroAfectado> getAllRows1(){
    	SuministroAfectadoDao dao = new SuministroAfectadoDaoImpl();
    	return dao.getAllRows1();
    }
    
    public List<SuministroAfectado> getAllRows1(long interrupcionId){
    	SuministroAfectadoDao dao = new SuministroAfectadoDaoImpl();
    	return dao.getAllRows1(interrupcionId);
    }
    
    public List<SuministroAfectado> getInterrupcionxSuministro(String numSumAfectado){
    	SuministroAfectadoDao dao = new SuministroAfectadoDaoImpl();
    	List<SuministroAfectado> suministroAfectado=null;
    	try {
    		suministroAfectado = dao.getInterrupcionxSuministro(numSumAfectado);
			
		} catch (HibernateException e) {
			throw new HibernateException("No se puede encontrar interrupciones para el Suministro.", e);
		}
    	  	
    	return suministroAfectado;
    }
    
    public List<SuministroAfectado> getInterxSuministroAnoSem(String numSumAfectado, String ano, String semestre){
    	SuministroAfectadoDao dao = new SuministroAfectadoDaoImpl();
    	List<SuministroAfectado> suministroAfectado=null;
    	try {
    		suministroAfectado = dao.getInterxSuministroAnoSem(numSumAfectado,ano,semestre);
			
		} catch (HibernateException e) {
			throw new HibernateException("No se puede encontrar interrupciones para el Suministro x a�o y semsstre. ", e);
		}
    	  	
    	return suministroAfectado;
    }
}
