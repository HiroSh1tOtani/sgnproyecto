package pe.com.calidad.suministro.service;

import java.util.Set;
import java.util.List;

import org.hibernate.HibernateException;

import pe.com.calidad.suministro.dao.ModalidadDeteccionDao;
import pe.com.calidad.suministro.dao.ModalidadDeteccionDaoImpl;
import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.entity.ModalidadDeteccion;

/**
 *
 * @author Lucho
 */
public class ModalidadDeteccionService {

    public void create(ModalidadDeteccion modalidadDeteccion) {
    	ModalidadDeteccionDao dao = new ModalidadDeteccionDaoImpl();

        try {
        	dao.create(modalidadDeteccion);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Modalidad Deteccion.", e);
		}
        
    }

    public ModalidadDeteccion ReadById(long modDeteccionId) {
    	ModalidadDeteccionDao dao = new ModalidadDeteccionDaoImpl();
        
    	ModalidadDeteccion modalidadDeteccion = null;
        
        try {
        	modalidadDeteccion = dao.ReadById(modDeteccionId);
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Modalidad Deteccion."
		}
        
        return modalidadDeteccion;
        
    }

    public ModalidadDeteccion ReadByCod(String codModDeteccion) {
    	ModalidadDeteccionDao dao = new ModalidadDeteccionDaoImpl();
    	ModalidadDeteccion modalidadDeteccion = null;
        
        try {
        	modalidadDeteccion = dao.ReadByCod(codModDeteccion);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Modalidad Deteccion."
		}
        return modalidadDeteccion;
        
    }

    public void update(ModalidadDeteccion modalidadDeteccion) {
    	ModalidadDeteccionDao dao = new ModalidadDeteccionDaoImpl();
         
        try {
        	 dao.update(modalidadDeteccion);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long modDeteccionId) {
    	ModalidadDeteccionDao dao = new ModalidadDeteccionDaoImpl();
        
        dao.delete(modDeteccionId);
    }

    public Set<Interrupcion> selectInterrupcion(long modDeteccionId) {
    	ModalidadDeteccionDao dao = new ModalidadDeteccionDaoImpl();
        return dao.selectInterrupcion(modDeteccionId);
    }
    
    public List<ModalidadDeteccion>getAllRows(){
    	ModalidadDeteccionDao dao = new ModalidadDeteccionDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<ModalidadDeteccion>getAllRows1(){
    	ModalidadDeteccionDao dao = new ModalidadDeteccionDaoImpl();
    	return dao.getAllRows();
    }
}


