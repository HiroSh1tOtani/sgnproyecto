package pe.com.calidad.suministro.service;

import java.util.List;

import org.hibernate.HibernateException;

import pe.com.calidad.suministro.dao.CostoRacionamientoDao;
import pe.com.calidad.suministro.dao.CostoRacionamientoDaoImpl;
import pe.com.calidad.suministro.entity.CostoRacionamiento;


/**
 *
 * @author Luis
 */
public class CostoRacionamietoService {

    public void create(CostoRacionamiento costoRacionamiento) {
    	CostoRacionamientoDao dao = new CostoRacionamientoDaoImpl();
        
        try {
        	dao.create(costoRacionamiento);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Costo Racionamiento.", e);
		}
        
    }

    public CostoRacionamiento ReadById(long cosRacionamientoId) {
    	CostoRacionamientoDao dao = new CostoRacionamientoDaoImpl();
    	CostoRacionamiento costoRacionamiento = null;
        
        try {
        	costoRacionamiento = dao.ReadById(cosRacionamientoId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); 
		}
        
        return costoRacionamiento;
    }

    public void update(CostoRacionamiento costoRacionamiento) {
    	CostoRacionamientoDao dao = new CostoRacionamientoDaoImpl();
        
        try {
        	dao.update(costoRacionamiento);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public void delete(long cosRacionamientoId) {
    	CostoRacionamientoDao dao = new CostoRacionamientoDaoImpl();
        
        try {
        	dao.delete(cosRacionamientoId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

     
    public List<CostoRacionamiento> getAllRows(){
    	CostoRacionamientoDao dao = new CostoRacionamientoDaoImpl();
    	return dao.getAllRows();
    }  
    
    public List<CostoRacionamiento> getAllRows1(){
    	CostoRacionamientoDao dao = new CostoRacionamientoDaoImpl();
    	return dao.getAllRows();
    }   
    
   
}
