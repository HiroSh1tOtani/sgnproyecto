package pe.com.calidad.suministro.service;

import java.util.Set;
import java.util.List;

import org.hibernate.HibernateException;

import pe.com.calidad.suministro.dao.PliegoDao;
import pe.com.calidad.suministro.dao.PliegoDaoImpl;
import pe.com.calidad.suministro.entity.CargoTarifa;
import pe.com.calidad.suministro.entity.Pliego;

/**
 *
 * @author Lucho
 */
public class PliegoService {

    public void create(Pliego pliego) {
    	PliegoDao dao = new PliegoDaoImpl();

        try {
        	dao.create(pliego);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Pliego.", e);
		}
        
    }

    public Pliego ReadById(long pliegoId) {
    	PliegoDao dao = new PliegoDaoImpl();
        
    	Pliego pliego = null;
        
        try {
        	pliego = dao.ReadById(pliegoId);
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Pliego."
		}
        
        return pliego;
        
    }

      public void update(Pliego pliego) {
    	  PliegoDao dao = new PliegoDaoImpl();
         
        try {
        	 dao.update(pliego);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long pliegoId) {
    	PliegoDao dao = new PliegoDaoImpl();
        
        dao.delete(pliegoId);
    }

    public Set<CargoTarifa> selectInterrupcion(long pliegoId) {
    	PliegoDao dao = new PliegoDaoImpl();
        return dao.selectCargoTarifa(pliegoId);
    }
    
    public List<Pliego>getAllRows(){
    	PliegoDao dao = new PliegoDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<Pliego>getAllRows1(){
    	PliegoDao dao = new PliegoDaoImpl();
    	return dao.getAllRows1();
    }
}


