package pe.com.calidad.suministro.service;

import java.util.List;

import org.hibernate.HibernateException;

import pe.com.calidad.suministro.dao.CompensacionSuministroDao;
import pe.com.calidad.suministro.dao.CompensacionSuministroDaoImpl;
import pe.com.calidad.suministro.dao.InterrupcionDao;
import pe.com.calidad.suministro.dao.InterrupcionDaoImpl;
import pe.com.calidad.suministro.dao.SuministroAfectadoDao;
import pe.com.calidad.suministro.dao.SuministroAfectadoDaoImpl;
import pe.com.calidad.suministro.entity.CompensacionSuministro;
import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.entity.SuministroAfectado;
import pe.com.indra.calidad.dao.IntervaloFRTensionDao;
import pe.com.indra.calidad.dao.IntervaloFRTensionDaoImpl;
import pe.com.indra.calidad.entity.IntervaloFRTension;

/**
 *
 * @author Luis
 */
public class CompensacionSuministroService {

    public void create(CompensacionSuministro compensacionSuministro) {
    	CompensacionSuministroDao dao = new CompensacionSuministroDaoImpl();

        try {
        	dao.create(compensacionSuministro);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Compensacion Suministro.", e);
		}
    }

    public CompensacionSuministro ReadById(long comSuministroId) {
    	CompensacionSuministroDao dao = new CompensacionSuministroDaoImpl();
    	CompensacionSuministro compensacionSuministro = null;
     
        try {
        	compensacionSuministro = dao.ReadById(comSuministroId);
		}  catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Compensacion Suministro."
		}
        return compensacionSuministro;
    }

    public CompensacionSuministro ReadByCod(String codRelevador) {
    	CompensacionSuministroDao dao = new CompensacionSuministroDaoImpl();
        
    	CompensacionSuministro compensacionSuministro = null;
        
        try {
        	dao.ReadByCod(codRelevador);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Compensacion Suministro."
		}
        return compensacionSuministro;
    }

    public void update(CompensacionSuministro compensacionSuministro) {
    	CompensacionSuministroDao dao = new CompensacionSuministroDaoImpl();

        try {
        	dao.update(compensacionSuministro);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long comSuministroId) {
    	CompensacionSuministroDao dao = new CompensacionSuministroDaoImpl();
        
        try {
        	dao.delete(comSuministroId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    
	public List<CompensacionSuministro> getAllRows() {
		CompensacionSuministroDao dao = new CompensacionSuministroDaoImpl();
		return dao.getAllRows();
	}
	
	public CompensacionSuministro getAllRows1(String numSumAfectado,String semestre) {
		CompensacionSuministroDao dao = new CompensacionSuministroDaoImpl();
		return dao.getAllRows1(numSumAfectado,semestre);
	}
    
	 public List<CompensacionSuministro> getAllComp(String ano, String semestre){
		 CompensacionSuministroDao dao = new CompensacionSuministroDaoImpl();
	    	return dao.getAllComp(ano, semestre);
	    	
	    } 
}
