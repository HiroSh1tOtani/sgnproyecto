package pe.com.calidad.suministro.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;



import pe.com.calidad.suministro.dao.ClientexSistemaDao;
import pe.com.calidad.suministro.dao.ClientexSistemaDaoImpl;
import pe.com.calidad.suministro.entity.ClientexSistema;
import pe.com.indra.calidad.entity.PeriodoMedicion;

/**
 *
 * @author Luis
 */
public class ClientexSistemaService {

    public void create(ClientexSistema clientexSistema) {
    	ClientexSistemaDao dao = new ClientexSistemaDaoImpl();
        
        try {
        	dao.create(clientexSistema);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear ClientexSistema.", e);
		}
        
    }

    public ClientexSistema ReadById(long cliexSistemaId) {
    	ClientexSistemaDao dao = new ClientexSistemaDaoImpl();
    	ClientexSistema clientexSistema = null;
        
        try {
        	clientexSistema = dao.ReadById(cliexSistemaId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); 
		}
        
        return clientexSistema;
    }

    public void update(ClientexSistema clientexSistema) {
    	ClientexSistemaDao dao = new ClientexSistemaDaoImpl();
        
        try {
        	dao.update(clientexSistema);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public void delete(long cliexSistemaId) {
    	ClientexSistemaDao dao = new ClientexSistemaDaoImpl();
        
        try {
        	dao.delete(cliexSistemaId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public PeriodoMedicion selectPeriodo(long cliexSistemaId) {
    	ClientexSistemaDao dao = new ClientexSistemaDaoImpl();
        return dao.selectPeriodo(cliexSistemaId);
    }
    
    
    public List<ClientexSistema> getAllRows(){
    	ClientexSistemaDao dao = new ClientexSistemaDaoImpl();
    	return dao.getAllRows();
    }  
    
    public List<ClientexSistema> getAllRows1(){
    	ClientexSistemaDao dao = new ClientexSistemaDaoImpl();
    	return dao.getAllRows();
    }   
    
    public List<ClientexSistema> getAllRows1(long perMedicionId){
    	ClientexSistemaDao dao = new ClientexSistemaDaoImpl();
    	return dao.getAllRows1(perMedicionId);
    } 
    
  
    
    
}
