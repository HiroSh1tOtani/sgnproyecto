package pe.com.calidad.suministro.service;

import java.util.List;

import org.hibernate.HibernateException;

import pe.com.calidad.suministro.dao.RelevadorRechazoCargaDao;
import pe.com.calidad.suministro.dao.RelevadorRechazoCargaDaoImpl;
import pe.com.calidad.suministro.entity.RelevadorRechazoCarga;
import pe.com.indra.calidad.dao.MedicionDao;
import pe.com.indra.calidad.dao.MedicionDaoImpl;
import pe.com.indra.calidad.entity.Medicion;


/**
 *
 * @author Luis
 */
public class RelevadorRechazoCargaService {

    public void create(RelevadorRechazoCarga relevadorRechazoCarga) {
    	RelevadorRechazoCargaDao dao = new RelevadorRechazoCargaDaoImpl();
        
        try {
        	dao.create(relevadorRechazoCarga);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Relevador RechazoCarga.", e);
		}
        
    }

    public RelevadorRechazoCarga ReadById(long relRechazoId) {
    	RelevadorRechazoCargaDao dao = new RelevadorRechazoCargaDaoImpl();
    	RelevadorRechazoCarga relevadorRechazoCarga = null;
        
        try {
        	relevadorRechazoCarga = dao.ReadById(relRechazoId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); 
		}
        
        return relevadorRechazoCarga;
    }

    public void update(RelevadorRechazoCarga relevadorRechazoCarga) {
    	RelevadorRechazoCargaDao dao = new RelevadorRechazoCargaDaoImpl();
        
        try {
        	dao.update(relevadorRechazoCarga);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public void delete(long relRechazoId) {
    	RelevadorRechazoCargaDao dao = new RelevadorRechazoCargaDaoImpl();
        
        try {
        	dao.delete(relRechazoId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

     
    public List<RelevadorRechazoCarga> getAllRows(){
    	RelevadorRechazoCargaDao dao = new RelevadorRechazoCargaDaoImpl();
    	return dao.getAllRows();
    }  
    
    public List<RelevadorRechazoCarga> getAllRows1(){
    	RelevadorRechazoCargaDao dao = new RelevadorRechazoCargaDaoImpl();
    	return dao.getAllRows();
    }   
    
    public List<RelevadorRechazoCarga> getBusquedaxAnoSemestre(String ano,String semestre) {
    	RelevadorRechazoCargaDao dao = new RelevadorRechazoCargaDaoImpl();
    	List<RelevadorRechazoCarga> relevadorRechazoCargas=null;
    	try {
    		relevadorRechazoCargas = dao.getBusquedaxAnoSemestre(ano, semestre);
			
		} catch (HibernateException e) {
			throw new HibernateException("No se puede encontrar relevadores para el ano y semestre.", e);
		}
    	  	
    	return relevadorRechazoCargas;
    	
    	
    }
   
}
