package pe.com.calidad.suministro.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;



import pe.com.calidad.suministro.dao.Indicador074Dao;
import pe.com.calidad.suministro.dao.Indicador074DaoImpl;
import pe.com.calidad.suministro.entity.Indicador074;
import pe.com.indra.calidad.entity.PeriodoMedicion;

/**
 *
 * @author Luis
 */
public class Indicador074Service {

    public void create(Indicador074 indicador074) {
    	Indicador074Dao dao = new Indicador074DaoImpl();
        
        try {
        	dao.create(indicador074);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Indicador074.", e);
		}
        
    }

    public Indicador074 ReadById(long calIndicadorId) {
    	Indicador074Dao dao = new Indicador074DaoImpl();
    	Indicador074 indicador074 = null;
        
        try {
        	indicador074 = dao.ReadById(calIndicadorId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); 
		}
        
        return indicador074;
    }

    public void update(Indicador074 indicador074) {
    	Indicador074Dao dao = new Indicador074DaoImpl();
        
        try {
        	dao.update(indicador074);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public void delete(long calIndicadorId) {
    	Indicador074Dao dao = new Indicador074DaoImpl();
        
        try {
        	dao.delete(calIndicadorId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public PeriodoMedicion selectPeriodo(long calIndicadorId) {
    	Indicador074Dao dao = new Indicador074DaoImpl();
        return dao.selectPeriodo(calIndicadorId);
    }
    
    
    public List<Indicador074> getAllRows(){
    	Indicador074Dao dao = new Indicador074DaoImpl();
    	return dao.getAllRows();
    }  
    
    public List<Indicador074> getAllRows1(){
    	Indicador074Dao dao = new Indicador074DaoImpl();
    	return dao.getAllRows();
    }   
    
    public List<Indicador074> getAllRows1(long perMedicionId){
    	Indicador074Dao dao = new Indicador074DaoImpl();
    	return dao.getAllRows1(perMedicionId);
    } 
    
  
    
    
}
