package pe.com.calidad.suministro.service;

import java.util.List;

import org.hibernate.HibernateException;

import pe.com.calidad.suministro.dao.ParametroAnualDao;//RelevadorRechazoCargaDao;
import pe.com.calidad.suministro.dao.ParametroAnualDaoImpl;//RelevadorRechazoCargaDaoImpl;
import pe.com.calidad.suministro.entity.ParametroAnual;//RelevadorRechazoCarga;



/**
 *
 * @author Luis
 */
public class ParametroAnualService {

    public void create(ParametroAnual parametroAnual) {
    	ParametroAnualDao dao = new ParametroAnualDaoImpl();
        
        try {
        	dao.create(parametroAnual);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Parametro Anual.", e);
		}
        
    }

    public ParametroAnual ReadById(long parAnualId) {
    	ParametroAnualDao dao = new ParametroAnualDaoImpl();
    	ParametroAnual parametroAnual = null;
        
        try {
        	parametroAnual = dao.ReadById(parAnualId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); 
		}
        
        return parametroAnual;
    }

    public void update(ParametroAnual parametroAnual) {
    	ParametroAnualDao dao = new ParametroAnualDaoImpl();
        
        try {
        	dao.update(parametroAnual);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public void delete(long parAnualId) {
    	ParametroAnualDao dao = new ParametroAnualDaoImpl();
        
        try {
        	dao.delete(parAnualId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

     
    public List<ParametroAnual> getAllRows(){
    	ParametroAnualDao dao = new ParametroAnualDaoImpl();
    	return dao.getAllRows();
    }  
    
       
    public List<ParametroAnual> getBusquedaxAno(String ano) {
    	ParametroAnualDao dao = new ParametroAnualDaoImpl();
    	List<ParametroAnual> parametrosAnual=null;
    	try {
    		parametrosAnual = dao.getBusquedaxAno(ano);
			
		} catch (HibernateException e) {
			throw new HibernateException("No se puede encontrar ParametroAnual para el a�o.", e);
		}
    	  	
    	return parametrosAnual;
    	
    	
    }
   
}
