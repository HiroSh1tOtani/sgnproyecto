package pe.com.calidad.suministro.service;

import java.util.List;

import org.hibernate.HibernateException;

import pe.com.calidad.suministro.dao.CargoTarifaDao;
import pe.com.calidad.suministro.dao.CargoTarifaDaoImpl;
import pe.com.calidad.suministro.entity.CargoTarifa;
import pe.com.calidad.suministro.entity.Pliego;

/**
 *
 * @author Luis
 */
public class CargoTarifaService {

    public void create(CargoTarifa cargoTarifa) {
    	CargoTarifaDao dao = new CargoTarifaDaoImpl();
        
        try {
        	dao.create(cargoTarifa);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Cargo Tarifa.", e);
		}
        
    }

    public CargoTarifa ReadById(long carTarifaId) {
    	CargoTarifaDao dao = new CargoTarifaDaoImpl();
    	CargoTarifa cargoTarifa = null;
        
        try {
        	cargoTarifa = dao.ReadById(carTarifaId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); 
		}
        
        return cargoTarifa;
    }

    public void update(CargoTarifa cargoTarifa) {
    	CargoTarifaDao dao = new CargoTarifaDaoImpl();
        
        try {
        	dao.update(cargoTarifa);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public void delete(long carTarifaId) {
    	CargoTarifaDao dao = new CargoTarifaDaoImpl();
        
        try {
        	dao.delete(carTarifaId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public Pliego selectPliego(long carTarifaId) {
    	CargoTarifaDao dao = new CargoTarifaDaoImpl();
        return dao.selectPliego(carTarifaId);
    }
    
    
    public List<CargoTarifa> getAllRows(){
    	CargoTarifaDao dao = new CargoTarifaDaoImpl();
    	return dao.getAllRows();
    }  
    
    public List<CargoTarifa> getAllRows1(){
    	CargoTarifaDao dao = new CargoTarifaDaoImpl();
    	return dao.getAllRows();
    }   
    
    public List<CargoTarifa> getAllRows1(long pliegoId){
    	CargoTarifaDao dao = new CargoTarifaDaoImpl();
    	return dao.getAllRows1(pliegoId);
    } 
    
  
    
    
}
