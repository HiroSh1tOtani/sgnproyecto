package pe.com.calidad.suministro.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;










import pe.com.calidad.suministro.dao.InterrupcionDao;
import pe.com.calidad.suministro.dao.InterrupcionDaoImpl;
import pe.com.calidad.suministro.entity.SuministroAfectado;
import pe.com.calidad.suministro.entity.ModalidadDeteccion;
//import pe.com.calidad.suministro.entity.Periodo;
import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.entity.VAnexoIDetail;
import pe.com.calidad.suministro.entity.CausaInterrupcion;
import pe.com.calidad.suministro.entity.TipoInterrupcion;
import pe.com.calidad.suministro.entity.TipoNotificacion;
import pe.com.indra.calidad.dao.MedicionDao;
import pe.com.indra.calidad.dao.MedicionDaoImpl;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.PeriodoMedicion;

/**
 *
 * @author Luis
 */
public class InterrupcionService {

    public void create(Interrupcion interrupcion) {
    	InterrupcionDao dao = new InterrupcionDaoImpl();
        
        try {
        	dao.create(interrupcion);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Interrupcion.", e);
		}
        
    }

    public Interrupcion ReadById(long interrupcionId) {
    	InterrupcionDao dao = new InterrupcionDaoImpl();
    	Interrupcion interrupcion = null;
        
        try {
        	interrupcion = dao.ReadById(interrupcionId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Medici�n."
		}
        
        return interrupcion;
    }

    public void update(Interrupcion interrupcion) {
    	InterrupcionDao dao = new InterrupcionDaoImpl();
        
        try {
        	dao.update(interrupcion);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public void delete(long interrupcionId) {
    	InterrupcionDao dao = new InterrupcionDaoImpl();
        
        try {
        	dao.delete(interrupcionId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public PeriodoMedicion selectPeriodo(long interrupcionId) {
    	InterrupcionDao dao = new InterrupcionDaoImpl();
        return dao.selectPeriodo(interrupcionId);
    }
    

    public CausaInterrupcion selectCausaInterrupcion(long interrupcionId) {
    	InterrupcionDao dao = new InterrupcionDaoImpl();
        return dao.selectCausaInterrupcion(interrupcionId);
    }

    public ModalidadDeteccion selectModalidadDeteccion(long interrupcionId) {
    	InterrupcionDao dao = new InterrupcionDaoImpl();
        return dao.selectModalidadDeteccion(interrupcionId);
    }

    public TipoNotificacion selectTipoNotificacion(long interrupcionId) {
    	InterrupcionDao dao = new InterrupcionDaoImpl();
        return dao.selectTipoNotificacion(interrupcionId);
    }

    public TipoInterrupcion selectTipoInterrupcion(long interrupcionId) {
    	InterrupcionDao dao = new InterrupcionDaoImpl();
        return dao.selectTipoInterrupcion(interrupcionId);
    }

    public Set<SuministroAfectado> selectSuministroAfectado(long interrupcionId) {
    	InterrupcionDao dao = new InterrupcionDaoImpl();
        return dao.selectSuministroAfectado(interrupcionId);
    }
    
    public List<Interrupcion> getAllRows1(){
    	InterrupcionDao dao = new InterrupcionDaoImpl();
    	return dao.getAllRows();
    }   
    
    public List<Interrupcion> getAllRows1(long perMedicionId){
    	InterrupcionDao dao = new InterrupcionDaoImpl();
    	return dao.getAllRows1(perMedicionId);
    } 
    
    public List<VAnexoIDetail> getAnexo01Prox074Detail(long perMedicionId){
    	InterrupcionDao dao = new InterrupcionDaoImpl();
    	return dao.getAnexo01Prox074Detail(perMedicionId);
    }
    
    public List<Interrupcion> getAllRows(){
    	InterrupcionDao dao = new InterrupcionDaoImpl();
    	return dao.getAllRows();
    }  
    
    public List<Interrupcion> getAllFueU(String ano,String semestre){
    	InterrupcionDao dao = new InterrupcionDaoImpl();
    	return dao.getAllFueU(ano,semestre);
    	
    } 
    
    public List<Interrupcion> getAllFueR(String ano,String semestre){
    	InterrupcionDao dao = new InterrupcionDaoImpl();
    	return dao.getAllFueR(ano,semestre);
    } 
      
}
