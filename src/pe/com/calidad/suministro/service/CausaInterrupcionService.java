package pe.com.calidad.suministro.service;

import java.util.Set;
import java.util.List;

import org.hibernate.HibernateException;

import pe.com.calidad.suministro.dao.CausaInterrupcionDao;
import pe.com.calidad.suministro.dao.CausaInterrupcionDaoImpl;
import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.entity.CausaInterrupcion;

/**
 *
 * @author Lucho
 */
public class CausaInterrupcionService {

    public void create(CausaInterrupcion causaInterrupcion) {
    	CausaInterrupcionDao dao = new CausaInterrupcionDaoImpl();

        try {
        	dao.create(causaInterrupcion);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Causa Interrupcion.", e);
		}
        
    }

    public CausaInterrupcion ReadById(long cauInterrupcionId) {
    	CausaInterrupcionDao dao = new CausaInterrupcionDaoImpl();
        
    	CausaInterrupcion causaInterrupcion = null;
        
        try {
        	causaInterrupcion = dao.ReadById(cauInterrupcionId);
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Causa Interrupcion."
		}
        
        return causaInterrupcion;
        
    }

    public CausaInterrupcion ReadByCod(String codCauInterrupcion) {
    	CausaInterrupcionDao dao = new CausaInterrupcionDaoImpl();
    	CausaInterrupcion causaInterrupcion = null;
        
        try {
        	causaInterrupcion = dao.ReadByCod(codCauInterrupcion);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Causa Interrupcion."
		}
        return causaInterrupcion;
        
    }

    public void update(CausaInterrupcion causaInterrupcion) {
    	CausaInterrupcionDao dao = new CausaInterrupcionDaoImpl();
         
        try {
        	 dao.update(causaInterrupcion);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long cauInterrupcionId) {
    	CausaInterrupcionDao dao = new CausaInterrupcionDaoImpl();
        
        dao.delete(cauInterrupcionId);
    }

    public Set<Interrupcion> selectInterrupcion(long cauInterrupcionId) {
    	CausaInterrupcionDao dao = new CausaInterrupcionDaoImpl();
        return dao.selectInterrupcion(cauInterrupcionId);
    }
    
    public List<CausaInterrupcion>getAllRows(){
    	CausaInterrupcionDao dao = new CausaInterrupcionDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<CausaInterrupcion>getAllRows1(){
    	CausaInterrupcionDao dao = new CausaInterrupcionDaoImpl();
    	return dao.getAllRows1();
    }
}


