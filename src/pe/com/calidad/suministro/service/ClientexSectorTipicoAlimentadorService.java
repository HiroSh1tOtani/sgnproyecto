package pe.com.calidad.suministro.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import pe.com.calidad.suministro.dao.ClientexSectorTipicoAlimentadorDao;
import pe.com.calidad.suministro.dao.ClientexSectorTipicoAlimentadorDaoImpl;
import pe.com.calidad.suministro.entity.ClientexAlimentador;
import pe.com.calidad.suministro.entity.ClientexAlimentadorSistema;
import pe.com.calidad.suministro.entity.ClientexSectorTipico;
import pe.com.indra.calidad.util.Sql;

/**
 *
 * @author Luis
 */
public class ClientexSectorTipicoAlimentadorService {
    
    public List<ClientexSectorTipico> getClientexSectorTipico(long perMedicionId){
    	ClientexSectorTipicoAlimentadorDao dao = new ClientexSectorTipicoAlimentadorDaoImpl();
    	return dao.getClientexSectorTipico(perMedicionId);
    } 
    
    public List<ClientexAlimentadorSistema> getClientexAlimentador(long perMedicionId){
    	//ClientexSectorTipicoAlimentadorDao dao = new ClientexSectorTipicoAlimentadorDaoImpl();
    	
    	Sql sql = new Sql("CALIDAD");
    	
    	String s = "SELECT DISTINCT clal.clixalimentador_id, CLAL.ALIMENTADOR, clal.sum_cantidad, SIEL.LABEL, SIEL.TYPICAL_SECTOR FROM CAL_CLIENTEXALIMENTADOR CLAL " + 
    			"INNER JOIN ZNET_MV_LINE ALIM ON CLAL.ALIMENTADOR=ALIM.LABEL " + 
    			"INNER JOIN zeus_electric_sets SIEL ON ALIM.ELECTRIC_SET_ID=SIEL.ID " + 
    			"WHERE CLAL.PK_PER_MEDICION_ID=" + perMedicionId + " " +
    			"ORDER BY SIEL.LABEL";
		
        List<Object[]> listObject = sql.consulta(s, false);
		
        List<ClientexAlimentadorSistema> result = new ArrayList<>();
        
        if(listObject!=null) {
        	for (Object[] obje: listObject) {
            	ClientexAlimentadorSistema obj = new ClientexAlimentadorSistema((BigDecimal) obje[0], (String) obje[1], (BigDecimal) obje[2], (String) obje[3], (BigDecimal) obje[4]);
            	result.add(obj);
            }
        }
        
    	return result;
    }
}
