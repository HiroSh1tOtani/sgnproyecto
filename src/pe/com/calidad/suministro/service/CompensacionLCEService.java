package pe.com.calidad.suministro.service;

import java.util.List;

import org.hibernate.HibernateException;

import pe.com.calidad.suministro.dao.CompensacionLCEDao;
import pe.com.calidad.suministro.dao.CompensacionLCEDaoImpl;
import pe.com.calidad.suministro.entity.CompensacionLCE;


/**
 *
 * @author Luis
 */
public class CompensacionLCEService {

 

     
    public List<CompensacionLCE> getAllRows(){
    	CompensacionLCEDao dao = new CompensacionLCEDaoImpl();
    	return dao.getAllRows();
    }  
    
     
    public List<CompensacionLCE> getAllRows1(String ano,String mes) {
    	CompensacionLCEDao dao = new CompensacionLCEDaoImpl();
    	List<CompensacionLCE> compensacionLCE=null;
    	try {
    		compensacionLCE = dao.getAllRows1(ano, mes);
			
		} catch (HibernateException e) {
			throw new HibernateException("No se puede encontrar compensacionLCE para el ano y mes.", e);
		}
    	  	
    	return compensacionLCE;
    	
    	
    }
   
}
