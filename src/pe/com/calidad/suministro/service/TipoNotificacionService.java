package pe.com.calidad.suministro.service;

import java.util.Set;
import java.util.List;

import org.hibernate.HibernateException;

import pe.com.calidad.suministro.dao.TipoNotificacionDao;//CausaInterrupcionDao;
import pe.com.calidad.suministro.dao.TipoNotificacionDaoImpl;//CausaInterrupcionDaoImpl;
import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.entity.TipoNotificacion;//CausaInterrupcion;

/**
 *
 * @author Lucho
 */
public class TipoNotificacionService {

    public void create(TipoNotificacion tipoNotificacion) {
    	TipoNotificacionDao dao = new TipoNotificacionDaoImpl();

        try {
        	dao.create(tipoNotificacion);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Tipo Notificacion.", e);
		}
        
    }

    public TipoNotificacion ReadById(long tipNotificacionId) {
    	TipoNotificacionDao dao = new TipoNotificacionDaoImpl();
        
    	TipoNotificacion tipoNotificacion = null;
        
        try {
        	tipoNotificacion = dao.ReadById(tipNotificacionId);
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Tipo Notificacion."
		}
        
        return tipoNotificacion;
        
    }

    public TipoNotificacion ReadByCod(String codTipNotificacion) {
    	TipoNotificacionDao dao = new TipoNotificacionDaoImpl();
    	TipoNotificacion tipoNotificacion = null;
        
        try {
        	tipoNotificacion = dao.ReadByCod(codTipNotificacion);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Tipo Notificacion."
		}
        return tipoNotificacion;
        
    }

    public void update(TipoNotificacion tipoNotificacion) {
    	TipoNotificacionDao dao = new TipoNotificacionDaoImpl();
         
        try {
        	 dao.update(tipoNotificacion);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long tipNotificacionId) {
    	TipoNotificacionDao dao = new TipoNotificacionDaoImpl();
        
        dao.delete(tipNotificacionId);
    }

      
    public List<TipoNotificacion>getAllRows(){
    	TipoNotificacionDao dao = new TipoNotificacionDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<TipoNotificacion>getAllRows1(){
    	TipoNotificacionDao dao = new TipoNotificacionDaoImpl();
    	return dao.getAllRows();
    }
}


