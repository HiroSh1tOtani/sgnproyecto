package pe.com.calidad.suministro.service;

import java.util.Set;
import java.util.List;

import org.hibernate.HibernateException;

import pe.com.calidad.suministro.dao.TipoInterrupcionDao;
import pe.com.calidad.suministro.dao.TipoInterrupcionDaoImpl;
import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.entity.TipoInterrupcion;

/**
 *
 * @author Lucho
 */
public class TipoInterrupcionService {

    public void create(TipoInterrupcion tipoInterrupcion) {
    	TipoInterrupcionDao dao = new TipoInterrupcionDaoImpl();

        try {
        	dao.create(tipoInterrupcion);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Tipo de Interrupcion.", e);
		}
        
    }

    public TipoInterrupcion ReadById(long tipInterrupcionId) {
    	TipoInterrupcionDao dao = new TipoInterrupcionDaoImpl();
        
    	TipoInterrupcion tipoInterrupcion = null;
        
        try {
        	tipoInterrupcion = dao.ReadById(tipInterrupcionId);
		} catch (HibernateException e) {
			e.printStackTrace();
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Tipo de Interrupcion."
		}
        
        return tipoInterrupcion;
        
    }

    public TipoInterrupcion ReadByCod(String codTipInterrupcion) {
    	TipoInterrupcionDao dao = new TipoInterrupcionDaoImpl();
    	TipoInterrupcion tipoInterrupcion = null;
        
        try {
        	tipoInterrupcion = dao.ReadByCod(codTipInterrupcion);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); //"No se puede leer Tipo de Interrupcion."
		}
        return tipoInterrupcion;
        
    }

    public void update(TipoInterrupcion tipoInterrupcion) {
    	TipoInterrupcionDao dao = new TipoInterrupcionDaoImpl();
         
        try {
        	 dao.update(tipoInterrupcion);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
    }

    public void delete(long tipInterrupcionId) {
    	TipoInterrupcionDao dao = new TipoInterrupcionDaoImpl();
        
        dao.delete(tipInterrupcionId);
    }

    public Set<Interrupcion> selectInterrupcion(long tipInterrupcionId) {
    	TipoInterrupcionDao dao = new TipoInterrupcionDaoImpl();
        return dao.selectInterrupcion(tipInterrupcionId);
    }
    
    public List<TipoInterrupcion>getAllRows(){
    	TipoInterrupcionDao dao = new TipoInterrupcionDaoImpl();
    	return dao.getAllRows();
    }
    
    public List<TipoInterrupcion>getAllRows1(){
    	TipoInterrupcionDao dao = new TipoInterrupcionDaoImpl();
    	return dao.getAllRows();
    }
}


