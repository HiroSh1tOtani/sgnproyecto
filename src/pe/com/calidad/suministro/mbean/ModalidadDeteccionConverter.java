package pe.com.calidad.suministro.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.calidad.suministro.entity.ModalidadDeteccion;//TipoNotificacion;
import pe.com.calidad.suministro.service.ModalidadDeteccionService;

@FacesConverter(value="modalidadDeteccionConverter",forClass=ModalidadDeteccionConverter.class)

public class ModalidadDeteccionConverter implements Converter{

	@Override
	public ModalidadDeteccion getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long modDeteccionId=new Long(cadena);
		ModalidadDeteccionService service=new ModalidadDeteccionService();
		ModalidadDeteccion modalidadDeteccion=new ModalidadDeteccion();
		modalidadDeteccion=service.ReadById(modDeteccionId);
		System.out.println(modalidadDeteccion.getModDeteccionId());
		return modalidadDeteccion;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			ModalidadDeteccion modalidadDeteccion=(ModalidadDeteccion) objeto;
			//System.out.println("objeto:"+new Long(modalidadDeteccion.getModDeteccionId()).toString());
			return new Long(modalidadDeteccion.getModDeteccionId()).toString();
		}
		return null;
	}


}
