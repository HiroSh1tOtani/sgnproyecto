package pe.com.calidad.suministro.mbean;


import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import pe.com.calidad.suministro.entity.ClientexAlimentador;
import pe.com.calidad.suministro.entity.ClientexAlimentadorSistema;
import pe.com.calidad.suministro.entity.ClientexSectorTipico;
import pe.com.calidad.suministro.entity.ClientexSistema;
import pe.com.indra.calidad.entity.PeriodoMedicion;
import pe.com.indra.calidad.service.PeriodoMedicionService;
import pe.com.indra.calidad.util.ConectaDb;
import pe.com.calidad.suministro.service.ClientexSectorTipicoAlimentadorService;
import pe.com.calidad.suministro.service.ClientexSistemaService;


@ManagedBean(name="clientexSectorTipicoAlimentadorBean")
@SessionScoped
public class ClientexSectorTipicoAlimentadorBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<ClientexSectorTipico> clientexSectorTipicos;
	private List<ClientexAlimentadorSistema> clientexAlimentadores;
	private List<ClientexSistema> clientexSistemas;
	private PeriodoMedicion periodoMedicion;
	private List<SelectItem> itemsPeriodoMedicion1;
	
	private Long totalSectorTipico;
	
	private BigDecimal totalAlimentador;
	
	private Long totalSistemaConSer;
	
	private String mensaje;
	
	@PostConstruct 
	public void init(){
		clientexSectorTipicos = new ArrayList<>();
		clientexAlimentadores = new ArrayList<>();
		clientexSistemas = new ArrayList<>();
	}
	
	public List<ClientexSectorTipico> getClientexSectorTipicos() {
		return clientexSectorTipicos;
	}

	public void setClientexSectorTipicos(List<ClientexSectorTipico> clientexSectorTipicos) {
		this.clientexSectorTipicos = clientexSectorTipicos;
	}

	public List<ClientexAlimentadorSistema> getClientexAlimentadores() {
		return clientexAlimentadores;
	}

	public void setClientexAlimentadores(List<ClientexAlimentadorSistema> clientexAlimentadores) {
		this.clientexAlimentadores = clientexAlimentadores;
	}
	
	public List<ClientexSistema> getClientexSistemas() {
		return clientexSistemas;
	}

	public void setClientexSistemas(List<ClientexSistema> clientexSistemas) {
		this.clientexSistemas = clientexSistemas;
	}

	public Long getTotalSectorTipico() {
		return totalSectorTipico;
	}

	public void setTotalSectorTipico(Long totalSectorTipico) {
		this.totalSectorTipico = totalSectorTipico;
	}

	public BigDecimal getTotalAlimentador() {
		return totalAlimentador;
	}

	public void setTotalAlimentador(BigDecimal totalAlimentador) {
		this.totalAlimentador = totalAlimentador;
	}

	public Long getTotalSistemaConSer() {
		return totalSistemaConSer;
	}

	public void setTotalSistemaConSer(Long totalSistemaConSer) {
		this.totalSistemaConSer = totalSistemaConSer;
	}

	public String ClientexSectorTipicoAlimentador() {
		ClientexSectorTipicoAlimentadorService service=new ClientexSectorTipicoAlimentadorService();
		ClientexSistemaService serviceSistema=new ClientexSistemaService();
		
		if(periodoMedicion.getPerMedicionId() > 0) {
			clientexSectorTipicos= service.getClientexSectorTipico(periodoMedicion.getPerMedicionId());
			clientexAlimentadores= service.getClientexAlimentador(periodoMedicion.getPerMedicionId());
			clientexSistemas = serviceSistema.getAllRows1(periodoMedicion.getPerMedicionId());
			
			totalSectorTipico = 0L;
			for(ClientexSectorTipico obj : clientexSectorTipicos) {
				totalSectorTipico += obj.getSumCantidad();
			}
			
			totalAlimentador = new BigDecimal(0);
			for(ClientexAlimentadorSistema obj : clientexAlimentadores) {
				totalAlimentador = totalAlimentador.add(obj.getSumCantidad());
			}
			
			totalSistemaConSer = 0L;
			for(ClientexSistema obj : clientexSistemas) {
				totalSistemaConSer += obj.getSumCantidad();
			}
			
		}
		return "/zonasegura/interrupcion/clientexSectorTipicoAlimentadorListar?faces-redirect=true";
	}
	
	
	public void  filtroChanged(AjaxBehaviorEvent event) {  
		
		ClientexSectorTipicoAlimentador();
	}
	
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public PeriodoMedicion getPeriodoMedicion() {
		return periodoMedicion;
	}

	public void setPeriodoMedicion(PeriodoMedicion periodoMedicion) {
		this.periodoMedicion = periodoMedicion;
	}


	public List<SelectItem> getItemsPeriodoMedicion1() {
		PeriodoMedicionService service=new PeriodoMedicionService();
		itemsPeriodoMedicion1 = new ArrayList<SelectItem>();
		for(PeriodoMedicion tp:service.getAllRows1()){
			itemsPeriodoMedicion1.add(new SelectItem(tp,tp.getAno()+"-"+tp.getPeriodo()));
		}
		return itemsPeriodoMedicion1;
	}


	public void setItemsPeriodoMedicion1(List<SelectItem> itemsPeriodoMedicion1) {
		this.itemsPeriodoMedicion1 = itemsPeriodoMedicion1;
	}
	
	public String calcularCantidadSuministros(){
		ConectaDb db = new ConectaDb("CALIDAD");		
        Connection cn = db.getConnection();
        CallableStatement cstmt = null;
		try {
		
			cstmt = cn.prepareCall("{CALL CAL_SUMINISTRO_PACKAGE.CALCULAR_CANTIDAD_SEC_TIP_ALIM(?)}"); 
							
			cstmt.setLong(1,periodoMedicion.getPerMedicionId());
							
			cstmt.executeUpdate();
			cstmt.close();
		
		} catch (SQLException e) {
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	            
	    } finally {
	        try {
	            cn.close();
	        } catch (SQLException e) {
				setMensaje(e.getMessage()+":"+e.getCause());
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	        }
	    }
		
		ClientexSectorTipicoAlimentador();
		
		return "/zonasegura/interrupcion/clientexSectorTipicoAlimentadorListar?faces-redirect=true";
	}
	
}
