package pe.com.calidad.suministro.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.calidad.suministro.entity.TipoNotificacion;
import pe.com.calidad.suministro.service.TipoNotificacionService;

@FacesConverter(value="tipoNotificacionConverter",forClass=TipoNotificacionConverter.class)

public class TipoNotificacionConverter implements Converter{

	@Override
	public TipoNotificacion getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long tipNotificacionId=new Long(cadena);
		TipoNotificacionService service=new TipoNotificacionService();
		TipoNotificacion tipoNotificacion=new TipoNotificacion();
		tipoNotificacion=service.ReadById(tipNotificacionId);
		System.out.println(tipoNotificacion.getTipNotificacionId());
		return tipoNotificacion;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			TipoNotificacion tipoNotificacion=(TipoNotificacion) objeto;
			//System.out.println("objeto:"+new Long(tipoNotificacion.getTipNotificacionId()).toString());
			return new Long(tipoNotificacion.getTipNotificacionId()).toString();
		}
		return null;
	}


}
