package pe.com.calidad.suministro.mbean;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.http.HTTPException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.HibernateException;
import org.richfaces.component.UIExtendedDataTable;
import org.richfaces.model.Filter;

import pe.com.calidad.suministro.entity.CompensacionSuministro;
import pe.com.calidad.suministro.entity.Indicador074;
import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.entity.SuministroAfectado;
import pe.com.calidad.suministro.service.CompensacionSuministroService;
import pe.com.calidad.suministro.service.Indicador074Service;
import pe.com.calidad.suministro.service.InterrupcionService;
import pe.com.calidad.suministro.service.SuministroAfectadoService;
import pe.com.indra.calidad.entity.Cuadrilla;
import pe.com.indra.calidad.entity.IntervaloTension;
import pe.com.indra.calidad.entity.Localidad;
import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.PeriodoMedicion;
import pe.com.indra.calidad.entity.Reporte;
import pe.com.indra.calidad.entity.TipoAlimentacion;
import pe.com.indra.calidad.service.CampanaMedicionService;
import pe.com.indra.calidad.service.CuadrillaService;
import pe.com.indra.calidad.service.EmpresaService;
import pe.com.indra.calidad.service.IntervaloTensionService;
import pe.com.indra.calidad.service.LocalidadService;
import pe.com.indra.calidad.service.MedicionService;
import pe.com.indra.calidad.service.TipoAlimentacionService;
import pe.com.indra.calidad.util.ConectaDb;
import pe.com.indra.calidad.util.ModificarExcel;
import pe.com.indra.calidad.util.Sql;
import pe.com.indra.calidad.util.Utilidad;

@ManagedBean(name="suministroAfectadoBean")
@SessionScoped
public class SuministroAfectadoBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	private List<SuministroAfectado> suministrosAfectados;
	private long interrupcionId;
	
	private List<SuministroAfectado> interrupcionxSuministro;
	
	private String numSumAfectadoFilter;
	private String tenSuministroFilter;
	private String codSedFilter;
	
	private SuministroAfectado suministroAfectado;
	

	private String mensaje;
	private String numSumAfectado;
	private Interrupcion interrupcion;

	
	private java.util.Date fecIniIntReal;
	private java.util.Date fecFinIntReal;
	
	private List<SelectItem> itemsSemestre;
	private List<SelectItem> itemsSincronizar;
	private List<SelectItem> itemsTipServicio;
	private String tipoServicioFilter;
	private List<SelectItem> itemsTipServicio1;
	private List<SelectItem> itemsPerteneceSer;
	private String perteneceSerFilter;
	private List<SelectItem> itemsPerteneceSer1;
	private List<SelectItem> itemsLocalidad1;
	private PeriodoMedicion periodoMedicion;
	private long filasTotal;
	private Collection<Object> selection;
	private List<SuministroAfectado> selectionItems = new ArrayList<SuministroAfectado>();	
	
	public Collection<Object> getSelection() {
		return selection;
	}

	public void setSelection(Collection<Object> selection) {
		this.selection = selection;
	}

	public List<SuministroAfectado> getSelectionItems() {
		return selectionItems;
	}

	public void setSelectionItems(List<SuministroAfectado> selectionItems) {
		this.selectionItems = selectionItems;
	}	

	@PostConstruct
	public void init(){
			
		
	}
	
	public String mostrarAfectados(){
		SuministroAfectadoService service = new SuministroAfectadoService();
		String interrupcionIdStr = Utilidad.getParametro("interrupcionId");
		
		if(interrupcionIdStr != null){
			interrupcionId = Long.valueOf(interrupcionIdStr);
			
			InterrupcionService interrupcionService = new InterrupcionService();
			
			interrupcion = interrupcionService.ReadById(interrupcionId);
			
			suministrosAfectados = (List<SuministroAfectado>) service.getAllRows1(Long.valueOf(interrupcionIdStr));
		}else {
			suministrosAfectados = (List<SuministroAfectado>) service.getAllRows1(interrupcionId);
		}
		
		
		
		return "/zonasegura/interrupcion/suministroAfectadoListar?faces-redirect=true";
	}
	
	
	public String downLoadReporteSuministro(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
	    
		Medicion medicion = null; //Cargar
	    String rutaArchivoOut;
	    
	    String rutaUp;
	    
	    String numSumAfectado = Utilidad.getParametro("numSumAfectado");	
	    //interrupcionId = Utilidad.getParametro("interrupcionId");
	    
	    
	    rutaUp = facesContext.getExternalContext().getRealPath("/reportes/ReporteCalidadSuministro.xlsx");
	    
	    /*
	    if(medicion.getTipServicio().equals("U")){
	    	rutaUp = facesContext.getExternalContext().getRealPath("/reportes/ReporteCalidadSuministro.xlsx");
	    }else{
	    	rutaUp = facesContext.getExternalContext().getRealPath("/reportes/ReporteCalidadSuministroRural.xlsx");
	    }
	    */
	    String path = System.getProperty("uploads.folder");
	    String nombreArchivo;
	    
	    System.out.print(path + "\n");
	    
	    
	    nombreArchivo = "Reporte_Calidad_Suministro" + new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsx";
	    
	    rutaArchivoOut = path + nombreArchivo;
	    	    
	    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);

		XSSFSheet objHssfSheet = objHssfWorkbook.getSheetAt(0);
		
		ModificarExcel.setCellValue(objHssfSheet, 9, 3, 1);
		/*
		if(medicion.getTipServicio().equals("U")){
			objHssfSheet = objHssfWorkbook.getSheetAt(0);
		
		}else{
			//colocar aqui lo rural
		}
		*/

		ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
		
		System.out.print(rutaArchivoOut + "\n");
	    
		HttpServletResponse response = null;
		try {
			response =((HttpServletResponse)facesContext.getExternalContext().getResponse());
		} catch (HTTPException e) {
			e.printStackTrace();
		}
		
	    
		System.out.print("ERROR !\n");
	    
	    
	    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
	    System.out.print("ERROR 3!\n");
	    facesContext.responseComplete();
	    System.out.print("ERROR 4!\n");

	    return null;		
	}
	
	void writeOutContent(final HttpServletResponse res, final File content, final String theFilename, String contentType) {
	    if (content == null) {
	    	
	    	return;
	    }
	    try {
	    	
	    	System.out.print("ERROR 2!\n");
	      res.setHeader("Pragma", "no-cache");
	      res.setDateHeader("Expires", 0);
	      res.setContentType(contentType);
	      res.setHeader("Content-disposition", "attachment; filename=" + theFilename);
	      
	      fastChannelCopy(Channels.newChannel(new FileInputStream(content)), Channels.newChannel(res.getOutputStream()));
	    } catch (final IOException e) {
	      
	      
	      e.printStackTrace(); 
	    }
	  }
	 
	  void fastChannelCopy(final ReadableByteChannel src, final WritableByteChannel dest) throws IOException {
	    final ByteBuffer buffer = ByteBuffer.allocateDirect(1000 * 1024);
	    while (src.read(buffer) != -1) {
	      buffer.flip();
	      dest.write(buffer);
	      buffer.compact();
	    }
	    buffer.flip();
	    while (buffer.hasRemaining()){
	      dest.write(buffer);
	    }
	  }
	
	  public String InterrupcionxSuministro() {
			SuministroAfectadoService service=new SuministroAfectadoService();
			
			try {
				
				if(!numSumAfectado.trim().equals("")) {
					interrupcionxSuministro= service.getInterrupcionxSuministro(numSumAfectado);		
				} else {
					interrupcionxSuministro = null;
				}
					
				
			} catch (HibernateException e) {
				setMensaje(e.getMessage());
			}
			
			
			return "/zonasegura/interrupcion/interrupcionxSuministroListar?faces-redirect=true";
		}  
	  
	  
	  
	  
	public List<SuministroAfectado> getSuministrosAfectados() {
		
		
		return suministrosAfectados;
	}

	public void setSuministrosAfectados(List<SuministroAfectado> suministrosAfectados) {
		this.suministrosAfectados = suministrosAfectados;
	}

	public long getInterrupcionId() {
		return interrupcionId;
	}

	public void setInterrupcionId(long interrupcionId) {
		this.interrupcionId = interrupcionId;
	}

	public String getNumSumAfectadoFilter() {
		return numSumAfectadoFilter;
	}

	public void setNumSumAfectadoFilter(String numSumAfectadoFilter) {
		this.numSumAfectadoFilter = numSumAfectadoFilter;
	}

	public String getTenSuministroFilter() {
		return tenSuministroFilter;
	}

	public void setTenSuministroFilter(String tenSuministroFilter) {
		this.tenSuministroFilter = tenSuministroFilter;
	}

	public String getCodSedFilter() {
		return codSedFilter;
	}

	public void setCodSedFilter(String codSedFilter) {
		this.codSedFilter = codSedFilter;
	}

	public List<SuministroAfectado> getInterrupcionxSuministro() {
		return interrupcionxSuministro;
	}

	public void setInterrupcionxSuministro(List<SuministroAfectado> interrupcionxSuministro) {
		this.interrupcionxSuministro = interrupcionxSuministro;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public String getNumSumAfectado() {
		return numSumAfectado;
	}

	public void setNumSumAfectado(String numSumAfectado) {
		this.numSumAfectado = numSumAfectado;
	}
	
	
	public Interrupcion getInterrupcion() {
		return interrupcion;
	}

	public void setInterrupcion(Interrupcion interrupcion) {
		this.interrupcion = interrupcion;
	}
	
	
	public java.util.Date getFecIniIntReal() {
		return fecIniIntReal;
	}

	public void setFecIniIntReal(java.util.Date fecIniIntReal) {
		this.fecIniIntReal = fecIniIntReal;
	}

	public java.util.Date getFecFinIntReal() {
		return fecFinIntReal;
	}

	public void setFecFinIntReal(java.util.Date fecFinIntReal) {
		this.fecFinIntReal = fecFinIntReal;
	}

	
	public SuministroAfectado getSuministroAfectado() {
		return suministroAfectado;
	}

	public void setSuministroAfectado(SuministroAfectado suministroAfectado) {
		this.suministroAfectado = suministroAfectado;
	}
	
	public List<SelectItem> getItemsSincronizar() {
		itemsSincronizar=new ArrayList<SelectItem>();
		itemsSincronizar.add(new SelectItem("SI","SI"));
		itemsSincronizar.add(new SelectItem("NO","NO"));
		return itemsSincronizar;
	}

	public void setItemsSincronizar(List<SelectItem> itemsSincronizar) {
		this.itemsSincronizar = itemsSincronizar;
	}
	
	public List<SelectItem> getItemsSemestre() {
		itemsSemestre=new ArrayList<SelectItem>();
		itemsSemestre.add(new SelectItem("S1","SEMESTRE 01"));
		itemsSemestre.add(new SelectItem("S2","SEMESTRE 02"));
		return itemsSemestre;
	}

	public void setItemsSemestre(List<SelectItem> itemsSemestre) {
		this.itemsSemestre = itemsSemestre;
	}
	
	
	public PeriodoMedicion getPeriodoMedicion() {
		return periodoMedicion;
	}

	public void setPeriodoMedicion(PeriodoMedicion periodoMedicion) {
		this.periodoMedicion = periodoMedicion;
	}

	public long getFilasTotal() {
		return filasTotal;
	}

	public void setFilasTotal(long filasTotal) {
		this.filasTotal = filasTotal;
	}

	public String prepararCrear(){
		//Esto funciona siempre que el bean tenga ambito de sesion 
		setSuministroAfectado(new SuministroAfectado());
		getSuministroAfectado().setEstado("1");
		fecIniIntReal = null;
		fecFinIntReal = null;
		
		return "/zonasegura/interrupcion/suministroAfectadoCrear?faces-redirect=true";
	}
	
	public String prepararEditar(){
		
		
		try {
			cargarSuministroAfectadoActual();
			return "/zonasegura/interrupcion/suministroAfectadoEditar?faces-redirect=true";
		} catch (HibernateException e) {
			setMensaje(e.getMessage());
			return "/zonasegura/interrupcion/suministroAfectadoListar?faces-redirect=true";
		}
	
		
	}
	
	public String prepararVer(){
			
		try {
			cargarSuministroAfectadoActual();
			return "/zonasegura/interrupcion/suministroAfectadoVer?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/interrupcion/suministroAfectadoListar?faces-redirect=true";
		}
	}
	
	
	public String salvar(){
				
		SuministroAfectadoService service=new SuministroAfectadoService();
		
		try {
			
			if(fecIniIntReal!=null){
				suministroAfectado.setFecIniIntReal(new java.sql.Timestamp(fecIniIntReal.getTime()));
			}else {
				suministroAfectado.setFecIniIntReal(null);
			}
			if(fecFinIntReal!=null){
				suministroAfectado.setFecFinIntReal(new java.sql.Timestamp(fecFinIntReal.getTime()));
			}else {
				suministroAfectado.setFecFinIntReal(null);
			}
			
			if (suministroAfectado.getInterrupcion()==null){
				//mvargas
				suministroAfectado.setInterrupcion(new Interrupcion());
				suministroAfectado.getInterrupcion().setInterrupcionId(interrupcionId);
			}
			service.create(suministroAfectado);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		
		mostrarAfectados();
		
		return "/zonasegura/interrupcion/suministroAfectadoListar?faces-redirect=true";
	}
	
	public String actualizar(){
	
		SuministroAfectadoService service=new SuministroAfectadoService();
		
		try {
			if(fecIniIntReal!=null){
				suministroAfectado.setFecIniIntReal(new java.sql.Timestamp(fecIniIntReal.getTime()));
			}else {
				suministroAfectado.setFecIniIntReal(null);
			}
			if(fecFinIntReal!=null){
				suministroAfectado.setFecFinIntReal(new java.sql.Timestamp(fecFinIntReal.getTime()));
			}else {
				suministroAfectado.setFecFinIntReal(null);
			}
			service.update(suministroAfectado);
			
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		
		mostrarAfectados();
		
		return "/zonasegura/interrupcion/suministroAfectadoListar?faces-redirect=true";
	}
	
	public String eliminar(){
		String suministroAfectadoId=Utilidad.getParametro("sumAfectadoId");
		SuministroAfectadoService service=new SuministroAfectadoService();

		
		try {
			suministroAfectado=service.ReadById(new Long(suministroAfectadoId));
			suministroAfectado.setEstado("0");
			service.update(suministroAfectado);
		} catch (HibernateException e) {
			
			
			System.out.println(e.getMessage()+":"+e.getCause());
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			
		} catch (Exception e) {
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		
		return "/zonasegura/interrupcion/suministroAfectadoListar?faces-redirect=true";
	}

	public void cargarSuministroAfectadoActual(){
		
		String sumAfectadoId=Utilidad.getParametro("sumAfectadoId");
		SuministroAfectadoService service=new SuministroAfectadoService();
		
		try {
			
			suministroAfectado=service.ReadById(new Long(sumAfectadoId));
			
			if(suministroAfectado.getFecIniIntReal()!=null) {
				fecIniIntReal = new java.util.Date( suministroAfectado.getFecIniIntReal().getTime());				
			}else {
				fecIniIntReal = null;
			}
			
			if(suministroAfectado.getFecFinIntReal()!=null) {
				fecFinIntReal = new java.util.Date( suministroAfectado.getFecFinIntReal().getTime());				
			}else {
				fecFinIntReal = null;
			}
			
			
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			e.printStackTrace();
			
			throw new HibernateException("No se puede cargar Suministro Afectado.", e);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Ocurrio un error", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			e.printStackTrace();
		}
		
	}
	
	public void handleEvent(ComponentSystemEvent event){
		UIExtendedDataTable dataTable = (UIExtendedDataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("idform-tableSuministroAfectado");
		
		filasTotal = dataTable.getRowCount();		
		
		System.out.println(dataTable.getRowCount() + "-" + dataTable.getClientRows() + "-" + dataTable.getChildCount() + "-" + dataTable.getRows() + "-" + dataTable.getResourceBundleMap().size());

	}
	
	public void actualizaFilas2(AjaxBehaviorEvent event){
		//UIExtendedDataTable dataTable = (UIExtendedDataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("idform-tableMedicion");
		UIExtendedDataTable dataTable = (UIExtendedDataTable) event.getComponent();
		filasTotal = dataTable.getRowCount();		
		
		Object originalKey = dataTable.getRowKey();
		selectionItems.clear();
		for (Object selectionKey : selection) {
			dataTable.setRowKey(selectionKey);
			if (dataTable.isRowAvailable()) {
				selectionItems.add((SuministroAfectado) dataTable.getRowData());
			}
		}		
		
		dataTable.setRowKey(originalKey);
		
		System.out.println(dataTable.getRowCount() + "-" + dataTable.getClientRows() + "-" + dataTable.getChildCount() + "-" + dataTable.getRows() + "-" + dataTable.getResourceBundleMap().size());

	}

	public String exportarSuministrosAfec(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
	    
	    String rutaArchivoOut;
	    String rutaUp;
	    rutaUp = facesContext.getExternalContext().getRealPath("/reportes/AnexosunAfecInter.xlsx");
	    String path = System.getProperty("uploads.folder");
	    String nombreArchivo;
	    
	    System.out.print(path + "\n");
	    
	    	    
	    nombreArchivo = "SuministroAfectado" + "_" + getInterrupcion().getCodInterrupcion()  + "_"+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsx";
	    rutaArchivoOut = path + nombreArchivo;
	    	    
	    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);

		XSSFSheet objHssfSheet = null;
		
		try  {
	    
	    SuministroAfectadoService service = new SuministroAfectadoService();
	    
	    objHssfSheet = objHssfWorkbook.getSheetAt(0);
		
		int intRow = 1;
		int intCell = 0;
		
		EmpresaService empService = new EmpresaService();
		
		for (SuministroAfectado suministroAfectado : service.getAllRows1(interrupcion.getInterrupcionId())) {
			
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell, suministroAfectado.getNumSumAfectado());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 1, suministroAfectado.getUbiSuministro());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 2, suministroAfectado.getTenSuministro());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 3, suministroAfectado.getCodSed());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 4, suministroAfectado.getCodAlimentador());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 5, suministroAfectado.getOpcTarifa());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 6, suministroAfectado.getFecIniIntReal());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 7, suministroAfectado.getFecFinIntReal());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 8, suministroAfectado.getCodSet());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 9, suministroAfectado.getSemestre());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 10, suministroAfectado.getEneSemSuministrada());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 11, suministroAfectado.getConEneTeorica());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 12, suministroAfectado.getPotFacturada());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 13, suministroAfectado.getEneCompensar());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 14, suministroAfectado.getPotCompensar());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 15, suministroAfectado.getComEnergia());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 16, suministroAfectado.getComPotencia());
				
				if(suministroAfectado.getInterrupcion() != null){
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 17, suministroAfectado.getInterrupcion().getCodInterrupcion());
				}
				if(suministroAfectado.getLocalidad() != null){
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 18, suministroAfectado.getLocalidad().getCodLocalidad()+"-"+suministroAfectado.getLocalidad().getNombre());
				}
				
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 19, suministroAfectado.getTipServicio());
				
				intRow++;
				

		}
		
		ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());

	    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
	    facesContext.responseComplete();	
				
		return null;
	}

	public List<SelectItem> getItemsTipServicio() {
		itemsTipServicio=new ArrayList<SelectItem>();
		itemsTipServicio.add(new SelectItem("U","URBANO"));
		itemsTipServicio.add(new SelectItem("R","RURAL"));
		return itemsTipServicio;
	}

	public void setItemsTipServicio(List<SelectItem> itemsTipServicio) {
		this.itemsTipServicio = itemsTipServicio;
	}

	public String getTipoServicioFilter() {
		return tipoServicioFilter;
	}

	public void setTipoServicioFilter(String tipoServicioFilter) {
		this.tipoServicioFilter = tipoServicioFilter;
	}
	
	public Filter<?> getFilterTipServicio() {
        return new Filter<SuministroAfectado>() {
            public boolean accept(SuministroAfectado t) {
            	String tipServicio = getTipoServicioFilter();
                if (tipServicio == null || tipServicio.length() == 0 || tipServicio.equals(t.getTipServicio())) {
                    return true;
                }
                return false;
            }
        };
    }

	public List<SelectItem> getItemsTipServicio1() {
		itemsTipServicio1=new ArrayList<SelectItem>();
		itemsTipServicio1.add(new SelectItem("", ""));
		itemsTipServicio1.add(new SelectItem("U","URBANO"));
		itemsTipServicio1.add(new SelectItem("R","RURAL"));
		return itemsTipServicio1;
	}

	public void setItemsTipServicio1(List<SelectItem> itemsTipServicio1) {
		this.itemsTipServicio1 = itemsTipServicio1;
	}

	public List<SelectItem> getItemsPerteneceSer() {
		itemsPerteneceSer=new ArrayList<SelectItem>();
		itemsPerteneceSer.add(new SelectItem("SI","SI"));
		itemsPerteneceSer.add(new SelectItem("NO","NO"));
		return itemsPerteneceSer;
	}

	public void setItemsPerteneceSer(List<SelectItem> itemsPerteneceSer) {
		this.itemsPerteneceSer = itemsPerteneceSer;
	}

	public String getPerteneceSerFilter() {
		return perteneceSerFilter;
	}

	public void setPerteneceSerFilter(String perteneceSerFilter) {
		this.perteneceSerFilter = perteneceSerFilter;
	}

	public Filter<?> getFilterPerteneceSer() {
        return new Filter<SuministroAfectado>() {
            public boolean accept(SuministroAfectado t) {
            	String perteneceSer = getPerteneceSerFilter();
                if (perteneceSer == null || perteneceSer.length() == 0 || perteneceSer.equals(t.getPerteneceSer())) {
                    return true;
                }
                return false;
            }
        };
    }

	public List<SelectItem> getItemsPerteneceSer1() {
		itemsPerteneceSer1=new ArrayList<SelectItem>();
		itemsPerteneceSer1.add(new SelectItem("",""));
		itemsPerteneceSer1.add(new SelectItem("SI","SI"));
		itemsPerteneceSer1.add(new SelectItem("NO","NO"));
		return itemsPerteneceSer1;
	}

	public void setItemsPerteneceSer1(List<SelectItem> itemsPerteneceSer1) {
		this.itemsPerteneceSer1 = itemsPerteneceSer1;
	}

	public List<SelectItem> getItemsLocalidad1() {
		LocalidadService service=new LocalidadService();
		itemsLocalidad1=new ArrayList<SelectItem>();
		for(Localidad tp:service.getAllRows1()){
			itemsLocalidad1.add(new SelectItem(tp,tp.getCodLocalidad()+"-"+tp.getNombre()));
		}
		return itemsLocalidad1;
		
	}

	public void setItemsLocalidad1(List<SelectItem> itemsLocalidad1) {
		this.itemsLocalidad1 = itemsLocalidad1;
	}

	public String exportarSumxIntxAnoSem(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
	    
	    String rutaArchivoOut;
	    String rutaUp;
	    rutaUp = facesContext.getExternalContext().getRealPath("/reportes/Detalle_calculoCompenSum.xlsx");
	    String path = System.getProperty("uploads.folder");
	    String nombreArchivo;
	    
		String semestre = null;
		String ano = null;
		long diferenciaHoras=0;
				
		semestre = interrupcion.getPeriodoMedicion().getSemestre();
		ano = interrupcion.getPeriodoMedicion().getAno();
		String codNormaNprima = null;
		String codNormaDprima = null;
		String nivel = null;
		//nivel = suministroAfectado.getTenSuministro();
	    
		/*if(suministroAfectado.getTenSuministro() != null){
			nivel = suministroAfectado.getTenSuministro();
		}*/
		
		
	    System.out.print(path + "\n");
	    
	    nombreArchivo = "Detalle_calculoCompenSum" + "_" + getInterrupcion().getPeriodoMedicion().getAno() + "_" + getInterrupcion().getPeriodoMedicion().getSemestre() + "_"+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsx";
	    rutaArchivoOut = path + nombreArchivo;
	    	    
	    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);

		XSSFSheet objHssfSheet = null;
		SuministroAfectadoService suministroAfectadoService = new SuministroAfectadoService();
		//suministroAfectado = suministroAfectadoService.ReadById(400726);
		objHssfSheet = objHssfWorkbook.getSheetAt(0);
		
		for (SuministroAfectado sum : selectionItems) {
		//
			suministroAfectado = sum;
			
			 if(suministroAfectado.getTenSuministro().equals("MA")){
				 codNormaNprima =  "N_PRIMA_MAAT";
			 		 }
			 if(suministroAfectado.getTenSuministro().equals("AT")){
				 codNormaNprima =  "N_PRIMA_MAAT";
			 		 }
			 if(suministroAfectado.getTenSuministro().equals("MT")){
				 codNormaNprima =  "N_PRIMA_MT";
			 		 }
			 if(suministroAfectado.getTenSuministro().equals("BT")){
				 codNormaNprima =  "N_PRIMA_BT";
			 		 }
					
			Sql sql = new Sql("CALIDAD");
			
			String selectSql = "SELECT P.PAR_NOR_VALOR FROM CAL_PARAMETRO_NORMA P, CAL_TIPO_PARAMETRO T WHERE  P.PK_TIP_PARAMETRO_ID = T.TIP_PARAMETRO_ID AND P.COD_PAR_NORMA = '"
					+ codNormaNprima
					+ "' AND T.COD_TIP_PARAMETRO = 'SI'  AND P.ESTADO = '1' AND T.ESTADO = '1'";
				
			System.out.println("llega1:" + selectSql);
			Object[] objTabla = sql.getFila(selectSql);
			if (!objTabla.equals(null)) {
				Object object = objTabla[0];
				
				ModificarExcel.setCellValue(objHssfSheet, 12,4, (BigDecimal)object);
				
			}
			
			if(suministroAfectado.getTenSuministro().equals("MA")){
				codNormaDprima =  "D_PRIMA_MAAT";
			 }
			 if(suministroAfectado.getTenSuministro().equals("AT")){
				 codNormaDprima =  "D_PRIMA_MAAT";
			 }
			 if(suministroAfectado.getTenSuministro().equals("MT")){
				 codNormaDprima =  "D_PRIMA_MT";
			 }
			 if(suministroAfectado.getTenSuministro().equals("BT")){
				 codNormaDprima =  "D_PRIMA_BT";
			 }
			
				
			String selectSql2 = "SELECT P.PAR_NOR_VALOR FROM CAL_PARAMETRO_NORMA P, CAL_TIPO_PARAMETRO T WHERE  P.PK_TIP_PARAMETRO_ID = T.TIP_PARAMETRO_ID AND P.COD_PAR_NORMA = '"
						+ codNormaDprima
						+ "' AND T.COD_TIP_PARAMETRO = 'SI'  AND P.ESTADO = '1' AND T.ESTADO = '1'";
				
			System.out.println("llega2:" + selectSql2);
				Object[] objTabla2 = sql.getFila(selectSql2);
				if (!objTabla2.equals(null)) {
					Object object2 = objTabla2[0];		
					ModificarExcel.setCellValue(objHssfSheet, 18,4,(BigDecimal)object2);
					
				}
				
			
			try  {
		    
				
				SuministroAfectadoService service = new SuministroAfectadoService();
				
			   
				
				int intRow = 21;
				int intCell = 0;
				
				ModificarExcel.setCellValue(objHssfSheet, 2, 2, semestre);
				ModificarExcel.setCellValue(objHssfSheet, 2, 4, ano);
				ModificarExcel.setCellValue(objHssfSheet, 3, 2, suministroAfectado.getTenSuministro());
				ModificarExcel.setCellValue(objHssfSheet, 4, 2, suministroAfectado.getNumSumAfectado());
				ModificarExcel.setCellValue(objHssfSheet, 5, 2, suministroAfectado.getCodSed());
				ModificarExcel.setCellValue(objHssfSheet, 6, 2, suministroAfectado.getCodAlimentador());
				
				//
				String anoMes = interrupcion.getPeriodoMedicion().getAno()+interrupcion.getPeriodoMedicion().getPeriodo();
				
				String result;
				ConectaDb db = new ConectaDb("CALIDAD");
		
				Connection cn = db.getConnection();
				int dato=0;
				double energia1=0;
				double energia2=0;
				double energia3=0;
				double energia4=0;
				double energia5=0;
				double energia6=0;
				
		        CallableStatement cstmt = null;  
		        try {
		
		        	cstmt = cn.prepareCall("{CALL ? := CALIDAD_PACKAGE.FN_NHM_SEM(?,?)}");
					
					cstmt.registerOutParameter(1, Types.INTEGER);
					cstmt.setString(2, suministroAfectado.getInterrupcion().getPeriodoMedicion().getAno());
					cstmt.setString(3, suministroAfectado.getInterrupcion().getPeriodoMedicion().getSemestre());
					
		            int ctos = cstmt.executeUpdate();
		            dato = cstmt.getInt(1);
		            cstmt.close(); 
		            
		            cstmt = cn.prepareCall("{CALL ? := CALIDAD_PACKAGE.FN_OBTENER_ENERGIA_AUX(?,?,?)}");
					
					cstmt.registerOutParameter(1, Types.DOUBLE);
					cstmt.setString(2, suministroAfectado.getNumSumAfectado());
					cstmt.setString(3, suministroAfectado.getInterrupcion().getPeriodoMedicion().getAno());
					
					if (suministroAfectado.getInterrupcion().getPeriodoMedicion().getSemestre().equals("S1")){
						cstmt.setString(4, "01");
						
			            ctos = cstmt.executeUpdate();
			            energia1 = cstmt.getDouble(1);
			            
			            cstmt.setString(4, "02");
						
			            ctos = cstmt.executeUpdate();
			            energia2 = cstmt.getDouble(1);
			            
			            cstmt.setString(4, "03");
						
			            ctos = cstmt.executeUpdate();
			            energia3 = cstmt.getDouble(1);
			            
			            cstmt.setString(4, "04");
						
			            ctos = cstmt.executeUpdate();
			            energia4 = cstmt.getDouble(1);
			            
			            cstmt.setString(4, "05");
						
			            ctos = cstmt.executeUpdate();
			            energia5 = cstmt.getDouble(1);
			            
			            cstmt.setString(4, "06");
						
			            ctos = cstmt.executeUpdate();
			            energia6 = cstmt.getDouble(1);
					}else{
						cstmt.setString(4, "07");
						
			            ctos = cstmt.executeUpdate();
			            energia1 = cstmt.getDouble(1);
			            
			            cstmt.setString(4, "08");
						
			            ctos = cstmt.executeUpdate();
			            energia2 = cstmt.getDouble(1);
			            
			            cstmt.setString(4, "09");
						
			            ctos = cstmt.executeUpdate();
			            energia3 = cstmt.getDouble(1);
			            
			            cstmt.setString(4, "10");
						
			            ctos = cstmt.executeUpdate();
			            energia4 = cstmt.getDouble(1);
			            
			            cstmt.setString(4, "11");
						
			            ctos = cstmt.executeUpdate();
			            energia5 = cstmt.getDouble(1);
			            
			            cstmt.setString(4, "12");
						
			            ctos = cstmt.executeUpdate();
			            energia6 = cstmt.getDouble(1);
					}
					
		            cstmt.close();
		            
		            
		            if (ctos == 0) {
		                result = "0 filas afectadas";
		            }
		        	
		        } catch (SQLException e) {
		            result = e.getMessage();
		           
		            
					FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", "No se pudo completar la Transacción"));
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		        } finally {
		            try {
		                cn.close();
		            } catch (SQLException e) {
		                result = e.getMessage();
		                
		    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", "No se pudo completar la Transacción"));
		    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		                
		            }				
		        }	
				
		        
		        ModificarExcel.setCellValue(objHssfSheet, 9, 7, dato);
		     
		        if(suministroAfectado.getInterrupcion().getPeriodoMedicion().getSemestre().equals("S1")){
		        	ModificarExcel.setCellValue(objHssfSheet, 9, 12, "01");
		        	ModificarExcel.setCellValue(objHssfSheet, 10, 12, "02");
		        	ModificarExcel.setCellValue(objHssfSheet, 11, 12, "03");
		        	ModificarExcel.setCellValue(objHssfSheet, 12, 12, "04");
		        	ModificarExcel.setCellValue(objHssfSheet, 13, 12, "05");
		        	ModificarExcel.setCellValue(objHssfSheet, 14, 12, "06");
		        }else{
		        	ModificarExcel.setCellValue(objHssfSheet, 9, 12, "07");
		        	ModificarExcel.setCellValue(objHssfSheet, 10, 12, "08");
		        	ModificarExcel.setCellValue(objHssfSheet, 11, 12, "09");
		        	ModificarExcel.setCellValue(objHssfSheet, 12, 12, "10");
		        	ModificarExcel.setCellValue(objHssfSheet, 13, 12, "11");
		        	ModificarExcel.setCellValue(objHssfSheet, 14, 12, "12");
		        }
		        ModificarExcel.setCellValue(objHssfSheet, 9, 13, energia1);
		        ModificarExcel.setCellValue(objHssfSheet, 10, 13, energia2);
		        ModificarExcel.setCellValue(objHssfSheet, 11, 13, energia3);
		        ModificarExcel.setCellValue(objHssfSheet, 12, 13, energia4);
		        ModificarExcel.setCellValue(objHssfSheet, 13, 13, energia5);
		        ModificarExcel.setCellValue(objHssfSheet, 14, 13, energia6);
				//
								
					for (SuministroAfectado suministroAfectadoFor : service.getInterxSuministroAnoSem(suministroAfectado.getNumSumAfectado(),interrupcion.getPeriodoMedicion().getAno(), interrupcion.getPeriodoMedicion().getSemestre())) {
							
							if(suministroAfectadoFor.getInterrupcion().getTipoInterrupcion().getCodTipInterrupcion().equals("E") || suministroAfectadoFor.getInterrupcion().getTipoInterrupcion().getCodTipInterrupcion().equals("N")){
								if(suministroAfectadoFor.getInterrupcion().getSolFueMayor().equals("F")){
									continue;
								}
							}
							ModificarExcel.setCellValue(objHssfSheet,intRow, intCell, suministroAfectadoFor.getInterrupcion().getCodInterrupcion());
							
							if(suministroAfectadoFor.getInterrupcion().getTipoInterrupcion().getCodTipInterrupcion().equals("E") || suministroAfectadoFor.getInterrupcion().getTipoInterrupcion().getCodTipInterrupcion().equals("M")){
								ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 1,'P');
							} else {
								ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 1,'N');
							}
							ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 2,suministroAfectadoFor.getInterrupcion().getFecIniInterrupcion());
							ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 3,suministroAfectadoFor.getInterrupcion().getFecFinInterrupcion());
							ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 4,suministroAfectadoFor.getInterrupcion().getFecIniProgramada());
							ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 5,suministroAfectadoFor.getInterrupcion().getFecFinProgramada());
							ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 6,((double)(suministroAfectadoFor.getFecFinIntReal().getTime()-suministroAfectadoFor.getFecIniIntReal().getTime())/1000)/3600);
											
							ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 8, suministroAfectadoFor.getOpcTarifa());
							ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 9, suministroAfectadoFor.getCodSed());
							ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 10, suministroAfectadoFor.getTenSuministro());
							ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 11, suministroAfectadoFor.getLocalidad().getCodLocalidad());
							ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 12, suministroAfectadoFor.getLocalidad().getSecTipico());
							
							if(suministroAfectadoFor.getInterrupcion().getTipoInterrupcion().getCodTipInterrupcion().equals("M")){
								ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 13,'1');
							} else {
								ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 13,'0');
							}
							
							if(suministroAfectadoFor.getInterrupcion().getTipoInterrupcion().getCodTipInterrupcion().equals("M")){
								ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 14,((double)(suministroAfectadoFor.getFecFinIntReal().getTime()-suministroAfectadoFor.getFecIniIntReal().getTime())/1000)/3600);
							} else {
								ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 14,'0');
							}
							
							//Exc_Dur_Pro_mtto//ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 15, );
							if(suministroAfectadoFor.getInterrupcion().getTipoInterrupcion().getCodTipInterrupcion().equals("E")){
								ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 16,'1');
							} else {
								ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 16,'0');
							}
				
							if(suministroAfectadoFor.getInterrupcion().getTipoInterrupcion().getCodTipInterrupcion().equals("E")){
								ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 17,((double)(suministroAfectadoFor.getFecFinIntReal().getTime()-suministroAfectadoFor.getFecIniIntReal().getTime())/1000)/3600);
							} else {
								ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 17,'0');
							}
							
							//Exc_Dur_Pro_exp//ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 18, );
							if(suministroAfectadoFor.getInterrupcion().getTipoInterrupcion().getCodTipInterrupcion().equals("N")){
								ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 19,'1');
							} else {
								ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 19,'0');
							}
							if(suministroAfectadoFor.getInterrupcion().getTipoInterrupcion().getCodTipInterrupcion().equals("N")){
								ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 20,((double)(suministroAfectadoFor.getFecFinIntReal().getTime()-suministroAfectadoFor.getFecIniIntReal().getTime())/1000)/3600);
							} else {
								ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 20,'0');
							}
						
							//N34   //ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 21, );
							if((((suministroAfectadoFor.getFecFinIntReal().getTime()-suministroAfectadoFor.getFecIniIntReal().getTime())/1000)/3600)>34){
								ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 21,'1');
							}else{
								ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 21,'0');
							}
							intRow++;
						
					}
									
				
				ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
			
			} catch (Exception e) {
				e.printStackTrace();
			}
		    
		    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());
	
		    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
		    facesContext.responseComplete();
		}
				
		return null;
	}

	   public void selectionListener(AjaxBehaviorEvent event) {
	        UIExtendedDataTable dataTable = (UIExtendedDataTable) event.getComponent();
	        Object originalKey = dataTable.getRowKey();
	        selectionItems.clear();
	        for (Object selectionKey : selection) {
	            dataTable.setRowKey(selectionKey);
	            if (dataTable.isRowAvailable()) {
	                selectionItems.add((SuministroAfectado) dataTable.getRowData());
	            }
	        }
	        dataTable.setRowKey(originalKey);
	    }	
	
	   public String exportartablainterrupxSum(){
			final FacesContext facesContext = FacesContext.getCurrentInstance();
		    
		    String rutaArchivoOut;
		    String rutaUp;
		    rutaUp = facesContext.getExternalContext().getRealPath("/reportes/AnexoInterxSum.xlsx");
		    String path = System.getProperty("uploads.folder");
		    String nombreArchivo;
		    
		    System.out.print(path + "\n");
		    
		    nombreArchivo = "interrupciones_x_sum" + "_" + getNumSumAfectado() + "_"+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsx";
		    rutaArchivoOut = path + nombreArchivo;
		    	    
		    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);

			XSSFSheet objHssfSheet = null;
			
			try  {
		    
			    SuministroAfectadoService service = new SuministroAfectadoService();
			    
			    objHssfSheet = objHssfWorkbook.getSheetAt(0);
				
				int intRow = 1;
				int intCell = 0;
			
				for (SuministroAfectado suministroAfectado : service.getInterrupcionxSuministro(numSumAfectado)) {
					
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell, suministroAfectado.getNumSumAfectado());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 1, suministroAfectado.getUbiSuministro());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 2, suministroAfectado.getTenSuministro());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 3, suministroAfectado.getCodSed());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 4, suministroAfectado.getCodSet());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 5, suministroAfectado.getCodAlimentador());
						if(suministroAfectado.getInterrupcion().getTipoInterrupcion() != null ){
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 6, suministroAfectado.getInterrupcion().getTipoInterrupcion().getDescripcion());
						}
						if(suministroAfectado.getInterrupcion() != null ){
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 7, suministroAfectado.getInterrupcion().getSolFueMayor());
						}
						if(suministroAfectado.getInterrupcion().getCausaInterrupcion() != null ){
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 8, suministroAfectado.getInterrupcion().getCausaInterrupcion().getDescripcion());
						}
						
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 9, suministroAfectado.getInterrupcion().getPeriodoMedicion().getAno()+"-" +suministroAfectado.getInterrupcion().getPeriodoMedicion().getPeriodo());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 10, suministroAfectado.getFecIniIntReal());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 11, suministroAfectado.getFecFinIntReal());
						if(suministroAfectado.getInterrupcion() != null ){
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 12, suministroAfectado.getInterrupcion().getFecIniInterrupcion());
						}
						if(suministroAfectado.getInterrupcion() != null ){
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 13, suministroAfectado.getInterrupcion().getFecFinInterrupcion());
						}
						if(suministroAfectado.getInterrupcion() != null ){
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 14, suministroAfectado.getInterrupcion().getFecIniProgramada());
						}
						if(suministroAfectado.getInterrupcion() != null ){
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 15, suministroAfectado.getInterrupcion().getFecFinProgramada());
						}
						if(suministroAfectado.getInterrupcion() != null ){
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 16, suministroAfectado.getInterrupcion().getCodInterrupcion());
						}
						intRow++;
						
		
				}
				
				ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
			
			} catch (Exception e) {
				e.printStackTrace();
			}
		    
		    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());

		    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
		    facesContext.responseComplete();	
					
			return null;
		}
	   
}
