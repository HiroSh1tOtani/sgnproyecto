package pe.com.calidad.suministro.mbean;


import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import org.hibernate.HibernateException;

import pe.com.calidad.suministro.entity.ParametroAnual;//RelevadorRechazoCarga;
import pe.com.calidad.suministro.entity.RelevadorRechazoCarga;
import pe.com.calidad.suministro.service.ParametroAnualService;//RelevadorRechazoCargaService;
import pe.com.indra.calidad.util.ConectaDb;
import pe.com.indra.calidad.util.Sql;
import pe.com.indra.calidad.util.Utilidad;

@ManagedBean(name="parametroAnualBean")
@SessionScoped
public class ParametroAnualBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	private List<ParametroAnual> parametrosAnual; 
	private ParametroAnual parametroAnual;
	private String mensaje;
	private List<SelectItem> itemsAno;
	private String anoSemestre;
	private String campoAnoSemestre;
	
	
		
	@PostConstruct
	public void init(){
		parametroAnual=new ParametroAnual();
		parametroAnual.setEstado("1");
		
	}



	public String getMensaje() {
		return mensaje;
	}


	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public void  filtroChanged(AjaxBehaviorEvent event) {  
		
		busquedaxAno();
	}
	
	public String prepararCrear(){
		//Esto funciona siempre que el bean tenga ambito de sesion 
		setParametroAnual(new ParametroAnual());
		getParametroAnual().setAno(anoSemestre.substring(0, 4));
		campoAnoSemestre = anoSemestre;
		return "/zonasegura/interrupcion/parametroAnualCrear?faces-redirect=true";
	}
	
	public String prepararEditar(){
				
		try {
			cargarParametroAnualActual();
			
			return "/zonasegura/interrupcion/parametroAnualEditar?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/interrupcion/parametroAnualListar?faces-redirect=true";
		}
	}

	public String prepararVer(){
				
		try {
			cargarParametroAnualActual();
			return "/zonasegura/interrupcion/parametroAnualVer?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/interrupcion/parametroAnualListar?faces-redirect=true";
		}
	}
	
	public String salvar(){

		
		ParametroAnualService service=new ParametroAnualService();
		
		try {
			parametroAnual.setAno(campoAnoSemestre.substring(0, 4));
			service.create(parametroAnual);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/interrupcion/parametroAnualListar?faces-redirect=true";
	}
	
	public String actualizar(){
		
		ParametroAnualService service=new ParametroAnualService();
		
		try {
			parametroAnual.setAno(campoAnoSemestre.substring(0, 4));
			service.update(parametroAnual);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/interrupcion/parametroAnualListar?faces-redirect=true";
	}
	
	public String eliminar(){
		String parAnualId=Utilidad.getParametro("parAnualId");
		ParametroAnualService service=new ParametroAnualService();
		
		
		try {
			parametroAnual=service.ReadById(new Long(parAnualId));
			parametroAnual.setEstado("0");
			
			service.update(parametroAnual);
		} catch (HibernateException e) {
			
			
			System.out.println(e.getMessage()+":"+e.getCause());
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			
		} catch (Exception e) {
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/interrupcion/parametroAnualListar?faces-redirect=true";
	}
	
	public void cargarParametroAnualActual(){
		String parAnualId=Utilidad.getParametro("parAnualId");
		ParametroAnualService service=new ParametroAnualService();

		
		try {
			parametroAnual=service.ReadById(new Long(parAnualId));
			campoAnoSemestre = parametroAnual.getAno() ;
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

			throw new HibernateException("No se puede cargar ParametroAnual.", e);
			
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
	}

	public String busquedaxAno() {
		
		String ano;
	
		
		ParametroAnualService service=new ParametroAnualService();
		try {
			
			ano = anoSemestre.substring(0, 4);
			System.out.println("ano :" + ano);
			parametrosAnual=service.getBusquedaxAno(ano.trim());
			
		} catch (HibernateException e) {
			setMensaje(e.getMessage());
		}
		
		return "/zonasegura/interrupcion/parametroAnualListar?faces-redirect=true";
	}



	public List<ParametroAnual> getParametrosAnual() {
		return parametrosAnual;
	}



	public void setParametrosAnual(List<ParametroAnual> parametrosAnual) {
		this.parametrosAnual = parametrosAnual;
	}


	public List<SelectItem> getItemsAno() {
		itemsAno=new ArrayList<SelectItem>();
		Sql sql = new Sql("CALIDAD");
		String s = "SELECT DISTINCT ANO FROM CAL_PERIODO_MEDICION WHERE ESTADO = '1' ORDER BY ANO DESC ";
		List<Object[]> listObject = sql.consulta(s, false);
		for (Object[] objects: listObject) {
			itemsAno.add(new SelectItem((String)objects[0],(String)objects[0]));
		}
		
		return itemsAno;
	}


	public void setItemsAno(List<SelectItem> itemsAno) {
		this.itemsAno = itemsAno;
	}


	public ParametroAnual getParametroAnual() {
		return parametroAnual;
	}


	public void setParametroAnual(ParametroAnual parametroAnual) {
		this.parametroAnual = parametroAnual;
	}

	public String getAnoSemestre() {
		return anoSemestre;
	}

	public void setAnoSemestre(String anoSemestre) {
		this.anoSemestre = anoSemestre;
	}

	public String getCampoAnoSemestre() {
		return campoAnoSemestre;
	}

	public void setCampoAnoSemestre(String campoAnoSemestre) {
		this.campoAnoSemestre = campoAnoSemestre;
	}
	
}
