package pe.com.calidad.suministro.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.calidad.suministro.entity.CausaInterrupcion;
import pe.com.calidad.suministro.service.CausaInterrupcionService;

@FacesConverter(value="causaInterrupcionConverter",forClass=CausaInterrupcionConverter.class)

public class CausaInterrupcionConverter implements Converter{

	@Override
	public CausaInterrupcion getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long cauInterrupcionId=new Long(cadena);
		CausaInterrupcionService service=new CausaInterrupcionService();
		CausaInterrupcion causaInterrupcion=new CausaInterrupcion();
		causaInterrupcion=service.ReadById(cauInterrupcionId);
		System.out.println(causaInterrupcion.getCauInterrupcionId());
		return causaInterrupcion;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			CausaInterrupcion causaInterrupcion=(CausaInterrupcion) objeto;
			////System.out.println("objeto:"+new Long(causaInterrupcion.getCauInterrupcionId()).toString());
			return new Long(causaInterrupcion.getCauInterrupcionId()).toString();
		}
		return null;
	}


}
