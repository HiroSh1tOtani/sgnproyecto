package pe.com.calidad.suministro.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;


import pe.com.calidad.suministro.entity.SuministroAfectado;

import pe.com.calidad.suministro.service.SuministroAfectadoService;

@FacesConverter(value="interrupcionConverter",forClass=SuministroAfectadoConverter.class)

public class SuministroAfectadoConverter implements Converter{

	@Override
	public SuministroAfectado getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long sumAfectadoId=new Long(cadena);
		SuministroAfectadoService service=new SuministroAfectadoService();
		SuministroAfectado suministroAfectado=new SuministroAfectado();
		suministroAfectado=service.ReadById(sumAfectadoId);
		System.out.println(suministroAfectado.getSumAfectadoId());
		return suministroAfectado;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			SuministroAfectado suministroAfectado=(SuministroAfectado) objeto;
			//System.out.println("objeto:"+new Long(suministroAfectado.getSumAfectadoId()).toString());
			return new Long(suministroAfectado.getSumAfectadoId()).toString();
		}
		return null;
	}


}
