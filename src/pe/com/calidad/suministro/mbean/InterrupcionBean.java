package pe.com.calidad.suministro.mbean;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.HibernateException;

import pe.com.calidad.alumbradopublico.entity.Via;
import pe.com.calidad.alumbradopublico.service.ViaService;
import pe.com.calidad.suministro.entity.CompensacionSuministro;
import pe.com.calidad.suministro.entity.Indicador074;
import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.entity.VAnexoIDetail;
import pe.com.calidad.suministro.entity.CausaInterrupcion;
import pe.com.calidad.suministro.entity.SuministroAfectado;
import pe.com.calidad.suministro.entity.TipoInterrupcion;
import pe.com.calidad.suministro.entity.TipoNotificacion;
import pe.com.calidad.suministro.entity.ModalidadDeteccion;
import pe.com.indra.calidad.entity.Localidad;
import pe.com.indra.calidad.entity.Medicion;
//import pe.com.calidad.suministro.entity.SuministroAfectado;
import pe.com.indra.calidad.entity.PeriodoMedicion;
import pe.com.indra.calidad.entity.Reporte;
import pe.com.calidad.suministro.service.CompensacionSuministroService;
import pe.com.calidad.suministro.service.Indicador074Service;
import pe.com.calidad.suministro.service.InterrupcionService;
import pe.com.calidad.suministro.service.CausaInterrupcionService;
import pe.com.calidad.suministro.service.SuministroAfectadoService;
import pe.com.calidad.suministro.service.TipoInterrupcionService;
import pe.com.calidad.suministro.service.TipoNotificacionService;
import pe.com.calidad.suministro.service.ModalidadDeteccionService;
import pe.com.indra.calidad.service.EmpresaService;
import pe.com.indra.calidad.service.LocalidadService;
import pe.com.indra.calidad.service.MedicionService;
//import pe.com.calidad.suministro.service.SuministroAfectadoService;
import pe.com.indra.calidad.service.PeriodoMedicionService;
import pe.com.indra.calidad.util.ConectaDb;
import pe.com.indra.calidad.util.ModificarExcel;
import pe.com.indra.calidad.util.Sql;
//import pe.com.indra.calidad.util.ConectaDb;
//import pe.com.indra.calidad.util.Sql;
import pe.com.indra.calidad.util.Utilidad;

import org.richfaces.component.UIExtendedDataTable;
import org.richfaces.model.Filter;

@ManagedBean(name="interrupcionBean")
@SessionScoped
public class InterrupcionBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Object[] Object = null;
	
	private List<Interrupcion> interrupciones;
	private Interrupcion interrupcion;
	private List<SelectItem> itemsCausaInterrupcion;
	private List<SelectItem> itemsCausaInterrupcion1;
	private List<SelectItem> itemsCausaInterrupcion2;
	
	private List<SelectItem> itemsTipoInterrupcion;
	private List<SelectItem> itemsTipoInterrupcion1;
	private List<SelectItem> itemsTipoInterrupcion2;
	
	
	private List<SelectItem> itemsTipoNotificacion;
	private List<SelectItem> itemsTipoNotificacion1;
	
	private List<SelectItem> itemsModalidadDeteccion;
	private List<SelectItem> itemsModalidadDeteccion1;
	
	private List<SelectItem> itemsPeriodoMedicion;
	private List<SelectItem> itemsPeriodoMedicion1;
	
	private List<SelectItem> itemsSolFueMayor;
	
	private java.util.Date fecIniProgramada;
	private java.util.Date fecFinProgramada;
	private java.util.Date fecNotificacion1;
	private java.util.Date fecNotificacion2;
	private java.util.Date fecIniInterrupcion;
	private java.util.Date fecFinInterrupcion;
	private java.util.Date fecOficio;
	private java.util.Date fecFinComSalio;
	private java.util.Date fecResol;
	private int origen;
	
	private String codInterrupcionFilter;
	private String tipInterrupcionFilter;
	private String cauInterrupcionFilter;
	private String solFueMayorFilter;
	
	private java.util.Date fecIniProFilter;
	private java.util.Date fecFinProFilter;
	private java.util.Date fecIniIntFilter;
	private java.util.Date fecFinIntFilter;
	private java.util.Date fecFinExoneracion;	

	private Long NumSumRegEstimadosFilter;
	private Long NumSumRegAfectadosFilter;
	private Long NumCliLibAfectadosFilter;
	private String tenInterrupcionFilter;

	private PeriodoMedicion periodoMedicion;
	
	private List<SelectItem> itemsLocalidad;
	private List<SelectItem> itemsLocalidad1;

	private List<SelectItem> itemsNivelTensio;
	private List<SelectItem> itemsfasInterrumpida;
	private List<SelectItem> itemsSolFuerzaMayor;
	private List<SelectItem> itemsZonAfectada;
	private List<SelectItem> itemsOriInterrupcion;
	private List<SelectItem> itemsSincronizar;
	private List<SelectItem> itemsTipoRelevador;
	private List<SelectItem> itemsRecCarga;
	
	private List<SelectItem> itemsTenInterrupcion;
	
	private String secTipicoFilter;
	private List<SelectItem> itemsSecTipico;
	
	private List<SelectItem> itemsTipInsSalio;
	private List<SelectItem> itemsTipInsFalla;
	private List<SelectItem> itemsNaturaleza;
	private List<SelectItem> itemsCodOriInterrupcion;
	private List<SelectItem> itemsPropiedad;
	private List<SelectItem> itemsResponsable;
	private List<SelectItem> itemsCodCausa074;
	private List<SelectItem> itemsIntImportante;
	private List<SelectItem> itemsAnexo1074;
	
	private String anexo1074Filter;
	private List<SelectItem> itemsAnexo10742;
	
	private List<SelectItem> itemsOperacion;
	private java.util.Date fechaSincronizar;
	private String operacion;
	private List<SelectItem> itemsTipoSin;
	private long tipoSin;
	private String concesionesFilter;
	private List<SelectItem> itemsConcesiones;
	
	private List<SelectItem> itemsSectorTipico;
	private List<SelectItem> itemsEstadoResolucion;
	private String motivoFilter;
	private String localizacionFallaFilter;
	private String zonaAfectadaFilter;
	private List<SelectItem> itemsZonaAfectada;
	private String oriInterrupcionFilter;
	private List<SelectItem> itemsOrigenInterrupcion;	
	private String sincronizarFilter;
	private List<SelectItem> itemsSincronizacion;
	private String codigoInterCambi;
	
	private long filasTotal;
	private List<SelectItem> itemsCodRelevador;

	private Collection<Object> selection;
	private List<Interrupcion> selectionItems = new ArrayList<Interrupcion>();
	private String ExternalContext;
	
	public Collection<Object> getSelection() {
		return selection;
	}

	public void setSelection(Collection<Object> selection) {
		this.selection = selection;
	}

	public List<Interrupcion> getSelectionItems() {
		return selectionItems;
	}

	public void setSelectionItems(List<Interrupcion> selectionItems) {
		this.selectionItems = selectionItems;
	}

	public java.util.Date getFecIniProgramada() {
		return fecIniProgramada;
	}


	public void setFecIniProgramada(java.util.Date fecIniProgramada) {
		this.fecIniProgramada = fecIniProgramada;
	}


	public java.util.Date getFecFinProgramada() {
		return fecFinProgramada;
	}


	public void setFecFinProgramada(java.util.Date fecFinProgramada) {
		this.fecFinProgramada = fecFinProgramada;
	}


	public java.util.Date getFecNotificacion1() {
		return fecNotificacion1;
	}


	public void setFecNotificacion1(java.util.Date fecNotificacion1) {
		this.fecNotificacion1 = fecNotificacion1;
	}


	public java.util.Date getFecNotificacion2() {
		return fecNotificacion2;
	}


	public void setFecNotificacion2(java.util.Date fecNotificacion2) {
		this.fecNotificacion2 = fecNotificacion2;
	}


	public java.util.Date getFecIniInterrupcion() {
		return fecIniInterrupcion;
	}


	public void setFecIniInterrupcion(java.util.Date fecIniInterrupcion) {
		this.fecIniInterrupcion = fecIniInterrupcion;
	}


	public java.util.Date getFecFinInterrupcion() {
		return fecFinInterrupcion;
	}


	public void setFecFinInterrupcion(java.util.Date fecFinInterrupcion) {
		this.fecFinInterrupcion = fecFinInterrupcion;
	}
	
	public java.util.Date getFecOficio() {
		return fecOficio;
	}


	public void setFecOficio(java.util.Date fecOficio) {
		this.fecOficio = fecOficio;
	}


	public java.util.Date getFecResol() {
		return fecResol;
	}


	public void setFecResol(java.util.Date fecResol) {
		this.fecResol = fecResol;
	}


	public java.util.Date getFecFinExoneracion() {
		return fecFinExoneracion;
	}


	public void setFecFinExoneracion(java.util.Date fecFinExoneracion) {
		this.fecFinExoneracion = fecFinExoneracion;
	}
	

	private String mensaje;
	
	@PostConstruct 
	public void init(){
		interrupcion=new Interrupcion();
		interrupcion.setEstado("1");
		fechaSincronizar = Calendar.getInstance().getTime();
		tipoSin = 1;
		operacion = "SIRN";
	}
	
	
	public List<Interrupcion> getInterrupciones() {

		return interrupciones;
	}

	public void setInterrupciones(List<Interrupcion> interrupciones) {
		this.interrupciones = interrupciones;
	}
	
	public Interrupcion getInterrupcion() {
		return interrupcion;
	}


	public void setInterrupcion(Interrupcion interrupcion) {
		this.interrupcion = interrupcion;
	}


	public List<SelectItem> getItemsTipoNotificacion() {
		TipoNotificacionService service=new TipoNotificacionService();
		itemsTipoNotificacion=new ArrayList<SelectItem>();
		for(TipoNotificacion tp:service.getAllRows()){
			itemsTipoNotificacion.add(new SelectItem(tp,tp.getCodTipNotificacion()+"-"+tp.getDescripcion()));
		}
		return itemsTipoNotificacion;
	}

	public void setItemsTipoNotificacion(List<SelectItem> itemsTipoNotificacion) {
		this.itemsTipoNotificacion = itemsTipoNotificacion;
	}

	
	public List<SelectItem> getItemsTipoNotificacion1() {
		TipoNotificacionService service=new TipoNotificacionService();
		itemsTipoNotificacion1=new ArrayList<SelectItem>();
		for(TipoNotificacion tp:service.getAllRows1()){
			itemsTipoNotificacion1.add(new SelectItem(tp,tp.getCodTipNotificacion()+"-"+tp.getDescripcion()));
		}
		return itemsTipoNotificacion1;
	}


	public void setItemsTipoNotificacion1(List<SelectItem> itemsTipoNotificacion1) {
		this.itemsTipoNotificacion1 = itemsTipoNotificacion1;
	}
	
	
	public List<SelectItem> getItemsCausaInterrupcion(long tipo) {
		CausaInterrupcionService service = new CausaInterrupcionService();
		itemsCausaInterrupcion = new ArrayList<SelectItem>();
		for(CausaInterrupcion tp:service.getAllRows()){
			if(tipo==1 && (!tp.getCodCauInterrupcion().equals("B"))){
				itemsCausaInterrupcion.add(new SelectItem(tp,tp.getCodCauInterrupcion()+"-"+tp.getDescripcion()));
			}
			if(tipo==2 && (!tp.getCodCauInterrupcion().equals("E"))){
				itemsCausaInterrupcion.add(new SelectItem(tp,tp.getCodCauInterrupcion()+"-"+tp.getDescripcion()));
			}
		}
		return itemsCausaInterrupcion;
	}
	
	public void setItemsCausaInterrupcion(List<SelectItem> itemsCausaInterrupcion) {
		this.itemsCausaInterrupcion = itemsCausaInterrupcion;
	}
	
	public List<SelectItem> getItemsCausaInterrupcion1(long tipo) {
		CausaInterrupcionService service = new CausaInterrupcionService();
		itemsCausaInterrupcion1 = new ArrayList<SelectItem>();
		for(CausaInterrupcion tp:service.getAllRows1()){
			if(tipo==1 && (!tp.getCodCauInterrupcion().equals("B"))){
				itemsCausaInterrupcion1.add(new SelectItem(tp,tp.getCodCauInterrupcion()+"-"+tp.getDescripcion()));
			}
			if(tipo==2 && (!tp.getCodCauInterrupcion().equals("E"))){
				itemsCausaInterrupcion1.add(new SelectItem(tp,tp.getCodCauInterrupcion()+"-"+tp.getDescripcion()));
			}
		}
		return itemsCausaInterrupcion1;
	}

	public void setItemsCausaInterrupcion1(List<SelectItem> itemsCausaInterrupcion1) {
		this.itemsCausaInterrupcion1 = itemsCausaInterrupcion1;
	}
						  
	public List<SelectItem> getItemsCausaInterrupcion2() {
		CausaInterrupcionService service = new CausaInterrupcionService();
		itemsCausaInterrupcion2 = new ArrayList<SelectItem>();
		itemsCausaInterrupcion2.add(new SelectItem("", ""));
		for(CausaInterrupcion tp:service.getAllRows1()){
			itemsCausaInterrupcion2.add(new SelectItem(tp.getCodCauInterrupcion(),tp.getCodCauInterrupcion()+"-"+tp.getDescripcion()));
		}
		return itemsCausaInterrupcion2;
	}


	public void setItemsCausaInterrupcion2(List<SelectItem> itemsCausaInterrupcion2) {
		this.itemsCausaInterrupcion2 = itemsCausaInterrupcion2;
	}
	
	public List<SelectItem> getItemsSolFueMayor() {
		itemsSolFueMayor=new ArrayList<SelectItem>();
		itemsSolFueMayor.add(new SelectItem("", ""));
		itemsSolFueMayor.add(new SelectItem("F","SI"));
		itemsSolFueMayor.add(new SelectItem("N","NO"));
		return itemsSolFueMayor;
	}


	public void setItemsSolFueMayor(List<SelectItem> itemsSolFueMayor) {
		this.itemsSolFueMayor = itemsSolFueMayor;
	}
	
	public List<SelectItem> getItemsTenInterrupcion() {
		itemsTenInterrupcion=new ArrayList<SelectItem>();
		itemsTenInterrupcion.add(new SelectItem("", ""));
		itemsTenInterrupcion.add(new SelectItem("MA", "MUY ALTA TENSION"));
		itemsTenInterrupcion.add(new SelectItem("AT", "ALTA TENSION"));
		itemsTenInterrupcion.add(new SelectItem("MT", "MEDIA TENSION"));
		itemsTenInterrupcion.add(new SelectItem("BT", "BAJA TENSION"));
		return itemsTenInterrupcion;
	}


	public void setItemsTenInterrupcion(List<SelectItem> itemsTenInterrupcion) {
		this.itemsTenInterrupcion = itemsTenInterrupcion;
	}
	
	public List<SelectItem> getItemsSecTipico() {
		itemsSecTipico=new ArrayList<SelectItem>();
		itemsSecTipico.add(new SelectItem("", ""));
		itemsSecTipico.add(new SelectItem("2", "Sector Tipico 2"));
		itemsSecTipico.add(new SelectItem("3", "Sector Tipico 3"));
		itemsSecTipico.add(new SelectItem("4", "Sector Tipico 4"));
		itemsSecTipico.add(new SelectItem("5", "Sector Tipico 5"));
		itemsSecTipico.add(new SelectItem("6", "Sector Tipico 6"));	
		return itemsSecTipico;
	}

	public void setItemsSecTipico(List<SelectItem> itemsSecTipico) {
		this.itemsSecTipico = itemsSecTipico;
	}
	
	public List<SelectItem> getItemsNivelTensio() {
		itemsNivelTensio=new ArrayList<SelectItem>();
		itemsNivelTensio.add(new SelectItem("MA", "MUY ALTA TENSION"));
		itemsNivelTensio.add(new SelectItem("AT", "ALTA TENSION"));
		itemsNivelTensio.add(new SelectItem("MT", "MEDIA TENSION"));
		itemsNivelTensio.add(new SelectItem("BT", "BAJA TENSION"));
		return itemsNivelTensio;
	}

	public void setItemsNivelTensio(List<SelectItem> itemsNivelTensio) {
		this.itemsNivelTensio = itemsNivelTensio;
	}

	public List<SelectItem> getItemsfasInterrumpida() {
		itemsfasInterrumpida=new ArrayList<SelectItem>();
		itemsfasInterrumpida.add(new SelectItem("R", "FASE R"));
		itemsfasInterrumpida.add(new SelectItem("S", "FASE S"));
		itemsfasInterrumpida.add(new SelectItem("T", "FASE T"));
		itemsfasInterrumpida.add(new SelectItem("RS", "FASES RS"));
		itemsfasInterrumpida.add(new SelectItem("RT", "FASES RT"));
		itemsfasInterrumpida.add(new SelectItem("ST", "FASES ST"));
		itemsfasInterrumpida.add(new SelectItem("RST", "FASES RST"));
		return itemsfasInterrumpida;
	}


	public void setItemsfasInterrumpida(List<SelectItem> itemsfasInterrumpida) {
		this.itemsfasInterrumpida = itemsfasInterrumpida;
	}
	
	public List<SelectItem> getItemsSolFuerzaMayor() {
		itemsSolFuerzaMayor=new ArrayList<SelectItem>();
		itemsSolFuerzaMayor.add(new SelectItem("N", "NO"));
		itemsSolFuerzaMayor.add(new SelectItem("F", "SI"));
		return itemsSolFuerzaMayor;
	}


	public void setItemsSolFuerzaMayor(List<SelectItem> itemsSolFuerzaMayor) {
		this.itemsSolFuerzaMayor = itemsSolFuerzaMayor;
	}
	
	public List<SelectItem> getItemsZonAfectada() {
		itemsZonAfectada=new ArrayList<SelectItem>();
		itemsZonAfectada.add(new SelectItem("U", "ZONA URBANA"));
		itemsZonAfectada.add(new SelectItem("R", "ZONA RURAL"));
		itemsZonAfectada.add(new SelectItem("P", "ZONAS URBANAS Y RURALES"));
		return itemsZonAfectada;
	}


	public void setItemsZonAfectada(List<SelectItem> itemsZonAfectada) {
		this.itemsZonAfectada = itemsZonAfectada;
	}


	public List<SelectItem> getItemsOriInterrupcion() {
		itemsOriInterrupcion=new ArrayList<SelectItem>();
		itemsOriInterrupcion.add(new SelectItem("D", "DENTRO DEL SER"));
		itemsOriInterrupcion.add(new SelectItem("E", "EXTERNO AL SER"));
		return itemsOriInterrupcion;
	}


	public void setItemsOriInterrupcion(List<SelectItem> itemsOriInterrupcion) {
		this.itemsOriInterrupcion = itemsOriInterrupcion;
	}

	public List<SelectItem> getItemsSincronizar() {
		itemsSincronizar=new ArrayList<SelectItem>();
		itemsSincronizar.add(new SelectItem("SI", "SI"));
		itemsSincronizar.add(new SelectItem("NO", "NO"));
		return itemsSincronizar;
	}


	public void setItemsSincronizar(List<SelectItem> itemsSincronizar) {
		this.itemsSincronizar = itemsSincronizar;
	}
	
	
	public List<SelectItem> getItemsTipoRelevador() {
		itemsTipoRelevador=new ArrayList<SelectItem>();
		itemsTipoRelevador.add(new SelectItem("RE", "RECLOSER"));
		itemsTipoRelevador.add(new SelectItem("IN", "INTERRUPTOR"));
		itemsTipoRelevador.add(new SelectItem("SE", "SECCIONAMIENTO"));
		itemsTipoRelevador.add(new SelectItem("CU", "CUT OUT"));
		itemsTipoRelevador.add(new SelectItem("OT", "OTROS"));
		return itemsTipoRelevador;
	}


	public void setItemsTipoRelevador(List<SelectItem> itemsTipoRelevador) {
		this.itemsTipoRelevador = itemsTipoRelevador;
	}

	public List<SelectItem> getItemsRecCarga() {
		itemsRecCarga=new ArrayList<SelectItem>();
		itemsRecCarga.add(new SelectItem("NA", "NO ASIGNADO"));
		itemsRecCarga.add(new SelectItem("MF", "RECHAZO DE CARGA POR MINIMA FRECUENCIA"));
		itemsRecCarga.add(new SelectItem("MT", "RECHAZO DE CARGA POR MINIMA TENSION"));
		itemsRecCarga.add(new SelectItem("MA", "RECHAZO DE CARGA MANUAL"));
		return itemsRecCarga;
	}


	public void setItemsRecCarga(List<SelectItem> itemsRecCarga) {
		this.itemsRecCarga = itemsRecCarga;
	}

	
	public List<SelectItem> getItemsTipoInterrupcion(long tipo) {
		TipoInterrupcionService service = new TipoInterrupcionService();
		itemsTipoInterrupcion = new ArrayList<SelectItem>();
		/*for(TipoInterrupcion tp:service.getAllRows()){
			if(tipo==1 && (tp.getCodTipInterrupcion().equals("M")||tp.getCodTipInterrupcion().equals("E"))){
				itemsTipoInterrupcion.add(new SelectItem(tp,tp.getCodTipInterrupcion()+"-"+tp.getDescripcion()));
			}
			if(tipo==2 && (tp.getCodTipInterrupcion().equals("N")||tp.getCodTipInterrupcion().equals("R"))){
				itemsTipoInterrupcion.add(new SelectItem(tp,tp.getCodTipInterrupcion()+"-"+tp.getDescripcion()));
			}
		}*/
		for(TipoInterrupcion tp:service.getAllRows()){
			
				itemsTipoInterrupcion.add(new SelectItem(tp,tp.getCodTipInterrupcion()+"-"+tp.getDescripcion()));
			
		}
		return itemsTipoInterrupcion;
	}


	public void setItemsTipoInterrupcion(List<SelectItem> itemsTipoInterrupcion) {
		this.itemsTipoInterrupcion = itemsTipoInterrupcion;
	}
	
	public List<SelectItem> getItemsTipoInterrupcion1(long tipo) {
		TipoInterrupcionService service = new TipoInterrupcionService();
		itemsTipoInterrupcion1 = new ArrayList<SelectItem>();
		for(TipoInterrupcion tp:service.getAllRows1()){
			if(tipo==1 && (tp.getCodTipInterrupcion().equals("M")||tp.getCodTipInterrupcion().equals("E"))){
				itemsTipoInterrupcion1.add(new SelectItem(tp,tp.getCodTipInterrupcion()+"-"+tp.getDescripcion()));
			}
			if(tipo==2 && (tp.getCodTipInterrupcion().equals("N")||tp.getCodTipInterrupcion().equals("R"))){
				itemsTipoInterrupcion1.add(new SelectItem(tp,tp.getCodTipInterrupcion()+"-"+tp.getDescripcion()));
			}
		}
		return itemsTipoInterrupcion1;
	}


	public void setItemsTipoInterrupcion1(List<SelectItem> itemsTipoInterrupcion1) {
		this.itemsTipoInterrupcion1 = itemsTipoInterrupcion1;
	}
	
	public List<SelectItem> getItemsTipoInterrupcion2() {
		TipoInterrupcionService service = new TipoInterrupcionService();
		itemsTipoInterrupcion2 = new ArrayList<SelectItem>();
		
		itemsTipoInterrupcion2.add(new SelectItem("", ""));
		for(TipoInterrupcion tp:service.getAllRows()){
			itemsTipoInterrupcion2.add(new SelectItem(tp.getCodTipInterrupcion(),tp.getCodTipInterrupcion()+"-"+tp.getDescripcion()));
		}
		return itemsTipoInterrupcion2;
		
	}


	public void setItemsTipoInterrupcion2(List<SelectItem> itemsTipoInterrupcion2) {
		this.itemsTipoInterrupcion2 = itemsTipoInterrupcion2;
	}
	
	public List<SelectItem> getItemsModalidadDeteccion(long tipo) {
		ModalidadDeteccionService service = new ModalidadDeteccionService();
		itemsModalidadDeteccion  = new ArrayList<SelectItem>();
		for(ModalidadDeteccion tp:service.getAllRows()){
			
			if(tipo==1 && tp.getCodModDeteccion().equals("P")){
				itemsModalidadDeteccion.add(new SelectItem(tp,tp.getDescripcion()));
			}
			
			if(tipo==2 && !tp.getCodModDeteccion().equals("P")){
				itemsModalidadDeteccion.add(new SelectItem(tp,tp.getDescripcion()));
			}
			
		}
		return itemsModalidadDeteccion;
	}


	
	
	public void setItemsModalidadDeteccion(List<SelectItem> itemsModalidadDeteccion) {
		this.itemsModalidadDeteccion = itemsModalidadDeteccion;
	}

	public List<SelectItem> getItemsModalidadDeteccion1(long tipo) {
		ModalidadDeteccionService service = new ModalidadDeteccionService();
		itemsModalidadDeteccion1  = new ArrayList<SelectItem>();
		for(ModalidadDeteccion tp:service.getAllRows1()){
			if(tipo==1 && tp.getCodModDeteccion().equals("P")){
				itemsModalidadDeteccion1.add(new SelectItem(tp,tp.getDescripcion()));
			}
			
			if(tipo==2 && !tp.getCodModDeteccion().equals("P")){
				itemsModalidadDeteccion1.add(new SelectItem(tp,tp.getDescripcion()));
			}
			
		}
		return itemsModalidadDeteccion1;
	}


	public void setItemsModalidadDeteccion1(List<SelectItem> itemsModalidadDeteccion1) {
		this.itemsModalidadDeteccion1 = itemsModalidadDeteccion1;
	}
	
	public List<SelectItem> getItemsPeriodoMedicion() {
		PeriodoMedicionService service=new PeriodoMedicionService();
		itemsPeriodoMedicion = new ArrayList<SelectItem>();
		for(PeriodoMedicion tp:service.getAllRows()){
			itemsPeriodoMedicion.add(new SelectItem(tp,tp.getAno()+"-"+tp.getPeriodo()));
		}
		return itemsPeriodoMedicion;
	}
	
	public void setItemsPeriodoMedicion(List<SelectItem> itemsPeriodoMedicion) {
		this.itemsPeriodoMedicion = itemsPeriodoMedicion;
	}

	public List<SelectItem> getItemsPeriodoMedicion1() {
		PeriodoMedicionService service=new PeriodoMedicionService();
		itemsPeriodoMedicion1 = new ArrayList<SelectItem>();
		for(PeriodoMedicion tp:service.getAllRows1()){
			itemsPeriodoMedicion1.add(new SelectItem(tp,tp.getAno()+"-"+tp.getPeriodo()));
		}
		return itemsPeriodoMedicion1;
	}
	
	public void setItemsPeriodoMedicion1(List<SelectItem> itemsPeriodoMedicion1) {
		this.itemsPeriodoMedicion1 = itemsPeriodoMedicion1;
	}

	public List<SelectItem> getItemsLocalidad() {
		LocalidadService service=new LocalidadService();
		itemsLocalidad=new ArrayList<SelectItem>();
		for(Localidad tp:service.getAllRows()){
			itemsLocalidad.add(new SelectItem(tp,tp.getCodLocalidad()+"-"+tp.getNombre()));
		}
		return itemsLocalidad;
	}


	public void setItemsLocalidad(List<SelectItem> itemsLocalidad) {
		this.itemsLocalidad = itemsLocalidad;
	}


	public List<SelectItem> getItemsLocalidad1() {
		LocalidadService service=new LocalidadService();
		itemsLocalidad1=new ArrayList<SelectItem>();
		for(Localidad tp:service.getAllRows1()){
			itemsLocalidad1.add(new SelectItem(tp,tp.getCodLocalidad()+"-"+tp.getNombre()));
		}
		return itemsLocalidad1;
	}


	public void setItemsLocalidad1(List<SelectItem> itemsLocalidad1) {
		this.itemsLocalidad1 = itemsLocalidad1;
	}

	
	public String prepararCrear(){
		//Esto funciona siempre que el bean tenga ambito de sesion 
		
		fecIniProgramada = null;
		fecFinProgramada = null;
		fecNotificacion1 = null;
		fecNotificacion2 = null;
		fecIniInterrupcion = null;
		fecFinInterrupcion = null;
		fecOficio = null;
		fecFinComSalio = null;
		fecResol = null;
				
		setInterrupcion(new Interrupcion());
		getInterrupcion().setEstado("1");
		getInterrupcion().setPeriodoMedicion(periodoMedicion);
		
		return "/zonasegura/interrupcion/interrupcionCrear?faces-redirect=true";
	}
	
	public String prepararCrearNoProgramada(){
		//Esto funciona siempre que el bean tenga ambito de sesion 
		setInterrupcion(new Interrupcion());
		getInterrupcion().setEstado("1");
		getInterrupcion().setPeriodoMedicion(periodoMedicion);
		fecIniProgramada = null;
		fecFinProgramada = null;
		fecNotificacion1 = null;
		fecNotificacion2 = null;
		fecIniInterrupcion = null;
		fecFinInterrupcion = null;
		fecOficio = null;
		fecFinComSalio = null;
		fecResol = null;		
		
		return "/zonasegura/interrupcion/interrupcionCrearNoProgramada?faces-redirect=true";
	}
	
	public String prepararEditar(){
		
		
		try {
			cargarInterrupcionActual();
			return "/zonasegura/interrupcion/interrupcionEditar?faces-redirect=true";
		} catch (HibernateException e) {
			setMensaje(e.getMessage());
			return "/zonasegura/interrupcion/interrupcionListar?faces-redirect=true";
		}
		
	}
	
	public String prepararEditarNoProgramada(){
		
		
		try {
			cargarInterrupcionActual();
			return "/zonasegura/interrupcion/interrupcionEditarNoProgramada?faces-redirect=true";
		} catch (HibernateException e) {
			setMensaje(e.getMessage());
			return "/zonasegura/interrupcion/interrupcionListar?faces-redirect=true";
		}
	
		
	}

	public String prepararVer(){
		//TODO: Por implementar carga de la instancia
		//System.out.println("parametroNormaId:"+Utilidad.getParametro("parametroNormaId"));
		try {
			cargarInterrupcionActual();
			return "/zonasegura/interrupcion/interrupcionVer?faces-redirect=true";
		} catch (HibernateException e) {
			setMensaje(e.getMessage());
			return "/zonasegura/interrupcion/interrupcionListar?faces-redirect=true";
		}
	}
	
	public String prepararVerNoProgramada(){
		//TODO: Por implementar carga de la instancia
		//System.out.println("parametroNormaId:"+Utilidad.getParametro("parametroNormaId"));
		try {
			cargarInterrupcionActual();
			return "/zonasegura/interrupcion/interrupcionVerNoProgramada?faces-redirect=true";
		} catch (HibernateException e) {
			setMensaje(e.getMessage());
			return "/zonasegura/interrupcion/interrupcionListar?faces-redirect=true";
		}
	}
	
	public String salvar(){
				
		InterrupcionService service=new InterrupcionService();
		
		try {
			
			if(!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("N") &&
					!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("R"))
			{
				
				if(fecIniProgramada!=null){
					interrupcion.setFecIniProgramada(new java.sql.Timestamp(fecIniProgramada.getTime()));
				}else {
					interrupcion.setFecIniProgramada(null);
				}
				
				if(fecFinProgramada!=null){
					interrupcion.setFecFinProgramada(new java.sql.Timestamp(fecFinProgramada.getTime()));
				}else {
					interrupcion.setFecFinProgramada(null);
				}
				
				if(fecNotificacion1!=null){
					interrupcion.setFecNotificacion1(new java.sql.Timestamp(fecNotificacion1.getTime()));
				}else {
					interrupcion.setFecNotificacion1(null);
				}
				if(fecNotificacion2!=null){
					interrupcion.setFecNotificacion2(new java.sql.Timestamp(fecNotificacion2.getTime()));
				}else {
					interrupcion.setFecNotificacion2(null);
				}
					
				if (!interrupcion.getSolFueMayor().equals(null) && interrupcion.getDuracionExoFum() != null){
					if (interrupcion.getSolFueMayor().equals("F") && interrupcion.getDuracionExoFum().doubleValue() == 0){
						setMensaje("Si se presenta solicitud la Duración tiene que ser mayor a 0 horas ");			
		    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,getMensaje(), getMensaje()));
		    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		    			
		    			if(!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("N") &&
		    					!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("R"))
		    			{
		    				return "/zonasegura/interrupcion/interrupcionCrear?faces-redirect=true";
		    			} else {
		    				return "/zonasegura/interrupcion/interrupcionCrearNoProgramada?faces-redirect=true";
		    			}
					}
				}
				
				if(interrupcion.getDuracionExoFum() != null){
					if(interrupcion.getDuracionExoFum().doubleValue() > 0){
						Calendar fecCalculada = Calendar.getInstance();
						fecCalculada.setTime(interrupcion.getFecIniProgramada());
						fecCalculada.add(Calendar.SECOND, (int)(interrupcion.getDuracionExoFum().doubleValue() * 3600));
						
						fecFinExoneracion = fecCalculada.getTime();
						
						interrupcion.setFecFinExoneracion(new java.sql.Timestamp(fecFinExoneracion.getTime()));			
					}
				}
				
				if((interrupcion.getFecIniProgramada() != null) && (interrupcion.getFecFinProgramada() != null)) {
					if(interrupcion.getFecIniProgramada().after(interrupcion.getFecFinProgramada())){
		    			setMensaje("La fecha de inicio programada no puede ser posterior a su fin.");			
		    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,getMensaje(), getMensaje()));
		    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		    			
		    			if(!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("N") &&
		    					!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("R"))
		    			{
		    				return "/zonasegura/interrupcion/interrupcionCrear?faces-redirect=true";
		    			} else {
		    				return "/zonasegura/interrupcion/interrupcionCrearNoProgramada?faces-redirect=true";
		    			}
					}
				}
				if((interrupcion.getFecNotificacion1() != null) && (interrupcion.getFecNotificacion2() != null)) {
					if(interrupcion.getFecNotificacion1().after(interrupcion.getFecNotificacion2())){
		    			setMensaje("La fecha de la primera notificación no puede ser posterior a la de segunda Notificación.");			
		    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,getMensaje(), getMensaje()));
		    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		    			
		    			if(!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("N") &&
		    					!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("R"))
		    			{
		    				return "/zonasegura/interrupcion/interrupcionCrear?faces-redirect=true";
		    			} else {
		    				return "/zonasegura/interrupcion/interrupcionCrearNoProgramada?faces-redirect=true";
		    			}
					}
				}
			}
				
			if(fecIniInterrupcion!=null){
				interrupcion.setFecIniInterrupcion(new java.sql.Timestamp(fecIniInterrupcion.getTime()));
			}else {
				interrupcion.setFecIniInterrupcion(null);
			}
			if(fecFinInterrupcion!=null){
				interrupcion.setFecFinInterrupcion(new java.sql.Timestamp(fecFinInterrupcion.getTime()));
			}else {
				interrupcion.setFecFinInterrupcion(null);
			}
			if(fecOficio!=null){
				interrupcion.setFecOficio(new java.sql.Timestamp(fecOficio.getTime()));
			}else {
				interrupcion.setFecOficio(null);
			}
			if(fecFinComSalio!=null){
				interrupcion.setFecFinComSalio(new java.sql.Timestamp(fecFinComSalio.getTime()));
			}else {
				interrupcion.setFecFinComSalio(null);
			}
			if(fecResol!=null){
				interrupcion.setFecResol(new java.sql.Timestamp(fecResol.getTime()));
			}else {
				interrupcion.setFecResol(null);
			}
			
			if((interrupcion.getFecIniInterrupcion() != null) && (interrupcion.getFecFinInterrupcion() != null)) {
				if(interrupcion.getFecIniInterrupcion().after(interrupcion.getFecFinInterrupcion())){
	    			setMensaje("La fecha de inicio de Interrupción no puede ser posterior a su fin.");			
	    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,getMensaje(), getMensaje()));
	    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	    			
	    			if(!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("N") &&
	    					!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("R"))
	    			{
	    				return "/zonasegura/interrupcion/interrupcionCrear?faces-redirect=true";
	    			} else {
	    				return "/zonasegura/interrupcion/interrupcionCrearNoProgramada?faces-redirect=true";
	    			}
				}
			}
			
				
			
    		service.create(interrupcion);
    		
    		InterrupcionesxPeriodo();
    		
    		return "/zonasegura/interrupcion/interrupcionListar?faces-redirect=true";
        				
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			e.printStackTrace();
			
			if(!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("N") &&
					!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("R"))
			{
				return "/zonasegura/interrupcion/interrupcionCrear?faces-redirect=true";
			} else {
				return "/zonasegura/interrupcion/interrupcionCrearNoProgramada?faces-redirect=true";
			}

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			e.printStackTrace();
			
			if(!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("N") &&
					!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("R"))
			{
				return "/zonasegura/interrupcion/interrupcionCrear?faces-redirect=true";
			} else {
				return "/zonasegura/interrupcion/interrupcionCrearNoProgramada?faces-redirect=true";
			}
		}
		
		
	}
	
	public String actualizar(){
		
		InterrupcionService service=new InterrupcionService();
		
		try {
			
			if(!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("N") &&
				!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("R"))
			{
				
				if(fecIniProgramada!=null){
					interrupcion.setFecIniProgramada(new java.sql.Timestamp(fecIniProgramada.getTime()));
				}else {
					interrupcion.setFecIniProgramada(null);
				}
				
				if(fecFinProgramada!=null){
					interrupcion.setFecFinProgramada(new java.sql.Timestamp(fecFinProgramada.getTime()));
				}else {
					interrupcion.setFecFinProgramada(null);
				}
				
				if(fecNotificacion1!=null){
					interrupcion.setFecNotificacion1(new java.sql.Timestamp(fecNotificacion1.getTime()));
				}else {
					interrupcion.setFecNotificacion1(null);
				}
				if(fecNotificacion2!=null){
					interrupcion.setFecNotificacion2(new java.sql.Timestamp(fecNotificacion2.getTime()));
				}else {
					interrupcion.setFecNotificacion2(null);
				}
				
				if (!interrupcion.getSolFueMayor().equals(null) && interrupcion.getDuracionExoFum() != null){
					if (interrupcion.getSolFueMayor().equals("F") && interrupcion.getDuracionExoFum().doubleValue() == 0){
						setMensaje("Si se presenta solicitud la Duración tiene que ser mayor a 0 horas ");			
		    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,getMensaje(), getMensaje()));
		    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		    			
		    			if(!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("N") &&
		    					!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("R"))
		    			{
		    				return "/zonasegura/interrupcion/interrupcionCrear?faces-redirect=true";
		    			} else {
		    				return "/zonasegura/interrupcion/interrupcionCrearNoProgramada?faces-redirect=true";
		    			}
					}
				}
				
				
				if(interrupcion.getDuracionExoFum() != null){
					if(interrupcion.getDuracionExoFum().doubleValue() > 0){
						Calendar fecCalculada = Calendar.getInstance();
						fecCalculada.setTime(interrupcion.getFecIniProgramada());
						fecCalculada.add(Calendar.SECOND, (int)(interrupcion.getDuracionExoFum().doubleValue() * 3600));
						
						fecFinExoneracion = fecCalculada.getTime();
						
						interrupcion.setFecFinExoneracion(new java.sql.Timestamp(fecFinExoneracion.getTime()));			
					}
				}
				
				if((interrupcion.getFecIniProgramada() != null) && (interrupcion.getFecFinProgramada() != null)) {
					if(interrupcion.getFecIniProgramada().after(interrupcion.getFecFinProgramada())){
		    			setMensaje("La fecha de inicio programada no puede ser posterior a su fin.");			
		    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,getMensaje(), getMensaje()));
		    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		    			
		    			if(!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("N") &&
		    					!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("R"))
		    			{
		    				return "/zonasegura/interrupcion/interrupcionCrear?faces-redirect=true";
		    			} else {
		    				return "/zonasegura/interrupcion/interrupcionCrearNoProgramada?faces-redirect=true";
		    			}
					}
				}
				if((interrupcion.getFecNotificacion1() != null) && (interrupcion.getFecNotificacion2() != null)) {
					if(interrupcion.getFecNotificacion1().after(interrupcion.getFecNotificacion2())){
		    			setMensaje("La fecha de la primera notificación no puede ser posterior a la de segunda Notificación.");			
		    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,getMensaje(), getMensaje()));
		    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		    			
		    			if(!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("N") &&
		    					!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("R"))
		    			{
		    				return "/zonasegura/interrupcion/interrupcionCrear?faces-redirect=true";
		    			} else {
		    				return "/zonasegura/interrupcion/interrupcionCrearNoProgramada?faces-redirect=true";
		    			}
					}
				}
			}
				
			if(fecIniInterrupcion!=null){
				interrupcion.setFecIniInterrupcion(new java.sql.Timestamp(fecIniInterrupcion.getTime()));
			}else {
				interrupcion.setFecIniInterrupcion(null);
			}
			if(fecFinInterrupcion!=null){
				interrupcion.setFecFinInterrupcion(new java.sql.Timestamp(fecFinInterrupcion.getTime()));
			}else {
				interrupcion.setFecFinInterrupcion(null);
			}
			if(fecOficio!=null){
				interrupcion.setFecOficio(new java.sql.Timestamp(fecOficio.getTime()));
			}else {
				interrupcion.setFecOficio(null);
			}
			if(fecFinComSalio!=null){
				interrupcion.setFecFinComSalio(new java.sql.Timestamp(fecFinComSalio.getTime()));
			}else {
				interrupcion.setFecFinComSalio(null);
			}
			if(fecResol!=null){
				interrupcion.setFecResol(new java.sql.Timestamp(fecResol.getTime()));
			}else {
				interrupcion.setFecResol(null);
			}
				
			
			if((interrupcion.getFecIniInterrupcion() != null) && (interrupcion.getFecFinInterrupcion() != null)) {
				if(interrupcion.getFecIniInterrupcion().after(interrupcion.getFecFinInterrupcion())){
	    			setMensaje("La fecha de inicio de Interrupción no puede ser posterior a su fin.");			
	    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,getMensaje(), getMensaje()));
	    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	    			
	    			if(!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("N") &&
	    					!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("R"))
	    			{
	    				return "/zonasegura/interrupcion/interrupcionCrear?faces-redirect=true";
	    			} else {
	    				return "/zonasegura/interrupcion/interrupcionCrearNoProgramada?faces-redirect=true";
	    			}
				}
			}
			
			service.update(interrupcion);
			
			InterrupcionesxPeriodo();
			
			return "/zonasegura/interrupcion/interrupcionListar?faces-redirect=true";
			
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			if(!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("N") &&
					!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("R"))
			{
				return "/zonasegura/interrupcion/interrupcionEditar?faces-redirect=true";
			} else {
				return "/zonasegura/interrupcion/interrupcionEditarNoProgramada?faces-redirect=true";
			}
			
			
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error desconocido.", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			e.printStackTrace();
			
			if(!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("N") &&
					!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("R"))
			{
				return "/zonasegura/interrupcion/interrupcionEditar?faces-redirect=true";
			} else {
				return "/zonasegura/interrupcion/interrupcionEditarNoProgramada?faces-redirect=true";
			}
		
		}
		
	}
	
	public String eliminar(){
		String interrupcionId=Utilidad.getParametro("interrupcionId");
		InterrupcionService service=new InterrupcionService();
		
		
		try {
			
			interrupcion=service.ReadById(new Long(interrupcionId));
			
			service.delete(interrupcion.getInterrupcionId());
			
			/*
			interrupcion.setEstado("0");
			
			for(SuministroAfectado suministroAfectado:interrupcion.getSuministrosAfectado()){
				suministroAfectado.setEstado("0");
			}
			
			service.update(interrupcion);
			*/
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		
		InterrupcionesxPeriodo();
		
		return "/zonasegura/interrupcion/interrupcionListar?faces-redirect=true";
	}
	
	public void cargarInterrupcionActual(){
		String interrupcionId=Utilidad.getParametro("interrupcionId");
		String interrupcionIdX=Utilidad.getParametro("interrupcionIdX");
		long interrupcionLong = 0 ;
		
		if(interrupcionId != null){
			interrupcionLong = Long.valueOf(interrupcionId);
			origen = 1;
		}
		
		if(interrupcionIdX != null){
			interrupcionLong = Long.valueOf(interrupcionIdX);
			origen = 0;
		}
		
	
		InterrupcionService service=new InterrupcionService();
		
		
		try {
			interrupcion=service.ReadById(interrupcionLong);
			
			if(!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("N") &&
					!interrupcion.getTipoInterrupcion().getCodTipInterrupcion().equals("R")){
				
				if(interrupcion.getFecIniProgramada()!=null) {
					fecIniProgramada = new java.util.Date( interrupcion.getFecIniProgramada().getTime());				
				}else {
					fecIniProgramada = null;
				}
				
				if(interrupcion.getFecFinProgramada()!=null) {
					fecFinProgramada = new java.util.Date( interrupcion.getFecFinProgramada().getTime());				
				}else {
					fecFinProgramada = null;
				}
				
				if(interrupcion.getFecNotificacion1()!=null) {
					fecNotificacion1 = new java.util.Date( interrupcion.getFecNotificacion1().getTime());				
				}else {
					fecNotificacion1 = null;
				}
				
				if(interrupcion.getFecNotificacion2()!=null) {
					fecNotificacion2 = new java.util.Date( interrupcion.getFecNotificacion2().getTime());				
				}else {
					fecNotificacion2 = null;
				}
				
				if(interrupcion.getFecFinExoneracion()!=null) {
					fecFinExoneracion = new java.util.Date( interrupcion.getFecFinExoneracion().getTime());				
				}else {
					fecFinExoneracion = null;
				}
				
			}
			

			
			if(interrupcion.getFecIniInterrupcion()!=null) {
				fecIniInterrupcion = new java.util.Date( interrupcion.getFecIniInterrupcion().getTime());				
			}else {
				fecIniInterrupcion = null;
			}
			
			if(interrupcion.getFecFinInterrupcion()!=null) {
				fecFinInterrupcion = new java.util.Date( interrupcion.getFecFinInterrupcion().getTime());				
			}else {
				fecFinInterrupcion = null;
			}
			if(interrupcion.getFecOficio()!=null) {
				fecOficio = new java.util.Date( interrupcion.getFecOficio().getTime());				
			}else {
				fecOficio = null;
			}
			if(interrupcion.getFecFinComSalio()!=null) {
				fecFinComSalio = new java.util.Date( interrupcion.getFecFinComSalio().getTime());				
			}else {
				fecFinComSalio = null;
			}
			if(interrupcion.getFecResol()!=null) {
				fecResol = new java.util.Date( interrupcion.getFecResol().getTime());				
			}else {
				fecResol = null;
			}
				
			
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			throw new HibernateException("No se puede cargar Interrupcion.", e);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		
	}
	
	public String InterrupcionesxPeriodo() {
		InterrupcionService service=new InterrupcionService();
		
		codInterrupcionFilter = null;
		tipInterrupcionFilter = null;
		cauInterrupcionFilter = null;
		solFueMayorFilter = null;
		
		fecIniProFilter = null;
		fecFinProFilter = null;
		fecIniIntFilter = null;
		fecFinIntFilter = null;
		secTipicoFilter = null;

		NumSumRegEstimadosFilter = null;
		NumSumRegAfectadosFilter = null;
		NumCliLibAfectadosFilter = null;
		tenInterrupcionFilter = null;
		anexo1074Filter = null;
		
		concesionesFilter = null;
		motivoFilter = null;
		localizacionFallaFilter = null;
		zonaAfectadaFilter = null;
		oriInterrupcionFilter = null;
		sincronizarFilter = null;

		
		if(periodoMedicion.getPerMedicionId() > 0) {
			interrupciones= service.getAllRows1(periodoMedicion.getPerMedicionId());
		}
		return "/zonasegura/interrupcion/interrupcionListar?faces-redirect=true";
	}
	
	
	
	public void  filtroChanged(AjaxBehaviorEvent event) {  
		
		InterrupcionesxPeriodo();
	}
	
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}


	public PeriodoMedicion getPeriodoMedicion() {
		return periodoMedicion;
	}


	public void setPeriodoMedicion(PeriodoMedicion periodoMedicion) {
		this.periodoMedicion = periodoMedicion;
	}


	public String getCodInterrupcionFilter() {
		return codInterrupcionFilter;
	}


	public void setCodInterrupcionFilter(String codInterrupcionFilter) {
		this.codInterrupcionFilter = codInterrupcionFilter;
	}


	public int getOrigen() {
		return origen;
	}


	public void setOrigen(int origen) {
		this.origen = origen;
	}


	public String getTipInterrupcionFilter() {
		return tipInterrupcionFilter;
	}


	public void setTipInterrupcionFilter(String tipInterrupcionFilter) {
		this.tipInterrupcionFilter = tipInterrupcionFilter;
	}


	public String getCauInterrupcionFilter() {
		return cauInterrupcionFilter;
	}


	public void setCauInterrupcionFilter(String cauInterrupcionFilter) {
		this.cauInterrupcionFilter = cauInterrupcionFilter;
	}

	public String getSolFueMayorFilter() {
		return solFueMayorFilter;
	}


	public void setSolFueMayorFilter(String solFueMayorFilter) {
		this.solFueMayorFilter = solFueMayorFilter;
	}

	public java.util.Date getFecIniProFilter() {
		return fecIniProFilter;
	}
	
	
	public void setFecIniProFilter(java.util.Date fecIniProFilter) {
		this.fecIniProFilter = fecIniProFilter;
	}

	public String getTenInterrupcionFilter() {
		return tenInterrupcionFilter;
	}


	public void setTenInterrupcionFilter(String tenInterrupcionFilter) {
		this.tenInterrupcionFilter = tenInterrupcionFilter;
	}
	
	public String getSecTipicoFilter() {
		return secTipicoFilter;
	}


	public void setSecTipicoFilter(String secTipicoFilter) {
		this.secTipicoFilter = secTipicoFilter;
	}


	public java.util.Date getFecFinProFilter() {
		return fecFinProFilter;
	}


	public void setFecFinProFilter(java.util.Date fecFinProFilter) {
		this.fecFinProFilter = fecFinProFilter;
	}

	public java.util.Date getFecIniIntFilter() {
		return fecIniIntFilter;
	}


	public void setFecIniIntFilter(java.util.Date fecIniIntFilter) {
		this.fecIniIntFilter = fecIniIntFilter;
	}


	public java.util.Date getFecFinIntFilter() {
		return fecFinIntFilter;
	}


	public void setFecFinIntFilter(java.util.Date fecFinIntFilter) {
		this.fecFinIntFilter = fecFinIntFilter;
	}
	
	
	public Long getNumSumRegEstimadosFilter() {
		return NumSumRegEstimadosFilter;
	}


	public void setNumSumRegEstimadosFilter(Long numSumRegEstimadosFilter) {
		NumSumRegEstimadosFilter = numSumRegEstimadosFilter;
	}	
	
	public Long getNumSumRegAfectadosFilter() {
		return NumSumRegAfectadosFilter;
	}


	public void setNumSumRegAfectadosFilter(Long numSumRegAfectadosFilter) {
		NumSumRegAfectadosFilter = numSumRegAfectadosFilter;
	}


	public Long getNumCliLibAfectadosFilter() {
		return NumCliLibAfectadosFilter;
	}


	public void setNumCliLibAfectadosFilter(Long numCliLibAfectadosFilter) {
		NumCliLibAfectadosFilter = numCliLibAfectadosFilter;
	}

	
	public Filter<?> getFilterTipoInterrupcion() {
        return new Filter<Interrupcion>() {
            public boolean accept(Interrupcion t) {
            	String tipoInterrupcion = getTipInterrupcionFilter();
                if (tipoInterrupcion == null || tipoInterrupcion.length() == 0 || tipoInterrupcion.equals(t.getTipoInterrupcion().getCodTipInterrupcion())) {
                    return true;
                }
                return false;
            }
        };
    }
	
	
	public Filter<?> getFilterCausaInterrupcion() {
        return new Filter<Interrupcion>() {
            public boolean accept(Interrupcion t) {
            	String causaInterrupcion = getCauInterrupcionFilter();
                if (causaInterrupcion == null || causaInterrupcion.length() == 0 || causaInterrupcion.equals(t.getCausaInterrupcion().getCodCauInterrupcion())) {
                    return true;
                }
                return false;
            }
        };
    }
	
	public Filter<?> getFilterSolFueMayor() {
        return new Filter<Interrupcion>() {
            public boolean accept(Interrupcion t) {
            	String solFueMayor = getSolFueMayorFilter();
                if (solFueMayor == null || solFueMayor.length() == 0 || solFueMayor.equals(t.getSolFueMayor())) {
                    return true;
                }
                return false;
            }
        };
    }
	
	public Filter<?> getFiltertenInterrupcion() {
        return new Filter<Interrupcion>() {
            public boolean accept(Interrupcion t) {
            	String tenInterrupcion = getTenInterrupcionFilter();
                if (tenInterrupcion == null || tenInterrupcion.length() == 0 || tenInterrupcion.equals(t.getTenInterrupcion())) {
                    return true;
                }
                return false;
            }
        };
    }
	
	public Filter<?> getFilterSecTipico() {
        return new Filter<Interrupcion>() {
            public boolean accept(Interrupcion t) {
            	String secTipico = getSecTipicoFilter();
                if (secTipico == null || secTipico.length() == 0 || secTipico.equals(t.getSecTipico())) {
                    return true;
                }
                return false;
            }
        };
    }
	
	public Filter<?> getFilterFecIniProgramada() {
        return new Filter<Interrupcion>() {
            public boolean accept(Interrupcion interrupcion) {
                Date fechaIngresada = getFecIniProFilter();
                if (fechaIngresada == null) {
                	
                	return true;
                }else {
                	
                	Calendar calendar1 = Calendar.getInstance();
                	Calendar calendar2 = Calendar.getInstance();
                	
                	calendar1.setTime(fechaIngresada);
                	
                	if(interrupcion.getFecIniProgramada() == null){
                		return false;
                	}
                	
                	calendar2.setTime(interrupcion.getFecIniProgramada()); 
                	
                	if(calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR) && calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH) &&
                			calendar1.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH)){
                		
                		return true;
                		
                	}else{
                		return false;
                	}
                    
                }
            }
        };
	}	
	
	public Filter<?> getFilterFecFinProgramada() {
        return new Filter<Interrupcion>() {
            public boolean accept(Interrupcion interrupcion) {
                Date fechaIngresada = getFecFinProFilter();
                if (fechaIngresada == null) {
                	
                	return true;
                }else {
                	
                	Calendar calendar1 = Calendar.getInstance();
                	Calendar calendar2 = Calendar.getInstance();
                	
                	calendar1.setTime(fechaIngresada);
                	
                	if(interrupcion.getFecFinProgramada() == null){
                		return false;
                	}
                	
                	calendar2.setTime(interrupcion.getFecFinProgramada()); 
                	
                	if(calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR) && calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH) &&
                			calendar1.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH)){
                		
                		return true;
                		
                	}else{
                		return false;
                	}
                    
                }
            }
        };
	}
	
	public Filter<?> getFilterFecIniInt() {
        return new Filter<Interrupcion>() {
            public boolean accept(Interrupcion interrupcion) {
                Date fechaIngresada = getFecIniIntFilter();
                if (fechaIngresada == null) {
                	
                	return true;
                }else {
                	
                	Calendar calendar1 = Calendar.getInstance();
                	Calendar calendar2 = Calendar.getInstance();
                	
                	calendar1.setTime(fechaIngresada);
                	
                	if(interrupcion.getFecIniInterrupcion() == null){
                		return false;
                	}
                	
                	calendar2.setTime(interrupcion.getFecIniInterrupcion()); 
                	
                	if(calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR) && calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH) &&
                			calendar1.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH)){
                		
                		return true;
                		
                	}else{
                		return false;
                	}
                    
                }
            }
        };
	}
	
	public Filter<?> getFilterFecFinInt() {
        return new Filter<Interrupcion>() {
            public boolean accept(Interrupcion interrupcion) {
                Date fechaIngresada = getFecFinIntFilter();
                if (fechaIngresada == null) {
                	
                	return true;
                }else {
                	
                	Calendar calendar1 = Calendar.getInstance();
                	Calendar calendar2 = Calendar.getInstance();
                	
                	calendar1.setTime(fechaIngresada);
                	
                	if(interrupcion.getFecFinInterrupcion() == null){
                		return false;
                	}
                	
                	calendar2.setTime(interrupcion.getFecFinInterrupcion()); 
                	
                	if(calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR) && calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH) &&
                			calendar1.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH)){
                		
                		return true;
                		
                	}else{
                		return false;
                	}
                    
                }
            }
        };
	}

	public Filter<?> getNumSumRegEstimados() {
	    return new Filter<Interrupcion>() {
	        public boolean accept(Interrupcion item) {
	            Long filter = getNumSumRegEstimadosFilter();
	            if (filter == null || filter == 0 ) {
	                return true;
	            }else {
	            	if(item.getNumSumRegEstimados() == 0){
	            		return false;
	            	}
	            	
	            	return (item.getNumSumRegEstimados() > filter.longValue());
	            	
	            }
	        }
	    };
	}


	public Filter<?> getNumSumRegAfectados() {
	    return new Filter<Interrupcion>() {
	        public boolean accept(Interrupcion item) {
	            Long filter = getNumSumRegAfectadosFilter();
	            if (filter == null || filter == 0 ) {
	                return true;
	            }else {
	            	if(item.getNumSumRegAfectados() == 0){
	            		return false;
	            	}
	            	
	            	return (item.getNumSumRegAfectados() > filter.longValue());
	            	
	            }
	        }
	    };
	}

	public Filter<?> getNumCliLibAfectados() {
	    return new Filter<Interrupcion>() {
	        public boolean accept(Interrupcion item) {
	            Long filter = getNumCliLibAfectadosFilter();
	            if (filter == null || filter == 0 ) {
	                return true;
	            }else {
	            	if(item.getNumCliLibAfectados() == 0){
	            		return false;
	            	}
	            	
	            	return (item.getNumCliLibAfectados() > filter.longValue());
	            	
	            }
	        }
	    };
	}

	public String sincronizarInterrupcion(){

		ConectaDb db = new ConectaDb("CALIDAD");		
        Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        int ctos;
        String result;
		try {
		
			cstmt = cn.prepareCall("{CALL CAL_SUMINISTRO_PACKAGE.SINCRONIZAR_INTERRUPCIONES(?,?,?,?,?)}"); 
							
			cstmt.setString(1,periodoMedicion.getAno());
			cstmt.setString(2,periodoMedicion.getPeriodo());
			cstmt.setLong(3,tipoSin);
			cstmt.setString(4,operacion);
			cstmt.setDate(5, new java.sql.Date(fechaSincronizar.getTime()));

			System.out.println("ANO:" + periodoMedicion.getAno());
			System.out.println("MES:" + periodoMedicion.getPeriodo());
			System.out.println("TIPO SIN:" + tipoSin);
			System.out.println("OPERACION:" + operacion);
			System.out.println("FECHA SIN:" + fechaSincronizar);
				
			cstmt.execute();
			cstmt.close();
		
			
		
		} catch (SQLException e) {
	        result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	            
	    } finally {
	        try {
	            cn.close();
	        } catch (SQLException e) {
	            result = e.getMessage();
				setMensaje(e.getMessage()+":"+e.getCause());
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	        }
	    }
		
		InterrupcionesxPeriodo();
		
		return "/zonasegura/interrupcion/interrupcionListar?faces-redirect=true";		
	}

	
	public String compensarInterrupciones(){

		ConectaDb db = new ConectaDb("CALIDAD");		
        Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        int ctos;
        String result;
		try {
		
			cstmt = cn.prepareCall("{CALL CAL_SUMINISTRO_PACKAGE.COMPENSAR_INTERRUPCIONES(?,?)}"); 
							
			cstmt.setString(1,periodoMedicion.getAno());
			cstmt.setString(2,periodoMedicion.getSemestre());
							
			ctos = cstmt.executeUpdate();
		
			if (ctos == 0) {
				result = "0 filas afectadas";
			}
		
		} catch (SQLException e) {
	        result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	            
	    } finally {
	        try {
	            cn.close();
	        } catch (SQLException e) {
	            result = e.getMessage();
				setMensaje(e.getMessage()+":"+e.getCause());
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	        }
	    }
		
		InterrupcionesxPeriodo();
		
		return "/zonasegura/interrupcion/interrupcionListar?faces-redirect=true";		
	}

	public String calcularIndicadores074(){
		ConectaDb db = new ConectaDb("CALIDAD");		
        Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        int ctos;
        String result;
		try {
		
			cstmt = cn.prepareCall("{CALL CAL_SUMINISTRO_PACKAGE.CALCULAR_INDICADORES_074(?)}"); 
							
			cstmt.setLong(1,periodoMedicion.getPerMedicionId());
							
			ctos = cstmt.executeUpdate();
		
			if (ctos == 0) {
				result = "0 filas afectadas";
			}
		
		} catch (SQLException e) {
	        result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	            
	    } finally {
	        try {
	            cn.close();
	        } catch (SQLException e) {
	            result = e.getMessage();
				setMensaje(e.getMessage()+":"+e.getCause());
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	        }
	    }
		
		InterrupcionesxPeriodo();
		return null;
	}
	
	public void exportaranexo1_074_Detail() {
final FacesContext facesContext = FacesContext.getCurrentInstance();
	    
	    String rutaArchivoOut;
	    String rutaUp;
	    rutaUp = facesContext.getExternalContext().getRealPath("/reportes/Anexo1074Detallado.xlsx");
	    String path = System.getProperty("uploads.folder");
	    String nombreArchivo;
	    
	    System.out.print(path + "\n");
	    
	   nombreArchivo = "Anexo_1" + "_" + getPeriodoMedicion().getAno() + "_" + getPeriodoMedicion().getPeriodo() + "_"+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+"_Detail.xlsx";
	    
	    rutaArchivoOut = path + nombreArchivo;
	    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);
		XSSFSheet objHssfSheet = null;
		
		try  {
	    
	    InterrupcionService service = new InterrupcionService();
	    objHssfSheet = objHssfWorkbook.getSheetAt(0);
	    
			
		int intRow = 1;
		int intCell = 0; 
		
		EmpresaService empService = new EmpresaService();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		List<VAnexoIDetail> lista = service.getAnexo01Prox074Detail(periodoMedicion.getPerMedicionId());
		
		for (VAnexoIDetail inter : lista) {
			
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell, empService.getAllRows().get(0).getEmpresa());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 1, inter.getSisElectrico());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 2, inter.getCodSecOsinerg());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 3, inter.getCodInterrupcion());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 4, inter.getTipInsSalio());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 5, inter.getCodInsSalio());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 6, inter.getTipInsFalla());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 7, inter.getCodInsFalla());
			
			if(inter.getFecIniInterrupcion()!= null) {
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 8, sdf.format(inter.getFecIniInterrupcion()));				
			} else {
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 8, inter.getFecIniInterrupcion());
			}
			
			if(inter.getFecFinComSalio()!= null) {
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 9, sdf.format(inter.getFecFinComSalio()));				
			} else {
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 9, inter.getFecFinComSalio());
			}
			
			if(inter.getFecFinInterrupcion()!= null) {
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 10, sdf.format(inter.getFecFinInterrupcion()));				
			} else {
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 10, inter.getFecFinInterrupcion());
			}
			
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 11, inter.getNumSumRegAfectados());
			
			if(inter.getPotIntEstimada() != null){
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 12, inter.getPotIntEstimada());
			}
			
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 13, inter.getDurHorSuministro());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 14, inter.getNaturaleza());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 15, inter.getNivelTension());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 16, inter.getPropiedad());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 17, inter.getResponsable());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 18, inter.getCodCausa074());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 19, inter.getSolFueMayor());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 20, inter.getTipoRelevador().substring(0, 1));
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 21, inter.getCodosi());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 22, inter.getNroClieSisElectrico());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 23, inter.getSaifi());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 24, inter.getSaidi());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 25, inter.getCausa());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 26, inter.getResponsabilidad());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 27, inter.getMeses());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 28, inter.getSectorTipico());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 29, inter.getClientesXSectorTipico());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 30, inter.getSaifiSt());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 31, inter.getSaidiSt());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 32, inter.getClientesElsm());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 33, inter.getSaifiElsm());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 34, inter.getSaidiElsm());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 35, inter.getZonal());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 36, inter.getTipoInterrupcion());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 37, inter.getDuracion());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 38, inter.getSer());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 39, inter.getNIC());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 40, inter.getFactorK());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 41, inter.getDic());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 42, inter.getSemestre());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 43, inter.getAlimentadorMt());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 44, inter.getNroClieAlimentador());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 45, inter.getSaifiAlimentador());
			ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 46, inter.getSaidiAlimentador());
			intRow++;
		}
		
		ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());

	    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
	    facesContext.responseComplete();
	    
	}
	
	public String exportaranexo1_074(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
	    
	    String rutaArchivoOut;
	    String rutaUp;
	    rutaUp = facesContext.getExternalContext().getRealPath("/reportes/Anexo1074.xlsx");
	    String path = System.getProperty("uploads.folder");
	    String nombreArchivo;
	    
	    System.out.print(path + "\n");
	    
	   nombreArchivo = "Anexo_1" + "_" + getPeriodoMedicion().getAno() + "_" + getPeriodoMedicion().getPeriodo() + "_"+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsx";
	    
	    rutaArchivoOut = path + nombreArchivo;
	    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);
		XSSFSheet objHssfSheet = null;
		
		try  {
	    
	    InterrupcionService service = new InterrupcionService();
	    objHssfSheet = objHssfWorkbook.getSheetAt(0);
	    
			
		int intRow = 1;
		int intCell = 0; 
		
		EmpresaService empService = new EmpresaService();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		for (Interrupcion inter : service.getAllRows1(periodoMedicion.getPerMedicionId())) {
			if(inter.getSuministrosAfectado() == null){
				continue;
			}
			
			SuministroAfectadoService suministroAfectadoService = new SuministroAfectadoService();
			if(suministroAfectadoService.getAllRows1(inter.getInterrupcionId()).size() == 0){
				continue;
			}
			
			if(inter.getAnexo1074().equals("SI")){
				
				BigDecimal duracion = new BigDecimal((inter.getFecFinInterrupcion().getTime() - inter.getFecIniInterrupcion().getTime())/(60*1000));
				
				if(duracion.floatValue() < 3) {
					continue;
				}
				
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell, empService.getAllRows().get(0).getEmpresa());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 1, inter.getSisElectrico());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 2, inter.getCodSecOsinerg());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 3, inter.getCodInterrupcion());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 4, inter.getTipInsSalio());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 5, inter.getCodInsSalio());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 6, inter.getTipInsFalla());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 7, inter.getCodInsFalla());
				
				if(inter.getFecIniInterrupcion()!= null) {
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 8, sdf.format(inter.getFecIniInterrupcion()));				
				} else {
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 8, inter.getFecIniInterrupcion());
				}
				
				if(inter.getFecFinComSalio()!= null) {
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 9, sdf.format(inter.getFecFinComSalio()));				
				} else {
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 9, inter.getFecFinComSalio());
				}
				
				if(inter.getFecFinInterrupcion()!= null) {
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 10, sdf.format(inter.getFecFinInterrupcion()));				
				} else {
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 10, inter.getFecFinInterrupcion());
				}
				
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 11, inter.getNumCliLibAfectados() + inter.getNumSumRegAfectados());
				
				if(inter.getPotIntEstimada() != null){
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 12, inter.getPotIntEstimada().multiply(new BigDecimal(1000)));//kW
				}
				
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 13, inter.getDurHorSuministro());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 14, inter.getNaturaleza());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 15, inter.getCodOriInterrupcion());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 16, inter.getPropiedad());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 17, inter.getResponsable());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 18, inter.getCodCausa074());
				
				if(!inter.getSolFueMayor().equals(null)) {
					if(inter.getSolFueMayor().equals("F")){
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 19, "S");
					} else {
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 19, inter.getSolFueMayor());
					}
				}else {
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 19, inter.getSolFueMayor());
				}
				
				if(!inter.getTipoRelevador().equals(null)){
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 20, inter.getTipoRelevador().substring(0, 1));
				}
				
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 21, inter.getCodosi());
				intRow++;
			}
		}
		
		ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());

	    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
	    facesContext.responseComplete();
		
		return null;
	}
	
	public String exportaranexo2_074(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
	    
	    String rutaArchivoOut;
	    String rutaUp;
	    rutaUp = facesContext.getExternalContext().getRealPath("/reportes/Anexo2074.xlsx");
	    String path = System.getProperty("uploads.folder");
	    String nombreArchivo;
	    
	    System.out.print(path + "\n");
	    
	    nombreArchivo = "Anexo_2" + "_" + getPeriodoMedicion().getAno() + "_" + getPeriodoMedicion().getPeriodo() + "_"+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsx";
	    rutaArchivoOut = path + nombreArchivo;
	    	    
	    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);

		XSSFSheet objHssfSheet = null;
		
		try  {
	    
	    Indicador074Service service = new Indicador074Service();
	    
	    objHssfSheet = objHssfWorkbook.getSheetAt(0);
		
		int intRow = 1;
		int intCell = 0;
		
		EmpresaService empService = new EmpresaService();
		
		for (Indicador074 indicador : service.getAllRows1(periodoMedicion.getPerMedicionId())) {
			
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell, empService.getAllRows().get(0).getEmpresa());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 1, indicador.getSisElectrico());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 2, indicador.getNroCliSisElectrico());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 3, indicador.getSaifiSisElectrico());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 4, indicador.getSaifiIntProgramadas());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 5, indicador.getSaifiIntNoProgramadas());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 6, indicador.getSaifiRecCarga());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 7, indicador.getSaifiInsDistribucion());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 8, indicador.getSaifiInsTransmision());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 9, indicador.getSaifiInsGeneracion());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 10, indicador.getSaifiCauPropias());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 11, indicador.getSaifiCauTerceros());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 12, indicador.getSaifiCauOtras());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 13, indicador.getSaifiCauFenomenos());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 14, indicador.getSaifiFueMayor());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 15, indicador.getSaidiSisElectrico());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 16, indicador.getSaidiIntProgramadas());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 17, indicador.getSaidiIntNoProgramadas());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 18, indicador.getSaidiRecCarga());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 19, indicador.getSaidiInsDistribucion());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 20, indicador.getSaidiInsTransmision());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 21, indicador.getSaidiInsGeneracion());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 22, indicador.getSaidiCauPropias());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 23, indicador.getSaidiCauTerceros());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 24, indicador.getSaidiCauOtras());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 25, indicador.getSaidiCauFenomenos());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 26, indicador.getSaidiFueMayor());
				intRow++;
				

		}
		
		ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());

	    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
	    facesContext.responseComplete();	
				
		return null;
	}
	
	void writeOutContent(final HttpServletResponse res, final File content, final String theFilename, String contentType) {
	   if (content == null)
	     return;
	   try {
	     res.setHeader("Pragma", "no-cache");
	    res.setDateHeader("Expires", 0);
	    res.setContentType(contentType);
	    res.setHeader("Content-disposition", "attachment; filename=" + theFilename);
	    fastChannelCopy(Channels.newChannel(new FileInputStream(content)), Channels.newChannel(res.getOutputStream()));
	  } catch (final IOException e) {
	    // TODO produce a error message <img src="http://s0.wp.com/wp-includes/images/smilies/icon_smile.gif?m=1129645325g" alt=":)" class="wp-smiley"> 
	  }
	}
	 
	void fastChannelCopy(final ReadableByteChannel src, final WritableByteChannel dest) throws IOException {
	  final ByteBuffer buffer = ByteBuffer.allocateDirect(1000 * 1024);
	  while (src.read(buffer) != -1) {
	    buffer.flip();
	    dest.write(buffer);
	    buffer.compact();
	  }
	  buffer.flip();
	  while (buffer.hasRemaining()){
	    dest.write(buffer);
	  }
	}	
	
	public List<SelectItem> getItemsTipInsSalio() {
		itemsTipInsSalio=new ArrayList<SelectItem>();
		itemsTipInsSalio.add(new SelectItem("1", "SET"));
		itemsTipInsSalio.add(new SelectItem("2", "ALIMENTADOR MT"));
		itemsTipInsSalio.add(new SelectItem("3", "SECCION ALIMENTADOR"));
		itemsTipInsSalio.add(new SelectItem("4", "SED MT/BT"));
		itemsTipInsSalio.add(new SelectItem("5", "LINEA AT"));
		itemsTipInsSalio.add(new SelectItem("6", "SISTEMA ELECTRICO"));
		return itemsTipInsSalio;
	}


	public void setItemsTipInsSalio(List<SelectItem> itemsTipInsSalio) {
		this.itemsTipInsSalio = itemsTipInsSalio;
	}


	public List<SelectItem> getItemsTipInsFalla() {
		itemsTipInsFalla=new ArrayList<SelectItem>();
		itemsTipInsFalla.add(new SelectItem("1", "LINEA AT"));
		itemsTipInsFalla.add(new SelectItem("2", "SET"));
		itemsTipInsFalla.add(new SelectItem("3", "ALIMENTADOR MT"));
		itemsTipInsFalla.add(new SelectItem("4", "SECCION ALIMENTADOR"));
		itemsTipInsFalla.add(new SelectItem("5", "SED MT/BT"));
		itemsTipInsFalla.add(new SelectItem("6", "EXTERNO"));
		return itemsTipInsFalla;
	}


	public void setItemsTipInsFalla(List<SelectItem> itemsTipInsFalla) {
		this.itemsTipInsFalla = itemsTipInsFalla;
	}

	public List<SelectItem> getItemsNaturaleza() {
		itemsNaturaleza=new ArrayList<SelectItem>();
		itemsNaturaleza.add(new SelectItem("PM", "PROGRAMADO,MANTENIMIENTO"));
		itemsNaturaleza.add(new SelectItem("PE", "PROGRAMADO,EXPANSION O REFORZAMIENTO"));
		itemsNaturaleza.add(new SelectItem("NF", "NO PROGRAMADO,FALLA"));
		itemsNaturaleza.add(new SelectItem("NO", "NO PROGRAMADO,OPERACION"));
		itemsNaturaleza.add(new SelectItem("NT", "NO PROGRAMADO,ACCION DE TERCEROS"));
		itemsNaturaleza.add(new SelectItem("NC", "NO PROGRAMADO,FENOMENOS NATURALES"));
		itemsNaturaleza.add(new SelectItem("NR", "RECHAZO DE CARGA"));
		return itemsNaturaleza;
	}


	public void setItemsNaturaleza(List<SelectItem> itemsNaturaleza) {
		this.itemsNaturaleza = itemsNaturaleza;
	}
	
	public List<SelectItem> getItemsCodOriInterrupcion() {
		itemsCodOriInterrupcion=new ArrayList<SelectItem>();
		itemsCodOriInterrupcion.add(new SelectItem("G", "INSTALACION DE GENERACION"));
		itemsCodOriInterrupcion.add(new SelectItem("T", "INSTALACION DE TRANSMISION"));
		itemsCodOriInterrupcion.add(new SelectItem("D", "INSTALACION DE DISTRIBUCION"));
		return itemsCodOriInterrupcion;
	}


	public void setItemsCodOriInterrupcion(List<SelectItem> itemsCodOriInterrupcion) {
		this.itemsCodOriInterrupcion = itemsCodOriInterrupcion;
	}


	public List<SelectItem> getItemsPropiedad() {
		itemsPropiedad=new ArrayList<SelectItem>();
		itemsPropiedad.add(new SelectItem("P", "PROPIA"));
		itemsPropiedad.add(new SelectItem("O", "EXTERNA"));
		return itemsPropiedad;
	}


	public void setItemsPropiedad(List<SelectItem> itemsPropiedad) {
		this.itemsPropiedad = itemsPropiedad;
	}


	public List<SelectItem> getItemsResponsable() {
		itemsResponsable=new ArrayList<SelectItem>();
		itemsResponsable.add(new SelectItem("P", "PROPIA"));
		itemsResponsable.add(new SelectItem("O", "OTRAS EMPRESAS ELECTRICAS"));
		itemsResponsable.add(new SelectItem("T", "TERCEROS"));
		itemsResponsable.add(new SelectItem("F", "FENOMENOS NATURALES"));
		return itemsResponsable;
	}


	public void setItemsResponsable(List<SelectItem> itemsResponsable) {
		this.itemsResponsable = itemsResponsable;
	}
	
	public List<SelectItem> getItemsCodCausa074() {
		itemsCodCausa074=new ArrayList<SelectItem>();
		itemsCodCausa074.add(new SelectItem("1", "POR MANTENIMIENTO"));
		itemsCodCausa074.add(new SelectItem("2", "POR EXPANSION O REFORZAMIENTO DE REDES"));
		itemsCodCausa074.add(new SelectItem("3", "AJUSTE INADECUADO DE LA PROTECCION"));
		itemsCodCausa074.add(new SelectItem("4", "BAJO NIVEL DE AISLAMIENTO(AISLADOR ROTO/TENSION INADECUADA)"));
		itemsCodCausa074.add(new SelectItem("5", "FALLA EQUIPO (TRANSFORMADOR,INTERRUPTOR,SECCIONADOR DE POTENCIA,ETC)"));
		itemsCodCausa074.add(new SelectItem("6", "FALLA EMPALME DE RED"));
		itemsCodCausa074.add(new SelectItem("7", "FALLA TERMINAL CABLE"));
		itemsCodCausa074.add(new SelectItem("8", "CAIDA CONDUCTOR DE RED"));
		itemsCodCausa074.add(new SelectItem("9", "CAIDA DE ESTRUCTURA"));
		itemsCodCausa074.add(new SelectItem("10", "CONTACTO DE RED CON ARBOL"));
		itemsCodCausa074.add(new SelectItem("11", "CONTACTO DE RED CON EDIFICACION"));
		itemsCodCausa074.add(new SelectItem("12", "CONTACTO ENTRE CONDUCTORES"));
		itemsCodCausa074.add(new SelectItem("13", "ERROR DE MANIOBRA"));
		itemsCodCausa074.add(new SelectItem("14", "CORTE DE EMERGENCIA (NO INCLUIDOS EN PM Y PE"));
		itemsCodCausa074.add(new SelectItem("15", "ANIMALES (FELINOS Y ROEDORES)"));
		itemsCodCausa074.add(new SelectItem("16", "PICADO DE CABLE POR PERSONAL PROPIO"));
		itemsCodCausa074.add(new SelectItem("17", "OTROS , POR FALLA EN COMPONENTE(S) DEL SISTEMA DE POTENCIA"));
		itemsCodCausa074.add(new SelectItem("18", "AVES"));
		itemsCodCausa074.add(new SelectItem("19", "COMETAS"));
		itemsCodCausa074.add(new SelectItem("20", "IMPACTO VEHICULAR"));
		itemsCodCausa074.add(new SelectItem("21", "VANDALISMO"));
		itemsCodCausa074.add(new SelectItem("22", "HURTO DE CONDUCTOR O ELEMENTO ELECTRICO"));
		itemsCodCausa074.add(new SelectItem("23", "CAIDA DE ARBOL"));
		itemsCodCausa074.add(new SelectItem("24", "PICADO DE CABLE"));
		itemsCodCausa074.add(new SelectItem("25", "CONTACTO ACCIDENTAL CON LINEA"));
		itemsCodCausa074.add(new SelectItem("26", "PEDIDO DE AUTORIDAD"));
		itemsCodCausa074.add(new SelectItem("27", "OTROS ,CAUSADOS POR TERCEROS"));
		itemsCodCausa074.add(new SelectItem("28", "DESCARGAS ATMOSFERICAS"));
		itemsCodCausa074.add(new SelectItem("29", "FUERTES VIENTOS"));
		itemsCodCausa074.add(new SelectItem("30", "INUNDACIONES"));
		itemsCodCausa074.add(new SelectItem("31", "SISMO"));
		itemsCodCausa074.add(new SelectItem("32", "OTROS FENOMENOS NATURALES Y/O AMBIENTALES"));
		itemsCodCausa074.add(new SelectItem("33", "POR MANTENIMIENTO"));
		itemsCodCausa074.add(new SelectItem("34", "POR EXPANSION O REFORZAMIENTO DE REDES"));
		itemsCodCausa074.add(new SelectItem("35", "FALLA SISTEMA INTERCONECTADO"));
		itemsCodCausa074.add(new SelectItem("36", "DEFICIT DE GENERACION"));
		itemsCodCausa074.add(new SelectItem("37", "OTROS, CAUSADO POR OTRA EMPRESA EXTERNA"));
		itemsCodCausa074.add(new SelectItem("38", "CUANDO LA INTERRUPCION ES PROVOCADA POR OTRA EMPRESA"));
		itemsCodCausa074.add(new SelectItem("39", "OTROS POR FALLA HUMANA"));
		
		return itemsCodCausa074;
	}


	public void setItemsCodCausa074(List<SelectItem> itemsCodCausa074) {
		this.itemsCodCausa074 = itemsCodCausa074;
	}


	
	public List<SelectItem> getItemsIntImportante() {
		itemsIntImportante=new ArrayList<SelectItem>();
		itemsIntImportante.add(new SelectItem("SI", "SI"));
		itemsIntImportante.add(new SelectItem("NO", "NO"));
		return itemsIntImportante;
	}


	public void setItemsIntImportante(List<SelectItem> itemsIntImportante) {
		this.itemsIntImportante = itemsIntImportante;
	}


	public List<SelectItem> getItemsAnexo1074() {
		itemsAnexo1074=new ArrayList<SelectItem>();
		itemsAnexo1074.add(new SelectItem("", ""));
		itemsAnexo1074.add(new SelectItem("SI", "SI"));
		itemsAnexo1074.add(new SelectItem("NO", "NO"));
		return itemsAnexo1074;
	}


	public void setItemsAnexo1074(List<SelectItem> itemsAnexo1074) {
		this.itemsAnexo1074 = itemsAnexo1074;
	}
	
	public List<SelectItem> getItemsSectorTipico() {
		itemsSectorTipico=new ArrayList<SelectItem>();
		itemsSectorTipico.add(new SelectItem("2", "Sector 2"));
		itemsSectorTipico.add(new SelectItem("3", "Sector 3"));
		itemsSectorTipico.add(new SelectItem("4", "Sector 4"));
		itemsSectorTipico.add(new SelectItem("5", "Sector 5"));
		itemsSectorTipico.add(new SelectItem("6", "Sector 6"));
		return itemsSectorTipico;
	}


	public void setItemsSectorTipico(List<SelectItem> itemsSectorTipico) {
		this.itemsSectorTipico = itemsSectorTipico;
	}

	public List<SelectItem> getItemsEstadoResolucion() {
		itemsEstadoResolucion=new ArrayList<SelectItem>();
		itemsEstadoResolucion.add(new SelectItem("P", "PENDIENTE"));
		itemsEstadoResolucion.add(new SelectItem("R", "CON RESOLUCIO"));
		return itemsEstadoResolucion;
	}


	public void setItemsEstadoResolucion(List<SelectItem> itemsEstadoResolucion) {
		this.itemsEstadoResolucion = itemsEstadoResolucion;
	}
	
	
	public String getAnexo1074Filter() {
		return anexo1074Filter;
	}


	public void setAnexo1074Filter(String anexo1074Filter) {
		this.anexo1074Filter = anexo1074Filter;
	}
	
	public String getConcesionesFilter() {
		return concesionesFilter;
	}


	public void setConcesionesFilter(String concesionesFilter) {
		this.concesionesFilter = concesionesFilter;
	}

	
	
	public Filter<?> getFilterAnexo1074() {
        return new Filter<Interrupcion>() {
            public boolean accept(Interrupcion t) {
            	String anexo1074 = getAnexo1074Filter();
                if (anexo1074 == null || anexo1074.length() == 0 || anexo1074.equals(t.getAnexo1074())) {
                    return true;
                }
                return false;
            }
        };
    }
	
	public Filter<?> getFilterConcesiones() {
        return new Filter<Interrupcion>() {
            public boolean accept(Interrupcion t) {
            	String concesiones = getConcesionesFilter();
                if (concesiones == null || concesiones.length() == 0 || concesiones.equals(t.getLeyConcesiones())) {
                    return true;
                }
                return false;
            }
        };
    }
	
	public List<SelectItem> getItemsAnexo10742() {
		itemsAnexo10742=new ArrayList<SelectItem>();
		itemsAnexo10742.add(new SelectItem("", ""));
		itemsAnexo10742.add(new SelectItem("SI","SI"));
		itemsAnexo10742.add(new SelectItem("NO","NO"));
		return itemsAnexo10742;
	}


	public void setItemsAnexo10742(List<SelectItem> itemsAnexo10742) {
		this.itemsAnexo10742 = itemsAnexo10742;
	}
	
	public List<SelectItem> getItemsConcesiones() {
		itemsConcesiones=new ArrayList<SelectItem>();
		itemsConcesiones.add(new SelectItem("", ""));
		itemsConcesiones.add(new SelectItem("SI","SI"));
		itemsConcesiones.add(new SelectItem("NO","NO"));
		return itemsConcesiones;
	}


	public void setItemsConcesiones(List<SelectItem> itemsConcesiones) {
		this.itemsConcesiones = itemsConcesiones;
	}


	public List<SelectItem> getItemsOperacion() {
		itemsOperacion= new ArrayList<SelectItem>();
		itemsOperacion.add(new SelectItem("SIRN","SOLO INSERTAR REGISTROS NUEVOS"));
		itemsOperacion.add(new SelectItem("AREIRN","ACTUALIZAR REGISTROS EXISTENTES E INSERTAR NUEVOS"));
		itemsOperacion.add(new SelectItem("EREITN","ELIMINAR REGISTROS EXISTENTES E INSERTAR NUEVAMENTE"));
		
		return itemsOperacion;
	}

	public void setItemsOperacion(List<SelectItem> itemsOperacion) {
		this.itemsOperacion = itemsOperacion;
	}


	public java.util.Date getFechaSincronizar() {
		return fechaSincronizar;
	}


	public void setFechaSincronizar(java.util.Date fechaSincronizar) {
		this.fechaSincronizar = fechaSincronizar;
	}


	public java.util.Date getFecFinComSalio() {
		return fecFinComSalio;
	}


	public void setFecFinComSalio(java.util.Date fecFinComSalio) {
		this.fecFinComSalio = fecFinComSalio;
	}


	public long getTipoSin() {
		return tipoSin;
	}


	public void setTipoSin(long tipoSin) {
		this.tipoSin = tipoSin;
	}


	public String getOperacion() {
		return operacion;
	}


	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}


	public List<SelectItem> getItemsTipoSin() {
		itemsTipoSin= new ArrayList<SelectItem>();
		itemsTipoSin.add(new SelectItem(1, "SINCRONIZAR REGISTROS DE LA FECHA ESPECIFICADA"));
		itemsTipoSin.add(new SelectItem(2, "SINCRONIZAR REGISTROS DEL PERIODO SELECCIONADO"));
		
		
		return itemsTipoSin;
	}


	public void setItemsTipoSin(List<SelectItem> itemsTipoSin) {
		this.itemsTipoSin = itemsTipoSin;
	}

	public void localidadChanged(AjaxBehaviorEvent event){
		/*
		if(!interrupcion.getLocalidad().getSisElectrico().substring(0, 2).equals("SR")) {
			if(interrupcion.getLocalidad().getSecTipico().equals("1") || 
					interrupcion.getLocalidad().getSecTipico().equals("2") ||
					interrupcion.getLocalidad().getSecTipico().equals("3")) {
				
				interrupcion.setZonAfectada("U");
			}
			
			if(interrupcion.getLocalidad().getSecTipico().equals("4") || 
					interrupcion.getLocalidad().getSecTipico().equals("5") ||
					interrupcion.getLocalidad().getSecTipico().equals("6")) {
				
				interrupcion.setZonAfectada("R");
			}
		}else {			
			interrupcion.setZonAfectada("R");
		}*/
		
		interrupcion.setSisElectrico(interrupcion.getLocalidad().getSisElectrico());
		interrupcion.setSecTipico(interrupcion.getLocalidad().getSecTipico());
	}
	
	public void tensionChanged(AjaxBehaviorEvent event){
		
		if(interrupcion.getZonAfectada().equals("R") && interrupcion.getTenInterrupcion().equals("AT")){
			
			interrupcion.setOriInterrupcion("E");
		}
		
		if(interrupcion.getZonAfectada().equals("R") && interrupcion.getTenInterrupcion().equals("MT")){
			
			interrupcion.setOriInterrupcion("D");
		}
		
		if(interrupcion.getZonAfectada().equals("R") && interrupcion.getTenInterrupcion().equals("BT")){
			
			interrupcion.setOriInterrupcion("D");
		}
		
		if(interrupcion.getZonAfectada().equals("P")){
			
			interrupcion.setOriInterrupcion("E");
		}
		
		if(interrupcion.getZonAfectada().equals("U")){
			
			interrupcion.setOriInterrupcion("E");
		}
		
	}

	public void zonaAfectadaChanged(AjaxBehaviorEvent event){
		
		if(interrupcion.getZonAfectada().equals("R") && interrupcion.getTenInterrupcion().equals("AT")){
			
			interrupcion.setOriInterrupcion("E");
		}
		
		if(interrupcion.getZonAfectada().equals("R") && interrupcion.getTenInterrupcion().equals("MT")){
			
			interrupcion.setOriInterrupcion("D");
		}
		
		if(interrupcion.getZonAfectada().equals("R") && interrupcion.getTenInterrupcion().equals("BT")){
			
			interrupcion.setOriInterrupcion("D");
		}
		
		if(interrupcion.getZonAfectada().equals("P")){
			
			interrupcion.setOriInterrupcion("E");
		}
		
		if(interrupcion.getZonAfectada().equals("U")){
			
			interrupcion.setOriInterrupcion("E");
		}
		
	}
	
	public void solFueMayorChanged(AjaxBehaviorEvent event){
		if(interrupcion.getSolFueMayor().equals("F")) {
			BigDecimal duracion;
			
			System.out.println("Llego1");
			System.out.println("Llego1");
			System.out.println("Llego1");
			System.out.println("Llego1");
			System.out.println("Llego1");
			
			if(fecIniProgramada != null && fecFinProgramada != null){
				System.out.println("Llego2");
				System.out.println("Llego2");
				System.out.println("Llego2");
				System.out.println("Llego2");
				System.out.println("Llego2");
				if(!fecIniProgramada.after(fecFinProgramada)){			
			
					duracion = new BigDecimal((fecFinProgramada.getTime() - fecIniProgramada.getTime())/(60*60*1000));
					interrupcion.setDuracionExoFum(duracion);
								
					fecFinExoneracion = fecFinProgramada;
					System.out.println("Llego3");
					System.out.println("Llego3");
					System.out.println("Llego3");
					System.out.println("Llego3");
					System.out.println("Llego3");
					//interrupcion.setFecFinExoneracion(new java.sql.Timestamp(fecFinExoneracion.getTime()));	
				}
			}
		}
		
		if(interrupcion.getSolFueMayor().equals("N")) {
			//interrupcion.setFecFinExoneracion(null);
			fecFinExoneracion = null;
			interrupcion.setDuracionExoFum(new BigDecimal(0));
		}
	}
	
	public void duracionChanged(AjaxBehaviorEvent event){
		if(interrupcion.getDuracionExoFum() != null){
			if(interrupcion.getDuracionExoFum().doubleValue() > 0){
				if(fecIniProgramada != null && fecFinProgramada != null){
					if(!fecIniProgramada.after(fecFinProgramada)){
						Calendar fecCalculada = Calendar.getInstance();
						fecCalculada.setTime(fecIniProgramada);
						fecCalculada.add(Calendar.SECOND, (int)(interrupcion.getDuracionExoFum().doubleValue() * 3600));
						
						fecFinExoneracion = fecCalculada.getTime();
						
						//interrupcion.setFecFinExoneracion(new java.sql.Timestamp(fecFinExoneracion.getTime()));
					}
				}
			}
			
			if(interrupcion.getDuracionExoFum().doubleValue() == 0) {
				
				if(fecIniProgramada != null && fecFinProgramada != null){
					if(!fecIniProgramada.after(fecFinProgramada)){
				
						fecFinExoneracion = fecIniProgramada;
						
						//interrupcion.setFecFinExoneracion(new java.sql.Timestamp(fecIniProgramada.getTime()));
					}
				}
				
			}
			
		}		
	}
	
	
	public String getMotivoFilter() {
		return motivoFilter;
	}


	public void setMotivoFilter(String motivoFilter) {
		this.motivoFilter = motivoFilter;
	}


	public String getLocalizacionFallaFilter() {
		return localizacionFallaFilter;
	}


	public void setLocalizacionFallaFilter(String localizacionFallaFilter) {
		this.localizacionFallaFilter = localizacionFallaFilter;
	}


	public String getZonaAfectadaFilter() {
		return zonaAfectadaFilter;
	}


	public void setZonaAfectadaFilter(String zonaAfectadaFilter) {
		this.zonaAfectadaFilter = zonaAfectadaFilter;
	}
	
	public Filter<?> getFilterZonaAfectada() {
        return new Filter<Interrupcion>() {
            public boolean accept(Interrupcion t) {
            	String zonaAfectada = getZonaAfectadaFilter();
                if (zonaAfectada == null || zonaAfectada.length() == 0 || zonaAfectada.equals(t.getZonAfectada())) {
                    return true;
                }
                return false;
            }
        };
    }


	public List<SelectItem> getItemsZonaAfectada() {
		itemsZonaAfectada=new ArrayList<SelectItem>();
		itemsZonaAfectada.add(new SelectItem("", ""));
		itemsZonaAfectada.add(new SelectItem("U", "ZONA URBANA"));
		itemsZonaAfectada.add(new SelectItem("R", "ZONA RURAL"));
		itemsZonaAfectada.add(new SelectItem("P", "ZONAS URBANAS Y RURALES"));
		return itemsZonaAfectada;
	}


	public void setItemsZonaAfectada(List<SelectItem> itemsZonaAfectada) {
		this.itemsZonaAfectada = itemsZonaAfectada;
	}


	public String getOriInterrupcionFilter() {
		return oriInterrupcionFilter;
	}


	public void setOriInterrupcionFilter(String oriInterrupcionFilter) {
		this.oriInterrupcionFilter = oriInterrupcionFilter;
	}

	public Filter<?> getFilterOriInterrupcion() {
        return new Filter<Interrupcion>() {
            public boolean accept(Interrupcion t) {
            	String oriInterrupcion = getOriInterrupcionFilter();
                if (oriInterrupcion == null || oriInterrupcion.length() == 0 || oriInterrupcion.equals(t.getOriInterrupcion())) {
                    return true;
                }
                return false;
            }
        };
    }

	public List<SelectItem> getItemsOrigenInterrupcion() {
		itemsOrigenInterrupcion=new ArrayList<SelectItem>();
		itemsOrigenInterrupcion.add(new SelectItem("", ""));
		itemsOrigenInterrupcion.add(new SelectItem("D", "DENTRO DEL SER"));
		itemsOrigenInterrupcion.add(new SelectItem("E", "EXTERNO AL SER"));
		return itemsOrigenInterrupcion;
	}


	public void setItemsOrigenInterrupcion(List<SelectItem> itemsOrigenInterrupcion) {
		this.itemsOrigenInterrupcion = itemsOrigenInterrupcion;
	}


	public String getSincronizarFilter() {
		return sincronizarFilter;
	}


	public void setSincronizarFilter(String sincronizarFilter) {
		this.sincronizarFilter = sincronizarFilter;
	}

	public Filter<?> getFilterSincronizar() {
        return new Filter<Interrupcion>() {
            public boolean accept(Interrupcion t) {
            	String sincronizar = getSincronizarFilter();
                if (sincronizar == null || sincronizar.length() == 0 || sincronizar.equals(t.getSincronizar())) {
                    return true;
                }
                return false;
            }
        };
    }

	public List<SelectItem> getItemsSincronizacion() {
		itemsSincronizacion=new ArrayList<SelectItem>();
		itemsSincronizacion.add(new SelectItem("", ""));
		itemsSincronizacion.add(new SelectItem("SI", "SI"));
		itemsSincronizacion.add(new SelectItem("NO", "NO"));
		return itemsSincronizacion;
	}


	public void setItemsSincronizacion(List<SelectItem> itemsSincronizacion) {
		this.itemsSincronizacion = itemsSincronizacion;
	}


	public String getCodigoInterCambi() {
		return codigoInterCambi;
	}


	public void setCodigoInterCambi(String codigoInterCambi) {
		this.codigoInterCambi = codigoInterCambi;
	}
	
	public String convertirNoProgrogamada(){
		String listaCodigos = "";
		
		if(codigoInterCambi.equals(null) || codigoInterCambi.equals("")){
			return null;
		}
		
		Sql sql = new Sql("CALIDAD");
		
		String[] listaCodigosArr = codigoInterCambi.trim().split(",");
		
		for (String codigo : listaCodigosArr) {
			listaCodigos = listaCodigos + "'" + codigo.trim() + "',";
		}
		
		listaCodigos = listaCodigos.substring(0, listaCodigos.length() - 1);
		
		String updateSql = "UPDATE CAL_INTERRUPCION SET COD_CAUSA_074 = '14', NATURALEZA = 'NO', RESPONSABLE = 'P', PK_TIP_INTERRUPCION_ID = (SELECT TIP_INTERRUPCION_ID FROM CAL_TIPO_INTERRUPCION WHERE COD_TIP_INTERRUPCION = 'N') " +
							"WHERE COD_INTERRUPCION NOT IN (" + listaCodigos + ") AND PK_PER_MEDICION_ID = " + String.valueOf(periodoMedicion.getPerMedicionId()) +" AND ESTADO = '1' AND " +
							"PK_TIP_INTERRUPCION_ID IN (SELECT TIP_INTERRUPCION_ID FROM CAL_TIPO_INTERRUPCION WHERE COD_TIP_INTERRUPCION IN ('E','M'))";
		/*
		System.out.println(updateSql);
		System.out.println(updateSql);
		System.out.println(updateSql);
		*/
		String resultado = sql.ejecuta(updateSql);			
		
		updateSql = "UPDATE CAL_INTERRUPCION SET SINCRONIZAR = 'NO' " +
					"WHERE COD_INTERRUPCION IN (" + listaCodigos + ") AND PK_PER_MEDICION_ID = " + String.valueOf(periodoMedicion.getPerMedicionId()) +" AND ESTADO = '1'";

		resultado = sql.ejecuta(updateSql);			
		
		
		InterrupcionesxPeriodo();
		
		return "/zonasegura/interrupcion/interrupcionListar?faces-redirect=true";		
	}
	
	public void handleEvent(ComponentSystemEvent event){
		UIExtendedDataTable dataTable = (UIExtendedDataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("idformint-tableInterrupcion");
		
		filasTotal = dataTable.getRowCount();		
		
		System.out.println(dataTable.getRowCount() + "-" + dataTable.getClientRows() + "-" + dataTable.getChildCount() + "-" + dataTable.getRows() + "-" + dataTable.getResourceBundleMap().size());

	}


	public long getFilasTotal() {
		return filasTotal;
	}


	public void setFilasTotal(long filasTotal) {
		this.filasTotal = filasTotal;
	}	
	
	public void actualizaFilas2(AjaxBehaviorEvent event){
		//UIExtendedDataTable dataTable = (UIExtendedDataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("idform-tableMedicion");
		UIExtendedDataTable dataTable = (UIExtendedDataTable) event.getComponent();
		filasTotal = dataTable.getRowCount();		
		
		Object originalKey = dataTable.getRowKey();
		selectionItems.clear();
		for (Object selectionKey : selection) {
			dataTable.setRowKey(selectionKey);
			if (dataTable.isRowAvailable()) {
				selectionItems.add((Interrupcion) dataTable.getRowData());
			}
		}		
		
		dataTable.setRowKey(originalKey);
		
		System.out.println(dataTable.getRowCount() + "-" + dataTable.getClientRows() + "-" + dataTable.getChildCount() + "-" + dataTable.getRows() + "-" + dataTable.getResourceBundleMap().size());

	}
	
	public void selectionListener(AjaxBehaviorEvent event) {
		UIExtendedDataTable dataTable = (UIExtendedDataTable) event.getComponent();
		Object originalKey = dataTable.getRowKey();
		selectionItems.clear();
		for (Object selectionKey : selection) {
			dataTable.setRowKey(selectionKey);
			if (dataTable.isRowAvailable()) {
				selectionItems.add((Interrupcion) dataTable.getRowData());
			}
		}
		
		dataTable.setRowKey(originalKey);
	}	
	
	public String exportartablainterrup(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
	    
	    String rutaArchivoOut;
	    String rutaUp;
	    rutaUp = facesContext.getExternalContext().getRealPath("/reportes/Anexointerrupcion.xlsx");
	    String path = System.getProperty("uploads.folder");
	    String nombreArchivo;
	    
	    System.out.print(path + "\n");
	    
	    nombreArchivo = "interrupcion" + "_" + getPeriodoMedicion().getAno() + "_" + getPeriodoMedicion().getPeriodo() + "_"+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsx";
	    rutaArchivoOut = path + nombreArchivo;
	    	    
	    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);

		XSSFSheet objHssfSheet = null;
		
		try  {
	    
		    InterrupcionService service = new InterrupcionService();
		    
		    objHssfSheet = objHssfWorkbook.getSheetAt(0);
			
			int intRow = 2;
			int intCell = 0;
			
			for (Interrupcion interrupcion : service.getAllRows1(periodoMedicion.getPerMedicionId())) {
				
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell, interrupcion.getTipoInterrupcion().getDescripcion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 1, interrupcion.getCodInterrupcion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 2, interrupcion.getCausaInterrupcion().getDescripcion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 3, interrupcion.getAnexo1074());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 4, interrupcion.getSolFueMayor());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 5, interrupcion.getLeyConcesiones());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 6, interrupcion.getTenInterrupcion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 7, interrupcion.getSecTipico());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 8, interrupcion.getFecIniProgramada());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 9, interrupcion.getFecFinProgramada());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 10, interrupcion.getFecIniInterrupcion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 11, interrupcion.getFecFinInterrupcion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 12, interrupcion.getNumSumRegEstimados());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 13, interrupcion.getNumSumRegAfectados());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 14, interrupcion.getNumCliLibAfectados());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 15, interrupcion.getMotFalla());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 16, interrupcion.getLocFalla());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 17, interrupcion.getZonAfectada());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 18, interrupcion.getOriInterrupcion());
					
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 19,interrupcion.getFecNotificacion1());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 20,interrupcion.getFecNotificacion2());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 21,interrupcion.getUbiPtoIntProgramado());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 22,interrupcion.getNomResponsable());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 23,interrupcion.getResumen());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 24,interrupcion.getSustentacion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 25,interrupcion.getFasInterrumpida());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 26,interrupcion.getPotIntEstimada());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 27,interrupcion.getCodRelevador());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 28,interrupcion.getEneTeoNoSuministrada());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 29,interrupcion.getRecCarga());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 30,interrupcion.getDesZonAfectada());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 31,interrupcion.getSemestre());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 32,interrupcion.getNroOficio());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 33,interrupcion.getFecOficio());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 34,interrupcion.getCodosi());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 35,interrupcion.getNroExp());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 36,interrupcion.getNroResol());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 37,interrupcion.getFecResol());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 38,interrupcion.getEstResol());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 39,interrupcion.getTipoRelevador());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 40,interrupcion.getDuracionExoFum());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 41,interrupcion.getFecFinExoneracion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 42,interrupcion.getSisElectrico());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 43,interrupcion.getNroClieSisElectrico());
						
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 44,interrupcion.getEneNoSumEstimada());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 45,interrupcion.getUbiFalla());
					if(interrupcion.getModalidadDeteccion() != null ){
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 46,interrupcion.getModalidadDeteccion().getDescripcion());
					}
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 47,interrupcion.getPeriodoMedicion().getAno()+"-" +getPeriodoMedicion().getPeriodo());
					
					if(interrupcion.getLocalidad() != null){
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 48,interrupcion.getLocalidad().getCodLocalidad()+"-"+interrupcion.getLocalidad().getNombre());
					}
					if(interrupcion.getTipoNotificacion1() != null){
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 49, interrupcion.getTipoNotificacion1().getDescripcion());
					}
					if(interrupcion.getTipoNotificacion2() != null){
							ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 50,interrupcion.getTipoNotificacion2().getDescripcion());
					}
					//074
					
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 51,interrupcion.getCodInsSalio());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 52,interrupcion.getTipInsSalio());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 53,interrupcion.getCodInsFalla());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 54,interrupcion.getFecFinComSalio());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 55,interrupcion.getCodOriInterrupcion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 56,interrupcion.getPropiedad());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 57,interrupcion.getResponsable());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 58,interrupcion.getCodCausa074());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 59,interrupcion.getIntImportante());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 60,interrupcion.getNaturaleza());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 61,interrupcion.getDurHorSuministro());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 62,interrupcion.getCodSecOsinerg());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 63,interrupcion.getTipInsFalla());
					intRow++;
					
	
			}
			
			ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());

	    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
	    facesContext.responseComplete();	
				
		return null;
	}
	
	public String calcularEnergiaPotencia(){	
		String result;
		
		ConectaDb db = new ConectaDb("CALIDAD");
		Connection cn = db.getConnection();
        CallableStatement cstmt = null;
        
        for (Interrupcion item_interrupcion : selectionItems) {
	        try {
	        	cstmt = cn.prepareCall("{CALL CAL_SUMINISTRO_PACKAGE.CALCULAR_ENER_POT_NS(?)}");
			   	cstmt.setString(1, item_interrupcion.getCodInterrupcion());
				
	            int ctos = cstmt.executeUpdate();;
	            cstmt.close(); 
	
	            if (ctos == 0) {
	                result = "0 filas afectadas";
	            }
	
	        } catch (SQLException e) {
	            result = e.getMessage();
				setMensaje(e.getMessage()+":"+e.getCause());
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	            
	        } finally {
	            try {
	                cn.close();
	            } catch (SQLException e) {
	                result = e.getMessage();
	                
	    			setMensaje(e.getMessage()+":"+e.getCause());
	    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
	    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	                
	            }
	        }
        }
        
        //InterrupcionesxPeriodo();
		
		//return "/zonasegura/interrupcion/interrupcionListar?faces-redirect=true";
        
        return null;
	}
	
	public String calcularAllEnergiaPotencia(){	
		String result;
		
		ConectaDb db = new ConectaDb("CALIDAD");
		Connection cn = db.getConnection();
        CallableStatement cstmt = null;
        
        
        try {
        	for (Interrupcion item_interrupcion : interrupciones) {
        	
	        	cstmt = cn.prepareCall("{CALL CAL_SUMINISTRO_PACKAGE.CALCULAR_ENER_POT_NS(?)}");
			   	cstmt.setString(1, item_interrupcion.getCodInterrupcion());
				
	            int ctos = cstmt.executeUpdate();;
	            cstmt.close(); 
	
	            if (ctos == 0) {
	                result = "0 filas afectadas";
	            }
            }		            

        } catch (SQLException e) {
            result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
            
        } finally {
            try {
                cn.close();
            } catch (SQLException e) {
                result = e.getMessage();
                
    			setMensaje(e.getMessage()+":"+e.getCause());
    			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
    			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                
            }
        }

        
        //InterrupcionesxPeriodo();
		
		//return "/zonasegura/interrupcion/interrupcionListar?faces-redirect=true";
        
        return null;
	}
	
	/*
	 * *METODO DE RESPALDO BOTON EXPORTAR COMPENSACION LCE
	 * 
	public String exportarCompensacionLCE(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
	    
	    String rutaArchivoOut;
	    String rutaUp;
	    rutaUp = facesContext.getExternalContext().getRealPath("/reportes/Anexointerrupcion.xlsx");
	    String path = System.getProperty("uploads.folder");
	    String nombreArchivo;
	    
	    System.out.print(path + "\n");
	    
	    nombreArchivo = "interrupcion" + "_" + getPeriodoMedicion().getAno() + "_" + getPeriodoMedicion().getPeriodo() + "_"+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsx";
	    rutaArchivoOut = path + nombreArchivo;
	    	    
	    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);

		XSSFSheet objHssfSheet = null;
		
		try  {
	    
		    InterrupcionService service = new InterrupcionService();
		    
		    objHssfSheet = objHssfWorkbook.getSheetAt(0);
			
			int intRow = 2;
			int intCell = 0;
			
			for (Interrupcion interrupcion : service.getAllRows1(periodoMedicion.getPerMedicionId())) {
				
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell, interrupcion.getTipoInterrupcion().getDescripcion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 1, interrupcion.getCodInterrupcion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 2, interrupcion.getCausaInterrupcion().getDescripcion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 3, interrupcion.getAnexo1074());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 4, interrupcion.getSolFueMayor());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 5, interrupcion.getLeyConcesiones());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 6, interrupcion.getTenInterrupcion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 7, interrupcion.getSecTipico());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 8, interrupcion.getFecIniProgramada());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 9, interrupcion.getFecFinProgramada());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 10, interrupcion.getFecIniInterrupcion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 11, interrupcion.getFecFinInterrupcion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 12, interrupcion.getNumSumRegEstimados());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 13, interrupcion.getNumSumRegAfectados());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 14, interrupcion.getNumCliLibAfectados());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 15, interrupcion.getMotFalla());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 16, interrupcion.getLocFalla());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 17, interrupcion.getZonAfectada());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 18, interrupcion.getOriInterrupcion());
					
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 19,interrupcion.getFecNotificacion1());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 20,interrupcion.getFecNotificacion2());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 21,interrupcion.getUbiPtoIntProgramado());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 22,interrupcion.getNomResponsable());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 23,interrupcion.getResumen());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 24,interrupcion.getSustentacion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 25,interrupcion.getFasInterrumpida());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 26,interrupcion.getPotIntEstimada());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 27,interrupcion.getCodRelevador());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 28,interrupcion.getEneTeoNoSuministrada());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 29,interrupcion.getRecCarga());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 30,interrupcion.getDesZonAfectada());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 31,interrupcion.getSemestre());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 32,interrupcion.getNroOficio());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 33,interrupcion.getFecOficio());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 34,interrupcion.getCodosi());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 35,interrupcion.getNroExp());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 36,interrupcion.getNroResol());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 37,interrupcion.getFecResol());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 38,interrupcion.getEstResol());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 39,interrupcion.getTipoRelevador());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 40,interrupcion.getDuracionExoFum());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 41,interrupcion.getFecFinExoneracion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 42,interrupcion.getSisElectrico());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 43,interrupcion.getNroClieSisElectrico());
						
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 44,interrupcion.getEneNoSumEstimada());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 45,interrupcion.getUbiFalla());
					if(interrupcion.getModalidadDeteccion() != null ){
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 46,interrupcion.getModalidadDeteccion().getDescripcion());
					}
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 47,interrupcion.getPeriodoMedicion().getAno()+"-" +getPeriodoMedicion().getPeriodo());
					
					if(interrupcion.getLocalidad() != null){
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 48,interrupcion.getLocalidad().getCodLocalidad()+"-"+interrupcion.getLocalidad().getNombre());
					}
					if(interrupcion.getTipoNotificacion1() != null){
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 49, interrupcion.getTipoNotificacion1().getDescripcion());
					}
					if(interrupcion.getTipoNotificacion2() != null){
							ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 50,interrupcion.getTipoNotificacion2().getDescripcion());
					}
					//074
					
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 51,interrupcion.getCodInsSalio());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 52,interrupcion.getTipInsSalio());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 53,interrupcion.getCodInsFalla());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 54,interrupcion.getFecFinComSalio());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 55,interrupcion.getCodOriInterrupcion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 56,interrupcion.getPropiedad());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 57,interrupcion.getResponsable());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 58,interrupcion.getCodCausa074());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 59,interrupcion.getIntImportante());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 60,interrupcion.getNaturaleza());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 61,interrupcion.getDurHorSuministro());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 62,interrupcion.getCodSecOsinerg());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 63,interrupcion.getTipInsFalla());
					intRow++;
					
	
			}
			
			ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());

	    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
	    facesContext.responseComplete();	
				
		return null;
	}
	*/
	
	public String calcularCompensacionLCE(){
		ConectaDb db = new ConectaDb("CALIDAD");
        Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        int ctos;
        String result;
		try {
		
			cstmt = cn.prepareCall("{CALL CAL_OTROS_PACKAGE.PROCESA_COMPENSACION_LEY(?,?,?)}"); 
			
			System.out.println(periodoMedicion.getAno());
			cstmt.setString(1,periodoMedicion.getAno());
			
			System.out.println(periodoMedicion.getSemestre());
			cstmt.setString(2,periodoMedicion.getSemestre());
			
			System.out.println(periodoMedicion.getPeriodo());
			cstmt.setString(3,periodoMedicion.getPeriodo());
							
			cstmt.execute();
			cstmt.close();
		
		
		
		} catch (SQLException e) {
	        result = e.getMessage();
	        System.out.println(result);
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción - Error SQL", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
		} catch (Exception e) {
	        result = e.getMessage();
	        System.out.println(result);
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción - Error Desconocido", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            	            
	    } finally {
	        try {
	            cn.close();
	        } catch (SQLException e) {
	            result = e.getMessage();
	            System.out.println(result);
				setMensaje(e.getMessage()+":"+e.getCause());
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción - Error al Cerrar la Conexion", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	        }
	    }
		
		//mostrarMensaje("Proceso Culminado.");
		/*
		FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_INFO,"El proceso ha culminado", getMensaje()));
		FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		*/
		
		InterrupcionesxPeriodo();
		
		return "/zonasegura/interrupcion/interrupcionListar?faces-redirect=true";
	}
	
	public String calcularCompensacionRC(){
		ConectaDb db = new ConectaDb("CALIDAD");		
        Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        int ctos;
        String result;
		try {
		
			cstmt = cn.prepareCall("{CALL CAL_SUMINISTRO_PACKAGE.COMPENSAR_RC(?,?)}"); 
							
			cstmt.setString(1,periodoMedicion.getAno());
			cstmt.setString(2,periodoMedicion.getSemestre());
							
			cstmt.execute();
			cstmt.close();
		
		
		
		} catch (SQLException e) {
	        result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	            
	    } finally {
	        try {
	            cn.close();
	        } catch (SQLException e) {
	            result = e.getMessage();
				setMensaje(e.getMessage()+":"+e.getCause());
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	        }
	    }
		
		InterrupcionesxPeriodo();
		
		return "/zonasegura/interrupcion/interrupcionListar?faces-redirect=true";
	}

	public List<SelectItem> getItemsCodRelevador() {
		itemsCodRelevador = new ArrayList<SelectItem>();
		itemsCodRelevador.add(new SelectItem("", "NO ASIGNADO"));
		Sql sql = new Sql("CALIDAD");
		String s = "SELECT  COD_RELEVADOR,COD_ALIMENTADOR FROM CAL_RELEVADOR_RECHAZO_CARGA WHERE ESTADO = '1' AND ANO = '" + periodoMedicion.getAno() + "' AND SEMESTRE = '" + periodoMedicion.getSemestre() + "' ORDER BY COD_ALIMENTADOR ASC";
		List<Object[]> listObject = sql.consulta(s, false);
		
		System.out.println("ANO:" + periodoMedicion.getAno());
		System.out.println("SEMESTRE:" + periodoMedicion.getSemestre());
		
		if(listObject != null){
			for (Object[] objects: listObject) {
				itemsCodRelevador.add(new SelectItem((String)objects[0],(String)objects[0] + " - " +(String)objects[1]));
			}
		}
		return itemsCodRelevador;
	}

	public void setItemsCodRelevador(List<SelectItem> itemsCodRelevador) {
		this.itemsCodRelevador = itemsCodRelevador;
	}	
	
	
	public void generarReporteConsolidado(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
		ConectaDb db = new ConectaDb("CALIDAD");		
        Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        int ctos;
        String result;
		try {
		
			cstmt = cn.prepareCall("{CALL CAL_SUMINISTRO_PACKAGE.GENERAR_REPORTE_CONSOLIDADO(?,?)}"); 
							
			cstmt.setString(1,periodoMedicion.getAno());
			cstmt.setString(2,periodoMedicion.getSemestre());
							
			cstmt.execute();
			cstmt.close();
		
		
		
		} catch (SQLException e) {
	        result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	            
	    } finally {
	        try {
	            cn.close();
	        } catch (SQLException e) {
	            result = e.getMessage();
				setMensaje(e.getMessage()+":"+e.getCause());
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	        }
	    }
		
		String rutaUp;
		String rutaArchivoOut;
		String path = System.getProperty("uploads.folder");
		String nombreArchivo;
		int numFila;

		rutaUp = facesContext.getExternalContext().getRealPath(
				"/reportes/AnexoInformeconsolidado.xlsx");

		Sql sql = new Sql("CALIDAD");
		String semestre = null;

		String ano = null;
		EmpresaService empService = new EmpresaService();
		ano = periodoMedicion.getAno();
		semestre = periodoMedicion.getSemestre();
		String selectSql = "SELECT DISTINCT M.LOCALIDAD, (SELECT INT_PROM_USUARIO FROM CAL_REPORTE_CONSOLIDADO WHERE LOCALIDAD = M.LOCALIDAD AND NIVEL_TENSION = 'MT' AND ANO = M.ANO AND SEMESTRE = M.SEMESTRE), " + 
							"(SELECT DUR_INT_PROM_USUARIO FROM CAL_REPORTE_CONSOLIDADO WHERE LOCALIDAD = M.LOCALIDAD AND NIVEL_TENSION = 'MT' AND ANO = M.ANO AND SEMESTRE = M.SEMESTRE),  " +
							"(SELECT CANT_SUMINISTROS FROM CAL_REPORTE_CONSOLIDADO WHERE LOCALIDAD = M.LOCALIDAD AND NIVEL_TENSION = 'MT' AND ANO = M.ANO AND SEMESTRE = M.SEMESTRE),  " +
							"(SELECT INT_PROM_USUARIO FROM CAL_REPORTE_CONSOLIDADO WHERE LOCALIDAD = M.LOCALIDAD AND NIVEL_TENSION = 'BT' AND ANO = M.ANO AND SEMESTRE = M.SEMESTRE),  " +
							"(SELECT DUR_INT_PROM_USUARIO FROM CAL_REPORTE_CONSOLIDADO WHERE LOCALIDAD = M.LOCALIDAD AND NIVEL_TENSION = 'BT' AND ANO = M.ANO AND SEMESTRE = M.SEMESTRE),  " +
							"(SELECT CANT_SUMINISTROS FROM CAL_REPORTE_CONSOLIDADO WHERE LOCALIDAD = M.LOCALIDAD AND NIVEL_TENSION = 'BT' AND ANO = M.ANO AND SEMESTRE = M.SEMESTRE)  " +
							" FROM CAL_REPORTE_CONSOLIDADO M " +  
							"WHERE M.ANO = '" + ano + "' AND M.SEMESTRE = '" + semestre + "' " + 
							"ORDER BY M.LOCALIDAD ASC ";
		
		/*
		String selectSql = "SELECT (SELECT LOCALIDAD FROM CAL_REPORTE_CONSOLIDADO WHERE LOCALIDAD = M.LOCALIDAD ),(SELECT INT_PROM_USUARIO FROM CAL_REPORTE_CONSOLIDADO WHERE LOCALIDAD = M.LOCALIDAD AND NIVEL_TENSION = 'MT'),(SELECT DUR_INT_PROM_USUARIO FROM CAL_REPORTE_CONSOLIDADO WHERE LOCALIDAD = M.LOCALIDAD AND NIVEL_TENSION = 'MT'),(SELECT CANT_SUMINISTROS FROM CAL_REPORTE_CONSOLIDADO WHERE LOCALIDAD = M.LOCALIDAD AND NIVEL_TENSION = 'MT'),(SELECT INT_PROM_USUARIO FROM CAL_REPORTE_CONSOLIDADO WHERE LOCALIDAD = M.LOCALIDAD AND NIVEL_TENSION = 'BT'),(SELECT DUR_INT_PROM_USUARIO FROM CAL_REPORTE_CONSOLIDADO WHERE LOCALIDAD = M.LOCALIDAD AND NIVEL_TENSION = 'BT'),(SELECT CANT_SUMINISTROS FROM CAL_REPORTE_CONSOLIDADO WHERE LOCALIDAD = M.LOCALIDAD AND NIVEL_TENSION = 'BT')FROM CAL_REPORTE_CONSOLIDADO M WHERE ANO = '"
				+ ano
				+ "' AND SEMESTRE = '"
				+ semestre
				+ "' ORDER BY M.LOCALIDAD ASC";
		*/		
		List<Object[]> objTabla = sql.consulta(selectSql, false);
		
		nombreArchivo = "Anexo Reporte_Con"
				+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss")
						.format(new Date()) + ".xlsx";
		rutaArchivoOut = path + nombreArchivo;

		System.out.println("consulta:" + selectSql);
		
		XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);
		XSSFSheet objHssfSheet = null;
		objHssfSheet = objHssfWorkbook.getSheetAt(0);
		
		//ModificarExcel.setCellValue(objHssfSheet, 3, 2, empService.getAllRows().get(0).getEmpresa());
		ModificarExcel.setCellValue(objHssfSheet, 3, 5, semestre);
		ModificarExcel.setCellValue(objHssfSheet, 3, 8, ano);
		
		if (objTabla != null) {
		
			int intRow = 8;
			int intCell = 0;
			
			for (Object[] objFila : objTabla) {
								
				System.out.println("llega:" + objTabla);
				intCell = 0;
				for (Object object : objFila) {
					
					BigDecimal campoBigDecimal;
					String campoString;
					try {
						campoString = (String) object;
						ModificarExcel.setCellValue(objHssfSheet, intRow,intCell, campoString);
					} catch (Exception e) {
						campoBigDecimal = (BigDecimal) object;
						ModificarExcel.setCellValue(objHssfSheet, intRow,intCell, campoBigDecimal);
					}
					intCell++;
				}
				intRow++;
			}
		}

		ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);

		HttpServletResponse response = ((HttpServletResponse) facesContext
				.getExternalContext().getResponse());
		writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,
				"application/vnd.ms-excel");// "text/xml"
		facesContext.responseComplete();
	}
				
	public String exportarFzaMayorU(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
	    
	    String rutaArchivoOut;
	    String rutaUp;
	    rutaUp = facesContext.getExternalContext().getRealPath("/reportes/AnexoSolFzaMayorU.xlsx");
	    String path = System.getProperty("uploads.folder");
	    String nombreArchivo;
	    
		String semestre = null;
		String ano = null;
		ano = periodoMedicion.getAno();
		semestre = periodoMedicion.getSemestre();
	    
	    System.out.print(path + "\n");
	    
	    nombreArchivo = "ResFuerzaMayor_Ur" + "_" + getPeriodoMedicion().getAno() + "_" + getPeriodoMedicion().getPeriodo() + "_"+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsx";
	    rutaArchivoOut = path + nombreArchivo;
	    	    
	    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);

		XSSFSheet objHssfSheet = null;
		
		try  {
	    
		    InterrupcionService service = new InterrupcionService();
		    
		    objHssfSheet = objHssfWorkbook.getSheetAt(0);
			
			int intRow = 6;
			int intCell = 1;
			
			ModificarExcel.setCellValue(objHssfSheet, 3, 5, semestre);
			ModificarExcel.setCellValue(objHssfSheet, 3, 9, ano);
			
			for (Interrupcion interrupcion : service.getAllFueU(periodoMedicion.getAno(),periodoMedicion.getSemestre())) {
				
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell, interrupcion.getCodInterrupcion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 1, interrupcion.getSolFueMayor());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 2, interrupcion.getNroExp());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 3, interrupcion.getNroOficio());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 4, interrupcion.getFecOficio());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 5, interrupcion.getCodosi());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 6, interrupcion.getNroResol());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 7, interrupcion.getFecResol());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 8, interrupcion.getEstResol());
					intRow++;
					
	
			}
			
			ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());

	    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
	    facesContext.responseComplete();	
				
		return null;
	}	
			
	public String exportarFzaMayorR(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
	    
	    String rutaArchivoOut;
	    String rutaUp;
	    rutaUp = facesContext.getExternalContext().getRealPath("/reportes/AnexoSolFzaMayorR.xlsx");
	    String path = System.getProperty("uploads.folder");
	    String nombreArchivo;
	    
		String semestre = null;
		String ano = null;
		ano = periodoMedicion.getAno();
		semestre = periodoMedicion.getSemestre();
	    
	    System.out.print(path + "\n");
	    
	    nombreArchivo = "ResFuerzaMayor_Ru" + "_" + getPeriodoMedicion().getAno() + "_" + getPeriodoMedicion().getPeriodo() + "_"+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsx";
	    rutaArchivoOut = path + nombreArchivo;
	    	    
	    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);

		XSSFSheet objHssfSheet = null;
		
		try  {
	    
		    InterrupcionService service = new InterrupcionService();
		    
		    objHssfSheet = objHssfWorkbook.getSheetAt(0);
			
			int intRow = 6;
			int intCell = 1;
			
			ModificarExcel.setCellValue(objHssfSheet, 3, 5, semestre);
			ModificarExcel.setCellValue(objHssfSheet, 3, 9, ano);
			
			for (Interrupcion interrupcion : service.getAllFueR(periodoMedicion.getAno(),periodoMedicion.getSemestre())) {
				
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell, interrupcion.getCodInterrupcion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 1, interrupcion.getSolFueMayor());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 2, interrupcion.getNroExp());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 3, interrupcion.getNroOficio());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 4, interrupcion.getFecOficio());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 5, interrupcion.getCodosi());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 6, interrupcion.getNroResol());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 7, interrupcion.getFecResol());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 8, interrupcion.getEstResol());
					intRow++;
					
	
			}
			
			ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());

	    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
	    facesContext.responseComplete();	
				
		return null;
	}	
	
	public void generarReporteResCompSemestre(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
		ConectaDb db = new ConectaDb("CALIDAD");		
        Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        int ctos;
        String result;
		
		
		String rutaUp;
		String rutaArchivoOut;
		String path = System.getProperty("uploads.folder");
		String nombreArchivo;
		int numFila;

		rutaUp = facesContext.getExternalContext().getRealPath(
				"/reportes/AnexoResCompSemestre.xlsx");

		Sql sql = new Sql("CALIDAD");
		String semestre = null;

		String ano = null;
		EmpresaService empService = new EmpresaService();
		ano = periodoMedicion.getAno();
		semestre = periodoMedicion.getSemestre();
		String selectSql = "SELECT DISTINCT " + 
				"L.COD_OSINERG ||'-'|| L.NOMBRE_OSINERG, " + 
				"(SELECT SUM(S.MON_COM_INT_PTO_ENTREGA) FROM CAL_COMPENSACION_SUMINISTRO S WHERE S.COMPENSADO = 'SI' AND S.COD_LOCALIDAD = M.COD_LOCALIDAD AND NIVEL_TENSION = 'BT' AND S.TIP_COMPENSACION = '123PUNU' AND S.ANO = M.ANO AND S.SEMESTRE = M.SEMESTRE), " + 
				"(SELECT SUM(S.MON_COM_INT_PTO_ENTREGA) FROM CAL_COMPENSACION_SUMINISTRO S WHERE S.COMPENSADO = 'SI' AND S.COD_LOCALIDAD = M.COD_LOCALIDAD AND NIVEL_TENSION = 'MT' AND S.TIP_COMPENSACION = '123PUNU' AND S.ANO = M.ANO AND S.SEMESTRE = M.SEMESTRE), " + 
				"(SELECT SUM(S.MON_COM_INT_PTO_ENTREGA) FROM CAL_COMPENSACION_SUMINISTRO S WHERE S.COMPENSADO = 'SI' AND S.COD_LOCALIDAD = M.COD_LOCALIDAD AND NIVEL_TENSION IN ('AT','MAT') AND S.TIP_COMPENSACION = '123PUNU' AND S.ANO = M.ANO AND S.SEMESTRE = M.SEMESTRE), " + 
				"(SELECT SUM(S.MON_COM_LEY_CONCESIONES) FROM CAL_COMPENSACION_SUMINISTRO S WHERE S.COMPENSADO = 'SI' AND S.COD_LOCALIDAD = M.COD_LOCALIDAD AND NIVEL_TENSION = 'BT' AND S.TIP_COMPENSACION = '123PUNU' AND S.ANO = M.ANO AND S.SEMESTRE = M.SEMESTRE), " + 
				"(SELECT SUM(S.MON_COM_LEY_CONCESIONES) FROM CAL_COMPENSACION_SUMINISTRO S WHERE S.COMPENSADO = 'SI' AND S.COD_LOCALIDAD = M.COD_LOCALIDAD AND NIVEL_TENSION = 'MT' AND S.TIP_COMPENSACION = '123PUNU' AND S.ANO = M.ANO AND S.SEMESTRE = M.SEMESTRE), " + 
				"(SELECT SUM(S.MON_COM_LEY_CONCESIONES) FROM CAL_COMPENSACION_SUMINISTRO S WHERE S.COMPENSADO = 'SI' AND S.COD_LOCALIDAD = M.COD_LOCALIDAD AND NIVEL_TENSION IN ('AT','MAT') AND S.TIP_COMPENSACION = '123PUNU' AND S.ANO = M.ANO AND S.SEMESTRE = M.SEMESTRE) " + 
				"FROM CAL_COMPENSACION_SUMINISTRO M , CAL_LOCALIDAD L  " + 
				"WHERE M.COD_LOCALIDAD = L.COD_LOCALIDAD AND M.ANO = '" + ano + "' AND M.SEMESTRE = '" + semestre + "'  AND M.COMPENSADO = 'SI' AND M.TIP_COMPENSACION = '123PUNU'  " + 
				"ORDER BY L.COD_OSINERG ||'-'|| L.NOMBRE_OSINERG ASC ";
		
		
		
		List<Object[]> objTabla = sql.consulta(selectSql, false);
		
		nombreArchivo = "Anexo ResCompSemestre"
				+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss")
						.format(new Date()) + ".xlsx";
		rutaArchivoOut = path + nombreArchivo;

		System.out.println("consulta:" + selectSql);
		
		XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);
		XSSFSheet objHssfSheet = null;
		objHssfSheet = objHssfWorkbook.getSheetAt(0);
		
		
		ModificarExcel.setCellValue(objHssfSheet, 3, 5, semestre);
		ModificarExcel.setCellValue(objHssfSheet, 3, 7, ano);
		
		if (objTabla != null) {
		
			int intRow = 7;
			int intCell = 1;
			
			for (Object[] objFila : objTabla) {
								
				System.out.println("llega:" + objTabla);
				intCell = 1;
				for (Object object : objFila) {
					
					BigDecimal campoBigDecimal;
					String campoString;
					try {
						campoString = (String) object;
						ModificarExcel.setCellValue(objHssfSheet, intRow,intCell, campoString);
					} catch (Exception e) {
						campoBigDecimal = (BigDecimal) object;
						ModificarExcel.setCellValue(objHssfSheet, intRow,intCell, campoBigDecimal);
					}
					intCell++;
				}
				intRow++;
			}
		}

		ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);

		HttpServletResponse response = ((HttpServletResponse) facesContext
				.getExternalContext().getResponse());
		writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,
				"application/vnd.ms-excel");// "text/xml"
		facesContext.responseComplete();
	}
	
	public String proceso1(){

		ConectaDb db = new ConectaDb("CALIDAD");		
        Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        int ctos;
        String result;
		try {
		
			cstmt = cn.prepareCall("{CALL CAL_SUMINISTRO_PACKAGE.COMPENSAR_SUMINISTRO(?,?)}"); 
							
			cstmt.setString(1,periodoMedicion.getAno());
			cstmt.setString(2,periodoMedicion.getSemestre());

/*			ctos = cstmt.executeUpdate();
			if (ctos == 0) {
				result = "0 filas afectadas";
			}	
*/					
			cstmt.execute();
			cstmt.close();
		
			
		
		} catch (SQLException e) {
	        result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	            
	    } finally {
	        try {
	            cn.close();
	        } catch (SQLException e) {
	            result = e.getMessage();
				setMensaje(e.getMessage()+":"+e.getCause());
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	        }
	    }
		
		//InterrupcionesxPeriodo();
		
		return "/zonasegura/interrupcion/interrupcionListar?faces-redirect=true";		
	}
	
	public String proceso2y4(){

		ConectaDb db = new ConectaDb("CALIDAD");		
        Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        int ctos;
        String result;
		try {
		
			cstmt = cn.prepareCall("{CALL CAL_SUMINISTRO_PACKAGE.PROCESO_2y4(?,?)}"); 
							
			cstmt.setString(1,periodoMedicion.getAno());
			cstmt.setString(2,periodoMedicion.getSemestre());

/*			ctos = cstmt.executeUpdate();
			if (ctos == 0) {
				result = "0 filas afectadas";
			}	
*/					
			cstmt.execute();
			cstmt.close();
		
			
		
		} catch (SQLException e) {
	        result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	            
	    } finally {
	        try {
	            cn.close();
	        } catch (SQLException e) {
	            result = e.getMessage();
				setMensaje(e.getMessage()+":"+e.getCause());
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	        }
	    }
		
		//InterrupcionesxPeriodo();
		
		return "/zonasegura/interrupcion/interrupcionListar?faces-redirect=true";		
	}
	
	public String proceso3y5(){

		ConectaDb db = new ConectaDb("CALIDAD");		
        Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        int ctos;
        String result;
		try {
		
			cstmt = cn.prepareCall("{CALL CAL_SUMINISTRO_PACKAGE.PROCESO_3y5(?,?)}"); 
							
			cstmt.setString(1,periodoMedicion.getAno());
			cstmt.setString(2,periodoMedicion.getSemestre());


/*			ctos = cstmt.executeUpdate();
			if (ctos == 0) {
				result = "0 filas afectadas";
			}	
*/					
			cstmt.execute();
			cstmt.close();
		
			
		
		} catch (SQLException e) {
	        result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	            
	    } finally {
	        try {
	            cn.close();
	        } catch (SQLException e) {
	            result = e.getMessage();
				setMensaje(e.getMessage()+":"+e.getCause());
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	        }
	    }
		
		//InterrupcionesxPeriodo();
		
		return "/zonasegura/interrupcion/interrupcionListar?faces-redirect=true";		
	}
	
	public String ActualizarEnergiaSemestre(){

		ConectaDb db = new ConectaDb("CALIDAD");		
        Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        int ctos;
        String result;
		try {
		
			cstmt = cn.prepareCall("{CALL CAL_SUMINISTRO_PACKAGE.ACTUALIZAR_ENERGIAS(?,?)}"); 
							
			cstmt.setString(1,periodoMedicion.getAno());
			cstmt.setString(2,periodoMedicion.getSemestre());
			
			//System.out.println("ANO:" + periodoMedicion.getAno());
			
			//System.out.println("semes:" + periodoMedicion.getSemestre());
							
			ctos = cstmt.executeUpdate();
		
			if (ctos == 0) {
				result = "0 filas afectadas";
			}
		
		} catch (SQLException e) {
	        result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	            
	    } finally {
	        try {
	            cn.close();
	        } catch (SQLException e) {
	            result = e.getMessage();
				setMensaje(e.getMessage()+":"+e.getCause());
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	        }
	    }
		
		//InterrupcionesxPeriodo();
		
		return "/zonasegura/interrupcion/interrupcionListar?faces-redirect=true";		
	}
	
	
	
	//Res_Comp_S1_13_INT
	public void generarReporteCompSerSemestre(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
		ConectaDb db = new ConectaDb("CALIDAD");		
        Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        int ctos;
        String result;
		
		
		String rutaUp;
		String rutaArchivoOut;
		String path = System.getProperty("uploads.folder");
		String nombreArchivo;
		int numFila;

		rutaUp = facesContext.getExternalContext().getRealPath(
				"/reportes/AnexoResCompIntSemestre.xlsx");

		Sql sql = new Sql("CALIDAD");
		String semestre = null;

		String ano = null;
		EmpresaService empService = new EmpresaService();
		ano = periodoMedicion.getAno();
		semestre = periodoMedicion.getSemestre();
		
		String selectSql = "SELECT DISTINCT SIS_ELECTRICO,(SELECT SUM(S.MON_COM_INT_PTO_ENTREGA)FROM CAL_COMPENSACION_SUMINISTRO S WHERE S.COMPENSADO = 'SI' AND S.SIS_ELECTRICO = M.SIS_ELECTRICO AND S.NIVEL_TENSION = 'BT' AND S.TIP_COMPENSACION IN('456DNR','123RSR') AND S.ANO = M.ANO AND S.SEMESTRE = M.SEMESTRE) , " + 
				"(SELECT SUM(S.MON_COM_INT_PTO_ENTREGA)FROM CAL_COMPENSACION_SUMINISTRO S WHERE S.COMPENSADO = 'SI' AND S.SIS_ELECTRICO = M.SIS_ELECTRICO AND S.NIVEL_TENSION = 'MT' AND S.TIP_COMPENSACION IN('456DNR','123RSR') AND S.ANO = M.ANO AND S.SEMESTRE = M.SEMESTRE),  " +
				"(SELECT SUM(MON_COM_LEY_CONCESIONES)FROM CAL_COMPENSACION_SUMINISTRO S WHERE S.COMPENSADO = 'SI' AND S.SIS_ELECTRICO = M.SIS_ELECTRICO AND S.NIVEL_TENSION = 'BT' AND S.TIP_COMPENSACION IN('456DNR','123RSR') AND S.ANO = M.ANO AND S.SEMESTRE = M.SEMESTRE),  " +
				"(SELECT SUM(MON_COM_LEY_CONCESIONES)FROM CAL_COMPENSACION_SUMINISTRO S WHERE S.COMPENSADO = 'SI' AND S.SIS_ELECTRICO = M.SIS_ELECTRICO AND S.NIVEL_TENSION = 'MT' AND S.TIP_COMPENSACION IN('456DNR','123RSR') AND S.ANO = M.ANO AND S.SEMESTRE = M.SEMESTRE)  " +
				"FROM CAL_COMPENSACION_SUMINISTRO M  " +
				"WHERE M.ANO = '" + ano + "' AND M.SEMESTRE = '" + semestre + "' AND M.COMPENSADO = 'SI' AND M.TIP_COMPENSACION IN('456DNR','123RSR') " + 
				"ORDER BY M.SIS_ELECTRICO ASC ";
;
			
						
		
		List<Object[]> objTabla = sql.consulta(selectSql, false);
		
		nombreArchivo = "Anexo ResCompInt"
				+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss")
						.format(new Date()) + ".xlsx";
		rutaArchivoOut = path + nombreArchivo;

		System.out.println("consulta:" + selectSql);
		
		XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);
		XSSFSheet objHssfSheet = null;
		objHssfSheet = objHssfWorkbook.getSheetAt(0);
		
		//ModificarExcel.setCellValue(objHssfSheet, 3, 2, empService.getAllRows().get(0).getEmpresa());
		ModificarExcel.setCellValue(objHssfSheet, 3, 6, semestre);
		ModificarExcel.setCellValue(objHssfSheet, 3, 8, ano);
		
		if (objTabla != null) {
		
			int intRow = 7;
			int intCell = 2;
			
			for (Object[] objFila : objTabla) {
								
				//System.out.println("llega:" + objTabla);
				intCell = 2;
				for (Object object : objFila) {
					
					BigDecimal campoBigDecimal;
					String campoString;
					try {
						campoString = (String) object;
						ModificarExcel.setCellValue(objHssfSheet, intRow,intCell, campoString);
					} catch (Exception e) {
						campoBigDecimal = (BigDecimal) object;
						ModificarExcel.setCellValue(objHssfSheet, intRow,intCell, campoBigDecimal);
					}
					intCell++;
				}
				intRow++;
			}
		}

		ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);

		HttpServletResponse response = ((HttpServletResponse) facesContext
				.getExternalContext().getResponse());
		writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,
				"application/vnd.ms-excel");// "text/xml"
		facesContext.responseComplete();
	}
	
	public void generarReporteConsolidadoP2y4(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
		ConectaDb db = new ConectaDb("CALIDAD");		
        Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        int ctos;
        String result;
		
		
		String rutaUp;
		String rutaArchivoOut;
		String path = System.getProperty("uploads.folder");
		String nombreArchivo;
		int numFila;

		rutaUp = facesContext.getExternalContext().getRealPath(
				"/reportes/AnexoReporteConsolidadoP2y4.xlsx");

		Sql sql = new Sql("CALIDAD");
		String semestre = null;

		String ano = null;
		EmpresaService empService = new EmpresaService();
		ano = periodoMedicion.getAno();
		semestre = periodoMedicion.getSemestre();
		
		String selectSql = "SELECT DISTINCT M.SIS_ELECTRICO, " + 
							"(SELECT NVL(NIC_INT_NO_PROGRAMADAS,0) + NVL(NIC_INT_PRO_MANTENIMIENTO,0) + NVL(NIC_INT_PRO_EXP_REFORZAMIENTO,0) FROM CAL_COMPENSACION_SUMINISTRO S WHERE S.SIS_ELECTRICO = M.SIS_ELECTRICO AND S.NIVEL_TENSION = 'MT' AND S.TIP_COMPENSACION IN('456DNR','123RSR') AND S.ANO = M.ANO AND S.SEMESTRE = M.SEMESTRE) , " +  
							"(SELECT NVL(DIC_INT_NO_PROGRAMADAS,0) + NVL(DIC_INT_PRO_MANTENIMIENTO,0) + NVL(DIC_INT_PRO_EXP_REFORZAMIENTO,0) FROM CAL_COMPENSACION_SUMINISTRO S WHERE S.SIS_ELECTRICO = M.SIS_ELECTRICO AND S.NIVEL_TENSION = 'MT' AND S.TIP_COMPENSACION IN('456DNR','123RSR') AND S.ANO = M.ANO AND S.SEMESTRE = M.SEMESTRE) , " +  
							"(SELECT AFECTADOS FROM CAL_COMPENSACION_SUMINISTRO S WHERE S.SIS_ELECTRICO = M.SIS_ELECTRICO AND S.NIVEL_TENSION = 'MT' AND S.TIP_COMPENSACION IN('456DNR','123RSR') AND S.ANO = M.ANO AND S.SEMESTRE = M.SEMESTRE) , " +  
							"(SELECT NVL(NIC_INT_NO_PROGRAMADAS,0) + NVL(NIC_INT_PRO_MANTENIMIENTO,0) + NVL(NIC_INT_PRO_EXP_REFORZAMIENTO,0) FROM CAL_COMPENSACION_SUMINISTRO S WHERE S.SIS_ELECTRICO = M.SIS_ELECTRICO AND S.NIVEL_TENSION = 'BT' AND S.TIP_COMPENSACION IN('456DNR','123RSR') AND S.ANO = M.ANO AND S.SEMESTRE = M.SEMESTRE) , " +  
							"(SELECT NVL(DIC_INT_NO_PROGRAMADAS,0) + NVL(DIC_INT_PRO_MANTENIMIENTO,0) + NVL(DIC_INT_PRO_EXP_REFORZAMIENTO,0) FROM CAL_COMPENSACION_SUMINISTRO S WHERE S.SIS_ELECTRICO = M.SIS_ELECTRICO AND S.NIVEL_TENSION = 'BT' AND S.TIP_COMPENSACION IN('456DNR','123RSR') AND S.ANO = M.ANO AND S.SEMESTRE = M.SEMESTRE) , " +  
							"(SELECT AFECTADOS FROM CAL_COMPENSACION_SUMINISTRO S WHERE S.SIS_ELECTRICO = M.SIS_ELECTRICO AND S.NIVEL_TENSION = 'BT' AND S.TIP_COMPENSACION IN('456DNR','123RSR') AND S.ANO = M.ANO AND S.SEMESTRE = M.SEMESTRE) " +  
							"FROM CAL_COMPENSACION_SUMINISTRO M " +  
							"WHERE M.ANO = '" + ano + "' AND M.SEMESTRE = '" + semestre + "' AND M.TIP_COMPENSACION IN('456DNR','123RSR') " +  
							"ORDER BY M.SIS_ELECTRICO ASC ";
				
		List<Object[]> objTabla = sql.consulta(selectSql, false);
		
		nombreArchivo = "Anexo ResInt"
				+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss")
						.format(new Date()) + ".xlsx";
		rutaArchivoOut = path + nombreArchivo;

		System.out.println("consulta:" + selectSql);
		
		XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);
		XSSFSheet objHssfSheet = null;
		objHssfSheet = objHssfWorkbook.getSheetAt(0);
		
		//ModificarExcel.setCellValue(objHssfSheet, 3, 2, empService.getAllRows().get(0).getEmpresa());
		ModificarExcel.setCellValue(objHssfSheet, 3, 6, semestre);
		ModificarExcel.setCellValue(objHssfSheet, 3, 8, ano);
		
		if (objTabla != null) {
		
			int intRow = 7;
			int intCell = 1;
			
			for (Object[] objFila : objTabla) {
								
				//System.out.println("llega:" + objTabla);
				intCell = 1;
				for (Object object : objFila) {
					
					BigDecimal campoBigDecimal;
					String campoString;
					try {
						campoString = (String) object;
						ModificarExcel.setCellValue(objHssfSheet, intRow,intCell, campoString);
					} catch (Exception e) {
						campoBigDecimal = (BigDecimal) object;
						ModificarExcel.setCellValue(objHssfSheet, intRow,intCell, campoBigDecimal);
					}
					intCell++;
				}
				intRow++;
			}
		}

		ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);

		HttpServletResponse response = ((HttpServletResponse) facesContext
				.getExternalContext().getResponse());
		writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,
				"application/vnd.ms-excel");// "text/xml"
		facesContext.responseComplete();
	}
	
	//Res_Comp_S1_13_EXT
	public void generarReporteCompfueraSerSem(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
		ConectaDb db = new ConectaDb("CALIDAD");		
	    Connection cn = db.getConnection();
	    CallableStatement cstmt = null;  
	    int ctos;
	    String result;
		
		
		String rutaUp;
		String rutaArchivoOut;
		String path = System.getProperty("uploads.folder");
		String nombreArchivo;
		int numFila;
	
		rutaUp = facesContext.getExternalContext().getRealPath(
				"/reportes/AnexoResCompExtSemestre.xlsx");
	
		Sql sql = new Sql("CALIDAD");
		String semestre = null;
	
		String ano = null;
		EmpresaService empService = new EmpresaService();
		ano = periodoMedicion.getAno();
		semestre = periodoMedicion.getSemestre();
		
		String selectSql = "SELECT DISTINCT SIS_ELECTRICO,(SELECT SUM(S.MON_COM_INT_PTO_ENTREGA)FROM CAL_COMPENSACION_SUMINISTRO S WHERE S.COMPENSADO = 'SI' AND S.SIS_ELECTRICO = M.SIS_ELECTRICO AND S.NIVEL_TENSION = 'BT' AND S.TIP_COMPENSACION IN('456ENU','123PSU') AND S.ANO = M.ANO AND S.SEMESTRE = M.SEMESTRE), " +  
				"(SELECT SUM(S.MON_COM_INT_PTO_ENTREGA)FROM CAL_COMPENSACION_SUMINISTRO S WHERE S.COMPENSADO = 'SI' AND S.SIS_ELECTRICO = M.SIS_ELECTRICO AND S.NIVEL_TENSION = 'MT' AND S.TIP_COMPENSACION IN('456ENU','123PSU') AND S.ANO = M.ANO AND S.SEMESTRE = M.SEMESTRE), " +  
				"(SELECT SUM(MON_COM_LEY_CONCESIONES)FROM CAL_COMPENSACION_SUMINISTRO S WHERE S.COMPENSADO = 'SI' AND S.SIS_ELECTRICO = M.SIS_ELECTRICO AND S.NIVEL_TENSION = 'BT' AND S.TIP_COMPENSACION IN('456ENU','123PSU') AND S.ANO = M.ANO AND S.SEMESTRE = M.SEMESTRE), " +  
				"(SELECT SUM(MON_COM_LEY_CONCESIONES)FROM CAL_COMPENSACION_SUMINISTRO S WHERE S.COMPENSADO = 'SI' AND S.SIS_ELECTRICO = M.SIS_ELECTRICO AND S.NIVEL_TENSION = 'MT' AND S.TIP_COMPENSACION IN('456ENU','123PSU') AND S.ANO = M.ANO AND S.SEMESTRE = M.SEMESTRE) " +  
				"FROM CAL_COMPENSACION_SUMINISTRO M WHERE M.ANO = '" + ano + "' AND M.SEMESTRE = '" + semestre + "' AND M.COMPENSADO = 'SI' AND M.TIP_COMPENSACION IN('456ENU','123PSU')  " + 
				"ORDER BY M.SIS_ELECTRICO ASC ";		
						
		
		List<Object[]> objTabla = sql.consulta(selectSql, false);
		
		nombreArchivo = "Anexo ResCompExt"
				+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss")
						.format(new Date()) + ".xlsx";
		rutaArchivoOut = path + nombreArchivo;
	
		System.out.println("consulta:" + selectSql);
		
		XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);
		XSSFSheet objHssfSheet = null;
		objHssfSheet = objHssfWorkbook.getSheetAt(0);
		
		//ModificarExcel.setCellValue(objHssfSheet, 3, 2, empService.getAllRows().get(0).getEmpresa());
		ModificarExcel.setCellValue(objHssfSheet, 3, 7, semestre);
		ModificarExcel.setCellValue(objHssfSheet, 3, 9, ano);
		
		if (objTabla != null) {
		
			int intRow = 7;
			int intCell = 2;
			
			for (Object[] objFila : objTabla) {
								
				//System.out.println("llega:" + objTabla);
				intCell = 2;
				for (Object object : objFila) {
					
					BigDecimal campoBigDecimal;
					String campoString;
					try {
						campoString = (String) object;
						ModificarExcel.setCellValue(objHssfSheet, intRow,intCell, campoString);
					} catch (Exception e) {
						campoBigDecimal = (BigDecimal) object;
						ModificarExcel.setCellValue(objHssfSheet, intRow,intCell, campoBigDecimal);
					}
					intCell++;
				}
				intRow++;
			}
		}
	
		ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
	
		HttpServletResponse response = ((HttpServletResponse) facesContext
				.getExternalContext().getResponse());
		writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,
				"application/vnd.ms-excel");// "text/xml"
		facesContext.responseComplete();
	}
	
	//Res_Comp_S1_13_DISP
	public void generarReporteCompDisSem(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
		ConectaDb db = new ConectaDb("CALIDAD");		
	    Connection cn = db.getConnection();
	    CallableStatement cstmt = null;  
	    int ctos;
	    String result;
		
		
		String rutaUp;
		String rutaArchivoOut;
		String path = System.getProperty("uploads.folder");
		String nombreArchivo;
		int numFila;
	
		rutaUp = facesContext.getExternalContext().getRealPath(
				"/reportes/AnexoResCompDisSemestre.xlsx");
	
		Sql sql = new Sql("CALIDAD");
		String semestre = null;
	
		String ano = null;
		EmpresaService empService = new EmpresaService();
		ano = periodoMedicion.getAno();
		semestre = periodoMedicion.getSemestre();
		
		String selectSql = "SELECT DISTINCT SIS_ELECTRICO,(SELECT SUM(S.MON_COMPENSACION)FROM CAL_COMPENSACION_RC S WHERE S.SIS_ELECTRICO = M.SIS_ELECTRICO AND S.NIVEL_TENSION = 'BT' AND S.ANO = '"
				+ ano
				+ "' AND S.SEMESTRE = '"
				+ semestre
				+ "') , (SELECT SUM(S.MON_COMPENSACION)FROM CAL_COMPENSACION_RC S WHERE S.SIS_ELECTRICO = M.SIS_ELECTRICO AND S.NIVEL_TENSION = 'MT' AND S.ANO = '"
				+ ano
				+ "' AND S.SEMESTRE = '"
				+ semestre
				+ "') , (SELECT SUM(S.MON_COM_LEY_CON_RC)FROM CAL_COMPENSACION_RC S WHERE S.SIS_ELECTRICO = M.SIS_ELECTRICO AND S.NIVEL_TENSION = 'BT' AND S.ANO = '"
				+ ano
				+ "' AND S.SEMESTRE = '"
				+ semestre
				+ "') , (SELECT SUM(S.MON_COM_LEY_CON_RC)FROM CAL_COMPENSACION_RC S WHERE S.SIS_ELECTRICO = M.SIS_ELECTRICO AND S.NIVEL_TENSION = 'MT' AND S.ANO = '"
				+ ano
				+ "' AND S.SEMESTRE = '"
				+ semestre
				+"')FROM CAL_COMPENSACION_RC M WHERE M.ANO = '"
				+ ano
				+ "' AND M.SEMESTRE = '"
				+ semestre 
				+ "'ORDER BY M.SIS_ELECTRICO ASC";
				
	
		
		List<Object[]> objTabla = sql.consulta(selectSql, false);
		
		nombreArchivo = "Anexo ResCompDis"
				+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss")
						.format(new Date()) + ".xlsx";
		rutaArchivoOut = path + nombreArchivo;
	
		System.out.println("consulta:" + selectSql);
		
		XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);
		XSSFSheet objHssfSheet = null;
		objHssfSheet = objHssfWorkbook.getSheetAt(0);
		
	
		ModificarExcel.setCellValue(objHssfSheet, 4, 3, semestre);
		ModificarExcel.setCellValue(objHssfSheet, 4, 5, ano);
		
		if (objTabla != null) {
		
			int intRow = 8;
			int intCell = 1;
			
			for (Object[] objFila : objTabla) {
								
				//System.out.println("llega:" + objTabla);
				intCell = 1;
				for (Object object : objFila) {
					
					BigDecimal campoBigDecimal;
					String campoString;
					try {
						campoString = (String) object;
						ModificarExcel.setCellValue(objHssfSheet, intRow,intCell, campoString);
					} catch (Exception e) {
						campoBigDecimal = (BigDecimal) object;
						ModificarExcel.setCellValue(objHssfSheet, intRow,intCell, campoBigDecimal);
					}
					intCell++;
				}
				intRow++;
			}
		}
	
		ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
	
		HttpServletResponse response = ((HttpServletResponse) facesContext
				.getExternalContext().getResponse());
		writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,
				"application/vnd.ms-excel");// "text/xml"
		facesContext.responseComplete();
	}	
	
	public String exportarComp(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
	    
	    String rutaArchivoOut;
	    String rutaUp;
	    rutaUp = facesContext.getExternalContext().getRealPath("/reportes/AnexocompSum.xlsx");
	    String path = System.getProperty("uploads.folder");
	    String nombreArchivo;
	    
		String semestre = null;
		String ano = null;
				
		semestre = periodoMedicion.getSemestre();
		ano = periodoMedicion.getAno();
	    
	    System.out.print(path + "\n");
	    
	    nombreArchivo = "Compen_Sum" + "_" + getPeriodoMedicion().getAno() + "_" + getPeriodoMedicion().getSemestre() + "_"+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsx";
	    rutaArchivoOut = path + nombreArchivo;
	    	    
	    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);
	
		XSSFSheet objHssfSheet = null;
		
		try  {
	    
			CompensacionSuministroService service = new CompensacionSuministroService();
		    
		    objHssfSheet = objHssfWorkbook.getSheetAt(0);
			
			int intRow = 1;
			int intCell = 0;
			
			//ModificarExcel.setCellValue(objHssfSheet, 3, 5, semestre);
							
				for (CompensacionSuministro compensacionSuministro : service.getAllComp(periodoMedicion.getAno(), periodoMedicion.getSemestre())) {
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell, compensacionSuministro.getCodRelevador());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 1, compensacionSuministro.getNumIntNoProgramadas());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 2, compensacionSuministro.getNumIntProMantenimiento());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 3, compensacionSuministro.getNumIntProExpReforzamiento());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 4, compensacionSuministro.getDurIntNoProgramadas());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 5, compensacionSuministro.getDurIntProMantenimiento());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 6, compensacionSuministro.getDurIntProExpReforzamiento());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 7, compensacionSuministro.getEneRegSemestre());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 8, compensacionSuministro.getMonComIntPtoEntrega());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 9, compensacionSuministro.getMonComLeyConcesiones());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 10, compensacionSuministro.getSemestre());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 11, compensacionSuministro.getNumSumAfectado());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 12, compensacionSuministro.getIndicadorN());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 13, compensacionSuministro.getIndicadorD());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 14, compensacionSuministro.getAno());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 15, compensacionSuministro.getMonComRecCarga());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 16, compensacionSuministro.getNumIntRecCarga());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 17, compensacionSuministro.getDurIntRecCarga());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 18, compensacionSuministro.getTipCompensacion());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 19, compensacionSuministro.getCodAlimentador());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 20, compensacionSuministro.getSisElectrico());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 21, compensacionSuministro.getNicIntNoProgramadas());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 22, compensacionSuministro.getNicIntProMantenimiento());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 23, compensacionSuministro.getNicIntProExpReforzamiento());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 24, compensacionSuministro.getDicIntNoProgramadas());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 25, compensacionSuministro.getDicIntProMantenimiento());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 26, compensacionSuministro.getDicIntProExpReforzamiento());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 27, compensacionSuministro.getNivelTension());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 28, compensacionSuministro.getUbiSuministro());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 29, compensacionSuministro.getCodLocalidad());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 30, compensacionSuministro.getAfectados());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 31, compensacionSuministro.getCodSed());
						ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 32, compensacionSuministro.getCompensado());
						
						intRow++;
						
				}
			
			ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());
	
	    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
	    facesContext.responseComplete();	
				
		return null;
	}	
		
	
	 //METODO REEMPLAZO  BOTON EXPORTAR COMPENSACION LCE
	  
	  //public String exportarCompensacionLCE(){
  public String exportarCompensacionLCE(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
		ConectaDb db = new ConectaDb("CALIDAD");		
        Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        int ctos;
        String result;
		
		
		String rutaUp;
		String rutaArchivoOut;
		String path = System.getProperty("uploads.folder");
		String nombreArchivo;
		int numFila;

		rutaUp = facesContext.getExternalContext().getRealPath(
				"/reportes/AnexoCompLCE.xlsx");

		Sql sql = new Sql("CALIDAD");
		String periodo = null;

		String ano = null;
		EmpresaService empService = new EmpresaService();
		ano = periodoMedicion.getAno();
		periodo = periodoMedicion.getPeriodo();
		
		String selectSql = "SELECT COD_INTERRUPCION,DURACION,DURACION_HP,DURACION_HFP,HORAS_MES,SUMINISTRO,ANO,MES,COS_RACIONAMIENTO,TARIFA,POT_DISTRIBUCION,POT_DIS_CARGO,POT_DIS_COMPENSAR,COM_POT_DISTRIBUCION,POT_DIS_HFP,POT_DIS_HFP_CARGO,POT_DIS_HFP_COMPENSAR,COM_POT_DIS_HFP,POT_DIS_HP,POT_DIS_HP_CARGO,POT_DIS_HP_COMPENSAR,COM_POT_DIS_HP,POT_GENERACION,POT_GEN_CARGO,POT_GEN_COMPENSAR,COM_POT_GENERACION,ENE_ACTIVA,ENE_ACT_CARGO,ENE_ACT_COMPENSAR,COM_ENE_ACTIVA,ENE_ACT_HP,ENE_ACT_HP_CARGO,ENE_ACT_HP_COMPENSAR,COM_ENE_ACT_HP,ENE_ACT_HFP,ENE_ACT_HFP_CARGO,ENE_ACT_HFP_COMPENSAR,COM_ENE_ACT_HFP,COM_TOTAL,SEMESTRE,TO_CHAR(FEC_INI_INTERRUPCION,'DD/MM/YYYY HH24:MI:SS'),TO_CHAR(FEC_FIN_INTERRUPCION,'DD/MM/YYYY HH24:MI:SS'),TIP_CAMBIO,COM_TOT_DOLARES, B1, B2, NHUBT FROM CAL_COMPENSACION_LCE M WHERE M.ANO = '"
				+ ano
				+ "' AND M.MES = '"
				+ periodo 
				+ "'  ORDER BY M.FEC_INI_INTERRUPCION ASC";
			
						
		
		List<Object[]> objTabla = sql.consulta(selectSql, false);
		
		nombreArchivo = "Anexo Comp_LCE"
				+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss")
						.format(new Date()) + ".xlsx";
		rutaArchivoOut = path + nombreArchivo;

		System.out.println("consulta:" + selectSql);
		
		XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);
		XSSFSheet objHssfSheet = null;
		objHssfSheet = objHssfWorkbook.getSheetAt(0);
		
		//ModificarExcel.setCellValue(objHssfSheet, 3, 2, empService.getAllRows().get(0).getEmpresa());
		//ModificarExcel.setCellValue(objHssfSheet, 3, 6, periodo);
		//ModificarExcel.setCellValue(objHssfSheet, 3, 8, ano);
		
		if (objTabla != null) {
		
			int intRow = 1;
			int intCell = 0;
			
			for (Object[] objFila : objTabla) {
								
				//System.out.println("llega:" + objTabla);
				intCell = 0;
				for (Object object : objFila) {
					
					BigDecimal campoBigDecimal;
					String campoString;
					try {
						campoString = (String) object;
						ModificarExcel.setCellValue(objHssfSheet, intRow,intCell, campoString);
					} catch (Exception e) {
						campoBigDecimal = (BigDecimal) object;
						ModificarExcel.setCellValue(objHssfSheet, intRow,intCell, campoBigDecimal);
					}
					intCell++;
				}
				intRow++;
			}
		}

		ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);

		HttpServletResponse response = ((HttpServletResponse) facesContext
				.getExternalContext().getResponse());
		writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,
				"application/vnd.ms-excel");// "text/xml"
		facesContext.responseComplete();
		
		return null;
	}
		  
	public String identificarInterrupcionesLCE(){

		ConectaDb db = new ConectaDb("CALIDAD");		
        Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        int ctos;
        String result;
		try {
		
			cstmt = cn.prepareCall("{CALL CAL_SUMINISTRO_PACKAGE.MARCAR_INTERRUPCIONES_LCE(?)}"); 
							
			cstmt.setLong(1,periodoMedicion.getPerMedicionId());


/*			ctos = cstmt.executeUpdate();
			if (ctos == 0) {
				result = "0 filas afectadas";
			}	
*/					
			cstmt.execute();
			cstmt.close();
		
			
		
		} catch (SQLException e) {
	        result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	            
	    } finally {
	        try {
	            cn.close();
	        } catch (SQLException e) {
	            result = e.getMessage();
				setMensaje(e.getMessage()+":"+e.getCause());
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	        }
	    }
		
		//InterrupcionesxPeriodo();
		
		mostrarMensaje("Proceso Culminado.");
		
		return "/zonasegura/interrupcion/interrupcionListar?faces-redirect=true";		
	}		  
	 	
	public void mostrarMensaje(String mensaje){
        FacesMessage fm = new FacesMessage(mensaje);

        fm.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, fm);
	}		
}


