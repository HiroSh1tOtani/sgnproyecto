package pe.com.calidad.suministro.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.calidad.suministro.entity.TipoInterrupcion;
import pe.com.calidad.suministro.service.TipoInterrupcionService;

@FacesConverter(value="tipoInterrupcionConverter",forClass=TipoInterrupcionConverter.class)

public class TipoInterrupcionConverter implements Converter{

	@Override
	public TipoInterrupcion getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long tipInterrupcionId=new Long(cadena);
		TipoInterrupcionService service=new TipoInterrupcionService();
		TipoInterrupcion tipoInterrupcion=new TipoInterrupcion();
		tipoInterrupcion=service.ReadById(tipInterrupcionId);
		System.out.println(tipoInterrupcion.getCodTipInterrupcion());
		return tipoInterrupcion;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			TipoInterrupcion tipoInterrupcion=(TipoInterrupcion) objeto;
			//System.out.println("objeto:"+new Long(tipoInterrupcion.getTipInterrupcionId()).toString());
			return new Long(tipoInterrupcion.getTipInterrupcionId()).toString();
		}
		return null;
	}


}
