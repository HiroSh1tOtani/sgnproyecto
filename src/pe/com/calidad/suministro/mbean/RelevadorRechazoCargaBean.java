package pe.com.calidad.suministro.mbean;


import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import org.hibernate.HibernateException;

import pe.com.calidad.proc228.service.DeficienciaService;
import pe.com.calidad.suministro.entity.RelevadorRechazoCarga;
import pe.com.calidad.suministro.service.RelevadorRechazoCargaService;
import pe.com.indra.calidad.entity.TipoMedicion;
import pe.com.indra.calidad.service.TipoMedicionService;
import pe.com.indra.calidad.util.ConectaDb;
import pe.com.indra.calidad.util.Sql;
import pe.com.indra.calidad.util.Utilidad;

@ManagedBean(name="relevadorRechazoCargaBean")
@SessionScoped
public class RelevadorRechazoCargaBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	private List<RelevadorRechazoCarga> relevadorRechazoCargas;
	private RelevadorRechazoCarga relevadorRechazoCarga;
	private String mensaje;
	private List<SelectItem> itemsAnoSemestre;
	private String anoSemestre;
	private String campoAnoSemestre;
	
		
	@PostConstruct
	public void init(){
		relevadorRechazoCarga=new RelevadorRechazoCarga();
		relevadorRechazoCarga.setEstado("1");
	}


	public List<RelevadorRechazoCarga> getRelevadorRechazoCargas() {
		return relevadorRechazoCargas;
	}

	public void setRelevadorRechazoCargas(
			List<RelevadorRechazoCarga> relevadorRechazoCargas) {
		this.relevadorRechazoCargas = relevadorRechazoCargas;
	}

	public RelevadorRechazoCarga getRelevadorRechazoCarga() {
		return relevadorRechazoCarga;
	}

	public void setRelevadorRechazoCarga(RelevadorRechazoCarga relevadorRechazoCarga) {
		this.relevadorRechazoCarga = relevadorRechazoCarga;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void  filtroChanged(AjaxBehaviorEvent event) {  
		
		busquedaxAnoSemestre();
	}
	

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public String prepararCrear(){
		//Esto funciona siempre que el bean tenga ambito de sesion 
		setRelevadorRechazoCarga(new RelevadorRechazoCarga());
		getRelevadorRechazoCarga().setEstado("1");
		getRelevadorRechazoCarga().setAno(anoSemestre.substring(0, 4));
		getRelevadorRechazoCarga().setSemestre(anoSemestre.substring(4));
		campoAnoSemestre = anoSemestre;
		return "/zonasegura/interrupcion/relevadorRechazoCargaCrear?faces-redirect=true";
	}
	
	public String prepararEditar(){
				
		try {
			cargarRelevadorRechazoCargaActual();
			
			return "/zonasegura/interrupcion/relevadorRechazoCargaEditar?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/interrupcion/relevadorRechazoCargaListar?faces-redirect=true";
		}
	}

	public String prepararVer(){
				
		try {
			cargarRelevadorRechazoCargaActual();
			return "/zonasegura/interrupcion/relevadorRechazoCargaVer?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/interrupcion/relevadorRechazoCargaListar?faces-redirect=true";
		}
	}
	
	public String salvar(){

		
		RelevadorRechazoCargaService service=new RelevadorRechazoCargaService();
		
		try {
			relevadorRechazoCarga.setAno(campoAnoSemestre.substring(0, 4));
			relevadorRechazoCarga.setSemestre(campoAnoSemestre.substring(4));
			service.create(relevadorRechazoCarga);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/interrupcion/relevadorRechazoCargaListar?faces-redirect=true";
	}
	
	public String actualizar(){
		
		RelevadorRechazoCargaService service=new RelevadorRechazoCargaService();
		
		try {
			relevadorRechazoCarga.setAno(campoAnoSemestre.substring(0, 4));
			relevadorRechazoCarga.setSemestre(campoAnoSemestre.substring(4));
			service.update(relevadorRechazoCarga);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/interrupcion/relevadorRechazoCargaListar?faces-redirect=true";
	}
	
	public String eliminar(){
		String relRechazoId=Utilidad.getParametro("relRechazoId");
		RelevadorRechazoCargaService service=new RelevadorRechazoCargaService();
		
		
		try {
			relevadorRechazoCarga=service.ReadById(new Long(relRechazoId));
			relevadorRechazoCarga.setEstado("0");
			//service.delete(localidad.getLocalidadId());
			service.update(relevadorRechazoCarga);
		} catch (HibernateException e) {
			
			
			System.out.println(e.getMessage()+":"+e.getCause());
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			
		} catch (Exception e) {
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/interrupcion/relevadorRechazoCargaListar?faces-redirect=true";
	}
	
	public void cargarRelevadorRechazoCargaActual(){
		String relRechazoId=Utilidad.getParametro("relRechazoId");
		RelevadorRechazoCargaService service=new RelevadorRechazoCargaService();

		
		try {
			relevadorRechazoCarga=service.ReadById(new Long(relRechazoId));
			campoAnoSemestre = relevadorRechazoCarga.getAno() + relevadorRechazoCarga.getSemestre();
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

			throw new HibernateException("No se puede cargar RelevadorRechazoCarga.", e);
			
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
	}

	public String busquedaxAnoSemestre() {
		String ano;
		String semestre;
		RelevadorRechazoCargaService service=new RelevadorRechazoCargaService();
		try {
			
			ano = anoSemestre.substring(0, 4);
			semestre = anoSemestre.substring(4);
			System.out.println("ano :" + ano);
			System.out.println("semestre:" + semestre);
			relevadorRechazoCargas=service.getBusquedaxAnoSemestre(ano.trim(), semestre.trim());
			
		} catch (HibernateException e) {
			setMensaje(e.getMessage());
		}
		
		return "/zonasegura/interrupcion/relevadorRechazoCargaListar?faces-redirect=true";
	}


	public List<SelectItem> getItemsAnoSemestre() {
		itemsAnoSemestre=new ArrayList<SelectItem>();
		Sql sql = new Sql("CALIDAD");
		String s = "SELECT DISTINCT ANO,SEMESTRE FROM CAL_PERIODO_MEDICION WHERE ESTADO = '1' ORDER BY ANO DESC ,SEMESTRE DESC";
		List<Object[]> listObject = sql.consulta(s, false);
		for (Object[] objects: listObject) {
			itemsAnoSemestre.add(new SelectItem((String)objects[0]+(String)objects[1],(String)objects[0]+ "-" + (String)objects[1]));
		}
		return itemsAnoSemestre;
	}

	public void setItemsAnoSemestre(List<SelectItem> itemsAnoSemestre) {
		this.itemsAnoSemestre = itemsAnoSemestre;
	}


	public String getAnoSemestre() {
		return anoSemestre;
	}


	public void setAnoSemestre(String anoSemestre) {
		this.anoSemestre = anoSemestre;
	}


	public String getCampoAnoSemestre() {
		return campoAnoSemestre;
	}


	public void setCampoAnoSemestre(String campoAnoSemestre) {
		this.campoAnoSemestre = campoAnoSemestre;
	}

	public String ActualizarRelevadores(){
		ConectaDb db = new ConectaDb("CALIDAD");		
        Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        int ctos;
        String result;
		try {
		
			cstmt = cn.prepareCall("{CALL CAL_SUMINISTRO_PACKAGE.COPIAR_RELEVADORES(?,?)}"); 
							
			cstmt.setString(1,anoSemestre.substring(0, 4));
			cstmt.setString(2,anoSemestre.substring(4));
			
			
			ctos = cstmt.executeUpdate();
			cstmt.close();
		
			if (ctos == 0) {
				result = "0 filas afectadas";
			}
		
		} catch (SQLException e) {
	        result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	            
	    } finally {
	        try {
	            cn.close();
	        } catch (SQLException e) {
	            result = e.getMessage();
				setMensaje(e.getMessage()+":"+e.getCause());
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	        }
	    }
		
		busquedaxAnoSemestre();
		
		return "/zonasegura/interrupcion/relevadorRechazoCargaListar?faces-redirect=true";
	}
}
