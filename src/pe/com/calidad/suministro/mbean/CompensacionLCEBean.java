package pe.com.calidad.suministro.mbean;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;

import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.PostConstruct;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


import pe.com.calidad.suministro.entity.CompensacionLCE;
import pe.com.indra.calidad.entity.PeriodoMedicion;
import pe.com.indra.calidad.util.ModificarExcel;
import pe.com.calidad.suministro.service.CompensacionLCEService;



@ManagedBean(name="compensacionLCEBean")
@SessionScoped
public class CompensacionLCEBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<CompensacionLCE> compensacionesLCE;
	private CompensacionLCE compensacionLCE;
	private PeriodoMedicion periodoMedicion;

	@PostConstruct 
	public void init(){
		
	}
	
	
	public List<CompensacionLCE> getCompensacionesLCE() {
		return compensacionesLCE;
	}


	public void setCompensacionesLCE(List<CompensacionLCE> compensacionesLCE) {
		this.compensacionesLCE = compensacionesLCE;
	}


	public CompensacionLCE getCompensacionLCE() {
		return compensacionLCE;
	}


	public void setCompensacionLCE(CompensacionLCE compensacionLCE) {
		this.compensacionLCE = compensacionLCE;
	}


	
	public PeriodoMedicion getPeriodoMedicion() {
		return periodoMedicion;
	}


	public void setPeriodoMedicion(PeriodoMedicion periodoMedicion) {
		this.periodoMedicion = periodoMedicion;
	}


	void writeOutContent(final HttpServletResponse res, final File content, final String theFilename, String contentType) {
	   if (content == null)
	     return;
	   try {
	     res.setHeader("Pragma", "no-cache");
	    res.setDateHeader("Expires", 0);
	    res.setContentType(contentType);
	    res.setHeader("Content-disposition", "attachment; filename=" + theFilename);
	    fastChannelCopy(Channels.newChannel(new FileInputStream(content)), Channels.newChannel(res.getOutputStream()));
	  } catch (final IOException e) {
	    // TODO produce a error message <img src="http://s0.wp.com/wp-includes/images/smilies/icon_smile.gif?m=1129645325g" alt=":)" class="wp-smiley"> 
	  }
	}
	 
	void fastChannelCopy(final ReadableByteChannel src, final WritableByteChannel dest) throws IOException {
	  final ByteBuffer buffer = ByteBuffer.allocateDirect(1000 * 1024);
	  while (src.read(buffer) != -1) {
	    buffer.flip();
	    dest.write(buffer);
	    buffer.compact();
	  }
	  buffer.flip();
	  while (buffer.hasRemaining()){
	    dest.write(buffer);
	  }
	}	
	


	
	public String exportarCompensacionLCE(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
	    
	    String rutaArchivoOut;
	    String rutaUp;
	    rutaUp = facesContext.getExternalContext().getRealPath("/reportes/Anexocompensacion.xlsx");
	    String path = System.getProperty("uploads.folder");
	    String nombreArchivo;
	    
	    System.out.print(path + "\n");
	    
	    nombreArchivo = "compensacion" + "_" + getCompensacionesLCE() + "_" + new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss")+".xlsx";
	    rutaArchivoOut = path + nombreArchivo;
	    	    
	    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);

		XSSFSheet objHssfSheet = null;
		
		try  {
	    
			CompensacionLCEService service = new CompensacionLCEService();
		    
		    objHssfSheet = objHssfWorkbook.getSheetAt(0);
			
			int intRow = 1;
			int intCell = 0;
			
			for (CompensacionLCE compensacionLCE : service.getAllRows1(periodoMedicion.getAno(), periodoMedicion.getPeriodo())) {
				
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell, compensacionLCE.getCodInterrupcion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 1, compensacionLCE.getDuracion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 1, compensacionLCE.getDuracion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 2, compensacionLCE.getDuracionHp());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 3, compensacionLCE.getDuracionHfp());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 4, compensacionLCE.getHorasMes());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 5, compensacionLCE.getSuministro());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 6, compensacionLCE.getAno());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 7, compensacionLCE.getMes());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 8, compensacionLCE.getCosRacionamiento());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 9, compensacionLCE.getTarifa());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 10, compensacionLCE.getPotDistribucion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 11, compensacionLCE.getPotDisCargo());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 12, compensacionLCE.getPotDisCompensar());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 13, compensacionLCE.getComPotDistribucion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 14, compensacionLCE.getPotDisHfp());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 15, compensacionLCE.getPotDisHfpCargo());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 16, compensacionLCE.getPotDisHfpCompensar());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 17, compensacionLCE.getComPotDisHfp());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 18, compensacionLCE.getPotDisHp());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 19, compensacionLCE.getPotDisHpCargo());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 20, compensacionLCE.getPotDisHpCompensar());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 21, compensacionLCE.getComPotDisHp());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 22, compensacionLCE.getPotGeneracion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 23, compensacionLCE.getPotGenCargo());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 24, compensacionLCE.getPotGenCompensar());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 25, compensacionLCE.getComPotGeneracion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 26, compensacionLCE.getEneActiva());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 27, compensacionLCE.getEneActCargo());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 28, compensacionLCE.getEneActCompensar());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 29, compensacionLCE.getComEneActiva());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 30, compensacionLCE.getEneActHp());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 31, compensacionLCE.getEneActHpCargo());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 32, compensacionLCE.getEneActHpCompensar());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 33, compensacionLCE.getComEneActHp());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 34, compensacionLCE.getEneActHfp());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 35, compensacionLCE.getEneActHfpCargo());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 36, compensacionLCE.getEneActHfpCompensar());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 37, compensacionLCE.getComEneActHfp());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 38, compensacionLCE.getComTotal());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 39, compensacionLCE.getSemestre());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 40, compensacionLCE.getFecIniInterrupcion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 41, compensacionLCE.getFecFinInterrupcion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 42, compensacionLCE.getTipCambio());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 43, compensacionLCE.getComTotDolares());
					intRow++;
					
	
			}
			
			ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());

	    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
	    facesContext.responseComplete();	
				
		return null;
	}
	

}
