package pe.com.calidad.suministro.mbean;


import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import pe.com.calidad.suministro.entity.ClientexSistema;
import pe.com.indra.calidad.entity.PeriodoMedicion;
import pe.com.indra.calidad.service.PeriodoMedicionService;
import pe.com.indra.calidad.util.ConectaDb;
import pe.com.calidad.suministro.service.ClientexSistemaService;


@ManagedBean(name="clientexSistemaBean")
@SessionScoped
public class ClientexSistemaBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<ClientexSistema> clientexSistemas;
	private ClientexSistema clientexSistema;
	private PeriodoMedicion periodoMedicion;
	private List<SelectItem> itemsPeriodoMedicion1;
	
	private String mensaje;
	
	@PostConstruct 
	public void init(){
		System.out.println("se ejecuto PostConstruct "+new java.util.Date());
		clientexSistema=new ClientexSistema();
			
	}
	
		
	public List<ClientexSistema> getClientexSistemas() {
		return clientexSistemas;
	}

	public void setClientexSistemas(List<ClientexSistema> clientexSistemas) {
		this.clientexSistemas = clientexSistemas;
	}


	public ClientexSistema getClientexSistema() {
		return clientexSistema;
	}

	public void setClientexSistema(ClientexSistema clientexSistema) {
		this.clientexSistema = clientexSistema;
	}

	
	public String ClientexSistemaxPeriodo() {
		ClientexSistemaService service=new ClientexSistemaService();
		
		
		if(periodoMedicion.getPerMedicionId() > 0) {
			clientexSistemas= service.getAllRows1(periodoMedicion.getPerMedicionId());
		}
		return "/zonasegura/interrupcion/clientexSistemaListar?faces-redirect=true";
	}
	
	
	public void  filtroChanged(AjaxBehaviorEvent event) {  
		
		ClientexSistemaxPeriodo();
	}
	
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public PeriodoMedicion getPeriodoMedicion() {
		return periodoMedicion;
	}

	public void setPeriodoMedicion(PeriodoMedicion periodoMedicion) {
		this.periodoMedicion = periodoMedicion;
	}


	public List<SelectItem> getItemsPeriodoMedicion1() {
		PeriodoMedicionService service=new PeriodoMedicionService();
		itemsPeriodoMedicion1 = new ArrayList<SelectItem>();
		for(PeriodoMedicion tp:service.getAllRows1()){
			itemsPeriodoMedicion1.add(new SelectItem(tp,tp.getAno()+"-"+tp.getPeriodo()));
		}
		return itemsPeriodoMedicion1;
	}


	public void setItemsPeriodoMedicion1(List<SelectItem> itemsPeriodoMedicion1) {
		this.itemsPeriodoMedicion1 = itemsPeriodoMedicion1;
	}
	
	public String calcularCantidadSuministros(){
		ConectaDb db = new ConectaDb("CALIDAD");		
        Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        int ctos;
        String result;
		try {
		
			cstmt = cn.prepareCall("{CALL CAL_SUMINISTRO_PACKAGE.CALCULAR_CANTIDAD_SUMINISTROS(?)}"); 
							
			cstmt.setLong(1,periodoMedicion.getPerMedicionId());
							
			ctos = cstmt.executeUpdate();
			cstmt.close();
		
			if (ctos == 0) {
				result = "0 filas afectadas";
			}
		
		} catch (SQLException e) {
	        result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	            
	    } finally {
	        try {
	            cn.close();
	        } catch (SQLException e) {
	            result = e.getMessage();
				setMensaje(e.getMessage()+":"+e.getCause());
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	        }
	    }
		
		ClientexSistemaxPeriodo();
		
		return "/zonasegura/interrupcion/clientexSistemaListar?faces-redirect=true";
	}
	
}
