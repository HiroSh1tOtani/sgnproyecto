package pe.com.calidad.suministro.mbean;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.service.InterrupcionService;

@FacesConverter(value="interrupcionConverter",forClass=InterrupcionConverter.class)

public class InterrupcionConverter implements Converter{

	@Override
	public Interrupcion getAsObject(FacesContext arg0, UIComponent arg1, String cadena) {
		System.out.println("Cadena:"+cadena);
		Long interrupcionId=new Long(cadena);
		InterrupcionService service=new InterrupcionService();
		Interrupcion interrupcion=new Interrupcion();
		interrupcion=service.ReadById(interrupcionId);
		System.out.println(interrupcion.getInterrupcionId());
		return interrupcion;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object objeto) {
		if(objeto!=null){
			Interrupcion interrupcion=(Interrupcion) objeto;
			////System.out.println("objeto:"+new Long(interrupcion.getInterrupcionId()).toString());
			return new Long(interrupcion.getInterrupcionId()).toString();
		}
		return null;
	}


}
