package pe.com.calidad.suministro.mbean;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.HibernateException;

import pe.com.calidad.suministro.entity.CompensacionSuministro;
import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.service.InterrupcionService;
import pe.com.calidad.suministro.service.CompensacionSuministroService;
import pe.com.indra.calidad.entity.PeriodoMedicion;
import pe.com.indra.calidad.util.ModificarExcel;
import pe.com.indra.calidad.util.Utilidad;

@ManagedBean(name="compensacionSuministroBean")
@SessionScoped
public class CompensacionSuministroBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<CompensacionSuministro> CompensacionesSuministro;
	private CompensacionSuministro compensacionSuministro;

	private long interrupcionId;

	private Interrupcion interrupcion;
	private String mensaje;
	private String numSumAfectado;
	private String semestre;
	
	private PeriodoMedicion periodoMedicion;
	

	@PostConstruct 
	public void init(){
		System.out.println("se ejecuto PostConstruct "+new java.util.Date());
		compensacionSuministro=new CompensacionSuministro();
		compensacionSuministro.setEstado("1");
	}
	
	public List<CompensacionSuministro> getCompensacionesSuministro() {
		CompensacionSuministroService service=new CompensacionSuministroService();
		CompensacionesSuministro=service.getAllRows();
		return CompensacionesSuministro;
	}

	public void setCompensacionesSuministro(
			List<CompensacionSuministro> compensacionesSuministro) {
		CompensacionesSuministro = compensacionesSuministro;
	}

	public CompensacionSuministro getCompensacionSuministro() {
		return compensacionSuministro;
	}

	public void setCompensacionSuministro(
			CompensacionSuministro compensacionSuministro) {
		this.compensacionSuministro = compensacionSuministro;
	}
	
	/*		
	public String prepararCrear(){
		//Esto funciona siempre que el bean tenga ambito de sesion 
		setCompensacionSuministro(new CompensacionSuministro());
		getCompensacionSuministro().setEstado("1");
		return "/zonasegura/interrupcion/compensacionSuministroCrear?faces-redirect=true";
	}*/
	/*
	public String prepararEditar(){
		
		try {
			cargarCompensacionSuministroActual();
			return "/zonasegura/interrupcion/compensacionSuministroEditar?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/interrupcion/suministroAfectadoListar?faces-redirect=true";
		}
	}*/

	public String prepararVer(){
		
		try {
			cargarCompensacionSuministroActual();
			
			return "/zonasegura/interrupcion/compensacionSuministroVer?faces-redirect=true";
		} catch (HibernateException e) {
			// TODO: handle exception
			setMensaje(e.getMessage());
			return "/zonasegura/interrupcion/suministroAfectadoListar?faces-redirect=true";
		}
	}
	/*
	public String salvar(){
				
		CompensacionSuministroService service=new CompensacionSuministroService();
		
		try {
			service.create(compensacionSuministro);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/interrupcion/compensacionSuministroListar?faces-redirect=true";
	}*/
	/*
	public String actualizar(){
			
		CompensacionSuministroService service=new CompensacionSuministroService();
		
		try {
			service.update(compensacionSuministro);
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/interrupcion/compensacionSuministroListar?faces-redirect=true";
	}*/
	/*
	public String eliminar(){
		String comSuministroId=Utilidad.getParametro("comSuministroId");
		CompensacionSuministroService service=new CompensacionSuministroService();
		
		
		try {
			compensacionSuministro=service.ReadById(new Long(comSuministroId));
			compensacionSuministro.setEstado("0");
			
			service.update(compensacionSuministro);
		} catch (HibernateException e) {
			
			
			System.out.println(e.getMessage()+":"+e.getCause());
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			
			
		} catch (Exception e) {
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
		return "/zonasegura/interrupcion/compensacionSuministroListar?faces-redirect=true";
	}*/
	
	public void cargarCompensacionSuministroActual(){
		
		
		String numSumAfectadoStr=Utilidad.getParametro("numSumAfectado");		
		String semestreStr = Utilidad.getParametro("semestre");
		numSumAfectado = String.valueOf(numSumAfectadoStr); //capturar valor
		semestre = String.valueOf(semestreStr);//capturar valor
		
				
		CompensacionSuministroService compensacionSuministroService =  new CompensacionSuministroService();
		
		
		try {
			compensacionSuministro = compensacionSuministroService.getAllRows1(numSumAfectado, semestre);
			
		/*	if(compensacionSuministro == null){
				return;
			}*/
		} catch (HibernateException e) {
			setMensaje(e.getMessage()+":"+e.getCause());			
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

			throw new HibernateException("No se puede cargar Compensacion Suministro.", e);
			
		} catch (Exception e) {
			
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacción", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		}
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public long getInterrupcionId() {
		return interrupcionId;
	}

	public void setInterrupcionId(long interrupcionId) {
		this.interrupcionId = interrupcionId;
	}

	public String getNumSumAfectado() {
		return numSumAfectado;
	}

	public void setNumSumAfectado(String numSumAfectado) {
		this.numSumAfectado = numSumAfectado;
	}

	public String getSemestre() {
		return semestre;
	}

	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}
	
	public PeriodoMedicion getPeriodoMedicion() {
		return periodoMedicion;
	}

	public void setPeriodoMedicion(PeriodoMedicion periodoMedicion) {
		this.periodoMedicion = periodoMedicion;
	}

	public Interrupcion getInterrupcion() {
		InterrupcionService service=new InterrupcionService();
		interrupcion=service.ReadById(new Long(interrupcionId));
		return interrupcion;
	}

	public void setInterrupcion(Interrupcion interrupcion) {
		this.interrupcion = interrupcion;
	}

	public String exportarComp(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
	    
	    String rutaArchivoOut;
	    String rutaUp;
	    rutaUp = facesContext.getExternalContext().getRealPath("/reportes/AnexocompSum.xlsx");
	    String path = System.getProperty("uploads.folder");
	    String nombreArchivo;
	    
		String semestre = null;
				
		semestre = periodoMedicion.getSemestre();
	    
	    System.out.print(path + "\n");
	    
	    nombreArchivo = "Compen_Sum" + "_" + getPeriodoMedicion().getAno() + "_" + getPeriodoMedicion().getSemestre() + "_"+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsx";
	    rutaArchivoOut = path + nombreArchivo;
	    	    
	    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);

		XSSFSheet objHssfSheet = null;
		
		try  {
	    
			CompensacionSuministroService service = new CompensacionSuministroService();
		    
		    objHssfSheet = objHssfWorkbook.getSheetAt(0);
			
			int intRow = 1;
			int intCell = 0;
			
			//ModificarExcel.setCellValue(objHssfSheet, 3, 5, semestre);
			
			
			for (CompensacionSuministro compensacionSuministro : service.getAllComp(periodoMedicion.getAno(), periodoMedicion.getSemestre())) {
				
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell, compensacionSuministro.getCodRelevador());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 1, compensacionSuministro.getNumIntNoProgramadas());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 2, compensacionSuministro.getNumIntProMantenimiento());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 3, compensacionSuministro.getNumIntProExpReforzamiento());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 4, compensacionSuministro.getDurIntNoProgramadas());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 5, compensacionSuministro.getDurIntProMantenimiento());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 6, compensacionSuministro.getDurIntProExpReforzamiento());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 7, compensacionSuministro.getEneRegSemestre());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 8, compensacionSuministro.getMonComIntPtoEntrega());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 9, compensacionSuministro.getMonComLeyConcesiones());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 10, compensacionSuministro.getSemestre());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 11, compensacionSuministro.getNumSumAfectado());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 12, compensacionSuministro.getIndicadorN());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 13, compensacionSuministro.getIndicadorD());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 14, compensacionSuministro.getAno());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 15, compensacionSuministro.getMonComRecCarga());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 16, compensacionSuministro.getNumIntRecCarga());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 17, compensacionSuministro.getDurIntRecCarga());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 18, compensacionSuministro.getTipCompensacion());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 19, compensacionSuministro.getCodAlimentador());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 20, compensacionSuministro.getSisElectrico());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 21, compensacionSuministro.getNicIntNoProgramadas());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 22, compensacionSuministro.getNicIntProMantenimiento());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 23, compensacionSuministro.getNicIntProExpReforzamiento());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 24, compensacionSuministro.getDicIntNoProgramadas());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 25, compensacionSuministro.getDicIntProMantenimiento());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 26, compensacionSuministro.getDicIntProExpReforzamiento());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 27, compensacionSuministro.getNivelTension());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 28, compensacionSuministro.getUbiSuministro());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 29, compensacionSuministro.getCodLocalidad());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 30, compensacionSuministro.getAfectados());
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 31, compensacionSuministro.getCodSed());
					intRow++;
					
	
			}
			
			ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());

	    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
	    facesContext.responseComplete();	
				
		return null;
	}	
	
	void writeOutContent(final HttpServletResponse res, final File content, final String theFilename, String contentType) {
		   if (content == null)
		     return;
		   try {
		     res.setHeader("Pragma", "no-cache");
		    res.setDateHeader("Expires", 0);
		    res.setContentType(contentType);
		    res.setHeader("Content-disposition", "attachment; filename=" + theFilename);
		    fastChannelCopy(Channels.newChannel(new FileInputStream(content)), Channels.newChannel(res.getOutputStream()));
		  } catch (final IOException e) {
		    // TODO produce a error message <img src="http://s0.wp.com/wp-includes/images/smilies/icon_smile.gif?m=1129645325g" alt=":)" class="wp-smiley"> 
		  }
		}
		 
		void fastChannelCopy(final ReadableByteChannel src, final WritableByteChannel dest) throws IOException {
		  final ByteBuffer buffer = ByteBuffer.allocateDirect(1000 * 1024);
		  while (src.read(buffer) != -1) {
		    buffer.flip();
		    dest.write(buffer);
		    buffer.compact();
		  }
		  buffer.flip();
		  while (buffer.hasRemaining()){
		    dest.write(buffer);
		  }
		}	
	
}
