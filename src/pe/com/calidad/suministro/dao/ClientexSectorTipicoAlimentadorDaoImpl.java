/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.suministro.dao;

import java.util.Set;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.calidad.suministro.entity.ClientexAlimentador;
import pe.com.calidad.suministro.entity.ClientexSectorTipico;
import pe.com.calidad.suministro.entity.ClientexSistema;//Indicador074;
import pe.com.calidad.suministro.entity.Indicador074;
import pe.com.indra.calidad.dao.HibernateUtil;
import pe.com.indra.calidad.entity.PeriodoMedicion;

/**
 *
 * @author Luis
 */
public class ClientexSectorTipicoAlimentadorDaoImpl implements ClientexSectorTipicoAlimentadorDao {

    private static ClientexSectorTipicoAlimentadorDaoImpl instance = null;

	@Override
	public List<ClientexSectorTipico> getClientexSectorTipico(long perMedicionId) {
		List<ClientexSectorTipico> clientexSectorTipico=null;
		try {
            HibernateUtil.begin();
            
            Query q = HibernateUtil.getSession().createQuery("from ClientexSectorTipico i where  i.periodoMedicion = :perMedicionId");
            q.setLong("perMedicionId", perMedicionId);
            
            clientexSectorTipico=(List<ClientexSectorTipico>) q.list();
           
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Cliente x Sector Electrico.", e);
        }
		return clientexSectorTipico;
	}

	@Override
	public List<ClientexAlimentador> getClientexAlimentador(long perMedicionId) {
		List<ClientexAlimentador> clientexAlimentador=null;
		try {
            HibernateUtil.begin();
            
            Query q = HibernateUtil.getSession().createQuery("from ClientexAlimentador i where  i.periodoMedicion = :perMedicionId");
            //q.setString("estado","1");
            q.setLong("perMedicionId", perMedicionId);
            
            clientexAlimentador=(List<ClientexAlimentador>) q.list();
            
           
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Cliente x Alimentador.", e);
        }
		return clientexAlimentador;
	}

}
