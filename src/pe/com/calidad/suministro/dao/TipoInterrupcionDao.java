package pe.com.calidad.suministro.dao;

import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.entity.TipoInterrupcion;

import java.util.List;
import java.util.Set;

/**
 *
 * @author Luis
 */
public interface TipoInterrupcionDao {

    public void create(TipoInterrupcion tipoInterrupcion);
    public TipoInterrupcion ReadById(long tipInterrupcionId);
    public TipoInterrupcion ReadByCod(String codTipInterrupcion);
    public void update(TipoInterrupcion tipoInterrupcion);
    public void delete(long tipInterrupcionId);
    public Set<Interrupcion> selectInterrupcion(long tipInterrupcionId);
    public List<TipoInterrupcion> getAllRows();
    public List<TipoInterrupcion> getAllRows1();
    
}
