package pe.com.calidad.suministro.dao;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import java.util.List;
import java.util.Set;

import pe.com.calidad.suministro.entity.CargoTarifa;
import pe.com.calidad.suministro.entity.CompensacionSuministro;
import pe.com.calidad.suministro.entity.CostoRacionamiento;//CompensacionSuministro;
import pe.com.calidad.suministro.entity.Indicador074;
import pe.com.calidad.suministro.entity.Pliego;
import pe.com.indra.calidad.dao.HibernateUtil;
import pe.com.indra.calidad.entity.PeriodoMedicion;


/**
 *
 * @author Luis
 */
public class CargoTarifaDaoImpl implements CargoTarifaDao {

    private static CargoTarifaDaoImpl instance = null;

    public static CargoTarifaDaoImpl getInstance() {
        if (instance == null) {
            instance = new CargoTarifaDaoImpl();
        }

        return instance;
    }

    public CargoTarifaDaoImpl() {
    }

	@Override
	public void create(CargoTarifa cargoTarifa) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(cargoTarifa);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Cargo Tarifa.", e);
        }
    }

	@Override
	public CargoTarifa ReadById(long carTarifaId) {
		CargoTarifa cargoTarifa = null;
        try {
            HibernateUtil.begin();

            cargoTarifa = (CargoTarifa) HibernateUtil.getSession().load(CargoTarifa.class, carTarifaId);

            Hibernate.initialize(cargoTarifa);
            Hibernate.initialize(cargoTarifa.getPliego());
          
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Cargo Tarifa.", e);
        }
        return cargoTarifa;
    }

	@Override
	public void update(CargoTarifa cargoTarifa) {
		 try {
	            HibernateUtil.begin();
	            HibernateUtil.getSession().update(cargoTarifa);
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede Cargo Tarifa.", e);
	        }
		
	}

	@Override
	public void delete(long carTarifaId) {
		CargoTarifa cargoTarifa = null;
		cargoTarifa = ReadById(carTarifaId);

        if (cargoTarifa != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(cargoTarifa);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Cargo Tarifa.", e);
            }
        }
		
	}

	@Override
	public List<CargoTarifa> getAllRows() {
		List<CargoTarifa> cargoTarifa=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from CargoTarifa");
	            
	            cargoTarifa=(List<CargoTarifa>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Cargo Tarifa.", e);
	        }
		return cargoTarifa;
	}

	@Override
	public List<CargoTarifa> getAllRows1() {
		List<CargoTarifa> cargoTarifa=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from CargoTarifa where estado =:estado");
	            q.setString("estado","1");
	            cargoTarifa=(List<CargoTarifa>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Cargo Tarifa .", e);
	        }
		return cargoTarifa;
	}

	@Override
	public List<CargoTarifa> getAllRows1(long pliegoId) {
		List<CargoTarifa> cargoTarifa=null;
		try {
            HibernateUtil.begin();
            
            Query q = HibernateUtil.getSession().createQuery("from CargoTarifa i where i.estado =:estado and i.pliego = :pliegoId");
            q.setString("estado","1");
            q.setLong("pliegoId", pliegoId);
            
            cargoTarifa=(List<CargoTarifa>) q.list();
            
           
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Cargo Tarifa.", e);
        }
	return cargoTarifa;
	
	}

	@Override
	public Pliego selectPliego(long carTarifaId) {
		CargoTarifa cargoTarifa = null;
		cargoTarifa = ReadById(carTarifaId);

        return cargoTarifa.getPliego();
		
	}
	
}
