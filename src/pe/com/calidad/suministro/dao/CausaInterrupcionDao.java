package pe.com.calidad.suministro.dao;

import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.entity.CausaInterrupcion;

import java.util.List;
import java.util.Set;

/**
 *
 * @author Lucho
 * 
 */
public interface CausaInterrupcionDao {

    public void create(CausaInterrupcion causaInterrupcion);
    public CausaInterrupcion ReadById(long cauInterrupcionId);
    public CausaInterrupcion ReadByCod(String codCauInterrupcion);
    public void update(CausaInterrupcion causaInterrupcion);
    public void delete(long cauInterrupcionId);
    public Set<Interrupcion> selectInterrupcion(long cauInterrupcionId);
    public List<CausaInterrupcion> getAllRows();
    public List<CausaInterrupcion> getAllRows1();
   
}
