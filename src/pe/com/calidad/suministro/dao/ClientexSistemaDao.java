package pe.com.calidad.suministro.dao;

/**
 *
 * @author Luis
 */
import java.util.List;


import pe.com.calidad.suministro.entity.ClientexSistema;
import pe.com.indra.calidad.entity.PeriodoMedicion;
import java.util.Set;

public interface ClientexSistemaDao {

    public void create(ClientexSistema clientexSistema);
    public ClientexSistema ReadById(long cliexSistemaId);
    public void update(ClientexSistema clientexSistema);
    public void delete(long cliexSistemaId);

    public PeriodoMedicion selectPeriodo(long cliexSistemaId);
  
    
    
    public List<ClientexSistema> getAllRows();
    public List<ClientexSistema> getAllRows1();
    public List<ClientexSistema> getAllRows1(long perMedicionId);
    
}
