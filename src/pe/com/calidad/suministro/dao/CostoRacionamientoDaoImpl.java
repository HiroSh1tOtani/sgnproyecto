package pe.com.calidad.suministro.dao;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import java.util.List;
import pe.com.calidad.suministro.entity.CostoRacionamiento;
import pe.com.indra.calidad.dao.HibernateUtil;


/**
 *
 * @author Luis
 */
public class CostoRacionamientoDaoImpl implements CostoRacionamientoDao {

    private static CostoRacionamientoDaoImpl instance = null;

    public static CostoRacionamientoDaoImpl getInstance() {
        if (instance == null) {
            instance = new CostoRacionamientoDaoImpl();
        }

        return instance;
    }

    public CostoRacionamientoDaoImpl() {
    }

	@Override
	public void create(CostoRacionamiento costoRacionamiento) {

        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(costoRacionamiento);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Costo Racionamiento.", e);
        }
  	
	}

	@Override
	public CostoRacionamiento ReadById(long cosRacionamientoId) {
		CostoRacionamiento costoRacionamiento = null;
        try {
            HibernateUtil.begin();

            costoRacionamiento = (CostoRacionamiento) HibernateUtil.getSession().load(CostoRacionamiento.class, cosRacionamientoId);

            Hibernate.initialize(costoRacionamiento);
         
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar  Costo Racionamiento.", e);
        }
        return costoRacionamiento;
    }

	@Override
	public void update(CostoRacionamiento costoRacionamiento) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(costoRacionamiento);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Costo Racionamiento.", e);
        }
    }

	@Override
	public void delete(long cosRacionamientoId) {
		CostoRacionamiento costoRacionamiento = null;
		costoRacionamiento = ReadById(cosRacionamientoId);

        if (costoRacionamiento != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(costoRacionamiento);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Costo Racionamiento.", e);
            }
        }
    }

	@Override
	public List<CostoRacionamiento> getAllRows() {
		List<CostoRacionamiento> costoRacionamiento=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from CostoRacionamiento");
	            
	            costoRacionamiento=(List<CostoRacionamiento>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Costo Racionamiento .", e);
	        }
		return costoRacionamiento;
	}

	@Override
	public List<CostoRacionamiento> getAllRows1() {
		List<CostoRacionamiento> costoRacionamiento=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from CostoRacionamiento where estado =:estado");
	            q.setString("estado","1");
	            costoRacionamiento=(List<CostoRacionamiento>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Costo Racionamiento .", e);
	        }
		return costoRacionamiento;
	}

   

	
}
