
package pe.com.calidad.suministro.dao;

import java.util.List;


import pe.com.calidad.suministro.entity.ParametroAnual;//RelevadorRechazoCarga;

/**
 *
 * @author Lucho
 */
public interface ParametroAnualDao {
    public void create(ParametroAnual parametroAnual);
    public ParametroAnual ReadById(long parAnualId);
	public void update(ParametroAnual parametroAnual);
	public void delete(long parAnualId);
   	public List<ParametroAnual> getAllRows();
	public List<ParametroAnual> getBusquedaxAno(String ano);
	
        
}
