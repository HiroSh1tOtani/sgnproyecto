/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.suministro.dao;

import java.util.Set;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.calidad.suministro.entity.ClientexSistema;//Indicador074;
import pe.com.calidad.suministro.entity.Indicador074;
import pe.com.indra.calidad.dao.HibernateUtil;
import pe.com.indra.calidad.entity.PeriodoMedicion;

/**
 *
 * @author Luis
 */
public class ClientexSistemaDaoImpl implements ClientexSistemaDao {

    private static ClientexSistemaDaoImpl instance = null;

    public static ClientexSistemaDaoImpl getInstance() {
        if (instance == null) {
            instance = new ClientexSistemaDaoImpl();
        }

        return instance;
    }

    public ClientexSistemaDaoImpl() {
    }

	@Override
	public void create(ClientexSistema clientexSistema) {

		try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(clientexSistema);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
           
            throw new HibernateException("No se puede crear clientexSistema", e);
        }
	
	}

	@Override
	public ClientexSistema ReadById(long cliexSistemaId) {

		ClientexSistema clientexSistema = null;
        try {
            HibernateUtil.begin();

            clientexSistema = (ClientexSistema) HibernateUtil.getSession().load(ClientexSistema.class, cliexSistemaId);

            Hibernate.initialize(clientexSistema);
            Hibernate.initialize(clientexSistema.getPeriodoMedicion());
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar clientexSistema.", e);
        }
        return clientexSistema;
	}

	@Override
	public void update(ClientexSistema clientexSistema) {
		 try {
	            HibernateUtil.begin();
	            HibernateUtil.getSession().update(clientexSistema);
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede actualizar clientexSistema.", e);
	        }
		
	}

	@Override
	public void delete(long cliexSistemaId) {
		ClientexSistema clientexSistema = null;
		clientexSistema = ReadById(cliexSistemaId);

        if (clientexSistema != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(clientexSistema);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar clientexSistema.", e);
            }
        }
		
	}

	@Override
	public PeriodoMedicion selectPeriodo(long cliexSistemaId) {
		ClientexSistema clientexSistema = null;
		clientexSistema = ReadById(cliexSistemaId);

        return clientexSistema.getPeriodoMedicion();
		
	}

	@Override
	public List<ClientexSistema> getAllRows() {
		List<ClientexSistema> clientesxSistema=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from ClientexSistema ");
	            
	            clientesxSistema=(List<ClientexSistema>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar ClientexSistema.", e);
	        }
		return clientesxSistema;
		
	}

	@Override
	public List<ClientexSistema> getAllRows1() {
		List<ClientexSistema> clientesxSistema=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from ClientexSistema where estado =:estado ");
	            q.setString("estado","1");
	            clientesxSistema=(List<ClientexSistema>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar ClientexSistema.", e);
	        }
		return clientesxSistema;
	}

	@Override
	public List<ClientexSistema> getAllRows1(long perMedicionId) {
		List<ClientexSistema> clientesxSistema=null;
		try {
            HibernateUtil.begin();
            
            Query q = HibernateUtil.getSession().createQuery("from ClientexSistema i where  i.periodoMedicion = :perMedicionId");
            //q.setString("estado","1");
            q.setLong("perMedicionId", perMedicionId);
            
            clientesxSistema=(List<ClientexSistema>) q.list();
            
           
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Cliente _x Sistema.", e);
        }
	return clientesxSistema;
	}

	

}
