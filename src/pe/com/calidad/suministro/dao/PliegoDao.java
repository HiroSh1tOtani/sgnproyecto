
package pe.com.calidad.suministro.dao;

import java.util.List;
import java.util.Set;

import pe.com.calidad.suministro.entity.CargoTarifa;
import pe.com.calidad.suministro.entity.Pliego;


/**
 *
 * @author Lucho
 */
public interface PliegoDao {
    public void create(Pliego pliego);
    public Pliego ReadById(long pliegoId);
	public void update(Pliego pliego);
	public void delete(long pliegoId);
	 public Set<CargoTarifa> selectCargoTarifa(long pliegoId);
   	public List<Pliego> getAllRows();
	public List<Pliego> getAllRows1();
	
        
}
