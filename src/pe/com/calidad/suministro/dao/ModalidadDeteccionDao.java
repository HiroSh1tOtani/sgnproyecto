package pe.com.calidad.suministro.dao;

import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.entity.ModalidadDeteccion;

import java.util.List;
import java.util.Set;

/**
 *
 * @author Luis
 */
public interface ModalidadDeteccionDao {

    public void create(ModalidadDeteccion modalidadDeteccion);
    public ModalidadDeteccion ReadById(long modDeteccionId);
    public ModalidadDeteccion ReadByCod(String codModDeteccion);
    public void update(ModalidadDeteccion modalidadDeteccion);
    public void delete(long modDeteccionId);
    public Set<Interrupcion> selectInterrupcion(long modDeteccionId);
    public List<ModalidadDeteccion> getAllRows();
    public List<ModalidadDeteccion> getAllRows1();
    
    
}
