/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.suministro.dao;


import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.entity.TipoNotificacion;//CausaInterrupcion;
import pe.com.indra.calidad.dao.HibernateUtil;

import java.util.Set;

import org.hibernate.Hibernate;

/**
 *
 * @author Luis
 */
public class TipoNotificacionDaoImpl implements TipoNotificacionDao {

    private static TipoNotificacionDaoImpl instance = null;

    public static TipoNotificacionDaoImpl getInstance() {
        if (instance == null) {
            instance = new TipoNotificacionDaoImpl();
        }

        return instance;
    }

    public TipoNotificacionDaoImpl() {
    }

    @Override
    public void create(TipoNotificacion tipoNotificacion) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(tipoNotificacion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Tipo de Notificacion.", e);
        } 
    }

    @Override
    public TipoNotificacion ReadById(long tipNotificacionId) {
    	TipoNotificacion tipoNotificacion = null;
        try {
            HibernateUtil.begin();
            
            /*
            Query q = HibernateUtil.getSession().createQuery("from CAL_TIPO_NOTIFICACION where TIP_NOTTIFICACION_ID = :tipNotificacionId");
            q.setLong("tipNotificacionId",tipNotificacionId);
            causaInterrupcion = (CausaInterrupcion)q.uniqueResult();*/
            
            tipoNotificacion = (TipoNotificacion) HibernateUtil.getSession().load(TipoNotificacion.class, tipNotificacionId);
            
            Hibernate.initialize(tipoNotificacion);
            Hibernate.initialize(tipoNotificacion.getInterrupciones()); 
        
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Tipo de Notificacion.", e);
        } 
        return tipoNotificacion;
    }

    @Override
    public TipoNotificacion ReadByCod(String codTipNotificacion) {
    	TipoNotificacion tipoNotificacion = null;
        try {
            HibernateUtil.begin();
            
            Query q = HibernateUtil.getSession().createQuery("from TipoNotificacion where codTipNotificacion = :codTipNotificacion and estado = '1'");
            q.setString("codTipNotificacion",codTipNotificacion);
            tipoNotificacion = (TipoNotificacion)q.uniqueResult();
            
            Hibernate.initialize(tipoNotificacion);
            Hibernate.initialize(tipoNotificacion.getInterrupciones()); 
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Tipo de Notificacion.", e);
        } 
        return tipoNotificacion;
    }

    @Override
    public void update(TipoNotificacion tipoNotificacion) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(tipoNotificacion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Tipo de Notificacion.", e);
        } 
    }

    @Override
    public void delete(long tipNotificacionId) {
        
    	TipoNotificacion tipoNotificacion = null;
    	tipoNotificacion = ReadById(tipNotificacionId);
        
        if(tipoNotificacion != null){               
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(tipoNotificacion);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Tipo de Notificacion.", e); 
            }
        }
    }

       
	@SuppressWarnings("unchecked")
	@Override
	public List<TipoNotificacion> getAllRows() {
		  List<TipoNotificacion> interrupciones=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from TipoNotificacion");
	            interrupciones= (List<TipoNotificacion>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Tipo de Notificacion.", e);
	        } 
		return interrupciones;
	}
	
	@Override
	public List<TipoNotificacion> getAllRows1() {
		  List<TipoNotificacion> interrupciones=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from TipoNotificacion where estado =:estado");
	            q.setString("estado","1");
	            interrupciones= (List<TipoNotificacion>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Tipo de Notificacion.", e);
	        } 
		return interrupciones;
	}
	
	
}
