
package pe.com.calidad.suministro.dao;

import java.util.List;

import pe.com.calidad.suministro.entity.CargoTarifa;
import pe.com.calidad.suministro.entity.Pliego;
import pe.com.indra.calidad.entity.PeriodoMedicion;




/**
 *
 * @author Lucho
 */
public interface CargoTarifaDao {
    public void create(CargoTarifa cargoTarifa);
    public CargoTarifa ReadById(long carTarifaId);
	public void update(CargoTarifa cargoTarifa);
	public void delete(long carTarifaId);
	
	public Pliego selectPliego(long carTarifaId);
	
   	public List<CargoTarifa> getAllRows();
	public List<CargoTarifa> getAllRows1();
	public List<CargoTarifa> getAllRows1(long pliegoId);
	
        
}
