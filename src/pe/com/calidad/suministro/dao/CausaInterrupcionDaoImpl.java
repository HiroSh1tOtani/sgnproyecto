/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.suministro.dao;


import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Hibernate;



import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.entity.CausaInterrupcion;
import pe.com.indra.calidad.dao.HibernateUtil;

import java.util.Set;



/**
 *
 * @author Luis
 */
public class CausaInterrupcionDaoImpl implements CausaInterrupcionDao {

    private static CausaInterrupcionDaoImpl instance = null;

    public static CausaInterrupcionDaoImpl getInstance() {
        if (instance == null) {
            instance = new CausaInterrupcionDaoImpl();
        }

        return instance;
    }

    public CausaInterrupcionDaoImpl() {
    }

    @Override
    public void create(CausaInterrupcion causaInterrupcion) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(causaInterrupcion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Causa de Interrupcion.", e);
        } 
    }

    @Override
    public CausaInterrupcion ReadById(long cauInterrupcionId) {
    	CausaInterrupcion causaInterrupcion = null;
        try {
            HibernateUtil.begin();
            
            /*
            Query q = HibernateUtil.getSession().createQuery("from CAL_CAUSA_INTERRUPCION where CAU_INTERRUPCION_ID = :cauInterrupcionId");
            q.setLong("cauiInterrupcionId",cauiInterrupcionId);
            causaInterrupcion = (CausaInterrupcion)q.uniqueResult();*/
            
            causaInterrupcion = (CausaInterrupcion) HibernateUtil.getSession().load(CausaInterrupcion.class, cauInterrupcionId);
            
            Hibernate.initialize(causaInterrupcion);
            Hibernate.initialize(causaInterrupcion.getInterrupciones()); 
        
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Causa de Interrupcion.", e);
        } 
        return causaInterrupcion;
    }

    @Override
    public CausaInterrupcion ReadByCod(String codCauInterrupcion) {
    	CausaInterrupcion causaInterrupcion = null;
        try {
            HibernateUtil.begin();
            
            Query q = HibernateUtil.getSession().createQuery("from CausaInterrupcion where codCauInterrupcion = :codCauInterrupcion and estado = '1'");
            q.setString("codCauInterrupcion",codCauInterrupcion);
            causaInterrupcion = (CausaInterrupcion)q.uniqueResult();
            
            Hibernate.initialize(causaInterrupcion);
            Hibernate.initialize(causaInterrupcion.getInterrupciones()); 
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Causa de Interrupcion.", e);
        } 
        return causaInterrupcion;
    }

    @Override
    public void update(CausaInterrupcion causaInterrupcion) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(causaInterrupcion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Causa de Interrupcion.", e);
        } 
    }

    @Override
    public void delete(long cauInterrupcionId) {
        
    	CausaInterrupcion causaInterrupcion = null;
    	causaInterrupcion = ReadById(cauInterrupcionId);
        
        if(causaInterrupcion != null){               
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(causaInterrupcion);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Causa de Interrupcion.", e); 
            }
        }
    }

    @Override
    public Set<Interrupcion> selectInterrupcion(long cauInterrupcionId) {
    /*
        String hql = "from CAL_CAUSA_INTERRUPCION where PK_CAU_INTERRUPCION_ID = :cauiInterrupcionId";
        Query query = HibernateUtil.getSession().createQuery(hql);
        query.setLong("cauiInterrupcionId",cauiInterrupcionId);
        Set<Interrupcion> results = (Set<Interrupcion>)query.list();
        
        return results;
    */
    	CausaInterrupcion causaInterrupcion = null;
        causaInterrupcion = ReadById(cauInterrupcionId);
        
        return causaInterrupcion.getInterrupciones();
    }
   
	@SuppressWarnings("unchecked")
	@Override
	public List<CausaInterrupcion> getAllRows() {
		  List<CausaInterrupcion> interrupciones=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from CausaInterrupcion  order by cauInterrupcionId asc");
	            interrupciones= (List<CausaInterrupcion>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Causa de Interrupcion.", e);
	        } 
		return interrupciones;
	}
	
	@Override
	public List<CausaInterrupcion> getAllRows1() {
		  List<CausaInterrupcion> interrupciones=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from CausaInterrupcion where estado =:estado order by cauInterrupcionId asc");
	            q.setString("estado","1");
	            interrupciones= (List<CausaInterrupcion>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Causa de Interrupcion.", e);
	        } 
		return interrupciones;
	}
	
	
}
