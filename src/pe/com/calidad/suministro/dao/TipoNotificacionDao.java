package pe.com.calidad.suministro.dao;


import pe.com.calidad.suministro.entity.TipoNotificacion;

import java.util.List;
import java.util.Set;

/**
 *
 * @author Luis
 */
public interface TipoNotificacionDao {

    public void create(TipoNotificacion tipoNotificacion);
    public TipoNotificacion ReadById(long tipNotificacionId);
    public TipoNotificacion ReadByCod(String codTipNotificacion);
    public void update(TipoNotificacion tipoNotificacion);
    public void delete(long tipNotificacionId);
    public List<TipoNotificacion> getAllRows();
    public List<TipoNotificacion> getAllRows1();
    
    
}
