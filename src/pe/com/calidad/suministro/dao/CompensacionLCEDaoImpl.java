package pe.com.calidad.suministro.dao;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import java.util.List;
import pe.com.calidad.suministro.entity.CompensacionLCE;
import pe.com.indra.calidad.dao.HibernateUtil;


/**
 *
 * @author Luis
 */
public class CompensacionLCEDaoImpl implements CompensacionLCEDao {

    private static CompensacionLCEDaoImpl instance = null;

    public static CompensacionLCEDaoImpl getInstance() {
        if (instance == null) {
            instance = new CompensacionLCEDaoImpl();
        }

        return instance;
    }

    public CompensacionLCEDaoImpl() {
    }

	

	@Override
	public List<CompensacionLCE> getAllRows() {
		List<CompensacionLCE> compensacionLCE=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from CompensacionLCE");
	            
	            compensacionLCE=(List<CompensacionLCE>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar CompensacionLCE .", e);
	        }
		return compensacionLCE;
	}

	@Override
	public List<CompensacionLCE> getAllRows1(String ano,String mes) {
		List<CompensacionLCE> compensacionLCE=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from CompensacionLCE where ano = '" + ano + "' and mes = '" + mes +"'");
	            compensacionLCE=(List<CompensacionLCE>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar compensacionLCE.", e);
	        }
		return compensacionLCE;
	}

   

	
}
