package pe.com.calidad.suministro.dao;

import java.util.List;
import java.util.Set;





import pe.com.calidad.suministro.entity.SuministroAfectado;//CampanaMedicion;
import pe.com.calidad.suministro.entity.CompensacionSuministro;//Medicion;
import pe.com.calidad.suministro.entity.Interrupcion;//PeriodoMedicion;
//import pe.com.calidad.suministro.entity.TipoParametro;

/**
  * @author Lucho
 */
public interface SuministroAfectadoDao {

    public void create(SuministroAfectado suministroAfectado);
    public SuministroAfectado ReadById(long sumAfectadoId);
    public void update(SuministroAfectado suministroAfectado);
    public void delete(long sumAfectadoId);
    public Interrupcion selectInterrupcion(long sumAfectadoId);
    public List<SuministroAfectado> getAllRows();
    public List<SuministroAfectado> getAllRows1();
    public List<SuministroAfectado> getAllRows1(long interrupcionId);
    public List<SuministroAfectado> getInterrupcionxSuministro(String numSumAfectado);
    public List<SuministroAfectado> getInterxSuministroAnoSem(String numSumAfectado,String ano,String semestre);
}
