/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.suministro.dao;

import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.springframework.jdbc.object.StoredProcedure;

import com.fasterxml.jackson.annotation.JsonSubTypes.Type;

import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.entity.SuministroAfectado;
import pe.com.calidad.suministro.entity.VAnexoIDetail;
import pe.com.calidad.suministro.entity.CausaInterrupcion;
import pe.com.calidad.suministro.entity.TipoInterrupcion;
import pe.com.calidad.suministro.entity.TipoNotificacion;
import pe.com.calidad.suministro.entity.ModalidadDeteccion;
import pe.com.indra.calidad.dao.HibernateUtil;
import pe.com.indra.calidad.entity.PeriodoMedicion;
import pe.com.indra.calidad.util.ConectaDb;

/**
 *
 * @author Luis
 */
public class InterrupcionDaoImpl implements InterrupcionDao {

	private static InterrupcionDaoImpl instance = null;

	public static InterrupcionDaoImpl getInstance() {
		if (instance == null) {
			instance = new InterrupcionDaoImpl();
		}

		return instance;
	}

	public InterrupcionDaoImpl() {
	}

	@Override
	public void create(Interrupcion interrupcion) {
		try {
			HibernateUtil.begin();
			HibernateUtil.getSession().save(interrupcion);
			HibernateUtil.commit();
			HibernateUtil.close();

		} catch (HibernateException e) {
			HibernateUtil.rollback();

			throw new HibernateException("No se puede crear interrupcion.", e);
		}
	}

	@Override
	public Interrupcion ReadById(long interrupcionId) {
		Interrupcion interrupcion = null;
		try {
			HibernateUtil.begin();

			interrupcion = (Interrupcion) HibernateUtil.getSession().load(Interrupcion.class, interrupcionId);

			Hibernate.initialize(interrupcion);
			Hibernate.initialize(interrupcion.getCausaInterrupcion());

			Hibernate.initialize(interrupcion.getTipoInterrupcion());
			Hibernate.initialize(interrupcion.getTipoNotificacion1());
			Hibernate.initialize(interrupcion.getModalidadDeteccion());
			// Hibernate.initialize(interrupcion.getSuministrosAfectado());
			Hibernate.initialize(interrupcion.getPeriodoMedicion());

			HibernateUtil.commit();
			HibernateUtil.close();

		} catch (HibernateException e) {
			HibernateUtil.rollback();
			throw new HibernateException("No se puede encontrar Interrupcion.", e);
		}
		return interrupcion;
	}

	@Override
	public void update(Interrupcion interrupcion) {
		try {
			HibernateUtil.begin();
			HibernateUtil.getSession().update(interrupcion);
			HibernateUtil.commit();
			HibernateUtil.close();

		} catch (HibernateException e) {
			HibernateUtil.rollback();
			throw new HibernateException("No se puede actualizar Interrupcion.", e);
		}
	}

	@Override
	public void delete(long interrupcionId) {

		Interrupcion interrupcion = null;
		interrupcion = ReadById(interrupcionId);

		if (interrupcion != null) {
			try {
				HibernateUtil.begin();
				HibernateUtil.getSession().delete(interrupcion);
				HibernateUtil.commit();
				HibernateUtil.close();
			} catch (HibernateException e) {
				HibernateUtil.rollback();
				throw new HibernateException("No se puede eliminar interrupcion.", e);
			}
		}
	}

	@Override
	public CausaInterrupcion selectCausaInterrupcion(long interrupcionId) {
		Interrupcion interrupcion = null;
		interrupcion = ReadById(interrupcionId);

		return interrupcion.getCausaInterrupcion();
	}

	@Override
	public PeriodoMedicion selectPeriodo(long interrupcionId) {
		Interrupcion interrupcion = null;
		interrupcion = ReadById(interrupcionId);

		return interrupcion.getPeriodoMedicion();
	}

	@Override
	public TipoInterrupcion selectTipoInterrupcion(long interrupcionId) {
		Interrupcion interrupcion = null;
		interrupcion = ReadById(interrupcionId);

		return interrupcion.getTipoInterrupcion();
	}

	@Override
	public TipoNotificacion selectTipoNotificacion(long interrupcionId) {
		Interrupcion interrupcion = null;
		interrupcion = ReadById(interrupcionId);

		return interrupcion.getTipoNotificacion1();
	}

	@Override
	public ModalidadDeteccion selectModalidadDeteccion(long interrupcionId) {
		Interrupcion interrupcion = null;
		interrupcion = ReadById(interrupcionId);

		return interrupcion.getModalidadDeteccion();
	}

	@Override
	public Set<SuministroAfectado> selectSuministroAfectado(long interrupcionId) {
		Interrupcion interrupcion = null;
		interrupcion = ReadById(interrupcionId);

		return interrupcion.getSuministrosAfectado();
	}

	@Override
	public List<Interrupcion> getAllRows() {
		List<Interrupcion> suministrosAfectado = null;
		try {
			HibernateUtil.begin();

			Query q = HibernateUtil.getSession().createQuery("from Interrupcion order by fecIniInterrupcion desc");

			suministrosAfectado = (List<Interrupcion>) q.list();

			HibernateUtil.commit();
			HibernateUtil.close();

		} catch (HibernateException e) {
			HibernateUtil.rollback();
			throw new HibernateException("No se puede encontrar Interrupcion.", e);
		}
		return suministrosAfectado;
	}

	@Override
	public List<Interrupcion> getAllRows1() {
		List<Interrupcion> suministrosAfectado = null;
		try {
			HibernateUtil.begin();

			Query q = HibernateUtil.getSession()
					.createQuery("from Interrupcion where estado =:estado order by fecIniInterrupcion desc");
			q.setString("estado", "1");
			suministrosAfectado = (List<Interrupcion>) q.list();

			HibernateUtil.commit();
			HibernateUtil.close();

		} catch (HibernateException e) {
			HibernateUtil.rollback();
			throw new HibernateException("No se puede encontrar Interrupcion.", e);
		}
		return suministrosAfectado;
	}

	@Override
	public List<Interrupcion> getAllRows1(long perMedicionId) {
		List<Interrupcion> suministrosAfectado = null;
		try {
			HibernateUtil.begin();

			Query q = HibernateUtil.getSession().createQuery(
					"from Interrupcion i where i.estado =:estado and i.periodoMedicion = :perMedicionId  order by fecIniInterrupcion desc");
			q.setString("estado", "1");
			q.setLong("perMedicionId", perMedicionId);

			suministrosAfectado = (List<Interrupcion>) q.list();

			for (Interrupcion inter : suministrosAfectado) {
				// Hibernate.initialize(inter.getSuministrosAfectado());
			}

			HibernateUtil.commit();
			HibernateUtil.close();

		} catch (HibernateException e) {
			HibernateUtil.rollback();
			throw new HibernateException("No se puede encontrar Interrupcion.", e);
		}
		return suministrosAfectado;
	}

	@Override
	public List<VAnexoIDetail> getAnexo01Prox074Detail(long perMedicionId) {
		List<VAnexoIDetail> result=new ArrayList<>();
		ConectaDb db = new ConectaDb("CALIDAD");		
        Connection cn = db.getConnection();
        Statement stmt = null;
		try {
			
			stmt = cn.createStatement();
			
			ResultSet rs = stmt
				    .executeQuery("SELECT * FROM V_ANEXO_I_DETAIL WHERE PK_PER_MEDICION_ID = " + perMedicionId);
			
			while(rs.next())
            {
				VAnexoIDetail dato = new VAnexoIDetail(rs.getString(1), rs.getString(2), rs.getString(3), rs.getLong(4), rs.getInt(5), rs.getString(6), rs.getInt(7), 
						rs.getString(8), rs.getTimestamp(9), rs.getTimestamp(10), rs.getTimestamp(11), rs.getLong(12), rs.getLong(13), rs.getBigDecimal(14), rs.getString(15), rs.getString(16), 
						rs.getString(17), rs.getString(18), rs.getString(19), rs.getString(20), rs.getString(21), rs.getLong(22), rs.getLong(23), rs.getBigDecimal(24), rs.getBigDecimal(25), 
						rs.getString(26), rs.getString(27), rs.getString(28), rs.getInt(29), rs.getLong(30), rs.getBigDecimal(31), rs.getBigDecimal(32), rs.getLong(33), rs.getBigDecimal(34), 
						rs.getBigDecimal(35), rs.getString(36), rs.getString(37), rs.getBigDecimal(38), rs.getString(39), rs.getBigDecimal(40), rs.getBigDecimal(41), rs.getBigDecimal(42), 
						rs.getString(43), rs.getString(44), rs.getLong(45), rs.getBigDecimal(46), rs.getBigDecimal(47));
                result.add(dato);
            }
			
		} catch (SQLException e) {
	        e.printStackTrace();
	    } finally {
            try {
            	stmt.close();
                cn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
		
		return result;
	}

	@Override
	public List<Interrupcion> getAllFueU(String ano, String semestre) {
		List<Interrupcion> suministrosAfectado = null;
		try {
			HibernateUtil.begin();

			Query q = HibernateUtil.getSession()
					.createQuery("from Interrupcion i where i.estado =:estado and i.periodoMedicion.ano = '" + ano
							+ "' and i.periodoMedicion.semestre = '" + semestre
							+ "' and solFueMayor = 'F' and zonAfectada in('U','P') order by fecIniInterrupcion desc");
			q.setString("estado", "1");

			suministrosAfectado = (List<Interrupcion>) q.list();

			for (Interrupcion inter : suministrosAfectado) {
				// Hibernate.initialize(inter.getSuministrosAfectado());
			}

			HibernateUtil.commit();
			HibernateUtil.close();

		} catch (HibernateException e) {
			HibernateUtil.rollback();
			throw new HibernateException("No se puede encontrar Interrupcion U.", e);
		}
		return suministrosAfectado;
	}

	@Override
	public List<Interrupcion> getAllFueR(String ano, String semestre) {
		List<Interrupcion> suministrosAfectado = null;
		try {
			HibernateUtil.begin();

			Query q = HibernateUtil.getSession()
					.createQuery("from Interrupcion i where i.estado =:estado and i.periodoMedicion.ano = '" + ano
							+ "' and i.periodoMedicion.semestre = '" + semestre
							+ "' and solFueMayor = 'F' and zonAfectada in('P','R') order by fecIniInterrupcion desc");
			q.setString("estado", "1");

			suministrosAfectado = (List<Interrupcion>) q.list();

			for (Interrupcion inter : suministrosAfectado) {
				// Hibernate.initialize(inter.getSuministrosAfectado());
			}

			HibernateUtil.commit();
			HibernateUtil.close();

		} catch (HibernateException e) {
			HibernateUtil.rollback();
			throw new HibernateException("No se puede encontrar Interrupcion R.", e);
		}
		return suministrosAfectado;
	}

}
