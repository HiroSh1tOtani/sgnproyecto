package pe.com.calidad.suministro.dao;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import java.util.List;



import pe.com.calidad.suministro.entity.ParametroAnual;//RelevadorRechazoCarga;
import pe.com.indra.calidad.dao.HibernateUtil;



/**
 *
 * @author Luis
 */
public class ParametroAnualDaoImpl implements ParametroAnualDao {

    private static ParametroAnualDaoImpl instance = null;

    public static ParametroAnualDaoImpl getInstance() {
        if (instance == null) {
            instance = new ParametroAnualDaoImpl();
        }

        return instance;
    }

    public ParametroAnualDaoImpl() {
    }

	@Override
	public void create(ParametroAnual parametroAnual) {

        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(parametroAnual);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear parametro Anual.", e);
        }
  	
		
	}

	@Override
	public ParametroAnual ReadById(long parAnualId) {
		ParametroAnual parametroAnual = null;
        try {
            HibernateUtil.begin();

            parametroAnual = (ParametroAnual) HibernateUtil.getSession().load(ParametroAnual.class, parAnualId);

            Hibernate.initialize(parametroAnual);
         
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Parametro Anual.", e);
        }
        return parametroAnual;
	}

	@Override
	public void update(ParametroAnual parametroAnual) {
	    try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(parametroAnual);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar parametro Anual.", e);
        }
	}

	@Override
	public void delete(long parAnualId) {
		ParametroAnual parametroAnual = null;
		parametroAnual = ReadById(parAnualId);

        if (parametroAnual != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(parametroAnual);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Parametro Anual.", e);
            }
        }
	}

	@Override
	public List<ParametroAnual> getAllRows() {
		List<ParametroAnual> parametroAnual=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from ParametroAnual");
	            
	            parametroAnual=(List<ParametroAnual>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Parametro Anual .", e);
	        }
		return parametroAnual;
	}


	@Override
	public List<ParametroAnual> getBusquedaxAno(String ano) {
		List<ParametroAnual> parametroAnual=null;
		 try {
	            HibernateUtil.begin();
	            
	            Query q = HibernateUtil.getSession().createQuery("from ParametroAnual where ano = '" + ano + "'" );
	            
	            parametroAnual = (List<ParametroAnual>) q.list();	     
	           
	            
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Parametro Anual.", e);
	        }
		return parametroAnual;
	}
	
}
