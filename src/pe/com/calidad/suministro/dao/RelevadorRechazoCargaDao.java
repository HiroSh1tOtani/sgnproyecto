
package pe.com.calidad.suministro.dao;

import java.util.List;

import pe.com.calidad.proc228.entity.Deficiencia;
import pe.com.calidad.suministro.entity.RelevadorRechazoCarga;

/**
 *
 * @author Lucho
 */
public interface RelevadorRechazoCargaDao {
    public void create(RelevadorRechazoCarga relevadorRechazoCarga);
    public RelevadorRechazoCarga ReadById(long relRechazoId);
	public void update(RelevadorRechazoCarga relevadorRechazoCarga);
	public void delete(long relRechazoId);
   	public List<RelevadorRechazoCarga> getAllRows();
	public List<RelevadorRechazoCarga> getAllRows1();
	public List<RelevadorRechazoCarga> getBusquedaxAnoSemestre(String ano,String semestre);
	
        
}
