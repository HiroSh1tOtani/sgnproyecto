/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.suministro.dao;

import java.util.Set;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.calidad.suministro.entity.CargoTarifa;
import pe.com.calidad.suministro.entity.ModalidadDeteccion;
import pe.com.calidad.suministro.entity.Pliego;//Indicador074;
import pe.com.indra.calidad.dao.HibernateUtil;
/**
 *
 * @author Luis
 */
public class PliegoDaoImpl implements PliegoDao {

    private static PliegoDaoImpl instance = null;

    public static PliegoDaoImpl getInstance() {
        if (instance == null) {
            instance = new PliegoDaoImpl();
        }

        return instance;
    }

    public PliegoDaoImpl() {
    }

	@Override
	public void create(Pliego pliego) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(pliego);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear pliego.", e);
        } 
    }

	@Override
	public Pliego ReadById(long pliegoId) {
		Pliego pliego = null;
        try {
            HibernateUtil.begin();
            
                    
            pliego = (Pliego) HibernateUtil.getSession().load(Pliego.class, pliegoId);
            
            Hibernate.initialize(pliego);
            Hibernate.initialize(pliego.getCargosTarifa()); 
        
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar pliego.", e);
        } 
        return pliego;
    }

	@Override
	public void update(Pliego pliego) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(pliego);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Pliego .", e);
        } 
    }

	@Override
	public void delete(long pliegoId) {
        
		Pliego pliego = null;
		pliego = ReadById(pliegoId);
        
        if(pliego != null){               
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(pliego);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Pliego.", e); 
            }
        }
    }

	@Override
	public Set<CargoTarifa> selectCargoTarifa(long pliegoId) {
    
		Pliego pliego = null;
		pliego = ReadById(pliegoId);
        
        return pliego.getCargosTarifa();
    }

	@Override
	public List<Pliego> getAllRows() {
		  List<Pliego> cargosPliegos=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from Pliego ");
	            cargosPliegos= (List<Pliego>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Pliego.", e);
	        } 
		return cargosPliegos;
	}

	@Override
	public List<Pliego> getAllRows1() {
		  List<Pliego> cargosPliegos=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from Pliego where estado =:estado ");
	            q.setString("estado","1");
	            cargosPliegos= (List<Pliego>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Pliego.", e);
	        } 
		return cargosPliegos;
	}

	
}
