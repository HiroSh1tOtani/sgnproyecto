package pe.com.calidad.suministro.dao;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import java.util.List;
import java.util.Set;









import pe.com.calidad.suministro.entity.CompensacionSuministro;
import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.entity.SuministroAfectado;
import pe.com.indra.calidad.dao.HibernateUtil;
import pe.com.indra.calidad.entity.CompensacionUnitaria;
import pe.com.indra.calidad.entity.IntervaloFRTension;
import pe.com.indra.calidad.entity.IntervaloTension;


/**
 *
 * @author Luis
 */
public class CompensacionSuministroDaoImpl implements CompensacionSuministroDao {

    private static CompensacionSuministroDaoImpl instance = null;

    public static CompensacionSuministroDaoImpl getInstance() {
        if (instance == null) {
            instance = new CompensacionSuministroDaoImpl();
        }

        return instance;
    }

    public CompensacionSuministroDaoImpl() {
    }

    @Override
    public void create(CompensacionSuministro compensacionSuministro) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(compensacionSuministro);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Compensacion Suministro.", e);
        }
    }

    @Override
    public CompensacionSuministro ReadById(long comSuministroId) {
    	CompensacionSuministro compensacionSuministro = null;
        try {
            HibernateUtil.begin();

            compensacionSuministro = (CompensacionSuministro) HibernateUtil.getSession().load(CompensacionSuministro.class, comSuministroId);

            Hibernate.initialize(compensacionSuministro);
            //Hibernate.initialize(compensacionSuministro.getSuministroAfectado());

            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Compensacion Suministro.", e);
        }
        return compensacionSuministro;
    }
    
    @Override
	public CompensacionSuministro ReadByCod(String codRelevador) {
    	CompensacionSuministro compensacionSuministro = null;
        try {
            HibernateUtil.begin();
            
            Query q = HibernateUtil.getSession().createQuery("from CompensacionSuministro where codRelevalor = :codRelevador and estado = '1'");
            q.setString("codRelevador",codRelevador);
            compensacionSuministro = (CompensacionSuministro)q.uniqueResult();
            
            Hibernate.initialize(compensacionSuministro);
            //Hibernate.initialize(compensacionSuministro.getSuministroAfectado()); 
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Compensacion Suministro.", e);
        } 
        return compensacionSuministro;   
	}

    @Override
    public void update(CompensacionSuministro compensacionSuministro) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(compensacionSuministro);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Compensacion Suministro.", e);
        }
    }

    @Override
    public void delete(long comSuministroId) {
    	CompensacionSuministro compensacionSuministro = null;
    	compensacionSuministro = ReadById(comSuministroId);

        if (compensacionSuministro != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(compensacionSuministro);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Compensacion Suministro.", e);
            }
        }
    }

        
	@Override
	public List<CompensacionSuministro> getAllRows() {
		List<CompensacionSuministro> compensacionSuministro=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from CompensacionSuministro");
	            
	            compensacionSuministro=(List<CompensacionSuministro>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();
 
	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Compensacion Suministro .", e);
	        }
		return compensacionSuministro;
	}
	
	@Override
	public List<CompensacionSuministro> getAllRows1() {
		List<CompensacionSuministro> compensacionSuministro=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from CompensacionSuministro where estado =:estado");
	            q.setString("estado","1");
	            compensacionSuministro=(List<CompensacionSuministro>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();
 
	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Compensacion Suministro .", e);
	        }
		return compensacionSuministro;
	}

	@Override
	public CompensacionSuministro getAllRows1(String numSumAfectado, String semestre) 
	{		CompensacionSuministro compensacionSuministro=null;
			 try {
		            HibernateUtil.begin();
		            Query q = HibernateUtil.getSession().createQuery("from CompensacionSuministro where estado =:estado and numSumAfectado = :numSumAfectado and semestre =:semestre ");
		            q.setString("numSumAfectado",numSumAfectado);
		            q.setString("semestre",semestre);
		            q.setString("estado", "1");
		            
		            if(q.list().size() > 0){
		             
		            	compensacionSuministro = (CompensacionSuministro) q.list().get(0);
		            
		            }
		            HibernateUtil.commit();
		            HibernateUtil.close();
		            
			         
		        } catch (HibernateException e) {
		            HibernateUtil.rollback();
		            throw new HibernateException("No se puede encontrar Compensacion Suministro.", e);
		        } 
			return compensacionSuministro;	

	    
		}

	@Override
	public List<CompensacionSuministro> getAllComp(String ano, String semestre) {
		List<CompensacionSuministro> compensacionSuministro = null;
		 try {
			 HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from CompensacionSuministro i where i.estado =:estado and i.ano = '" + ano + "' and i.semestre = '" + semestre + "' ");
	            q.setString("estado","1");
	            
	            compensacionSuministro =(List<CompensacionSuministro>) q.list();

	            /*
	            for(CompensacionSuministro inter:compensacionSuministro){
	            	//Hibernate.initialize(inter.getSuministrosAfectado());
	            }
	            */

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar compensacionSuministro.", e);
	        }
		 
		return compensacionSuministro;
	}

	
}
