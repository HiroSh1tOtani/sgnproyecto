package pe.com.calidad.suministro.dao;

/**
 *
 * @author Luis
 */
import java.util.List;


import pe.com.calidad.suministro.entity.Indicador074;
import pe.com.indra.calidad.entity.PeriodoMedicion;
import java.util.Set;

public interface Indicador074Dao {

    public void create(Indicador074 indicador074);
    public Indicador074 ReadById(long calIndicadorId);
    public void update(Indicador074 indicador074);
    public void delete(long calIndicadorId);

    public PeriodoMedicion selectPeriodo(long calIndicadorId);
  
    
    
    public List<Indicador074> getAllRows();
    public List<Indicador074> getAllRows1();
    public List<Indicador074> getAllRows1(long perMedicionId);
    
}
