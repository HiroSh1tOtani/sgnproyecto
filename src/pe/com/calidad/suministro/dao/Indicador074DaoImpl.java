/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.suministro.dao;

import java.util.Set;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.calidad.suministro.entity.Indicador074;
import pe.com.indra.calidad.entity.PeriodoMedicion;
import pe.com.indra.calidad.dao.HibernateUtil;
/**
 *
 * @author Luis
 */
public class Indicador074DaoImpl implements Indicador074Dao {

    private static Indicador074DaoImpl instance = null;

    public static Indicador074DaoImpl getInstance() {
        if (instance == null) {
            instance = new Indicador074DaoImpl();
        }

        return instance;
    }

    public Indicador074DaoImpl() {
    }

	@Override
	public void create(Indicador074 indicador074) {
		try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(indicador074);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
           
            throw new HibernateException("No se puede crear indicador074.", e);
        }
		
	}

	@Override
	public Indicador074 ReadById(long calIndicadorId) {

		Indicador074 indicador074 = null;
        try {
            HibernateUtil.begin();

            indicador074 = (Indicador074) HibernateUtil.getSession().load(Indicador074.class, calIndicadorId);

            Hibernate.initialize(indicador074);
            Hibernate.initialize(indicador074.getPeriodoMedicion());
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Indicador074.", e);
        }
        return indicador074;
	}

	@Override
	public void update(Indicador074 indicador074) {
		 try {
	            HibernateUtil.begin();
	            HibernateUtil.getSession().update(indicador074);
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede actualizar Indicador074.", e);
	        }
		
	}

	@Override
	public void delete(long calIndicadorId) {
		Indicador074 indicador074 = null;
		indicador074 = ReadById(calIndicadorId);

        if (indicador074 != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(indicador074);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar indicador074.", e);
            }
        }
		
	}

	@Override
	public PeriodoMedicion selectPeriodo(long calIndicadorId) {
		Indicador074 indicador074 = null;
		indicador074 = ReadById(calIndicadorId);

        return indicador074.getPeriodoMedicion();
		
	}

	@Override
	public List<Indicador074> getAllRows() {
		List<Indicador074> indicadores=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from Indicador074 ");
	            
	            indicadores=(List<Indicador074>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar indicador074.", e);
	        }
		return indicadores;
		
	}

	@Override
	public List<Indicador074> getAllRows1() {
		List<Indicador074> indicadores=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from Indicador074 where estado =:estado ");
	            q.setString("estado","1");
	            indicadores=(List<Indicador074>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar indicador074.", e);
	        }
		return indicadores;
	}

	@Override
	public List<Indicador074> getAllRows1(long perMedicionId) {
		List<Indicador074> indicadores=null;
		try {
            HibernateUtil.begin();
            
            Query q = HibernateUtil.getSession().createQuery("from Indicador074 i where i.estado =:estado and i.periodoMedicion = :perMedicionId");
            q.setString("estado","1");
            q.setLong("perMedicionId", perMedicionId);
            
            indicadores=(List<Indicador074>) q.list();
            
           
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar indicador074.", e);
        }
	return indicadores;
	}

}
