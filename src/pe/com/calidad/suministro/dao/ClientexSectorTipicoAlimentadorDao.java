package pe.com.calidad.suministro.dao;

/**
 *
 * @author Luis
 */
import java.util.List;

import pe.com.calidad.suministro.entity.ClientexAlimentador;
import pe.com.calidad.suministro.entity.ClientexSectorTipico;

public interface ClientexSectorTipicoAlimentadorDao {
    public List<ClientexSectorTipico> getClientexSectorTipico(long perMedicionId);
    public List<ClientexAlimentador> getClientexAlimentador(long perMedicionId);
}
