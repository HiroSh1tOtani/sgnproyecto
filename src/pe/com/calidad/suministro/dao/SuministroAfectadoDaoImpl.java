/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.suministro.dao;

import java.util.Set;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.calidad.suministro.entity.SuministroAfectado;
import pe.com.calidad.suministro.entity.CompensacionSuministro;
import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.indra.calidad.dao.HibernateUtil;
import pe.com.indra.calidad.entity.Medicion;

/**
 *
 * @author Luis
 */
public class SuministroAfectadoDaoImpl implements SuministroAfectadoDao {

    private static SuministroAfectadoDaoImpl instance = null;

    public static SuministroAfectadoDaoImpl getInstance() {
        if (instance == null) {
            instance = new SuministroAfectadoDaoImpl();
        }

        return instance;
    }

    public SuministroAfectadoDaoImpl() {
    }

    @Override
    public void create(SuministroAfectado suministroAfectado) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(suministroAfectado);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            
            throw new HibernateException("No se puede crear Suministro Afectado.", e);
        }
    }

    @Override
    public SuministroAfectado ReadById(long sumAfectadoId) {
    	SuministroAfectado suministroAfectado = null;
        try {
            HibernateUtil.begin();

            /*
             Query q = HibernateUtil.getSession().createQuery("from CAL_PARAMETRO_NORMA where PAR_NORMA_ID = :parNormaId");
             q.setLong("parNormaId",parNormaId);
             parametroNorma = (ParametroNorma)q.uniqueResult();*/

            suministroAfectado = (SuministroAfectado) HibernateUtil.getSession().load(SuministroAfectado.class, sumAfectadoId);
            

            Hibernate.initialize(suministroAfectado);
            //Hibernate.initialize(suministroAfectado.getTipoParametro());
            Hibernate.initialize(suministroAfectado.getInterrupcion());
           // Hibernate.initialize(suministroAfectado.getCompensacionSuministro());
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Suministro Afectado.", e);
        }
        return suministroAfectado;
    }

    @Override
    public void update(SuministroAfectado suministroAfectado) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(suministroAfectado);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Suministro Afectado.", e);
        }
    }

    @Override
    public void delete(long sumAfectadoId) {

    	SuministroAfectado suministroAfectado = null;
    	suministroAfectado = ReadById(sumAfectadoId);

        if (suministroAfectado != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(suministroAfectado);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Suministro Afectado.", e);
            }
        }
    }

    
    @Override
    public Interrupcion selectInterrupcion(long sumAfectadoId) {
    	SuministroAfectado suministroAfectado = null;
    	suministroAfectado = ReadById(sumAfectadoId);

        return suministroAfectado.getInterrupcion();
    }

    
	@Override
	public List<SuministroAfectado> getAllRows() {
		List<SuministroAfectado> suministroAfectado=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from SuministroAfectado");
	            
	            suministroAfectado=(List<SuministroAfectado>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Suministro Afectado.", e);
	        }
		return suministroAfectado;
	}

	
	@Override
	public List<SuministroAfectado> getAllRows1() {
		List<SuministroAfectado> suministroAfectado=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from SuministroAfectado where estado =:estado");
	            q.setString("estado","1");
	            suministroAfectado=(List<SuministroAfectado>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Suministro Afectado.", e);
	        }
		return suministroAfectado;
	}

	@Override
	public List<SuministroAfectado> getAllRows1(long interrupcionId) {
		List<SuministroAfectado> suministroAfectado=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from SuministroAfectado where estado =:estado and interrupcion.interrupcionId = :interrupcionId");
	            q.setString("estado","1");
	            q.setLong("interrupcionId",interrupcionId);
	            
	            suministroAfectado=(List<SuministroAfectado>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Suministro Afectado.", e);
	        }
		return suministroAfectado;
	}

	@Override
	public List<SuministroAfectado> getInterrupcionxSuministro(String numSumAfectado) {
		List<SuministroAfectado> suministroAfectado=null;
		 try {
	            HibernateUtil.begin();
	            
	           Query q = HibernateUtil.getSession().createQuery("from SuministroAfectado m where m.estado = '1' and m.numSumAfectado = '" + numSumAfectado.trim() + "' order by m.fecIniIntReal desc");
	            
	            
	            suministroAfectado=(List<SuministroAfectado>) q.list();	            
	            
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar muestras para el Suministro.", e);
	        }
		return suministroAfectado;
		
	}

	@Override
	public List<SuministroAfectado> getInterxSuministroAnoSem(String numSumAfectado,String ano, String semestre) {
		List<SuministroAfectado> suministroAfectado=null;
		try {
            HibernateUtil.begin();
            
           Query q = HibernateUtil.getSession().createQuery("from SuministroAfectado m where m.estado = '1' and m.numSumAfectado = '" + numSumAfectado.trim()  + "' and m.interrupcion.periodoMedicion.ano = '" + ano + "' and m.interrupcion.periodoMedicion.semestre = '" + semestre + "'  order by m.fecIniIntReal desc");
            
            
            suministroAfectado=(List<SuministroAfectado>) q.list();	            
            
            HibernateUtil.commit();
            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar muestras para el Suministro x a�o y semsstre.", e);
	        }
		return suministroAfectado;
		}
    
    
}
