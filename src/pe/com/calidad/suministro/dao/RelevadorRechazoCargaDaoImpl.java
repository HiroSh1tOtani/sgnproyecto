package pe.com.calidad.suministro.dao;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import java.util.List;

import pe.com.calidad.suministro.entity.CostoRacionamiento;
import pe.com.calidad.suministro.entity.RelevadorRechazoCarga;
import pe.com.indra.calidad.dao.HibernateUtil;
import pe.com.indra.calidad.entity.Medicion;


/**
 *
 * @author Luis
 */
public class RelevadorRechazoCargaDaoImpl implements RelevadorRechazoCargaDao {

    private static RelevadorRechazoCargaDaoImpl instance = null;

    public static RelevadorRechazoCargaDaoImpl getInstance() {
        if (instance == null) {
            instance = new RelevadorRechazoCargaDaoImpl();
        }

        return instance;
    }

    public RelevadorRechazoCargaDaoImpl() {
    }

	@Override
	public void create(RelevadorRechazoCarga relevadorRechazoCarga) {

        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(relevadorRechazoCarga);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Relevador RechazoCarga.", e);
        }
  	
	}

	@Override
	public RelevadorRechazoCarga ReadById(long relRechazoId) {
		RelevadorRechazoCarga relevadorRechazoCarga = null;
        try {
            HibernateUtil.begin();

            relevadorRechazoCarga = (RelevadorRechazoCarga) HibernateUtil.getSession().load(RelevadorRechazoCarga.class, relRechazoId);

            Hibernate.initialize(relevadorRechazoCarga);
         
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Relevador RechazoCarga.", e);
        }
        return relevadorRechazoCarga;
    }

	@Override
	public void update(RelevadorRechazoCarga relevadorRechazoCarga) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(relevadorRechazoCarga);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Relevador RechazoCarga.", e);
        }
    }

	@Override
	public void delete(long relRechazoId) {
		RelevadorRechazoCarga relevadorRechazoCarga = null;
		relevadorRechazoCarga = ReadById(relRechazoId);

        if (relevadorRechazoCarga != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(relevadorRechazoCarga);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Relevador RechazoCarga.", e);
            }
        }
    }

	@Override
	public List<RelevadorRechazoCarga> getAllRows() {
		List<RelevadorRechazoCarga> relevadorRechazoCarga=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from RelevadorRechazoCarga");
	            
	            relevadorRechazoCarga=(List<RelevadorRechazoCarga>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Relevador RechazoCarga .", e);
	        }
		return relevadorRechazoCarga;
	}

	@Override
	public List<RelevadorRechazoCarga> getAllRows1() {
		List<RelevadorRechazoCarga> relevadorRechazoCarga=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from RelevadorRechazoCarga where estado =:estado");
	            q.setString("estado","1");
	            relevadorRechazoCarga=(List<RelevadorRechazoCarga>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Relevador RechazoCarga .", e);
	        }
		return relevadorRechazoCarga;
	}

	@Override
	public List<RelevadorRechazoCarga> getBusquedaxAnoSemestre(String ano,String semestre) {
		List<RelevadorRechazoCarga> relevadorRechazoCarga=null;
		 try {
	            HibernateUtil.begin();
	            
	            Query q = HibernateUtil.getSession().createQuery("from RelevadorRechazoCarga where estado ='1' and ano = '" + ano + "' and semestre = '" + semestre +"'" );
	            
	 	        relevadorRechazoCarga = (List<RelevadorRechazoCarga>) q.list();	     
	           
	            
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar RelevadorRechazoCarga.", e);
	        }
		return relevadorRechazoCarga;
	}

	
}
