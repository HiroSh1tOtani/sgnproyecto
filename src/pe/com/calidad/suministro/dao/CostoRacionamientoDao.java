
package pe.com.calidad.suministro.dao;

import java.util.List;

import pe.com.calidad.suministro.entity.CostoRacionamiento;

/**
 *
 * @author Lucho
 */
public interface CostoRacionamientoDao {
    public void create(CostoRacionamiento costoRacionamiento);
    public CostoRacionamiento ReadById(long cosRacionamientoId);
	public void update(CostoRacionamiento costoRacionamiento);
	public void delete(long cosRacionamientoId);
   	public List<CostoRacionamiento> getAllRows();
	public List<CostoRacionamiento> getAllRows1();
	
        
}
