/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.suministro.dao;


import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.entity.TipoInterrupcion;
import pe.com.indra.calidad.dao.HibernateUtil;

import java.util.Set;

import org.hibernate.Hibernate;

/**
 *
 * @author Luis
 */
public class TipoInterrupcionDaoImpl implements TipoInterrupcionDao {

    private static TipoInterrupcionDaoImpl instance = null;

    public static TipoInterrupcionDaoImpl getInstance() {
        if (instance == null) {
            instance = new TipoInterrupcionDaoImpl();
        }

        return instance;
    }

    public TipoInterrupcionDaoImpl() {
    }

    @Override
    public void create(TipoInterrupcion tipoInterrupcion) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(tipoInterrupcion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Tipo de Interrupcion.", e);
        } 
    }

    @Override
    public TipoInterrupcion ReadById(long tipInterrupcionId) {
    	TipoInterrupcion tipoInterrupcion = null;
        try {
            HibernateUtil.begin();
            
            /*
            Query q = HibernateUtil.getSession().createQuery("from CAL_TIPO_INTERRUPCION where TIP_INTERRUPCION_ID = :tipInterrupcionId");
            q.setLong("tipParametroId",tipParametroId);
            tipoInterrupcion = (TipoInterrupcion)q.uniqueResult();*/
            
            tipoInterrupcion = (TipoInterrupcion) HibernateUtil.getSession().load(TipoInterrupcion.class, tipInterrupcionId);
            
            Hibernate.initialize(tipoInterrupcion);
            Hibernate.initialize(tipoInterrupcion.getInterrupciones()); 
        
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Tipo de Interrupcion.", e);
        } 
        return tipoInterrupcion;
    }

    @Override
    public TipoInterrupcion ReadByCod(String codTipInterrupcion) {
    	TipoInterrupcion tipoInterrupcion = null;
        try {
            HibernateUtil.begin();
            
            Query q = HibernateUtil.getSession().createQuery("from TipoInterrupcion where codTipInterrupcion = :codTipInterrupcion and estado = '1'");
            q.setString("codTipInterrupcion",codTipInterrupcion);
            tipoInterrupcion = (TipoInterrupcion)q.uniqueResult();
            
            Hibernate.initialize(tipoInterrupcion);
            Hibernate.initialize(tipoInterrupcion.getInterrupciones()); 
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Tipo de Interrupcion.", e);
        } 
        return tipoInterrupcion;
    }

    @Override
    public void update(TipoInterrupcion tipoInterrupcion) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(tipoInterrupcion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Tipo de Interrupcion.", e);
        } 
    }

    @Override
    public void delete(long tipParametroId) {
        
    	TipoInterrupcion tipoInterrupcion = null;
    	tipoInterrupcion = ReadById(tipParametroId);
        
        if(tipoInterrupcion != null){               
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(tipoInterrupcion);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Tipo de Interrupcion.", e); 
            }
        }
    }

    @Override
    public Set<Interrupcion> selectInterrupcion(long tipInterrupcionId) {
    /*
        String hql = "from CAL_TIPO_INTERRUPCION where PK_TIP_INTERRUPCION_ID = :tipInterrupcionId";
        Query query = HibernateUtil.getSession().createQuery(hql);
        query.setLong("tipInterrupcionId",tipInterrupcionId);
        Set<Interrupcion> results = (Set<Interrupcion>)query.list();
        
        return results;
    */
    	TipoInterrupcion tipoInterrupcion = null;
    	tipoInterrupcion = ReadById(tipInterrupcionId);
        
        return tipoInterrupcion.getInterrupciones();
    }
   
	@SuppressWarnings("unchecked")
	@Override
	public List<TipoInterrupcion> getAllRows() {
		  List<TipoInterrupcion> interrupciones=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from TipoInterrupcion order by codTipInterrupcion asc");
	            interrupciones= (List<TipoInterrupcion>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Tipo de Interrupcion.", e);
	        } 
		return interrupciones;
	}
	
	@Override
	public List<TipoInterrupcion> getAllRows1() {
		  List<TipoInterrupcion> interrupciones=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from TipoInterrupcion where estado =:estado order by codTipInterrupcion asc");
	            q.setString("estado","1");
	            interrupciones= (List<TipoInterrupcion>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Tipo de Interrupcion.", e);
	        } 
		return interrupciones;
	}
	
	
}
