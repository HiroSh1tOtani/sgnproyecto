
package pe.com.calidad.suministro.dao;

import java.util.List;

import pe.com.calidad.suministro.entity.CompensacionSuministro;//ParametroNorma;
import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.entity.SuministroAfectado;//TipoParametro;
import pe.com.indra.calidad.entity.IntervaloFRTension;

/**
 *
 * @author Lucho
 */
public interface CompensacionSuministroDao {
    public void create(CompensacionSuministro compensacionSuministro);
    public CompensacionSuministro ReadById(long codSuministroId);
	public CompensacionSuministro ReadByCod(String codRelevador);
	public void update(CompensacionSuministro compensacionSuministro);
	public void delete(long codSuministroId);
   	public List<CompensacionSuministro> getAllRows();
	public List<CompensacionSuministro> getAllRows1();
	public CompensacionSuministro getAllRows1(String numSumAfectado,String semestre);
        
	public List<CompensacionSuministro> getAllComp(String ano, String semestre);
}
