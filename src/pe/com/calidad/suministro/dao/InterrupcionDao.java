package pe.com.calidad.suministro.dao;

/**
 *
 * @author Luis
 */
import java.util.List;

import pe.com.calidad.suministro.entity.SuministroAfectado;
import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.entity.VAnexoIDetail;
import pe.com.calidad.suministro.entity.CausaInterrupcion;
import pe.com.calidad.suministro.entity.TipoInterrupcion;
import pe.com.calidad.suministro.entity.ModalidadDeteccion;
import pe.com.calidad.suministro.entity.TipoNotificacion;




import pe.com.indra.calidad.entity.Medicion;
import pe.com.indra.calidad.entity.PeriodoMedicion;

import java.util.Set;

public interface InterrupcionDao {

    public void create(Interrupcion interrupcion);
    public Interrupcion ReadById(long interrupcionId);
    public void update(Interrupcion interrupcion);
    public void delete(long interrupcionId);
    
    public CausaInterrupcion selectCausaInterrupcion(long interrupcionId);
    public TipoInterrupcion selectTipoInterrupcion(long interrupcionId);
    public TipoNotificacion selectTipoNotificacion(long interrupcionId);
    public ModalidadDeteccion selectModalidadDeteccion(long interrupcionId);
    public PeriodoMedicion selectPeriodo(long interrupcionId);
  
    
    public Set<SuministroAfectado> selectSuministroAfectado(long interrupcionId);
    public List<Interrupcion> getAllRows();
    public List<Interrupcion> getAllRows1();
    public List<Interrupcion> getAllRows1(long perMedicionId);
    public List<Interrupcion> getAllFueU(String ano,String semestre);
    public List<Interrupcion> getAllFueR(String ano,String semestre);
	public List<VAnexoIDetail> getAnexo01Prox074Detail(long perMedicionId);
    
}
