/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.suministro.dao;


import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.entity.ModalidadDeteccion;//CausaInterrupcion;
import pe.com.indra.calidad.dao.HibernateUtil;

import java.util.Set;

import org.hibernate.Hibernate;

/**
 *
 * @author Luis
 */
public class ModalidadDeteccionDaoImpl implements ModalidadDeteccionDao {

    private static ModalidadDeteccionDaoImpl instance = null;

    public static ModalidadDeteccionDaoImpl getInstance() {
        if (instance == null) {
            instance = new ModalidadDeteccionDaoImpl();
        }

        return instance;
    }

    public ModalidadDeteccionDaoImpl() {
    }

    @Override
    public void create(ModalidadDeteccion modalidadDeteccion) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(modalidadDeteccion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede crear Modalidad de Deteccion.", e);
        } 
    }

    @Override
    public ModalidadDeteccion ReadById(long modDeteccionId) {
    	ModalidadDeteccion modalidadDeteccion = null;
        try {
            HibernateUtil.begin();
            
            /*
            Query q = HibernateUtil.getSession().createQuery("from CAL_MODALIDAD_DETECCION where MOD_DETECCION_ID = :modDeteccionId");
            q.setLong("modDeteccionId",modDeteccionId);
            modalidadDeteccion = (ModalidadDeteccion)q.uniqueResult();*/
            
            modalidadDeteccion = (ModalidadDeteccion) HibernateUtil.getSession().load(ModalidadDeteccion.class, modDeteccionId);
            
            Hibernate.initialize(modalidadDeteccion);
            Hibernate.initialize(modalidadDeteccion.getInterrupciones()); 
        
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Modalidad de Deteccion.", e);
        } 
        return modalidadDeteccion;
    }

    @Override
    public ModalidadDeteccion ReadByCod(String codModDeteccion) {
    	ModalidadDeteccion modalidadDeteccion = null;
        try {
            HibernateUtil.begin();
            
            Query q = HibernateUtil.getSession().createQuery("from ModalidadDeteccion where codModDeteccion = :codModDeteccion and estado = '1'");
            q.setString("codModDeteccion",codModDeteccion);
            modalidadDeteccion = (ModalidadDeteccion)q.uniqueResult();
            
            Hibernate.initialize(modalidadDeteccion);
            Hibernate.initialize(modalidadDeteccion.getInterrupciones()); 
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Modalidad de Deteccion.", e);
        } 
        return modalidadDeteccion;
    }

    @Override
    public void update(ModalidadDeteccion modalidadDeteccion) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(modalidadDeteccion);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Modalidad de Deteccion.", e);
        } 
    }

    @Override
    public void delete(long modDeteccionId) {
        
    	ModalidadDeteccion modalidadDeteccion = null;
    	modalidadDeteccion = ReadById(modDeteccionId);
        
        if(modalidadDeteccion != null){               
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(modalidadDeteccion);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar Modalidad de Deteccion.", e); 
            }
        }
    }

    @Override
    public Set<Interrupcion> selectInterrupcion(long modDeteccionId) {
    
    	ModalidadDeteccion modalidadDeteccion = null;
    	modalidadDeteccion = ReadById(modDeteccionId);
        
        return modalidadDeteccion.getInterrupciones();
    }
   
	@SuppressWarnings("unchecked")
	@Override
	public List<ModalidadDeteccion> getAllRows() {
		  List<ModalidadDeteccion> interrupciones=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from ModalidadDeteccion order by modDeteccionId asc");
	            interrupciones= (List<ModalidadDeteccion>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Modalidad de Deteccion.", e);
	        } 
		return interrupciones;
	}
	
	@Override
	public List<ModalidadDeteccion> getAllRows1() {
		  List<ModalidadDeteccion> interrupciones=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from ModalidadDeteccion where estado =:estado order by modDeteccionId asc");
	            q.setString("estado","1");
	            interrupciones= (List<ModalidadDeteccion>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Modalidad de Deteccion.", e);
	        } 
		return interrupciones;
	}
	
	
}
