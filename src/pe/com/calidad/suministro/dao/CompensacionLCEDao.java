
package pe.com.calidad.suministro.dao;

import java.util.List;

import pe.com.calidad.suministro.entity.CompensacionLCE;

/**
 *
 * @author Lucho
 */
public interface CompensacionLCEDao {
   	public List<CompensacionLCE> getAllRows();
	public List<CompensacionLCE> getAllRows1(String ano,String mes);
        
}
