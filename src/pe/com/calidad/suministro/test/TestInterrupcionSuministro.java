package pe.com.calidad.suministro.test;


import pe.com.calidad.suministro.dao.SuministroAfectadoDaoImpl;//CausaInterrupcionDaoImpl;
import pe.com.calidad.suministro.dao.SuministroAfectadoDao;//CausaInterrupcionDao;
import pe.com.calidad.suministro.entity.SuministroAfectado;//CausaInterrupcion;


public class TestInterrupcionSuministro {
	
	public static void main(String[] args) {
		System.out.println("Iniciando el programa");
		SuministroAfectadoDao dao = new SuministroAfectadoDaoImpl();
		for(SuministroAfectado p: dao.getInterrupcionxSuministro("101000101")){
		 System.out.println(p.getNumSumAfectado()+" "+p.getInterrupcion());
		}
	}

}
