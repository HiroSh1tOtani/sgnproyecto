package pe.com.calidad.suministro.test;


import java.math.BigDecimal;

import pe.com.calidad.suministro.dao.Indicador074DaoImpl;//InterrupcionDaoImpl;
import pe.com.calidad.suministro.dao.Indicador074Dao;//InterrupcionDao;
import pe.com.calidad.suministro.entity.Indicador074;//Interrupcion;
import pe.com.indra.calidad.entity.PeriodoMedicion;


public class TestIndicador074 {
	
	public static void main(String[] args) {
		System.out.println("Iniciando el programa");
		Indicador074Dao dao = new Indicador074DaoImpl();
		
	/*	//INSERT
		Indicador074 indicador074 = new Indicador074();
		indicador074.setCalIndicadorId(1);
		
		indicador074.setSisElectrico("SE0044");
		indicador074.setNroCliSisElectrico(50);
		indicador074.setSaifiSisElectrico(new BigDecimal(100));
		indicador074.setSaifiIntProgramadas(new BigDecimal(10));
		indicador074.setSaifiIntNoProgramadas(new BigDecimal(11));
		indicador074.setSaifiRecCarga(new BigDecimal(12));
		indicador074.setSaifiInsDistribucion(new BigDecimal(13));
		indicador074.setSaifiInsTransmision(new BigDecimal(14));
		indicador074.setSaifiInsGeneracion(new BigDecimal(15));
		indicador074.setSaifiCauPropias(new BigDecimal(16));
		indicador074.setSaifiCauTerceros(new BigDecimal(17));
		indicador074.setSaifiCauOtras(new BigDecimal(18));
		indicador074.setSaifiCauFenomenos(new BigDecimal(19));
		indicador074.setSaifiFueMayor(new BigDecimal(20));
		indicador074.setSaidiSisElectrico(new BigDecimal(21));
		indicador074.setSaidiIntProgramadas(new BigDecimal(22));
		indicador074.setSaidiIntNoProgramadas(new BigDecimal(23));
		indicador074.setSaidiRecCarga(new BigDecimal(24));
		indicador074.setSaidiInsDistribucion(new BigDecimal(25));
		indicador074.setSaidiInsTransmision(new BigDecimal(26));
		indicador074.setSaidiInsGeneracion(new BigDecimal(27));
		indicador074.setSaidiCauPropias(new BigDecimal(28));
		indicador074.setSaidiCauTerceros(new BigDecimal(29));
		indicador074.setSaidiCauOtras(new BigDecimal(30));
		indicador074.setSaidiCauFenomenos(new BigDecimal(31));
		indicador074.setSaidiFueMayor(new BigDecimal(31));
		indicador074.setPeriodoMedicion(null);
		indicador074.setEstado("1");
		dao.create(indicador074);
		*/
	
		
		//LEER
		for(Indicador074 p: dao.getAllRows()){
		 System.out.println(p.getCalIndicadorId()+" "+p.getSaidiCauPropias() + " " + p.getPeriodoMedicion().getAno() );
		}	
			
	}

}
