package pe.com.calidad.suministro.test;


import java.math.BigDecimal;

import pe.com.calidad.suministro.dao.RelevadorRechazoCargaDaoImpl;
import pe.com.calidad.suministro.dao.RelevadorRechazoCargaDao;
import pe.com.calidad.suministro.entity.RelevadorRechazoCarga;


public class TestRelevadorRechazoCarga {
	
	public static void main(String[] args) {
		System.out.println("Iniciando el programa");
		RelevadorRechazoCargaDao dao = new RelevadorRechazoCargaDaoImpl();
		//INSERT
		/*	RelevadorRechazoCarga relevadorRechazoCarga = new RelevadorRechazoCarga();
		relevadorRechazoCarga.setRelRechazoId(1);
		relevadorRechazoCarga.setAno("2014");
		relevadorRechazoCarga.setSemestre("S1");
		relevadorRechazoCarga.setCodRelevador("0001");
		relevadorRechazoCarga.setCodAlimentador("00001");
		relevadorRechazoCarga.setMontoRepartir(new BigDecimal(100));
		relevadorRechazoCarga.setEneNoSuministrada(new BigDecimal(1000));
		relevadorRechazoCarga.setNroIntRecCarga(new BigDecimal(10));
		relevadorRechazoCarga.setNroHorIntRecCarga(new BigDecimal(11));
		relevadorRechazoCarga.setEstado("1");
		dao.create(relevadorRechazoCarga);
		*/
		//LEER
		
		for(RelevadorRechazoCarga p: dao.getBusquedaxAnoSemestre("2014", "S1")){
		 System.out.println(p.getRelRechazoId()+" "+p.getCodRelevador()+" "+p.getAno());
		}
		
	}

}
