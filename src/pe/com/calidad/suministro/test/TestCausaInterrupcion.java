package pe.com.calidad.suministro.test;


import pe.com.calidad.suministro.dao.CausaInterrupcionDaoImpl;
import pe.com.calidad.suministro.dao.CausaInterrupcionDao;
import pe.com.calidad.suministro.entity.CausaInterrupcion;


public class TestCausaInterrupcion {
	
	public static void main(String[] args) {
		System.out.println("Iniciando el programa");
		CausaInterrupcionDao dao = new CausaInterrupcionDaoImpl();
		/*for(CausaInterrupcion p: dao.getAllRows()){
		 System.out.println(p.getCauInterrupcionId()+" "+p.getDescripcion());
		}
		*/
		CausaInterrupcion causaInterrupcion = new CausaInterrupcion();
		causaInterrupcion.setCauInterrupcionId(8);
		causaInterrupcion.setCodCauInterrupcion("O");
		causaInterrupcion.setDescripcion("OTRAS CAUSALES");
		causaInterrupcion.setEstado("1");
		dao.create(causaInterrupcion);
		
	}

}
