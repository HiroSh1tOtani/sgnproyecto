package pe.com.calidad.suministro.test;


import pe.com.calidad.suministro.dao.TipoNotificacionDaoImpl;
import pe.com.calidad.suministro.dao.TipoNotificacionDao;
import pe.com.calidad.suministro.entity.TipoNotificacion;


public class TestTipoNotificacion {
	
	public static void main(String[] args) {
		System.out.println("Iniciando el programa");
		TipoNotificacionDao dao = new TipoNotificacionDaoImpl();
		//INSERT
		/*		TipoNotificacion tipoNotificacion = new TipoNotificacion();
		tipoNotificacion.setTipNotificacionId(1);
		tipoNotificacion.setCodTipNotificacion("O");
		tipoNotificacion.setDescripcion("POR OTROS MEDIOS");
		tipoNotificacion.setEstado("1");
		dao.create(tipoNotificacion);
		*/
		
		//LEER
		for(TipoNotificacion p: dao.getAllRows()){
		 System.out.println(p.getTipNotificacionId()+" "+p.getCodTipNotificacion()+" "+p.getDescripcion());
		}
		
	}

}
