package pe.com.calidad.suministro.test;


import pe.com.calidad.suministro.dao.ModalidadDeteccionDaoImpl;
import pe.com.calidad.suministro.dao.ModalidadDeteccionDao;
import pe.com.calidad.suministro.entity.ModalidadDeteccion;


public class TestModalidadDeteccion {
	
	public static void main(String[] args) {
		System.out.println("Iniciando el programa");
		ModalidadDeteccionDao dao = new ModalidadDeteccionDaoImpl();
		//INSERT
		/*	ModalidadDeteccion modalidadDeteccion = new ModalidadDeteccion();
		modalidadDeteccion.setModDeteccionId(1);
		modalidadDeteccion.setCodModDeteccion("P");
		modalidadDeteccion.setDescripcion("CUANDO LA INTERRUPCION ES PROGRAMADA");
		modalidadDeteccion.setEstado("1");
		dao.create(modalidadDeteccion);
		*/
		
		//LEER
		for(ModalidadDeteccion p: dao.getAllRows()){
		 System.out.println(p.getModDeteccionId()+" "+p.getCodModDeteccion()+" "+p.getDescripcion());
		}
		
	}

}
