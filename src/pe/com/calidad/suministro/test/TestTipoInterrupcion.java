package pe.com.calidad.suministro.test;


import pe.com.calidad.suministro.dao.TipoInterrupcionDaoImpl;
import pe.com.calidad.suministro.dao.TipoInterrupcionDao;
import pe.com.calidad.suministro.entity.TipoInterrupcion;


public class TestTipoInterrupcion {
	
	public static void main(String[] args) {
		System.out.println("Iniciando el programa");
		TipoInterrupcionDao dao = new TipoInterrupcionDaoImpl();
		//INSERT
		/*	TipoInterrupcion tipoInterrupcion = new TipoInterrupcion();
		tipoInterrupcion.setTipInterrupcionId(2);
		tipoInterrupcion.setCodTipInterrupcion("R");
		tipoInterrupcion.setDescripcion("RECHAZO DE CARGA");
		tipoInterrupcion.setEstado("1");
		dao.create(tipoInterrupcion);
		*/
		//LEER
		for(TipoInterrupcion p: dao.getAllRows()){
		 System.out.println(p.getTipInterrupcionId()+" "+p.getCodTipInterrupcion()+" "+p.getDescripcion());
		}
		
	}

}
