package pe.com.calidad.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import pe.com.calidad.entity.FiltroProceso;
import pe.com.calidad.entity.Proceso;

@Component
public class UtilDAOImpl implements  UtilDAO {

	private JdbcTemplate jdbcTemplate;
	
	public UtilDAOImpl(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }
	
	/*
	public Proceso obtenerProceso(FiltroProceso filtro) {
	    String sql = "SELECT * FROM contact WHERE contact_id=" + contactId;
	    return jdbcTemplate.query(sql, new ResultSetExtractor<Contact>() {
	 
	        @Override
	        public Contact extractData(ResultSet rs) throws SQLException,
	                DataAccessException {
	            if (rs.next()) {
	                Contact contact = new Contact();
	                contact.setId(rs.getInt("contact_id"));
	                contact.setName(rs.getString("name"));
	                contact.setEmail(rs.getString("email"));
	                contact.setAddress(rs.getString("address"));
	                contact.setTelephone(rs.getString("telephone"));
	                return contact;
	            }
	 
	            return null;
	        }
	 
	    });
	}*/
	
	private RowMapper<Proceso> getRowMapper(){
		return new RowMapper<Proceso>() {
			@Override
			public Proceso mapRow(ResultSet rs, int rowNum) throws SQLException {
				Proceso item = new Proceso();
				item.setIdCalProceso(rs.getInt("ID_CAL_PROCESO"));
				item.setPaso(rs.getInt("PASO"));
				item.setSubPaso(rs.getInt("SUB_PASO"));
				item.setIdTipoProceso(rs.getInt("TIPO_PROCESO"));
				item.setFechaHoraInicio(rs.getTimestamp("FECHA_HORA_INICIO"));
				item.setFechaHoraFin(rs.getTimestamp("FECHA_HORA_FIN"));
				item.setFechaHoraInicioPaso(rs.getTimestamp("FECHA_HORA_INICIO_PASO"));
				item.setDesPaso(rs.getString("DES_PASO"));
				item.setDesSubPaso(rs.getString("DES_SUB_PASO"));
				item.setEstado(rs.getString("ESTADO"));
				item.setTipoProceso(rs.getString("DESCRIPCION"));
				item.setTotalPasos(rs.getInt("TOTAL_PASOS"));
				item.setFechaHoraInicioSegundos(rs.getLong("FECHA_INICIO_SEG"));
				item.setFechaHoraInicioPasoSegundos(rs.getLong("FECHA_INICIO_PASO_SEG"));
				return item;
			}
	    };
	}
	
	@Override
	public List<Proceso> buscarProceso(FiltroProceso filtro) {
		String sql = "SELECT A.*,B.*,(SYSDATE - FECHA_HORA_INICIO)*60*60*24 AS FECHA_INICIO_SEG,(SYSDATE - FECHA_HORA_INICIO_PASO)*60*60*24 AS FECHA_INICIO_PASO_SEG FROM CAL_PROCESO A,CAL_TIPO_PROCESO B WHERE A.TIPO_PROCESO = B.ID_CAL_TIPO_PROCESO AND A.ESTADO='1'";
	    List<Proceso> lst = jdbcTemplate.query(sql, getRowMapper());
	    return lst;
	}
	
	@Override
	public List<Proceso> buscarUltimo5Proceso(FiltroProceso filtro) {
		String sql = "SELECT T.*,0 AS FECHA_INICIO_SEG,0 AS FECHA_INICIO_PASO_SEG FROM (SELECT * FROM CAL_PROCESO A,CAL_TIPO_PROCESO B WHERE A.TIPO_PROCESO = B.ID_CAL_TIPO_PROCESO AND A.ESTADO='2' ORDER BY FECHA_HORA_INICIO DESC) T WHERE ROWNUM<6";
	    List<Proceso> lst = jdbcTemplate.query(sql, getRowMapper());
	    return lst;
	}


}
