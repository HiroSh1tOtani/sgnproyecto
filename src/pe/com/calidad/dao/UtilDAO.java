package pe.com.calidad.dao;

import java.util.List;

import pe.com.calidad.entity.FiltroProceso;
import pe.com.calidad.entity.Proceso;

public interface UtilDAO {
	public List<Proceso> buscarProceso(FiltroProceso filtro);
	public List<Proceso> buscarUltimo5Proceso(FiltroProceso filtro);
}
