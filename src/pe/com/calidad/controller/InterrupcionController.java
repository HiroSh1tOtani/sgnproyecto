package pe.com.calidad.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.calidad.dao.UtilDAO;
import pe.com.calidad.entity.FiltroProceso;
import pe.com.calidad.entity.Proceso;

@RestController
@RequestMapping("/Interrupcion")
public class InterrupcionController {
	
	 @Autowired
	 private UtilDAO utilDAO;
	
	@PostMapping(value = "/listarInterrupcionTareasEnEjecucion")
	public ResponseEntity<List<Proceso>> listarInterrupcionTareasEnEjecucion(@RequestBody InterrupcionFiltro par) 
	{
		return new ResponseEntity<List<Proceso>>(utilDAO.buscarProceso(new FiltroProceso()), HttpStatus.OK);
	}
	
	@PostMapping(value = "/listarUltimo5Proceso")
	public ResponseEntity<List<Proceso>> listarUltimo5Proceso(@RequestBody InterrupcionFiltro par) 
	{
		return new ResponseEntity<List<Proceso>>(utilDAO.buscarUltimo5Proceso(new FiltroProceso()), HttpStatus.OK);
	}
	
}
