package pe.com.calidad.controller;

public class InterrupcionFiltro {
	private String anio;
	private String mes;
	
	public String getAnio() {
		return anio;
	}
	public void setAnio(String anio) {
		this.anio = anio;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	
	
}
