package pe.com.calidad.proc078.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;
import pe.com.calidad.proc078.dao.HistoricoDeficienciaApDao;//DeficienciaDao;
import pe.com.calidad.proc078.dao.HistoricoDeficienciaApDaoImpl;//DeficienciaDaoImpl;
import pe.com.calidad.proc078.entity.HistoricoDeficienciaAp;//Deficiencia;
import pe.com.indra.calidad.entity.Localidad;//EstadoSubsanacion;
import pe.com.indra.calidad.entity.PeriodoMedicion;

/**
 *
 * @author Luis
 */
public class HistoricoDeficienciaApService {

    public void create(HistoricoDeficienciaAp historicoDeficienciaAp) {
    	HistoricoDeficienciaApDao dao = new HistoricoDeficienciaApDaoImpl();
        
        try {
        	dao.create(historicoDeficienciaAp);
		} catch (HibernateException e) {
			throw new HibernateException("No se puede crear Deficiencia.", e);
		}
        
    }

    public HistoricoDeficienciaAp ReadById(long hisDeficienciaId) {
    	HistoricoDeficienciaApDao dao = new HistoricoDeficienciaApDaoImpl();
    	HistoricoDeficienciaAp historicoDeficienciaAp = null;
        
        try {
        	historicoDeficienciaAp = dao.ReadById(hisDeficienciaId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e); 
		}
        
        return historicoDeficienciaAp;
    }

        
    public void update(HistoricoDeficienciaAp historicoDeficienciaAp) {
    	HistoricoDeficienciaApDao dao = new HistoricoDeficienciaApDaoImpl();
        
        try {
        	dao.update(historicoDeficienciaAp);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public void delete(long hisDeficienciaId) {
    	HistoricoDeficienciaApDao dao = new HistoricoDeficienciaApDaoImpl();
        
        try {
        	dao.delete(hisDeficienciaId);
		} catch (HibernateException e) {
			throw new HibernateException(e.getMessage(), e);
		}
        
    }

    public PeriodoMedicion selectPeriodo(long hisDeficienciaId) {
    	HistoricoDeficienciaApDao dao = new HistoricoDeficienciaApDaoImpl();
        return dao.selectPeriodo(hisDeficienciaId);
    }
    

    public Localidad selectLocalidad(long deficienciaId) {
    	HistoricoDeficienciaApDao dao = new HistoricoDeficienciaApDaoImpl();
        return dao.selectLocalidad(deficienciaId);
    }

        
    public List<HistoricoDeficienciaAp> getAllRows(){
    	HistoricoDeficienciaApDao dao = new HistoricoDeficienciaApDaoImpl();
    	return dao.getAllRows1();
    } 
    
    public List<HistoricoDeficienciaAp> getAllRows1(){
    	HistoricoDeficienciaApDao dao = new HistoricoDeficienciaApDaoImpl();
    	return dao.getAllRows1();
    } 
    
    public List<HistoricoDeficienciaAp> getAllRows1(long perMedicionId){
    	 HistoricoDeficienciaApDao dao = new HistoricoDeficienciaApDaoImpl();	
    	 return dao.getAllRows1(perMedicionId);
    }
   
  
    
}
