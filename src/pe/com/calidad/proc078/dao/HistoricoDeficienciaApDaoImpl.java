/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.calidad.proc078.dao;

import java.util.Set;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import pe.com.calidad.proc078.entity.HistoricoDeficienciaAp;
import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.indra.calidad.dao.HibernateUtil;
import pe.com.indra.calidad.entity.Localidad;
import pe.com.indra.calidad.entity.PeriodoMedicion;


/**
 *
 * @author Luis
 */
public class HistoricoDeficienciaApDaoImpl implements HistoricoDeficienciaApDao {

    private static HistoricoDeficienciaApDaoImpl instance = null;

    public static HistoricoDeficienciaApDaoImpl getInstance() {
        if (instance == null) {
            instance = new HistoricoDeficienciaApDaoImpl();
        }

        return instance;
    }

    public HistoricoDeficienciaApDaoImpl() {
    }

	@Override
	public void create(HistoricoDeficienciaAp historicoDeficienciaAp) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().save(historicoDeficienciaAp);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
           
            throw new HibernateException("No se puede crear historico DeficienciaAp.", e);
        }
    }

	@Override
	public HistoricoDeficienciaAp ReadById(long hisDeficienciaId) {
		HistoricoDeficienciaAp historicoDeficienciaAp = null;
        try {
            HibernateUtil.begin();

            historicoDeficienciaAp = (HistoricoDeficienciaAp) HibernateUtil.getSession().load(HistoricoDeficienciaAp.class, hisDeficienciaId);

            Hibernate.initialize(historicoDeficienciaAp);
            Hibernate.initialize(historicoDeficienciaAp.getLocalidad());
            Hibernate.initialize(historicoDeficienciaAp.getPeriodoMedicion());
            
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede encontrar Historico DeficienciaAp.", e);
        }
        return historicoDeficienciaAp;
    }

	@Override
	public void update(HistoricoDeficienciaAp historicoDeficienciaAp) {
        try {
            HibernateUtil.begin();
            HibernateUtil.getSession().update(historicoDeficienciaAp);
            HibernateUtil.commit();
            HibernateUtil.close();

        } catch (HibernateException e) {
            HibernateUtil.rollback();
            throw new HibernateException("No se puede actualizar Historico DeficienciaAP.", e);
        }
    }

	@Override
	public void delete(long hisDeficienciaId) {

		HistoricoDeficienciaAp historicoDeficienciaAp = null;
		historicoDeficienciaAp = ReadById(hisDeficienciaId);

        if (historicoDeficienciaAp != null) {
            try {
                HibernateUtil.begin();
                HibernateUtil.getSession().delete(historicoDeficienciaAp);
                HibernateUtil.commit();
                HibernateUtil.close();
            } catch (HibernateException e) {
                HibernateUtil.rollback();
                throw new HibernateException("No se puede eliminar HistoricoDe ficienciaAp.", e);
            }
        }
    }

	@Override
	public Localidad selectLocalidad(long hisDeficienciaId) {
		HistoricoDeficienciaAp historicoDeficienciaAp = null;
		historicoDeficienciaAp = ReadById(hisDeficienciaId);
		return historicoDeficienciaAp.getLocalidad();
	}

	
	public PeriodoMedicion selectPeriodo(long hisDeficienciaId){
		HistoricoDeficienciaAp historicoDeficienciaAp = null;
		historicoDeficienciaAp = ReadById(hisDeficienciaId);

        return historicoDeficienciaAp.getPeriodoMedicion();
    }

	@Override
	public List<HistoricoDeficienciaAp> getAllRows1() {
		List<HistoricoDeficienciaAp> historicoDeficienciaAp=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from HistoricoDeficienciaAp where estado =:estado");
	            q.setString("estado","1");
	            historicoDeficienciaAp=(List<HistoricoDeficienciaAp>) q.list();

	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Historico DeficienciaAP.", e);
	        }
		return historicoDeficienciaAp;
	}

	@Override
	public List<HistoricoDeficienciaAp> getAllRows() {
		  List<HistoricoDeficienciaAp> historicoDeficienciaAp=null;
		 try {
	            HibernateUtil.begin();
	            Query q = HibernateUtil.getSession().createQuery("from HistoricoDeficienciaAp ");
	            historicoDeficienciaAp= (List<HistoricoDeficienciaAp>) q.list();
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Historico DeficienciaAP.", e);
	        } 
		return historicoDeficienciaAp;
	}

	@Override
	public List<HistoricoDeficienciaAp> getAllRows1(long perMedicionId) {
		List<HistoricoDeficienciaAp> historicoDeficienciaAp=null;
		 try {
	            HibernateUtil.begin();

	            Query q = HibernateUtil.getSession().createQuery("from HistoricoDeficienciaAp i where i.estado =:estado and i.periodoMedicion.perMedicionId = :perMedicionId order by fecRecepcion desc");
	            q.setString("estado","1");
	            q.setLong("perMedicionId", perMedicionId);
	            
	            historicoDeficienciaAp=(List<HistoricoDeficienciaAp>) q.list();
	            
	            HibernateUtil.commit();
	            HibernateUtil.close();

	        } catch (HibernateException e) {
	            HibernateUtil.rollback();
	            throw new HibernateException("No se puede encontrar Historico DeficienciaAp.", e);
	        }
		return historicoDeficienciaAp;
	}

    

}
