package pe.com.calidad.proc078.dao;

/**
 *
 * @author Luis
 */
import java.util.List;




import pe.com.calidad.proc078.entity.HistoricoDeficienciaAp;

import pe.com.indra.calidad.entity.Localidad;
import pe.com.indra.calidad.entity.PeriodoMedicion;

import java.util.Set;

public interface HistoricoDeficienciaApDao {

    public void create(HistoricoDeficienciaAp historicoDeficienciaAp);
    public HistoricoDeficienciaAp ReadById(long hisDeficienciaId);
    public void update(HistoricoDeficienciaAp historicoDeficienciaAp);
    public void delete(long hisDeficienciaId);
    
    public Localidad selectLocalidad(long hisDeficienciaId);
    public PeriodoMedicion selectPeriodo(long hisDeficienciaId);
      
    public List<HistoricoDeficienciaAp> getAllRows();
    public List<HistoricoDeficienciaAp> getAllRows1();
    public List<HistoricoDeficienciaAp> getAllRows1(long perMedicionId);
    
}
