package pe.com.calidad.proc078.dao;


import pe.com.calidad.proc078.entity.ResumenSubsanacion;


import java.util.List;
import java.util.Set;

/**
 *
 * @author Luis
 */
public interface ResumenSubsanacionDao {

   
    public List<ResumenSubsanacion> getAllRows();
    public List<ResumenSubsanacion> getAllRows1();
  
    
}
