package pe.com.calidad.proc078.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import pe.com.calidad.proc228.entity.Deficiencia;

@Entity
@Table(name="CAL_RESUMEN_SUBSANACION")
@SequenceGenerator(name = "ResSubsanacionIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "ResumenSubsanacionSeq")
public class ResumenSubsanacion implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long resSubsanacionId;
	private String ano;
	private String trimestre;
	private String codDefTipica;
	private long denPendentes;
	private long denPresentadas;
	private long denResueltas;
	private long denDesestimadas;
	private long denProximo;
	private long denDenPlazo;
	private long denFuePlazo;
	private long denSolAmpliacion;
	private BigDecimal dafp;
	
	public ResumenSubsanacion() {

	}

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ResSubsanacionIdGenerator")
    @Column(name = "RES_SUBSANACION_ID",nullable=false,unique=true)
	public long getResSubsanacionId() {
		return resSubsanacionId;
	}

	public void setResSubsanacionId(long resSubsanacionId) {
		this.resSubsanacionId = resSubsanacionId;
	}

	@Column(name="ANO",nullable=true)
	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	@Column(name="TRIMESTRE",nullable=true)
	public String getTrimestre() {
		return trimestre;
	}

	public void setTrimestre(String trimestre) {
		this.trimestre = trimestre;
	}

	@Column(name="COD_DEF_TIPICA",nullable=true)
	public String getCodDefTipica() {
		return codDefTipica;
	}

	public void setCodDefTipica(String codDefTipica) {
		this.codDefTipica = codDefTipica;
	}

	@Column(name="DEN_PENDENTES",nullable=true)
	public long getDenPendentes() {
		return denPendentes;
	}

	public void setDenPendentes(long denPendentes) {
		this.denPendentes = denPendentes;
	}

	@Column(name="DEN_PRESENTADAS",nullable=true)
	public long getDenPresentadas() {
		return denPresentadas;
	}

	public void setDenPresentadas(long denPresentadas) {
		this.denPresentadas = denPresentadas;
	}

	@Column(name="DEN_RESUELTAS",nullable=true)
	public long getDenResueltas() {
		return denResueltas;
	}

	public void setDenResueltas(long denResueltas) {
		this.denResueltas = denResueltas;
	}

	@Column(name="DEN_DESESTIMADAS",nullable=true)
	public long getDenDesestimadas() {
		return denDesestimadas;
	}

	public void setDenDesestimadas(long denDesestimadas) {
		this.denDesestimadas = denDesestimadas;
	}

	@Column(name="DEN_PROXIMO",nullable=true)
	public long getDenProximo() {
		return denProximo;
	}

	public void setDenProximo(long denProximo) {
		this.denProximo = denProximo;
	}

	@Column(name="DEN_DEN_PLAZO",nullable=true)
	public long getDenDenPlazo() {
		return denDenPlazo;
	}

	public void setDenDenPlazo(long denDenPlazo) {
		this.denDenPlazo = denDenPlazo;
	}

	@Column(name="DEN_FUE_PLAZO",nullable=true)
	public long getDenFuePlazo() {
		return denFuePlazo;
	}

	public void setDenFuePlazo(long denFuePlazo) {
		this.denFuePlazo = denFuePlazo;
	}

	@Column(name="DEN_SOL_AMPLIACION",nullable=true)
	public long getDenSolAmpliacion() {
		return denSolAmpliacion;
	}

	public void setDenSolAmpliacion(long denSolAmpliacion) {
		this.denSolAmpliacion = denSolAmpliacion;
	}

	@Column(name="DAFP",nullable=true)
	public BigDecimal getDafp() {
		return dafp;
	}

	public void setDafp(BigDecimal dafp) {
		this.dafp = dafp;
	}
	
	
	public String toString(){
		return new Long(this.getResSubsanacionId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getResSubsanacionId()==((ResumenSubsanacion)obj).getResSubsanacionId()){
			return true;
		}else{
			return false;
		}
	}
	

}
