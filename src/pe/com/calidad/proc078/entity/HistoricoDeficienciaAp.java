package pe.com.calidad.proc078.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import pe.com.calidad.proc228.entity.Deficiencia;
import pe.com.indra.calidad.entity.Localidad;
import pe.com.indra.calidad.entity.PeriodoMedicion;

@Entity
@Table(name="CAL_HISTORICO_DEFICIENCIA_AP")
@SequenceGenerator(name = "HisDeficienciaIdGenerator", initialValue = 0, allocationSize = 0, sequenceName = "HistoricoDeficienciaAPSeq")
public class HistoricoDeficienciaAp implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long hisDeficienciaId;
	private String ano;
	private String trimestre;
	private String sisElectrico;
	private String codIdeDenuncia;
	private String codIdeUniDeficiencia;
	private String numDenuncia;
	private Date fecRecepcion;
	private String forDetDenuncia;
	private String nomDenunciante;
	private String dirFroUap;
	private String dirDenunciante;
	private String denTranscripcion;
	private String telDenunciante;
	private String codDefTipicaRep;
	private String claZona;
	private String ubigeo;
	private Date fecVerCampo;
	private String codDefTipicaVer;
	private String codSed;
	private String codUap;
	private Date fecSubsanacion;
	private String otNumero;
	private String nomResponsable;
	private String cumplimiento;
	private String cauNoCumplimiento;
	private String solAmpPlazo;
	private String observacion;
	private String estado;
	private String sincronizar;
	
	private Date fecMaxResolucion;
	
	private Localidad localidad;
	private PeriodoMedicion periodoMedicion;
	
	public HistoricoDeficienciaAp() {

	}

    @ManyToOne
    @JoinColumn(name="PK_LOCALIDAD_ID", nullable=false)
	public Localidad getLocalidad() {
		return localidad;
	}

	public void setLocalidad(Localidad localidad) {
		this.localidad = localidad;
	}

    @ManyToOne
    @JoinColumn(name="PK_PER_MEDICION_ID", nullable=false)
	public PeriodoMedicion getPeriodoMedicion() {
		return periodoMedicion;
	}

	public void setPeriodoMedicion(PeriodoMedicion periodoMedicion) {
		this.periodoMedicion = periodoMedicion;
	}

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HisDeficienciaIdGenerator")
    @Column(name = "HIS_DEFICIENCIA_ID",nullable=false,unique=true) 	
	public long getHisDeficienciaId() {
		return hisDeficienciaId;
	}

	public void setHisDeficienciaId(long hisDeficienciaId) {
		this.hisDeficienciaId = hisDeficienciaId;
	}

	@Column(name="ANO",nullable=true)
	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	@Column(name="TRIMESTRE",nullable=true)
	public String getTrimestre() {
		return trimestre;
	}

	public void setTrimestre(String trimestre) {
		this.trimestre = trimestre;
	}

	@Column(name="SIS_ELECTRICO",nullable=true)
	public String getSisElectrico() {
		return sisElectrico;
	}

	public void setSisElectrico(String sisElectrico) {
		this.sisElectrico = sisElectrico;
	}

	@Column(name="COD_IDE_DENUNCIA",nullable=true)
	public String getCodIdeDenuncia() {
		return codIdeDenuncia;
	}

	public void setCodIdeDenuncia(String codIdeDenuncia) {
		this.codIdeDenuncia = codIdeDenuncia;
	}

	@Column(name="COD_IDE_UNI_DEFICIENCIA",nullable=true)
	public String getCodIdeUniDeficiencia() {
		return codIdeUniDeficiencia;
	}

	public void setCodIdeUniDeficiencia(String codIdeUniDeficiencia) {
		this.codIdeUniDeficiencia = codIdeUniDeficiencia;
	}

	@Column(name="NUM_DENUNCIA",nullable=true)
	public String getNumDenuncia() {
		return numDenuncia;
	}

	public void setNumDenuncia(String numDenuncia) {
		this.numDenuncia = numDenuncia;
	}

	@Column(name="FEC_RECEPCION",nullable=true)
	public Date getFecRecepcion() {
		return fecRecepcion;
	}

	public void setFecRecepcion(Date fecRecepcion) {
		this.fecRecepcion = fecRecepcion;
	}

	@Column(name="FOR_DET_DENUNCIA",nullable=true)
	public String getForDetDenuncia() {
		return forDetDenuncia;
	}

	public void setForDetDenuncia(String forDetDenuncia) {
		this.forDetDenuncia = forDetDenuncia;
	}

	@Column(name="NOM_DENUNCIANTE",nullable=true)
	public String getNomDenunciante() {
		return nomDenunciante;
	}

	public void setNomDenunciante(String nomDenunciante) {
		this.nomDenunciante = nomDenunciante;
	}

	@Column(name="DIR_FRO_UAP",nullable=true)
	public String getDirFroUap() {
		return dirFroUap;
	}

	public void setDirFroUap(String dirFroUap) {
		this.dirFroUap = dirFroUap;
	}

	@Column(name="DIR_DENUNCIANTE",nullable=true)
	public String getDirDenunciante() {
		return dirDenunciante;
	}

	public void setDirDenunciante(String dirDenunciante) {
		this.dirDenunciante = dirDenunciante;
	}

	@Column(name="DEN_TRANSCRIPCION",nullable=true)
	public String getDenTranscripcion() {
		return denTranscripcion;
	}

	public void setDenTranscripcion(String denTranscripcion) {
		this.denTranscripcion = denTranscripcion;
	}

	@Column(name="TEL_DENUNCIANTE",nullable=true)
	public String getTelDenunciante() {
		return telDenunciante;
	}

	public void setTelDenunciante(String telDenunciante) {
		this.telDenunciante = telDenunciante;
	}

	@Column(name="COD_DEF_TIPICA_REP",nullable=true)
	public String getCodDefTipicaRep() {
		return codDefTipicaRep;
	}

	public void setCodDefTipicaRep(String codDefTipicaRep) {
		this.codDefTipicaRep = codDefTipicaRep;
	}

	@Column(name="CLA_ZONA",nullable=true)
	public String getClaZona() {
		return claZona;
	}

	public void setClaZona(String claZona) {
		this.claZona = claZona;
	}

	@Column(name="UBIGEO",nullable=true)
	public String getUbigeo() {
		return ubigeo;
	}

	public void setUbigeo(String ubigeo) {
		this.ubigeo = ubigeo;
	}

	@Column(name="FEC_VER_CAMPO",nullable=true)
	public Date getFecVerCampo() {
		return fecVerCampo;
	}

	public void setFecVerCampo(Date fecVerCampo) {
		this.fecVerCampo = fecVerCampo;
	}

	@Column(name="COD_DEF_TIPICA_VER",nullable=true)
	public String getCodDefTipicaVer() {
		return codDefTipicaVer;
	}

	public void setCodDefTipicaVer(String codDefTipicaVer) {
		this.codDefTipicaVer = codDefTipicaVer;
	}

	@Column(name="COD_SED",nullable=true)
	public String getCodSed() {
		return codSed;
	}

	public void setCodSed(String codSed) {
		this.codSed = codSed;
	}

	@Column(name="COD_UAP",nullable=true)
	public String getCodUap() {
		return codUap;
	}

	public void setCodUap(String codUap) {
		this.codUap = codUap;
	}

	@Column(name="FEC_SUBSANACION",nullable=true)
	public Date getFecSubsanacion() {
		return fecSubsanacion;
	}

	public void setFecSubsanacion(Date fecSubsanacion) {
		this.fecSubsanacion = fecSubsanacion;
	}

	@Column(name="OT_NUMERO",nullable=true)
	public String getOtNumero() {
		return otNumero;
	}

	public void setOtNumero(String otNumero) {
		this.otNumero = otNumero;
	}

	@Column(name="NOM_RESPONSABLE",nullable=true)
	public String getNomResponsable() {
		return nomResponsable;
	}

	public void setNomResponsable(String nomResponsable) {
		this.nomResponsable = nomResponsable;
	}

	@Column(name="CUMPLIMIENTO",nullable=true)
	public String getCumplimiento() {
		return cumplimiento;
	}

	public void setCumplimiento(String cumplimiento) {
		this.cumplimiento = cumplimiento;
	}

	@Column(name="CAU_NO_CUMPLIMIENTO",nullable=true)
	public String getCauNoCumplimiento() {
		return cauNoCumplimiento;
	}

	public void setCauNoCumplimiento(String cauNoCumplimiento) {
		this.cauNoCumplimiento = cauNoCumplimiento;
	}

	@Column(name="SOL_AMP_PLAZO",nullable=true)
	public String getSolAmpPlazo() {
		return solAmpPlazo;
	}

	public void setSolAmpPlazo(String solAmpPlazo) {
		this.solAmpPlazo = solAmpPlazo;
	}

	@Column(name="OBSERVACION",nullable=true)
	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	@Column(name="ESTADO",nullable=true)
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Column(name="SINCRONIZAR",nullable=true)
	public String getSincronizar() {
		return sincronizar;
	}

	public void setSincronizar(String sincronizar) {
		this.sincronizar = sincronizar;
	}

	@Column(name="FEC_MAX_RESOLUCION",nullable=true)
	public Date getFecMaxResolucion() {
		return fecMaxResolucion;
	}

	public void setFecMaxResolucion(Date fecMaxResolucion) {
		this.fecMaxResolucion = fecMaxResolucion;
	}

	public String toString(){
		return new Long(this.getHisDeficienciaId()).toString();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this.getHisDeficienciaId()==((HistoricoDeficienciaAp)obj).getHisDeficienciaId()){
			return true;
		}else{
			return false;
		}
	}
	
}
