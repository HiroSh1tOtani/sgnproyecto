package pe.com.calidad.proc078.mbean;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.HibernateException;
import org.jfree.data.time.Year;
import org.richfaces.model.Filter;

import pe.com.calidad.alumbradopublico.entity.ProgramaAp;
import pe.com.calidad.proc078.entity.HistoricoDeficienciaAp;
import pe.com.calidad.proc078.service.HistoricoDeficienciaApService;
import pe.com.calidad.suministro.entity.Interrupcion;
import pe.com.calidad.suministro.entity.SuministroAfectado;
import pe.com.calidad.suministro.service.SuministroAfectadoService;
import pe.com.indra.calidad.entity.Localidad;
import pe.com.indra.calidad.entity.ParametroNorma;
import pe.com.indra.calidad.entity.PeriodoMedicion;
import pe.com.indra.calidad.entity.Reporte;
import pe.com.indra.calidad.service.EmpresaService;
import pe.com.indra.calidad.service.LocalidadService;
import pe.com.indra.calidad.service.ParametroNormaService;
import pe.com.indra.calidad.service.PeriodoMedicionService;
import pe.com.indra.calidad.service.ReporteService;
import pe.com.indra.calidad.util.ConectaDb;
import pe.com.indra.calidad.util.ModificarExcel;
import pe.com.indra.calidad.util.Sql;
import pe.com.indra.calidad.util.Utilidad;

@ManagedBean(name = "historicoDeficienciaApBean")
@SessionScoped
public class HistoricoDeficienciaApBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<HistoricoDeficienciaAp> historicosDeficienciaAp; // programasAp;
	private HistoricoDeficienciaAp historicoDeficienciaAp;

	private List<SelectItem> itemsPeriodoMedicion;
	private List<SelectItem> itemsPeriodoMedicion1;

	private List<SelectItem> itemsLocalidad;
	private List<SelectItem> itemsLocalidad1;
	private List<SelectItem> itemsForDetDenuncia;
	private List<SelectItem> itemsCodDefTipicaRep;
	private List<SelectItem> itemsClaZona;
	private List<SelectItem> itemsCumplimiento;
	private List<SelectItem> itemsSolAmpPlazo;
	private List<SelectItem> itemsCodDefTipicaVer;

	private PeriodoMedicion periodoMedicion;
	private String localidadFilter;
	private String numDenunciaFilter;
	private String codIdeDenunciaFilter;
	private String codIdeUniDeficienciaFilter;
	private String anoFilter;
	private String sisElectricoFilter;
	private String nomDenuncianteFilter;

	private String ubigeoFilter;
	private String codSedFilter;
	private String codUapFilter;
	private String otNumeroFilter;
	private String nomResponsableFilter;
	private String cauNoCumplimientoFilter;

	private java.util.Date fecRecepcionFilter;
	private java.util.Date fecVerCampoFilter;
	private java.util.Date fecSubsanacionFilter;
	private java.util.Date fecMaxResolucionFilter;

	private String forDetDenunciaFilter;
	private List<SelectItem> itemsForDetDenuncia1;
	private String codDefTipicaRepFilter;
	private List<SelectItem> itemsCodDefTipicaRep1;
	private String claZonaFilter;
	private List<SelectItem> itemsClaZona1;
	private String codDefTipicaVerFilter;
	private List<SelectItem> itemsCodDefTipicaVer1;
	private String solAmpPlazoFilter;
	private List<SelectItem> itemsSolAmpPlazo1;

	private java.util.Date fecRecepcion;
	private java.util.Date fecVerCampo;
	private java.util.Date fecSubsanacion;
	private java.util.Date fecMaxResolucion;
	private List<SelectItem> itemsTrimestre;
	private List<SelectItem> itemsSincronizar;

	private List<SelectItem> itemsTipoSin;
	private long tipoSin;
	private List<SelectItem> itemsOperacion;
	private String operacion;
	private java.util.Date fechaSincronizar;
	private String cumplimientoFilter;
	private List<SelectItem> itemsCumplimiento1;

	

	private String mensaje;

	@PostConstruct
	public void init() {
		System.out.println("se ejecuto PostConstruct " + new java.util.Date());
		historicoDeficienciaAp = new HistoricoDeficienciaAp();
		historicoDeficienciaAp.setEstado("1");
		tipoSin = 1;
		operacion = "SIRN";
		fechaSincronizar = Calendar.getInstance().getTime();

	}

	public List<HistoricoDeficienciaAp> getHistoricosDeficienciaAp() {
		return historicosDeficienciaAp;
	}

	public void setHistoricosDeficienciaAp(
			List<HistoricoDeficienciaAp> historicosDeficienciaAp) {
		this.historicosDeficienciaAp = historicosDeficienciaAp;
	}

	public HistoricoDeficienciaAp getHistoricoDeficienciaAp() {
		return historicoDeficienciaAp;
	}

	public void setHistoricoDeficienciaAp(
			HistoricoDeficienciaAp historicoDeficienciaAp) {
		this.historicoDeficienciaAp = historicoDeficienciaAp;
	}

	public List<SelectItem> getItemsPeriodoMedicion() {
		PeriodoMedicionService service = new PeriodoMedicionService();
		itemsPeriodoMedicion = new ArrayList<SelectItem>();
		for (PeriodoMedicion tp : service.getAllRows()) {
			itemsPeriodoMedicion.add(new SelectItem(tp, tp.getAno() + "-"
					+ tp.getPeriodo()));
		}
		return itemsPeriodoMedicion;
	}

	public void setItemsPeriodoMedicion(List<SelectItem> itemsPeriodoMedicion) {
		this.itemsPeriodoMedicion = itemsPeriodoMedicion;
	}

	public List<SelectItem> getItemsPeriodoMedicion1() {
		PeriodoMedicionService service = new PeriodoMedicionService();
		itemsPeriodoMedicion1 = new ArrayList<SelectItem>();
		for (PeriodoMedicion tp : service.getAllRows1()) {
			itemsPeriodoMedicion1.add(new SelectItem(tp, tp.getAno() + "-"
					+ tp.getPeriodo()));
		}
		return itemsPeriodoMedicion1;
	}

	public void setItemsPeriodoMedicion1(List<SelectItem> itemsPeriodoMedicion1) {
		this.itemsPeriodoMedicion1 = itemsPeriodoMedicion1;
	}

	public List<SelectItem> getItemsLocalidad() {
		LocalidadService service = new LocalidadService();
		itemsLocalidad = new ArrayList<SelectItem>();
		for (Localidad tp : service.getAllRows()) {
			itemsLocalidad.add(new SelectItem(tp, tp.getCodLocalidad() + "-"
					+ tp.getNombre()));
		}
		return itemsLocalidad;
	}

	public void setItemsLocalidad(List<SelectItem> itemsLocalidad) {
		this.itemsLocalidad = itemsLocalidad;
	}

	public List<SelectItem> getItemsLocalidad1() {
		LocalidadService service = new LocalidadService();
		itemsLocalidad1 = new ArrayList<SelectItem>();
		itemsLocalidad1.add(new SelectItem("", ""));
		for (Localidad tp : service.getAllRows1()) {
			itemsLocalidad1.add(new SelectItem(tp.getCodLocalidad(), tp
					.getCodLocalidad() + "-" + tp.getNombre()));
		}
		return itemsLocalidad1;
	}

	public void setItemsLocalidad1(List<SelectItem> itemsLocalidad1) {
		this.itemsLocalidad1 = itemsLocalidad1;
	}

	public String prepararCrear() {
		// Esto funciona siempre que el bean tenga ambito de sesion
		setHistoricoDeficienciaAp(new HistoricoDeficienciaAp());
		getHistoricoDeficienciaAp().setEstado("1");
		getHistoricoDeficienciaAp().setPeriodoMedicion(periodoMedicion);
		getHistoricoDeficienciaAp().setAno(periodoMedicion.getAno());

		historicoDeficienciaAp = new HistoricoDeficienciaAp();

		if (historicoDeficienciaAp.getPeriodoMedicion() != null) {
			if (historicoDeficienciaAp.getPeriodoMedicion().getPeriodo()
					.equals("01")
					|| historicoDeficienciaAp.getPeriodoMedicion().getPeriodo()
							.equals("02")
					|| historicoDeficienciaAp.getPeriodoMedicion().getPeriodo()
							.equals("03")) {

				historicoDeficienciaAp.setTrimestre("1");
			}

			if (historicoDeficienciaAp.getPeriodoMedicion().getPeriodo()
					.equals("04")
					|| historicoDeficienciaAp.getPeriodoMedicion().getPeriodo()
							.equals("05")
					|| historicoDeficienciaAp.getPeriodoMedicion().getPeriodo()
							.equals("06")) {

				historicoDeficienciaAp.setTrimestre("2");
			}

			if (historicoDeficienciaAp.getPeriodoMedicion().getPeriodo()
					.equals("07")
					|| historicoDeficienciaAp.getPeriodoMedicion().getPeriodo()
							.equals("08")
					|| historicoDeficienciaAp.getPeriodoMedicion().getPeriodo()
							.equals("09")) {

				historicoDeficienciaAp.setTrimestre("3");
			}

			if (historicoDeficienciaAp.getPeriodoMedicion().getPeriodo()
					.equals("10")
					|| historicoDeficienciaAp.getPeriodoMedicion().getPeriodo()
							.equals("11")
					|| historicoDeficienciaAp.getPeriodoMedicion().getPeriodo()
							.equals("12")) {

				historicoDeficienciaAp.setTrimestre("4");
			}
		}

		return "/zonasegura/proc078/historicoDeficienciaApCrear?faces-redirect=true";
	}

	public String prepararEditar() {

		try {
			cargarHistoricoDeficienciaApActual();

			return "/zonasegura/proc078/historicoDeficienciaApEditar?faces-redirect=true";
		} catch (HibernateException e) {
			setMensaje(e.getMessage());
			return "/zonasegura/proc078/historicoDeficienciaApListar?faces-redirect=true";
		}

	}

	public String prepararVer() {
		// TODO: Por implementar carga de la instancia

		try {
			cargarHistoricoDeficienciaApActual();
			return "/zonasegura/proc078/historicoDeficienciaApVer?faces-redirect=true";
		} catch (HibernateException e) {
			setMensaje(e.getMessage());
			return "/zonasegura/proc078/historicoDeficienciaApListar?faces-redirect=true";
		}
	}

	public String salvar() {

		HistoricoDeficienciaApService service = new HistoricoDeficienciaApService();

		try {

			if (fecRecepcion != null) {
				historicoDeficienciaAp.setFecRecepcion(new java.sql.Date(
						fecRecepcion.getTime()));
			} else {
				historicoDeficienciaAp.setFecRecepcion(null);
			}
			if (fecVerCampo != null) {
				historicoDeficienciaAp.setFecVerCampo(new java.sql.Date(
						fecVerCampo.getTime()));
			} else {
				historicoDeficienciaAp.setFecVerCampo(null);
			}
			if (fecSubsanacion != null) {
				historicoDeficienciaAp.setFecSubsanacion(new java.sql.Date(
						fecSubsanacion.getTime()));
			} else {
				historicoDeficienciaAp.setFecSubsanacion(null);
			}
			if (fecMaxResolucion != null) {
				historicoDeficienciaAp.setFecMaxResolucion(new java.sql.Date(
						fecMaxResolucion.getTime()));
			} else {
				historicoDeficienciaAp.setFecMaxResolucion(null);
			}

			if (historicoDeficienciaAp.getFecRecepcion() != null
					&& historicoDeficienciaAp.getFecVerCampo() != null) {
				if (historicoDeficienciaAp.getFecRecepcion().getTime() >= historicoDeficienciaAp
						.getFecVerCampo().getTime()) {
					setMensaje("La fecha de Recepcion no puede ser posterior a la de Verificacion.");
					FacesContext.getCurrentInstance().addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR,
									getMensaje(), getMensaje()));
					FacesContext.getCurrentInstance().getExternalContext()
							.getFlash().setKeepMessages(true);
				}
			}

			if (historicoDeficienciaAp.getFecRecepcion() != null
					&& historicoDeficienciaAp.getFecMaxResolucion() != null) {
				if (historicoDeficienciaAp.getFecRecepcion().getTime() > historicoDeficienciaAp
						.getFecMaxResolucion().getTime()) {
					setMensaje("La fecha de Recepcion no puede ser posterior a la de la Resolucion.");
					FacesContext.getCurrentInstance().addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR,
									getMensaje(), getMensaje()));
					FacesContext.getCurrentInstance().getExternalContext()
							.getFlash().setKeepMessages(true);
				}
			}

			if (historicoDeficienciaAp.getFecVerCampo() != null
					&& historicoDeficienciaAp.getFecSubsanacion() != null) {
				if (historicoDeficienciaAp.getFecVerCampo().getTime() > historicoDeficienciaAp
						.getFecSubsanacion().getTime()) {
					setMensaje("La fecha de Verificacion no puede ser posterior a la de la Subsanacion.");
					FacesContext.getCurrentInstance().addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR,
									getMensaje(), getMensaje()));
					FacesContext.getCurrentInstance().getExternalContext()
							.getFlash().setKeepMessages(true);
				}
			}

			int anoRecepcion = 0;
			int mesRecepcion = 0;
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new java.sql.Date(fecRecepcion.getTime())); // Configuramos
																			// la
																			// fecha
																			// que
																			// se
																			// recibe
			anoRecepcion = calendar.get(Calendar.YEAR);
			mesRecepcion = calendar.get(Calendar.MONTH);

			String anoPeriodo = null;
			String mesPeriodo = null;
			Long anoPeriodoLong;
			Long mesPeriodoLong;
			anoPeriodo = periodoMedicion.getAno();
			mesPeriodo = periodoMedicion.getPeriodo();

			anoPeriodoLong = Long.valueOf(anoPeriodo);
			mesPeriodoLong = Long.valueOf(mesPeriodo);

			if (anoRecepcion != anoPeriodoLong
					|| mesRecepcion != mesPeriodoLong) {
				setMensaje("La fecha de Recepcion esta fuera del Periodo.");
				FacesContext.getCurrentInstance().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								getMensaje(), getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext()
						.getFlash().setKeepMessages(true);

				return "/zonasegura/proc078/historicoDeficienciaApCrear?faces-redirect=true";
			}

			service.create(historicoDeficienciaAp);

		} catch (HibernateException e) {
			setMensaje(e.getMessage() + ":" + e.getCause());
			FacesContext.getCurrentInstance()
					.addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR,
									"No se pudo completar la Transacci�n",
									getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash()
					.setKeepMessages(true);

		} catch (Exception e) {

			setMensaje(e.getMessage() + ":" + e.getCause());
			FacesContext.getCurrentInstance()
					.addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR,
									"No se pudo completar la Transacci�n",
									getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash()
					.setKeepMessages(true);

		}

		HistoricoDeficienciaApxPeriodo();

		return "/zonasegura/proc078/historicoDeficienciaApListar?faces-redirect=true";
	}

	public String actualizar() {

		HistoricoDeficienciaApService service = new HistoricoDeficienciaApService();

		try {

			if (fecRecepcion != null) {
				historicoDeficienciaAp.setFecRecepcion(new java.sql.Date(
						fecRecepcion.getTime()));
			} else {
				historicoDeficienciaAp.setFecRecepcion(null);
			}
			if (fecVerCampo != null) {
				historicoDeficienciaAp.setFecVerCampo(new java.sql.Date(
						fecVerCampo.getTime()));
			} else {
				historicoDeficienciaAp.setFecVerCampo(null);
			}
			if (fecSubsanacion != null) {
				historicoDeficienciaAp.setFecSubsanacion(new java.sql.Date(
						fecSubsanacion.getTime()));
			} else {
				historicoDeficienciaAp.setFecSubsanacion(null);
			}
			if (fecMaxResolucion != null) {
				historicoDeficienciaAp.setFecMaxResolucion(new java.sql.Date(
						fecMaxResolucion.getTime()));
			} else {
				historicoDeficienciaAp.setFecMaxResolucion(null);
			}

			if (historicoDeficienciaAp.getFecRecepcion() != null
					&& historicoDeficienciaAp.getFecVerCampo() != null) {
				if (historicoDeficienciaAp.getFecRecepcion().getTime() >= historicoDeficienciaAp
						.getFecVerCampo().getTime()) {
					setMensaje("La fecha de Recepcion no puede ser posterior a la de Verificacion.");
					FacesContext.getCurrentInstance().addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR,
									getMensaje(), getMensaje()));
					FacesContext.getCurrentInstance().getExternalContext()
							.getFlash().setKeepMessages(true);
				}
			}

			if (historicoDeficienciaAp.getFecRecepcion() != null
					&& historicoDeficienciaAp.getFecMaxResolucion() != null) {
				if (historicoDeficienciaAp.getFecRecepcion().getTime() > historicoDeficienciaAp
						.getFecMaxResolucion().getTime()) {
					setMensaje("La fecha de Recepcion no puede ser posterior a la de la Resolucion.");
					FacesContext.getCurrentInstance().addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR,
									getMensaje(), getMensaje()));
					FacesContext.getCurrentInstance().getExternalContext()
							.getFlash().setKeepMessages(true);
				}
			}

			if (historicoDeficienciaAp.getFecVerCampo() != null
					&& historicoDeficienciaAp.getFecSubsanacion() != null) {
				if (historicoDeficienciaAp.getFecVerCampo().getTime() > historicoDeficienciaAp
						.getFecSubsanacion().getTime()) {
					setMensaje("La fecha de Verificacion no puede ser posterior a la de la Subsanacion.");
					FacesContext.getCurrentInstance().addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR,
									getMensaje(), getMensaje()));
					FacesContext.getCurrentInstance().getExternalContext()
							.getFlash().setKeepMessages(true);
				}
			}

			int anoRecepcion = 0;
			int mesRecepcion = 0;
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new java.sql.Date(fecRecepcion.getTime())); // Configuramos
																			// la
																			// fecha
																			// que
																			// se
																			// recibe
			anoRecepcion = calendar.get(Calendar.YEAR);
			mesRecepcion = calendar.get(Calendar.MONTH);

			String anoPeriodo = null;
			String mesPeriodo = null;
			Long anoPeriodoLong;
			Long mesPeriodoLong;
			anoPeriodo = periodoMedicion.getAno();
			mesPeriodo = periodoMedicion.getPeriodo();

			anoPeriodoLong = Long.valueOf(anoPeriodo);
			mesPeriodoLong = Long.valueOf(mesPeriodo);

			if (anoRecepcion != anoPeriodoLong
					|| mesRecepcion != mesPeriodoLong) {
				setMensaje("La fecha de Recepcion no puede ser diferente del A�o y mes del Periodo.");
				FacesContext.getCurrentInstance().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								getMensaje(), getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext()
						.getFlash().setKeepMessages(true);
			}

			service.update(historicoDeficienciaAp);

		} catch (HibernateException e) {
			setMensaje(e.getMessage() + ":" + e.getCause());
			FacesContext.getCurrentInstance()
					.addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR,
									"No se pudo completar la Transacci�n",
									getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash()
					.setKeepMessages(true);

		} catch (Exception e) {

			setMensaje(e.getMessage() + ":" + e.getCause());
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Error desconocido.", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash()
					.setKeepMessages(true);

			e.printStackTrace();
		}

		HistoricoDeficienciaApxPeriodo();

		return "/zonasegura/proc078/historicoDeficienciaApListar?faces-redirect=true";
	}

	public String eliminar() {
		String hisDeficienciaId = Utilidad.getParametro("hisDeficienciaId");
		HistoricoDeficienciaApService service = new HistoricoDeficienciaApService();

		try {
			historicoDeficienciaAp = service
					.ReadById(new Long(hisDeficienciaId));
			historicoDeficienciaAp.setEstado("0");

			service.update(historicoDeficienciaAp);
		} catch (HibernateException e) {
			setMensaje(e.getMessage() + ":" + e.getCause());
			e.printStackTrace();
			FacesContext.getCurrentInstance()
					.addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR,
									"No se pudo completar la Transacci�n",
									getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash()
					.setKeepMessages(true);

		} catch (Exception e) {

			setMensaje(e.getMessage() + ":" + e.getCause());
			FacesContext.getCurrentInstance()
					.addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR,
									"No se pudo completar la Transacci�n",
									getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash()
					.setKeepMessages(true);
		}

		HistoricoDeficienciaApxPeriodo();

		return "/zonasegura/proc078/historicoDeficienciaApListar?faces-redirect=true";
	}

	public void cargarHistoricoDeficienciaApActual() {
		String hisDeficienciaId = Utilidad.getParametro("hisDeficienciaId");

		HistoricoDeficienciaApService service = new HistoricoDeficienciaApService();

		try {

			historicoDeficienciaAp = service
					.ReadById(new Long(hisDeficienciaId));

			if (historicoDeficienciaAp.getFecRecepcion() != null) {
				fecRecepcion = new java.util.Date(historicoDeficienciaAp
						.getFecRecepcion().getTime());
			} else {
				fecRecepcion = null;
			}
			if (historicoDeficienciaAp.getFecVerCampo() != null) {
				fecVerCampo = new java.util.Date(historicoDeficienciaAp
						.getFecVerCampo().getTime());
			} else {
				fecVerCampo = null;
			}
			if (historicoDeficienciaAp.getFecSubsanacion() != null) {
				fecSubsanacion = new java.util.Date(historicoDeficienciaAp
						.getFecSubsanacion().getTime());
			} else {
				fecSubsanacion = null;
			}
			if (historicoDeficienciaAp.getFecMaxResolucion() != null) {
				fecMaxResolucion = new java.util.Date(historicoDeficienciaAp
						.getFecMaxResolucion().getTime());
			} else {
				fecMaxResolucion = null;
			}

		} catch (HibernateException e) {
			setMensaje(e.getMessage() + ":" + e.getCause());
			FacesContext.getCurrentInstance()
					.addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR,
									"No se pudo completar la Transacci�n",
									getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash()
					.setKeepMessages(true);

			throw new HibernateException("No se puede cargar ProgramaAp.", e);

		} catch (Exception e) {

			setMensaje(e.getMessage() + ":" + e.getCause());
			FacesContext.getCurrentInstance()
					.addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR,
									"No se pudo completar la Transacci�n",
									getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash()
					.setKeepMessages(true);
		}

	}

	public String HistoricoDeficienciaApxPeriodo() {
		HistoricoDeficienciaApService service = new HistoricoDeficienciaApService();

		localidadFilter = null;
		numDenunciaFilter = null;
		codIdeDenunciaFilter = null;
		codIdeUniDeficienciaFilter = null;
		anoFilter = null;
		sisElectricoFilter = null;
		nomDenuncianteFilter = null;

		ubigeoFilter = null;
		codSedFilter = null;
		codUapFilter = null;
		otNumeroFilter = null;
		nomResponsableFilter = null;
		cauNoCumplimientoFilter = null;

		fecRecepcionFilter = null;
		fecVerCampoFilter = null;
		fecSubsanacionFilter = null;
		fecMaxResolucionFilter = null;

		forDetDenunciaFilter = null;
		codDefTipicaRepFilter = null;
		claZonaFilter = null;
		codDefTipicaVerFilter = null;
		solAmpPlazoFilter = null;
		cumplimientoFilter = null;

		if (periodoMedicion.getPerMedicionId() > 0) {
			historicosDeficienciaAp = service.getAllRows1(periodoMedicion
					.getPerMedicionId());
		}
		return "/zonasegura/proc078/historicoDeficienciaApListar?faces-redirect=true";
	}

	public void filtroChanged(AjaxBehaviorEvent event) {

		HistoricoDeficienciaApxPeriodo();
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public PeriodoMedicion getPeriodoMedicion() {
		return periodoMedicion;
	}

	public void setPeriodoMedicion(PeriodoMedicion periodoMedicion) {
		this.periodoMedicion = periodoMedicion;
	}



	public Filter<?> getFilterLocalidad() {
        return new Filter<HistoricoDeficienciaAp>() {
            public boolean accept(HistoricoDeficienciaAp t) {
            	String localidad = getLocalidadFilter();
            	
                if (localidad == null) {
                	return true;
                }
                if (localidad.length() == 0) {
                	return true;
                }	
                if(localidad.length() > 0) {
                	if(t.getLocalidad() == null){
                		return false;
                	}
                	
                	if(localidad.equals(t.getLocalidad().getCodLocalidad())) {
                		return true;
                	}
                }
            	
                return false;
            }
        };
    }

	public java.util.Date getFecRecepcion() {
		return fecRecepcion;
	}

	public void setFecRecepcion(java.util.Date fecRecepcion) {
		this.fecRecepcion = fecRecepcion;
	}

	public java.util.Date getFecVerCampo() {
		return fecVerCampo;
	}

	public void setFecVerCampo(java.util.Date fecVerCampo) {
		this.fecVerCampo = fecVerCampo;
	}

	public java.util.Date getFecSubsanacion() {
		return fecSubsanacion;
	}

	public void setFecSubsanacion(java.util.Date fecSubsanacion) {
		this.fecSubsanacion = fecSubsanacion;
	}

	public java.util.Date getFecMaxResolucion() {
		return fecMaxResolucion;
	}

	public void setFecMaxResolucion(java.util.Date fecMaxResolucion) {
		this.fecMaxResolucion = fecMaxResolucion;
	}

	public List<SelectItem> getItemsForDetDenuncia() {
		itemsForDetDenuncia = new ArrayList<SelectItem>();
		itemsForDetDenuncia.add(new SelectItem("1", "1-ORAL (PERSONAL)"));
		itemsForDetDenuncia.add(new SelectItem("2", "2-ESCRITA"));
		itemsForDetDenuncia.add(new SelectItem("3", "3-TELEFONICA"));
		itemsForDetDenuncia.add(new SelectItem("4", "4-CORREO ELECTRONICO"));
		itemsForDetDenuncia.add(new SelectItem("5",	"5-POR SUPERVISION (FISCALIZACION)"));
		itemsForDetDenuncia.add(new SelectItem("6", "6-POR MEDICIONES NTCSE"));
		itemsForDetDenuncia.add(new SelectItem("7", "7-POR OTRA MODALIDAD"));
		return itemsForDetDenuncia;
	}

	public void setItemsForDetDenuncia(List<SelectItem> itemsForDetDenuncia) {
		this.itemsForDetDenuncia = itemsForDetDenuncia;
	}

	public List<SelectItem> getItemsCodDefTipicaRep() {
		itemsCodDefTipicaRep = new ArrayList<SelectItem>();
		itemsCodDefTipicaRep.add(new SelectItem("DT1",
				"DT1 - LAMPARA INOPERATIVA"));
		itemsCodDefTipicaRep.add(new SelectItem("DT2",
				"DT2 - PASTORAL ROTO O MAL ORIENTADO"));
		itemsCodDefTipicaRep.add(new SelectItem("DT3",
				"DT3 - FALTA DE UNIDAD DE AP"));
		itemsCodDefTipicaRep.add(new SelectItem("DT4",
				"DT4 - INTERFERENCIA DE ARBOL"));
		itemsCodDefTipicaRep.add(new SelectItem("DT5",
				"DT5 - DIFUSOR INOPERATIVO"));
		return itemsCodDefTipicaRep;
	}

	public void setItemsCodDefTipicaRep(List<SelectItem> itemsCodDefTipicaRep) {
		this.itemsCodDefTipicaRep = itemsCodDefTipicaRep;
	}

	public List<SelectItem> getItemsClaZona() {
		itemsClaZona = new ArrayList<SelectItem>();
		itemsClaZona.add(new SelectItem("ST1", "SECTOR TIPICO 1"));
		itemsClaZona.add(new SelectItem("ST2", "SECTOR TIPICO 2"));
		itemsClaZona.add(new SelectItem("ST3", "SECTOR TIPICO 3"));
		itemsClaZona.add(new SelectItem("ST4", "SECTOR TIPICO 4"));
		itemsClaZona.add(new SelectItem("ST5", "SECTOR TIPICO 5"));
		itemsClaZona.add(new SelectItem("ST6", "SECTOR TIPICO 6"));
		itemsClaZona.add(new SelectItem("ST9", "SECTOR TIPICO 9"));
		return itemsClaZona;
	}

	public void setItemsClaZona(List<SelectItem> itemsClaZona) {
		this.itemsClaZona = itemsClaZona;
	}

	public List<SelectItem> getItemsCumplimiento() {
		itemsCumplimiento = new ArrayList<SelectItem>();
		itemsCumplimiento.add(new SelectItem("N", "N-NO ATENDIDA"));
		itemsCumplimiento.add(new SelectItem("0", "0-FUERA DE PLAZO"));
		itemsCumplimiento.add(new SelectItem("1", "1-DENTRO PLAZO"));
		itemsCumplimiento.add(new SelectItem("2", "2-DESESTIMADA"));
		return itemsCumplimiento;
	}

	public void setItemsCumplimiento(List<SelectItem> itemsCumplimiento) {
		this.itemsCumplimiento = itemsCumplimiento;
	}

	public List<SelectItem> getItemsSolAmpPlazo() {
		itemsSolAmpPlazo = new ArrayList<SelectItem>();
		itemsSolAmpPlazo.add(new SelectItem("S", "SI"));
		itemsSolAmpPlazo.add(new SelectItem("N", "NO"));
		return itemsSolAmpPlazo;
	}

	public void setItemsSolAmpPlazo(List<SelectItem> itemsSolAmpPlazo) {
		this.itemsSolAmpPlazo = itemsSolAmpPlazo;
	}

	public String getLocalidadFilter() {
		return localidadFilter;
	}

	public void setLocalidadFilter(String localidadFilter) {
		this.localidadFilter = localidadFilter;
	}

	public List<SelectItem> getItemsCodDefTipicaVer() {
		itemsCodDefTipicaVer = new ArrayList<SelectItem>();
		itemsCodDefTipicaVer.add(new SelectItem("DT1",
				"DT1 - LAMPARA INOPERATIVA"));
		itemsCodDefTipicaVer.add(new SelectItem("DT2",
				"DT2 - PASTORAL ROTO O MAL ORIENTADO"));
		itemsCodDefTipicaVer.add(new SelectItem("DT3",
				"DT3 - FALTA DE UNIDAD DE AP"));
		itemsCodDefTipicaVer.add(new SelectItem("DT4",
				"DT4 - INTERFERENCIA DE ARBOL"));
		itemsCodDefTipicaVer.add(new SelectItem("DT5",
				"DT5 - DIFUSOR INOPERATIVO"));
		return itemsCodDefTipicaVer;
	}

	public void setItemsCodDefTipicaVer(List<SelectItem> itemsCodDefTipicaVer) {
		this.itemsCodDefTipicaVer = itemsCodDefTipicaVer;
	}

	public String getNumDenunciaFilter() {
		return numDenunciaFilter;
	}

	public void setNumDenunciaFilter(String numDenunciaFilter) {
		this.numDenunciaFilter = numDenunciaFilter;
	}

	public String getCodIdeDenunciaFilter() {
		return codIdeDenunciaFilter;
	}

	public void setCodIdeDenunciaFilter(String codIdeDenunciaFilter) {
		this.codIdeDenunciaFilter = codIdeDenunciaFilter;
	}

	public String getCodIdeUniDeficienciaFilter() {
		return codIdeUniDeficienciaFilter;
	}

	public void setCodIdeUniDeficienciaFilter(String codIdeUniDeficienciaFilter) {
		this.codIdeUniDeficienciaFilter = codIdeUniDeficienciaFilter;
	}

	public String getAnoFilter() {
		return anoFilter;
	}

	public void setAnoFilter(String anoFilter) {
		this.anoFilter = anoFilter;
	}

	public String getSisElectricoFilter() {
		return sisElectricoFilter;
	}

	public void setSisElectricoFilter(String sisElectricoFilter) {
		this.sisElectricoFilter = sisElectricoFilter;
	}

	public String getNomDenuncianteFilter() {
		return nomDenuncianteFilter;
	}

	public void setNomDenuncianteFilter(String nomDenuncianteFilter) {
		this.nomDenuncianteFilter = nomDenuncianteFilter;
	}

	public String getUbigeoFilter() {
		return ubigeoFilter;
	}

	public void setUbigeoFilter(String ubigeoFilter) {
		this.ubigeoFilter = ubigeoFilter;
	}

	public String getCodSedFilter() {
		return codSedFilter;
	}

	public void setCodSedFilter(String codSedFilter) {
		this.codSedFilter = codSedFilter;
	}

	public String getCodUapFilter() {
		return codUapFilter;
	}

	public void setCodUapFilter(String codUapFilter) {
		this.codUapFilter = codUapFilter;
	}

	public String getOtNumeroFilter() {
		return otNumeroFilter;
	}

	public void setOtNumeroFilter(String otNumeroFilter) {
		this.otNumeroFilter = otNumeroFilter;
	}

	public String getNomResponsableFilter() {
		return nomResponsableFilter;
	}

	public void setNomResponsableFilter(String nomResponsableFilter) {
		this.nomResponsableFilter = nomResponsableFilter;
	}

	public String getCauNoCumplimientoFilter() {
		return cauNoCumplimientoFilter;
	}

	public void setCauNoCumplimientoFilter(String cauNoCumplimientoFilter) {
		this.cauNoCumplimientoFilter = cauNoCumplimientoFilter;
	}

	public java.util.Date getFecRecepcionFilter() {
		return fecRecepcionFilter;
	}

	public void setFecRecepcionFilter(java.util.Date fecRecepcionFilter) {
		this.fecRecepcionFilter = fecRecepcionFilter;
	}

	public java.util.Date getFecVerCampoFilter() {
		return fecVerCampoFilter;
	}

	public void setFecVerCampoFilter(java.util.Date fecVerCampoFilter) {
		this.fecVerCampoFilter = fecVerCampoFilter;
	}

	public java.util.Date getFecSubsanacionFilter() {
		return fecSubsanacionFilter;
	}

	public void setFecSubsanacionFilter(java.util.Date fecSubsanacionFilter) {
		this.fecSubsanacionFilter = fecSubsanacionFilter;
	}

	public Filter<?> getFilterFecMaxResolucion() {
		return new Filter<HistoricoDeficienciaAp>() {
			public boolean accept(HistoricoDeficienciaAp historicoDeficienciaAp) {
				Date fecMaxResolucion = getFecMaxResolucionFilter();
				if (fecMaxResolucion == null) {

					return true;
				} else {

					Calendar calendar1 = Calendar.getInstance();
					Calendar calendar2 = Calendar.getInstance();

					calendar1.setTime(fecRecepcion);

					if (historicoDeficienciaAp.getFecMaxResolucion() == null) {
						return false;
					}

					calendar2.setTime(historicoDeficienciaAp
							.getFecMaxResolucion());

					if (calendar1.get(Calendar.YEAR) == calendar2
							.get(Calendar.YEAR)
							&& calendar1.get(Calendar.MONTH) == calendar2
									.get(Calendar.MONTH)
							&& calendar1.get(Calendar.DAY_OF_MONTH) == calendar2
									.get(Calendar.DAY_OF_MONTH)) {

						return true;

					} else {
						return false;
					}

				}
			}
		};
	}

	public Filter<?> getFilterFecVerCampo() {
		return new Filter<HistoricoDeficienciaAp>() {
			public boolean accept(HistoricoDeficienciaAp historicoDeficienciaAp) {
				Date fecVerCampo = getFecVerCampoFilter();
				if (fecVerCampo == null) {

					return true;
				} else {

					Calendar calendar1 = Calendar.getInstance();
					Calendar calendar2 = Calendar.getInstance();

					calendar1.setTime(fecVerCampo);

					if (historicoDeficienciaAp.getFecVerCampo() == null) {
						return false;
					}

					calendar2.setTime(historicoDeficienciaAp.getFecVerCampo());

					if (calendar1.get(Calendar.YEAR) == calendar2
							.get(Calendar.YEAR)
							&& calendar1.get(Calendar.MONTH) == calendar2
									.get(Calendar.MONTH)
							&& calendar1.get(Calendar.DAY_OF_MONTH) == calendar2
									.get(Calendar.DAY_OF_MONTH)) {

						return true;

					} else {
						return false;
					}

				}
			}
		};
	}

	public Filter<?> getFilterFecSubsanacion() {
		return new Filter<HistoricoDeficienciaAp>() {
			public boolean accept(HistoricoDeficienciaAp historicoDeficienciaAp) {
				Date fecSubsanacion = getFecSubsanacionFilter();
				if (fecSubsanacion == null) {

					return true;
				} else {

					Calendar calendar1 = Calendar.getInstance();
					Calendar calendar2 = Calendar.getInstance();

					calendar1.setTime(fecSubsanacion);

					if (historicoDeficienciaAp.getFecSubsanacion() == null) {
						return false;
					}

					calendar2.setTime(historicoDeficienciaAp
							.getFecSubsanacion());

					if (calendar1.get(Calendar.YEAR) == calendar2
							.get(Calendar.YEAR)
							&& calendar1.get(Calendar.MONTH) == calendar2
									.get(Calendar.MONTH)
							&& calendar1.get(Calendar.DAY_OF_MONTH) == calendar2
									.get(Calendar.DAY_OF_MONTH)) {

						return true;

					} else {
						return false;
					}

				}
			}
		};
	}

	public Filter<?> getFilterFecRecepcion() {
		return new Filter<HistoricoDeficienciaAp>() {
			public boolean accept(HistoricoDeficienciaAp historicoDeficienciaAp) {
				Date fecRecepcion = getFecRecepcionFilter();
				if (fecRecepcion == null) {

					return true;
				} else {

					Calendar calendar1 = Calendar.getInstance();
					Calendar calendar2 = Calendar.getInstance();

					calendar1.setTime(fecRecepcion);

					if (historicoDeficienciaAp.getFecRecepcion() == null) {
						return false;
					}

					calendar2.setTime(historicoDeficienciaAp.getFecRecepcion());

					if (calendar1.get(Calendar.YEAR) == calendar2
							.get(Calendar.YEAR)
							&& calendar1.get(Calendar.MONTH) == calendar2
									.get(Calendar.MONTH)
							&& calendar1.get(Calendar.DAY_OF_MONTH) == calendar2
									.get(Calendar.DAY_OF_MONTH)) {

						return true;

					} else {
						return false;
					}

				}
			}
		};
	}

	public String getForDetDenunciaFilter() {
		return forDetDenunciaFilter;
	}

	public void setForDetDenunciaFilter(String forDetDenunciaFilter) {
		this.forDetDenunciaFilter = forDetDenunciaFilter;
	}

	public Filter<?> getFilterForDetDenuncia() {
		return new Filter<HistoricoDeficienciaAp>() {
			public boolean accept(HistoricoDeficienciaAp t) {
				String forDetDenuncia = getForDetDenunciaFilter();
				if (forDetDenuncia == null || forDetDenuncia.length() == 0
						|| forDetDenuncia.equals(t.getForDetDenuncia())) {
					return true;
				}
				return false;
			}
		};
	}

	public List<SelectItem> getItemsForDetDenuncia1() {
		itemsForDetDenuncia1 = new ArrayList<SelectItem>();
		itemsForDetDenuncia1.add(new SelectItem("", ""));
		itemsForDetDenuncia1.add(new SelectItem("1", "1-ORAL (PERSONAL)"));
		itemsForDetDenuncia1.add(new SelectItem("2", "2-ESCRITA"));
		itemsForDetDenuncia1.add(new SelectItem("3", "3-TELEFONICA"));
		itemsForDetDenuncia1.add(new SelectItem("4", "4-CORREO ELECTRONICO"));
		itemsForDetDenuncia1.add(new SelectItem("5", "5-POR SUPERVISION (FISCALIZACION)"));
		itemsForDetDenuncia1.add(new SelectItem("6", "6-POR MEDICIONES NTCSE"));
		itemsForDetDenuncia1.add(new SelectItem("7", "7-POR OTRA MODALIDAD"));
		return itemsForDetDenuncia1;
	}

	public void setItemsForDetDenuncia1(List<SelectItem> itemsForDetDenuncia1) {
		this.itemsForDetDenuncia1 = itemsForDetDenuncia1;
	}

	public String getCodDefTipicaRepFilter() {
		return codDefTipicaRepFilter;
	}

	public void setCodDefTipicaRepFilter(String codDefTipicaRepFilter) {
		this.codDefTipicaRepFilter = codDefTipicaRepFilter;
	}

	public Filter<?> getFilterCodDefTipicaRep() {
		return new Filter<HistoricoDeficienciaAp>() {
			public boolean accept(HistoricoDeficienciaAp t) {
				String codDefTipicaRep = getCodDefTipicaRepFilter();
				if (codDefTipicaRep == null || codDefTipicaRep.length() == 0
						|| codDefTipicaRep.equals(t.getCodDefTipicaRep())) {
					return true;
				}
				return false;
			}
		};
	}

	public List<SelectItem> getItemsCodDefTipicaRep1() {
		itemsCodDefTipicaRep1 = new ArrayList<SelectItem>();
		itemsCodDefTipicaRep1.add(new SelectItem("", ""));
		itemsCodDefTipicaRep1.add(new SelectItem("DT1",
				"DT1 - LAMPARA INOPERATIVA"));
		itemsCodDefTipicaRep1.add(new SelectItem("DT2",
				"DT2 - PASTORAL ROTO O MAL ORIENTADO"));
		itemsCodDefTipicaRep1.add(new SelectItem("DT3",
				"DT3 - FALTA DE UNIDAD DE AP"));
		itemsCodDefTipicaRep1.add(new SelectItem("DT4",
				"DT4 - INTERFERENCIA DE ARBOL"));
		itemsCodDefTipicaRep1.add(new SelectItem("DT5",
				"DT5 - DIFUSOR INOPERATIVO"));
		return itemsCodDefTipicaRep1;
	}

	public void setItemsCodDefTipicaRep1(List<SelectItem> itemsCodDefTipicaRep1) {
		this.itemsCodDefTipicaRep1 = itemsCodDefTipicaRep1;
	}

	public String getClaZonaFilter() {
		return claZonaFilter;
	}

	public void setClaZonaFilter(String claZonaFilter) {
		this.claZonaFilter = claZonaFilter;
	}

	public Filter<?> getFilterClaZona() {
		return new Filter<HistoricoDeficienciaAp>() {
			public boolean accept(HistoricoDeficienciaAp t) {
				String claZona = getClaZonaFilter();
				if (claZona == null || claZona.length() == 0
						|| claZona.equals(t.getClaZona())) {
					return true;
				}
				return false;
			}
		};
	}

	public List<SelectItem> getItemsClaZona1() {
		itemsClaZona1 = new ArrayList<SelectItem>();
		itemsClaZona1.add(new SelectItem("", ""));
		itemsClaZona1.add(new SelectItem("ST1", "SECTOR TIPICO 1"));
		itemsClaZona1.add(new SelectItem("ST2", "SECTOR TIPICO 2"));
		itemsClaZona1.add(new SelectItem("ST3", "SECTOR TIPICO 3"));
		itemsClaZona1.add(new SelectItem("ST4", "SECTOR TIPICO 4"));
		itemsClaZona1.add(new SelectItem("ST5", "SECTOR TIPICO 5"));
		itemsClaZona1.add(new SelectItem("ST6", "SECTOR TIPICO 6"));
		itemsClaZona1.add(new SelectItem("ST9", "SECTOR TIPICO 9"));
		return itemsClaZona1;
	}

	public void setItemsClaZona1(List<SelectItem> itemsClaZona1) {
		this.itemsClaZona1 = itemsClaZona1;
	}

	public String getCodDefTipicaVerFilter() {
		return codDefTipicaVerFilter;
	}

	public void setCodDefTipicaVerFilter(String codDefTipicaVerFilter) {
		this.codDefTipicaVerFilter = codDefTipicaVerFilter;
	}

	public Filter<?> getFilterCodDefTipicaVer() {
		return new Filter<HistoricoDeficienciaAp>() {
			public boolean accept(HistoricoDeficienciaAp t) {
				String codDefTipicaVer = getCodDefTipicaVerFilter();
				if (codDefTipicaVer == null || codDefTipicaVer.length() == 0
						|| codDefTipicaVer.equals(t.getCodDefTipicaVer())) {
					return true;
				}
				return false;
			}
		};
	}

	public List<SelectItem> getItemsCodDefTipicaVer1() {
		itemsCodDefTipicaVer1 = new ArrayList<SelectItem>();
		itemsCodDefTipicaVer1.add(new SelectItem("", ""));
		itemsCodDefTipicaVer1.add(new SelectItem("DT1",
				"DT1 - LAMPARA INOPERATIVA"));
		itemsCodDefTipicaVer1.add(new SelectItem("DT2",
				"DT2 - PASTORAL ROTO O MAL ORIENTADO"));
		itemsCodDefTipicaVer1.add(new SelectItem("DT3",
				"DT3 - FALTA DE UNIDAD DE AP"));
		itemsCodDefTipicaVer1.add(new SelectItem("DT4",
				"DT4 - INTERFERENCIA DE ARBOL"));
		itemsCodDefTipicaVer1.add(new SelectItem("DT5",
				"DT5 - DIFUSOR INOPERATIVO"));
		return itemsCodDefTipicaVer1;
	}

	public void setItemsCodDefTipicaVer1(List<SelectItem> itemsCodDefTipicaVer1) {
		this.itemsCodDefTipicaVer1 = itemsCodDefTipicaVer1;
	}

	public String getSolAmpPlazoFilter() {
		return solAmpPlazoFilter;
	}

	public void setSolAmpPlazoFilter(String solAmpPlazoFilter) {
		this.solAmpPlazoFilter = solAmpPlazoFilter;
	}

	public Filter<?> getFilterSolAmpPlazo() {
		return new Filter<HistoricoDeficienciaAp>() {
			public boolean accept(HistoricoDeficienciaAp t) {
				String solAmpPlazo = getSolAmpPlazoFilter();
				if (solAmpPlazo == null || solAmpPlazo.length() == 0
						|| solAmpPlazo.equals(t.getSolAmpPlazo())) {
					return true;
				}
				return false;
			}
		};
	}

	public List<SelectItem> getItemsSolAmpPlazo1() {
		itemsSolAmpPlazo1 = new ArrayList<SelectItem>();
		itemsSolAmpPlazo1.add(new SelectItem("", ""));
		itemsSolAmpPlazo1.add(new SelectItem("S", "SI"));
		itemsSolAmpPlazo1.add(new SelectItem("N", "NO"));
		return itemsSolAmpPlazo1;
	}

	public void setItemsSolAmpPlazo1(List<SelectItem> itemsSolAmpPlazo1) {
		this.itemsSolAmpPlazo1 = itemsSolAmpPlazo1;
	}

	public void generarAnexo3() {
		final FacesContext facesContext = FacesContext.getCurrentInstance();
		String rutaUp;
		String rutaArchivoOut;
		String path = System.getProperty("uploads.folder");
		String nombreArchivo;
		int numFila;

		rutaUp = facesContext.getExternalContext().getRealPath(
				"/reportes/ReporteAnexo3_078.xlsx");

		String result;
		Sql sql = new Sql("CALIDAD");
		String trimestre = null;
		if (periodoMedicion.getPeriodo().equals("01")
				|| periodoMedicion.getPeriodo().equals("02")
				|| periodoMedicion.getPeriodo().equals("03")) {
			trimestre = "1";
		}
		if (periodoMedicion.getPeriodo().equals("04")
				|| periodoMedicion.getPeriodo().equals("05")
				|| periodoMedicion.getPeriodo().equals("06")) {
			trimestre = "2";
		}

		if (periodoMedicion.getPeriodo().equals("07")
				|| periodoMedicion.getPeriodo().equals("08")
				|| periodoMedicion.getPeriodo().equals("09")) {
			trimestre = "3";
		}

		if (periodoMedicion.getPeriodo().equals("10")
				|| periodoMedicion.getPeriodo().equals("11")
				|| periodoMedicion.getPeriodo().equals("12")) {
			trimestre = "4";
		}

		String ano = null;
		EmpresaService empService = new EmpresaService();
		ano = periodoMedicion.getAno();
		String selectSql = "SELECT COD_DEF_TIPICA, DEN_PENDENTES,DEN_PRESENTADAS,DEN_RESUELTAS,DEN_DESESTIMADAS,DEN_PROXIMO,DEN_DEN_PLAZO,DEN_FUE_PLAZO,DEN_SOL_AMPLIACION,DAFP FROM CAL_RESUMEN_SUBSANACION WHERE ANO = '"
				+ ano
				+ "' AND TRIMESTRE = '"
				+ trimestre
				+ "' ORDER BY COD_DEF_TIPICA ASC";
		List<Object[]> objTabla = sql.consulta(selectSql, false);
		nombreArchivo = "Anexo 3AP_ELSM"
				+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss")
						.format(new Date()) + ".xlsx";
		rutaArchivoOut = path + nombreArchivo;

		XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);
		XSSFSheet objHssfSheet = null;
		objHssfSheet = objHssfWorkbook.getSheetAt(0);
		ModificarExcel.setCellValue(objHssfSheet, 3, 2, empService.getAllRows()
				.get(0).getEmpresa());
		ModificarExcel.setCellValue(objHssfSheet, 3, 5, trimestre);
		ModificarExcel.setCellValue(objHssfSheet, 3, 8, ano);
		if (objTabla != null) {
			String fila;
			int intRow = 8;
			int intCell = 1;
			String codDefTipica = null;
			for (Object[] objFila : objTabla) {
				numFila = 0;
				BigDecimal campoBigDecimal;
				String campoString;
				for (Object object : objFila) {
					if (numFila == 0) {
						codDefTipica = (String) object;
						if (codDefTipica.equals("DT1")) {
							intRow = 8;
							intCell = 1;
						}
						if (codDefTipica.equals("DT2")) {
							intRow = 9;
							intCell = 1;
						}
						if (codDefTipica.equals("DT3")) {
							intRow = 10;
							intCell = 1;
						}
						if (codDefTipica.equals("DT4")) {
							intRow = 11;
							intCell = 1;
						}
						if (codDefTipica.equals("DT5")) {
							intRow = 12;
							intCell = 1;
						}
					}
					if (numFila > 0) {
						try {
							campoString = (String) object;
							ModificarExcel.setCellValue(objHssfSheet, intRow,
									intCell, campoString);
						} catch (Exception e) {
							campoBigDecimal = (BigDecimal) object;
							ModificarExcel.setCellValue(objHssfSheet, intRow,
									intCell, campoBigDecimal);
						}
						intCell++;
						// NumberFormatException
					}
					numFila++;
				}
			}

			ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);

			HttpServletResponse response = ((HttpServletResponse) facesContext
					.getExternalContext().getResponse());
			writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,
					"application/vnd.ms-excel");// "text/xml"
			facesContext.responseComplete();
		}
	}

	void writeOutContent(final HttpServletResponse res, final File content,
			final String theFilename, String contentType) {
		if (content == null)
			return;
		try {
			res.setHeader("Pragma", "no-cache");
			res.setDateHeader("Expires", 0);
			res.setContentType(contentType);
			res.setHeader("Content-disposition", "attachment; filename="
					+ theFilename);
			fastChannelCopy(Channels.newChannel(new FileInputStream(content)),
					Channels.newChannel(res.getOutputStream()));
		} catch (final IOException e) {
			// TODO produce a error message <img
			// src="http://s0.wp.com/wp-includes/images/smilies/icon_smile.gif?m=1129645325g"
			// alt=":)" class="wp-smiley">
		}
	}

	void fastChannelCopy(final ReadableByteChannel src,
			final WritableByteChannel dest) throws IOException {
		final ByteBuffer buffer = ByteBuffer.allocateDirect(1000 * 1024);
		while (src.read(buffer) != -1) {
			buffer.flip();
			dest.write(buffer);
			buffer.compact();
		}
		buffer.flip();
		while (buffer.hasRemaining()) {
			dest.write(buffer);
		}
	}

	public java.util.Date getFecMaxResolucionFilter() {
		return fecMaxResolucionFilter;
	}

	public void setFecMaxResolucionFilter(java.util.Date fecMaxResolucionFilter) {
		this.fecMaxResolucionFilter = fecMaxResolucionFilter;
	}

	public List<SelectItem> getItemsTipoSin() {
		itemsTipoSin = new ArrayList<SelectItem>();
		itemsTipoSin.add(new SelectItem(1,
				"SINCRONIZAR REGISTROS DE LA FECHA ESPECIFICADA"));
		itemsTipoSin.add(new SelectItem(2,
				"SINCRONIZAR REGISTROS DEL PERIODO SELECCIONADO"));
		return itemsTipoSin;
	}

	public void setItemsTipoSin(List<SelectItem> itemsTipoSin) {
		this.itemsTipoSin = itemsTipoSin;
	}

	public long getTipoSin() {
		return tipoSin;
	}

	public void setTipoSin(long tipoSin) {
		this.tipoSin = tipoSin;
	}

	public List<SelectItem> getItemsOperacion() {
		itemsOperacion = new ArrayList<SelectItem>();
		itemsOperacion.add(new SelectItem("SIRN",
				"SOLO INSERTAR REGISTROS NUEVOS"));
		itemsOperacion.add(new SelectItem("AREIRN",
				"ACTUALIZAR REGISTROS EXISTENTES E INSERTAR NUEVOS"));
		itemsOperacion.add(new SelectItem("EREITN",
				"ELIMINAR REGISTROS EXISTENTES E INSERTAR NUEVAMENTE"));
		return itemsOperacion;
	}

	public void setItemsOperacion(List<SelectItem> itemsOperacion) {
		this.itemsOperacion = itemsOperacion;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public java.util.Date getFechaSincronizar() {
		return fechaSincronizar;
	}

	public void setFechaSincronizar(java.util.Date fechaSincronizar) {
		this.fechaSincronizar = fechaSincronizar;
	}

	public String sincronizarDeficienciasAP() {

		ConectaDb db = new ConectaDb("CALIDAD");
		Connection cn = db.getConnection();
		CallableStatement cstmt = null;
		int ctos;
		String result;
		try {

			cstmt = cn.prepareCall("{CALL CAL_PROC_078.SINCRONIZAR_DEFICIENCIAS_AP(?,?,?,?,?)}");

			cstmt.setString(1, periodoMedicion.getAno());
			cstmt.setString(2, periodoMedicion.getPeriodo());
			cstmt.setLong(3, tipoSin);
			cstmt.setString(4, operacion);
			cstmt.setDate(5, new java.sql.Date(fechaSincronizar.getTime()));

			System.out.println("ANO:" + periodoMedicion.getAno());
			System.out.println("MES:" + periodoMedicion.getPeriodo());
			System.out.println("TIPO SIN:" + tipoSin);
			System.out.println("OPERACION:" + operacion);
			System.out.println("FECHA SIN:" + fechaSincronizar);
			
			ctos = cstmt.executeUpdate();

			if (ctos == 0) {
				result = "0 filas afectadas";
			}

		} catch (SQLException e) {
			result = e.getMessage();
			setMensaje(e.getMessage() + ":" + e.getCause());
			FacesContext.getCurrentInstance()
					.addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR,
									"No se pudo completar la Transacci�n",
									getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash()
					.setKeepMessages(true);

		} finally {
			try {
				cn.close();
			} catch (SQLException e) {
				result = e.getMessage();
				setMensaje(e.getMessage() + ":" + e.getCause());
				FacesContext.getCurrentInstance().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"No se pudo completar la Transacci�n",
								getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext()
						.getFlash().setKeepMessages(true);

			}
		}

		HistoricoDeficienciaApxPeriodo();

		return "/zonasegura/proc078/historicoDeficienciaApListar?faces-redirect=true";
	}

	public String calcularAnexo3(){
		ConectaDb db = new ConectaDb("CALIDAD");		
        Connection cn = db.getConnection();
        CallableStatement cstmt = null;  
        int ctos;
        String result;
		String trimestre = null;
        
        try {
		
			cstmt = cn.prepareCall("{CALL CAL_PROC_078.GENERAR_RESUMEN_SUBSANACION(?,?)}"); 
			
			if(periodoMedicion.getPeriodo().equals("01") || periodoMedicion.getPeriodo().equals("02") ||
					periodoMedicion.getPeriodo().equals("03")){
				trimestre = "1";
			}
			
			if(periodoMedicion.getPeriodo().equals("04") || periodoMedicion.getPeriodo().equals("05") ||
					periodoMedicion.getPeriodo().equals("06")){
				trimestre = "2";
			}
			
			if(periodoMedicion.getPeriodo().equals("07") || periodoMedicion.getPeriodo().equals("08") ||
					periodoMedicion.getPeriodo().equals("09")){
				trimestre = "3";
			}
			
			if(periodoMedicion.getPeriodo().equals("10") || periodoMedicion.getPeriodo().equals("11") ||
					periodoMedicion.getPeriodo().equals("12")){
				trimestre = "4";
			}
			
			cstmt.setString(1,periodoMedicion.getAno());
			cstmt.setString(2,trimestre);
							
			ctos = cstmt.executeUpdate();
		
			if (ctos == 0) {
				result = "0 filas afectadas";
			}
		
		} catch (SQLException e) {
	        result = e.getMessage();
			setMensaje(e.getMessage()+":"+e.getCause());
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacci�n", getMensaje()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	            
	    } finally {
	        try {
	            cn.close();
	        } catch (SQLException e) {
	            result = e.getMessage();
				setMensaje(e.getMessage()+":"+e.getCause());
				FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"No se pudo completar la Transacci�n", getMensaje()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	            
	        }
	    }
		
		HistoricoDeficienciaApxPeriodo();
		return null;
	}

	public List<SelectItem> getItemsTrimestre() {
		itemsTrimestre = new ArrayList<SelectItem>();
		itemsTrimestre.add(new SelectItem("1", "1 TRIMESTRE"));
		itemsTrimestre.add(new SelectItem("2", "2 TRIMESTRE"));
		itemsTrimestre.add(new SelectItem("3", "3 TRIMESTRE"));
		itemsTrimestre.add(new SelectItem("4", "4 TRIMESTRE"));
		return itemsTrimestre;
	}

	public void setItemsTrimestre(List<SelectItem> itemsTrimestre) {
		this.itemsTrimestre = itemsTrimestre;
	}

	public java.util.Date sumarRestarDiasFechaCalendario(java.util.Date fecha,
			long dias) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha); // Configuramos la fecha que se recibe
		calendar.add(Calendar.DAY_OF_YEAR, (int) dias); // numero de d�as a
														// a�adir, o restar en
														// caso de d�as<0
		return calendar.getTime(); // Devuelve el objeto Date con los nuevos
									// d�as a�adidos

	}

	public java.util.Date sumarRestarDiasFechaHabiles(java.util.Date fecha,
			long dias) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha); // Configuramos la fecha que se recibe
		long numDias = 0;

		while (numDias < dias) {
			calendar.add(Calendar.DAY_OF_YEAR, 1);
			if (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY
					&& calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY) {
				numDias = numDias + 1;
			}
		}

		return calendar.getTime();
	}

	public String fechaRecepcionCambio() {
		java.util.Date fecMax;
		String servicio = null;
		String codDeficiencia = null;
		if (historicoDeficienciaAp.getLocalidad() != null) {

			if (!historicoDeficienciaAp.getLocalidad().getSisElectrico()
					.substring(0, 2).equals("SR")) {
				if (historicoDeficienciaAp.getLocalidad().getSecTipico()
						.equals("1")
						|| historicoDeficienciaAp.getLocalidad().getSecTipico()
								.equals("2")
						|| historicoDeficienciaAp.getLocalidad().getSecTipico()
								.equals("3")) {

					servicio = "U";
				}

				if (historicoDeficienciaAp.getLocalidad().getSecTipico()
						.equals("4")
						|| historicoDeficienciaAp.getLocalidad().getSecTipico()
								.equals("5")
						|| historicoDeficienciaAp.getLocalidad().getSecTipico()
								.equals("6")) {

					servicio = "R";
				}
			} else {
				servicio = "R";
			}

			ParametroNormaService parametroNormaService = new ParametroNormaService();
			ParametroNorma parametroNorma = null;

			if (historicoDeficienciaAp.getCodDefTipicaVer() != null) {
				if (!historicoDeficienciaAp.getCodDefTipicaVer().trim()
						.equals("")) {
					codDeficiencia = historicoDeficienciaAp
							.getCodDefTipicaVer();
				} else {
					codDeficiencia = historicoDeficienciaAp
							.getCodDefTipicaRep();
				}
			} else {
				codDeficiencia = historicoDeficienciaAp.getCodDefTipicaRep();
			}

			if (servicio.equals("R") && codDeficiencia.equals("DT1")) {
				parametroNorma = parametroNormaService.ReadByCod("PDT1_R");
			}

			if (servicio.equals("R") && codDeficiencia.equals("DT2")) {
				parametroNorma = parametroNormaService.ReadByCod("PDT2_R");
			}

			if (servicio.equals("R") && codDeficiencia.equals("DT3")) {
				parametroNorma = parametroNormaService.ReadByCod("PDT3_R");
			}

			if (servicio.equals("R") && codDeficiencia.equals("DT4")) {
				parametroNorma = parametroNormaService.ReadByCod("PDT4_R");
			}

			if (servicio.equals("R") && codDeficiencia.equals("DT5")) {
				parametroNorma = parametroNormaService.ReadByCod("PDT5_R");
			}

			if (servicio.equals("U") && codDeficiencia.equals("DT1")) {
				parametroNorma = parametroNormaService.ReadByCod("PDT1_U");
			}
			if (servicio.equals("U") && codDeficiencia.equals("DT2")) {
				parametroNorma = parametroNormaService.ReadByCod("PDT2_U");
			}
			if (servicio.equals("U") && codDeficiencia.equals("DT3")) {
				parametroNorma = parametroNormaService.ReadByCod("PDT3_U");
			}
			if (servicio.equals("U") && codDeficiencia.equals("DT4")) {
				parametroNorma = parametroNormaService.ReadByCod("PDT4_U");
			}
			if (servicio.equals("U") && codDeficiencia.equals("DT5")) {
				parametroNorma = parametroNormaService.ReadByCod("PDT5_U");
			}

			if (fecRecepcion != null) {
				if (servicio.equals("R")
						&& historicoDeficienciaAp.getCodDefTipicaRep().equals(
								"DT1")) {
					fecMax = sumarRestarDiasFechaCalendario(fecRecepcion,
							(long) parametroNorma.getParNorValor().longValue());
				} else {
					fecMax = sumarRestarDiasFechaHabiles(fecRecepcion,
							(long) parametroNorma.getParNorValor().longValue());
				}

				fecMaxResolucion = fecMax;

			}
		}
		return null;
	}

	public List<SelectItem> getItemsSincronizar() {
		itemsSincronizar = new ArrayList<SelectItem>();
		itemsSincronizar.add(new SelectItem("SI", "SI"));
		itemsSincronizar.add(new SelectItem("NO", "NO"));
		return itemsSincronizar;
	}

	public void setItemsSincronizar(List<SelectItem> itemsSincronizar) {
		this.itemsSincronizar = itemsSincronizar;
	}

	public String getCumplimientoFilter() {
		return cumplimientoFilter;
	}

	public void setCumplimientoFilter(String cumplimientoFilter) {
		this.cumplimientoFilter = cumplimientoFilter;
	}

	public Filter<?> getFilterCumplimiento() {
		return new Filter<HistoricoDeficienciaAp>() {
			public boolean accept(HistoricoDeficienciaAp t) {
				String cumplimiento = getCumplimientoFilter();
				if (cumplimiento == null || cumplimiento.length() == 0
						|| cumplimiento.equals(t.getCumplimiento())) {
					return true;
				}
				return false;
			}
		};
	}

	public List<SelectItem> getItemsCumplimiento1() {
		itemsCumplimiento1 = new ArrayList<SelectItem>();
		itemsCumplimiento1.add(new SelectItem("", ""));
		itemsCumplimiento1.add(new SelectItem("N", "N-NO ATENDIDA"));
		itemsCumplimiento1.add(new SelectItem("0", "0-FUERA DE PLAZO"));
		itemsCumplimiento1.add(new SelectItem("1", "1-DENTRO PLAZO"));
		itemsCumplimiento1.add(new SelectItem("2", "2-DESESTIMADA"));
		return itemsCumplimiento1;
	}

	public void setItemsCumplimiento1(List<SelectItem> itemsCumplimiento1) {
		this.itemsCumplimiento1 = itemsCumplimiento1;
	}
	
	public String exportarHistoricoDeficienciaAp(){
		final FacesContext facesContext = FacesContext.getCurrentInstance();
	    
	    String rutaArchivoOut;
	    String rutaUp;
	    rutaUp = facesContext.getExternalContext().getRealPath("/reportes/AnexoHistoricoDefAp.xlsx");
	    String path = System.getProperty("uploads.folder");
	    String nombreArchivo;
	    
	    System.out.print(path + "\n");
	    
	    	    
	    nombreArchivo = "HistoricoDeficiencia" + "_" + getHistoricoDeficienciaAp().getCodIdeDenuncia()  + "_"+ new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss").format(new Date())+".xlsx";
	    rutaArchivoOut = path + nombreArchivo;
	    	    
	    XSSFWorkbook objHssfWorkbook = ModificarExcel.getPlantilla(rutaUp);

		XSSFSheet objHssfSheet = null;
		
		try  {
	    
	 
	    HistoricoDeficienciaApService service = new HistoricoDeficienciaApService();
	    
	    objHssfSheet = objHssfWorkbook.getSheetAt(0);
		
		int intRow = 1;
		int intCell = 0;
		
		EmpresaService empService = new EmpresaService();
		
		
			
		for (HistoricoDeficienciaAp historicoDeficienciaAp : service.getAllRows1(periodoMedicion.getPerMedicionId())) {
			
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell, historicoDeficienciaAp.getNumDenuncia());
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 1, historicoDeficienciaAp.getCodIdeDenuncia());//codIdeDenuncia
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 2, historicoDeficienciaAp.getCodIdeUniDeficiencia());//codIdeUniDeficiencia
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 3, historicoDeficienciaAp.getAno());//ano
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 4, historicoDeficienciaAp.getTrimestre());//trimestre
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 5, historicoDeficienciaAp.getSisElectrico());//sisElectrico
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 6, historicoDeficienciaAp.getFecRecepcion());//fecRecepcion
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 7, historicoDeficienciaAp.getForDetDenuncia());//forDetDenuncia
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 8, historicoDeficienciaAp.getNomDenunciante());//nomDenunciante
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 9, historicoDeficienciaAp.getDirFroUap());//dirFroUap
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 10, historicoDeficienciaAp.getDirDenunciante());//dirDenunciante
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 11, historicoDeficienciaAp.getDenTranscripcion());//denTranscripcion
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 12, historicoDeficienciaAp.getTelDenunciante());//telDenunciante
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 13, historicoDeficienciaAp.getCodDefTipicaRep());//codDefTipicaRep
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 14, historicoDeficienciaAp.getClaZona());//claZona
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 15, historicoDeficienciaAp.getUbigeo());//ubigeo
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 16, historicoDeficienciaAp.getFecVerCampo());//fecVerCampo
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 17, historicoDeficienciaAp.getCodDefTipicaVer());//codDefTipicaVer
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 18, historicoDeficienciaAp.getCodSed());//codSed
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 19, historicoDeficienciaAp.getCodUap());//codUap
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 20, historicoDeficienciaAp.getFecSubsanacion());//fecSubsanacion
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 21, historicoDeficienciaAp.getOtNumero());//otNumero
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 22, historicoDeficienciaAp.getNomResponsable());//nomResponsable
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 23, historicoDeficienciaAp.getCumplimiento());//cumplimiento
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 24, historicoDeficienciaAp.getCauNoCumplimiento());//cauNoCumplimiento
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 25, historicoDeficienciaAp.getSolAmpPlazo());//solAmpPlazo
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 26, historicoDeficienciaAp.getObservacion());//observacion
				if(historicoDeficienciaAp.getLocalidad() != null){
					ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 27, historicoDeficienciaAp.getLocalidad().getCodLocalidad()+"-"+historicoDeficienciaAp.getLocalidad().getNombre());
				}
				
				ModificarExcel.setCellValue(objHssfSheet,intRow, intCell + 28, historicoDeficienciaAp.getFecMaxResolucion());//fecMaxResolucion
				intRow++;
				

		}
		
		ModificarExcel.saveFile(rutaArchivoOut, objHssfWorkbook);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    HttpServletResponse response =((HttpServletResponse)facesContext.getExternalContext().getResponse());

	    writeOutContent(response, new File(rutaArchivoOut), nombreArchivo,"application/vnd.ms-excel" );// "text/xml"
	    facesContext.responseComplete();	
				
		return null;
	}
	
	
}
