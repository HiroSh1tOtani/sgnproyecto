package pe.com.calidad.entity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Proceso {
	private Integer IdCalProceso;
	private Date fechaHoraInicio;
	private Date fechaHoraInicioPaso;
	
	private Long fechaHoraInicioSegundos;
	private Long fechaHoraInicioPasoSegundos;
	
	private Date fechaHoraFin;
	private String desPaso;
	private String desSubPaso;
	private Integer paso;
	private Integer subPaso;
	private String tipoProceso;
	private Integer idTipoProceso;
	private Integer totalPasos;
	private String estado;
	
	public String getFechaHoraInicioString(){
		if (this.fechaHoraInicio!=null){
			return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(this.fechaHoraInicio);
		}
		return "";
	}
	
	
	public String getFechaHoraFinString(){
		if (this.fechaHoraFin!=null){
			return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(this.fechaHoraFin);
		}
		return "";
	}
	
	public String getFechaHoraInicioPasoString(){
		if (this.fechaHoraInicioPaso!=null){
			return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(this.fechaHoraInicioPaso);
		}
		return "";
	}
	
	public String getTiempoEjecucionString(){
		if (this.fechaHoraInicioSegundos!=null){
			return convertirSegundosHaciaDDHHmmss(this.fechaHoraInicioSegundos);
		}
		return "";
	}
	
	public String getTiempoEjecucionTotalString(){
		if (this.fechaHoraInicio!=null && this.fechaHoraFin!=null && this.estado.equals("2")){
			long diff = Math.abs(fechaHoraFin.getTime() - fechaHoraInicio.getTime());
			return convertirSegundosHaciaDDHHmmss(diff/1000);
		}
		return "";
	}
	
	public String getTiempoEjecucionPasoString(){
		if (this.fechaHoraInicioPasoSegundos!=null){
			return convertirSegundosHaciaDDHHmmss(this.fechaHoraInicioPasoSegundos);
		}
		return "";
	}
	
	private String convertirSegundosHaciaDDHHmmss(long seg){
		long dias = seg/(60*60*24);
		long horas = (seg - (60*60*24)*dias)/(60*60);
		long minutos = (seg - (60*60*24)*dias - (60*60)*horas)/60;
		long segundos = seg - (60*60*24)*dias - (60*60)*horas - (60)*minutos;
		return (dias<10?"0":"") + dias + " " + (horas<10?"0":"") + horas + ":" + (minutos<10?"0":"") + minutos + ":" + (segundos<10?"0":"") + segundos;
	}
	
	
	
	public Integer getIdTipoProceso() {
		return idTipoProceso;
	}
	public void setIdTipoProceso(Integer idTipoProceso) {
		this.idTipoProceso = idTipoProceso;
	}
	public String getTipoProceso() {
		return tipoProceso;
	}
	public void setTipoProceso(String tipoProceso) {
		this.tipoProceso = tipoProceso;
	}
	public Date getFechaHoraInicioPaso() {
		return fechaHoraInicioPaso;
	}
	public void setFechaHoraInicioPaso(Date fechaHoraInicioPaso) {
		this.fechaHoraInicioPaso = fechaHoraInicioPaso;
	}
	public Integer getIdCalProceso() {
		return IdCalProceso;
	}
	public void setIdCalProceso(Integer idCalProceso) {
		IdCalProceso = idCalProceso;
	}
	public Date getFechaHoraInicio() {
		return fechaHoraInicio;
	}
	public void setFechaHoraInicio(Date fechaHoraInicio) {
		this.fechaHoraInicio = fechaHoraInicio;
	}
	public Date getFechaHoraFin() {
		return fechaHoraFin;
	}
	public void setFechaHoraFin(Date fechaHoraFin) {
		this.fechaHoraFin = fechaHoraFin;
	}
	public String getDesPaso() {
		return desPaso;
	}
	public void setDesPaso(String desPaso) {
		this.desPaso = desPaso;
	}
	public String getDesSubPaso() {
		return desSubPaso;
	}
	public void setDesSubPaso(String desSubPaso) {
		this.desSubPaso = desSubPaso;
	}
	public Integer getPaso() {
		return paso;
	}
	public void setPaso(Integer paso) {
		this.paso = paso;
	}
	public Integer getSubPaso() {
		return subPaso;
	}
	public void setSubPaso(Integer subPaso) {
		this.subPaso = subPaso;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Integer getTotalPasos() {
		return totalPasos;
	}

	public void setTotalPasos(Integer totalPasos) {
		this.totalPasos = totalPasos;
	}


	public Long getFechaHoraInicioSegundos() {
		return fechaHoraInicioSegundos;
	}


	public void setFechaHoraInicioSegundos(Long fechaHoraInicioSegundos) {
		this.fechaHoraInicioSegundos = fechaHoraInicioSegundos;
	}


	public Long getFechaHoraInicioPasoSegundos() {
		return fechaHoraInicioPasoSegundos;
	}


	public void setFechaHoraInicioPasoSegundos(Long fechaHoraInicioPasoSegundos) {
		this.fechaHoraInicioPasoSegundos = fechaHoraInicioPasoSegundos;
	}
	
	
	
	
}
