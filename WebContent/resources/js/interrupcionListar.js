			function mostrarProcesosEjecucion(){
				CONT.util.fnConsultar({
	                ruta: '../../../rest/Interrupcion/listarInterrupcionTareasEnEjecucion',
	                parametro: {anio:2017},
	                fnExito: function (r) {
	                	if (r!=null){
	                		if (r.length>0){
		                		$('#idDivProcesoActivo').show();
			           			$('#idDiv1').html(r[0].tipoProceso);
			           			$('#idDiv2').html(r[0].fechaHoraInicioString);
			           			$('#idDiv3').html("Tarea " + r[0].paso + " de " + r[0].totalPasos);
			           			$('#idDiv4').html(r[0].desPaso);
			           			$('#idDiv5').html(r[0].fechaHoraInicioPasoString);
			           			$('#idDiv6').html(r[0].desSubPaso);
			           			$('#idDiv7').html(r[0].tiempoEjecucionString);
			           			$('#idDiv8').html(r[0].tiempoEjecucionPasoString);
		                	}
	                		else{
		                		$('#idDivProcesoActivo').hide();
		                	}
	                	}
	                	else{
	                		$('#idDivProcesoActivo').hide();
	                	}
	                	
	                }
	            });
			}
			function mostrarProcesosHistorico(){
				CONT.util.fnConsultar({
	                ruta: '../../../rest/Interrupcion/listarUltimo5Proceso',
	                parametro: {anio:2017},
	                fnExito: function (r) {
	                	if (r!=null){
	                		if (r.length>0){
	                			$('#idDivProcesoHistorico').html('');
	                			var html = '<table><tr><th>Ultimos procesos ejecutados</th><th>Fecha Inicio Ejecuci&oacute;n</th><th>Fecha Fin Ejecuci&oacute;n</th><th>Tiempo Total (dd HH:mm:ss)</th></tr>';

	                			for(var i=0;i<r.length;i=i+1)
	                			{
	                				html = html + '<tr>';
	                				html = html + '<td><div>' + r[i].tipoProceso + '</div></td>';
	                				html = html + '<td><div>' + r[i].fechaHoraInicioString + '</div></td>';
	                				html = html + '<td><div>' + r[i].fechaHoraFinString + '</div></td>';
	                				html = html + '<td><div>' + r[i].tiempoEjecucionTotalString + '</div></td>';
	                				html = html + '</tr>';
	                			}
	                			html = html + '</table>';
	                			$('#idDivProcesoHistorico').html(html);
		                	}
	                		else{
	                			$('#idDivProcesoHistorico').html('');
		                	}
	                	}
	                	else{
	                		$('#idDivProcesoHistorico').html('');
	                	}
	                	
	                }
	            });
			}
			$(function () {
				mostrarProcesosEjecucion();
				mostrarProcesosHistorico();
	        });