var CONT = {};
CONT.util = {};
CONT.util.fnConsultar = function(def){
	
	var parametro = {};
	if (def.parametro!=null)
		parametro = def.parametro;
	$.ajax({
	    type: "POST",
	    url: def.ruta,
	    dataType: 'json',
	    data: JSON.stringify(parametro),
	    contentType: "application/json",
	    headers: { 
			'Content-Type': 'application/json',
	        'Accept': 'application/json'
		},
		success: function(result,status,xhr){
			if (def.fnExito != null)
	            def.fnExito(result);
		},
		error: function(xhr,status,error){
			if (def.fnError != null)
	            def.fnError(error);
		}
		/*
		accepts: {
		    mycustomtype: 'application/x-some-custom-type'
		  }*/
	})
}


