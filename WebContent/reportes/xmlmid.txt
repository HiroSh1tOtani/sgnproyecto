											</object>
										</void>
										<void property="Activo">
											<object class="com.soluzionasf.bdi.conspet.filtros.arbolfiltros.XMLComponenteActivo" >
												<void property="Activo">
													<boolean>true</boolean>
												</void>
											</object>
										</void>
										<void property="Oculto">
											<boolean>false</boolean>
										</void>
										<void property="AccionesBDI">
											<object class="java.util.ArrayList" >
												<void size="3" />
												<void method="add">
													<object class="com.soluzionasf.bdi.conspet.filtros.editorfiltros.AccionBDI" >
														<void property="Nombre">
															<string>Cambiar s�mbolo</string>
														</void>
														<void property="Visible">
															<boolean>true</boolean>
														</void>
														<void property="Accion">
															<object class="com.soluzionasf.onis.datosgraficos.acciones.AccionSetSimbolo" >
																<void property="Simbolo">
																	<int>14</int>
																</void>
															</object>
														</void>
													</object>
												</void>
												<void method="add">
													<object class="com.soluzionasf.bdi.conspet.filtros.editorfiltros.AccionBDI" >
														<void property="Nombre">
															<string>Cambiar color</string>
														</void>
														<void property="Visible">
															<boolean>true</boolean>
														</void>
														<void property="Accion">
															<object class="com.soluzionasf.onis.datosgraficos.acciones.AccionSetColor" >
																<void property="Color">
																	<object class="java.awt.Color" >
																		<void red="255" green="0" blue="0" alpha="255" />
																	</object>
																</void>
																<void property="Modo">
																	<int>1</int>
																</void>
															</object>
														</void>
													</object>
												</void>
												<void method="add">
													<object class="com.soluzionasf.bdi.conspet.filtros.editorfiltros.AccionBDI" >
														<void property="Nombre">
															<string>Factor tama�o</string>
														</void>
														<void property="Visible">
															<boolean>true</boolean>
														</void>
														<void property="Accion">
															<object class="com.soluzionasf.onis.datosgraficos.acciones.AccionSetFactor" >
																<void property="Factor">
																	<float>8.0</float>
																</void>
																<void property="Campo">
																	<null/>
																</void>
																<void property="PorAttr">
																	<boolean>false</boolean>
																</void>
																<void property="Multiplica">
																	<boolean>true</boolean>
																</void>
																<void property="PosTxt">
																	<int>-1</int>
																</void>
															</object>
														</void>
													</object>
												</void>
											</object>
										</void>
										<void property="Descripcion">
											<object class="com.soluzionasf.bdi.conspet.filtros.arbolfiltros.XMLComponenteDescr" >
												<void property="Nombre">
													<string>Filtro_Suministros</string>
												</void>
												<void property="Tag">
													<string>Descripcion</string>
												</void>
												<void property="Descripcion">
													<string></string>
												</void>
											</object>
										</void>